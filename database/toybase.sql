--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.4

-- Started on 2018-12-31 15:36:17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2798 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 19788)
-- Name: libraries; Type: TABLE; Schema: public; Owner: toybase
--

CREATE TABLE public.libraries (
    library_code character varying(25) NOT NULL,
    dbpassword character varying(25)
);


ALTER TABLE public.libraries OWNER TO toybase;

--
-- TOC entry 2669 (class 2606 OID 19792)
-- Name: libraries library_code; Type: CONSTRAINT; Schema: public; Owner: toybase
--

ALTER TABLE ONLY public.libraries
    ADD CONSTRAINT library_code PRIMARY KEY (library_code);


-- Completed on 2018-12-31 15:36:17

--
-- PostgreSQL database dump complete
--

