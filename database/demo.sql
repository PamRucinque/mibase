--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.4

-- Started on 2018-12-28 11:10:30

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3321 (class 1262 OID 30994)
-- Name: demo; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE demo WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


\connect demo

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3323 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 294 (class 1255 OID 30995)
-- Name: date_round(timestamp with time zone, interval); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.date_round(base_date timestamp with time zone, round_interval interval) RETURNS timestamp with time zone
    LANGUAGE sql STABLE
    AS $_$
SELECT TO_TIMESTAMP((EXTRACT(epoch FROM $1)::INTEGER + EXTRACT(epoch FROM $2)::INTEGER / 2)
                / EXTRACT(epoch FROM $2)::INTEGER * EXTRACT(epoch FROM $2)::INTEGER)
$_$;


--
-- TOC entry 196 (class 1259 OID 30996)
-- Name: age_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.age_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 30998)
-- Name: age; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.age (
    id bigint DEFAULT nextval('public.age_id_seq'::regclass) NOT NULL,
    age text NOT NULL
);


--
-- TOC entry 198 (class 1259 OID 31005)
-- Name: bor_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 199 (class 1259 OID 31007)
-- Name: borwrs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.borwrs (
    id bigint NOT NULL,
    firstname character varying(100),
    surname character varying(100),
    membertype character varying(50),
    motherssurname character varying(100),
    partnersname character varying(100),
    partnerssurname character varying(100),
    address character varying(100),
    address2 character varying(100),
    address3 character varying(100),
    suburb character varying(100),
    city character varying(100),
    state character varying(100),
    postcode character varying(50),
    country character varying(50),
    work character varying(50),
    phone character varying(50),
    phone2 character varying(50),
    mobile1 character varying(50),
    mobile2 character varying(40),
    email boolean,
    email2 character varying(100),
    emailaddress character varying(100),
    rostertype character varying(100),
    rostertype2 character varying(100),
    rostertype3 character varying(40),
    rostertype4 character varying(40),
    rostertype5 character varying(40),
    id_license character(25),
    skills text,
    skills2 text,
    specialneeds text,
    alert text,
    notes text,
    rosternotes text,
    datejoined date,
    renewed date,
    expired date,
    resigneddate date,
    created timestamp with time zone DEFAULT now(),
    modified timestamp with time zone DEFAULT now(),
    flag boolean,
    lockdate date,
    levy boolean,
    lockreason character varying(200),
    resignedreason character varying(200),
    balance double precision,
    discoverytype character varying(50),
    lastarchive date,
    location character(50),
    member_status character varying(50),
    printbarcode boolean DEFAULT false,
    library character varying(50),
    pwd character varying(50) DEFAULT 'mibase'::character varying,
    helmet boolean,
    user1 character varying,
    rostertype6 character varying(100),
    marital_status character varying(20),
    age date,
    member_update character varying(50),
    changedby character varying(20),
    agree character varying(5),
    photos character varying(5),
    key character varying,
    helmet_date timestamp with time zone,
    dollar_to_date numeric(12,2),
    agree_date timestamp with time zone,
    wwc character varying(50),
    value_ytd numeric(12,2) DEFAULT 0,
    date_application date,
    conduct character varying,
    conduct_date timestamp with time zone,
    rego_id bigint,
    wwc2 character varying(50)
);


--
-- TOC entry 200 (class 1259 OID 31018)
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 201 (class 1259 OID 31020)
-- Name: category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.category (
    id bigint DEFAULT nextval('public.category_id_seq'::regclass) NOT NULL,
    category character varying(3) NOT NULL,
    description text,
    toys integer DEFAULT 1
);


--
-- TOC entry 202 (class 1259 OID 31028)
-- Name: child_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.child_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 203 (class 1259 OID 31030)
-- Name: children; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.children (
    childid bigint DEFAULT nextval('public.child_id_seq'::regclass) NOT NULL,
    child_name character varying(50),
    surname character varying(50),
    sex character varying(8),
    d_o_b date,
    notes character varying(1000),
    alert boolean,
    id bigint,
    changedby character varying(20)
);


--
-- TOC entry 204 (class 1259 OID 31037)
-- Name: city_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 205 (class 1259 OID 31039)
-- Name: city; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.city (
    id bigint DEFAULT nextval('public.city_id_seq'::regclass) NOT NULL,
    city character varying(50) NOT NULL,
    postcode character varying(10)
);


--
-- TOC entry 206 (class 1259 OID 31043)
-- Name: condition_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.condition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 207 (class 1259 OID 31045)
-- Name: condition; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.condition (
    id bigint DEFAULT nextval('public.condition_id_seq'::regclass) NOT NULL,
    condition character varying(50) NOT NULL
);


--
-- TOC entry 208 (class 1259 OID 31049)
-- Name: contact_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contact_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 209 (class 1259 OID 31051)
-- Name: contact_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contact_log (
    id bigint DEFAULT nextval('public.contact_log_id_seq'::regclass) NOT NULL,
    type_contact character varying,
    details character varying,
    datetime time with time zone[],
    borid bigint,
    template_id bigint,
    template character varying(20),
    subject character varying,
    email character varying,
    created timestamp with time zone DEFAULT now()
);


--
-- TOC entry 210 (class 1259 OID 31059)
-- Name: discovery_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.discovery_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 211 (class 1259 OID 31061)
-- Name: discovery; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.discovery (
    id bigint DEFAULT nextval('public.discovery_id_seq'::regclass) NOT NULL,
    discovery character varying(50) NOT NULL
);


--
-- TOC entry 212 (class 1259 OID 31065)
-- Name: event_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 213 (class 1259 OID 31067)
-- Name: event; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.event (
    id bigint DEFAULT nextval('public.event_id_seq'::regclass) NOT NULL,
    description text,
    memberid bigint,
    typeevent character varying(30),
    rostertype character varying(30),
    nohours numeric(12,1),
    completed boolean,
    alertuser boolean,
    event_date timestamp with time zone DEFAULT now(),
    amount double precision,
    username character varying(20)
);


--
-- TOC entry 214 (class 1259 OID 31075)
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 215 (class 1259 OID 31077)
-- Name: files; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.files (
    id bigint DEFAULT nextval('public.files_id_seq'::regclass) NOT NULL,
    filename character varying,
    description character varying,
    type_file character varying(20),
    lastupdate time with time zone,
    access character varying(20)
);


--
-- TOC entry 216 (class 1259 OID 31084)
-- Name: gift_cards_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gift_cards_sq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 217 (class 1259 OID 31086)
-- Name: gift_cards; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gift_cards (
    id bigint DEFAULT nextval('public.gift_cards_sq'::regclass) NOT NULL,
    p_name character varying,
    mobile character varying,
    email character varying,
    newsletter character varying(5),
    created timestamp with time zone,
    library character varying,
    email_sent character varying(5) DEFAULT 'No'::character varying,
    expired date,
    credit_sent character varying(5),
    amount numeric(12,2),
    borid bigint,
    online_id bigint
);


--
-- TOC entry 218 (class 1259 OID 31094)
-- Name: holds_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.holds_id_seq
    START WITH 126
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 219 (class 1259 OID 31096)
-- Name: homepage; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.homepage (
    id bigint NOT NULL,
    frontpage text,
    open_hours text,
    lastupdated date,
    webpage text,
    contact_info text,
    conditions text,
    payment_info character varying,
    castle character varying,
    castle_link character varying,
    helmet_waiver character varying,
    payment_info_join character varying,
    member_homepage character varying,
    wwc character varying,
    rego_id bigint,
    sponsor_page character varying,
    member_options character varying
);


--
-- TOC entry 220 (class 1259 OID 31102)
-- Name: id_seq_users; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.id_seq_users
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 221 (class 1259 OID 31104)
-- Name: journal_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.journal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 222 (class 1259 OID 31106)
-- Name: journal; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.journal (
    id bigint DEFAULT nextval('public.journal_id_seq'::regclass) NOT NULL,
    description text,
    name text,
    category text,
    cat2 text,
    typepayment text,
    bcode bigint,
    gst numeric(12,1),
    icode character varying(10),
    type character varying(2),
    amount numeric(12,1),
    datepaid date DEFAULT now(),
    debitdate timestamp with time zone DEFAULT now(),
    location character varying,
    changedby character varying(20),
    holdid bigint
);


--
-- TOC entry 223 (class 1259 OID 31115)
-- Name: loan_restrictions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loan_restrictions (
    id bigint NOT NULL,
    membertype character varying,
    category character varying,
    free bigint DEFAULT 0,
    weight numeric(12,2) DEFAULT 0,
    loan_type character varying NOT NULL,
    notes character varying(50)
);


--
-- TOC entry 224 (class 1259 OID 31123)
-- Name: location; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.location (
    id bigint NOT NULL,
    location character(15),
    description character varying(50)
);


--
-- TOC entry 225 (class 1259 OID 31126)
-- Name: log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 226 (class 1259 OID 31128)
-- Name: log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.log (
    ip character varying(20),
    id bigint DEFAULT nextval('public.log_id_seq'::regclass) NOT NULL,
    subdomain character varying(50),
    username character varying,
    password character varying,
    logintype character varying(10),
    status character varying,
    created timestamp without time zone DEFAULT now(),
    email character varying
);


--
-- TOC entry 227 (class 1259 OID 31136)
-- Name: manufacturer_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.manufacturer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 228 (class 1259 OID 31138)
-- Name: manufacturer; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.manufacturer (
    id bigint DEFAULT nextval('public.manufacturer_id_seq'::regclass) NOT NULL,
    manufacturer text,
    address text,
    phone text,
    fax text,
    email text,
    contact text,
    discount numeric(12,1)
);


--
-- TOC entry 229 (class 1259 OID 31145)
-- Name: membertype_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.membertype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 230 (class 1259 OID 31147)
-- Name: membertype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.membertype (
    id bigint DEFAULT nextval('public.membertype_id_seq'::regclass) NOT NULL,
    membertype character varying,
    maxnoitems bigint,
    description text,
    renewal_fee numeric(12,1),
    duties numeric(12,2),
    units bigint,
    bond numeric(12,1),
    rent numeric(12,1),
    returnperiod bigint,
    expiryperiod bigint,
    paypal character varying,
    exclude character varying(3),
    due date,
    jc character varying,
    self_checkout character varying(5),
    extra bigint,
    gold_star integer DEFAULT 0
);


--
-- TOC entry 231 (class 1259 OID 31155)
-- Name: nation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.nation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 232 (class 1259 OID 31157)
-- Name: nationality; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.nationality (
    id bigint DEFAULT nextval('public.nation_id_seq'::regclass) NOT NULL,
    nationality character varying NOT NULL,
    description character varying
);


--
-- TOC entry 233 (class 1259 OID 31164)
-- Name: notifications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.notifications (
    id bigint NOT NULL,
    notification_type character(10) DEFAULT 'email'::bpchar NOT NULL,
    notification_name character varying(50) NOT NULL,
    days_before integer DEFAULT 0,
    weekday character varying(15),
    username character varying,
    password character varying,
    source character varying,
    description character varying,
    active character varying(3)
);


--
-- TOC entry 234 (class 1259 OID 31172)
-- Name: overdue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.overdue (
    id integer NOT NULL,
    weekly_fine character varying(3) DEFAULT 'Yes'::character varying,
    rent_as_fine character varying(3) DEFAULT 'No'::character varying,
    fine_value numeric(5,2) DEFAULT 0,
    days_grace integer DEFAULT 0,
    record_fine character varying(3) DEFAULT 'No'::character varying,
    fine_factor numeric(5,2) DEFAULT 1,
    daily_limit numeric DEFAULT 0,
    daily_fine character varying(3),
    roundup_week character varying(3),
    hire_factor bigint DEFAULT 0 NOT NULL,
    extra_fine numeric(5,2) DEFAULT 0,
    custom character varying
);


--
-- TOC entry 235 (class 1259 OID 31187)
-- Name: parts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.parts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 236 (class 1259 OID 31189)
-- Name: parts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.parts (
    id bigint DEFAULT nextval('public.parts_id_seq'::regclass) NOT NULL,
    datepart date,
    itemno text,
    itemname text,
    description text NOT NULL,
    type text,
    qty bigint,
    found boolean,
    borcode bigint,
    cost numeric(12,1),
    foundby text,
    alertuser boolean,
    changedby character varying(20),
    borname character varying,
    comments character varying,
    paid boolean DEFAULT false
);


--
-- TOC entry 237 (class 1259 OID 31197)
-- Name: partypack; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.partypack (
    toyname character varying,
    id bigint NOT NULL,
    desc1 text,
    desc2 text,
    cost numeric(10,2),
    lastupdated date
);


--
-- TOC entry 238 (class 1259 OID 31203)
-- Name: partypack_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.partypack_id_seq
    START WITH 114
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 239 (class 1259 OID 31205)
-- Name: paymentoptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.paymentoptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 240 (class 1259 OID 31207)
-- Name: paymentoptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.paymentoptions (
    id bigint DEFAULT nextval('public.paymentoptions_id_seq'::regclass) NOT NULL,
    paymentoptions text NOT NULL,
    crdr text,
    accountcode text,
    amount numeric(12,1),
    typepayment text,
    group1 bigint,
    description character varying,
    "group" character varying
);


--
-- TOC entry 241 (class 1259 OID 31214)
-- Name: postcode_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.postcode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 242 (class 1259 OID 31216)
-- Name: postcode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.postcode (
    id bigint DEFAULT nextval('public.postcode_id_seq'::regclass) NOT NULL,
    suburb text,
    postcode text
);


--
-- TOC entry 243 (class 1259 OID 31223)
-- Name: report_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.report_id_seq
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 244 (class 1259 OID 31225)
-- Name: reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reports (
    id bigint DEFAULT nextval('public.report_id_seq'::regclass) NOT NULL,
    reportname character varying,
    category character varying,
    sql character varying,
    code character varying(20)
);


--
-- TOC entry 245 (class 1259 OID 31232)
-- Name: reserve_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reserve_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 246 (class 1259 OID 31234)
-- Name: reservations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reservations (
    approved boolean,
    member_id bigint,
    date_start date,
    date_end date,
    date_created timestamp without time zone,
    partypack_id bigint,
    id bigint DEFAULT nextval('public.reserve_id_seq'::regclass) NOT NULL
);


--
-- TOC entry 247 (class 1259 OID 31238)
-- Name: reserve_toy_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reserve_toy_id_seq
    START WITH 118
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 248 (class 1259 OID 31240)
-- Name: reserve_toy; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reserve_toy (
    approved boolean,
    member_id bigint,
    date_start date,
    date_end date,
    date_created timestamp without time zone,
    partypack_id bigint,
    id bigint DEFAULT nextval('public.reserve_toy_id_seq'::regclass) NOT NULL,
    idcat character varying(50),
    borname text,
    phone text,
    status character varying(10),
    paid character varying(5),
    contact character varying,
    address character varying,
    emailaddress character varying
);


--
-- TOC entry 249 (class 1259 OID 31247)
-- Name: roster_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.roster_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_with_oids = true;

--
-- TOC entry 250 (class 1259 OID 31249)
-- Name: roster; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.roster (
    id bigint DEFAULT nextval('public.roster_id_seq'::regclass),
    date_roster date,
    member_id bigint,
    duration double precision DEFAULT 1,
    type_roster character varying(100),
    approved boolean DEFAULT false,
    session_role character varying(50),
    roster_session character varying(50),
    weekday character varying(100),
    date_created date,
    complete boolean DEFAULT false,
    location character varying,
    comments character varying,
    status character varying(10),
    modified timestamp with time zone,
    contact bigint DEFAULT 1,
    delete_comments character varying
);


--
-- TOC entry 251 (class 1259 OID 31260)
-- Name: rostertypes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.rostertypes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_with_oids = false;

--
-- TOC entry 252 (class 1259 OID 31262)
-- Name: rostertypes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rostertypes (
    id bigint DEFAULT nextval('public.rostertypes_id_seq'::regclass) NOT NULL,
    rostertype text,
    description character varying(50),
    nohours numeric DEFAULT 1,
    volunteers bigint,
    weekday character varying(10),
    location character varying,
    reservations character varying(3) DEFAULT 'Yes'::character varying
);


--
-- TOC entry 253 (class 1259 OID 31271)
-- Name: select_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.select_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 254 (class 1259 OID 31273)
-- Name: selectbox; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.selectbox (
    id bigint DEFAULT nextval('public.select_id_seq'::regclass) NOT NULL,
    selectbox character varying
);


--
-- TOC entry 255 (class 1259 OID 31280)
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 256 (class 1259 OID 31282)
-- Name: settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.settings (
    setting_name character varying NOT NULL,
    setting_value character varying,
    id bigint DEFAULT nextval('public.settings_id_seq'::regclass),
    description character varying,
    setting_type character varying
);


--
-- TOC entry 257 (class 1259 OID 31289)
-- Name: stats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 258 (class 1259 OID 31291)
-- Name: stats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stats (
    id bigint DEFAULT nextval('public.stats_id_seq'::regclass) NOT NULL,
    current bigint,
    new bigint,
    locked bigint,
    date_stats date,
    timezone character varying,
    children bigint,
    week bigint,
    loans bigint,
    returns bigint,
    location character varying(20),
    members bigint,
    renewed bigint,
    weekday character varying(20),
    expired bigint,
    resigned bigint,
    total bigint,
    volunteer bigint
);


--
-- TOC entry 259 (class 1259 OID 31298)
-- Name: storage; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.storage (
    id bigint NOT NULL,
    storage character varying(50)
);


--
-- TOC entry 260 (class 1259 OID 31301)
-- Name: storage_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.storage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3324 (class 0 OID 0)
-- Dependencies: 260
-- Name: storage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.storage_id_seq OWNED BY public.storage.id;


--
-- TOC entry 261 (class 1259 OID 31303)
-- Name: stype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stype (
    id bigint NOT NULL,
    stype character varying(5),
    description character varying,
    amount numeric(12,2) DEFAULT 0,
    overdue numeric(12,2) DEFAULT 0
);


--
-- TOC entry 262 (class 1259 OID 31311)
-- Name: sub_category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_category (
    id bigint DEFAULT nextval('public.category_id_seq'::regclass) NOT NULL,
    sub_category character varying(3) NOT NULL,
    description text
);


--
-- TOC entry 263 (class 1259 OID 31318)
-- Name: sub_category_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 264 (class 1259 OID 31320)
-- Name: suburb; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.suburb (
    id bigint NOT NULL,
    suburb character varying
);


--
-- TOC entry 265 (class 1259 OID 31326)
-- Name: supplier_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 266 (class 1259 OID 31328)
-- Name: supplier; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.supplier (
    id bigint DEFAULT nextval('public.supplier_id_seq'::regclass) NOT NULL,
    supplier text,
    address text,
    phone text,
    fax text,
    email text,
    contact text,
    discount numeric(12,1)
);


--
-- TOC entry 267 (class 1259 OID 31335)
-- Name: template; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.template (
    id bigint NOT NULL,
    datetime time with time zone DEFAULT now(),
    subject character varying(100),
    message text,
    type_template character varying,
    template_email character varying(50),
    delivery_type character varying(15)
);


--
-- TOC entry 268 (class 1259 OID 31342)
-- Name: template_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3325 (class 0 OID 0)
-- Dependencies: 268
-- Name: template_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.template_id_seq OWNED BY public.template.id;


--
-- TOC entry 269 (class 1259 OID 31344)
-- Name: toy_holds; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.toy_holds (
    approved character varying,
    borid bigint,
    date_start date,
    date_end date,
    created timestamp without time zone,
    id bigint DEFAULT nextval('public.holds_id_seq'::regclass) NOT NULL,
    idcat character varying,
    status character varying(10),
    paid character varying,
    notify_date date,
    transid bigint,
    reminder_date date
);


--
-- TOC entry 270 (class 1259 OID 31351)
-- Name: toy_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.toy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 271 (class 1259 OID 31353)
-- Name: toys; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.toys (
    counter bigint DEFAULT nextval('public.toy_id_seq'::regclass) NOT NULL,
    id bigint NOT NULL,
    category character varying(3) NOT NULL,
    idcat character varying(50),
    reservecode character varying(50),
    toyname character varying(150),
    rent numeric(10,2),
    age character varying(30),
    datestocktake date,
    withdrawn boolean,
    withdrawndate date,
    manufacturer character varying(50),
    discountcost numeric(12,2),
    cost numeric(12,2),
    supplier character varying(50),
    desc1 character varying(2000),
    desc2 character varying(2000),
    comments character varying(1000),
    status boolean,
    returndateperiod integer DEFAULT 14,
    lockreason character varying(50),
    returndate timestamp with time zone,
    no_pieces integer DEFAULT 0,
    date_purchase date,
    sponsorname character varying(200),
    warnings character varying(1000),
    printbarcode boolean,
    user1 character varying(200),
    alert character varying(2000),
    toy_status character varying(20),
    stype character varying(20),
    units integer DEFAULT 1,
    borrower character varying(200),
    withdrawnreason text,
    reservedfor text,
    missingpieces text,
    reserved boolean,
    lockdate date,
    created timestamp with time zone DEFAULT now() NOT NULL,
    modified timestamp with time zone DEFAULT now(),
    storage character varying,
    helmet boolean,
    copy integer DEFAULT 1,
    location character varying(20),
    stocktake_status character(20),
    packaging_cost money,
    process_time numeric(12,2),
    loan_type character varying(20),
    freight numeric(10,2) DEFAULT 0,
    color character varying,
    changedby character varying(20),
    history character varying,
    link character varying(100),
    twitter character varying(100),
    sub_category character varying(10),
    count_loans bigint,
    no_pic character varying(3),
    block_image character varying(10)
);


--
-- TOC entry 272 (class 1259 OID 31367)
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 273 (class 1259 OID 31369)
-- Name: transaction; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaction (
    created timestamp with time zone DEFAULT now(),
    id bigint DEFAULT nextval('public.transaction_id_seq'::regclass) NOT NULL,
    idcat character varying(50) NOT NULL,
    borid bigint NOT NULL,
    due date,
    return date,
    borname character varying(100),
    item character varying(100),
    location character varying(50),
    units integer,
    timetrans time without time zone,
    date_loan date DEFAULT (now())::date NOT NULL,
    phone text,
    email_status date,
    contact_status bigint,
    session character varying(3),
    group_trans character varying(50),
    returnby character varying(20),
    loanby character varying(20),
    renew integer
);


--
-- TOC entry 274 (class 1259 OID 31378)
-- Name: typeevent_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.typeevent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 275 (class 1259 OID 31380)
-- Name: typeevent; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.typeevent (
    id bigint DEFAULT nextval('public.typeevent_id_seq'::regclass) NOT NULL,
    type text,
    nohours bigint,
    description character varying(100),
    amount double precision
);


--
-- TOC entry 276 (class 1259 OID 31387)
-- Name: typepart_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.typepart_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 277 (class 1259 OID 31389)
-- Name: typepart; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.typepart (
    id bigint DEFAULT nextval('public.typepart_id_seq'::regclass) NOT NULL,
    typepart text,
    description character varying(100),
    picture character varying
);


--
-- TOC entry 278 (class 1259 OID 31396)
-- Name: typepayment_id; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.typepayment_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 279 (class 1259 OID 31398)
-- Name: typepayment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.typepayment (
    id bigint DEFAULT nextval('public.typepayment_id'::regclass) NOT NULL,
    typepayment character varying
);


--
-- TOC entry 280 (class 1259 OID 31405)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    username character varying(25),
    password character varying(32),
    id bigint DEFAULT nextval('public.id_seq_users'::regclass) NOT NULL,
    login_type character varying(15) DEFAULT 'member'::bpchar NOT NULL,
    subdomain character varying(25),
    location character varying(20),
    libraryid bigint,
    lock boolean DEFAULT false NOT NULL,
    email character varying,
    mobile character varying(30),
    created date DEFAULT now(),
    level character varying
);


--
-- TOC entry 281 (class 1259 OID 31415)
-- Name: volunteer_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.volunteer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 282 (class 1259 OID 31417)
-- Name: volunteer; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.volunteer (
    id bigint DEFAULT nextval('public.volunteer_id_seq'::regclass) NOT NULL,
    volunteer character varying NOT NULL
);


--
-- TOC entry 283 (class 1259 OID 31424)
-- Name: warning_id; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.warning_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 284 (class 1259 OID 31426)
-- Name: warnings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.warnings (
    warning character varying(10),
    description character varying(100),
    id bigint DEFAULT nextval('public.warning_id'::regclass) NOT NULL
);


--
-- TOC entry 3039 (class 2604 OID 31430)
-- Name: storage id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.storage ALTER COLUMN id SET DEFAULT nextval('public.storage_id_seq'::regclass);


--
-- TOC entry 3045 (class 2604 OID 31431)
-- Name: template id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.template ALTER COLUMN id SET DEFAULT nextval('public.template_id_seq'::regclass);


--
-- TOC entry 3075 (class 2606 OID 31566)
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (category);


--
-- TOC entry 3077 (class 2606 OID 31568)
-- Name: children child_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.children
    ADD CONSTRAINT child_pkey PRIMARY KEY (childid);


--
-- TOC entry 3082 (class 2606 OID 31570)
-- Name: city city_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);


--
-- TOC entry 3071 (class 2606 OID 31572)
-- Name: borwrs company_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.borwrs
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);


--
-- TOC entry 3084 (class 2606 OID 31574)
-- Name: condition condition_condition_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.condition
    ADD CONSTRAINT condition_condition_key UNIQUE (condition);


--
-- TOC entry 3087 (class 2606 OID 31576)
-- Name: condition condition_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.condition
    ADD CONSTRAINT condition_pkey PRIMARY KEY (id);


--
-- TOC entry 3089 (class 2606 OID 31578)
-- Name: contact_log contact_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_log
    ADD CONSTRAINT contact_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3092 (class 2606 OID 31580)
-- Name: discovery discovery_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.discovery
    ADD CONSTRAINT discovery_pkey PRIMARY KEY (id);


--
-- TOC entry 3094 (class 2606 OID 31582)
-- Name: event event_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);


--
-- TOC entry 3098 (class 2606 OID 31584)
-- Name: files files_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.files
    ADD CONSTRAINT files_pk PRIMARY KEY (id);


--
-- TOC entry 3100 (class 2606 OID 31586)
-- Name: gift_cards gift_cards_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gift_cards
    ADD CONSTRAINT gift_cards_id PRIMARY KEY (id);


--
-- TOC entry 3165 (class 2606 OID 31588)
-- Name: toy_holds holds_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.toy_holds
    ADD CONSTRAINT holds_id PRIMARY KEY (id);


--
-- TOC entry 3168 (class 2606 OID 31590)
-- Name: toys id_cat; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.toys
    ADD CONSTRAINT id_cat PRIMARY KEY (category, id);


--
-- TOC entry 3184 (class 2606 OID 31592)
-- Name: users id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT id_key PRIMARY KEY (id);


--
-- TOC entry 3119 (class 2606 OID 31594)
-- Name: nationality id_nation; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nationality
    ADD CONSTRAINT id_nation PRIMARY KEY (id);


--
-- TOC entry 3102 (class 2606 OID 31596)
-- Name: homepage id_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.homepage
    ADD CONSTRAINT id_pk PRIMARY KEY (id);


--
-- TOC entry 3182 (class 2606 OID 31598)
-- Name: typepayment id_type; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.typepayment
    ADD CONSTRAINT id_type PRIMARY KEY (id);


--
-- TOC entry 3170 (class 2606 OID 31600)
-- Name: toys idcat; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.toys
    ADD CONSTRAINT idcat UNIQUE (idcat);


--
-- TOC entry 3104 (class 2606 OID 31602)
-- Name: journal journal_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.journal
    ADD CONSTRAINT journal_pkey PRIMARY KEY (id);


--
-- TOC entry 3106 (class 2606 OID 31604)
-- Name: loan_restrictions loan_restrictions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan_restrictions
    ADD CONSTRAINT loan_restrictions_pkey PRIMARY KEY (id);


--
-- TOC entry 3108 (class 2606 OID 31606)
-- Name: location location_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_id PRIMARY KEY (id);


--
-- TOC entry 3110 (class 2606 OID 31608)
-- Name: log log_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log
    ADD CONSTRAINT log_id PRIMARY KEY (id);


--
-- TOC entry 3112 (class 2606 OID 31610)
-- Name: manufacturer manufacturer_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.manufacturer
    ADD CONSTRAINT manufacturer_pkey PRIMARY KEY (id);


--
-- TOC entry 3148 (class 2606 OID 31612)
-- Name: stats member_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stats
    ADD CONSTRAINT member_stats_pkey PRIMARY KEY (id);


--
-- TOC entry 3115 (class 2606 OID 31614)
-- Name: membertype membertype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.membertype
    ADD CONSTRAINT membertype_pkey PRIMARY KEY (id);


--
-- TOC entry 3117 (class 2606 OID 31616)
-- Name: membertype memtype; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.membertype
    ADD CONSTRAINT memtype UNIQUE (membertype);


--
-- TOC entry 3121 (class 2606 OID 31618)
-- Name: notifications notifications_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pk PRIMARY KEY (id);


--
-- TOC entry 3068 (class 2606 OID 31620)
-- Name: age order_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.age
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);


--
-- TOC entry 3125 (class 2606 OID 31622)
-- Name: overdue overdue_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.overdue
    ADD CONSTRAINT overdue_id PRIMARY KEY (id);


--
-- TOC entry 3128 (class 2606 OID 31624)
-- Name: parts parts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parts
    ADD CONSTRAINT parts_pkey PRIMARY KEY (id);


--
-- TOC entry 3130 (class 2606 OID 31626)
-- Name: partypack party_pack_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.partypack
    ADD CONSTRAINT party_pack_id PRIMARY KEY (id);


--
-- TOC entry 3132 (class 2606 OID 31628)
-- Name: paymentoptions paymentoptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.paymentoptions
    ADD CONSTRAINT paymentoptions_pkey PRIMARY KEY (id);


--
-- TOC entry 3134 (class 2606 OID 31630)
-- Name: postcode postcode_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.postcode
    ADD CONSTRAINT postcode_pkey PRIMARY KEY (id);


--
-- TOC entry 3136 (class 2606 OID 31632)
-- Name: reports reports_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT reports_id PRIMARY KEY (id);


--
-- TOC entry 3138 (class 2606 OID 31634)
-- Name: reservations reserve_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reserve_id PRIMARY KEY (id);


--
-- TOC entry 3140 (class 2606 OID 31636)
-- Name: reserve_toy reserve_toy_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reserve_toy
    ADD CONSTRAINT reserve_toy_id PRIMARY KEY (id);


--
-- TOC entry 3142 (class 2606 OID 31638)
-- Name: rostertypes rostertypes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rostertypes
    ADD CONSTRAINT rostertypes_pkey PRIMARY KEY (id);


--
-- TOC entry 3144 (class 2606 OID 31640)
-- Name: selectbox selectbox_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.selectbox
    ADD CONSTRAINT selectbox_pkey PRIMARY KEY (id);


--
-- TOC entry 3146 (class 2606 OID 31642)
-- Name: settings setting_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT setting_pk PRIMARY KEY (setting_name);


--
-- TOC entry 3151 (class 2606 OID 31644)
-- Name: storage storage_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.storage
    ADD CONSTRAINT storage_pkey PRIMARY KEY (id);


--
-- TOC entry 3153 (class 2606 OID 31646)
-- Name: storage storage_storage_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.storage
    ADD CONSTRAINT storage_storage_key UNIQUE (storage);


--
-- TOC entry 3155 (class 2606 OID 31648)
-- Name: stype stype_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stype
    ADD CONSTRAINT stype_id PRIMARY KEY (id);


--
-- TOC entry 3157 (class 2606 OID 31650)
-- Name: sub_category sub_category_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_category
    ADD CONSTRAINT sub_category_pkey PRIMARY KEY (sub_category);


--
-- TOC entry 3159 (class 2606 OID 31652)
-- Name: suburb suburb_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.suburb
    ADD CONSTRAINT suburb_id PRIMARY KEY (id);


--
-- TOC entry 3161 (class 2606 OID 31654)
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (id);


--
-- TOC entry 3163 (class 2606 OID 31656)
-- Name: template template_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.template
    ADD CONSTRAINT template_id PRIMARY KEY (id);


--
-- TOC entry 3174 (class 2606 OID 31658)
-- Name: transaction trans_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT trans_pk PRIMARY KEY (date_loan, borid, idcat);


--
-- TOC entry 3123 (class 2606 OID 31660)
-- Name: notifications type_name; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT type_name UNIQUE (notification_type, notification_name);


--
-- TOC entry 3176 (class 2606 OID 31662)
-- Name: typeevent typeevent_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.typeevent
    ADD CONSTRAINT typeevent_pkey PRIMARY KEY (id);


--
-- TOC entry 3178 (class 2606 OID 31664)
-- Name: typepart typepart_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.typepart
    ADD CONSTRAINT typepart_pkey PRIMARY KEY (id);


--
-- TOC entry 3180 (class 2606 OID 31666)
-- Name: typepart typepart_typepart_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.typepart
    ADD CONSTRAINT typepart_typepart_key UNIQUE (typepart);


--
-- TOC entry 3186 (class 2606 OID 31668)
-- Name: users username; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT username UNIQUE (username);


--
-- TOC entry 3188 (class 2606 OID 31670)
-- Name: volunteer volunteer_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.volunteer
    ADD CONSTRAINT volunteer_pkey PRIMARY KEY (id);


--
-- TOC entry 3190 (class 2606 OID 31672)
-- Name: warnings warnings_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.warnings
    ADD CONSTRAINT warnings_id PRIMARY KEY (id);


--
-- TOC entry 3069 (class 1259 OID 31673)
-- Name: bor_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bor_name_index ON public.borwrs USING btree (lower((surname)::text));


--
-- TOC entry 3073 (class 1259 OID 31674)
-- Name: category_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX category_index ON public.category USING btree (upper((category)::text), upper((category)::text));


--
-- TOC entry 3080 (class 1259 OID 31675)
-- Name: city_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX city_index ON public.city USING btree (upper((city)::text), upper((city)::text));


--
-- TOC entry 3085 (class 1259 OID 31676)
-- Name: condition_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX condition_index ON public.condition USING btree (upper((condition)::text), upper((condition)::text));


--
-- TOC entry 3090 (class 1259 OID 31677)
-- Name: discovery_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discovery_index ON public.discovery USING btree (upper((discovery)::text), upper((discovery)::text));


--
-- TOC entry 3095 (class 1259 OID 31678)
-- Name: fki_borwrs; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_borwrs ON public.event USING btree (memberid);


--
-- TOC entry 3078 (class 1259 OID 31679)
-- Name: fki_child; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_child ON public.children USING btree (id);


--
-- TOC entry 3126 (class 1259 OID 31680)
-- Name: fki_idcat; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_idcat ON public.parts USING btree (itemno);


--
-- TOC entry 3072 (class 1259 OID 31681)
-- Name: fki_memtype; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_memtype ON public.borwrs USING btree (membertype);


--
-- TOC entry 3166 (class 1259 OID 31682)
-- Name: id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX id ON public.toys USING btree (id);


--
-- TOC entry 3171 (class 1259 OID 31683)
-- Name: id_trans; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX id_trans ON public.transaction USING btree (id);


--
-- TOC entry 3113 (class 1259 OID 31684)
-- Name: membertype_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX membertype_index ON public.membertype USING btree (upper((membertype)::text), upper((membertype)::text));


--
-- TOC entry 3079 (class 1259 OID 31685)
-- Name: name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_index ON public.children USING btree (lower((child_name)::text), lower((child_name)::text));


--
-- TOC entry 3149 (class 1259 OID 31686)
-- Name: storage_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX storage_index ON public.storage USING btree (upper((storage)::text), upper((storage)::text));


--
-- TOC entry 3172 (class 1259 OID 31687)
-- Name: trans_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX trans_index ON public.transaction USING btree (upper((idcat)::text));


--
-- TOC entry 3096 (class 1259 OID 31688)
-- Name: type_event_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX type_event_index ON public.event USING btree (upper((typeevent)::text), upper((typeevent)::text));


--
-- TOC entry 3194 (class 2606 OID 31689)
-- Name: toys category; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.toys
    ADD CONSTRAINT category FOREIGN KEY (category) REFERENCES public.category(category) ON DELETE RESTRICT;


--
-- TOC entry 3192 (class 2606 OID 31694)
-- Name: children child; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.children
    ADD CONSTRAINT child FOREIGN KEY (id) REFERENCES public.borwrs(id) ON DELETE RESTRICT;


--
-- TOC entry 3193 (class 2606 OID 31699)
-- Name: parts idcat; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parts
    ADD CONSTRAINT idcat FOREIGN KEY (itemno) REFERENCES public.toys(idcat) ON DELETE RESTRICT;


--
-- TOC entry 3191 (class 2606 OID 31704)
-- Name: borwrs memtype; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.borwrs
    ADD CONSTRAINT memtype FOREIGN KEY (membertype) REFERENCES public.membertype(membertype) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2018-12-28 11:10:36

--
-- PostgreSQL database dump complete
--

