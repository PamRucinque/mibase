# Mibase software for Toy Libraries.

**Mibase** is a system that manages the toy collection, members and, loan and return process for a toy library.

## About Mibase
Started in 1998 the software started as a desktop application and has been constantly developed to accommodate the needs and functionality required by a toy library.  
The development has been guided by the toy library community of Australia and New Zeeland.  
It now that will streamlines and  automates many  processes in the toy library saving valuable volunteer time.

## Running Modes
Mibase can run in 2 modes: 
* **Single user mode**: One Mibase server is dedicated to one library.
* **Multi user mode**: Many libraries share the same Mibase server. Multi user mode is ideal for a community of toy libraries to share a server and the running and administration costs.

This software went open source in 2018 to allow continued development, all with the aim to improve the way we work in Toy Libraries.

## Reports
Note that reports, bag labels and print outs are generated as pdf documents by a separate application, [mibase_reports](https://bitbucket.org/mibase/mibasereportrunner/src/master/).  This is a java web application that you would run in a web application container like tomcat.

## License and Warranty
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details <http://www.gnu.org/licenses/>.


## Development

### Project structure:
* `doc`:contains some core documentation for installation of this application. Detailed user documentation will be contained in the application wiki pages.
* `admin`: contains the admin web application php files. 
* `database`: holds the database schema and a demo database dump file. (with sample data) and a template database that has empty tables with limited sample data. This folder also contains the database version change sql files.
* `scripts`: contains cron job scripts used for example to send overdue reminder emails.

### Applications
* Admin application: Used in the toy library for toy management, loans and returns over the counter.
* Member application: Used by members to browse and reserve toys, put themselves down for duty and check what they have on loan.

### Development on Netbeans and Windows
Follow [these instructions](doc/Setup%20MiBaseOpen%20Development%20Environment%207Jul18.pdf) for development on an Windows environment supported by Netbeans.

### Development with Docker and batect (Experimental)

This setup is for running PHP and Apache within a container.
It uses [batect](https://batect.dev/) to facilitate local development

#### Pre-requisites:

* [JVM](https://java.com/en/download/) (version 8 or newer)
* [Docker](https://docs.docker.com/get-docker/) (Version 18.03.1 or newer)
* [Postgres](https://www.postgresql.org/download/) (Currently tested on version 12.3, The login screen shows a warning but the app works fine) - On Mac: `brew install postgres`

* On Linux and macOS: Bash and curl
* On Windows: Windows 10 / Windows Server 2016 or later

#### Setup the demo database using the demo backup

In the command line: 

1. Log into Postgres: i.e `psql postgres`
1. Create a demo DB user: `CREATE USER demo WITH PASSWORD 'choose-your-own-password';`
1. Create a demo database: `CREATE DATABASE demo;`
1. Exit Postgres
1. Restore the demo backup from the `mibase` project folder: `pg_restore -d demo -U demo ./database/demo.backup`

#### Allow connections to Postgres from inside the container

See [this page](https://stackoverflow.com/questions/38466190/cant-connect-to-postgresql-on-port-5432) for more details.

1. Modify `postgresql.conf` - _On Mac: `/usr/local/var/postgres/postgresql.conf`_

   Set `listen_addresses = '*'` in `postgresql.conf` to allow Postgres to listen on any address (not only `localhost` which is the defaul). 

1. Get your computer IP - _On Mac: `ifconfg | grep inet`_

1. Modify `pg_hba.conf` - _On Mac: `/usr/local/var/postgres/pg_hba.conf`_
   Add the following line in the `# IPv4 local connections:` section to allow connections from other computers (from the container's ip).

   `host    all             all             <your host ip>/24       md5`

1. Restart Postgres - _On Mac: `brew services restart postgres`_

#### Configure the app to run
Create a `config.php` inside `mibase/admin/` by duplicating the sample config file provided: `mibase/admin/config_sample.php`.

Uncomment and set these settings: 

``` php
$dbhost = '<your local ip>';
$app_root_location = '/admin';
$dbname = 'demo';
$dbuser = 'demo';
$dbpasswd = '<whatever password you chose>';
```

Comment this setting out:

``` php
$db_postgres_password = 'PASSWORD';
``` 

_Note: The credentials are currently in plain text. `config.php` is included in the .gitignore to prevent checking in the credentials to the repository._

#### Run the application

1. Go to `mibase` root folder
2. Run `./batect app` to run Apache
3. Go to http://localhost/admin/login.php
4. Login to Mibase (Credentials User:`admin` Password:`admin`)

![alt text](doc/Login.png "Login Page")
![alt text](doc/Toys.png "Toys Search")


