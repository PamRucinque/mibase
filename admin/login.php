<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="css/menu.css" />
        <!--   <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                  
           <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> -->
        <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" type="text/css" href="css/login.css">
    </head>
    <?php
    include( dirname(__FILE__) . '/config.php');
    include( dirname(__FILE__) . '/version.php');
    include( dirname(__FILE__) . '/functions.php');

    //if the shared server is not defined then set it to be a single instance server
    if (!isset($shared_server)) {
        $shared_server = false;
    }

    //if the $db_postgres_password is defined then set this user and password for the database and toybase user and password
    if (isset($db_postgres_password)) {
        if ($shared_server) {
            $toybasedbuser = 'postgres';
            $toybasedbpasswd = $db_postgres_password;
            if (!isset($toybasedbname)) {
                $toybasedbname = 'toybase';
            }
            // $dbuser = 'postgres';
            // $dbpasswd = $db_postgres_password;
        } else {
            if (!isset($dbname)) {
                $dbname = 'mibase';
            }
            $dbuser = 'postgres';
            $dbpasswd = $db_postgres_password;
        }
    }

//get the post variables if the user has clicked the login putton
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);

    if (isset($username)) {
        //limit length
        if ((strlen($username)) > 25) {
            echo 'Login error: Username exceeds minimum length of 25 charaters';
            exit;
        }
        //limit allowed charaters   
        if (preg_match('/^[\w@]*$/', $username) == 0) {
            echo 'Login error: Username can only contain word charaters and numbers and -_ charaters';
            exit;
        }
    }

    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    if (isset($password)) {
        //limit length
        if ((strlen($password)) > 31) {
            echo 'Login Error: Password exceeds minimum length of 30 charaters';
            exit;
        }
        //limit allowed charaters   
        if (preg_match('/^[\w@]*$/', $password) == 0) {
            echo 'Login error: Password can only contain word charaters and numbers and -_ charaters';
            exit;
        }
    }


    //if we are a shared server get the library code from the requested url, do some checks on it and set the database access variables.
    if ($shared_server) {
        $host = filter_input(INPUT_SERVER, "SERVER_NAME", FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
        //limit length
        if ((strlen($host)) > 31) {
            exit_script('Login Error: Server Name exceeds minimum length of 30 charaters');
        }
        //limit allowed charaters  
        if (preg_match('/^[\w\.\-]*$/', $host) == 0) {
            exit_script('Host Name Error: Host name can only contain word charaters and numbers _ - and . charaters');
        }

        if (ends_with($host, $shared_domain)) {
            $library_code = get_host_name($host, $shared_domain);
            $dbuser = $library_code;
            $dbname = $library_code;
            $dbpasswd = getLibraryPostgresPassword($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd, $library_code);
        } else {
            exit_script('requested host is not on this server');
        }
    } else {
        $host = filter_input(INPUT_SERVER, "SERVER_NAME", FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    }

    $note = '';

    if (isset($username)) {
//the user has attempted to log in
        if ($shared_server) {

            if (!session_id()) {
                session_start();
            }

            //get the library code from the host name requested by the client taking of the $shared_domain of this server
            //eg demo.mibase.com.au  $shared_domain mibase.com.au would give demo as the library code.
            $_SESSION['library_code'] = get_host_name($host, $shared_domain);
            //$str = 'Library code: ' . $_SESSION['library_code'];
            //$str .= ' Host ' . $host;
            //$str .= ' Domain ' . $shared_domain;
            $library_postgres_password = getLibraryPostgresPassword($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd, $_SESSION['library_code']);
            //echo $library_postgres_password;
            //validate this user in this library database
            $note = mibase_validate_login_shared($_SESSION['library_code'], $library_postgres_password, $username, $password);

            if ($note == 'ok') {
                //the login is ok set the session variable

                $_SESSION['shared_server'] = $shared_server;
                $_SESSION['password'] = $password;
                $_SESSION['username'] = $username;
                $_SESSION['app'] = 'admin';
                $connect_str = "host='" . $dbhost . "' port=" . $dbport . " dbname='" . $_SESSION['library_code'] . "' user='" . $_SESSION['library_code'] . "' password='" . $library_postgres_password . "'";
                $_SESSION['connect_str'] = $connect_str;
                //echo $connect_str;
                //$connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $_SESSION['library_code'];
                $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $_SESSION['library_code'];

                $_SESSION['connect_pdo'] = $connect_pdo;
                $_SESSION['dbname'] = $_SESSION['library_code'];
                $_SESSION['dbuser'] = $_SESSION['library_code'];
                $_SESSION['dbpasswd'] = $library_postgres_password;
                $user_details = getLoginType($username, $password, $connect_pdo, $_SESSION['library_code'], $library_postgres_password);
                //get the level of the user either admin or volunteer
                $_SESSION['level'] = $user_details['level'];
                $_SESSION['location'] = $user_details['location'];

                //save the library code in the session for later use
                $_SESSION['library_code'] = $_SESSION['library_code'];

                $_SESSION['web_server_protocol'] = $web_server_protocol;

                //set the host name for this session
                $_SESSION['host'] = $_SESSION['library_code'] . '.' . $shared_domain;

                $_SESSION['app_root_location'] = $app_root_location;
                $_SESSION['news_location'] = $news_location;
                $_SESSION['logos_location'] = $logos_location;
                $_SESSION['toy_images_location'] = $toy_images_location;
                $_SESSION['report_server_url'] = $report_server_url;
                $_SESSION['home_location'] = $home_location;
                $_SESSION['web_root_folder'] = $web_root_folder;

                $_SESSION['settings'] = fillSettingSessionVariables($connect_str);

                //check if we have set the time zone, if not use the default time zone
                if (!isValidTimezone($_SESSION['settings']['timezone'])) {
                    $_SESSION['settings']['timezone'] = date_default_timezone_get();
                }

                //fill other session variables that are need by the application
                fillOtherSessionVariables();

                $_SESSION['mibase_server'] = $mibase_server;

                //and forward the user to the admin home pages.
                $headerStr = 'location:' . $app_root_location . '/home/index.php';
                header($headerStr);
                exit();
            }
        } else {
            //its a single server
            $note = mibase_validate_login_single($username, $password);

            if ($note == 'ok') {
                //the login is ok set the session variable

                if (!session_id()) {
                    session_start();
                }
                $_SESSION['shared_server'] = $shared_server;
                $_SESSION['username'] = $username;
                $_SESSION['password'] = $password;
                $_SESSION['app'] = 'admin';
                $connect_str = "host='" . $dbhost . "' port=" . $dbport . " dbname='" . $dbname . "' user='" . $dbuser . "' password='" . $dbpasswd . "'";
                $_SESSION['connect_str'] = $connect_str;

                $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbname;
                $_SESSION['connect_pdo'] = $connect_pdo;
                $_SESSION['dbname'] = $dbname;
                $_SESSION['dbuser'] = $dbuser;
                $_SESSION['dbpasswd'] = $dbpasswd;

                $user_details = getLoginType($username, $password, $connect_pdo, $dbuser, $dbpasswd);
                //get the level of the user either admin or volunteer
                $_SESSION['level'] = $user_details['level'];
                $_SESSION['location'] = $user_details['location'];

                //save the library code in the session for later use
                //the libray_code is a zero length string for a single library system
                $_SESSION['library_code'] = '';

                $_SESSION['web_server_protocol'] = $web_server_protocol;

                //set the host name for this session
                $_SESSION['host'] = $host;

                $_SESSION['app_root_location'] = $app_root_location;
                $_SESSION['news_location'] = $news_location;
                $_SESSION['logos_location'] = $logos_location;
                $_SESSION['toy_images_location'] = $toy_images_location;
                $_SESSION['report_server_url'] = $report_server_url;
                $_SESSION['home_location'] = $home_location;

                $_SESSION['web_root_folder'] = $web_root_folder;

                $_SESSION['settings'] = fillSettingSessionVariables($connect_str);

                //check if we have set the time zone, if not use the default time zone
                if (!isValidTimezone($_SESSION['settings']['timezone'])) {
                    $_SESSION['settings']['timezone'] = date_default_timezone_get();
                }

                //fill other session variables that are need by the application
                fillOtherSessionVariables();

                $_SESSION['mibase_server'] = $mibase_server;

                //and forward the user to the admin home pages.
                $headerStr = 'location:' . $app_root_location . '/home/index.php';
                header($headerStr);
                exit();
            }
        }
    }

    $db_state_note = get_state($dbhost, $dbport, $dbname, $dbuser, $dbpasswd, $code_version_minor);
    ?>
    <style>
        /* background image for login */
            .backgroundimage {
                background-image: url('css/images/Background-image-login-5px.png');
                background-repeat: repeat;
                background-size: 180%, 100%;
                
                animation: slide 60s linear infinite;
                animation-delay: 0s;
            }

            @keyframes slide {
                from { background-position: 0 0; }
                to { background-position: -2200px 0; }
            }
    </style>
    <body class="backgroundimage">
    <div class="container">
        <div class="row">
            <div class="card card-container" style="border-radius: 20px;">
                <?php
                if ($shared_server) {
                    if (file_exists($web_root_folder . '/' . $logos_location . '/' . $library_code . '/logo.jpg')) {
                        echo '<img src="' . $logos_location . '/' . $library_code . '/logo.jpg' . '" alt="Library Logo" class="profile-img-card">';
                    } else {
                        echo '<img src="logo.jpg" alt="Library Logo" class="profile-img-card">';
                    }
                } else {
                    if (file_exists($web_root_folder . '/' . $logos_location . '/logo.jpg')) {
                        echo '<img src="' . $logos_location . '/logo.jpg" alt="Library Logo" class="profile-img-card">';
                    } else {
                        echo '<img src="logo.jpg" alt="Library Logo" class="profile-img-card">';
                    }
                }

                $home = $home_location . '/index.php';
                ?>
                <h1>Mibase Administrator Login</h1>
                <form name="form1" method="post" action="login.php" class="form-signin">
                    <span id="reauth-email" class="reauth-email" style="color:red;"><?php echo $note ?></span>
                    <?php
                    if ($db_state_note != '') {
                        echo '<div style="color:red;">' . $db_state_note . '<a href="./db/index.php" class="btn btn-info" role="button">Database Maintenance</a></div>';
                    }
                    ?>
                    <input name="username" type="text" id="username" class="form-control" placeholder="username">
                    <input name="password" type="password" id="password" class="form-control" placeholder="Password">
                    <div class="col-md-6">
                        <a href="<?php echo $home; ?>" class="btn btn-lg btn-info btn-block btn-signin">Home</a>
                    </div>
                    <div class="col-md-6">
                        <input type="submit" name="Submit" value="Login" class="btn btn-lg btn-primary btn-block btn-signin">  
                    </div>           
                </form><!-- /form -->   
            </div><!-- /card-container -->
        </div><!-- /row -->
    </div><!-- /container -->
    </body>




    <?php

    /**
     * 
     * @param type $fqn
     * @param type $shared_domain
     * @return type
     */
    function ends_with($fqn, $shared_domain) {
        return ( substr($fqn, strlen($fqn) - strlen($shared_domain)) == $shared_domain );
    }

    /**
     * gets the host name give the hosts fqn and the $shared_domain
     * eg $fqn = demo.mibase.com.au $shared_domain = mibase.com.au
     * @param type $fqn
     * @param type $shared_domain  
     * @return type string
     */

    /**
     * simple finction to output an error page and exit
     * @param $message the message to display
     */
    function exit_script($message) {
        ?>
        <!DOCTYPE html>
        <html>
            <head>
                <title>Mibase Error</title>
                <meta charset="windows-1250">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="stylesheet" type="text/css" href="css/view.css">
            </head>
            <body>
            <center>
                <div><h2><font color="blue">Mibase Error: <?php echo $message; ?></font></h2>
                </div>
            </center>
        </body>
    </html>
    <?php
    exit();
}

/**
 * 
 * @param type $username
 * @param type $password
 * @return string
 */
function mibase_validate_login_single($username, $password) {
    global $dbhost, $dbport, $dbname, $dbuser, $dbpasswd;
//now check the username and password in the users table
    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbname;
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $query_login = "select username, password FROM users WHERE username = ? AND password = ? AND login_type = 'admin' ;";
    $sth = $pdo->prepare($query_login);
    $array = array($username, $password);
    $sth->execute($array);

    // $result = $sth->fetchAll();
    $numrows = $sth->rowCount();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        return "An  error occurred checking the login: " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
    }
// 0    SQLSTATE error code (a five characters alphanumeric identifier defined in the ANSI SQL standard).   
// 1    Driver-specific error code.
// 2    Driver-specific error message.

    if ($numrows > 0) {
//the user is valid
        return 'ok';
    } else {
        return "Invalid User and Password";
    }
}

/**
 * 
 * @param string $_SESSION['library_code']
 * @param string $username
 * @param string $password
 * @return string
 */
function mibase_validate_login_shared($library_code, $library_postgres_password, $username, $password) {
    global $dbhost, $dbport;
    $pattern = '/^[\w@]*$/';
    if (preg_match($pattern, $username) == 0) {
        return 'Login error: Username can only contain word charaters and numbers and -_ charaters';
    }
    if (preg_match($pattern, $password) == 0) {
        return 'Login error: Password can only contain word charaters and numbers and -_ charaters';
    }
    if ((strlen($username)) > 25) {
        return 'Login error: Username exceeds minimum length of 25 charaters';
    }
    if ((strlen($password)) > 31) {
        return 'Login Error: Password exceeds minimum length of 30 charaters';
    }

    if ($library_postgres_password) {
//the user is trying to log into a valid toy library
//now check the username and password in the users table of that toylibrary
        $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $library_code;
        $pdo = new PDO($connect_pdo, $library_code, $library_postgres_password);
        $query_login = "SELECT username, password FROM users WHERE username = ? AND password = ? AND login_type = 'admin' ;";
        $sth = $pdo->prepare($query_login);
        $array = array($username, $password);
        $sth->execute($array);
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            return "An  error occurred checking the login: " . $stherr[2];
        }

        if ($numrows > 0) {
//the user is valid
            return 'ok';
        }
    } else {
        return 'Login Error: This library not on this server: ' . $library_code;
    }
}

/**
 * gets the postgres password for a librarys database in a shared system
 * the database user is the library_code
 * the database name is the library_code
 * the library databases are on the same server, port as the toybase database
 * @param type $_SESSION['library_code']
 * @return type
 */
/**
 * fills all the settings as session variables
 * @param string $connect_str
 */

/**
 * fills  other session variables
 * @param string $connect_str
 */
function mibase_log_login($username, $login_type) {

    $ip = filter_input(INPUT_SERVER, "REMOTE_ADDR", FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    // $ip = $_SERVER["REMOTE_ADDR"];


    $pdo = new PDO($connect_pdo_toybase, $dbuser_toybase, $dbpasswd_toybase);
//$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $query_toybase = "INSERT INTO log (ip, subdomain, username, password, logintype, status, email) VALUES (?,?,?,?,?,?,?)";


    $sth = $pdo->prepare($query_toybase);
    $array = array($ip, $library_code_url, $username, $password, $login_type, $myemail, $status);
//execute the preparedstament
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $return = "An INSERT query error occurred.\n";
        $return .= 'Error ' . $stherr[0] . '<br>';
        $return .= 'Error ' . $stherr[1] . '<br>';
        $return .= 'Error ' . $stherr[2] . '<br>';
    } else {
        $return = '';
    }
    return $return;
}

/**
 * Check if a time zone is valid
 *
 * @param string $timezone
 * @return true if it is valid false if not or it is null
 */
function isValidTimezone($timezone) {
    if ($timezone == NULL) {
        return false;
    }
    return in_array($timezone, timezone_identifiers_list());
}

function get_state($dbhost, $dbport, $dbname, $dbuser, $dbpasswd, $code_version_minor) {
    try {
        $minor = -1;
        $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbname;
        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sql = "SELECT minor FROM version WHERE item = 'db' ;";
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);
        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            return 'Database Error:' . $stherr[2] . '<br>Database maintenance required<br>';
        }

        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = $result[$ri];
            $minor = $row['minor'];
        }

        if ($minor == $code_version_minor) {
            return '';
        } else {
            return 'Database version does not match code version<br>Database maintenance required<br>';
        }
    } catch (Exception $e) {
        return 'Exception getting DB State<br>Database maintenance required<br>';
    }
}
