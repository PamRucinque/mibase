
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['app_root_location']; ?>/css/view.css">
<link rel="stylesheet" type="text/css" media="print" href="<?php echo $_SESSION['app_root_location']; ?>/css/print.css">
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['app_root_location']; ?>/css/superfish.css">
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['app_root_location']; ?>/css/jquery-ui.min.css">

<script type="text/javascript" src="<?php echo $_SESSION['app_root_location']; ?>/js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<?php echo $_SESSION['app_root_location']; ?>/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $_SESSION['app_root_location']; ?>/js/ajax.js"></script>
<script type="text/javascript" src="<?php echo $_SESSION['app_root_location']; ?>/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo $_SESSION['app_root_location']; ?>/js/tooltip.js"></script>
<?php
include('head.php');
