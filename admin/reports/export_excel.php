<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');


$link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/PdfReport';
$link_export = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/ExcelReport';
$idcat = '';
if (isset($_SESSION['idcat'])) {
    $idcat = $_SESSION['idcat'];
}
$toy_to_print = "'" . $idcat . "'";
$sql_report = "select * from borwrs order by id;";
if (!isset($_SESSION['start'])) {
    $_SESSION['start'] = $start;
}
if (!isset($_SESSION['end'])) {
    $_SESSION['end'] = $end;
}
?>
<script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "yy-mm-dd",
            showOtherMonths: true

        };
        $("#start_excel").datepicker(pickerOpts);
        $("#end_excel").datepicker(pickerOpts);
    });

</script>
<div id="form"  align="left" style="background-color: lightblue;" width="300px">
    <h2>Export To Excel</h2>

    <form id="report_id" name="report_id" method="post" action="../reports/report_runner.php" >
        Select data to Export:<br><?php include( dirname(__FILE__) . '/get_report_export_excel.php'); ?>
        <table><tr>
                <td>Select Start Day:<br>
                    <input type="text" name="start_excel" id ="start_excel" align="LEFT" value="<?php echo $_SESSION['start']; ?>" onchange='this.form.submit()'></input><br></td>
                <td>Select End Day (if date range):<br>
                    <input type="text" name="end_excel" id ="end_excel" align="LEFT" value="<?php echo $_SESSION['end']; ?>" onchange='this.form.submit()'></input><br></td>
            </tr></table>
        <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['username']; ?>">
        <input type="hidden" id="password" name="password" value="<?php echo $_SESSION['password']; ?>"> 
        <table align="top"><tr>
                <td align="right"><br><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Export to excel" /></td>
            </tr>   
        </table>

    </form>

</div>



