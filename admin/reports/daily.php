<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

$idcat = '';

$libraryname = $_SESSION['settings']['libraryname'];
$address = $_SESSION['settings']['address'];
$link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/PdfReport';
if (isset($_SESSION['idcat'])) {
    $idcat = $_SESSION['idcat'];
}

$toy_to_print = "'" . $idcat . "'";
?>
<script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "yy-mm-dd",
            showOtherMonths: true

        };
        $("#datetrans").datepicker(pickerOpts);
        $("#dateend").datepicker(pickerOpts);
    });

</script>

<div id="form"  align="left">
    <form id="reports"  method="post" action="../reports/report_runner.php">
        <table align="top"><tr>
                <td width="35%">Select Report to Print:<br><?php include( dirname(__FILE__) . '/get_report_daily.php'); ?>    </td>
                <td width="25%">Select Category:<br><?php include( dirname(__FILE__) . '/get_category.php'); ?>    </td>
                <td width="25%">Select Day to Print:<br>
                    <input type="text" name="datetrans" id ="datetrans" align="LEFT" value="<?php echo date('Y-m-d') ?>"></input><br></td>
                <td align="left"><br><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Open Report" /></td>
            </tr>   

            <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['username']; ?>">
            <input type="hidden" id="password" name="password" value="<?php echo $_SESSION['password']; ?>"> 
            <input type="hidden" id="libraryname" name="libraryname" value="<?php echo $libraryname; ?>"> 
            <input type="hidden" id="libraryaddress" name="libraryaddress" value="<?php echo $address; ?>"> 


        </table>

    </form>
</div>



