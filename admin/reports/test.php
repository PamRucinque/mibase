<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <script>
    function PrintPdf() {
        idPrint.disabled = 0;
        idPdf.Print();
    }

    function idPdf_onreadystatechange() {
        if (idPdf.readyState === 4)
            setTimeout(PrintPdf, 1000);
    }
    </script>

</head>

<body>
    <button id="idPrint" disabled=1 onclick="PrintPdf()">Print</button>
    <br>
    <object id="idPdf" onreadystatechange="idPdf_onreadystatechange()"
        width="300" height="400" type="application/pdf"
        data="loan_receipt.pdf?#view=Fit&scrollbar=0&toolbar=0&navpanes=0">
        <span>PDF plugin is not available.</span>
    </object>
</body>