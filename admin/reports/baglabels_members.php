<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');


$open_hours = $_SESSION['settings']['open_hours'];
$link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/PdfReport';
if (isset($_SESSION['borid'])) {
    $borid = $_SESSION['borid'];
}

//include( dirname(__FILE__) . '/../get_settings.php');

//echo $sql_report;
?>

<div id="form"  align="left">
    <form id="reports"  name="reports" method="post" action="../reports/report_runner.php">
        <table align="top"><tr>
                <td>Select Report to Print:<br><?php include( dirname(__FILE__) . '/get_report_members.php'); ?>    </td>
                <?php
                $borlist_str = '';
                if (isset($_SESSION['borid_list'])){
                  $borlist_str = $_SESSION['borid_list'];  
                }
                
                if ($borlist_str == '') {
                    echo '<td align="right"><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Open Report" onclick="return go()"/></td>';
                } else {
                    echo '<td align="right"><input id="saveForm" class="button1_red"  type="submit" value="Open Report" /></td>';
                }
                ?>
                <td>
                    <input type="button" id ="select_all" class="button1_yellow" onclick="test();" value="Select All"/>
                    <input type="hidden" name="borid_list" id="borid_list" value="<?php echo $borlist_str; ?>"/>
                    <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['username']; ?>"/>
                    <input type="hidden" id="password" name="password" value="<?php echo $_SESSION['password']; ?>"> 
                    <input type="hidden" id="libraryname" name="libraryname" value="<?php echo $_SESSION['settings']['libraryname']; ?>"> 
                    <input type="hidden" id="libraryaddress" name="libraryaddress" value="<?php echo $_SESSION['settings']['address']; ?>"> 
                    <input type="hidden" id="open_hours" name="open_hours" value="<?php echo $open_hours; ?>"> 

                </td>
                <td></td>
            </tr>   

        </table>

    </form>

</div>



