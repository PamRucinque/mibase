<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');

//include( dirname(__FILE__) . '/../config.php');
//the prefix if the report runner returns an error not the report
$error_prefix = "     ERROR     ";

//read all the post variables into and array after fitering
//http://php.net/manual/en/filter.filters.sanitize.php
//https://www.w3schools.com/php/filter_sanitize_string.asp

//$PostArgs = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES); // | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH
//not this will convert ' into &#39;  which breaks the catids 
//an array to hold all the parameters, we convert this to a JSON object at sending to report runner
$json_array = array();
//the extra parameters to go to the report
$parameters = array();
//add the database access details to the request
$json_array["dbName"] = $_SESSION['dbname'];
$json_array["dbUser"] = $_SESSION['dbuser'];
$json_array["dbPassword"] = $_SESSION['dbpasswd'];
$json_array["sharedServer"] = $_SESSION['shared_server'];   //eg false;
$json_array["libraryCode"] = $_SESSION['library_code'];

$parameters["webRootFolder"] = $_SESSION['web_root_folder'];   // eg 'C:/Apache24/htdocs';
//location to toy images
if ($_SESSION['shared_server']) {
    //location to toy images
    $parameters["toyImagesFolder"] = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . $_SESSION['library_code'];   // eg /toy_images/frankston
    // location to news
    $parameters["newsLocation"] = $_SESSION['news_location'] . $_SESSION['library_code'];  //eg  /news
    //location of the logos
    $parameters["logosLocation"] = $_SESSION['logos_location'] . $_SESSION['library_code']; //eg '/mibase/logs';
    //home location
    $parameters["homeLocation"] = $_SESSION['home_location'] . $_SESSION['library_code'];
    ; // eg  '/home';
} else {
    //location to toy images 
    $parameters["toyImagesFolder"] = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'];   // eg /toy_images
    // location to news
    $parameters["newsLocation"] = $_SESSION['news_location'];  //eg  /news
    //location of the logos
    $parameters["logosLocation"] = $_SESSION['logos_location']; //eg '/mibase/logs';
    //home location
    $parameters["homeLocation"] = $_SESSION['home_location']; // eg  '/home';
}

//location of the logos
$parameters["logosLocation"] = $_SESSION['logos_location']; //eg '/mibase/logs';


$parameters["homeLocation"] = $_SESSION['home_location']; // eg  '/home';
//move the post variables into the jason_array
foreach ($_POST as $keyu => $valueu) {
    if (strlen($keyu) < 100 && strlen($valueu) < 1000) {
        //echo $keyu . " > " . $valueu . "\r\n";
        //not excessive sizes
        $key = filter_var($keyu, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
        $value = filter_var($valueu, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
        if ($key === "report") {
            $json_array["report"] = $value;
        } else if ($key === "reportFormat") {
            $json_array["reportFormat"] = $value;
        } else {
            $parameters[$key] = $value;
        }
    }
}

//add the parameters to the jason array
$json_array["parameters"] = $parameters;


//$v = json_encode($json_array);
//echo $v;
//exit();
//disable the warnings that stream_socket_client may issue, eg the reportrunner is not listening
// save error reporting level
$ErrorReporting = error_reporting();
// disable warnings
error_reporting($ErrorReporting xor E_WARNING);
$client = stream_socket_client($_SESSION['report_server_url'], $errno, $errorMessage);
// restore error reporting level
error_reporting($ErrorReporting);


if (!$client) {
    echo "$errorMessage ($errno)<br />\n";
    exit();
}

fwrite($client, json_encode($json_array));

$content = stream_get_contents($client);

fclose($client);

if (strlen($content) < 200 && substr($content, 0, strlen($error_prefix)) === $error_prefix) {
    echo "<!DOCTYPE html>";
    echo "<html>";
    echo "<head>";
    echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
    echo "<title>Report</title>";
    echo "</head>";
    echo "<body>";
    echo "<div style=\"text-align:center\">";
    echo "I'm Sorry, Error Generating your report<br>";
    $errorDetail = substr($content, strlen($error_prefix));
    echo $errorDetail . "<br>";
    echo "</div>";
    echo "</body>";
    echo "</html>";
} else {
    //we have valid output
    //build a header for the respose
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Type: application/pdf");
    header("Content-Disposition: attachment; filename=\"test.pdf\"");
    header("Content-Transfer-Encoding: binary");
    echo $content;
}

