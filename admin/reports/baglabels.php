<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');



$libraryname = $_SESSION['settings']['libraryname'];

$link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/PdfReport';
$idcat = $_SESSION['idcat'];
$password = $_SESSION['password'];
$libraryname = $_SESSION['settings']['libraryname'];
$address = $_SESSION['settings']['address'];
$open_hours = $_SESSION['settings']['open_hours'];
$toy_to_print = "'" . $idcat . "'";
//include( dirname(__FILE__) . '/../../get_settings.php');
?>


<form id="reports"  method="post" action="<?php echo $link; ?>">
    <div class="row">
        <div class="col-sm-4">
            <?php include( dirname(__FILE__) . '/get_report.php'); ?>
        </div>


        <div class="col-sm-8">    
            <input id="saveForm" class="btn btn-colour-maroon"  type="submit" name="submit" value="Open Report" />

            <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['username']; ?>">
            <input type="hidden" id="password" name="password" value="<?php echo $password; ?>"> 
            <input type="hidden" id="libraryname" name="libraryname" value="<?php echo $libraryname; ?>"> 
            <input type="hidden" id="idcats_list" name="idcats_list" value="<?php echo $toy_to_print; ?>"> 
            <input type="hidden" id="libraryaddress" name="libraryaddress" value="<?php echo $address; ?>"> 
            <input type="hidden" id="open_hours" name="open_hours" value="<?php echo $open_hours; ?>"> 
        </div>
    </div>
</form>




