<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');


$link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/PdfReport';
$idcat = $_SESSION['idcat'];
$toy_to_print = "'" . $idcat . "'";
//include( dirname(__FILE__) . '/../get_settings.php');
if ($catsort == 'No') {
    $order_by = 'ORDER BY id ASC;';
} else {
    $order_by = 'ORDER BY category, id ASC;';
}
if (isset($_GET['submit'])) {
    $_SESSION['category'] = $_GET['category'];
    $_SESSION['age'] = $_GET['age'];
    $_SESSION['search'] = $_GET['search'];
    $_SESSION['select'] = $_POST['idcats_list'];
}
$search_str = '%' . $_SESSION['search'] . '%';
$age = '%' . $_SESSION['age'] . '%';
$category = '%' . $_SESSION['category'] . '%';

$where = " where (upper(toyname) LIKE upper('" . $search_str .
        "') OR upper(manufacturer) LIKE upper('" . $search_str .
        "') OR upper(storage) LIKE upper('" . $search_str .
        "') OR upper(idcat) LIKE upper('" . $search_str . "'))" .
        " AND (category LIKE '" . $category . "')" .
        " AND ((age LIKE '" . $age . "') OR (age IS NULL))" .
        " AND (toy_status = 'ACTIVE') ";
$sql_report = "select " . $export_toys . " from toys " . $where . $order_by;
$link_export = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/ExcelReport';

//echo $sql_report;
?>

<div id="form"  align="right">
    <form id="reports"  name="reports" method="post" action="../reports/report_runner.php">
        <table align="top"><tr>
                <td>Select Report to Print:<br><?php include( dirname(__FILE__) . '/get_report.php'); ?>    </td>
                <?php
                if ($_SESSION['idcat_select'] == '') {
                    echo '<td align="right"><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Open Report" onclick="return go()"/></td>';
                } else {
                    echo '<td align="right"><input id="saveForm" class="button1_red"  type="submit" value="Open Report" /></td>';
                }
                ?>
                <td>
                    <input type="button" class="button1_yellow" onclick="select_all()" value="Select All"/>
                    <input id="export" class="button1_green"  type="button" name="export" value="export" onclick="get_excel();"/>               
                    <input type="hidden" name="idcats_list" id="idcats_list" value="<?php echo $_SESSION['idcat_select']; ?>"/>
                    <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['username']; ?>"/>
                    <input type="hidden" id="password" name="password" value="<?php echo $_SESSION['password']; ?>"> 
                    <input type="hidden" id="libraryname" name="libraryname" value="<?php echo $_SESSION['libraryname']; ?>"> 
                    <input type="hidden" id="libraryaddress" name="libraryaddress" value="<?php echo $_SESSION['address']; ?>"> 
                    <input type="hidden" id="open_hours" name="open_hours" value="<?php echo $open_hours; ?>"> 

                </td>
                <td></td>
            </tr>   

        </table>

    </form>

    <form id="export_excel"  name="export_excel" method="post" action="../reports/report_runner.php">
        <input type="hidden" name="sql" id="sql" value="<?php echo $sql_report; ?>"/>
        <input type="hidden" name="report" id="report" value="mibase_export"/>
        <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['library_code']; ?>"/>
        <input type="hidden" id="password" name="password" value="<?php echo $_SESSION['mypassword']; ?>"> 

    </form>

</div>



