<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php
        include( dirname(__FILE__) . '/../header.php');
        //include( dirname(__FILE__) . '/../get_settings.php');
        ?> 
        <script>
            function get_report() {
                document.getElementById("export_excel").submit();
            }
        </script>
    </head>
    <body id="main_body">
        <div id="form_container">
            <?php include( dirname(__FILE__) . '/../menu.php'); ?>


            <script type="text/javascript">
                $(function () {
                    var pickerOpts = {
                        dateFormat: "yy-mm-dd",
                        showOtherMonths: true,
                        changeMonth: true,
                        changeYear: true,
                        showOn: "button"
                    };
                    $("#datefrom").datepicker(pickerOpts);
                    $("#dateto").datepicker(pickerOpts);
                    $("#dateend").datepicker(pickerOpts);
                    $("#start_excel").datepicker(pickerOpts);
                    $("#end_excel").datepicker(pickerOpts);
                });

            </script>
            <?php
            include ('../header_detail/header_detail.php');
            
            $link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/PdfReport';

            echo '<table width = "100%"><tr>';
            echo '<td width="80%"><h2>Reports:</h2></td>';
            if( $_SESSION['shared_server'] ) {
                echo '<td align="left">' . '<a class="button1_yellow" href="../add_reports/reports.php" title="Add more reports to the Report select box">Add Report to Reports List</a>' . '</td>';
            }

            echo '<tr></table>';
            include( dirname(__FILE__) . '/daily.php');
            include( dirname(__FILE__) . '/monthly.php');
            //echo '<br><h2>Export to excel function Temporarily available.</h2><br>';
            include( dirname(__FILE__) . '/export_excel.php');
            ?>
        </div>
    </body>
</html>


