<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');


$link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] .  '/PdfReport';
$idcat = $_SESSION['idcat'];
$toy_to_print = "'" . $idcat . "'";
//echo $loan_receipt;
//$loan_receipt = 'hello';
$loan_receipt = str_replace('"', "`", $loan_receipt);
?>
          <form id="reports"  method="post" action="../reports/report_runner.php">
            <table align="top"><tr>
                      <td align="right"><br><input id="loan_receipt" class="btn btn-colour-maroon"  type="submit" name="loan_receipt" value="Print Receipt" /></td>
                </tr>  

                <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['myusername']; ?>">
                <input type="hidden" id="password" name="password" value="<?php echo $_SESSION['mypassword']; ?>"> 
                <input type="hidden" id="libraryname" name="libraryname" value="<?php echo $_SESSION['libraryname']; ?>"> 
                <input type="hidden" id="report" name="report" value="loan_receipt">  
                <input type="hidden" id="bcode" name="bcode" value="<?php echo $_SESSION['borid']; ?>" >  
                <input type="hidden" id="libraryaddress" name="libraryaddress" value="<?php echo $_SESSION['address']; ?>"> 
                <input type="hidden" id="comments" name="comments" value="<?php echo $loan_receipt; ?>"> 
 
            </table>

        </form>
<?php //include("test.php"); ?>



