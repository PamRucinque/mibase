<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');

$query_reports = "Select * from reports where id = " . $_SESSION['report_id'] . ";";
//echo $query_reports;
$result_reports = pg_Exec($conn, $query_reports);
$numrows = pg_num_rows($result_reports);
for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_reports, $ri);
}
$report_sql = $row['sql'];
$report_name = $row['reportname'];
$date_range = $row['date_range'];
$report_id = $row['id'];
if ($date_range == 'Yes') {
    $report_sql = $row['sql'] . " where " . $row['where_sql'] . "::text <= '" . $_POST['end_excel'] . "'"
            . " and " . $row['where_sql'] . "::text >= '" . $_POST['start_excel'] . "' " . $row['sql_right'];
} else {
    $report_sql = $row['sql'];
}
if ($report_name == 'Buyers Report') {
    $report_sql = "SELECT
            toys.idcat,
            toys.id, toys.toyname,
            toys.category,
            category.description,
            coalesce(count(transaction.id),0) as no_loans,
            SUM(EXTRACT(EPOCH FROM transaction.return) - EXTRACT(EPOCH FROM transaction.created)) as days,
            min(transaction.date_loan)  as first,
            max(transaction.date_loan)  as last
            FROM toys LEFT JOIN category ON ( toys.category = category.category )
            LEFT OUTER JOIN transaction ON ( transaction.idcat = toys.idcat )
            WHERE toys.toy_status = 'ACTIVE'
            AND ((transaction.created::text >= '" . $_POST['start_excel'] .
            "' AND transaction.created::text <= '" . $_POST['end_excel'] . "') or transaction.created is null) 
            GROUP BY
            toys.idcat, toys.id, toys.toyname, toys.category, category.description
            ORDER BY category, id;";
}  
   

    