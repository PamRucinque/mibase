<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//connect to MySQL

//include( dirname(__FILE__) . '/../connect.php');
$query = "SELECT * FROM reports where category='excel' ORDER by reportname ASC;";
//echo $query;
$result = pg_Exec($conn, $query);

echo "<select name='report_code' id='report_code' style='width: 250px' Required>\n";

$numrows = pg_numrows($result);
//echo '<option value="" selected="selected"></option>';
//echo "<option value={$row['id']}>{$row['reportname']}   </option>\n";
echo "<option value='" . $report_id . "' selected='" . $report_name . "'></option>";

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    echo "<option value={$row['code']}>{$row['reportname']}   </option>\n";

    //echo "<option value=\"{$row['sql']}\">{$row['reportname']}   </option>\n";
}


echo "</select>\n";



?>

