<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');


$link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/PdfReport';
$idcat = $_SESSION['idcat'];
$toy_to_print = "'" . $idcat . "'";

?>
<br>

    <div id="form"  align="left">
        <form id="reports"  method="post" action="../reports/report_runner.php">
            <table align="top"><tr>
                      <td align="right"><br><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Print Roster" /></td>
                </tr>  

                <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['myusername']; ?>">
                <input type="hidden" id="password" name="password" value="<?php echo $_SESSION['mypassword']; ?>"> 
                <input type="hidden" id="libraryname" name="libraryname" value="<?php echo $_SESSION['libraryname']; ?>"> 
                <input type="hidden" id="report" name="report" value="Roster">  
                <input type="hidden" id="libraryaddress" name="libraryaddress" value="<?php echo $_SESSION['address']; ?>"> 
 
            </table>

        </form>
    </div>



