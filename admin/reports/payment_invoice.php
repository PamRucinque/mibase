<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>
<script>
    function go() {
        var idcats = "";

        $("tr.item").each(function() {
            $this = $(this);
            var idcat = $this.find("td.idcat").html();
            var value = $this.find("td :checkbox").is(':checked');
            if (value) {
                idcats +=  idcat + ",";
            }
        });

        if (idcats.length > 0) {
            idcats = idcats.substring(0, idcats.length - 1);
            $("#idcats_list_invoice").val(idcats);
            $("#idcats_list").val(idcats);
            //document.forms["reports"].submit();
            //document.reports.submit();
            return true;
        } else {
            alert('No payments selected!');
            return false;
        }

        //alert(idcats);
    }
                function select_all() {
                var idcats = "";
                $("tr.item").each(function() {
                    $this = $(this);
                    $this.find("td :checkbox").prop("checked", true);
                });
            }
</script>

<?php

$libraryname = $_SESSION['settings']['libraryname'];
$address = $_SESSION['settings']['address'];
$link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . '/' . $_SESSION['report_server_url'] . '/PdfReport';
$idcat = $_SESSION['idcat'];
$toy_to_print = "'" . $idcat . "'";
$payment_invoice = 'payment_invoice';

?>

<form id = "print_receipt"  name = "print_receipt" method = "post" action = "../reports/report_runner.php" >
    <input id="saveForm" class="button1_red"  type="submit" name="submit" value="Print Invoice" onclick="return go()"/>    
    <input type="hidden" name="idcats_list_invoice" id="idcats_list_invoice" value=""/>
    <input type = "hidden" id = "libraryname" name = "libraryname" value = "<?php echo $libraryname; ?>" >
    <input type="hidden" id="libraryaddress" name="libraryaddress" value="<?php echo $address; ?>"> 
    <input type = "hidden" id = "report" name = "report" value = "<?php echo $payment_invoice; ?>" >
    <input type = "hidden" id = "journal_id" name = "journal_id" value = "39768" >
    <input type="hidden" id="bcode" name="bcode" value="<?php echo $_SESSION['borid']; ?>" >  
    <input type = "hidden" id = "comments" name = "comments" value = "<?php echo $loan_receipt; ?>" >
    <input type = "hidden" id = "user" name = "user" value = "<?php echo $_SESSION['username']; ?>" >
    <input type = "hidden" id = "password" name = "password" value = "<?php echo $_SESSION['password']; ?>" >
</form>



