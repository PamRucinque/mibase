<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');


$loanperiod = $_SESSION['settings']['loanperiod'];
$password = $_SESSION['settings']['password'];
$libraryname = $_SESSION['settings']['libraryname'];


$link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/PdfReport';
$idcat = '';
if (isset($_SESSION['idcat'])) {
    $idcat = $_SESSION['idcat'];
}
$toy_to_print = "'" . $idcat . "'";
?>
<script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript">
    $(function() {
        var pickerOpts = {
            dateFormat: "yy-mm-dd",
            showOtherMonths: true

        };
        $("#start").datepicker(pickerOpts);
        $("#end").datepicker(pickerOpts);
    });

</script>
<?php
$month_start = strtotime('first day of this month', time());
$start = date('Y-m-d', $month_start);
$month_end = strtotime('last day of this month', time());
$end = date('Y-m-d', $month_end);
if ($loanperiod == ''){
    $loanperiod = 14;
}
?>

<h2>Reports with a Date Range (Monthly):</h2>
<div id="form"  align="left">
    <form id="reports"  method="post" action="../reports/report_runner.php">
        <table align="top"><tr>
                <td>Select Report to Print:<br><?php include( dirname(__FILE__) . '/get_report_monthly.php'); ?>    </td>
                <td>Select Start Day:<br>
                    <input type="text" name="start" id ="start" align="LEFT" value="<?php echo $start; ?>"></input><br></td>
                <td>Select End Day (if date range):<br>
                    <input type="text" name="end" id ="end" align="LEFT" value="<?php echo $end; ?>"></input><br></td>
                <td align="right"><br><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Open Report" /></td>
            </tr>   

            <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['username']; ?>">
            <input type="hidden" id="password" name="password" value="<?php echo $password; ?>"> 
            <input type="hidden" id="libraryname" name="libraryname" value="<?php echo $libraryname; ?>"> 
            <input type="hidden" id="libraryaddress" name="libraryaddress" value="<?php echo $address; ?>"> 
            <input type="hidden" id="loanperiod" name="loanperiod" value="<?php echo $loanperiod; ?>"> 
            <input type="hidden" id="borid" name="borid" value="<?php echo $_SESSION['borid']; ?>"> 
        </table>

    </form>
</div>



