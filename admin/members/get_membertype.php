<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<script>
    function viewroomSelected()
    {
        //alert(123);
        document.getElementById("filter").submit();
    }
</script>

<?php

//include( dirname(__FILE__) . '/../connect.php');

$conn = pg_connect($_SESSION['connect_str']);

$query = "SELECT id, membertype, description FROM membertype ORDER by membertype ASC;";
$result_mem = pg_Exec($conn, $query);

echo "<select name='membertype' id='membertype' style='width: 100px' onChange='viewroomSelected()'>\n";

$numrows = pg_numrows($result_mem);
echo '<option value="" selected="selected"></option>';
for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_mem, $ri);
    echo "<option value='{$row['membertype']}'>{$row['membertype']}
    </option>\n";
}
echo "<option value='New Members'>New Members</option>";
echo "</select>\n";
?>

