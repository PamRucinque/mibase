<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<p><font size="2" face="Arial, Helvetica, sans-serif"></font></p>

<font></font>
<?php
$id = $_GET['id'];
//echo '<br><a href="../toy_detail.php" class="button1">Back to Toy Detail</a>'; 
echo '<br><a href="membertypes.php" class="button1_red">Back to Member Types</a>';
?>
<script>

    $(function () {
        var pickerOpts = {
            dateFormat: "d MM yy",
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "2017:+15"

        };
        $("#due").datepicker(pickerOpts);

    });


</script>
<script type="text/javascript" src="../../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'edit_type.php'; ?>">

    <div id="form" style="background-color:lightgray;" align="left">
        <table align="top"><tr>

                <td align="right"><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Update Member Type" /></td>
            </tr>
            <tr>
                <td><h2>Member Type: <?php echo $membertype; ?> </h2><br><br>
            </tr>
            <tr>
                <td>Description:<br>
                    <input type="Text" name="description" align="LEFT"  size="50" value="<?php echo $description; ?>"></input><br></td>
            </tr>
            <tr><td>Max No Toys: <br><input type="number" name="maxnoitems" min="0" max="200" value="<?php echo $maxnounits; ?>"></td></tr>
            <tr><td>Required Duties: <br><input type="Number" name="duties" step ="0.1" min="0" max="20"size="10"  value="<?php echo $duties; ?>"></td></tr>
            <tr><td>Renewal Fee: <br><input type="number" name="renewal_fee" step ="0.1" min="0" max="1000" value="<?php echo $renewal_fee; ?>"></td></tr>
            <tr><td>Return Period (days): <br><input type="number" name="returnperiod" min="0" max="52" value="<?php echo $returnperiod; ?>"></td></tr>
            <tr><td>Expiry Period (months): <br><input type="number" name="expiryperiod" min="0" max="120" value="<?php echo $expiryperiod; ?>"></td></tr>
            <tr><td>Bond / Levy: <br><input type="number" name="bond" min="0" max="1000" id="bond" value="<?php echo $levy; ?>"></td></tr>
            <tr><td>Gold Star Toys: <br><input type="Number" name="gold_star" step ="0.1" min="0" max="20"size="10"  value="<?php echo $gold_star; ?>"></td></tr>

            <tr>
                <td>Member Type Due Date:<br>
                    <input type="text" name="due" id="due" align="LEFT"  size="25" value="<?php echo $due; ?>"></input></td>

            </tr>
            <tr><td>Exclude from New Member Online Form:<br>
                    <select id="exclude" name="exclude" style="width: 70px;">
                        <option value='<?php echo $exclude; ?>' selected="selected"><?php echo $exclude; ?></option>
                        <option value="Yes" >Yes</option>
                        <option value="No" >No</option>
                    </select>
                </td></tr>

            <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"> 
        </table>

    </div>
</form></p>

