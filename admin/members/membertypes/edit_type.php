<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>



    <body id="main_body" >
        <div id="form_container">
            <?php include( dirname(__FILE__) . '/../../menu.php'); ?>

            <?php
            //include( dirname(__FILE__) . '/get_type.php');
            //include( dirname(__FILE__) . '/toy_detail.php');


            if (isset($_POST['submit'])) {
                $description = clean($_POST['description']);
                $exclude = clean($_POST['exclude']);
                if (!is_numeric($_POST['renewal_fee'])) {
                    $_POST['renewal_fee'] = 0;
                }
                If ($_POST['due'] == '') {
                    $_POST['due'] = NULL;
                }
                if (!is_numeric($_POST['returnperiod'])) {
                    $_POST['returnperiod'] = 0;
                }
                if (!is_numeric($_POST['duties'])) {
                    $_POST['duties'] = 0;
                }
                if (!is_numeric($_POST['maxnoitems'])) {
                    $_POST['maxnoitems'] = 0;
                }
                if (!is_numeric($_POST['expiryperiod'])) {
                    $_POST['expiryperiod'] = 0;
                }
                if (!is_numeric($_POST['bond'])) {
                    $_POST['bond'] = 0;
                }
                if (!is_numeric($_POST['gold_star'])) {
                    $_POST['gold_star'] = 0;
                }
                //include( dirname(__FILE__) . '/../../connect.php');
                
                
                
                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $query_edit = "UPDATE membertype SET
                    maxnoitems = ?,
                    description = ?,
                    renewal_fee = ?,
                    gold_star = ?,
                    returnperiod = ?,
                    expiryperiod = ?,
                    duties = ?,
                    bond = ?,
                    exclude = ?,
                    due = ? 
                    WHERE id=?;";

                //create the array of data to pass into the prepared stament
                $sth = $pdo->prepare($query_edit);
                $array = array();
                $array = array($_POST['maxnoitems'], $description, $_POST['renewal_fee'], $_POST['gold_star'],
                    $_POST['returnperiod'], $_POST['expiryperiod'], $_POST['duties'], $_POST['bond'], $_POST['exclude'], $_POST['due'], $_POST['id']);
                $sth->execute($array);
                $stherr = $sth->errorInfo();


                if ($stherr[0] != '00000') {
                    echo "An UPDATE query error occurred.\n";
                    //echo $query_edit;
                    echo $connect_pdo;
                    echo 'Error ' . $stherr[0] . '<br>';
                    echo 'Error ' . $stherr[1] . '<br>';
                    echo 'Error ' . $stherr[2] . '<br>';
                    exit;
                } else {
                    echo "<br>The record was successfully saved. " . "<a class='button1_red' href='membertypes.php'>OK</a>" . "<br><br>";
                    $redirect = 'Location:membertypes.php';
                    //header($redirect);
                }
            } else {
                include( dirname(__FILE__) . '/get_type.php');
                include( dirname(__FILE__) . '/edit_form_type.php');
            }

            function clean($input) {
                $output = stripslashes($input);
                $output = str_replace("'", "`", $output);
                return $output;
            }
            ?>
        </div>
    </body>
</html>