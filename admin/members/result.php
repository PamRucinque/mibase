
<script>
    function update_status(rid, str) {
        //alert(input);
        // var myinput = document.getElementById("myinput");
        console.log("update_status" + rid + " " + str);
        var id = 'ms_' + rid;
        $.ajax({url: "update_status.php",
            data: {borid: rid, status: str},
            success: function (result) {
                if (result.match("saved")) {

                } else {
                    alert(result);
                    //alert(id);
                    document.getElementById(id).value = 'ACTIVE';

                }
            },
            error: function () {
                alert('error saving');
                //document.getElementById('member_status').selectedIndex = 0;

            }});

    }
</script>
<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
$members_show_joined = '';
//include( dirname(__FILE__) . '/../get_settings.php');
$orderby = "surname, firstname ASC;";

if (isset($_GET['submit'])) {
    $_SESSION['membertype'] = $_GET['membertype'];
    $_SESSION['search_mem'] = $_GET['search'];
}
//submit_search
if (isset($_GET['submit_search'])) {
    $_SESSION['membertype'] = '';
    $_SESSION['search_mem'] = '';
}

if (isset($_GET['membertype'])) {
    $_SESSION['membertype'] = $_GET['membertype'];
}
if (isset($_GET['orderby'])) {
    if ($_GET['orderby'] == 'expired') {
        $orderby = "expired, surname, firstname ASC;";
    }
    if ($_GET['orderby'] == 'membertype') {
        $orderby = "membertype, surname, firstname ASC;";
    }
    if ($_GET['orderby'] == 'surname') {
        $orderby = "surname, firstname ASC;";
    }
}
$membertype_str = '';
if (isset($_SESSION['membertype'])) {
    $membertype_str = $_SESSION['membertype'];
}



$search = "";
if (isset($_GET['search'])) {
    $search = $_GET['search'];
}
//
//"OR supplier LIKE '%" . $search . "%' " .

$search = strtoupper($search);
//error message (not found message)
//    (select count(id) from transaction where return is null and transaction.borid = borwrs.id) as loans
$query = "SELECT borwrs.*,
    (select count(childid) from children where children.id = borwrs.id) as children

FROM borwrs
WHERE (upper(surname) LIKE '%" . $search . "%' " .
        "OR upper(firstname) LIKE '%" . $search . "%' " .
        "OR id::varchar(255) LIKE '%" . $search . "%' " .
        "OR upper(partnersname) LIKE '%" . $search . "%' " .
        "OR upper(partnerssurname) LIKE '%" . $search . "%' " .
        "OR upper(emailaddress) LIKE '%" . $search . "%' " .
        ") AND (membertype  LIKE '%{$membertype_str}%')
AND (member_status = 'ACTIVE') 
ORDER BY " . $orderby;

if ($membertype_str == 'New Members') {
    $query = "SELECT borwrs.*,
        (select count(childid) from children where children.id = borwrs.id) as children
        FROM borwrs
        WHERE (upper(surname) LIKE '%" . $search . "%' " .
            "OR upper(firstname) LIKE '%" . $search . "%' " .
            "OR id::varchar(255) LIKE '%" . $search . "%' " .
            "OR upper(partnersname) LIKE '%" . $search . "%' " .
            "OR upper(emailaddress) LIKE '%" . $search . "%' " .
            ") AND (datejoined > current_date - 120)
        AND (member_status = 'ACTIVE') 
        ORDER BY datejoined desc, surname, firstname ASC;";
}

//$query = "SELECT * FROM toys ORDER by id ASC;";
//echo $query;

$XX = "No Record Found";
//print $query;
//$query = "SELECT * FROM hm_claims order by id";
$total = 0;
$children = 0;
//include( dirname(__FILE__) . '/../connect.php');
$result_list = pg_exec($conn, $query);
$numrows = pg_numrows($result_list);
//echo $query;
//$result_txt =  '<table border="1"><tr>';
$result_txt = '';


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_list, $ri);
    $total = $total + 1;
    $children = $children + $row['children'];
    $expired = $row['expired'];
    //$loans = $row['loans'];
    $expire_next_month = date('Y-m-d', strtotime('+1 month'));
    $format_expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
    $format_joined = substr($row['datejoined'], 8, 2) . '-' . substr($row['datejoined'], 5, 2) . '-' . substr($row['datejoined'], 0, 4);
    if ($expired <= date("Y-m-d")) {
        $expired_txt = '<font color="red">' . $format_expired . '</font>';
    } else {
        if (($expired > date("Y-m-d")) && ($expired < $expire_next_month)) {
            $expired_txt = '<font color="blue">' . $format_expired . '</font>';
        } else {
            $expired_txt = '<font color="black">' . $format_expired . '</font>';
        }
    }
    $membertype_str = '';
    if (isset($_GET['membertype'])) {
        $membertype_str = $_GET['membertype'];
    }


    if ($membertype_str == 'New Members') {
        $joined_txt = '<td align="center">' . $format_joined . '</td>';
    } else {
        if ($members_show_joined == 'Yes') {
            $joined_txt = '<td align="center">' . $format_joined . '</td>';
        } else {
            $joined_txt = '<td align="center">' . $expired_txt . '</td>';
        }
    }


    $borid = $row["id"];
    $firstname = $row['firstname'];
    $firstname = substr($firstname, 0, 15);
    $membertype = $row["membertype"];
    $surname = $row["surname"];
    $partnersname = $row['partnersname'];
    $partnersname = substr($partnersname, 0, 10);
    $email = $row['emailaddress'];
    $email = strtolower(substr($email, 0, 30));
    $phone = $row["phone"];
    $mobile1 = $row['phone2'];
    $mobile = $row["mobile1"];
    $member_status = $row["member_status"];
    $address = $row['address'];
    $suburb = $row['suburb'];
    $membertype = $row['membertype'];
    $email_link = '<a href="mailto:' . $email . '">send</a>';
    $date_expired = date_create_from_format('d-m-Y', $format_expired);
    $status_select = '<select id="ms_' . $borid . '" onchange="update_status(' . $borid . ', this.value)" />
                        <option value=' . $member_status . ' selected="selected">' . $member_status . '</option>
                        <option value="LOCKED" >LOCKED</option>
                        <option value="RESIGNED" >RESIGNED</option>
                      </select>';
    //$today = date;
    $link_member = 'update/member_detail.php?borid=' . $borid;
    $onclick_member = 'javascript:location.href="' . $link_member . '"';
    $result_txt .= "<tr border='1' class='member' id='red' ondblclick='" . $onclick_member . "'>";
    $result_txt .= '<td class="borid" align="center" width="10">' . $borid . '</td><td align="center"><input type="checkbox" ></td>';
    if (($partnersname != null) && ($partnersname != '')) {
        $partnersname = ' and ' . $partnersname;
    }
    //$result_txt .= '<tr border="1"><td border="1" width="50">' . $borid . '</td>';
    $result_txt .= '<td align="left">' . $surname . ', ' . $firstname . $partnersname;
    $result_txt .= '</td>';

    // $result_txt .= '<td align="left">' . $firstname . '</td>';


    $result_txt .= '<td>' . $status_select . '</td>';


    //$result_txt .= '<td align="left" style="overflow:hidden;">' . $partnersname . '</td>';
    $result_txt .= $joined_txt;
    $result_txt .= '<td align="left">' . $email . '</td>';
    //$result_txt .= '<td align="left">' . $email_link . '</td>';
    $result_txt .= '<td>' . $mobile1 . '</td>';
    //$result_txt .= '<td>' . $phone . '</td>';
    //$result_txt .= '<td>' . $email . '</td>';
    $result_txt .= '<td>' . $membertype . '</td>';
    //$result_txt .= '<td width="50"><a href="toy_detail.php?idcat=' . $idcat . '">View</a></td>';
    $result_txt .= '<td><a class="button_small" href="update/member_detail.php?borid=' . $borid . '"/>View</a></td>';
    $result_txt .= '</tr>';
}
$result_txt .= '</table>';

//print '<br/>' . $query;
//print '<br/>' . $location;
//below this is the function for no record!!
//end

$search_str = '';
if ($membertype_str != '') {
    $search_str .= $search_str . ' Filtered on Type = ' . $_SESSION['membertype'] . ' ';
}
$search_mem_str = '';
if (isset($_SESSION['search_mem'])) {
    $search_mem_str = $_SESSION['search_mem'];
}
if ($search_mem_str != '') {
    $search_str .= $search_str . ' Filtered on String = ' . $search_mem_str . ' ';
}
if ($search_str == '') {
    print '<div id="closed"><table width="100%"><tr><td width= 50%><font color="red">Double click on row to go to toy.</font></td>';
} else {
    print '<div id="open"><table width="100%"><tr><td width= 50%><h1 align="left">' . $search_str . '</h1></td>';
}
print '<td><h1 align="right">Total members: ' . $total . '<font color="blue">  Total Children: ' . $children . ' </font></h1></td></tr></table></div>';
print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
print '<tr style="color:green"><td>id</td><td>Select</td><td>Last Name</td><td>Change status</td><td>Exp/joined</td><td>Email</td><td>Mobile</td><td>Member Type</td></tr>';

print $result_txt;
?>
