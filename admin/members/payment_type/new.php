<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <body id="main_body" >
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');
            $conn = pg_connect($_SESSION['connect_str']);
            $_SESSION['error'] = '';
            echo "<br>" . "<a class='button1_red' href='payment_types.php'>Back to Payment Types</a>" . "<br><br>";
            include( dirname(__FILE__) . '/new_payment_typeid.php');
            include( dirname(__FILE__) . '/new_form_payment_type.php');

            if (isset($_POST['submit'])) {
                //$toyid = sprintf("%02s", $toyid);

                $paymenttype = pg_escape_string($_POST['code']);


                $query_option = "INSERT INTO typepayment (id, typepayment )
         VALUES ({$newpaymenttypeid}, 
         '{$paymenttype}');";

                $result_new = pg_Exec($conn, $query_option);


                if (!$result_new) {
                    //echo "An INSERT query error occurred.\n";
                    $_SESSION['error'] = "An INSERT query error occurred.\n" . $query_option;
                }

// Get the last record inserted
// Print out the Contact ID
                else {
                    $_SESSION['error'] = '';
                    echo "<br>The record was successfully entered and the ID is: " . $newpaymenttypeid . "<a class='button1_red' href='payment_types.php'>OK</a>" . "<br><br>";
                }
            }
            ?>

    </body>