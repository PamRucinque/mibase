<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');




$query = "SELECT * from typepayment order by typepayment;";
$conn = pg_connect($_SESSION['connect_str']);
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);


if ($numrows > 0) {

    echo '<table border="1" width="60%" style="border-collapse:collapse; border-color:grey">';
    echo '<tr><td>id</td><td>Payment Type</td><tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);

    


    echo '<td width="30" align="center">' . $row['id'] . '</td>';
    echo '<td align="left">' . $row['typepayment'] . '</td>';

    if ($row['typepayment'] != 'Levy') {
        //echo '<td><a class="button_small" href="edit_payment_type.php?id=' . $row['id'] . '"/>Edit</a>';
        echo '<td><a class="button_small_red" href="delete_payment_type.php?id=' . $row['id'] . '"/>Delete</a></td>';
    } else {
        echo '<td></td>';
    }

//echo '<td width="50" align="center">' . $download2 . '</td>';
//echo '<td width="100">' . $row['man_html'] . '</td>';
    echo '</tr>';
}
echo '<tr height="0"></tr>';
echo '</table>';
?>

</body>


