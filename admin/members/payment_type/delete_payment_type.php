<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');
//$numrows = pg_numrows($result);
// confirm that the 'id' variable has been set
if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    // get the 'id' variable from the URL
    $conn = pg_connect($_SESSION['connect_str']);
    $query = "DELETE FROM typepayment WHERE id = " . $_GET['id'] . ";";
    $result = pg_exec($conn, $query);

    // Check result
    // This shows the actual query sent to MySQL, and the error. Useful for debugging.
    if (!$result) {
        $message = 'Invalid query: ' . "\n";
        $message = 'Whole query: ' . $query;
        $message = 'There was an error deleting this Payment Type, check to see if it has related records in the Members table';
        //die($message);
        echo $message;
        $_SESSION['error'] = $message;
    } else {
        echo $query;
        $_SESSION['error'] = '<font color="red"><br>You have successfully deleted a record</font>';
    }
    $redirect = "Location: payment_types.php";               //print $redirect;
    header($redirect);
}
?>
