<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');

include('../../connect.php');

if (isset($_SESSION['borid'])) {
    $memberid = $_SESSION['borid'];
} else {
    $memberid = 0;
}
$x=0;
$total = 0;
$hrs = 0;

$sql = "select roster.id as id, to_char(date_roster,'mm/dd/yyyy') as date_roster, type_roster, weekday, roster_session, duration, status, notes,
to_char(expired,'dd/mm/yyyy') as expired, renewed,
m.expiryperiod, m.duties as required,

to_char(CASE
    WHEN renewed is null THEN (expired - m.expiryperiod * '1 month'::INTERVAL) 
    ELSE  renewed
  END,'dd/mm/yyyy') as start


from roster
left join borwrs b on b.id = roster.member_id
left join membertype m on m.membertype = b.membertype
where 
date_roster < (CASE
    WHEN renewed is null THEN (expired - m.expiryperiod * '1 month'::INTERVAL) 
    ELSE  renewed
  END)
and date_roster > current_date - INTERVAL '2 years'
and b.id = ?;";

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$sth = $pdo->prepare($sql);
$array = array($memberid);
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $_SESSION['error'] = "An  error occurred.\n";
    $_SESSION['error'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[2] . '<br>';
}
$numrows = $sth->rowCount();
if ($numrows > 0){
    //include('heading.php');
}
$complete_str = '';
include('heading_hist.php');

for ($ri = 0; $ri < $numrows; $ri++) {
    $roster = $result[$ri];

    $link = 'member_detail.php?borid=' . $roster['id'];
    $onclick = 'javascript:location.href="' . $link . '"';
    $ref_edit = '../../roster/edit_roster.php?id=' . $roster['id'];
    $str_edit = " <a class ='btn btn-primary btn-sm' href='" . $ref_edit . "'>Edit</a>";
    if ($roster['status'] == 'completed'){
        $complete_str = 'Yes';
    }

    include('row_bg_color_hist.php');
    include('row.php');
    $hrs = $hrs + $roster['duration'];
    $total = $total + 1;
}



