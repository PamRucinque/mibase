<h2>Roster Duties</h2>
<a class="btn btn-primary btn-sm" align="right" href="../../roster/new_roster.php">New</a>
 <a class="btn btn-success btn-sm" align="right" href="../../roster/credit_roster.php">Roster Credit</a>
 <a class="btn btn-danger btn-sm" align="right" href="../../roster/debit_roster.php">Roster Debit</a><br>
<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');

include('../../connect.php');

if (isset($_SESSION['borid'])) {
    $memberid = $_SESSION['borid'];
} else {
    $memberid = 0;
}
$x = 0;
$total = 0;
$hrs = 0;
$required = 0;
$tocomplete = 0;

$sql = "select roster.id as id, to_char(date_roster,'mm/dd/yyyy') as date_roster, type_roster, weekday, roster_session, duration, status, notes,
to_char(expired,'dd/mm/yyyy') as expired, renewed,
m.expiryperiod, coalesce(m.duties,0) as required,

to_char(CASE
    WHEN renewed is null THEN (expired - m.expiryperiod * '1 month'::INTERVAL) 
    ELSE  renewed
  END,'dd/mm/yyyy') as start


from roster
left join borwrs b on b.id = roster.member_id
left join membertype m on m.membertype = b.membertype
where 
date_roster >= (CASE
    WHEN renewed is null THEN (expired - m.expiryperiod * '1 month'::INTERVAL) 
    ELSE  renewed
  END)
and b.id = ?;";

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$sth = $pdo->prepare($sql);
$array = array($memberid);
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $_SESSION['error'] = "An  error occurred.\n";
    $_SESSION['error'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[2] . '<br>';
}
$numrows = $sth->rowCount();
if ($numrows > 0) {
    //include('heading.php');
}
$complete_str = '';
//include('heading.php');

for ($ri = 0; $ri < $numrows; $ri++) {
    $roster = $result[$ri];
    if ($ri == 0) {
        include('heading.php');
    }
    $link = 'member_detail.php?borid=' . $roster['id'];
    $onclick = 'javascript:location.href="' . $link . '"';
    $ref_edit = '../../roster/edit_roster.php?id=' . $roster['id'];
    $str_edit = " <a class ='btn btn-primary btn-sm' href='" . $ref_edit . "'>Edit</a>";
    if ($roster['status'] == 'completed') {
        $complete_str = 'Yes';
    }

    include('row_bg_color.php');
    include('row.php');
    $hrs = $hrs + $roster['duration'];
    $total = $total + 1;
}
if (isset($roster['required'])) {
    $tocomplete = $roster['required'] - $hrs;
}


//$required = number_format($roster['required'],0);
if ($numrows > 0) {
    include ('footer.php');
} else {
    echo '<h4>No Current Rosters Scheduled</h4>';
}
?>
<script>
    function toggle_hist() {
        if ($("#roster_history").is(":visible")) {
            $("#roster_history").hide();
            document.getElementById("show_history").value = 'Show More Roster Duties';
        }else{
            $("#roster_history").show();
            document.getElementById("show_history").value = 'Show Less Roster Duties';
        }
    }
</script>
<br><input type="button" class="btn btn-primary" value="Show More Roster Duties" name="show_history" id="show_history" onclick="toggle_hist();"/>
<div id="roster_history" style="display: none;">
    <?php include('list_hist.php'); ?>
</div>



