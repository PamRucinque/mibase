<?php

/*
 * Copyright (C) 2017 michelle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!session_id()) {
    session_start();
}
//include( dirname(__FILE__) . '/../connect.php');
$conn = pg_connect($_SESSION['connect_str']);
$borid = $_GET['borid'];
if (check_loans($_GET['borid']) == 0) {
    $query = "update borwrs set member_status = '" . $_GET['status'] . "' where id = " . $_GET['borid'] . ";";
    $conn = pg_connect($_SESSION['connect_str']);
    $result_mem = pg_Exec($conn, $query);
    if (!$result_mem) {
        echo 'Error in saving';
        echo $query;
        //exit;
    } else {
        echo 'saved';
    }
} else {
    echo 'This member has Toys on loan, cannot change status to LOCKED or RESIGNED';
}

function check_loans($borid) {
    //include( dirname(__FILE__) . '/../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $query = "select id from transaction where return is null and borid = " . $borid . ";";
    $result_mem = pg_Exec($conn, $query);
    $numrows = pg_numrows($result_mem);
    return $numrows;
}
