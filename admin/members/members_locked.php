<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>
    <?php $total = 0; ?>
    <body>
        <div id="form_container">
            <?php include( dirname(__FILE__) . '/../menu.php'); ?>
            <div style="display: none; position: absolute; z-index: 110; left: 400; top: 1000; width: 15; height: 15" id="preview_div"></div>
            <br>

            <form name="openclaims" id="openclaims" method="post" action="members_locked.php" >    
                <table width=100% height="60px">
                    <tr bgcolor="lightgray">
                        <td width="20%"><label class="description" for="element_12">Category: </label><?php include( dirname(__FILE__) . '/get_membertype.php'); ?> </td>
                        <td width="10%"><label class="description" for="search">Search String</label><input id="search" name="search"  type="text" maxlength="255"  value=""/></td>
                        <td width="40%" align="bottom"><label class="description" for="search">click to search</label><input class="button1_blue" type="submit" name ="submit" value="Search"></td>
                        <td width="10%" align="right"><a class="button1_yellow" href="members.php"/>Current Members</a></td>
                    </tr>
                </table>
            </form>

            <?php
            //if (isset($_POST['submit'])) {
            if ($_SESSION['level'] == 'admin') {
                include( dirname(__FILE__) . '/result_locked.php');
            } else {
                echo '<br><br><h2>The members list has been disabled for volunteer login.</h2>';
            }

            //include( dirname(__FILE__) . '/result_locked.php');
            // } else {
            //    $location = 'ALL';
            //    $assessor = '';
            //}
            //print 'hello'; }
            ?>

        </div>

    </body>

</html>
