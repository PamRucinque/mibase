<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');

//set default values
$location = '';
$suburb_title = '';
$suburb = '';
$city = '';
$membertype = '';
$discovery = '';
$rostertype = '';
$rostertype2 = '';
$rostertype3 = '';
$rostertype4 = '';
$nationality_label = '';
$user1_borwrs = '';
$license = '';
$help_txt = '';
$simple_new_member = $_SESSION['settings']['simple_new_member'];
$settings_expired = $_SESSION['settings']['settings_expired'];
$new_member_email = $_SESSION['settings']['new_member_email'];
?>
<!doctype html>
<html lang="en">
    <head>
<?php include( dirname(__FILE__) . '/../../header.php');
?> 
    </head>
    <script type="text/javascript">
        $(function () {
            var pickerOpts = {
                dateFormat: "d MM yy",
                showOtherMonths: true

            };
            $("#submitted").datepicker(pickerOpts);
            $("#closed").datepicker(pickerOpts);
            $("#joined").datepicker(pickerOpts);
        });

    </script>
    <body>
        <div id="form_container">
<?php include( dirname(__FILE__) . '/../../menu.php'); ?>

            <?php
            $automatic_debit_off = 'No';
            //include( dirname(__FILE__) . '/../../connect.php');
            //include( dirname(__FILE__) . '/../../get_settings.php');
            include( dirname(__FILE__) . '/functions.php');
            // echo $settings_expired . '<br>';
            ?>

            <?php
            if (isset($_POST['submit'])) {
                $discovery = $_POST['discovery'];
                $now = date('Y-m-d');

                //$toyid = sprintf("%02s", $toyid);

                $firstname = clean($_POST['firstname']);
                $surname = clean($_POST['surname']);
                $notes = clean($_POST['notes']);
                $partnersname = clean($_POST['partnersname']);
                $partnerssurname = clean($_POST['partnerssurname']);
                $membertype = clean($_POST['membertype']);

                $city = clean($_POST['city']);
                if ($simple_new_member != 'Yes') {
                    $suburb = clean($_POST['suburb']);
                    $rostertype3 = $_POST['rostertype3'];
                } else {
                    $rostertype3 = null;
                    $suburb = null;
                }
                $address = clean($_POST['address']);
                $address2 = clean($_POST['address2']);
                $postcode = clean($_POST['postcode']);
                $mobile1 = clean($_POST['mobile1']);
                $mobile2 = clean($_POST['mobile2']);
                $email = clean($_POST['emailaddress']);
                $phone = clean($_POST['phone']);
                $state = clean($_POST['state']);
                $alert = clean($_POST['alertmem']);
                $user1 = clean($_POST['user1']);
                $license = clean($_POST['license']);
                if (isset($_POST['help_txt'])){
                   $help = clean($_POST['help_txt']); 
                }else{
                    $help = '';
                }
                
                $skills = clean($_POST['skills']);
                $newmemid = $_POST['id'];
                $location = clean($_POST['location']);
                if ($_POST['joined'] == '') {
                    $joined = $now;
                } else {
                    $joined = $_POST['joined'];
                }
                $renewed = $now;

                if ($settings_expired != '') {
                    if ($settings_expired_year != '') {
                        $expired = trim($settings_expired_year) . "-" . trim($settings_expired);
                    } else {
                        $expired = date('Y', strtotime('+1 year')) . "-" . trim($settings_expired);
                    }
                } else {
                    $expired = date_expired($membertype, $joined);
                }


                $str = $surname;
                $length_surname = strlen($str);
                if ($length_surname < 10) {
                    $str = pad_surname($str);
                } else {
                    $str = substr($str, 0, 10);
                    $length_surname = 10;
                }
                $str = strtolower($str);
                $username = rtrim($str, "%") . $newmemid;
                $username = str_replace(" ", "", $username);
                $username = str_replace("-", "", $username);
                $username = str_replace("_", "", $username);
                $pwd = generateStrongPassword();


                
                
                

                try {
                    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                } catch (PDOException $e) {
                    print "Error! toys : " . $e->getMessage() . "<br/>";
                    die();
                }

                $query = "INSERT INTO borwrs (id, firstname, surname, membertype, notes, partnersname, 
        partnerssurname, suburb, city, address, postcode, 
        phone2, mobile1, emailaddress, phone, member_status, 
        datejoined, renewed, expired,
        rostertype, rostertype2, rostertype3,rostertype6, address2,state, discoverytype, alert, location, pwd, rostertype5, id_license, skills2,skills, changedby)
                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

                $sth = $pdo->prepare($query);
                if (isset($_POST['id'])) {
                    $mem_id = $_POST['id'];
                }

                //create the array of data to pass into the prepared stament
                $array = array($mem_id, $firstname, $surname, $membertype, $notes, $partnersname,
                    $partnerssurname, $suburb, $city, $address, $postcode,
                    $mobile1, $mobile2, $email, $phone, 'ACTIVE',
                    $joined, $renewed, $expired,
                    $_POST['rostertype1'], $_POST['rostertype2'], $rostertype3, $_POST['rostertype4'],
                    $_POST['address2'], $state, $discovery, $alert, $location, $pwd, $username, $license, $help, $skills, $_SESSION['username']);


                $sth->execute($array);

                $stherr = $sth->errorInfo();



                if ($stherr[0] != '00000') {
                    echo "An INSERT query error occurred.\n";
                    echo $query;
                    echo $connect_pdo;
                    exit;
                } else {
                    include( dirname(__FILE__) . '/delete_mem_history.php');

                    $type = get_type($membertype);
                    $amount = $type['renewal'];
                    $description = 'Membership fees';
                    $desc_levy = 'Membership Levy';
                    $category = $type['description'];
                    $levy = $type['levy'];
                    if ($category == '') {
                        $category = $membertype;
                    }

                    $longname = $firstname . ' ' . $surname;
                    //$borname = str_replace("'", "`", $_POST['borname']);
                    //echo 'Auto debit: ' . $automatic_debit_off . '<br>';
                    if ($automatic_debit_off != 'Yes') {
                        //include( dirname(__FILE__) . '/new_paymentid.php');
                        $payment_str = add_to_journal($joined, $_POST['id'], 0, $longname, $description, $category, $amount, 'DR', 'SUBS');
                        if ($levy != 0) {
                            $str = add_to_journal($joined, $_POST['id'], 0, $longname, $desc_levy, $category, $levy, 'DR', 'SUBS');
                            $payment_str .= '<br>The Membership Levy of ' . $levy . ' has been added.';
                        }
                        echo '<br>' . $payment_str;
                    }

                    $edit_url = 'member_detail.php?id=' . $_POST['id'];
                    echo "<br>Expires: " . $expired;
                    echo "<br>The record was successfully saved and the ID is:" . $_POST['id'] . "<br><br>";
                    echo '<a class="button1" href="member_detail.php?borid=' . $_POST['id'] . '">OK</a>';
                    $_SESSION['borid'] = $_POST['id'];
                    include( dirname(__FILE__) . '/get_member.php');
                    if ($new_member_email == 'Yes') {
                        echo '<a href="send_email_new.php?borid=' . $_POST['id'] . '" class="button1_green">Send Welcome Email</a><br>';

                        //echo 'Yes send me an email';
                        //include( dirname(__FILE__) . '/send_email_new.php');
                        //echo '<p>A Welcome Email has been sent to ' . $firstname . ' ' . $surname . ' email: ' . $email . '</p><br>';
                        //echo 'Subject: <font color="blue">' . $template_subject . '</font><br>';
                        //echo 'Message: <font color="blue">' . $template_message . '</font><br>';
                    }
                }


                pg_FreeResult($result);
// Close the connection
                pg_Close($conn);
            } else {
                //include( dirname(__FILE__) . '/newidcat.php');
                include( dirname(__FILE__) . '/new_memid.php');
                if (isset($_SESSION['simple_new_member']) && $_SESSION['simple_new_member'] == 'Yes') {
                    include( dirname(__FILE__) . '/new_form_simple.php');
                } else {
                    include( dirname(__FILE__) . '/new_form.php');
                }
            }
            ?>
        </div>
    </body>
</html>