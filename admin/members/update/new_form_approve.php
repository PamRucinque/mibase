<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');
?>

<body id="main_body">

    <div id="form_container">

        <font size="2" face="Arial, Helvetica, sans-serif"></font>


        <form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'new_approve.php?id=' . $_POST['id']; ?>">

            <div id="form" style="background-color:lightgray;" align="left">
                <table width="100%"><tr><td>
                            <h2>Member Details:</h2></td>
                        <td align="right"><input id="saveForm" class="button1_red" type="submit" name="submit" value="Save New Member" />


                        </td></tr>

                    <tr><td><table>
                                <tr><td width="35%">Contact 1 - First Name:<br>
                                        <input type="Text" name="firstname" align="LEFT" required="Yes" size="35" value="<?php echo $firstname_new; ?>"></input></td>
                                    <td width="35%">Surname:<br>
                                        <input type="Text" name="surname" align="LEFT" required="Yes" size="35" value="<?php echo $surname_new; ?>"></input><br></td>

                                    <td>Mobile:<br>
                                        <input type="Text" name="mobile1" align="LEFT" required="Yes" size="20" value="<?php echo $mobile1_new; ?>"></input><br></td>
                                <tr><td width="35%">Contact 2 - First Name:<br>
                                        <input type="Text" name="partnersname" align="LEFT" size="35" value="<?php echo $partnersname_new; ?>"></input></td>
                                    <td width="35%">Surname:<br>
                                        <input type="Text" name="partnerssurname" align="LEFT"  size="35" value="<?php echo $partnerssurname_new; ?>"></input><br></td>

                                    <td width="25%">Mobile:<br>
                                        <input type="Text" name="mobile2" align="LEFT"  size="20" value="<?php echo $mobile2_new; ?>"></input><br></td>
                            </table>
                            <h2>Contact Details:</h2>
                            <table>
                                <tr><td width="45%">Address:<br>
                                        <input type="Text" name="address" align="LEFT"  size="35" value="<?php echo $address_new; ?>"></input><br>
                                    <td width="40%" align='top'>Suburb / City:<br>
                                        <input type="Text" name="suburb" align="LEFT"  size="35" value="<?php echo $suburb_new; ?>"></input><br><br></td>

                                    <td  align='top'>Postcode:<br>
                                        <input type="Text" name="postcode" align="LEFT"  size="4" value="<?php echo $postcode_new; ?>"></input><br><br></td></tr>

                                <TR><td><br>Member Type: <br><?php include( dirname(__FILE__) . '/get_memtype.php'); ?></td></tr>
                                <tr><td width="35%">Email:<br>
                                        <input type="Text" name="emailaddress" align="LEFT"  size="35" value="<?php echo $email_new; ?>"></input></td></tr>
                                <tr> <td width="35%">Home Phone:<br>
                                        <input type="Text" name="phone" align="LEFT"  size="35" value="<?php echo $phone_new; ?>"></input><br></td></tr>
                                <tr> <td width="35%">Skills:<br>
                                        <input type="Text" name="skills2" align="LEFT"  size="50" value="<?php echo $skills2_new; ?>"></input><br></td></tr>
                                <tr> <td width="35%">How did you find us?:<br>
                                        <input type="Text" name="discoverytype" align="LEFT"  size="50" value="<?php echo $discovery_new; ?>"></input><br></td></tr>
                                <tr> <td width="35%">Notes:<br>
                                        <input type="Text" name="notes" align="LEFT"  size="50" value="<?php echo $notes_new; ?>"></input><br></td></tr>
                                <tr>
                                    <td><br>Roster Pref 1: <br><?php include( dirname(__FILE__) . '/get_rostertype1.php'); ?></td>
                                    <td><br>Roster Pref 2: <br><?php include( dirname(__FILE__) . '/get_rostertype2.php'); ?></td>
                                    <td><br>Roster Pref 3: <br><?php include( dirname(__FILE__) . '/get_rostertype3.php'); ?></td></tr>
                                 <tr><td><label>Roster Duty Days?: </label><br>
                                    <textarea id="rosternotes" name="rosternotes" rows="2" cols="30"><?php echo $rosternotes; ?></textarea></td></tr>


                                </tr>

                            </table>
                        </td></tr>
                </table>

                <input type="hidden" name="id" value="<?php echo $newmemid; ?>"> 

            </div>
        </form>
    </div>
</body>




