<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../../mibase_check_login.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../../header.php'); ?> 
    </head>
    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../../menu.php');
            include( dirname(__FILE__) . '/new_nationalityid.php');
            include( dirname(__FILE__) . '/new_form_nationality.php');
            //include( dirname(__FILE__) . '/../../../connect.php');

            if (isset($_POST['submit'])) {
                //$toyid = sprintf("%02s", $toyid);

                $nationality = pg_escape_string($_POST['nationality']);

                $query_new = "INSERT INTO nationality (id, nationality)
         VALUES ({$newnationalityid}, 
        '{$nationality}')";
         $conn = pg_connect($_SESSION['connect_str']);

                $result_new = pg_Exec($conn, $query_new);


                if (!$result_new) {
                    echo "An INSERT query error occurred.\n";
                    echo $query_new;
                    //echo $connection_str;
                    exit;
                } else {

                    include( dirname(__FILE__) . '/nationalities.php');
                }
            } else {
                include( dirname(__FILE__) . '/nationalities.php');
            }
            ?>
        </div>
    </body>
</html>