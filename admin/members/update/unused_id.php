<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');


$query_unused = "select * from generate_series(" . $newmemid . ", (SELECT MAX(id) FROM borwrs)) as unused
  where unused not in (select id from borwrs);";
//echo $query_unused;

$result = pg_Exec($conn, $query_unused);
//echo $connection_str;

$numrows_count = pg_numrows($result);
$numrows = 20;


if ($numrows_count > 0) {

    $result_txt = '<br>Lowest unused 20 numbers from ' . $newmemid . ': <font color="blue">';

    for ($ri = 0; $ri < $numrows; $ri++) {

        $row = pg_fetch_array($result, $ri);

        $toyid = $row["unused"];

        if (member_exits($toyid) == 0 && !($row["unused"] == null)) {
            $result_txt .= $toyid . '  ';
        }
    }
}
$result_txt .= '</font><br>';
echo $result_txt;
echo $result[0];



?>
