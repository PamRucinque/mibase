<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>

<script type="text/javascript" src="../../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

<script type="text/javascript">
    $(function() {
        var pickerOpts = {
            dateFormat: "yy-m-d",
            showOtherMonths: true

        };
        $("#dob").datepicker(pickerOpts);
    });

</script>
<?php 

?>
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'delete_mem.php?borid=' . $_GET['borid']; ?>">

    <div id="form" style="background-color:lightgray;" align="left" width="30%">

        <table align="top"><tr>

                <td>Password:
                    <input type="Text" name="password" id="password" align="LEFT"  size="15" value="" required></input><br></td>
                <td align="right"><input id="saveForm" class="button1_red"  type="submit" name="submit" value="<?php echo 'Delete Member ' . $_SESSION['borid']; ?>" /></td>

            </tr>
            <input type="hidden" name="borid" value="<?php echo $_GET['borid']; ?>"> 
        </table>
    </div>
</form></p>

