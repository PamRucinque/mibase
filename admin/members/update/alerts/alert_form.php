<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../../mibase_check_login.php');
?>
<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "yy-mm-dd",
            showOtherMonths: true

        };
        $("#date_alert").datepicker(pickerOpts);
    });

</script>
<table bgcolor="#CCFF99" width ="90%">
    <tr><td colspan = "3"></td></tr>
    <tr>

        <td width ="5%"></td>
        <td valign ="top">

            <form  id="alert_part" method="post" action="" width="100%">
                <table><tr><td>Date</td><td>Type Alert/Event</td><tr>
                    <tr><td><input type="text" name="date_alert" id ="date_alert" align="LEFT" size="10px" value="<?php echo date('Y-m-d'); ?>"  style="background-color:#F3F781;"></input></td>
                        <td><?php include( dirname(__FILE__) . '/alert_type.php'); ?></td>
                        <td><input id="submit_alert_member" class="button_small_yellow"  type="submit" name="submit_alert_member" value="Save Event" /></td>
                    </tr></table>

            </form> 
            <?php //echo $table_parts; ?>
        </td>
        <td width="10px"></td>

    </tr></table>

