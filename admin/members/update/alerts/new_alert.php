<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../../mibase_check_login.php');


include( dirname(__FILE__) . '/new_eventid.php');

$alert_type = $_POST['alert_type'];
include( dirname(__FILE__) . '/get_event_type.php');

if (substr($alert_type, 0,3) == 'UD_'){
    $alert_type = 'user_alert';
}
if ($alert_type == 'Levy Roster'){
    include( dirname(__FILE__) . '/../../roster/credit_roster.php');
}
$query = "INSERT INTO event (id, memberid, typeevent, event_date,amount, nohours,description,alertuser)
         VALUES ({$neweventid}, 
        {$_SESSION['borid']}, 
        '{$alert_type}',
         '{$_POST['date_alert']}',
          {$type_amount},
          {$type_hrs},
        '{$description}', TRUE)";

//echo $query;
$result_alert = pg_Exec($conn, $query);


if (!$result_alert) {
    echo "An INSERT query error occurred.\n";
    echo $query;
    //echo 'type quesry:' . $query_type;
    //echo $connection_str;
    exit;
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

