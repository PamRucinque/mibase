<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../../mibase_check_login.php');
//include( dirname(__FILE__) . '/../../connect.php');
//include( dirname(__FILE__) . '/children/new_form.php');
$query = "SELECT * from children where id = " . $_SESSION['borid'] . " ORDER by d_o_b";
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);
$children_txt = '';



for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);

    
    $notes = $row['notes'];
    $alert_txt = null;
    if ($row['alert'] == 't') {
        $alert_txt .= 'Yes';
    } else {
        $alert_txt .= 'No';
    }

    $children_txt .= $row['child_name'] . ',';
}


if ($numrows == 0) {
    $children_txt = '';
} else {
    if ($numrows == 1) {
        $children_txt = rtrim($children_txt, ",");
        //$children_txt = str_replace(',', ' and ', $children_txt);
        $children_txt = ' and ' . $children_txt;
    } else {
        $children_txt = rtrim($children_txt, ",");
        $search = ',';
        $replace = ' and ';
        $children_txt = strrev(implode(strrev($replace), explode($search, strrev($children_txt), 2)));
        $children_txt = ', ' . $children_txt;
    }
}



pg_close($conn);
?>

</body>


