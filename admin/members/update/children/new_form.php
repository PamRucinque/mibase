<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../../mibase_check_login.php');
?>
<script type="text/javascript">
            $(function () {
        var pickerOpts = {
            dateFormat: "d MM yy",
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "1990:+nn"

        };
        $("#dob").datepicker(pickerOpts);
    });

        </script>
<table bgcolor="#CCFF99" width ="90%">
    <tr><td colspan = "3"></td></tr>
    <tr>

        <td width ="5%"></td>
        <td valign ="top">

            <form  id="alert_part" method="post" action="" width="100%">
                <table><tr><td>dob</td><td>First name -- Surname</td><td>M or F</td></tr>
                    <tr><td><input type="text" name="dob" id ="dob" align="LEFT" size="10px" value=""  style="background-color:#F3F781;"></input></td>
                        <td><input type="Text" name="child_name" align="LEFT"  size="8px" value="" style="background-color:#F3F781;"></input>
                         <input type="Text" name="csurname" id="csurname" align="LEFT"  size="15px" value="<?php echo $surname; ?>" style="background-color:#F3F781;"></input></td>
                <td><select id="sex" name="sex"   style="background-color:#F3F781;">
                            <option value='F' selected="selected">F</option>
                            <option value="M" >M</option>
                            <option value="F" >F</option>
                            <option value="X" >X</option>
                    </select></td>
                    <td><input id="submit_child" class="button_small_yellow"  type="submit" name="submit_child" value="Save New Child" /></td>
                </tr></table>
            </form> 
          
        </td>
        <td width="10px"></td>

    </tr></table>

