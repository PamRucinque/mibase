<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');

include( dirname(__FILE__) . '/children/new_childid.php');
$sex = $_POST['sex'];
$dob = $_POST['dob'];
$child_name = $_POST['child_name'];
$surname = $_POST['csurname'];
if (($dob == '') || ($dob == null)) {
    $dob = '1900-1-1';
}

$query = "INSERT INTO children (childid, id, child_name, surname, sex, d_o_b)
         VALUES ({$newchildid}, 
        {$_SESSION['borid']}, 
        '{$child_name}',
         '{$surname}',
         '{$sex}',
        '{$dob}')";


//echo $query;
$result_alert = pg_Exec($conn, $query);


if (!$result_alert) {
    echo "An INSERT query error occurred.\n";
    echo $query;
    //echo $connection_str;
    exit;
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

