<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../../mibase_check_login.php');
//include( dirname(__FILE__) . '/../../connect.php');
include( dirname(__FILE__) . '/new_form.php');
$query = "SELECT * from children where id = " . $_SESSION['borid'] . " ORDER by d_o_b";
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);
$children_txt = '';


if ($numrows > 0) {


    echo '<table border="1" width="90%" style="border-collapse:collapse; border-color:grey">';
//echo '<tr><td>id</td><td>Date</td><td>Firstname</td><td>Surname</td><td>Gender</td><td>mem id</td><td></td> <td></td><tr>';
} else {
    echo '<table>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);

    
    $notes = $row['notes'];
    $format_dob = substr($row['d_o_b'], 8, 2) . '-' . substr($row['d_o_b'], 5, 2) . '-' . substr($row['d_o_b'], 0, 4);
    $alert_txt = null;
    if ($row['alert'] == 't') {
        $alert_txt .= 'Yes';
    } else {
        $alert_txt .= 'No';
    }

    echo '<tr>';
    echo '<td width="30px" align="center">' . $row['childid'] . '</td>';
    echo '<td width="80px">' . $format_dob . '</td>';
    echo '<td width="100px" align="left">' . $row['child_name'];
    if ($notes != '') {
        echo '<br><font color="red">' . $notes . '</font></td>';
    } else {
        echo '</td>';
    }

    echo '<td width="100px" align="left">' . $row['surname'] . '</td>';
//echo '<td width="20" align="center">' . $alert_txt . '</td>';
    echo '<td width="50" align="center">' . $row['sex'] . '</td>';
    echo '<td width="50" align="center">' . $row['id'] . '</td>';
    $ref = 'edit_child.php?childid=' . $row['childid'];
    $ref2 = 'children/delete_child.php?childid=' . $row['childid'];
    echo "<td width='50'><a class ='button_small_red' href='" . $ref . "'>Edit</a></td>";
    echo "<td width='50'><a class ='button_small_red' href='" . $ref2 . "'>Delete</a></td>";
//echo '<td width="50" align="center">' . $download2 . '</td>';
//echo '<td width="100">' . $row['man_html'] . '</td>';
    echo '</tr>';
    $children_txt = $row['child_name'] . ', ';
}
echo '<tr height="0"></tr>';
echo '</table><br>';
if ($numrows == 0) {
    $children_txt = '';
} else {
    $children_txt = ' and ' . $children_txt;
    $subject = 'bourbon, scotch, beer';
    $search = ',';
    $replace = ', and';
    $children_txt = strrev(implode(strrev($replace), explode($search, strrev($children_txt), 2)));
}



pg_close($conn);
?>

</body>


