<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');
$returns_txt = '';

$query = "SELECT parts.*,
toys.toyname as toyname 
from parts 
left join toys on toys.idcat = parts.itemno
where borcode = " . $_SESSION["borid"] . " and type = 'Missing' order by datepart desc";
$trans = pg_exec($conn, $query);
$numrows = pg_numrows($trans);
$total = 0;
//echo $query;

if ($numrows > 0) {
    $returns_txt .= '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey">';
    $returns_txt .= '<tr><td>Id</td><td>Date</td><td>Toy#</td><td>Toyname</td><td>Part</td></tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($trans, $ri);
    $total = $total + 1;
    //$weekday = date('l', strtotime($row['date_roster']));
    $part_id = $row['id'];
    $part_borid = $row['borcode'];
    $toyname = $row['toyname'];
    $idcat = $row['itemno'];
    $description = $row['description'];
    $format_date = substr($row['datepart'], 8, 2) . '-' . substr($row['datepart'], 5, 2) . '-' . substr($row['datepart'], 0, 4);

    //<a class="button_menu" href="../../toys/update/toy_detail.php">Toy</a>
    $returns_txt .= '<td width="20px">' . $part_id . '</td>';
    $returns_txt .= '<td width="30px">' . $format_date . '</td>';

    //echo '<td width="30" align="left"><a class="button_small" href="../../admin/toys/update/toy_detail.php?idcat=' . $trans_idcat . '">' . $trans_idcat . '</a></td>';
    $returns_txt .= '<td width="30px" align="left"><a class="button_small" href="../../toys/update/toy_detail.php?t=' . $idcat . '">' . $idcat . '</a></td>';

    $returns_txt .= '<td width="110px">' . $toyname . '</td>';
    //$returns_txt .=  '<td width="30px" align="left"><a class="button_small_red" href="returns.php?b=' . $trans_borid . '">' . $trans_borid . '</a></td>';
    //echo '<td width="150" align="left">' . $trans_bornmame . '</td>';
    //$returns_txt .=  '<td width="180" align="left">' . $trans_bornmame . '</td>';
    $returns_txt .= '<td width="90px" align="left">' . $description . '</td>';
    $returns_txt .= '</tr>';
}
if ($numrows > 0) {
    $returns_txt .= '</table><br>';
}

pg_close($conn);
if ($numrows > 0) {
    echo '<br><strong>Missing Pieces. <font color="blue">Total: ' . $total . '</font></strong><br>';
    echo $returns_txt;
}
?>

</body>


