<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>



    <body id="main_body" >
        <div id="form_container">
            <?php
            if (!session_id()) {
                session_start();
            }
            include( dirname(__FILE__) . '/../../menu.php');
            include( dirname(__FILE__) . '/get_child.php');
            //include( dirname(__FILE__) . '/toy_detail.php');

            if (isset($_POST['submit'])) {
                //include( dirname(__FILE__) . '/../../connect.php');
                
                
                

                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

                $query_edit = "UPDATE children SET
                    child_name = ?,
                    surname = ?,
                    d_o_b = ?,
                    sex = ?,
                    notes = ?
                    WHERE childid= ?;";
                //create the array of data to pass into the prepared stament
                $sth = $pdo->prepare($query_edit);
                $array = array($_POST['child_name'], $_POST['surname'], $_POST['dob'],
                    $_POST['sex'], $_POST['notes'], $_POST['childid']);
                $sth->execute($array);
                $stherr = $sth->errorInfo();


                if ($stherr[0] != '00000') {
                    echo "An UPDATE query error occurred.\n";
                    //echo $query_edit;
                    echo $connect_pdo;
                    echo 'Error ' . $stherr[0] . '<br>';
                    echo 'Error ' . $stherr[1] . '<br>';
                    echo 'Error ' . $stherr[2] . '<br>';
                    exit;
                } else {
                    $edit_url = 'member_detail.php?id=' . $_SESSION['borid'];
                    echo "<br>The record was successfully saved and the ID is:" . $_POST['childid'] . "<br><br>";
                    echo '<a class="button1_red" href="../members.php">Member List</a>';
                    echo '<a class="button1_red" href="' . $edit_url . '">Member Details</a>';
                    //include( dirname(__FILE__) . '/member_detail_print.php');

                    $redirect = 'Location:member_detail.php?id=' . $_SESSION['borid'];
                    //header($redirect);
                }
            } else {
                include( dirname(__FILE__) . '/get_child.php');
                include( dirname(__FILE__) . '/edit_form_child.php');
            }
            ?>
        </div>
    </body>
</html>