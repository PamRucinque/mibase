<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');
//include( dirname(__FILE__) . '/get_type.php');
//include( dirname(__FILE__) . '/get_settings.php');

if (!isset($_SESSION['borid'])) {
    if (isset($_GET['borid'])) {
        $_SESSION['borid'] = $_GET['borid'];
    }
}
$query = "SELECT borwrs.*,
membertype.expiryperiod as expiryperiod 
 from borwrs 
left join membertype on membertype.membertype = borwrs.membertype"
        . " WHERE borwrs.id = '" . $_SESSION["borid"] . "'";
$conn = pg_connect($_SESSION['connect_str']);
$result_member = pg_Exec($conn, $query);
$numrows = pg_numrows($result_member);


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_member, $ri);

    $joined = $row['datejoined'];
    $expired = $row['expired'];
    $date_application = $row['date_application'];
    $helmet = $row['helmet'];
    $helmet_date = $row['conduct_date'];
    $conduct = $row['conduct'];
    $conduct_date = $row['helmet_date'];
    $expiryperiod = $row['expiryperiod'];
    $expiry_str = $expired . ' -' . $expiryperiod . ' months';
    $start_member = date('Y-m-d', strtotime($expiry_str));
    $format_start_member = substr($start_member, 8, 2) . '-' . substr($start_member, 5, 2) . '-' . substr($start_member, 0, 4);
    $marital_status = $row['marital_status'];
    $created = $row['created'];
    $renewed = $row['renewed'];
    $format_expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
    $format_joined = substr($row['datejoined'], 8, 2) . '-' . substr($row['datejoined'], 5, 2) . '-' . substr($row['datejoined'], 0, 4);
    $format_renewed = substr($row['renewed'], 8, 2) . '-' . substr($row['renewed'], 5, 2) . '-' . substr($row['renewed'], 0, 4);

    $borid = $row["id"];
    $firstname = $row["firstname"];
    $membertype = $row["membertype"];
    $pwd = $row['pwd'];
    $username = $row['rostertype5'];
    $surname = $row["surname"];
    $partnersname = $row["partnersname"];
    $partnerssurname = $row['partnerssurname'];
    $email = $row["emailaddress"];
    $email2 = $row["email2"];
    $phone = $row["phone"];
    $postcode = $row['postcode'];
    $mobile1 = $row["phone2"];
    $mobile2 = $row['mobile1'];
    $member_status = $row["member_status"];
    $address = $row['address'];
    $address2 = $row['address2'];
    $suburb = $row['suburb'];
    $city = $row['city'];
    $state = $row['state'];
    $discovery = $row['discoverytype'];
    $rostertype = $row['rostertype'];
    $rostertype2 = $row['rostertype2'];
    $rostertype3 = $row['rostertype3'];
    $rostertype4 = $row['rostertype4'];
    $license = $row['id_license'];
    //$alert_mem = $row['specialneeds'];
    $notes = $row['notes'];
    $user1 = $row['user1'];
    $skills = $row['skills'];
    $skills2 = $row['skills2'];
    $alert_mem = $row['alert'];
    $member_update = $row['member_update'];
    //$history = $row['history'];
    $changedby = $row['changedby'];
    $agree = $row['agree'];
    $photos = $row['photos'];
    $location = $row['location'];
    $wwc = $row['wwc'];
    $levy = $row['levy'];
    $rego_id = $row['rego_id'];
    if ($partnersname == null || $partnerssurname == null) {
        $longname = $firstname . ' ' . $surname;
    } else {
        $longname = $firstname . ' ' . $surname . ' & ' . $partnersname . ' ' . $partnerssurname;
    }


    $email_link = '<a class="button_small_red" href="mailto:' . $email . '">send email</a>';
    $email_link2 = '<a class="button_small_red" href="mailto:' . $email2 . '">send email</a>';
}

$_SESSION['borid'] = $borid;
?>
