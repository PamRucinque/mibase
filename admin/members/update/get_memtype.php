<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');
$query = "SELECT id, membertype, description FROM membertype ORDER by membertype ASC;";
$result = pg_Exec($conn, $query);

//choose the database

//get data from database
echo "<select required name='membertype' id='membertype' style='width: 80%;background-color: white; border-color: red;'>\n";


$numrows = pg_numrows($result);
//echo '<option value="" selected="selected"></option>';
echo "<option value='" . $membertype . "' selected='selected'>" . $membertype . "</option>";

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    echo "<option value='{$row['membertype']}'>{$row['membertype']} : {$row['description']}
    </option>\n";
}

echo "</select>\n";


?>

