<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');
$show_password = $_SESSION['settings']['show_password'];
$nationality_label = $_SESSION['settings']['nationality_label'];
$roster = $_SESSION['settings']['roster'];
$toy_holds = $_SESSION['settings']['toy_holds'];


if (isset($_GET['borid'])) {
    $id = $_GET['borid'];
}

//echo "id:" . $id;
$renew_member_email = '';
include 'get_member.php';

//include( dirname(__FILE__) . '/get_settings.php');
//include( dirname(__FILE__) . '/../../get_settings.php');
include( dirname(__FILE__) . '/data/parts.php');

$logo = $_SESSION['logo'];



echo '<table width="100%"><tr><td width="70%">';
echo '<h1><font color="blue">' . $borid . ': ' . $longname . '</h1></td><td>';
echo '<td><a href="edit.php?borid=' . $_SESSION['borid'] . '" class="button1_red">Edit</a>';
if (($new_member_email == 'Yes') && ($email != '')) {
    echo '<a href="send_email_new.php?borid=' . $_SESSION['borid'] . '" class="button1_green">Send Welcome Email</a>';
}
if (($renew_member_email == 'Yes') && ($email != '')) {
    echo '<a href="send_email_renew.php?borid=' . $_SESSION['borid'] . '" class="button1_yellow">Send Renew Email</a>';
}

echo '<a href="delete_mem.php?borid=' . $_SESSION['borid'] . '" class="button1_logout">Delete</a>';
echo '</td></tr></table>';

//echo 'user' . $_SESSION['libraryname'];
$member_txt = '';
$member_txt .= '<br><strong>Contact 1: </strong> ' . $firstname . ' ' . $surname . ' ';
if (!$mobile1 == null) {
    $member_txt .= '<strong> mob: </strong> ' . ' ' . $mobile1 . '<br>';
} else {
    $member_txt .= '<br>';
}
if (!$partnersname == null) {
    $member_txt .= '<strong> Contact 2: </strong> ' . $partnersname . ' ' . $partnerssurname . ' ';
}
if (!$mobile2 == null) {
    $member_txt .= '<strong> mob: </strong> ' . $mobile2 . '<br>';
} else {
    $member_txt .= '<br>';
}
$member_txt .= '<strong>Date Created:</strong> ' . $created . ' Rego id: ' . $rego_id . '<br>';
if (!$date_application == null) {
    $member_txt .= '<strong>Date Online Application:</strong> ' . $date_application . '<br>';
}
$member_txt .= '<strong> Member status: </strong> ' . $member_status . '<br>';
$member_txt .= '<strong>Member Type: </strong> ' . $membertype . '<br>';
$member_txt .= '<strong>Address: </strong> ' . $address . ' ' . $address2 . ', ' . $suburb . ' ' . $city . ' ' . $postcode . ' ' . $state . '<br>';
if (!$email == null) {
    $member_txt .= '<strong>EMail: </strong> ' . $email . '  ' . $email_link . '<br>';
}
if (!$email2 == null) {
    $member_txt .= '<strong>EMail 2: </strong> ' . $email2 . '  ' . $email_link2 . '<br>';
}
if ($phone != '') {
    $member_txt .= '<strong>Home Phone: </strong> ' . $phone . '<br>';
}

$member_txt .= '<strong>Joined: </strong>' . $format_joined . '<br>';
$member_txt .= '<strong>Renewed: </strong>' . $format_renewed . '<br>';
$member_txt .= '<strong>Expires: </strong>' . $format_expired . '<br>';
//$member_txt .= '<strong>Start Roster: </strong>' . $start_member . '<br>';
if ($rostertype != '') {
    $member_txt .= '<strong>Roster Preferences: </strong><br>' . $rostertype . "<br>" . $rostertype2 . "<br>" . $rostertype4 . '<br>';
}
if ($selectbox == 'Yes') {
    $member_txt .= '<strong>' . $selectname . ' </strong> ' . $rostertype4 . '<br>';
}
if ($rostertype3 != '') {
    if ($nationality_label == '') {
        $nationality_label = 'LOTE: ';
    }
    $member_txt .= '<strong>' . $nationality_label . '</strong>' . $rostertype3 . "<br>";
}


if ($skills != '') {
    $member_txt .= '<strong>Skills: </strong>' . $skills . '<br>';
}
if ($skills2 != '') {
    $member_txt .= '<strong>Would like to get involved in: </strong>' . $skills2 . '<br>';
}
if ($notes != '') {
    $member_txt .= '<strong>Roster Notes: </strong>' . $notes . '<br>';
}

//$member_txt .= '<strong>Alerts: </strong>' . $alert_mem . '<br>';
if ($discovery != '') {
    $member_txt .= '<strong>Discovery: </strong>' . $discovery . '<br>';
}
if ($wwc != '') {
    $member_txt .= '<strong>Working With Children Card No#: </strong>' . $wwc . '<br>';
}

if ($license != '') {

    $member_txt .= '<strong>Licence Number / ID: </strong>' . $license . '<br>';
}
if (trim($location) != '') {
    $member_txt .= '<strong>Location: </strong>' . $location . '<br>';
}

if ($marital_status != '') {
    $member_txt .= '<strong>Marital Status: </strong>' . $marital_status . '<br>';
}
if ($alert_mem != '') {
    $member_txt .= '<strong><font color="red"> ' . $alert_mem . '</font></strong><br>';
}
if ($user1 != '') {
    $member_txt .= '<strong>' . $user1_borwrs . '</strong>' . $user1 . '<br>';
}

$member_txt .= '<strong>Username: </strong><font color="blue">' . $username . '</font><br>';
if ($show_password == 'Yes') {
    $member_txt .= '<strong>Password: <font color="blue">' . $pwd . '</font></strong><br>';
}
if ($member_update != null) {
    $member_txt .= '<strong>Checked by Member: <font color="green">' . $member_update . '</font></strong><br>';
}
if ($changedby != null) {
    $member_txt .= '<strong>Changed by User: <font color="green">' . $changedby . '</font></strong><br>';
}
if ($photos != '') {
    $member_txt .= '<strong>Permission to take photos: <font color="green">' . $photos . '</font></strong><br>';
}
if ($agree != '') {
    $member_txt .= '<strong>Agree to Terms and Conditions: <font color="green">' . $agree . '</font></strong><br>';
}
if ($helmet == 't') {
    $helmet = 'Yes';
}
if ($helmet == 'Yes') {
    $member_txt .= '<strong>Agreed to Helmet Waiver: <font color="green">' . $helmet . ' ' . $helmet_date . '</font></strong><br>';
} else {
    $member_txt .= '<strong>Agree to Helmet Waiver: <a href="update_helmet.php?borid=' . $_SESSION['borid'] . '" class="button_small_yellow">Agree</a></strong>';
}
if ($conduct == 't') {
    $conduct = 'Yes';
}
if ($conduct == 'Yes') {
    $member_txt .= '<br><strong>Agreed to Code of Conduct: <font color="green">' . $conduct . ' ' . $conduct_date . '</font></strong><br>';
} else {
    $member_txt .= '<br><strong>Agree to Code of Conduct: <a href="update_conduct.php?borid=' . $_SESSION['borid'] . '" class="button_small_yellow">Agree</a></strong>';
}
if ($levy == 't') {
    $member_txt .= '<br><strong>Agreed to Pay Levy, when signing up. Check Member Type!</strong><br>';
}

if ($email != '') {
    $member_txt .= '<br><a href="send_email_login.php?borid=' . $_SESSION['borid'] . '" class="button1_green">EMail Login To Member</a>';
}
//echo $levy;


echo '<table width="100%"><tr><td width="45%" valign="top">';


echo $member_txt;
include('../rosters/list.php');
echo $str_parts;
if ($_SESSION['settings']['member_alerts'] == 'Yes') {
    include( dirname(__FILE__) . '/alerts/alert_form.php');
    include( dirname(__FILE__) . '/alerts/alerts.php');
}
echo '</td><td valign="top" width="55%">';

include( dirname(__FILE__) . '/children/children.php');
include( dirname(__FILE__) . '/toys_onloan.php');
include( dirname(__FILE__) . '/../../member/reservations/list.php');
if ($_SESSION['settings']['roster'] == 'Yes') {
    if (isset($_SESSION['settings']['rosters_annual']) && $_SESSION['settings']['rosters_annual'] == 'Yes') {
        include( dirname(__FILE__) . '/rosters_annual.php');
    } else {
        include( dirname(__FILE__) . '/rosters.php');
        //if ($history != '') {
        //echo '<br><br>' . $history . '<br>';
        //}
    }
    include( dirname(__FILE__) . '/emails.php');
}
echo '</td><td valign="top" width="55%">';

if (($toy_holds == 'Yes') && ($_SESSION['mibase_server'] == 'Yes')) {
    include( dirname(__FILE__) . '/data/holds.php');
}

//include( dirname(__FILE__) . '/missing_pieces.php');

    echo '<tr><td colspan="2">';
    include( dirname(__FILE__) . '/reserves.php');
    include( dirname(__FILE__) . '/toys_onloan_hist.php');
    echo '</td></tr>';
    echo '</td></tr></table>';
    echo '<br>';
    echo '<br>';
    echo '<br>';
    ?>
</div>




