<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <script type="text/javascript">
        $(function () {
            var pickerOpts = {
                dateFormat: "d MM yy",
                showOtherMonths: true

            };
            $("#submitted").datepicker(pickerOpts);
            $("#closed").datepicker(pickerOpts);
        });

    </script>


</head>
<body>
    <div id="form_container">
        <?php
        include( dirname(__FILE__) . '/../../menu.php');

        include( dirname(__FILE__) . '/get_member_approve.php');
        //include( dirname(__FILE__) . '/../../connect.php');
        //include( dirname(__FILE__) . '/../../get_settings.php');

        include( dirname(__FILE__) . '/new_memid.php');
        include( dirname(__FILE__) . '/functions.php');

        if (isset($_POST['submit'])) {


            $firstname = pg_escape_string($_POST['firstname']);
            $surname = pg_escape_string($_POST['surname']);
            $notes = pg_escape_string($_POST['notes']);
            $partnersname = pg_escape_string($_POST['partnersname']);
            $partnerssurname = pg_escape_string($_POST['partnerssurname']);
            $membertype = pg_escape_string($_POST['membertype']);
            $suburb = pg_escape_string($_POST['suburb']);
            $address = pg_escape_string($_POST['address']);
            $postcode = pg_escape_string($_POST['postcode']);
            $mobile1 = pg_escape_string($_POST['mobile1']);
            $mobile2 = pg_escape_string($_POST['mobile2']);
            $email = pg_escape_string($_POST['emailaddress']);
            $phone = pg_escape_string($_POST['phone']);
            $skills2 = pg_escape_string($_POST['skills2']);
            $discoverytype = pg_escape_string($_POST['discoverytype']);
            $rostertype = $_POST['rostertype1'];
            $rostertype2 = $_POST['rostertype2'];
            $rostertype3 = $_POST['rostertype3'];
            $now = date('Y-m-d');
            $joined = $now;
            if ($settings_expired != '') {
                $expired = date('Y', strtotime('+1 year')) . "-" . trim($settings_expired);
            } else {
                $expired = date_expired($membertype, $joined);
            }


            $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
            $query = "INSERT INTO borwrs (id, firstname, surname, membertype, notes, partnersname, 
        partnerssurname, suburb, address, postcode, datejoined, renewed, expired,
        phone2, mobile1, emailaddress, phone, discoverytype, skills2, rostertype, rostertype2, rostertype3, rosternotes, member_status)
                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

            $sth = $pdo->prepare($query);

            //create the array of data to pass into the prepared stament
            $array = array($newmemid, $firstname, $surname, $membertype, $notes, $partnersname,
                $partnerssurname, $suburb, $address, $postcode, $now, $now, $expired, $mobile1, $mobile2,
                $email, $phone, $discoverytype, $skills2, $rostertype, $rostertype2, $rostertype3, $_POST['rosternotes'], 'ACTIVE');

            //execute the preparedstament
            $sth->execute($array);

            //get the error info for the statment just executed.
            //0 	SQLSTATE error code (a five characters alphanumeric identifier defined in the ANSI SQL standard).
            //1 	Driver specific error code.
            //2 	Driver specific error message.
            $stherr = $sth->errorInfo();



            if ($stherr[0] != '00000') {
                echo "An INSERT query error occurred.\n";
                echo $query;
                echo $connect_pdo;
                exit;
            } else {

                //$newid = $newid;
                $edit_url = 'member_detail.php?id=' . $_POST['id'];

                echo "<br>The record was successfully entered and the ID is:" . $_POST['id'] . "<br><br>";
                //echo 'format Toy id: ' . $format_toyid;

                echo '<a class="button1_red" href="' . $edit_url . '">View New Member</a>';
                echo '<a class="button1_red" href="../members.php">Member List</a>';



                // print $query;
                //include( dirname(__FILE__) . '/send_email.php');
                // include( dirname(__FILE__) . '/send_email_insurance.php');
            }

// Close the connection
            pg_Close($conn);
        } else {
            include( dirname(__FILE__) . '/newidcat.php');
            include( dirname(__FILE__) . '/new_form_approve.php');
        }
        ?>
    </div>
</body>
</html>