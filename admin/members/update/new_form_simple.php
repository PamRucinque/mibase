<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

$today = date('Y-m-d');
//$oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " + 365 day"));
?>

<body id="main_body">
    <script type="text/javascript" src="../../js/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
    <link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

    <script type="text/javascript">
        $(function () {
            var pickerOpts = {
                dateFormat: "d MM yy",
                showOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "2010:+nn"

            };
            $("#expired").datepicker(pickerOpts);
            $("#joined").datepicker(pickerOpts);
            $("#renewed").datepicker(pickerOpts);
        });
        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault)
                    theEvent.preventDefault();
            }
        }
        function get_postcode(str) {
            var input = str;

            if (str == "") {
                document.getElementById("postcode").innerHTML = "";
                return;
            }

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {

                document.getElementById("postcode").value = xmlhttp.responseText;

            }

            xmlhttp.open("GET", "getpostcode.php?q=" + str, true);
            xmlhttp.send();
        }
        function get_help(str) {
            var input = str;

            if (str == "") {
                document.getElementById("help_txt").innerHTML = "";
                return;
            } else {
                $str = document.getElementById("help_txt").value;
                //alert($str);
            }


            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                //var length = 100;
                //$str = trimFunction(100, $str);
                if ($str.length < 100) {
                    if ($str.length == 0) {
                        document.getElementById("help_txt").value = xmlhttp.responseText + $str;
                    } else {
                        document.getElementById("help_txt").value = xmlhttp.responseText + "," + $str;
                    }

                } else {
                    document.getElementById("help_txt").value = xmlhttp.responseText;
                }
            }

            xmlhttp.open("GET", "data/gethelp.php?q=" + str, true);
            xmlhttp.send();
        }
        function ClearFields() {

            document.getElementById("help_txt").value = "";
        }

    </script>
    <?php
    //include( dirname(__FILE__) . '/../../get_settings.php');
    if ($suburb_title == '') {
        $suburb_title = 'Suburb';
    }
    if ($city_title == '') {
        $city_title = 'City';
    }
    ?>
    <div id="form_container">


        <form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'new.php?id=' . $_POST['id']; ?>">

            <div id="form" style="background-color:lightgray;" align="left">

                <table width="100%"><tr><td>

                            <h2>Member Details:</h2></td>
                        <td align="right"><input id="saveForm" class="button1_red" type="submit" name="submit" value="Save Member" />

                        </td></tr></table>
                <table>
                    <tr><td width="25%">Contact 1 - First Name:<br>
                            <input STYLE="background-color: yellow;"  type="Text" name="firstname" align="LEFT" required="Yes" size="25" value=""></input></td>
                        <td width="30%">Surname:<br>
                            <input STYLE="background-color: yellow;" type="Text" name="surname" align="LEFT" required="Yes" size="30" value=""></input><br></td>

                        <td>Mobile:<br>
                            <input STYLE="background-color: yellow;" type="Text" name="mobile1" align="LEFT" size="20" value=""></input><br></td>
                        <td width="35%">Email:<br>
                            <?php
                            if ($_SESSION['library_code'] != 'demo') {
                                echo '<input STYLE="background-color: yellow;" type="Text" name="emailaddress" align="LEFT"  size="35" value=""></input>';
                            }
                            ?>
                            </td>
                    <tr><td width="25%">Contact 2 - First Name:<br>
                            <input type="Text" name="partnersname" align="LEFT" size="25" value=""></input></td>
                        <td width="30%">Surname:<br>
                            <input type="Text" name="partnerssurname" align="LEFT"  size="30" value=""</input><br></td>

                        <td width="25%">Mobile:<br>
                            <input type="Text" name="mobile2" align="LEFT"  size="20" value=""></input><br></td>
                        <td width="35%" style="padding-right: 10px;">Email:<br>
                            <input type="Text" name="email2" align="LEFT"  size="35" value=""></input></td>

                    </tr>
                </table>

                <table width="100%">
                    <tr><td style="padding-right: 20px;">Address:<br>
                            <input type="Text" name="address" align="LEFT"  size="55" value=""></input></td>

                        <?php
                        if ($suburb_title == 'City') {
                            
                        } else {
                            echo '<td>';
                            echo $suburb_title . '<br>';
                            include( dirname(__FILE__) . '/get_city.php');
                            echo '</td>';
                        }
                        ?>

                        <td  align='top' width="10%"><br>Pcode<br>
                            <input type="Text" id="postcode" name="postcode" align="LEFT"  size="4" value=""></input><br><br></td>
                        <td  align='top' width="10%"><br>State:<br>
                            <input type="Text" name="state" align="LEFT"  size="4" value="VIC"></input><br><br></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr><td style="padding-right: 20px;"><br>Member Type: <br><?php include( dirname(__FILE__) . '/get_memtype.php'); ?></td>
                        <td style="padding-right: 20px;"><br>Source: <br><?php include( dirname(__FILE__) . '/get_source.php'); ?><br></td>
                        <td><br>Language other than English:<br> <?php include( dirname(__FILE__) . '/get_rostertype3.php'); ?>
                        </td></tr></table>
                <table>

                    <tr><td width="60%"><label><br>Notes: </label><br>
                            <textarea id="notes" name="notes" rows="3" cols="35"></textarea></td>
                        <td><label><br>Alerts: </label><br>
                            <textarea id="alertmem" name="alertmem" rows="3" cols="35"></textarea></td>

                    </tr>
                    <tr><td><label><br>Skills / Occupation: </label><br>
                            <textarea id="skills" name="skills" rows="3" cols="35"></textarea></td>
                        <td><label><br>How can you help? (select more than one) </label><br>
<?php include( dirname(__FILE__) . '/data/get_volunteer.php'); ?>
                            <br><br><textarea id="help_txt" name="help_txt" rows="5" cols="60" maxlength="100" readonly></textarea>
                            <br><button type="button" onclick="ClearFields();">Clear</button>
                        </td>
                    </tr>
                </table>
                </td></tr>
                </table>

                <input type="hidden" name="id" value="<?php echo $newmemid; ?>"> 
            </div>
            </body>




