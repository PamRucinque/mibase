<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');
$query = "SELECT * from toys WHERE idcat = '" . $_GET["idcat"] . "'";
$result_toy = pg_Exec($conn, $query);
$numrows = pg_numrows($result_toy);
$status_txt = Null;


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_toy, $ri);


    $toy_id = $row['id'];
    $idcat = $row['idcat'];
    $desc1 = $row['desc1'];
    $desc2 = $row['desc2'];
    $category = $row['category'];
    $reservecode = $row['reservecode'];
    $toyname = $row['toyname'];
    $rent = $row['rent'];
    $age = $row['age'];
    $datestocktake = $row['datestocktake'];
    $manufacturer = $row['manufacturer'];
    $discountcost = $row['discountcost'];
    $status = $row['status'];
    $cost = $row['cost'];
    $supplier = $row['supplier'];
    $comments = $row['comments'];
    $returndateperiod = $row['returndateperiod'];
    $returndate = $row['returndate'];
    $no_pieces = $row['no_pieces'];
    $date_purchase = $row['date_purchase'];
    $sponsorname = $row['sponsorname'];
    $warnings = $row['warnings'];
    $user1 = $row['user1'];
    //$date_entered = date_format($row['date_entered'], 'd/m/y');
    $alert = $row['alert'];
    $toy_status = $row['toy_status'];
    $stype = $row['stype'];
    $units = $row['units'];
    $borrower = $row['borrower'];
    $lockdate = $row['lockdate'];
    $created = $row['created'];
    $modified = $row['modified'];

    pg_FreeResult($result_toy);
// Close the connection
    pg_Close($conn);
    
    $_SESSION['idcat'] = $idcat;

    if ($status == 't') {
        $status_txt .= 'ON LOAN';
    } else {
        $status_txt .= 'IN LIBRARY';
    }
}


//echo 'Type Claim: ' . $type_claim;
//$date_submitted = $date_submitted[weekday]. $date_submitted[month] . $date_submitted[mday] . $date_submitted[year];
?>