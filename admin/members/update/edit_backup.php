<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>

    <body id="main_body" >
        <div id="form_container">
            <?php include( dirname(__FILE__) . '/../../menu.php'); ?>


            <?php
            include( dirname(__FILE__) . '/functions.php');
            //include( dirname(__FILE__) . '/../../connect.php');
            //include( dirname(__FILE__) . '/../../get_settings.php');
            //include( dirname(__FILE__) . '/get_settings.php');

            $today = date('Y-m-d');
            $oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " + 365 day"));
            $postcode = clean($_POST['postcode']);

            $user1 = clean($_POST['user1']);
            $user1 = trim($user1);
            //include( dirname(__FILE__) . '/toy_detail.php');

            if (isset($_POST['submit'])) {


                $now = date('Y-m-d H:i:s');

                if ($_POST['expired'] == '') {
                    $expired = $oneYearOn;
                } else {
                    $expired = $_POST['expired'];
                }
                if ($_POST['renewed'] == '') {
                    $renewed = $today;
                } else {
                    $renewed = $_POST['renewed'];
                }
                if ($_POST['joined'] == '') {
                    $joined = date('Y-m-d H:i:s');
                } else {
                    $joined = $_POST['joined'];
                }

                if ($_POST['age'] == '') {
                    $age = '1900-01-01';
                } else {
                    $age = $_POST['age'];
                }
                $firstname = clean($_POST['firstname']);
                $surname = clean($_POST['surname']);
                if ($suburb_title != 'City') {
                    $city = $_POST['city'];
                }
                if ($_POST['member_status'] == 'LOCKED') {
                    $lockdate = date('Y-m-d');
                } else {
                    $lockdate = $now;
                }
                if ($_POST['rostertype4'] == '') {
                    $_POST['rostertype4'] = $_POST['select'];
                }

                //start
                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $query_edit = "UPDATE borwrs SET
                    firstname=?,
                    surname=?,
                    phone=?,
                    partnersname=?,
                    phone2=?,
                    mobile1=?,
                    partnerssurname =?,
                    address =?,
                    address2 =?,
                    emailaddress =?,
                    email2 =?,
                    membertype =?,
                    notes =?,
                    suburb =?,
                    city =?,
                    state =?,
                    rostertype =?,
                    rostertype2 =?,
                    rostertype3 =?,
                    rostertype4 =?,
                    discoverytype =?,
                    member_status =?,
                    alert =?,
                    skills =?,
                    skills2 =?,
                    location =?,
                    renewed =?,
                    expired =?,
                    rostertype5 =?,
                    pwd=?,
                    id_license=?,
                    datejoined =?,
                    postcode =?,
                    age =?,
                    marital_status=?,
                    modified =?,
                    lockdate =?
                    WHERE id=?;";

                //create the array of data to pass into the prepared stament
                $sth = $pdo->prepare($query_edit);
                $array = array($firstname, $surname, $_POST['phone'], $_POST['partnersname'], $_POST['mobile1'],
                    $_POST['mobile2'], $_POST['partnerssurname'], $_POST['address'], $_POST['address2'], $_POST['emailaddress'],
                    $_POST['email2'], $_POST['membertype'], $_POST['notes'], $_POST['suburb'], $city, $_POST['state'],
                    $_POST['rostertype1'], $_POST['rostertype2'], $_POST['rostertype3'], $_POST['rostertype4'], $_POST['discovery'], $_POST['member_status'],
                    $_POST['alertmem'], $_POST['skills'], $_POST['help'], $user1, $renewed, $expired, $_POST['username'], $_POST['password'], $_POST['license'], $joined, $postcode,
                    $age, $_POST['marital_status'], $now, $lockdate, $_POST['id']);
                $sth->execute($array);
                $stherr = $sth->errorInfo();


                if ($stherr[0] != '00000') {
                    echo "An UPDATE query error occurred.\n";
                    echo $query_edit;
                    echo $connect_pdo;
                } else {
                    //echo "record saved";
                    $edit_url = 'member_detail.php?id=' . $_SESSION['borid'];

                    echo "<br>The record was successfully saved and the ID is:" . $_SESSION['borid'] . "<br><br>";
                    echo '<a class="button1" href="../update/member_detail.php?borid=' . $_SESSION['borid'] . '">OK </a>';
                }
            }


            // include( dirname(__FILE__) . '/get_toy.php');
            if (isset($_POST['renew'])) {
        
                    date_default_timezone_set($_SESSION['settings']['timezone']);
            
                $now = date('Y-m-d H:i:s');
                $today = date('Y-m-d');
                if ($_POST['expired'] == NULL) {
                    $_POST['expired'] = $today;
                    $msg_blankdate = 'The Expiry date was left blank, so it has been set to the default date: ' . $today . '.';
                }
                $membertype = $_POST['membertype'];
                $expired = date_expired($membertype, $_POST['expired']);
                //$expired = $oneYearOn;
                $renewed = $today;

                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $query_renew = "UPDATE borwrs SET
                    membertype =?,
                    renewed =?,
                    expired =?,
                    modified =?     
                    WHERE id=?;";

                //create the array of data to pass into the prepared stament
                $sth = $pdo->prepare($query_renew);
                $array = array($_POST['membertype'], $renewed, $expired,
                    $now, $_POST['id']);
                $sth->execute($array);
                $stherr = $sth->errorInfo();
                if ($stherr[0] != '00000') {
                    echo "An UPDATE query error occurred.\n";
                    echo $query_renew;
                    echo $connect_pdo;
                } else {


                    $type = get_type($membertype);
                    $amount = $type['renewal'];
                    $description = 'Membership fees';
                    $category = $type['description'];
                    $desc_levy = 'Membership Levy';
                    if ($category == '') {
                        $category = $membertype;
                    }
                    $levy = $type['levy'];
                    if ($category == '') {
                        $category = $membertype;
                    }

                    $longname = $_POST['firstname'] . ' ' . $_POST['surname'];
                    $borname = str_replace("'", "`", $_POST['borname']);
                    if ($automatic_debit_off != 'Yes') {
                        //include( dirname(__FILE__) . '/new_paymentid.php');
                        $payment_str = '<br><font color="blue">' . add_to_journal($today, $_POST['id'], 0, $longname, $description, $category, $amount, 'DR', 'SUBS');
                        if ($levy != 0) {
                            $str = add_to_journal($today, $_POST['id'], 0, $longname, $desc_levy, $category, $levy, 'DR', 'SUBS');
                            $payment_str .= '<br>The Membership Levy of ' . $levy . ' has been added.';
                        }

                        $str_result = $payment_str;
                    } else {
                        $str_result = "<br><font color='blue'><br>";
                    }


                    $edit_url = 'member_detail.php?borid=' . $_POST['id'];
                    $_SESSION['borid'] = $_POST['id'];

                    $str_result .= "<br>This member has been renewed and the ID is:" . $_POST['id'] . "<br>";
                    $str_result .= "The Expired Date has been updated to: " . $expired . "<br></font>";
                    $str_result .= "<font color='red'>" . $msg_blankdate . "</font><br>";
                    $str_result .= '<a class="button1" href="../update/member_detail.php?borid=' . $_SESSION['borid'] . '">OK </a>';

                    if ($renew_member_email == 'Yes') {
                        include( dirname(__FILE__) . '/get_member.php');
                    }
                    echo $str_result;
                }


                //include( dirname(__FILE__) . '/member_detail_print.php');
            }
            if (!isset($_POST['submit']) && !isset($_POST['renew'])) {

                if ($simple_new_member == 'Yes') {
                    include( dirname(__FILE__) . '/edit_form_simple.php');
                    //include( dirname(__FILE__) . '/edit_form.php');
                } else {
                    include( dirname(__FILE__) . '/edit_form.php');
                }
            }
            ?>
        </div>
    </body>
</html>