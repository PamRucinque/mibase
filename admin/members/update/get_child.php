<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
$alert_txt = '';
if (isset($_GET['childid'])){
    $_SESSION['childid'] = $_GET['childid'];
}
//include( dirname(__FILE__) . '/../../connect.php');
$conn = pg_connect($_SESSION['connect_str']);
$query = "SELECT * from children WHERE childid = " . $_SESSION['childid'];

$result_part = pg_Exec($conn, $query);
$numrows = pg_numrows($result_part);
$status_txt = Null;

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_part, $ri);


    $childid = $row['childid'];
    $dob = $row['d_o_b'];
    $sex = $row['sex'];
    $child_surname = $row['surname'];
    $child_name = $row['child_name'];
    $notes = $row['notes'];
    $borid = $row['id'];
    $alert = $row['alert'];

    pg_FreeResult($result_part);
// Close the connection
    pg_Close($conn);

    if ($alert == 't') {
        $alert_txt .= 'YES';
    } else {
        $alert_txt .= 'NO';
    }

}


//echo 'Type Claim: ' . $type_claim;
//$date_submitted = $date_submitted[weekday]. $date_submitted[month] . $date_submitted[mday] . $date_submitted[year];
?>