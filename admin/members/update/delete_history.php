<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');


    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $query = "DELETE FROM transaction WHERE id = ?;";
//and get the statment object back
    $sth = $pdo->prepare($query);

    $array = array($_POST['id']);

//execute the preparedstament
    $sth->execute($array);
    $stherr = $sth->errorInfo();

    if ($stherr[0] != '00000') {
        echo "A DELETE query error occurred.\n";
        echo $connect_pdo;
        echo 'Error ' . $stherr[0] . '<br>';
        echo 'Error ' . $stherr[1] . '<br>';
        echo 'Error ' . $stherr[2] . '<br>';
        echo '<a class="button1" href="member_detail.php">Back to Member Details</a>';
        exit;
    }

?>
