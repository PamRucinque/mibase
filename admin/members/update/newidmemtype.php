<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

$catsort = $_SESSION['catsort'];
if ($catsort == 'Yes') {
    $query_check = "SELECT id FROM toys WHERE category = '" . $_SESSION['category'] . "' and id = " . $_SESSION['toyid'] . ";";
    $query_new = "SELECT MAX(id) as newidcat FROM toys WHERE category = '" . $_SESSION['category'] . "';";
} else {
    $query_new = "SELECT MAX(id) as newidcat FROM toys;";
    $query_check = "SELECT id FROM toys WHERE id = " . $_SESSION['toyid'] . ";";
}
//echo $query_new;

//include( dirname(__FILE__) . '/../../connect.php');
$nextval = pg_Exec($conn, $query_new);
//echo $connection_str;
$row = pg_fetch_array($nextval, 0);
$newidcat = $row['newidcat'];
$newidcat = $newidcat + 1;
if ($_SESSION['toyid'] == Null) {
    $_SESSION['toyid'] = $newidcat;
}
if ($_SESSION['toyid'] == 0) {
    $_SESSION['toyid'] = $newidcat;
}

if ($catsort == 'Yes') {
    $query_check = "SELECT id FROM toys WHERE category = '" . $_SESSION['category'] . "' and id = " . $_SESSION['toyid'] . ";";
} else {
    $query_check = "SELECT id FROM toys WHERE id = " . $_SESSION['toyid'] . ";";
}
$result_rows = pg_exec($conn, $query_check);
$numrows = pg_numrows($result_rows);

if ($category == Null) {
    $redirect = 'location: select_cat.php';
    header($redirect);
    $_SESSION['caterror'] = '<font color="red">Please Select a Category</font><br>';
}else{
    $_SESSION['caterror'] = '<font color="red"></font>';
}

  if (isset($_POST['submit_unused'])) {
      $redirect = 'location: select_cat.php';
        header($redirect);
        $_SESSION['unused'] = '<font color="red">Unused Numbers: </font>';
  }else{
      $_SESSION['unused'] = '<font color="red"></font>';
  }
      

if ($numrows == 1) {
    $redirect = 'location: select_cat.php';
    header($redirect);
    $_SESSION['toyiderror'] = '<font color="red">This number is already being used</font><br>';
    //echo 'This number is already being used';
    //echo '<br>' . $query_check;
} else {
    $_SESSION['toyiderror'] = '<font color="red"></font>';
}




    

$check_id = $_SESSION['toyid'];
//echo $check_id;
//echo $query_new;
echo '<br><h3>Next id: <font color="blue">' . $_SESSION['toyid'] . '</font> Category: <font color="blue">' . $_SESSION['category'] . ' </font> ';
echo " <a class ='button1' href='select_cat.php'>Change</a><br></h3>";


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
