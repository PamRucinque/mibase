<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

 
    $memberid = $_SESSION['borid'];

//$query = "select * from holmes_files order by id;";
$query_trans = "SELECT * FROM transaction WHERE borid = " . $memberid . " AND return Is Null ORDER BY created;";

//$numrows = pg_numrows($result);
$count = 1;
$result_trans = pg_exec($conn, $query_trans);
$numrows = pg_numrows($result_trans);


if ($numrows > 0) {
    echo '<h2> Toys On Loan: ' . $membername . '</h2>';
    echo '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey">';
    echo '<tr><td>Id</td><td>Date</td><td>Toy#</td><td>Toyname</td><td>Id</td><td>Due</td></tr>';

}


for ($ri = 0; $ri < $numrows; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($result_trans, $ri);
    //$weekday = date('l', strtotime($row['date_roster']));
    $trans_id = $row['id'];
    $trans_borid = $row['borid'];
    $trans_item = $row['item'];
    $trans_idcat = $row['idcat'];
    $trans_bornmame = $row['borname'];
    $trans_due = $row['due'];

    $format_created =  substr($row['created'],8,2) . '-'. substr($row['created'], 5,2) . '-' . substr($row['created'],0,4);
    $format_due =  substr($row['due'],8,2) . '-'. substr($row['due'], 5,2) . '-' . substr($row['due'],0,4);
  
    echo '<td width="30">' . $trans_id. '</td>';
    echo '<td width="50">' . $format_created . '</td>';
    echo '<td width="50">' . $trans_idcat . '</td>';
    echo '<td width="150" align="left">' . $trans_item . '</td>';
    echo '<td width="30" align="left">' . $trans_borid . '</td>';
    //echo '<td width="150" align="left">' . $trans_bornmame . '</td>';
    echo '<td width="100" align="left">' . $format_due . '</td>';



    echo '</tr>';
    
}

echo '</table>';



pg_close($link);
?>


