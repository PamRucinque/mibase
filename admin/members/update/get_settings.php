<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');
$query_settings = "SELECT * FROM settings;";
$result_settings = pg_Exec($conn, $query_settings);
$numrows = pg_numrows($result_settings);
$selectbox = '';
$show_password = 'No';

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_settings, $ri);
    if ($row['setting_name'] == 'selectbox') {
        $selectbox = $row['setting_value'];
    }
    if ($row['setting_name'] == 'selectname') {
        $selectname = $row['setting_value'];
    }
    //$user1_borwrs
    if ($row['setting_name'] == 'user_toys') {
        $user_toys = $row['setting_value'];
    }
    if ($row['setting_name'] == 'show_password') {
        $show_password = $row['setting_value'];
    }
}
?>

