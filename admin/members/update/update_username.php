<?php

//include( dirname(__FILE__) . '/../../connect.php');
$sql = "select firstname, emailaddress, expired, rostertype,membertype,partnersname,rostertype5,pwd,surname,id 
    from borwrs;";
//echo $conne;
$result = pg_Exec($conn, $sql);
$numrows = pg_numrows($result);

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    $memberid = $row['id'];
    $username = $row['rostertype5'];
    $surname = $row['surname'];
    $pwd = $row['pwd'];

    $length_surname = strlen($surname);
    if ($length_surname < 10) {
        $surname = pad_surname($surname);
    } else {
        $surname = substr($surname, 0, 10);
        $length_surname = 10;
    }
    if ($username == '' || $username == $_SESSION['library_code']) {
        $surname = strtolower($surname);
        $username = rtrim($surname, "%") . $memberid;
        $username = str_replace(" ", "", $username);
        $username = str_replace("'", "", $username);
        $username = str_replace("`", "", $username);
    }



    if (($pwd == '') || ($pwd == 'mibase')) {
        $pwd = generateStrongPassword();
    }
    $output = update_password($memberid, $pwd, $username);
    echo $output;
}

function pad_surname($string) {
    $length = 10;
    $out_string = '';
    for ($x = 0; $x < $length; $x++) {
        $char = substr($string, $x, 1);
        if ($char != '') {
            $out_string .= $char;
        } else {
            $out_string .= '%';
        }
    }

    return $out_string;
}

function generateStrongPassword($length = 10, $add_dashes = false, $available_sets = 'luds') {
    $sets = array();
    if (strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if (strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if (strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    if (strpos($available_sets, 's') == false)
        $sets[] = '!@#$%&*?';
    $all = '';
    $password = '';
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for ($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if (!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while (strlen($password) > $dash_len) {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

function update_password($memberid, $password, $username) {
    //include( dirname(__FILE__) . '/../../connect.php');
    $sql = "update borwrs set pwd = ?, rostertype5 = ?, modified = now() where id = ?;";
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $sth = $pdo->prepare($sql);
    $array = array($password, $username, $memberid);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $status = 'OK';
    } else {
        $status = '<br>Username and password has been created: ' . date("Y-m-d H:i:s") . '<br>';
    }
    return $status;
}
