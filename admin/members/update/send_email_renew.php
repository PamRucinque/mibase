<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
//$id = $_SESSION['borid'];
?>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>

    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');
            include( dirname(__FILE__) . '/../../header_detail/header_detail.php');
            include( dirname(__FILE__) . '/functions.php');
            //include( dirname(__FILE__) . '/data/children_new_email.php');
    

            $id = $_SESSION['borid'];
            //include( dirname(__FILE__) . '/../../get_settings.php');
            if ($email_from == '') {
                $email_from = 'noreply@mibase.com.au';
            }
            $strSubject = $_POST["txtSubject"];
            $strMessage = nl2br($_POST["txtDescription"]);



            $template = 'renew_member';
            include( dirname(__FILE__) . '/get_template.php');

            //echo 'member no: ' . $_SESSION['borid'] . '<br>';
            include( dirname(__FILE__) . '/get_member.php');
            $longname = $firstname . ' ' . $surname;
            $email_subject = $template_subject;

            $message = '';
            $message .= $template_message;
            $library = replace_bookmark($message, $id);
            //$email_to = 'michelle@mibase.com.au';
            $email_to = $library['email'];
            $libraryname = $_SESSION['libraryname'];
            $headers = get_header($libraryname, $email_from);

            echo '<br><a class="button1_red" href="member_detail.php?borid=' . $_SESSION['borid'] . '">Back to Member Details</a><br>';

            if ($email_to != '') {
                if (isset($_POST['submit'])) {
                    //$success = @mail($email_to, $email_subject, $message, $headers, $param);
                    $success = send_email($email_to, $email_subject, $library['message'], $headers['header'], $headers['param']);
                    if ($success) {
                        echo '<h2><font color="red">A Renewal Membership Email has been sent to email: </font>' . $library['email'] . '</h2><br>';
                        echo 'Subject: ' . $email_subject . '<br>';
                        echo 'Message: <br>' . $library['message'] . '<br>';
                    }
                } else {

                    echo '<p>A Renewal Email will be sent to email: ' . $library['email'] . '</p><br>';
                    echo '<font color="red"><p>Review the below details then scroll down and press the yellow send button to send. </p></font><br>';
                    echo 'Subject: <font color="green">' . $email_subject . '</font><br>';
                    echo 'Message: <font color="green">' . $library['message'] . '</font><br>';
                    include( dirname(__FILE__) . '/email_form_renew.php');
                }
            }

//$return = send_email($library['email'], $email_subject, $library['message'], $headers['header'], $headers['param']);

  
            ?>





