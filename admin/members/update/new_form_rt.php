<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<body id="main_body">

    <div id="form_container">

        <font size="2" face="Arial, Helvetica, sans-serif"></font>
        <table width="50%"><tr><td>


                    <form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'new_rt.php' ?>">

                        <div id="form" style="background-color:lightgray;" align="left">

                            <table align="top"><tr>

                                    <td align="right"><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Add a New Roster Group" /></td>
                                </tr>
                                <tr><td>Roster Group: <br>
                                        <select id="rt" name="rt">
                                            <option value='Roster' selected="selected">Roster</option>
                                            <option value="Roster Coord" >Roster Coord</option>
                                            <option value="Student" >Student</option>
                                            <option value="Extra" >Extra</option>
                                         </select></td></tr>
                                <tr>
                                    <td>Roster Hours:<br>
                                        <input type="Text" name="description" align="LEFT"  size="50" value=""></input><br></td>
                                </tr>
                                <tr><td>Roster Weekday: <br>
                                        <select id="weekday" name="weekday">
                                            <option value='No Weekday' selected="selected">No Weekday</option>
                                            <option value="Monday" >Monday</option>
                                            <option value="Tuesday" >Tuesday</option>
                                            <option value="Wednesday" >Wednesday</option>
                                            <option value="Thursday" >Thursday</option>
                                            <option value="Friday" >Friday</option>
                                            <option value="Saturday" >Saturday</option>
                                            <option value="Sunday" >Sunday</option>
                                        </select></td></tr>
                                <tr><td>No Hours:<br>
                                        <input type="Text" name="nohours" align="LEFT"  size="10" value=0></input><br></td>

                                    <td>No Volunteers:<br>
                                        <input type="Text" name="volunteers" align="LEFT"  size="10" value=0></input><br></td></tr>
                                <tr><td>Location:<br>
                                        <input type="Text" name="location" id="location" align="LEFT"  size="10"></input><br></td></tr>

                                <input type="hidden" name="id" value="<?php echo $newrtid; ?>"> 
                            </table>
                        </div>
                </td></tr></table>
    </form>
</div>
</body>




