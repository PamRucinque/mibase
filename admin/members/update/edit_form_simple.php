<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script type="text/javascript" src="../../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "d MM yy",
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "2010:+15"

        };
        $("#expired").datepicker(pickerOpts);
        $("#joined").datepicker(pickerOpts);
        $("#renewed").datepicker(pickerOpts);
        $("#age").datepicker(pickerOpts);
    });
    function update_member() {
        var dt = new Date();
        var yyyy = dt.getFullYear().toString();
        var mm = (dt.getMonth() + 1).toString();
        var dd = dt.getDate().toString();
        end = yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]);
        var str = 'CORRECT: ' + end;
        //alert(str);
        document.getElementById('update').innerHTML = str;
        document.getElementById("user1").focus();
    }
    function get_postcode(str) {
        var input = str;

        if (str == "") {
            document.getElementById("postcode").innerHTML = "";
            return;
        }

        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {

            document.getElementById("postcode").value = xmlhttp.responseText;

        }

        xmlhttp.open("GET", "getpostcode.php?q=" + str, true);
        xmlhttp.send();
    }




</script>
<?php

$id = $_GET['borid'];
include( dirname(__FILE__) . '/get_member.php');
//echo '<br><a href="../toy_detail.php" class="button1">Back to Toy Detail</a>'; 
echo '<br><a href="member_detail.php?borid=' . $_SESSION['borid'] . '" class="button1_red">Back to Member Detail</a>';
//include( dirname(__FILE__) . '/../../get_settings.php');
if ($suburb_title == '') {
    $suburb_title = 'Suburb';
}
if ($city_title == '') {
    $city_title = 'City';
}
$renew_button = $_SESSION['settings']['renew_button'];
?>
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'edit.php?borid=' . $_SESSION['borid']; ?>">

    <div id="form" style="background-color:lightgray;" align="left">

        <table width="100%"><tr><td>

                    <h2><font color="blue">Editing Member No: <?php echo $_SESSION['borid']; ?></font></h2></td>
                <td align="right">
                    <input id="saveForm" class="button1_red" type="submit" name="submit" value="Save Member" />
                    <?php
                    if ($renew_button == 'Yes') {
                        echo '<input id="renew" class="button1_red" type="submit" name="renew" value="Renew Member" />';
                    }
                    ?>
                </td></tr></table>
        <table>
            <tr><td width="25%">Contact 1 - First Name:<br>
                    <input type="Text" name="firstname" align="LEFT" required="Yes" size="25" value="<?php echo $firstname; ?>"></input></td>
                <td width="30%">Surname:<br>
                    <input type="Text" name="surname" align="LEFT" required="Yes" size="30" value="<?php echo $surname; ?>"></input><br></td>

                <td>Mobile:<br>
                    <input type="Text" name="mobile1" align="LEFT" size="20" value="<?php echo $mobile1; ?>"></input><br></td>
                <td width="35%">Email:<br>
                    <input STYLE="background-color: yellow;" type="Text" name="emailaddress" align="LEFT"  size="35" value="<?php echo $email; ?>"></input></td>

            <tr><td width="25%">Contact 2 - First Name:<br>
                    <input type="Text" name="partnersname" align="LEFT" size="25" value="<?php echo $partnersname; ?>"></input></td>
                <td width="30%">Surname:<br>
                    <input type="Text" name="partnerssurname" align="LEFT"  size="30" value="<?php echo $partnerssurname; ?>"></input><br></td>

                <td width="25%">Mobile:<br>
                    <input type="Text" name="mobile2" align="LEFT"  size="20" value="<?php echo $mobile2; ?>"></input><br></td>
                <td width="35%">Email:<br>
                    <input type="Text" name="email2" align="LEFT"  size="35" value="<?php echo $email2; ?>"></input></td>

            </tr>
            <tr><td width="35%">Home Phone:<br>
                    <input type="Text" name="phone" id="phone" align="LEFT"  size="25" value="<?php echo $phone; ?>"></input></td>
                <td width="35%">Date Joined:<br>
                    <input type="text" name="joined" id="joined" align="LEFT"  size="25" value="<?php echo $joined; ?>"></input></td>
                <td width="35%">Date Renewed:<br>
                    <input type="text" name="renewed" id="renewed" align="LEFT"  size="25" value="<?php echo $renewed; ?>"></input></td>
                <td width="35%">Date Expired:<br>
                    <input type="text" name="expired" id="expired" align="LEFT"  size="25" value="<?php echo $expired; ?>"></input></td>


            </tr></table>
        <h2>Contact Details:</h2>
        <table width="100%">
            <tr><td style="padding-right: 20px;">Address:<br>
                    <input type="Text" name="address" align="LEFT"  size="55" value="<?php echo $address; ?>"></input></td>

                <?php
                if ($suburb_title == 'City') {
                    
                } else {
                    echo '<td>';
                    echo $suburb_title . '<br>';
                    include( dirname(__FILE__) . '/get_city.php');
                    echo '</td>';
                }
                ?>

                <td  align='top' width="10%"><br>Pcode<br>
                    <input type="Text" id="postcode" name="postcode" align="LEFT"  size="4" value="<?php echo $postcode; ?>"></input><br><br></td>
                <td  align='top' width="10%"><br>State:<br>
                    <input type="Text" name="state" align="LEFT"  size="4" value="VIC"></input><br><br></td>
            </tr>
        </table>
        <table width="100%">
            <tr><td width="33%"><br>Member Type: <br><?php include( dirname(__FILE__) . '/get_memtype.php'); ?></td>
                <td width="10%"><br>Member Status: <br>
                    <select id="member_status" name="member_status">
                        <option value='<?php echo $member_status; ?>' selected="selected"><?php echo $member_status; ?></option>
                        <option value="ACTIVE" >ACTIVE</option>
                        <option value="LOCKED" >LOCKED</option>
                        <option value="RESIGNED" >RESIGNED</option>
                    </select></td>
                <td width="20%"><br>Source: <br><?php include( dirname(__FILE__) . '/get_source.php'); ?></td>
                <td  width="13%" align='top'><br>WWC number:<br>
                    <input type="Text" name="wwc" id = "wwc" align="LEFT"  size="20" value="<?php echo $wwc; ?>"></input><br>
                </td>


            </tr>

        </table>
        <table>
            <tr>
                <td width="30%">Username:<br>
                    <input type="Text" name="username1" align="LEFT" size="30" value="<?php echo $username; ?>"></input><br></td>
                <td width="30%">Password:<br>
                    <input type="Text" name="password1" id="password"align="LEFT"  size="30" value="<?php echo $pwd; ?>"></input><br></td>
                <td colspan="2"><br>Language other than English: <?php include( dirname(__FILE__) . '/get_rostertype3.php'); ?></td>


            </tr>
        </table>
        <table width="100%">

            <tr><td width="50%" ><label><br>Notes: </label><br>
                    <textarea id="notes" name="notes" rows="3" cols="35"><?php echo $notes; ?></textarea></td>
                <td><label><br>Alerts: </label><br>
                    <textarea id="alertmem" name="alertmem" rows="3" cols="35"><?php echo $alert_mem; ?></textarea></td>

            </tr>
            <tr><td><label><br>Skills / Occupation: </label><br>
                    <textarea id="skills" name="skills" rows="3" cols="35"><?php echo $skills; ?></textarea></td>
                <td><label><br>How can you help, get more involved? </label><br>
                    <textarea id="help" name="help" rows="3" cols="35"><?php echo $skills2; ?></textarea></td>
            </tr>

        </table>
        </td></tr>
        </table>

        <input type="hidden" name="id" id="id" value="<?php echo $_SESSION['borid']; ?>"> 
    </div>
</form></p>

