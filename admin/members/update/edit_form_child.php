<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script type="text/javascript" src="../../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "d MM yy",
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "1990:+nn"

        };
        $("#dob").datepicker(pickerOpts);
    });


    function myFunction()
    {
        alert("This Reservation has been saved!");
    }

</script>

<p><font size="2" face="Arial, Helvetica, sans-serif"></font></p>

<font></font>
<?php
$ref = 'member_detail.php?id=' . $_SESSION['borid'];
//echo "<td width='50'><a class ='button1' href='" . $ref  . "'>Edit</a></td>";
echo "<br><a href='" . $ref . "' class ='button1_red'>Back to Toy Member Details</a>";
?>
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'edit_child.php'; ?>">

    <div id="form" style="background-color:lightgray;" align="left">

        <table align="top"><tr>
                <?php echo 'Member Number: ' . $_SESSION['borid'] . '.'; ?>

                <td><h2>Edit a Child:</h2></td>
                <td align="right"><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Save" /></td>
            </tr>
            <tr>
                <td>First name:<br>
                    <input type="Text" name="child_name" align="LEFT"  size="50" value="<?php echo $child_name ?>"></input><br></td>
            </tr>
            <tr>
                <td>Surname:<br>
                    <input type="Text" name="surname" align="LEFT"  size="50" value="<?php echo $child_surname ?>"></input><br></td>
            </tr>
            <td><label>Gender: </label>
                <select id="sex" name="sex">
                    <option value='<?php echo $sex; ?>' selected="selected"><?php echo $sex; ?></option>
                    <option value="M" >M</option>
                    <option value="F" >F</option>
                    <option value="X" >X</option>
                </select></td>
            <tr>
                <td>Date of Birth:<br>
                    <input type="text" name="dob" id ="dob" align="LEFT" value="<?php echo $dob ?>"></input><br></td>
            </tr>
                      <tr><td><label><br>Notes / Special Needs: </label><br>
                    <textarea id="notes" name="notes" rows="3" cols="35"><?php echo $notes; ?></textarea></td>
                 </tr>

            <input type="hidden" name="childid" value="<?php echo $_SESSION['childid']; ?>"> 
            <input type="hidden" name="borid" value="<?php echo $_SESSION['borid']; ?>"> 
        </table>
    </div>
</form></p>

