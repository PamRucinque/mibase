

<?php

//echo $hover;
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

$memberid = $_SESSION['borid'];
//include( dirname(__FILE__) . '/../../connect.php');
//include( dirname(__FILE__) . '/get_type.php');

$query_trans = "SELECT * FROM contact_log WHERE borid = " . $borid . " and template != 'toys_overdue' and template != 'toys_due' ORDER BY created desc;";
$total = 0;
$hours = 0;
$tobedone = 0;
$done = 0;
$result_trans = pg_exec($conn, $query_trans);
$numrows = pg_numrows($result_trans);
$str_roster = '';

if ($expired_flag == 'Yes') {
    $start_member = date('Y-m-d', strtotime($expired));
    $expiry_str = $expired . ' +' . $expiryperiod . ' months';
    $expired_roster = date('Y-m-d', strtotime($expiry_str));
    $format_start_member = substr($start_member, 8, 2) . '-' . substr($start_member, 5, 2) . '-' . substr($start_member, 0, 4);
} else {
    $expired_roster = $expired;
}
$format_expired_roster = substr($expired_roster, 8, 2) . '-' . substr($expired_roster, 5, 2) . '-' . substr($expired_roster, 0, 4);


$str_roster .= '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey">';
if ($numrows > 0) {
    $str_roster .= '<tr><td>id</td><td></td><td>Date</td><td>Type</td><td align="center">Subject</td></tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
    //echo "<tr>\n";
    $total = $total + 1;

    $row = pg_fetch_array($result_trans, $ri);
    //$weekday = date('l', strtotime($row['date_roster']));
    $id = $row['id'];
    $created = $row['created'];

    $format_created = substr($row['created'], 8, 2) . '-' . substr($row['created'], 5, 2) . '-' . substr($row['created'], 0, 4);

    $borid = $row['borid'];
    $type = $row['type_contact'];
    $template = $row['template'];
    $subject = $row['subject'];
    $email = $row['email'];
    $details = $row['details'];

    //$hover =  '<td width = "50px"><div id="popup"><a>View Detail<span>'. $details .'</a></div></td>';
    $str_roster .= '<tr><td width="30px">' . $id . '</td>';

    $str_roster .= '<td width = "50px"><div id="popup"><a>View Detail<span>' . $details . '</span></a></div></td>';

    $str_roster .= '<td width="60" align="left">' . $format_created . '</td>';
    $str_roster .= '<td>' . $template . '<br>' . $type . '</td>';
    //$str_roster .= '<td>' . $template. '</td>';
    //<a href="//www.hscripts.com/tutorials" title="Free Tutorials">Free tutorials</a>
    //$str_roster .= '<td width="100" align="center"><a title="' . $details . '">Content</a></td>';
    $str_roster .= '<td align="center">' . $subject . '</td>';
    $str_roster .= '</tr>';
}

$str_roster .= '</table>';
echo $str_roster;
//pg_close($link);
?>


