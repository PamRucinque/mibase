<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
//$id = $_SESSION['borid'];
?>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>

    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');
            include( dirname(__FILE__) . '/../../header_detail/header_detail.php');
            include( dirname(__FILE__) . '/functions.php');
            //include( dirname(__FILE__) . '/data/children_new_email.php');


            $id = $_SESSION['borid'];
            //include( dirname(__FILE__) . '/../../get_settings.php');

            if (isset($_POST['txtSubject'])){
                $strSubject = $_POST["txtSubject"];
            }
            if (isset($_POST['txtDescription'])){
                $strMessage = nl2br($_POST["txtDescription"]);
            }


            $template = 'new_member';
            $libraryname = $_SESSION['settings']['libraryname'];
            include( dirname(__FILE__) . '/get_template.php');
            $email_from = $_SESSION['settings']['email_from'];
            if ($email_from == '') {
                $email_from = 'noreply@mibase.com.au';
            }

            //echo 'member no: ' . $_SESSION['borid'] . '<br>';
            include( dirname(__FILE__) . '/get_member.php');

            $longname = $firstname . ' ' . $surname;
            $email_subject = $template_subject;

            $message = '';
            $message .= $template_message;
            $message = str_replace('[password]', $email_subject, $message);
            $message = str_replace('[pwd]', $pwd, $message);
            $message = str_replace('[username]', $username, $message);
            $message = str_replace('[Libraryname]', $libraryname, $message);
            //[Libraryname]
            $library = replace_bookmark($message, $id);
            //$email_to = 'michelle@mibase.com.au';
            $email_to = $library['email'];

            $headers = get_header($libraryname, $email_from);

            echo '<br><a class="button1_red" href="member_detail.php?borid=' . $_SESSION['borid'] . '">Back to Member Details</a>';
            //
            $edit_template = '../../template/edit_template.php?t=new_member';
            //echo '<a class="button1_green" href="' . $edit_template . '">Edit Email</a><br>';

            if ($email_to != '') {
                if (isset($_POST['submit'])) {
                    //$success = @mail($email_to, $email_subject, $message, $headers, $param);
                    $success = send_email($email_to, $email_subject, $library['message'], $headers['header'], $headers['param']);
                    if ($success) {
                        echo '<h2><font color="red">A Welcome Email has been sent to email: </font>' . $library['email'] . '</h2><br>';
                        echo 'Subject: ' . $email_subject . '<br>';
                        echo 'Message: <br>' . $library['message'] . '<br>';
                    }
                } else {

                    echo '<p>A Welcome Email will be sent to email: ' . $library['email'] . '</p><br>';
                    echo '<font color="red"><p>Review the below details then scroll down and press the yellow send button to send. </p></font><br>';
                    echo 'Subject: <font color="green">' . $template_subject . '</font><br>';
                    echo 'Message: <font color="green">' . $library['message'] . '</font><br>';
                    include( dirname(__FILE__) . '/email_form.php');
                }
            }
            ?>





