<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');


//get configuration data
//include(__DIR__ . '/../../config.php');

$id = $_GET['id'];
//echo "id:" . $id;
include 'get_toy.php';
$logo = $_SESSION['logo'];

$desc1 = str_replace("\r\n", "</br>", $desc1);
$desc2 = str_replace("\r\n", "</br>", $desc2);




 
if ($_SESSION['shared_server']) {
    $pic_url = $_SESSION['web_server_protocol'] . "://" . $_SESSION['host'] . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . '.jpg';
} else {
    $pic_url = $_SESSION['web_server_protocol'] . "://" . $_SESSION['host'] . $_SESSION['toy_images_location'] . "/" . strtolower($idcat) . '.jpg';
}

if ($_SESSION['shared_server']) {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . "/" . strtolower($idcat) . '.jpg';
} else {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . "/" . strtolower($idcat) . '.jpg';
}


if (file_exists($file_pic)) {
    $img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';
} else {
    $img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';
}
?>
<br>

<table><tr><td width ="75%">
            <table>
                <tr>
                    <td><strong>Toy Name:</strong></td>
                    <td align ="left"><?php echo $toyname; ?></td></tr>
                <tr>
                    <td><strong>Toy Number:</strong></td>
                    <td align ="left"><?php echo $idcat; ?></td></tr>
                <tr>
                    <td><strong>Category:</strong></td>
                    <td align ="left"><?php echo $category; ?></td></tr>
                <tr>
                    <td><strong>No Pieces:</strong></td>
                    <td align ="left"><?php echo $no_pieces; ?></td>
                </tr> 
                <td><strong>Description:</strong></td>
                <td align ="left" width="40%"><?php echo $desc1; ?></td>

                <td></td>
                <td align ="left" align="top"><?php echo $desc2; ?></td></tr>
    <tr>
        <td><strong>Rent:</strong></td>
        <td align ="left"><?php echo $rent; ?></td></tr>
    <tr>
        <td><strong>Age:</strong></td>
        <td align ="left"><?php echo $age; ?></td></tr>
    <tr>
        <td><strong>Manufacturer:</strong></td>
        <td align ="left"><?php echo $manufacturer; ?></td></tr>
    <tr>
        <td><strong>Supplier:</strong></td>
        <td align ="left"><?php echo $supplier; ?></td></tr>
    <tr>
        <td><strong>Cost:</td>
        <td align ="left"><?php echo $cost; ?></td></tr>
    <tr>
        <td><strong>Comments:</strong></td>
        <td align ="left"><?php echo $comments; ?></td></tr>
    <tr>
        <td><strong>Toy Status:</strong></td>
        <td align ="left"><?php echo $status_txt; ?></td></tr>



</table>
</td>
<td><?php
      echo '<a href="edit.php?idcat=' . $_GET['idcat'] . '" class="button1">Edit</a>';
      echo '<a href="delete_toy.php?idcat=' . $_GET['idcat'] . '" class="button1">Delete this Toy</a>';
      echo $img; ?></td>
</tr></table>
<?php
if ($status == 't') {
    include( dirname(__FILE__) . '/trans_detail.php');
}
?>
<?php
include( dirname(__FILE__) . '/parts.php');
?>   
</div>
</body>