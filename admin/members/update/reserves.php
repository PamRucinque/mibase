<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//get settings
$timezone = $_SESSION['settings']['timezone'];



if (($reservations == 'Yes') && ($_SESSION['shared_server'])) {

    $memberid = $_SESSION['borid'];
    date_default_timezone_set($timezone);

    $tomorrow = date("Y-m-d", strtotime("+ 0 day"));
//echo 'loan period: ' . $loanperiod;
    $reserve_dates = array();


    $sql = "SELECT reserve_toy.*,
(select due from transaction where transaction.idcat = reserve_toy.idcat AND return is null) as due,
(select toyname from toys where toys.idcat = reserve_toy.idcat) as toyname
FROM reserve_toy 
WHERE member_id = " . $memberid . " ORDER BY date_start DESC;";

    //echo $trans;
    //$numrows = pg_numrows($result);
    $count = 1;
    $dbconn = pg_connect($_SESSION['connect_str']);
    $trans = pg_exec($dbconn, $sql);
    $x = pg_numrows($trans);
//echo 'number rows' . $x;
    $due = strtotime($trans_due);


    if ($x > 0) {
        echo '<br><strong><font color="black">Reservations</font></strong><br>';
        echo '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
        echo '<tr><td>id</td><td>Start</td><td>End</td><td>Toy name</td><td>Toy #</td><td></td><td>Due</td><td></td><td>Paid</td></tr>';
    } else {
        if (isset($_SESSION['borid'])) {
            //echo '<br>This member has no Reservations.';  
        }
    }


    for ($ri = 0; $ri < $x; $ri++) {
        //echo "<tr>\n";
        $row = pg_fetch_array($trans, $ri);
        //$weekday = date('l', strtotime($row['date_roster']));
        $reserve_id = $row['id'];
        $reserve_borid = $row['member_id'];
        $reserve_toyname = $row['item'];
        $reserve_idcat = $row['idcat'];
        $reserve_bornmame = $row['borname'];
        $reserve_phone = $row['phone'];
        $toyname = $row['toyname'];
        $trans_due = $row['due'];
        $paid = $row['paid'];

        $now = date('Y-m-d');
        $tomorrow = date("Y-m-d", strtotime("+ 1 day"));
        if ($row['date_start'] < $now) {
            $start = $tomorrow;
        } else {
            $start = $row['date_start'];
        }

        $diff = (strtotime($row['date_end']) - strtotime($start)) / 24 / 3600;
        for ($xi = 0; $xi <= $diff; $xi++) {
            $str = '+' . $xi . 'days';
            $curr = strtotime($str, strtotime($now));
            $str_date = date("Y-m-d", $curr);
            //echo $str_date . ' ';
            array_push($reserve_dates, $str_date);
        }
        //echo $diff;


        $format_start = substr($row['date_start'], 8, 2) . '-' . substr($row['date_start'], 5, 2) . '-' . substr($row['date_start'], 0, 4);
        $format_end = substr($row['date_end'], 8, 2) . '-' . substr($row['date_end'], 5, 2) . '-' . substr($row['date_end'], 0, 4);
        $now = date('Y-m-d');
        $ref2 = 'return_toy.php?id=' . $row['id'];
        $ref = 'renew_toy.php?id=' . $row['id'];

        if (strtotime($now) > strtotime($row['date_end'])) {
            $due_str = '<font color="red" font="strong"> OVERDUE  ' . $format_end . '</font>';
        } else {
            $due_str = $format_end;
        }
        //<a class="button_menu" href="../../toys/update/toy_detail.php">Toy</a>
        echo '<tr id="red" style="color:darkgreen;">';
        echo '<td>' . $reserve_id . '</td>';
        echo '<td>' . $format_start . '</td>';
        echo '<td>' . $format_end . '</td>';
        echo '<td>' . $toyname . '</td>';
        echo '<td>' . $row['status'] . '</td>';
        echo '<td width="30px" align="left"><a class="button_small" href="../../toys/update/toy_detail.php?t=' . $reserve_idcat . '">' . $reserve_idcat . '</a></td>';
        //echo '<td align="left"><a class="button_small" href="loan.php?t=' . $reserve_idcat . '">' . $reserve_idcat . '</a></td>';
        //echo '<td>' . $diff . '</td>';
        if ($row['due'] != '') {
            echo '<td  width="100px" align="center" style="background-color: yellow;"><a>' . $row['due'] . '</a></td>';
        } else {
            if (in_array($start, $reserve_dates) && in_array($row['date_end'], $reserve_dates)) {
                echo '<td width="100px" align="center" style="background-color: white;">OK</a></td>';
            } else {
                echo '<td width="100px" align="center" style="background-color: pink;"><a>RESERVED</a></td>';
            }
        }
        $ref1 = '<td width="60px"><form action="" method="POST"><input type="hidden" name="id" value="' . $row['id'] . '">';
        $ref1 .= '<input id="delete_reserve" name="delete_reserve" class="button_small_red"  type="submit" value="Delete" /></form></td>';

        //echo '<td align="left"><a class="button_small_red" href="delete_reservation.php?r=' . $reserve_id . '">Delete</a></td>';
        echo $ref1;
        echo '<td align="center">' . $paid . '</td>';


        //print_r($reserve_dates);
        //echo 'Start: ' . $start . ' End: ' . $row['date_end'];
        echo '</tr>';
    }

    echo '</table>';
//echo '<br>' . $start . ' Reserve Dates: ' . implode('|',$reserve_dates) . '<br>';
//pg_close($link);
}
?>


