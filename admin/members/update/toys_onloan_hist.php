<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

    //get configuration data
//include(__DIR__ . '/../../config.php');


//include( dirname(__FILE__) . '/../../connect.php');
$returns_txt = '';
$overdue = 0;

if (isset($_POST['id'])) {
    include( dirname(__FILE__) . '/delete_history.php');
}

$query = "SELECT * from transaction where (return is not null) and borid = " . $_SESSION["borid"] . " order by date_loan desc";
$trans = pg_exec($conn, $query);
$numrows = pg_numrows($trans);

if (isset($_GET['r'])) {
    $toprint = $numrows;
} else {
    $toprint = 30;
}
$total = 0;
$total = $numrows;
if ($numrows < 30) {
    $toprint = $numrows;
    $toprint_txt = " ";
} else {
    $link = '<a class="button_small" href="member_detail.php?r=' . $toprint . '">Showall</a>';
    $toprint_txt = $toprint . ' of ' . $total . " shown. " . $link;
}
//echo $query;

if ($numrows > 0) {
    $returns_txt .= '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey">';
    $returns_txt .= '<tr><td>#</td><td>Id</td><td>Date</td><td>Toy#</td><td></td><td>Toyname</td><td>Due</td><td>Returned</td><td>Overdue</td><td></td></tr>';
}

$count = 0;
for ($ri = 0; $ri < $toprint; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($trans, $ri);
    $count = $count + 1;
    $trans_id = $row['id'];
    $trans_borid = $row['borid'];
    $trans_item = $row['item'];
    $trans_idcat = $row['idcat'];
    $trans_bornmame = $row['borname'];
    $trans_due = $row['due'];
    $ts1 = strtotime($row['due']);
    $ts2 = strtotime($row['return']);
    
    $overdue = ($ts2 - $ts1) / (-60 * 60 * 24);
    $format_loan = substr($row['date_loan'], 8, 2) . '-' . substr($row['date_loan'], 5, 2) . '-' . substr($row['date_loan'], 0, 4);
    $format_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);
    $format_return = substr($row['return'], 8, 2) . '-' . substr($row['return'], 5, 2) . '-' . substr($row['return'], 0, 4);

    $now = date('Y-m-d');

    $ref1 = '<td width="60px"><form action="" method="POST"><input type="hidden" name="id" value="' . $row['id'] . '">';
    $ref1 .='<input id="submit" name="submit" class="button_small_red"  type="submit" value="Delete" /></form></td>';

    $due_str = $format_due;

    



 
if ($_SESSION['shared_server']) {
    $pic_url = $_SESSION['web_server_protocol'] . "://" . $_SESSION['host'] . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . '.jpg';
} else {
    $pic_url = $_SESSION['web_server_protocol'] . "://" . $_SESSION['host'] . $_SESSION['toy_images_location'] . "/" . strtolower($idcat) . '.jpg';
}

if ($_SESSION['shared_server']) {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . "/" . strtolower($idcat) . '.jpg';
} else {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . "/" . strtolower($idcat) . '.jpg';
}


    
    
    
    
    
    //<a class="button_menu" href="../../toys/update/toy_detail.php">Toy</a>
    $returns_txt .= '<td width="20px">' . $count . '</td>';
    $returns_txt .= '<td width="20px">' . $trans_id . '</td>';
    $returns_txt .= '<td width="30px">' . $format_loan . '</td>';
   
    //echo '<td width="30" align="left"><a class="button_small" href="../../admin/toys/update/toy_detail.php?idcat=' . $trans_idcat . '">' . $trans_idcat . '</a></td>';
    $returns_txt .= '<td width="30px" align="left"><a class="button_small" href="../../toys/update/toy_detail.php?t=' . $trans_idcat . '">' . $trans_idcat . '</a></td>';
    

    if (file_exists($file_pic)) {
        $pic =  $pic_url ;
    } else {
        $pic =    $_SESSION['web_server_protocol'] . "://" . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
    }
    //$pic = "'../../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . ".jpg'";
    $returns_txt .= '<td width="50" align="center"><a href="../../toys/update/toy_detail.php?idcat=' . $trans_idcat . '" onmouseover="showtrail(175,220, \'' . $pic . '\');" onmouseout="hidetrail();" class="button_small_yellow"/>View</a>';
    //$img = '<img height="200px" src="../..' . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg" alt="toy image">';

    
    $returns_txt .= '<td width="210px">' . $trans_item . '</td>';
    $returns_txt .= '<td width="90px" align="left">' . $due_str . '</td>';
    $returns_txt .= '<td width="90px" align="left">' . $format_return . '</td>';
    if ($overdue >= 0) {
        $returns_txt .= '<td width="90px" align="left">' . round($overdue, 0) . '</td>';
    } else {
        $returns_txt .= '<td width="90px" align="left"><font color="red">' . round($overdue * -1, 0) . '</font></td>';
    }
    $returns_txt .= $ref1;







    $returns_txt .= '</tr>';
}

$returns_txt .= '</table>';


pg_close($conn);

echo '<strong>History of Loans: <font color="darkcyan">Total: ' . $total . '</font></strong>';
echo $returns_txt;
echo $toprint_txt;
?>

</body>


