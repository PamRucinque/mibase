<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

$memberid = $_SESSION['borid'];
//include( dirname(__FILE__) . '/../../connect.php');
//include( dirname(__FILE__) . '/get_type.php');

$query_trans = "SELECT * FROM roster WHERE member_id = " . $memberid . " ORDER BY date_roster;";
$total = 0;
$hours = 0;
$tobedone = 0;
$done = 0;
$result_trans = pg_exec($conn, $query_trans);
$numrows = pg_numrows($result_trans);
$str_roster = '';


$start_member = date('Y-m-d', strtotime('first day of January this year'));
$expired_roster = date('Y-m-d', strtotime('12/31'));
//$start_member = date('Y-m-d', strtotime($expired));
//$expiry_str = $expired . ' +' . $expiryperiod . ' months';
//$expired_roster = date('Y-m-d', strtotime($expiry_str));
$format_start_member = substr($start_member, 8, 2) . '-' . substr($start_member, 5, 2) . '-' . substr($start_member, 0, 4);

$format_expired_roster = substr($expired_roster, 8, 2) . '-' . substr($expired_roster, 5, 2) . '-' . substr($expired_roster, 0, 4);


$str_roster .= '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey">';
if ($numrows > 0) {
    $str_roster .= '<tr><td>id</td><td>Date</td><td>Type</td><td>Weekday</td><td align="center">Session</td><td>Duration</td><td>completed</td><td></td></tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
    //echo "<tr>\n";
    $total = $total + 1;

    $row = pg_fetch_array($result_trans, $ri);
    //$weekday = date('l', strtotime($row['date_roster']));
    $id = $row['id'];
    $date_roster = $row['date_roster'];
    $member_id = $row['member_id'];
    $weekday = $row['weekday'];
    $type_roster = $row['type_roster'];
    $roster_session = $row['roster_session'];
    $duration = $row['duration'];
    $location = $row['location'];


    if ($date_roster >= $start_member && $date_roster <= $expired_roster) {
        $hours = $hours + $duration;
        if ($row['complete'] == 't') {
            $done = $done + $duration;
        } else {
            $tobedone = $tobedone + $duration;
        }
    }

    if ($row['complete'] == 't') {
        $completed = 'Yes';
        $str_roster .= '<tr>';
    } else {
        $completed = 'No';
        $str_roster .= '<tr style="background-color:#F3F781;">';

        if ($row['approved'] == 'f') {
            $str_roster .= '<tr style="background-color:lightpink;">';
        }
    }
    if ($date_roster <= $start_member || $date_roster >= $expired_roster) {
        $str_roster .= '<tr style="background-color:lightblue;">';
    }
    $date_roster = substr($row['date_roster'], 8, 2) . '-' . substr($row['date_roster'], 5, 2) . '-' . substr($row['date_roster'], 0, 4);

    $str_roster .= '<td width="30">' . $id . '</td>';
    $str_roster .= '<td width="60" align="left">' . $date_roster . '</td>';
    $str_roster .= '<td width="150">' . $type_roster . '</td>';
    $str_roster .= '<td width="50">' . $weekday . '</td>';
    $str_roster .= '<td width="100" align="center">' . $roster_session . '</td>';
    $str_roster .= '<td width="30" align="center">' . $duration . '</td>';

    //$str_roster .= '<td width="30" align="center">' . $completed . '</td>';

    if ($completed == 'Yes') {
        $str_roster .= "<td align='center'>Yes</td>";
        //$done = $done + $hours;
    } else {
        //$tobedone = $tobedone + $hours;
        $str_roster .= "<td><a class='button_small' href='roster/complete_roster.php?id=" . $row['id'] . "'>Completed</a></td>";
    }
    $str_roster .= "<td width='50'><a class='button_small_green' href='../../roster/edit_roster.php?id=" . $row['id'] . "'>Edit</a></td>";


    $str_roster .= '</tr>';
}

$str_roster .= '</table>';
$to_allocate = $duties - $hours;
$to_complete = $duties - $done;

echo 'Start Roster Date: <font color="blue">' . $format_start_member . '</font> End Roster Date: <font color="blue">' . $format_expired_roster . '</font><br>';
echo '<strong>Rostered Duties. <font color="red">  To Allocate: ' . $to_allocate . '</strong></font>';
echo '<a class="button_small_red" align="right" href="../../roster/new_roster.php">New</a><br>';
echo $str_roster;
echo '<strong><font color="blue">Total: ' . $total . '</font><font color="green">Sessions/Hours: ' . $hours . '</font><font color="purple">  Required: ' . $duties . '</font></strong>';
echo '<font color="#C73F17">  To complete: ' . $to_complete . '</font>';

pg_close($link);
?>


