<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <body>
        <div id="form_container">
            <?php include( dirname(__FILE__) . '/../../menu.php');
            $category = $_SESSION['category'];
            //echo  $category;
            ?>
            <br>
            <form name="openclaims" id="openclaims" method="post" action="new.php" >    

                <table  bgcolor="lightgray"><tr><td width="35%">
                            <label class="description" for="element_12">Select Category: </label>

                            <?php include( dirname(__FILE__) . '/get_category.php'); ?>
                        </td></tr>
                    <tr><td>
                            <input type="number" name="toyid" min="1" max="5000" value="<?php echo $_SESSION['toyid']; ?>"></input>
                        </td></tr>
                    <tr><td>
                            <input type="submit" name ="submit_category" value="NEXT">

                        </td></tr></table>

            </form>
            <?php
            echo $_SESSION['toyiderror'];
            echo $_SESSION['caterror'];
            echo $_SESSION['unused'];

            include ('unused.php');
            ?>


        </div>
    </body>
</html>

