<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');



$memberid = $_SESSION['borid'];
//include( dirname(__FILE__) . '/../../connect.php');
//include( dirname(__FILE__) . '/get_type.php');

$start_str = $expired . ' -' . round(1.25 * $expiryperiod) . ' months';
$start = date('Y-m-d', strtotime($start_str));

//$query_trans = "SELECT * FROM roster WHERE member_id = " . $memberid . " and date_roster > '" . $start . "' ORDER BY  date_roster;";
$query_trans = "SELECT roster.*, to_char((expired -  m.expiryperiod * INTERVAL '1 MONTH'), 'YYYY-MM-dd') as start 
FROM roster 
left join borwrs b on b.id = roster.member_id
left join membertype m on m.membertype = b.membertype
where member_id = " . $memberid . " order by date_roster;";

//echo $query_trans;
$total = 0;
$hours = 0;
$tobedone = 0;
$done = 0;
$count = array();
$count['member_complete'] = 0;
$count['c_complete'] = 0;
$pending = 0;

$conn = pg_connect($_SESSION['connect_str']);
$result_trans = pg_exec($conn, $query_trans);
$numrows = pg_numrows($result_trans);
$str_roster = '';
if ($renewed == ''){
    $renewed = $joined;
}
//echo $expired_flag;
if ($expired_flag == 'Yes') {
    $start_member = date('Y-m-d', strtotime($expired));
    $expiry_str = $expired . ' +' . $expiryperiod . ' months';
    $expired_roster = date('Y-m-d', strtotime($expiry_str));
    //$format_start_member = substr($start_member, 8, 2) . '-' . substr($start_member, 5, 2) . '-' . substr($start_member, 0, 4);
} else {
    $expired_roster = $expired;
    $start_member = date('Y-m-d', strtotime($renewed));
}
$format_start_member = substr($start_member, 8, 2) . '-' . substr($start_member, 5, 2) . '-' . substr($start_member, 0, 4);
$format_expired_roster = substr($expired_roster, 8, 2) . '-' . substr($expired_roster, 5, 2) . '-' . substr($expired_roster, 0, 4);


$str_roster .= '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey">';
if ($numrows > 0) {
    $str_roster .= '<tr><td>id</td><td>Date</td><td>Type</td><td>Weekday</td><td align="center">Session</td><td>hrs</td><td>completed</td><td></td></tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
    //echo "<tr>\n";
    $total = $total + 1;

    $row = pg_fetch_array($result_trans, $ri);
    //$weekday = date('l', strtotime($row['date_roster']));
    $id = $row['id'];
    if ($start != ''){
       $start_member = $row['start']; 
    }
    
    $date_roster = $row['date_roster'];
    $session_role = $row['session_role'];
    $member_id = $row['member_id'];
    $weekday = $row['weekday'];
    $type_roster = $row['type_roster'];
    $roster_session = $row['roster_session'];
    $duration = $row['duration'];
    $location = $row['location'];
    $status = trim($row['status']);
    $comments = $row['comments'];
    if ($member_id == 0) {
        if ($session_role == 'Co-ordinator') {
            $count['coord'] = $count['coord'] + 1;
        } else {
            $count['member'] = $count['member'] + 1;
        }
    } else {
        if ($session_role == 'Co-ordinator') {
            $count['c_complete'] = $count['c_complete'] + 1;
        } else {
            $count['member_complete'] = $count['member_complete'] + 1;
        }
    }

    if ($date_roster >= $start_member && $date_roster <= $expired_roster) {
        $hours = $hours + $duration;
        if ($status == 'completed') {
            $done = $done + $duration;
        } else {
            if ($status == 'pending') {
                $pending = $pending + $duration;
            }
        }
    }

    if ($status == 'completed') {
        $completed = 'Yes';
        $str_roster .= '<tr>';
    } else {
        $completed = 'No';
        $str_roster .= '<tr style="background-color:#F3F781;">';

        if ($row['approved'] == 'f') {
            $str_roster .= '<tr style="background-color:lightpink;">';
        }
    }
    if ($date_roster < $start_member || $date_roster >= $expired_roster) {
        $str_roster .= '<tr style="background-color:lightblue;">';
    }
    $date_roster = substr($row['date_roster'], 8, 2) . '-' . substr($row['date_roster'], 5, 2) . '-' . substr($row['date_roster'], 0, 4);

    $str_roster .= '<td width="30">' . $id . '</td>';
    $str_roster .= '<td width="60" align="left">' . $date_roster . '</td>';
    $str_roster .= '<td width="150">' . $type_roster . '<br>' . $comments . '</td>';
    $str_roster .= '<td width="50">' . $weekday . '</td>';
    $str_roster .= '<td width="100" align="center">' . $roster_session . '</td>';
    $str_roster .= '<td width="30" align="center">' . $duration . '</td>';


    if ($completed == 'Yes') {
        $str_roster .= "<td align='center'>Yes</td>";
        //$done = $done + $hours;
    } else {
        if ($status == 'pending') {
            $str_roster .= "<td><a class='button_small' href='complete_roster.php?id=" . $row['id'] . "'>Completed</a></td>";
        } else {
            $str_roster .= "<td align='center'>" . $row['status'] . "</td>";
        }
        //$tobedone = $tobedone + $hours;
    }
    $str_roster .= "<td width='50'><a class='button_small_green' href='../../roster/edit_roster.php?id=" . $row['id'] . "'>Edit</a></td>";
    $ref1 = '<td width="60px"><form action="" method="POST"><input type="hidden" name="id" value="' . $row['id'] . '">';
    //$ref1 .='<input id="submit_delete" name="submit_delete" class="button_small_red"  type="submit" value="Delete" /></form></td>';
    $str_roster .= $ref1;

    $str_roster .= '</tr>';
}

$str_roster .= '</table>';
$to_allocate = $duties - $tobedone;
$to_complete = $duties - $done;
echo 'Start Roster Date: <font color="blue">' . $format_start_member . '</font> End Roster Date: <font color="blue">' . $format_expired . '</font><br>';
echo '<strong>Rostered Duties. <font color="red">  To Allocate: ' . ($duties - $pending - $done) . '</strong></font>';
echo ' <a class="button_small_red" align="right" href="../../roster/new_roster.php">New</a>  ';
echo ' <a class="button_small_green" align="right" href="../../roster/credit_roster.php">Roster Credit</a>';
echo ' <a class="button_small_red" align="right" href="../../roster/debit_roster.php">Roster Debit</a><br>';

echo $str_roster;
echo '<strong><font color="blue"> Total: ' . $total . '</font><font color="green">  Sessions/Hours: ' . $hours . '</font><font color="purple">  Required: ' . $duties . '</font></strong>';
echo '<font color="#C73F17">  To complete: ' . $to_complete . '</font>';

?>


