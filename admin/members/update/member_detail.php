<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */

//top level, not included

require( dirname(__FILE__) .  '/../../mibase_check_login.php');
if (!session_id()) {
    session_start();
}


//set defaults
$membertype = '';
$expiryperiod = '';
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>

    <body id="main_body" onload="setFocus()">
        <div class="container-fluid">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');

            include( dirname(__FILE__) . '/../../header_detail/header_detail.php');
            //include( dirname(__FILE__) . '/../../header_detail/header_detail.php');

            if (!session_id()) {
                session_start();
            }
            $new_member_email = $_SESSION['settings']['new_member_email'];
            $selectbox = $_SESSION['settings']['selectbox'];

            if (isset($_GET['id'])) {
                $_SESSION['borid'] = $_GET['id'];
            }
            if (isset($_POST['borid'])) {
                $_SESSION['borid'] = $_POST['borid'];
            }
            if (isset($_POST['memberid'])) {
                $_SESSION['borid'] = $_POST['memberid'];
                $borid = $_SESSION['borid'];
            }
            if (isset($_POST['submit_alert_member'])) {
                include( dirname(__FILE__) . '/alerts/new_alert.php');
            }
            if (isset($_POST['submit_child'])) {
                include( dirname(__FILE__) . '/children/new_child.php');
            }
            if (isset($_POST['submit_delete'])) {
                //echo 'hello';
                include( dirname(__FILE__) . '/delete_roster.php');
            }
            //
            if (isset($_POST['change'])) {

                $_SESSION['borid'] = $_POST['borid'];
                $link = 'member_detail.php?id=' . $_SESSION['borid'];
                header('Location: ' . $link);
            } else {
                //$_SESSION['borid'] = $_GET['id'];
                $link = 'member_detail.php?id=' . $_SESSION['borid'];
            }
            if (isset($_POST['delete_reserve'])) {
                include( dirname(__FILE__) . '/delete_reservation.php');
            }
            ?>


            <?php
            if ($_SESSION['level'] == 'admin') {
                //echo $_SESSION['login_type'];
                include( dirname(__FILE__) . '/get_member.php');
                include( dirname(__FILE__) . '/../../header_detail/get_member_header.php');
                include( dirname(__FILE__) . '/../find/find_member.php');
                include( dirname(__FILE__) . '/member_detail_print.php');
            } else {
                echo '<br><br><h2>The members details have been disabled for this Login Type.</h2>';
            }
            ?>
        </div>
    </body>
