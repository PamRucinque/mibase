<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

if (isset($_GET['borid']) && is_numeric($_GET['borid']) && ($_SESSION['mibase'] != '')) {
// get the 'id' variable from the URL
    //include( dirname(__FILE__) . '/../../connect.php');

    $now = date("Y-m-d H:i:s");
    $now_str_ok = $now;
    $id = $_GET['borid'];
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $query = "UPDATE borwrs SET 
                    helmet_date=?, helmet=?  
                    WHERE id=?;";
    $array = array($now_str_ok, 'Yes', $id);


    $sth = $pdo->prepare($query);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    echo $query;
    if ($stherr[0] != '00000') {
        echo "An Update query error occurred.\n";
        echo $query;
//echo $connect_pdo;
        echo 'Error ' . $stherr[0] . '<br>';
        echo 'Error ' . $stherr[1] . '<br>';
        echo 'Error ' . $stherr[2] . '<br>';
    } 
}
$redirect = "Location: member_detail.php?id=" . $_SESSION['borid'];
header($redirect);
?>
