<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');
$query = "SELECT id, rostertype, description, weekday FROM rostertypes  where rostertype != 'Extra' ORDER by rostertype ASC;";
$result = pg_Exec($conn, $query);


//choose the database

//get data from database


echo "<select name='rostertype4' id='rostertype4' style='width: 150px'>\n";

$numrows = pg_numrows($result);
//echo '<option value="" selected="selected"></option>';
echo "<option value='" . $rostertype4 . "' selected='selected'>" . $rostertype4 . "</option>";

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    if (($row['rostertype'] == 'Roster')||($row['rostertype'] == 'Roster Coord')){ 
        $roster_desc = $row['rostertype'] . ': ' . $row['weekday'] . ': ' . $row['description'];
    }else{
        $roster_desc = $row['rostertype'];
    }
    
    echo "<option value='{$roster_desc}'>{$roster_desc}
    </option>\n";
}
echo "<option value='Any'>Any</option>\n";
echo "<option value=''></option>\n";
echo "</select>\n";


?>

