<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

function clean($input) {
    $output = stripslashes($input);
    $output = str_replace("'", "`", $output);
    return $output;
}

function date_expired($membertype, $expire) {

        date_default_timezone_set($_SESSION['settings']['timezone']);
   
    //include( dirname(__FILE__) . '/../../connect.php');

    $sql = "SELECT expiryperiod AS expires, due FROM membertype 
                WHERE (membertype)='" . $membertype . "'";
    $conn = pg_connect($_SESSION['connect_str']);
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $expired_months = $row['expires'];
    $due = $row['due'];
    // $expired_months = 12;
    $str_month = "+" . $expired_months . " months";
    //$str_month = "+ 3 months";
    $expires = strtotime(date("Y-m-d", strtotime($expire)) . $str_month);
    //$expires = $joined + $expire_months;
    $expires_str = date("Y-m-d", $expires);
    if ($due != '') {
        $expires_str = $due;
    }


    return $expires_str;
}

function add_to_journal($datepaid, $bcode, $icode, $name, $description, $category, $amount, $type, $typepayment) {

    //include( dirname(__FILE__) . '/../../connect.php');
    
    
    

    try {
        $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    } catch (PDOException $e) {
        print "Error! add to journal: " . $e->getMessage() . "<br/>";
        die();
    }
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $query_payment = "INSERT INTO journal (datepaid, bcode, icode, name, description, category, amount, type, typepayment)
                 VALUES (?,?,?,?,?,?,?,?,?);";

    $sth = $pdo->prepare($query_payment);
    $array = array($datepaid, $bcode, $icode, $name, $description, $category, $amount,
        $type, $typepayment);

    //$result_payment = pg_Exec($conn, $query_payment);
    if ($amount > 0) {
        $sth->execute($array);
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {

            $result = 'Error' . $stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>' . $query_payment;
            exit;
        } else {
            if (strtoupper($category) == 'GIFT CARD') {
                $result = 'A Gift Card Credit of $' . $amount . ' has been added';
            } else {
                $result = 'The Membership fee of ' . $amount . ' has been added';
            }
        }
    } else {
        $result = 'Amount needs to be greater than 0';
    }

 
    return $result;
}

function get_type($membertype) {
    //include( dirname(__FILE__) . '/../../connect.php');
    $sql = "SELECT * FROM membertype 
          WHERE (membertype)='" . $membertype . "'";
    $conn = pg_connect($_SESSION['connect_str']);
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $renewal_fee = $row['renewal_fee'];
    $description = $row['description'];
    $levy = $row['bond'];
    return array('renewal' => $renewal_fee, 'description' => $description, 'levy' => $levy);
}

function replace_bookmark($message, $id) {
    $generate_username = 'No';
    //include( dirname(__FILE__) . '/../../connect.php');
    $sql = "select firstname, emailaddress, expired, rostertype,membertype,partnersname,rostertype5,pwd,surname, partnersname, partnerssurname,
    (select description from membertype where borwrs.membertype = membertype.membertype) as mem_description,
    (select renewal_fee from membertype where borwrs.membertype = membertype.membertype) as mem_cost 
    from borwrs where id = " . $id . ";";
    $libraryname = $_SESSION['settings']['libraryname'];
    $_SESSION['library_code'] = $_SESSION['dbuser'];
    //echo $conne;
    $conn = pg_connect($_SESSION['connect_str']);
    $result = pg_Exec($conn, $sql);
    $numrows = pg_numrows($result);

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result, $ri);
        $firstname = $row['firstname'];
        $username = $row['rostertype5'];
        $surname = $row['surname'];
        $pwd = $row['pwd'];
        $email = $row['emailaddress'];
        $expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
        $mem_description = $row['mem_description'];
        $mem_cost = $row['mem_cost'];
        $partnersname = $row['partnersname'];
        $partnerssurname = $row['partnerssurname'];
        if ($surname == $partnerssurname) {
            $longname = $firstname . ' and ' . $partnersname . ' ' . $surname;
        } else {
            if ($partnersname != '') {
                $longname = $firstname . ' ' . $surname . ' and ' . $partnersname . ' ' . $partnerssurname;
            } else {
                $longname = $firstname . ' ' . $surname;
            }
        }
        if (($username == '') || $username == $_SESSION['library_code']) {
            $generate_username = 'Yes';
            $length_surname = strlen($surname);
            if ($length_surname < 10) {
                $surname = pad_surname($surname);
            } else {
                $surname = substr($surname, 0, 10);
                $length_surname = 10;
            }
            $surname = strtolower($surname);
            $username = rtrim($surname, "%") . $id;
            $username = str_replace(" ", "", $username);
        }
        if (($pwd == '') || $pwd == 'mibase') {
            $pwd = generateStrongPassword();
        }
    }
    $format_expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
    $children_txt = get_children($id);
    //$email = 'michelle@mibase.com.au';
    $output = $message;
    $output = str_replace("[firstname]", $firstname, $output);
    $output = str_replace("[username]", $username, $output);
    $output = str_replace("[pwd]", $pwd, $output);
    $output = str_replace("[email]", $email, $output);
    $output = str_replace("[expired]", $expired, $output);
    $output = str_replace("[mem_description]", $mem_description, $output);
    $output = str_replace("[subdomain]", $_SESSION['library_code'], $output);
    $output = str_replace("[mem_cost]", $mem_cost, $output);
    $output = str_replace("[libraryname]", $libraryname, $output);
    $output = str_replace("[subdomain]", $_SESSION['library_code'], $output);
    $output = str_replace("[children]", $children_txt, $output);
    $output = str_replace("[longname]", $longname, $output);
    if ($generate_username == 'Yes') {
        $output .= update_password($id, $pwd, $username);
    }

    return array('message' => $output, 'email' => $email, 'libraryname' => $libraryname);
}

function send_email($to, $subject, $message, $header, $param) {
    if ($to == '') {
        $output = '<br>Email not sent, email is blank.<br>';
    } else {
        $script_start = '<html>
<body>
<style>
p {
    font-size: 14px;
    margin: 0;
    padding: 0;
    border: 0;
}
</style>';
        $script_end = '</body></html>';
        $message = $script_start . $message . $script_end;
        $output = @mail($to, $subject, $message, $header, $param);
        //@mail('michelle@mibase.com.au',$template['subject'], $template['message'], $email['header'], $email['param']);
    }
    return $output;
}

function get_header($libraryname, $email_from) {
    $reply_to = $email_from;
    if ($email_from == '') {
        $email_from = 'noreply@mibase.com.au';
    }
    $mibase_email = 'noreply@mibase.com.au';
    $email_from = $mibase_email;
    $headers = '';
    $headers .= "From: " . $libraryname . " <" . $mibase_email . ">\r\n";
    $headers .= "Reply-To: " . $libraryname . " <" . $reply_to . ">\r\n";
    $headers .= "Cc: " . $libraryname . " <" . $reply_to . ">\r\n";
    $headers .= "Return-Path: " . $email_from . "\r\n";
    $headers .= "Organization: " . $libraryname . "\r\n";
    //$headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    $param = "-f" . $email_from;

    return array('header' => $headers, 'param' => $param);
}

function pad_surname($string) {
    $length = 10;
    $out_string = '';
    for ($x = 0; $x < $length; $x++) {
        $char = substr($string, $x, 1);
        if ($char != '') {
            $out_string .= $char;
        } else {
            $out_string .= '%';
        }
    }

    return $out_string;
}

function generateStrongPassword($length = 10, $add_dashes = false, $available_sets = 'luds') {
    $sets = array();
    if (strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if (strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if (strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    if (strpos($available_sets, 's') == false)
        $sets[] = '!@#$%&*?';
    $all = '';
    $password = '';
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for ($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if (!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while (strlen($password) > $dash_len) {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

function update_password($memberid, $password, $username) {
    //include( dirname(__FILE__) . '/../../connect.php');
    $sql = "update borwrs set pwd = ?, rostertype5 = ?, modified = now() where id = ?;";
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $sth = $pdo->prepare($sql);
    $array = array($password, $username, $memberid);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $status = 'OK';
    } else {
        $status = '<br>Username and password has been created: ' . date("Y-m-d H:i:s") . '<br>';
    }
    return $status;
}

function get_children($borid) {
    //include( dirname(__FILE__) . '/../../connect.php');
//include( dirname(__FILE__) . '/children/new_form.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $query = "SELECT * from children where id = " . $borid . " ORDER by d_o_b";
    $result = pg_exec($conn, $query);
    $numrows = pg_numrows($result);
    $children_txt = '';



    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = pg_fetch_array($result, $ri);

        
        $notes = $row['notes'];
        $alert_txt = null;
        if ($row['alert'] == 't') {
            $alert_txt .= 'Yes';
        } else {
            $alert_txt .= 'No';
        }

        $children_txt .= $row['child_name'] . ',';
    }


    if ($numrows == 0) {
        $children_txt = '';
    } else {
        if ($numrows == 1) {
            $children_txt = rtrim($children_txt, ",");
            //$children_txt = str_replace(',', ' and ', $children_txt);
            //$children_txt = ' and ' . $children_txt;
        } else {
            $children_txt = rtrim($children_txt, ",");
            $search = ',';
            $replace = ' and ';
            $children_txt = strrev(implode(strrev($replace), explode($search, strrev($children_txt), 2)));
            $children_txt = ', ' . $children_txt;
        }
    }
    return $numrows . ' ' . $children_txt;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
