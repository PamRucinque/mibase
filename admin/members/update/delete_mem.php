<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>



    <body id="main_body" >
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');

            if (isset($_GET['borid'])) {
                $id = $_GET['borid'];
            } 
            $password = $_SESSION['settings']['password'];
            $ref = 'member_detail.php?borid=' . $_SESSION['borid'];

            echo "<h2><br><a href='" . $ref . "' class ='button1_logout'>Cancel</a>";
            include( dirname(__FILE__) . '/get_member.php');
            $loans = check_loans($id);
            if ($loans == 0) {
                echo '<font color="red"><br> WARNING - YOU ARE ABOUT TO DELETE MEMBER:  </font><font color="blue">' . $borid . ': ' . $longname . '</font></h1><br>';
                include( dirname(__FILE__) . '/delete_form.php');
            } else {
                echo '<h2><font color="red">This Member has toys on loan and cannot be deleted.</font></h2>';
            }

            //include( dirname(__FILE__) . '/toy_detail.php');

            if ($password == '') {
                $password = 'mibase';
            }


            if (isset($_POST['submit'])) {

                if (isset($_GET['borid'])) {
                    // get the 'id' variable from the URL
                    //$id = $_GET['id'];
                    $query = "DELETE FROM children WHERE id = " . $_POST['borid'] . ";";
                    $query .= "DELETE FROM event WHERE memberid = " . $_POST['borid'] . ";";
                    $query .= "DELETE FROM journal WHERE bcode = " . $_POST['borid'] . ";";
                    $query .= "UPDATE Roster set member_id = 0 WHERE complete = FALSE and member_id = " . $_POST['borid'] . ";";
                    $query .= "DELETE FROM borwrs WHERE id = " . $_POST['borid'] . ";";
                    $query .= "DELETE from transaction WHERE borid = " . $_POST['borid'] . " and return is not null;";
                    if (($_POST['password'] == $password)) {
                        
                        $conn = pg_connect($_SESSION['connect_str']);

                        $result = pg_exec($conn, $query);
                        if (!$result) {
                            echo "An INSERT query error occurred.\n";
                            echo $query;
                            //exit;
                        } else {
                            $edit_url = 'member_detail.php?id=' . $_GET['borid'];
                            echo "<br><p>The record was successfully deleted.</p><br>";
                            echo '<a class="button1" href="../members.php">OK</a>';
                            $_SESSION['del_Status'] = '';
                        }

                        pg_FreeResult($result);
                        pg_Close($conn);
                    } else {
                        $edit_url = 'member_detail.php?id=' . $_GET['borid'];
                        echo "<br><p>Incorrect Password.</p><br>";
                        echo '<a class="button1" href="../members.php">OK</a>';
                    }
                }
            }
            ?>
        </div>
    </body>
</html>

<?php

function check_loans($id) {
    //include( dirname(__FILE__) . '/../../connect.php');
    
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "select * from transaction where borid=" . $id . " and return is null;";
    $result = pg_Exec($conn, $sql);
    $numrows = pg_numrows($result);
    //$trans = 4;
    return $numrows;
}
