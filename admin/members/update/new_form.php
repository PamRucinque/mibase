<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

$today = date('Y-m-d');
$oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 365 day"));
?>

<body id="main_body">
    
        <style>
            
            input {
                
                border-radius: 10px;
                height: 10%;
                padding-left: 10px;
                border: 1px solid gray;
                -webkit-border-top-left-radius: 0px;
                
            }
            
            .required {
               
                padding-left: 20px;
                border: 1px solid red;
                
            }
            
            input:hover {
                transition: all 0.8s ease;
                transform: scale(1.2);
                border: 1px solid green;
            }
            
           select {
                
                border-top-right-radius: 10px;
                height: 10%;
                padding-left: 5px;
                border: 1px solid gray;
                background-color: white;
            }
            
            select:hover {
                transition: all 0.8s ease;
                transform: scale(1.1);
                border: 1px solid green;
            }
            
            
            input:focus {
                transition: all 0.8s ease;
                border: 1px solid gray;
                transform: scale(1.2);
                background-color: lightgrey;
            }

            
            td {
                
               padding-top: 5px;
                
            }
            
            
            .line1 {
                height: 5px;
                border-top: 5px black;
                width: 60%;
                margin-left: 30px; margin-right: 30px;
            }
            
            requiredtext {
                color: red;
            }
            
            .circlebtn {
                background-color: lightslategray;
                border-style: none;
                border-radius: 100%;
                height: 20px;
                width: 20px;
            }
            

        </style>
        
        
    <script type="text/javascript" src="../../js/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
    <link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

    <script type="text/javascript">
        
        $(function () {
            var pickerOpts = {
                dateFormat: "d MM yy",
                showOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "2010:+nn"

            };
            $("#expired").datepicker(pickerOpts);
            $("#joined").datepicker(pickerOpts);
            $("#renewed").datepicker(pickerOpts);
        });
        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault)
                    theEvent.preventDefault();
            }
        }
        function get_postcode(str) {
            var input = str;

            if (str == "") {
                document.getElementById("postcode").innerHTML = "";
                return;
            }

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {

                document.getElementById("postcode").value = xmlhttp.responseText;

            }

            xmlhttp.open("GET", "data/getpostcode.php?q=" + str, true);
            xmlhttp.send();
        }
        
       /*
                    
        function secondmemeber() {
               if ($("secmember").visability === "hidden") {
                   document.getElementById("secmember").style.visability = "visible";
               ] else {
                   document.getElementById("secmember").style.visability = "hidden";
               }
           };

        */
       
    </script>
    
    <?php
    //include( dirname(__FILE__) . '/../../get_settings.php');

    if (isset($_SESSION['suburb_title']) && $_SESSION['suburb_title'] == '') {
        // if ($suburb_title == '') {
        $suburb_title = 'Suburb';
    }
    if (isset($_SESSION['city_title']) && $_SESSION['city_title'] == '') {
        //if ($city_title == '') {
        $city_title = 'City';
    }
    ?>
    <div id="form_container">


        <form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="
        <?php echo 'new.php?';
        if (isset($_POST['id'])) {
            echo 'id=' . $_POST['id'];
        } ?>

              ">
            
            <div id="form" style="background-color:white;" align="left">
                
                <table width="100%"><tr><td>
                            <td align="right"><input id="saveForm" class="button1_red" style="background: white; color: grey;" type="submit" name="submit" value="Save Member"/>
                            <h2 align="left">Member Details:</h2> <tr style="border-top:1px solid black;">
                            <td colspan="100%"></td> </td>
                            
                        </td></tr></table>
                <table>
                    <tr><td width="25%">First Name: <requiredtext>(required)</requiredtext><br>
                            <input type="Text" name="firstname" align="LEFT" required="Yes" size="25" value="" style="border-color: red;"></input></td>
                        <td width="30%">Surname: <requiredtext>(required)</requiredtext><br>
                        <input type="Text" name="surname" align="LEFT" required="Yes" size="30" value="" style="border-color: red;"></input><br></td>
                        <td>Mobile: <requiredtext>(required)</requiredtext><br>
                            <input type="Text" name="mobile1" align="LEFT" size="20" value="" style="border-color: red;"></input><br></td>
                        <td width="35%">Email: <requiredtext>(required)</requiredtext><br>
                            <input type="Text" name="emailaddress" align="LEFT"  size="35" value="" style="border-color: red;"></input></td>
                    
                    </tr>
                    
                    <tr><td width="35%">Home Phone:<br>
                            <input type="Text" name="phone" align="LEFT"  size="25" value=""></input></td>
                        <td width="35%">Date Joined:<br>
                            <input type="text" name="joined" id="joined" align="LEFT"  size="25" value="<?php echo $today; ?>"></input></td>
                        <td>Location:<br> <?php include( dirname(__FILE__) . '/get_location.php'); ?></td>
                    </tr>
                    
               
                <th>
                <!-- <button class="circlebtn"  style="margin-top: 50px;" onclick="secondmember()">+</button -->
                <h2 style="">2nd Member Details:</h2> 
                <tr style="border-top:1px solid gray">
                <td colspan="100%"></td>
                </th>   
                <secmember style="visibility: visible;">
                   
               
                        <tr><td width="25%">First Name:<br>
                                <input type="Text" name="partnersname" align="LEFT" size="25" value=""></input></td>
                            <td width="30%">Surname:<br>
                                <input type="Text" name="partnerssurname" align="LEFT"  size="30" value=""></input><br></td>
                            <td width="25%">Mobile:<br>
                                <input type="Text" name="mobile2" align="LEFT"  size="20" value=""></input><br></td>
                            <td width="35%">Email:<br>
                                <input type="Text" name="email2" align="LEFT"  size="35" value=""></input></td>
                        </tr>
                        
                        
                </table>
                </secmember>
                
              
                
                
                <table>
                    <h2>Contact Details:</h2> <tr style="border-top:1px solid black">
                    <td colspan="100%"></td>
                    
                    <tr><td width="40%">Address:<br>
                            <input type="Text" name="address" align="LEFT"  size="35" value=""></input></td>
                        <td>Address 2:<br><input type="Text" name="address2" align="LEFT" size="35" value=""></input></td></tr>
                    <tr>
                    
                         
                        <?php
                        if (isset($_SESSION['suburb_title']) && $_SESSION['suburb_title'] == 'City'){
                       // if ($suburb_title == 'City') {
                            
                        } else {
                            echo '<td>';
                            echo $suburb_title . '<br>';
                            include( dirname(__FILE__) . '/get_city.php');
                            echo '</td>';
                        }
                        if (!(isset($_SESSION['hide_city']) && $_SESSION['hide_city'] == 'Yes')) {
                            //If ($hide_city != 'Yes') {
                            echo '<td width="40%">City:<br>';
                            include( dirname(__FILE__) . '/get_suburb.php');
                            echo '</td>';
                        }
                        //else {
                        //  //echo '<td></td>';
                        //}
                        ?>

                        <td  align='top' width="30%"><br>Postcode:<br>
                            <input type="Text" name="postcode" id="postcode" align="LEFT"  size="4" value="" style="width: 40%;"></input><br><br></td>
                        <td  align='top' width="30%"><br>State:<br>
                            <input type="Text" name="state" align="LEFT"  size="4" value="" style="float: left;  width: 140%;"></input><br><br></td>
                    </tr>

                    <tr><td><br>Member Type: <requiredtext>(required)</requiredtext><br > <?php include( dirname(__FILE__) . '/get_memtype.php'); ?></td>
                        <td><br>Member Status: <br>
                            <select id="member_status"  style="width: 40%;" name="member_status">
                                <option value='ACTIVE' selected="selected">ACTIVE</option>
                                <option value="ACTIVE" >ACTIVE</option>
                                <option value="LOCKED" >LOCKED</option>
                                <option value="RESIGNED" >RESIGNED</option>
                            </select></td>
                        <td><br>Source: <br><?php include( dirname(__FILE__) . '/get_source.php'); ?><br></td>


                    </tr>

                </table>
                
                <table>
                    <h2>Roster:</h2> 
                    <td colspan="100%"></td>
                    
                    <tr>            
                        <td width="30%"><br>    Pref 1: <?php include( dirname(__FILE__) . '/get_rostertype1.php'); ?></td>
                        <td width="30%"><br>    Pref 2: <?php include( dirname(__FILE__) . '/get_rostertype2.php'); ?> </td>
                        <td width="30%"><br>    Pref 3: <?php include( dirname(__FILE__) . '/get_rostertype4.php'); ?></td>
                    </tr> 
                </table>
                    
                <table>
                    <h2>Extra information: </h2> <tr style="border-top: 1px solid black">
                    <td colspan="100%"></td>
                    
                    
                            
                            <tr><td colspan="2"><br><?php
                            if ($nationality_label == '') {
                                echo 'Language other than English: <br>';
                            } else {
                                echo $nationality_label . '<br>';
                            }

                            include( dirname(__FILE__) . '/get_rostertype3.php');
                            ?>

                        </td></tr>
                        <tr><td width="60%"><label><br>Notes: </label><br>
                            <textarea id="notes" name="notes" rows="3" cols="35"></textarea></td>
                        <td><label><br>Alerts: </label><br>
                            <textarea id="alertmem" name="alertmem" rows="3" cols="35"></textarea></td>

                    </tr>
                    <tr><td><label><br>Skills / Occupation: </label><br>
                            <textarea id="skills" name="skills" rows="3" cols="35"></textarea></td>
                        <td><label><br>How can you help? </label><br>
                            <textarea id="help" name="help" rows="3" cols="35"></textarea></td>

                    </tr>
                    <tr><td width="25%">
                            <?php
                            if ($user1_borwrs == '') {
                                echo 'User Defiend: ';
                            } else {
                                echo $user1_borwrs;
                            }
                            ?>
                            <br>
                            <input type="Text" name="user1" align="LEFT" size="45" value=""></input></td>
                        <td>Id (License#, passport, medicare#)<br><input type="Text" id="license" name="license" align="LEFT" size="55" value="<?php echo $license; ?>"></input></td>

                    </tr>
                    

                </table>
                </td></tr>
                </table>

                <input type="hidden" name="id" id="id" value="<?php echo $newmemid; ?>"> 
                
            </div>
            </body>




