<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

$query_unused = "select * from generate_series(1, (SELECT MAX(id) FROM toys where 
    category = '" . $category . "')) as unused
  where unused not in (select id from toys Where category = '" . $category . "');";
//echo $query_unused;
echo 'Lowest 20 unused numbers in category ' . $category . ': ';

//include( dirname(__FILE__) . '/../../connect.php');
$result = pg_Exec($conn, $query_unused);
//echo $connection_str;

$numrows = pg_numrows($result);
$numrows = 20;
//echo $query;
//$result_txt =  '<table border="1"><tr>';

for ($ri = 0; $ri < $numrows; $ri++) {
    
    $row = pg_fetch_array($result, $ri);
    echo $row["unused"] . ', ';   
}

?>
