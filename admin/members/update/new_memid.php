<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');

//get settings




$query_new = "SELECT MAX(id) as memid FROM borwrs where id != 9999;";
$newmemid = 0;

$conn = pg_connect($_SESSION['connect_str']);
$nextval = pg_Exec($conn, $query_new);
//echo $connection_str;
$row = pg_fetch_array($nextval, 0);
$newmemid = $row['memid'];
$newmemid = $newmemid + 1;
//$newmemid = 400;

if (isset($_SESSION['reuse_memberno']) &&  $_SESSION['reuse_memberno'] == 'Yes') {
    for ($ri = 1; $ri < $newmemid; $ri++) {
        $exits = member_exits($ri);
        if ($exits == 0) {
            $newmemid = $ri;
            $ri = $newmemid;
        }
    }
}
if (isset($_SESSION['reuse_memberno']) &&  $_SESSION['reuse_memberno'] == 'Yes') {
    if (isset($_POST['toyid'])) {
        $try =$_POST['toyid'];
        if (!member_exits($try)){
          $newmemid = $try;  
        }else{
            echo '<br><h1><font color="red">Sorry, member number ' .  $try . '  has already been allocated.</font></h1><br>';
        }
    }
    include( dirname(__FILE__) . '/new_form_memid.php'); 
}else{
   echo '<br><h3>Next id: <font color="blue">' . $newmemid . '</font>'; 
}
   

if (isset($_SESSION['reuse_memberno']) &&  $_SESSION['reuse_memberno'] == 'Yes') {
    include( dirname(__FILE__) . '/unused_id.php');

}


function member_exits($id) {
    //include( dirname(__FILE__) . '/../../connect.php');
    $query_new = "SELECT * from borwrs where id = " . $id . ";";
    $result = pg_Exec($conn, $query_new);
    $numrows = pg_numrows($result);
    return $numrows;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
