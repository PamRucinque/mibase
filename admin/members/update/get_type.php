<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */


$conn = pg_connect($_SESSION['connect_str']);

$query = "SELECT * FROM membertype where membertype = '" . $membertype . "';";
//echo $query;
$result = pg_Exec($conn, $query);


$numrows = pg_numrows($result);
//echo '<option value="" selected="selected"></option>';

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    $description = $row['description'];
    $maxnounits = $row['maxnoitems'];
    $renewal_fee = $row['renewal_fee'];
    $returnperiod = $row['returnperiod'];
    $expiryperiod = $row['expiryperiod'];
    $duties = $row['duties'];
}



?>

