<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<style type="text/css">
    tr:hover { 
        color: red; }
</style>

<?php

$search = "";
if (isset($_POST['search'])) {
    $search = $_POST['search'];
}
$mem_type = '';
if (isset($_POST['membertype'])){
    $mem_type = $_POST['membertype'];
}
//
//"OR supplier LIKE '%" . $search . "%' " .

$search = strtoupper($search);
//error message (not found message)

$query = "SELECT *
FROM borwrs
WHERE (upper(surname) LIKE '%" . $search . "%' " .
        "OR upper(firstname) LIKE '%" . $search . "%' " .
        "OR id::varchar(255) LIKE '%" . $search . "%' " .
        " OR upper(partnersname) LIKE '%" . $search . "%' " .
        ") AND (membertype  LIKE '%{$mem_type}%')
AND (member_status = 'LOCKED') 
ORDER BY surname, firstname ASC;";

//$query = "SELECT * FROM toys ORDER by id ASC;";
//echo $query;

$XX = "No Record Found";
//print $query;
//$query = "SELECT * FROM hm_claims order by id";
$total = 0;
//include( dirname(__FILE__) . '/../connect.php');
$result_list = pg_exec($conn, $query);
$numrows = pg_numrows($result_list);
//echo $query;
//$result_txt =  '<table border="1"><tr>';
$result_txt = '';


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_list, $ri);
    $total = $total + 1;
    $expired = $row['expired'];
    $format_expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);

    $borid = $row["id"];
    $firstname = $row["firstname"];
    $membertype = $row["membertype"];
    $surname = $row["surname"];
    $partnersname = $row["partnersname"];
    $email = $row["emailaddress"];
    $phone = $row["phone"];
    $mobile = $row["mobile1"];
    $member_status = $row["member_status"];
    $address = $row['address'];
    $email_link = '<a href="mailto:' . $email . '">send</a>';
    $date_expired = date_create_from_format('d-m-Y', $format_expired);

    $result_txt .= '<tr border="1"><td border="1" width="50">' . $borid . '</td>';
    $result_txt .= '<td align="left">' . $surname . '</td>';
    $result_txt .= '<td align="left">' . $firstname . '</td>';
    $result_txt .= '<td align="left">' . $partnersname . '</td>';
    $result_txt .= '<td align="center">' . $format_expired . '</td>';
    //$result_txt .= '<td align="left">' . $email . '</td>';
    //$result_txt .= '<td align="left">' . $email_link . '</td>';
    $result_txt .= '<td  width="100">' . $mobile . '</td>';
    $result_txt .= '<td  width="100">' . $phone . '</td>';
    $result_txt .= '<td>' . $membertype . '</td>';
    $result_txt .= '<td>' . $member_status . '</td>';
    //$result_txt .= '<td width="50"><a href="toy_detail.php?idcat=' . $idcat . '">View</a></td>';
    $result_txt .= '<td><a class="button_small_red" href="update/member_detail.php?borid=' . $borid . '"/>View</a>';
}
$result_txt .= '</tr></table>';

//print '<br/>' . $query;
//print '<br/>' . $location;
//below this is the function for no record!!
//end

print '<div id="open"><table width="100%"><tr><td width= 50%><h1 align="left"></h1></td><td><h1 align="right">Total: ' . $total . '</h1></td><tr></table></div>';
print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
print '<tr style="color:green"><td>id</td><td>Surname</td><td>Firstname</td><td>Partner</td><td>Expired</td><td>Mobile</td><td width="110">Phone</td><td>Type</td><tr>';

print $result_txt;
?>