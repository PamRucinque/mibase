<?php

require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
         <script>

            function go() {
                var borids = "";

                $("tr.member").each(function () {
                    $this = $(this);
                    var borid = $this.find("td.borid").html();
                    var value = $this.find("td :checkbox").is(':checked');
                    if (value) {
                        borids += "'" + borid + "',";
                    }
                });

                if (borids.length > 0) {
                    borids = borids.substring(0, borids.length - 1);
                    $("#borid_list").val(borids);
                    //document.forms["reports"].submit();
                    //document.reports.submit();
                    return true;
                } else {
                    alert('No members selected!');
                    return false;
                }

                //alert(idcats);
            }

        </script>
        <script>

            function test() {
                var borids = "";
                $("tr.member").each(function () {
                    $this = $(this);
                    $this.find("td :checkbox").prop("checked", true);
                });
            }
        </script>
    </head>

    <body>
        <?php $total = 0; ?>
        <div class="container-fluid">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            include( dirname(__FILE__) . '/../header_detail/header_detail.php');
            ?>

            <form name="filter" id="filter" method="get" action="members.php" >    
                <table width=100% height="60px">
                    <tr bgcolor="lightgray">
                        <td width="10%" style="padding-right: 5px;"><label class="description" for="element_12">Category: </label><?php include( dirname(__FILE__) . '/get_membertype.php'); ?> </td>
                        <td width="10%"><label class="description" for="element_12">Order by: </label><?php include( dirname(__FILE__) . '/get_order.php'); ?> </td>

                        <td width="10%"><label class="description" for="search">Search String</label><input id="search" name="search"  type="text" maxlength="255"  value=""/></td>
                        <td width="40%" align="bottom"><label class="description" for="search"></label><br><input class="button1_blue" type="submit" name ="submit_search" id="submit_search" 
                                                                                                                  value="Show All" onclick="test();"></td>
                        </form>
                        <td>
                            <table><tr><td width="5%"></td>
                                    <td width="50%">
                                        <?php include( dirname(__FILE__) . '/../reports/baglabels_members.php'); ?>
                                    </td></tr>
                            </table>  

                        </td> 

                    </tr>
                </table>

                <?php
//if (isset($_POST['submit'])) {
                if ($_SESSION['level'] == 'admin') {
                    include( dirname(__FILE__) . '/result.php');
                } else {
                    echo '<br><br><h2>The members list has been disabled for volunteer login.</h2>';
                }

// } else {
//    $location = 'ALL';
//    $assessor = '';
//}
//print 'hello'; }
                ?>
        </div>

    </body>

</html>
