<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

if (isset($_GET['term'])) {
    $query = '';
    $return_arr = array();
    $q = strtolower($_GET["term"]);
    if (preg_match('/^[0-9]*$/', $q)){
           $query = "SELECT id, firstname, surname, partnersname, partnerssurname FROM borwrs WHERE to_char(id, '99999') LIKE '%" . $q . "%' AND member_status='ACTIVE' ORDER by id, surname, firstname ASC;";
 
    }else{
             $query = "SELECT id, firstname, surname, partnersname, partnerssurname FROM borwrs 
               WHERE 
               to_char(id, '99999') LIKE '%" . $q . "%' OR 
               ((upper(replace(surname, '''', '`')) LIKE '" . strtoupper($_REQUEST['term']) . "%') "
                . "OR (upper(firstname) LIKE '" . strtoupper($_REQUEST['term']) . "%') "
                . "OR (upper(partnerssurname) LIKE '" . strtoupper($_REQUEST['term']) . "%') "
                . "OR (upper(partnersname) LIKE '" . strtoupper($_REQUEST['term']) . "%')) 
               AND member_status='ACTIVE' ORDER by surname, firstname ASC;";

 
    }
    //echo $query;

    $conn = pg_connect($_SESSION['connect_str']);
    $result_select = pg_Exec($conn, $query);
    $numrows = pg_numrows($result_select);
//echo '<option value="" selected="selected"></option>';
  
    
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result_select, $ri);
        $p_surname = $row['partnerssurname'];
        
        $return_arr[] = $row['id'] . ': ' . $row['surname'];
        $longname = $row['firstname']. ' '. $row['surname'];
        if (($row['surname'] != $p_surname)&&($p_surname != '')){
            $longname .=  ' & ' . $row['partnersname'] . ' ' . $p_surname;
        }
        $longname .=   ' : ' . $row['id'];
        $results[] = array('label' => $longname, 'value' => $row['id']);
    }

    /* Toss back results as json encoded array. */
    echo json_encode($results);
}
//echo 'hello';
?>