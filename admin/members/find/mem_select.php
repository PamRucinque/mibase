<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */

//included in /members/find/find_member.php

require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">

    <script>
        function setFocus()
        {
            document.getElementById("memberid").focus();
        }
        $(function () {

            //autocomplete
            $(".auto2").autocomplete({
                source: "../find/search_mem.php",
                autoFocus: true,
                select: function (event, ui) {

                    var selectedObj = ui.item;

                    document.getElementById('borid').value = selectedObj.value
                    document.forms["change"].submit();
                }

            });

        });
        function login() {
            if (event.keyCode == 13) {
                alert("You hit enter!");
            }
        }
    </script>
</head>
<body> 

    <table bgcolor="lightgrey"><tr>
            <td width="10px"></td>
            <td>
                <form  id="scan_mem" method="post" action="member_detail.php" width="100%"><label>Member id: </label><br>
                    <input align="center" type="text" name="memberid" id ="memberid" size="10" value="" onchange='this.form.submit()'></input>
                </form>      
            </td>
            <td width="10px"></td>
            <td>
                <form id="change" method="get" action="member_detail.php">
                    <label>Select Member:</label><br>
                    <input type='text' name='longname' id='longname' value='' class='auto2' onKeyPress="login()"></p>
                    <input type="hidden" id="borid" name ="borid" />
                </form></td>
            </body>
            </html>
    </table>