<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <body id="main_body" >
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');

            $_SESSION['error'] = '';
            echo "<br>" . "<a class='button1_red' href='event_types.php'>Back to Event Types</a>" . "<br><br>";
            include( dirname(__FILE__) . '/new_event_typeid.php');
            include( dirname(__FILE__) . '/new_form_event_type.php');

            if (isset($_POST['submit'])) {
                //$toyid = sprintf("%02s", $toyid);

                $code = 'UD_' . pg_escape_string($_POST['code']);
                $amount = pg_escape_string($_POST['amount']);
                $description = pg_escape_string($_POST['description']);

                $query_option = "INSERT INTO typeevent (id, type, amount, description, nohours )
         VALUES ({$neweventtypeid}, 
         '{$code}',{$amount},
         '{$description}', 1);";

                $result_new = pg_Exec($conn, $query_option);


                if (!$result_new) {
                    //echo "An INSERT query error occurred.\n";
                    $_SESSION['error'] = "An INSERT query error occurred.\n" . $query_option;
                }

// Get the last record inserted
// Print out the Contact ID
                else {
                    $_SESSION['error'] = '';
                    echo "<br>The record was successfully entered and the ID is: " . $neweventtypeid . "<a class='button1_red' href='event_types.php'>OK</a>" . "<br><br>";
                }
            }
            ?>

    </body>