<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../js/themes/base/jquery.ui.all.css" rel="stylesheet" />


<p><font size="2" face="Arial, Helvetica, sans-serif"></font></p>

<font></font>
<?php
echo "<br><a href='event_types.php' class ='button1_red'>Back to Options</a>";
?>
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'edit_event_type.php?id=' . $_GET['id']; ?>">

    <div id="form" style="background-color:lightgray;" align="left">

        <table align="top"><tr>
                <td><h2>Edit an Event Type:</h2></td>
                <td align="right"><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Save" /></td>
            </tr>
            <tr>
                <td>Code:<br><input type="Text" name="code" align="LEFT"  size="10" required readonly value="<?php echo $code; ?>"></input><br></td>
            </tr>
            <tr>
                <td>Description: (max length 100 chars):<br>
                    <textarea id="description" name="description" rows="3" cols="50" maxlength="100" required><?php echo $description; ?></textarea></td>
            </tr>

            <tr>
                <td>Amount:<br><input type="Number" name="amount" align="LEFT"  size="10" step ="0.1" value="<?php echo $amount ?>"></input><br></td>

            </tr>
            <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"> 
        </table>
    </div>
</form></p>

