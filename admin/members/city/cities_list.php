<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
//echo $_SESSION['mibase'] . '<br>';




include( dirname(__FILE__) . '/update_postcode.php');

//include( dirname(__FILE__) . '/../../connect.php');

$query = "SELECT * from city ORDER by city";
$conn = pg_connect($_SESSION['connect_str']);
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);

if ($numrows > 0) {
    echo '<h2> Suburbs: </h2>';
    echo '<p>To update the Postcode, type the new postcode in the yellow input box and press enter.</p>';
    echo '<table border="1" width="50%" style="border-collapse:collapse; border-color:grey">';
    echo '<tr style="color:green"><td>id</td><td>Suburb</td><tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {

//echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);
    $ref1 = '<td width="60px"><form action="" method="POST"><input type="hidden" name="id" value="' . $row['id'] . '">';
    $ref1 .='<input id="submit" name="submit" class="button_small_red"  type="submit" value="Delete" /></form></td>';

    echo '<td width="50px" align="center">' . $row['id'] . '</td>';
    echo '<td width="100px">' . $row['city'] . '</td>';
    echo '<td width="50px"><form action="cities.php" method="post"><input type="text" style="background-color:lightyellow;" name="postcode" id="postcode" value="' . $row['postcode'] . '" onchange="this.form.submit()" maxlength="4" size="4">'
    . '<input type="hidden" id="id" name="id" value="' . $row['id'] . '"></form></td>';
    echo '<td width="100px">' . $row['postcode'] . '</td>';

    echo $ref1;
    echo '</tr>';
}
echo '<tr height="0"></tr>';
echo '</table>';




