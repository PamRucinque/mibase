<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
//connect to MySQL

//include( dirname(__FILE__) . '/../connect.php');

if (!isset($_SESSION['idcat'])){
    $_SESSION['idcat'] = $_GET['idcat'];
    $idcat = $_SESSION['idcat'];
} 
    $_SESSION['idcat'] = strtoupper($_SESSION['idcat']);
    
    $query = "select toys.idcat,toyname, category, rent, transaction.due as due 
from toys LEFT JOIN transaction ON toys.idcat = transaction.idcat 
 WHERE upper(toys.idcat) = '" . $_SESSION["idcat"] . "' AND transaction.return is null";


//echo $query;
$result_toy = pg_Exec($conn, $query);
$numrows = pg_numrows($result_toy);
$status_txt = Null;


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_toy, $ri);


    $toy_id = $row['id'];
    //$idcat = $row['idcat'];
     $category = $row['category'];
    $toyname = $row['toyname'];
    $rent = $row['rent'];
    $user1 = $row['user1'];
    //$date_entered = date_format($row['date_entered'], 'd/m/y');
    $alert = $row['alert'];
     $lockdate = $row['lockdate'];
    $user1 = $row['user1'];
    $due = $row['due'];

    
    $format_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);
}
    pg_FreeResult($result_toy);
// Close the connection
    pg_Close($conn);
    
    session_start();
    $_SESSION['idcat'] = $idcat;

 



//echo 'Type Claim: ' . $type_claim;
//$date_submitted = $date_submitted[weekday]. $date_submitted[month] . $date_submitted[mday] . $date_submitted[year];
?>