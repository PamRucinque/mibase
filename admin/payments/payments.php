<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');
//$payment_description = '';
$invoices = $_SESSION['settings']['invoices'];
$receipt_email = $_SESSION['settings']['receipt_email'];
$payment_description = '';
$payment_amount = 0;
$type_payment = '';

?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <script>
            function get_report() {
                document.getElementById("print_receipt").submit();
            }
        </script>
    </head>
    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../menu.php');

            //echo $_SESSION['location'];
            $payment_amount = 0;

            //if ($branch == 'admin') {
            include( dirname(__FILE__) . '/../header_detail/header_detail.php');
            //}
            include( dirname(__FILE__) . '/functions/functions.php');
            //include( dirname(__FILE__) . '/../get_settings.php');
            //include( dirname(__FILE__) . '/get_settings.php');


            $_SESSION['payment_status'] = '';
            if (isset($_POST['payment'])) {
                include( dirname(__FILE__) . '/data/get_payment_amount.php');
                $payment_type = $_POST['payment'];
            }

            if (isset($_POST['new_payment'])) {

                //$amount = $_POST['amount'];
                if ($_POST['payment'] == '') {
                    $_POST['payment'] = 'Payment - Thankyou';
                }
                $description = $_POST['desc'];
                $borname = str_replace("'", "`", $_POST['borname']);
                $amount = $_POST['amount'];
                $_SESSION['payment_status'] = add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $_POST['payment'], $amount, 'CR', $_POST['typepayment']);
            }
            if (isset($_POST['refund'])) {
                $amount = -1 * $_POST['amount'];
                $description = 'Refund';
                if ($_POST['payment'] == '') {
                    $category = $_POST['payment'];
                } else {
                    $category = 'REFUND: ' . $_POST['payment'];
                }

                $borname = str_replace("'", "`", $_POST['borname']);
                $_SESSION['payment_status'] = add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $amount, 'DR', 'DEBIT');
                $id = $_POST['id'] + 1;
                $_SESSION['payment_status'] = add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $amount, 'CR', $_POST['typepayment']);
            }



            if (isset($_POST['debit_annual'])) {

                $membertype = get_membertype($_POST['bcode']);
                $type = get_type($membertype);
                $payment_amount = $type['renewal'];
                $description = 'Membership fees';
                $category = $type['description'];
                $borname = str_replace("'", "`", $_POST['borname']);
                $_SESSION['payment_status'] = $membertype . ' ' . add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $payment_amount, 'DR', 'SUBS');
                //include( dirname(__FILE__) . '/levy.php');
// Close the connection
            }
            if (isset($_POST['debit_levy'])) {
                $membertype = get_membertype($_POST['bcode']);
                $type = get_type($membertype);
                $levy_amount = $type['levy'];
                $description = 'Levy';
                $category = 'Membership Levy';
                $borname = str_replace("'", "`", $_POST['borname']);
                $_SESSION['payment_status'] = 'Levy amount debited: ' . add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $levy_amount, 'DR', 'SUBS');
//
// Close the connection
            }
            if (isset($_POST['debit_missing'])) {

                $payment_amount = get_missing_part();
                //$payment_amount = 1;
                $description = 'Missing Part';
                if ($_POST['desc'] == '') {
                    if ($idcat != '') {
                        $description = $idcat . ': ' . $toyname;
                    } else {
                        $description = 'No Current Toy';
                    }
                } else {
                    $description = $_POST['desc'];
                }

                $category = 'Fine: Missing Part';

                $borname = str_replace("'", "`", $_POST['borname']);
                $_SESSION['payment_status'] = 'Missing Part amount debited: ' . add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $payment_amount, 'DR', 'FINE');
// Close the connection
            }


            if (isset($_POST['debit'])) {

                $amount = $_POST['amount'];
                $description = $_POST['desc'];
                $category = $_POST['payment'];
                $borname = str_replace("'", "`", $_POST['borname']);
                $_SESSION['payment_status'] = add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $amount, 'DR', 'DEBIT');

// Close the connection
            }


            include('data/get_member.php');
            include('find/find_member.php');
            echo '<font color="red">' . $_SESSION['payment_status'] . '</font>';
            //include ('data/get_toy.php');
            echo '<table><tr><td>';
            include( dirname(__FILE__) . '/../reports/payment_receipt.php');
            echo '</td>';
            if ($invoices == 'Yes') {
                echo '<td valign="bottom">';
                include( dirname(__FILE__) . '/../reports/payment_invoice.php');
                echo '</td>';
            }
            if ($receipt_email == 'Yes1') {
                echo '<td valign="bottom">';
                include( dirname(__FILE__) . '/email_receipt.php');
                echo '</td>';
            }
            echo '</tr></table>';
            include( dirname(__FILE__) . '/new_form_payment.php');
            //echo $_SESSION['payment_status'];
            include ('result.php');
            //echo $query_payment;
            //pg_Close($conn);
            ?>
        </div>
    </body>
</html>