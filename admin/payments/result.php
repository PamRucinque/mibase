<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

$query = "SELECT *
        FROM journal
        WHERE (bcode = {$_SESSION['borid']})" .
        " ORDER BY id desc;";


$XX = "No Record Found";
//print $query;
//$query = "SELECT * FROM hm_claims order by id";
$total = 0;
$balance = 0;
//include( dirname(__FILE__) . '/../connect.php');
$result_list = pg_exec($conn, $query);
$numrows = pg_numrows($result_list);
//echo $query;
//$result_txt =  '<table border="1"><tr>';
$result_txt = '';
$cr = 0;
$dr = 0;


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_list, $ri);
    $total = $total + 1;


    $id = $row['id'];
    $datepaid = $row['datepaid'];
    $borid = $row["bcode"];
    $description = $row["description"];
    $memname = $row["name"];
    $typepayment = $row["typepayment"];
    $type = $row['type'];
    $category = $row['category'];
    $location = $row['location'];

    $format_datepaid = substr($row['datepaid'], 8, 2) . '-' . substr($row['datepaid'], 5, 2) . '-' . substr($row['datepaid'], 0, 4);
    $result_txt .= '<tr border="1" class="item" id="red"><td border="1" width="80px">' . $format_datepaid . '</td>';

    if ($type == 'CR') {
        $result_txt .= '<td class="idcat" width="50px" align="center">' . $id . '</td><td width="50px" align="center"><input type="checkbox" ></td>';
    } else {
        if ($invoices == 'Yes') {
            $result_txt .= '<td class="idcat" width="50px" align="center">' . $id . '</td><td width="50px" align="center"><input type="checkbox" ></td>';
        } else {
            $result_txt .= '<td class="idcat" width="50px" align="center">' . $id . '</td><td width="50px" align="center"></td>';
        }
    }
    $result_txt .= '<td align="left">' . substr($location, 0, 3) . '</td>';
    //$result_txt .= '<td align="left">' . $memname . '</td>';
    $result_txt .= '<td align="left">' . $category . '</td>';
    $result_txt .= '<td align="left">' . $description . '</td>';
    $result_txt .= '<td align="left">' . $typepayment . '</td>';
    if ($type == 'CR') {
        $cr = $cr + $row['amount'];
        $balance = $balance + $row['amount'];
        $result_txt .= '<td align="left">' . sprintf('%01.2f', $row['amount']) . '</td><td></td>';
        //sprintf('%01.2f', $row['amount'])
    } else {
        $dr = $dr + $row['amount'];
        $balance = $balance - $row['amount'];
        $result_txt .= '<td></td><td align="left"><font color="red">' . sprintf('%01.2f', $row['amount']) . '</font></td>';
    }


    $result_txt .= '<td align="left">' . $type . '</td>';
    $result_txt .= '<td><a class="button_small_green" href="../payments/edit_payment.php?id=' . $id . '"/>Edit</a>';

    //$result_txt .= '<td width="50"><a href="toy_detail.php?idcat=' . $idcat . '">View</a></td>';
    //if ($branch == 'admin') {
        $result_txt .= '<td><a class="button_small_red" href="delete_payment.php?id=' . $id . '"/>Delete</a>';
    //}
    if ($category == 'Fine: Missing Part') {
        $result_txt .= '<td><a class="button_small_yellow" href="refund.php?id=' . $id . '"/>Refund</a>';
    }
}


//print '<br/>' . $query;
//print '<br/>' . $location;
//below this is the function for no record!!
//end
if ($balance < 0) {
    $balance_txt = '<font color="red">$ ' . sprintf('%01.2f', $balance) . '</font>';
} else {
    $balance_txt = '$ ' . sprintf('%01.2f', $balance);
}
if ($numrows > 0) {
    print '<div id="open"><table width="100%"><tr><td width= 50%><h1 align="left"> Balance : ' . $balance_txt . ' </h1></td><td><h1 align="right">Total: ' . $total . '</h1></td><tr></table>';
}
print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
if ($numrows > 0) {
    print '<tr style="color:green"><td>Date</td><td>Id</td><td>Select</td><td>loc</td><td>Category</td><td>Description</td><td>Type</td><td>Payment</td><td>Debit</td><td>DR/CR</td><td></td><td></td><td></td><tr>';
}

print $result_txt;
if ($numrows > 0) {
    print '<tr style="color:green"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td><td>' . sprintf('%01.2f', $cr) . '</td><td>' . sprintf('%01.2f', $dr) . '</td><td></td><tr>';
    print '</tr></table>';
    //sprintf('%01.2f', $cr)
}
?>