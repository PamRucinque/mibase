<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//include( dirname(__FILE__) . '/../get_settings.php');
//$_SESSION['payment_status'] .= ' Member Alerts: ' . $member_alerts;
if ($member_alerts == 'Yes') {
    //include( dirname(__FILE__) . '/../connect.php');

    $query_event = "SELECT * from  event where typeevent LIKE '%Levy%' and memberid = " . $_POST['bcode'] . "";
    //$_SESSION['payment_status'] .= $query_event;
    $result_event = pg_Exec($conn, $query_event);
    $numrows = pg_numrows($result_event);

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result_event, $ri);

        $event_amount = $row['amount'];
        //$_SESSION['payment_status'] .= ' ' . $row['amount'] . ' ' ;
        $event_description = $row['description'];
        $event_hrs = $row['nohours'];
        $type_event = $row['typeevent'];
        $tomorrow = strtotime(date("Y-m-d H:i:s")." + 1 day");
        $date_roster =  date("Y-m-d", $tomorrow);
        
        //include( dirname(__FILE__) . '/new_paymentid.php');
        $_SESSION['payment_status'] .= ', ' . $type_event . ' added.';
        $result =  ' ' . $type_event .  ' ' . add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $event_description, $type_event, $event_amount, 'DR', 'SUBS');
        include( dirname(__FILE__) . '/new_rosterid.php');
        if ($event_hrs > 0){
           $_SESSION['payment_status'] .= ' ' . add_to_roster($rosterid, $date_roster, $_POST['bcode'], $event_hrs, 'Exemption', TRUE, 'Member', $type_event , '', TRUE);
       }
    }
}


    