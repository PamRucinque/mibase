<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');
if (!isset($_SESSION['idcat'])){
    $toyname = '';
}
?>

<body id="main_body">
    <script type="text/javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
    <link type="text/css" href="../js/themes/base/jquery.ui.all.css" rel="stylesheet" />
    <script type="text/javascript">
        $(function () {
            var pickerOpts = {
                dateFormat: "d MM yy",
                showOtherMonths: true

            };
            $("#datepayment").datepicker(pickerOpts);
        });

    </script>


    <?php
    include( dirname(__FILE__) . '/data/get_toy.php');
    include( dirname(__FILE__) . '/data/get_member.php');
    include( dirname(__FILE__) . '/new_paymentid.php');
    //$payment_description = '';
    ?>

    <form id="new" method="post" action="">
        <div id="form1234" style="background-color:lightgray;" align="left">
            <br>
            <table align="top">

                <tr><td  width="20%">Date of Payment:<br>
                        <?php
                        if (isset($_POST['datepayment'])) {
                            echo '<input type="text" name="datepayment" id ="datepayment" align="LEFT" style="width: 120px" value="' . $_POST['datepayment'] . '"></input><br></td>';
                        } else {
                            echo '<input type="text" name="datepayment" id ="datepayment" align="LEFT" style="width: 120px" value="' . date('Y-m-d') . '"></input><br></td>';
                        }
                        ?>
                    <td  width="30%">Category:<br><?php include( dirname(__FILE__) . '/data/get_payment_options_new.php'); ?> </td>
                    <td width="20%"  style="padding-right: 20px;" >Description:<br><input type="text" name="desc" id="desc" align="LEFT" size="40" value="<?php echo $payment_description; ?>"></input>

                    </td>

                    <td width="20%">Payment Type:<br><?php include( dirname(__FILE__) . '/data/get_payment_type_new.php'); ?>    </td>
                    <td  style="padding-right: 20px;">Amount:<br><input type="Number" name="amount" step="0.01" align="LEFT" size="10" Required ="Yes" value="<?php echo $payment_amount; ?>"></input><br></td>
                </tr></table>
            <table><tr>
                    <td align="center"><br><input id="new_payment" class="button1_green"  type="submit" name="new_payment" value="Make Payment" /></td>
                        <?php
                        echo '<td align="center"><br><input id="debit" class="button1_red"  type="submit" name="debit" value="Debit" />';
                        echo '<input id="debit_annual" class="button1_red"  type="submit" name="debit_annual" value="Debit Annual Fee" />';
                        echo '<input id="debit_levy" class="button1_red"  type="submit" name="debit_levy" value="Debit Levy" />';
                        echo '<input id="debit_missing" class="button1_red"  type="submit" name="debit_missing" value="Debit Missing Part" />';
                        echo '<input id="refund" class="button1_blue"  type="submit" name="refund" value="Refund" /></td>';
                        
                        ?>
                </tr>
                <input type="hidden" name="bcode" value="<?php echo $borid; ?>"></input>
                <input type="hidden" name="icode" value="<?php echo $_SESSION['idcat']; ?>"></input>
                <input type="hidden" name="borname" value="<?php echo $longname; ?>"></input>
                <input type="hidden" name="description" value="<?php echo $toyname; ?>"></input>
                <input type="hidden" name="id" value="<?php echo $paymentid; ?>"> 
            </table>
        </div>
    </form>



