<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

function get_type($membertype) {
     $conn = pg_connect($_SESSION['connect_str']);
    $sql = "SELECT * FROM membertype 
          WHERE (membertype)='" . $membertype . "'";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $renewal_fee = $row['renewal_fee'];
    $description = $row['description'];
    $levy = $row['bond'];
    return array('renewal' => $renewal_fee, 'description' => $description, 'levy' => $levy);
}

function get_missing_part() {
     $conn = pg_connect($_SESSION['connect_str']);
    $sql = "SELECT * FROM paymentoptions  
          WHERE upper(paymentoptions)='MISSING PART';";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $fee = $row['amount'];
    return $fee;
}

function get_levy() {
     $conn = pg_connect($_SESSION['connect_str']);
    $sql = "SELECT * FROM paymentoptions  
          WHERE upper(paymentoptions)='LEVY';";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $fee = $row['amount'];
    return $fee;
}

function get_membertype($borid) {
     $conn = pg_connect($_SESSION['connect_str']);
    $sql = "SELECT membertype AS membertype FROM borwrs 
          WHERE id=" . $borid;
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $membertype = $row['membertype'];
    return $membertype;
}

function add_to_journal($datepaid, $bcode, $icode, $name, $description, $category, $amount, $type, $typepayment) {
    $timezone = $_SESSION['settings']['timezone'];
    
    
    
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    date_default_timezone_set($timezone);
    $debitdate = date('Y-m-d H:i:s');

    $query_payment = "INSERT INTO journal (datepaid, bcode, icode, name, description, category, amount, type, typepayment, location, changedby, debitdate)
                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";

    $sth = $pdo->prepare($query_payment);
    $array = array($datepaid, $bcode, $icode, $name, $description, $category, $amount,
        $type, $typepayment, $_SESSION['location'], $_SESSION['username'], $debitdate);

    //$result_payment = pg_Exec($conn, $query_payment);
    if ($amount != 0) {
        $sth->execute($array);
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {

            $result = 'Error' . $stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>' . $query_payment;
            //exit;
        } else {
            $result = 'Payment / Debit has been added';
        }
    } else {
        $result = 'Amount needs to be not equal to 0';
    }

    return $result;
}

function add_to_roster($id, $date_roster, $member_id, $duration, $type_roster, $approved, $session_role, $roster_session, $weekday, $complete) {

    
    
    
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $now = date('Y-m-d H:i:s');

    $query_payment = "INSERT INTO roster (id, date_roster, member_id, duration, type_roster, approved, session_role, roster_session, weekday, complete, location, date_created)
                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";

    $sth = $pdo->prepare($query_payment);
    $array = array($id, $date_roster, $member_id, $duration, $type_roster, $approved,
        $session_role, $roster_session, $weekday, $complete, $_SESSION['location'], $now);
    //$result_payment = pg_Exec($conn, $query_payment);
    if ($duration != 0) {
        $sth->execute($array);
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {

            $result = 'Error' . $stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>' . $query_payment;
            exit;
        } else {
            $result = 'Roster credit has been added.';
        }
    } else {
        $result = 'There was an error adding the Roster credit.';
    }

    pg_FreeResult($result_payment);
    return $result;
}

?>
