<?php

/*
 * Copyright (C) 2018 SqualaDesign ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
include(dirname(__FILE__) . '/../mibase_check_login.php');
if(!isset($_SESSION)) {
 @session_start();
}
$category_str = '';
if (isset($_POST['category'])) {
    $category_str = "AND category LIKE '%" . $_POST['category'] . "%' ";
}
$payment_str = "";
if (isset($_POST['typepayment'])) {
    $payment_str = "AND typepayment LIKE '%" . $_POST['typepayment'] . "%' ";
}

$query = "SELECT *
        FROM journal
        WHERE type = 'CR' ".$category_str . $payment_str . $date_str . " ORDER BY datepaid DESC";
$XX = "No Record Found";
$total = 0;
$balance = 0;
$result_list = pg_exec($conn, $query);
$numrows = pg_numrows($result_list);
$result_txt = '';
$cr = 0;
$library_code = $_SESSION['library_code'];

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_list, $ri);
    $total = $total + 1;
    $id = $row['id'];
    $datepaid = $row['datepaid'];
    $borid = $row["bcode"];
    $description = $row["description"];
    $memname = $row["name"];
    $typepayment = $row["typepayment"];
    $type = $row['type'];
    $category = $row['category'];
    $changedby = $row['changedby'];

    $format_datepaid = substr($row['datepaid'], 8, 2) . '-' . substr($row['datepaid'], 5, 2) . '-' . substr($row['datepaid'], 0, 4);
    $result_txt .= '<tr><td>' . $format_datepaid . '</td>';
    $result_txt .= '<td>' . $id . '</td>';
    $result_txt .= '<td>' . $changedby . '</td>';
    $result_txt .= '<td>' . $borid . ': ' . $memname . '</td>';
    $result_txt .= '<td>' . $category . '</td>';
    $result_txt .= '<td>' . $description . '</td>';
    $result_txt .= '<td>' . $typepayment . '</td>';
    $cr = $cr + $row['amount'];
    $balance = $balance + $row['amount'];
    $result_txt .= '<td>$' . sprintf('%01.2f', $row['amount']) . '</td>';
}
if ($numrows > 0) {
    print '<div class="row">';
    print '<div class="col-md-offset-6 col-md-3"><div class="alert alert-warning"><strong>Total No of Transactions: </strong>' . $total . '</div></div>';
    print '<div class="col-md-3"><div class="alert alert-warning"><strong>Total: $</strong>' . $balance . '</div></div>';
    print '</div>';
}
print '<table id="provapdf" class="table table-striped table-responsive tablesorter ordinamentotabelle"><thead>';
print '<tr><th class="sorter-shortDate dateFormat-ddmmyyyy">Date</th><th>ID</th><th style="white-space: nowrap">Changed</th><th>Member</th><th>Category</th><th>Description</th><th>Type</th><th>Payment</th><tr></thead><tbody>';
print $result_txt;
print '</tr></tbody></table>';
?>