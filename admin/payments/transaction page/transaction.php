<?php
/*
 * Copyright (C) 2018 SqualaDesign ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
include('../mibase_check_login.php');
$payment_description = '';
$payment_amount = 0;
$type_payment = '';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>MiBaseNZ - Transactions</title>
	    <?php include('../header.php'); ?> 
        <script type="text/javascript">
           $( function() {
                var dateFormat = "yy-mm-dd",
                  start = $( "#start" )
                    .datepicker({dateFormat: "yy-mm-dd", showOtherMonths: true, changeMonth: true, changeYear: true })
                    .on( "change", function() {
                      end.datepicker( "option", "minDate", getDate( this ) );
                    }),
                  end = $( "#end" ).datepicker({dateFormat: "yy-mm-dd", showOtherMonths: true, changeMonth: true, changeYear: true })
                  .on( "change", function() {
                    start.datepicker( "option", "maxDate", getDate( this ) );
                  });
                function getDate( element ) {
                  var date;
                  try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                  } catch( error ) {
                    date = null;
                  }
                  return date;
                }
              } );
        </script>
    </head>
    <body id="main_body" >    
        <div class="container">
            <?php
                if(!isset($_SESSION)) {
                 @session_start();
                } 
                include('../header-logo.php');
            	echo '<div class="row">';
            	include('../menu.php');
            	echo '</div><div class="row">';
                $payment_amount = 0;
            	include('../header_detail/header_detail.php');
            	echo '</div>';
                include('functions/functions.php');
                // echo $_SESSION['payment_status'];
                if (isset($_POST['payment'])) {
                include('data/get_payment_amount.php');
                $payment_type = $_POST['payment'];
            }
            if (isset($_POST['new_payment'])) {
                if ($_POST['payment'] == '') {
                    $_POST['payment'] = 'Payment - Thankyou';
                }
                $description = $_POST['desc'];
                $borname = str_replace("'", "`", $_POST['borname']);
                $amount = $_POST['amount'];
                // $_SESSION['payment_status'] = add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $_POST['payment'], $amount, 'CR', $_POST['typepayment']);
            }
            if (isset($_POST['refund'])) {
                $amount = -1 * $_POST['amount'];
                $description = 'Refund';
                if ($_POST['payment'] == '') {
                    $category = $_POST['payment'];
                } else {
                    $category = 'REFUND: ' . $_POST['payment'];
                }
                $borname = str_replace("'", "`", $_POST['borname']);
                // $_SESSION['payment_status'] = add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $amount, 'DR', 'DEBIT');
                $id = $_POST['id'] + 1;
                // $_SESSION['payment_status'] = add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $amount, 'CR', $_POST['typepayment']);
            }
            if (isset($_POST['debit_annual'])) {

                $membertype = get_membertype($_POST['bcode']);
                $type = get_type($membertype);
                $payment_amount = $type['renewal'];
                $description = 'Membership fees';
                $category = $type['description'];
                $borname = str_replace("'", "`", $_POST['borname']);
                // $_SESSION['payment_status'] = $membertype . ' ' . add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $payment_amount, 'DR', 'SUBS');
            }
            if (isset($_POST['debit_levy'])) {
                $membertype = get_membertype($_POST['bcode']);
                $type = get_type($membertype);
                $levy_amount = $type['levy'];
                $description = 'Levy';
                $category = 'Membership Levy';
                $borname = str_replace("'", "`", $_POST['borname']);
                // $_SESSION['payment_status'] = 'Levy amount debited: ' . add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $levy_amount, 'DR', 'SUBS');
            }
            if (isset($_POST['debit_missing'])) {

                $payment_amount = get_missing_part();
                $description = 'Missing Part';
                if ($_POST['desc'] == '') {
                    if ($idcat != '') {
                        $description = $idcat . ': ' . $toyname;
                    } else {
                        $description = 'No Current Toy';
                    }
                } else {
                    $description = $_POST['desc'];
                }

                $category = 'Fine: Missing Part';

                $borname = str_replace("'", "`", $_POST['borname']);
                // $_SESSION['payment_status'] = '<div class="alert alert-danger">Missing Part amount debited: ' . add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $payment_amount, 'DR', 'FINE') . '</div>';
            }
            if (isset($_POST['debit'])) {

                $amount = $_POST['amount'];
                $description = $_POST['desc'];
                $category = $_POST['payment'];
                $borname = str_replace("'", "`", $_POST['borname']);
                // $_SESSION['payment_status'] = add_to_journal($_POST['datepayment'], $_POST['bcode'], $_POST['icode'], $borname, $description, $category, $amount, 'DR', 'DEBIT');
            }
            include ('data/get_member.php');
            // echo $_SESSION['payment_status'];
            
            $_SESSION['post'] = 0;
            $date_str = "";
            if (isset($_POST['start'])) {
                $_SESSION['post'] = 1;
                if($_POST['start'] != "" || $_POST['start'] != null) 
                    $start = $_POST['start'];
               
                if($_POST['end'] != "" || $_POST['end'] != null) 
                    $end = $_POST['end'];
                
                $_SESSION['startdate'] = $start;
                $_SESSION['enddate'] = $end;
                $date_str = "AND datepaid BETWEEN '" . $start . "' AND '" . $end ."'";
            }

            if (!isset($_SESSION['startdate']) || $_SESSION['post'] == 0){
                $query = "SELECT datepaid FROM journal ORDER by datepaid DESC LIMIT 1";
                $result = pg_exec($conn, $query);
                if( pg_numrows($result) == 1){
                    $row = pg_fetch_array($result, 0);
                    $start = $row['datepaid'];
                }
                else
                    $start = date("Y-m-d", 1);
                    
                $_SESSION['startdate'] = $start;
            }
            if (!isset($_SESSION['enddate']) || $_SESSION['post'] == 0){
                $end = date("Y-m-d");
                $_SESSION['enddate'] = $end;
            }
            
        	?>

        </div><!-- /container -->
        <div class="container main" id="form_container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Transactions</h2>
                    <div class="row bk-grey">
                        <form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action= "transaction.php">
                            <div class="form-group col-md-2">
                                <label>Start Day:</label>
                                <input type="text" name="start" id ="start" value="<?php echo $_SESSION['startdate']; ?>"></input>
                            </div>
                            <div class="form-group col-md-2">
                                <label>End Day:</label>
                                <input type="text" name="end" id ="end" value="<?php echo $_SESSION['enddate']; ?>"></input>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Category:</label>
                                <?php 
                                $_SESSION['selectCategory'] = $_POST['category'];
                                include('data/get_payment_options.php'); ?>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Type of payment:</label>
                                <?php 
                                $_SESSION['selectType'] = $_POST['typepayment'];
                                include('data/get_payment_type.php'); ?>
                            </div>
                            <div class="form-group col-md-1 button-max-margin">
                                <span class="icon-input-btn">
                                    <span class="glyphicon glyphicon-search"></span> 
                                    <input id="saveForm" class="btn btn-default btn-violet" type="submit" name="submit" value="Search" />
                                </span>
                            </div>
                            <div class="form-group col-md-1 button-max-margin">
                                <a href='transaction.php' class ='btn btn-default btn-orange'><i class="fas fa-redo-alt"></i> Reset</a>
                            </div>                        
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-sort"><strong>Info!</strong> Click on the table header to sort the table data. If you want to export the data you can filter and/or sort before dowloanding the pdf or excel.</div>
                        </div>
                    </div>
                    <div class="row btn-pdf-ex">
                        <div class="col-md-offset-8 col-md-4">
                            <button id="exportButtonpdf" class="btn btn-default btn-danger clearfix"><i class="fas fa-file-pdf"></i> Export to PDF</button>
                            <button id="exportButtonexcel" class="btn btn-default btn-success clearfix"><i class="fas fa-file-excel"></i> Export to Excel</button>
                        </div>
                    </div>
                    <?php 
                        include ('result-transaction.php');
                    ?>
                </div> <!-- /col-md-12 -->
            </div><!-- /row -->
        <?php include('../footer.php'); ?>
        </div> <!-- /container -->
        
        <!-- script for PDF and Excel button -->
        <script type="text/javascript">
        jQuery(function ($) {
                $("#exportButtonexcel").click(function () {
                    // parse the HTML table element having an id=exportTable
                    var dataSource = shield.DataSource.create({
                        data: "#provapdf",
                        schema: {
                            type: "table",
                            fields: {
                                Date: { type: String },
                                ID: { type: String },
                                Changed: { type: String },
                                Member: { type: String },
                                Category: { type: String },
                                Description: { type: String },
                                Type: { type: String },
                                Payment: { type: String }
                            }
                        }
                    });
                    // when parsing is done, export the data to Excel
                    dataSource.read().then(function (data) {
                        new shield.exp.OOXMLWorkbook({
                            author: "MibaseNZ",
                            worksheets: [
                                {
                                    name: "PrepBootstrap Table",
                                    rows: [
                                        {
                                            cells: [
                                                 { style: { bold: true }, type: String, value: "Date"},
                                                 { style: { bold: true }, type: String, value: "ID"},
                                                 { style: { bold: true }, type: String, value: "Changed"},
                                                 { style: { bold: true }, type: String, value: "Member"},
                                                 { style: { bold: true }, type: String, value: "Category"},
                                                 { style: { bold: true }, type: String, value: "Description"},
                                                 { style: { bold: true }, type: String, value: "Type"},
                                                 { style: { bold: true }, type: String, value: "Payment"}
                                            ]
                                        }
                                    ].concat($.map(data, function(item) {
                                        return {
                                            cells: [
                                                { type: String, value: item.Date },
                                                { type: String, value: item.ID },
                                                { type: String, value: item.Changed },
                                                { type: String, value: item.Member },
                                                { type: String, value: item.Category },
                                                { type: String, value: item.Description },
                                                { type: String, value: item.Type },
                                                { type: String, value: item.Payment }
                                            ]
                                        };
                                    }))
                                }
                            ]
                        }).saveAs({
                            fileName: "TransactionListExcel"
                        });
                    });
                });
            });
            jQuery(function ($) {
                $("#exportButtonpdf").click(function () {
                    // parse the HTML table element having an id=exportTable
                    var dataSource = shield.DataSource.create({
                        data: "#provapdf",
                        schema: {
                            type: "table",
                            fields: {
                                Date: { type: String },
                                ID: { type: String },
                                Changed: { type: String },
                                Member: { type: String },
                                Category: { type: String },
                                Description: { type: String },
                                Type: { type: String },
                                Payment: { type: String }
                            }
                        }
                    });

                    // when parsing is done, export the data to PDF
                    dataSource.read().then(function (data) {
                        var pdf = new shield.exp.PDFDocument({
                            author: "MibaseNZ",
                            created: new Date(),
                            fontSize: 10
                        });

                        pdf.addPage("a4", "landscape");

                        pdf.table(
                            40,
                            40,
                            data,
                            [
                                { field: "Date", title: "Date", width: 65 },
                                { field: "ID", title: "ID", width: 35 },
                                { field: "Changed", title: "Changed", width: 70 },
                                { field: "Member", title: "Member", width: 150 },
                                { field: "Category", title: "Category", width: 180 },
                                { field: "Description", title: "Description", width: 150 },
                                { field: "Type", title: "Type", width: 50 },
                                { field: "Payment", title: "Payment", width: 50 }
                            ],
                            {
                                margins: {
                                    left: 20,
                                    top: 20,
                                    bottom: 20
                                }
                            }
                        );
                        pdf.saveAs({
                            fileName: "TransactionListPDF"
                        });
                    });
                });
            });
        </script>
    </body>
</html>