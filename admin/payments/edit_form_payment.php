<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<br>
<script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

<script type="text/javascript">
    $(function(){
        var pickerOpts = {
            dateFormat:"yy-m-d",
            showOtherMonths: true
              
        }; 
        $("#datepaid").datepicker(pickerOpts);
    });
    function myFunction()
    {
        alert("This Payment has been saved!");
    }
      
</script>

<p><font size="2" face="Arial, Helvetica, sans-serif"></font></p>

<font></font>
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="edit_payment.php">

    <div id="form" style="background-color:lightgray;" align="left">

        <table align="top"><tr>
                <td><h2>Edit Payment:</h2></td>
                <td align="right"><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Save" /></td>
            </tr>
            <tr>
                <td>Date of Payment:<br>
                    <input type="text" name="datepaid" id ="datepaid" align="LEFT" value="<?php echo $datepaid; ?>"></input><br></td>
            </tr>
            <tr>
                <td>Name:<br>
                    <input type="Text" name="name" id="name" align="LEFT"  size="50" value="<?php echo $name; ?>"></input><br></td>
            </tr>
            <tr>
                <td>Description:<br>
                    <input type="Text" name="description" align="LEFT"  size="50" value="<?php echo $description; ?>"></input><br></td>
            </tr>
            <tr>
                <td>Category:<br><?php include( dirname(__FILE__) . '/data/get_payment_options.php'); ?> </td>

                <td>Type Payment:<br>
                                <br><?php include( dirname(__FILE__) . '/data/get_payment_type.php'); ?>     </tr>
            <tr>
                <td>Borrower Number:<br>
                    <input type="Text" name="bcode" align="LEFT"  size="10" value="<?php echo $bcode; ?>"></input><br></td>
                <td>Toy Code:<br>
                    <input type="Text" name="icode" align="LEFT"  size="10" value="<?php echo $icode; ?>"></input><br></td>

            </tr>
            <tr>
                <td><label>Type: </label>
                    <select id="type" name="type">
                        <option value='<?php echo $type; ?>' selected="selected"><?php echo $type; ?></option>
                        <option value="CR" >CR</option>
                        <option value="DR" >DR</option>
                    </select></td>

                <td>Amount:<input type="Number" name="amount" align="LEFT"  step="0.01" size="10" value="<?php echo $amount ?>"></input><br></td>
            </tr>

            <input type="hidden" name="id" value="<?php echo $id; ?>"> 
        </table>
    </div>
</form></p>

