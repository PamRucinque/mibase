<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');
$conn = pg_connect($_SESSION['connect_str']);


//session_start();
//if (!isset ($_SESSION['borid'])){
    //$_SESSION['borid'] = $_GET['id'];
//}


$conn = pg_connect($_SESSION['connect_str']);
$query = "SELECT * from borwrs WHERE id = '" . $_SESSION["borid"] . "'";
$result_member = pg_Exec($conn, $query);
$numrows = pg_numrows($result_member);


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_member, $ri);

    $expired = $row['expired'];
    $format_expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);

    $borid = $row["id"];
    $firstname = $row["firstname"];
    $membertype = $row["membertype"];
    $surname = $row["surname"];
    $partnersname = $row["partnersname"];
    $partnerssurname = $row['partnerssurname'];
    $email = $row["emailaddress"];
    $email2 = $row["email2"];
    $phone = $row["phone"];
    $postcode = $row['postcode'];
    $mobile1 = $row["phone2"];
    $mobile2 = $row['mobile1'];
    $member_status = $row["member_status"];
    $address = $row['address'];
    $address2 = $row['address2'];
    $suburb = $row['suburb'];
    $city = $row['city'];
    $state = $row['state'];
    $discovery = $row['discoverytype'];
    $rostertype = $row['rostertype'];
    $rostertype2 = $row['rostertype2'];
    $rostertype3 = $row['rostertype3'];
    $alert_mem = $row['specialneeds'];
    $notes = $row['notes'];
    $user1 = $row['location'];
    $member_status = $row['member_status'];
    if ($partnersname == null || $partnerssurname == null) {
        $longname =  $firstname . ' ' . $surname;
    }else{
        $longname =  $firstname . ' ' . $surname .  ' & ' . $partnersname . ' ' . $partnerssurname;
    }

    $email_link = '<a class="button_small_red" href="mailto:' . $email . '">send email</a>';
    $email_link2 = '<a class="button_small_red" href="mailto:' . $email2 . '">send email</a>';

}
$now = date('Y-m-d');

$diff = abs((strtotime($now) - strtotime($expired))/(60*60*24));

if ($diff > 30){
$format_expired_warning = '<h3><font color="darkgreen">Expires: ' . $format_expired. '</font></h3>';
}
    if ($diff >= 0 && $diff <= 30){
    $format_expired_warning = '<h3><font color="orange">Due to Expire on: ' . $format_expired . '</font></h3>';
}
if ($diff < 0){
    $format_expired_warning = '<h3><font color="red">Expired on: ' . $format_expired . '</font></h3>';
}



//session_start();
//$_SESSION['borid'] = $borid;
?>
