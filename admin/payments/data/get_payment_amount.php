<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');
$conn = pg_connect($_SESSION['connect_str']);

//include( dirname(__FILE__) . '/../connect.php');
$query = "SELECT amount,typepayment, description FROM paymentoptions where paymentoptions = '" . $_POST['payment'] .  "';";
$query = "select paymentoptions as paymentoptions, amount, description, 'OPTION' as tbl, typepayment from paymentoptions 
where paymentoptions = '" . $_POST['payment'] .  "'
UNION 
select membertype as paymentoptions, renewal_fee, description,  'SUBS' as tbl, 'fees' as tp from membertype 
where membertype = '" . $_POST['payment'] .  "' 
order by paymentoptions;";

$result = pg_Exec($conn, $query);
$numrows = pg_numrows($result);

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    $payment_amount = $row['amount'];
    $type_payment = $row['typepayment'];
    $payment_description = $row['description'];
}

?>

