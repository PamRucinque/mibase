<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../js/themes/base/jquery.ui.all.css" rel="stylesheet" />


<p><font size="2" face="Arial, Helvetica, sans-serif"></font></p>

<font></font>
<?php
if (isset($_GET['id'])){
    $id = $_GET['id'];
}
echo "<br><a href='options.php' class ='button1_red'>Back to Options</a>";
?>
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'edit_option.php?id=' . $id; ?>">

    <div id="form" style="background-color:lightgray;" align="left" style="padding-left: 20px;">
        <h2>Edit an Option:</h2>
        <input id="saveForm" class="button1_red"  type="submit" name="submit" value="Save" /><br>
        <br>Category:<input type="Text" name="paymentoptions" align="LEFT"  size="35" value="<?php echo $paymentoptions; ?>"></input><br>
        <br>Description:<input type="Text" name="description" align="LEFT"  size="35" value="<?php echo $description; ?>"></input><br>
        <br>Group:<input type="Text" name="group" align="LEFT"  size="35" value="<?php echo $group; ?>"></input><br>
        <br>Type Payment:<input type="Text" name="typepayment" align="LEFT"  size="35" value="<?php echo $typepayment ?>"></input><br>
        <br>Amount:<input type="Number" name="amount" align="LEFT"  size="10" step ="0.1" value="<?php echo $amount ?>"></input><br><br><br>
        <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"> 

    </div>
</form>

