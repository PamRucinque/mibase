<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <body id="main_body" >
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');
            include( dirname(__FILE__) . '/get_option.php');
            //include( dirname(__FILE__) . '/toy_detail.php');

            if (isset($_POST['submit'])) {

                $conn = pg_connect($_SESSION['connect_str']);
                $query = "UPDATE paymentoptions SET
                    paymentoptions = '{$_POST['paymentoptions']}',
                    description = '{$_POST['description']}',
                    typepayment = '{$_POST['typepayment']}',
                    amount = '{$_POST['amount']}'
                    WHERE id=" . $_POST['id'] . ";";

                $result = pg_Exec($conn, $query);
                //echo $query;
                //echo $connection_str;
                if (!$result) {
                    echo "An INSERT query error occurred.\n";
                    echo $query;
                    //exit;
                } else {
                    echo "<br>The record was successfully saved. " . "<a class='button1_red' href='options.php'>OK</a>" . "<br><br>";
                }
            } else {
                include( dirname(__FILE__) . '/get_option.php');
                include( dirname(__FILE__) . '/edit_form_option.php');
            }
            ?>
        </div>
    </body>
</html>