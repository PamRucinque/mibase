<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');
include( dirname(__FILE__) . '/functions/functions.php');

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    // get the 'id' variable from the URL

    include( dirname(__FILE__) . '/data/get_payment.php');
    $now = date('Y-m-d');
    $amount = -1 * $amount;
    $_SESSION['payment_status'] = 'Refund Missing Piece: ' . add_to_journal($now, $bcode, $icode, $name, $description, 'Refund: Missing Part', $amount, 'CR', 'Credit');
    $redirect = "Location: payments.php";

    header($redirect);
    //echo $_SESSION['payment_status'];
}
?>
