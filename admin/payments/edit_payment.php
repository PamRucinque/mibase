<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>


    <body id="main_body" >
        <div id="form_container">


            <?php
            include( dirname(__FILE__) . '/../menu.php');


            //include( dirname(__FILE__) . '/toy_detail.php');

            if (isset($_POST['submit'])) {
                //include( dirname(__FILE__) . '/../connect.php');
                $query = "UPDATE journal SET
                    description = '{$_POST['description']}',
                    datepaid = '{$_POST['datepaid']}',
                    name = '{$_POST['name']}',
                    category = '{$_POST['category']}',
                    typepayment = '{$_POST['typepayment']}',
                    bcode = {$_POST['bcode']},
                    icode = '{$_POST['icode']}',
                    type = '{$_POST['type']}',
                    amount = '{$_POST['amount']}'
                    WHERE id=" . $_POST['id'] . ";";

                    $conn = pg_connect($_SESSION['connect_str']);   
                    $result = pg_Exec($conn, $query);
                //echo $query;
                //echo $connection_str;
                if (!$result) {
                    echo "An UPDATE query error occurred.\n";
                    echo $query;
                    //exit;
                } else {
                    //$edit_url = 'payments.php';
                    echo "<br>The record was successfully saved and the ID is:" . $_POST['id'] . "<br><br>";
                    echo '<a class="button1_red" href="payments.php">Back to Payments</a>';

                }

                pg_FreeResult($result);
// Close the connection
                pg_Close($conn);
            } else {
                include( dirname(__FILE__) . '/data/get_payment.php');
                include( dirname(__FILE__) . '/edit_form_payment.php');
            }
            ?>
        </div>
    </body>
</html>