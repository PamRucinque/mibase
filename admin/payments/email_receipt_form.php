<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<script>
    function go() {
        var idcats = "";

        $("tr.item").each(function() {
            $this = $(this);
            var idcat = $this.find("td.idcat").html();
            var value = $this.find("td :checkbox").is(':checked');
            if (value) {
                idcats += "'" + idcat + "',";
            }
        });

        if (idcats.length > 0) {
            idcats = idcats.substring(0, idcats.length - 1);
            $("#idcats_list").val(idcats);
            return true;
        } else {
            alert('No payments selected!');
            return false;
        }

        //alert(idcats);
    }
                function select_all() {
                var idcats = "";
                $("tr.item").each(function() {
                    $this = $(this);
                    $this.find("td :checkbox").prop("checked", true);
                });
            }
</script>

<?php

$link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] .  '/PdfReport';
$idcat = $_SESSION['idcat'];
$toy_to_print = "'" . $idcat . "'";
?>
<br>
<form id = "print_receipt"  name = "print_receipt" method = "post" action = "" >
    <input id="saveForm" class="button1_red"  type="submit" name="submit" value="Email Receipt" onclick="return go()"/>    
    <input type="hidden" name="idcats_list" id="idcats_list" value=""/>
    <input type = "hidden" id = "libraryname" name = "libraryname" value = "<?php echo $_SESSION['libraryname']; ?>" >
    <input type="hidden" id="libraryaddress" name="libraryaddress" value="<?php echo $_SESSION['address']; ?>"> 
    <input type="hidden" id="bcode" name="bcode" value="<?php echo $_SESSION['borid']; ?>" >  
    <input type = "hidden" id = "comments" name = "comments" value = "<?php echo $loan_receipt; ?>" >
    <input type = "hidden" id = "user" name = "user" value = "<?php echo $_SESSION['myusername']; ?>" >
    <input type = "hidden" id = "password" name = "password" value = "<?php echo $_SESSION['mypassword']; ?>" >
</form>



