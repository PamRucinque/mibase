<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

function mibase_validate($input, $username, $pwd, $var) {
    $pattern = '/^\w{0,3}-?\w{1,6}$/';
    //$pattern = '/\d+/';
    if (preg_match($pattern, $input) != 0) {
        //echo 'Doesnt matchAAAA' . $getid;
        $return_value = $input;
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
        $location = $_SERVER['SCRIPT_FILENAME'];
        $connect_pdo = "pgsql:port=5432;dbname=toybase;host=localhost";
        $dbuser = "toybase";
        $dbpasswd = "xyz";
        $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
        if ($username == '') {
            $username = 'Not logged in';
        }
        if ($login_type == '') {
            $login_type = 'NA';
        }
        if ($pwd == '') {
            $pwd = 'NA';
        }


        $query = "INSERT INTO blocked (ip, injection, 
                 username, pwd, type_block, subdomain, login_type, page ,var)
                 VALUES (?,?,?,?,?,?,?,?,?);";
        //and get the statment object back
        $sth = $pdo->prepare($query);

        $array = array($ip, $input, $username, $pwd, 'SQL Injection', $_SESSION['library_code'], $login_type, $location, $var);


        //execute the preparedstament
        //$sth->execute($array);
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {
            //echo "An INSERT query error occurred.\n";
            //echo $connect_pdo;
            //$return_value .= 'Error ' . $stherr[0] . '<br>';
            //$return_value .= 'Error ' . $stherr[1] . '<br>';
            //$return_value .= 'Error ' . $stherr[2] . '<br>';
        }

        $return_value = '';
    }
    return $return_value;
}

function mibase_validate_id($idcat, $id, $type) {
    $pattern = '/^[a-zA-Z]+$/';
    $pattern_integer = '/^\d+$/';
    $return = 'OK';

    //$pattern = '/\d+/';
    if ($type == 'id') {
        if (preg_match($pattern_integer, $id) == 0) {
            $return .= 'Login error: Memberid  fails the regular expression test: ' . $id . ': ';
        }
        if ((strlen($id)) > 10) {
            $length = strlen($memberid);
            $return .= 'id exceeds max length of 10: ' . $length . ': ';
        }
    }
    if ($type == 'idcat') {
        if (preg_match($pattern, $idcat) == 0) {
            $return .= 'IDCat fails the regular expression test: ' . $idcat . ': ';
        }
    }


    return $return;
}
