<?php

function fillOtherSessionVariables() {

    $_SESSION['timezone'] = 'Australia/ACT';

    //$_SESSION['level'] = 'admin';
}

function fillSettingSessionVariables() {
    include('../connect/connect_local.php');
    $array = array();

    //set default values
    $array['selectbox'] = 'No';
    $array['overdue_txt'] = 'OVERDUE';
    $array['storage_label'] = 'No';
    $array['show_password'] = 'No';
    $array['roster_alerts'] = 'Yes';
    $array['holiday_override'] = 'No';
    $array['toy_holds'] = 'No';
    $array['hide_balance'] = 'No';
    $array['vol_balance'] = 'No';
    $array['display_overdues'] = 'Yes';
    $array['admin_notes'] = 'Yes';
    $array['receipt_printer'] = 'No';
    $array['receipt_email'] = 'No';
    $array['hide_value'] = 'No';
    $array['show_desc'] = 'No';
    $array['display_overdues'] = 'Yes';
    $array['free_rent_btn'] = 'No';
    $array['disable_returnall'] = 'No';
    $array['checked'] = 'No';
    $array['loan_restrictions'] = 'No';
    $array['address'] = '';
    $array['user_toys'] = 'User1';
    $array['sub_category_loans'] = 'No';
    $array['format_toyid'] = 'No';
    $array['rent_default'] = 0;
    $array['reserve_all'] = 'No';
    $array['sub_category_loans'] = 'No';
    $array['stocktake_override'] = 'No';
    $array['loan_receipt'] = 'No';
    $array['receipt_email'] = 'No';
    $array['invoices'] = 'No';
    $array['default_paymenttype'] = 'EFT';
    $array['timezone'] = 'Australia/ACT';
    $array['roster'] = 'Yes';
    $array['nationality_label'] = '';
    $array['reservations'] = 'No';
    $array['special_loan'] = 'No';
    $array['rent_factor'] = 'No';
    $array['extra_rent'] = 'No';
    $array['charge_rent'] = 'No';
    $array['hide_city'] = 'No';
    $array['global_rent'] = 0;
    $array['grace'] = 0;
    $array['speical_return'] = 'No';
    $array['unlock_returns'] = 'No';
    $array['special_return'] = 'No';
    $array['rentasfine'] = 'No';
    $array['fine_factor'] = 1;
    $array['fine_value'] = 0;
    $array['fine_amount'] = 3.2;
    $array['daily_fine'] = 'No';
    $array['extra_fine'] = 'No';
    $array['roundup_week'] = 'No';
    $array['hire_factor'] = 1;
    $array['email_from'] = 'info@mibase.com.au';
    $array['disable_cc_email'] = 'No';
    $array['loan_default_missing_paid'] = 'No';
    $array['auto_debit_missing'] = 'No';
    $array['loan_default_missing_alert'] = 'Yes';
    $array['admin_renewal_list'] = 'No';
    $array['toy_holds'] = 'No';
    $array['simple_new_member'] = 'Yes';
    $array['vol_no_toy_edits'] = 'Yes';
    $array['suburb_title'] = '';
    $array['city_title'] = '';
    $arary['renew_button'] = 'Yes';
    $array['hide_city'] = 'Yes';
    $array['selectbox'] = 'No';
    $array['user1_borwrs'] = '';
    $array['nationality_label'] = '';
    $array['override_date_loan'] = 'No';
    $array['storage_label'] = 'Storage: ';
    $array['donated_label'] = 'Donated by:';
    $array['warnings_label'] = 'Warnings';
    $array['format_toyid'] = 'No';
    $array['idcat_noedit'] = 'Yes';
    $array['catsort'] = 'Yes';
    $array['automatic_debit_off'] = 'Yes';
    $array['renew_member_email'] = 'No';
    $array['settings_expired'] = '';
    $array['new_member_email'] = 'Yes';
    $array['password'] = 'mibase';
    $array['reuse_memberno'] = 'No';
    $array['expired'] = '';
    $array['online_conduct'] = 'No';
    $array['weekly_fine'] = 'Yes';
    $array['daily_fine'] = 'No';
    $array['logo_height'] = '150px';
    $array['library_heading_off'] = 'No';
    $array['menu_member_options'] = 'No';
    $array['menu_faq'] = 'No';
    $array['upgrade'] = 'No';
    $array['set_due_date'] = 'No';
    $array['set_loan_date'] = 'No';
    $array['loanperiod'] = 21;
    $array['format_toyid_decimal'] = 0;
    $array['vol_expired_off'] = 'No';
    $array['rosters_annual'] = 'Yes';
    $array['mem_roster_lock'] = 'No';
    $array['mem_coord_off'] = 'Yes';
    $array['mem_roster_show_all'] = 'Yes';
    $array['automatic_debit_off'] = 'Yes';
    //new in version 2
    $array['loanperiod_mem'] = 'No';
    $array['lp_custom'] = 'No';
    $array['loan_special'] = 'No';
    $array['loan_weighting'] = 'No';
    $array['max_daily_fine'] = 0;



    $query_settings = "SELECT setting_name, setting_value FROM settings;";

    $result_settings = pg_Exec($conn, $query_settings);
    $numrows = pg_numrows($result_settings);

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result_settings, $ri);
        $array[$row['setting_name']] = $row['setting_value'];
    }

    pg_FreeResult($result_settings);
    return $array;
}

function check_setting($setting_name, $setting_value) {
    $error = '';
    if (($setting_value == null) || ($setting_value == '')) {
        include('../connect/local_connect.php');
        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $query_settings = "SELECT setting_name, setting_value FROM settings where setting_name = ? LIMIT 1;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);

        $sth = $pdo->prepare($query_settings);
        $array = array($setting_name);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {
            $error .= "An  error occurred getting setting value: " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
            //return $error;
        }
        if ($numrows > 0) {
            $row = $result[0];
            $_SESSION['settings'][$setting_name] = $row['setting_value'];
            return $row['setting_value'];
        } else {
            $error .= $setting_name . ' has not been set in global settings.<br>';
            //return $error;
        }
    } else {
        //$error .= 'Setting already set: ' . $setting_name . ': ' . $setting_value;
        return $setting_value;
    }
    if ($error != '') {
        //return array('setting_value' => $setting_value, 'setting_name' => $setting_name, 'error' => $error);
        return Null;
    }
}

function check_in_settings($setting_name) {
    $out = '';
    include('../connect/connect_local.php');
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $query_settings = "SELECT setting_name, setting_value, description FROM settings where setting_name = ? LIMIT 1;";

    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);

    $sth = $pdo->prepare($query_settings);
    $array = array($setting_name);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    $stherr = $sth->errorInfo();

    if ($stherr[0] != '00000') {
        $out .= "An  error occurred getting setting value: " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        //return $error;
    }
    if ($numrows > 0) {
        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = $result[$ri];
            $out .= '<b>' . $row['setting_name'] . '</b> <font color="green">: ' . $row['setting_value'] . '</font> ' . $row['description'] . '<br>';
        }
    } else {
        $out .= '<font color="red">' . $setting_name . ' has not been set in global settings.</font><br>';
        //return $error;
    }
    return $out;
}
