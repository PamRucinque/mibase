<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require( dirname(__FILE__) . '/mibase_check_login.php');

//get settings
$host = $_SESSION['host'];
    $url_css = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['app_root_location'] . '/';
    $url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['app_root_location'] . '/php/upload_toys.php';
    $url_pp = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['app_root_location'] . '/php/upload_pp.php';
    $url_news = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['app_root_location'] . '/php/upload_news.php';
    $url_menu = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['app_root_location'] . '/';
    $url_logout = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['app_root_location'] . '/Logout.php';
    $libraryname = $_SESSION['settings']['libraryname'];

$reservations = '';

?>

<script>
    $(document).ready(function () {
        $("#panel").hide();
        if ($("#members").is(":visible")) {
            $("#members").hide();
        }
        if ($("#home").is(":visible")) {
            $("#home").hide();
        }
        if ($("#select_toys").is(":visible")) {
            $("#select_toys").hide();
        }
        if ($("#select_members").is(":visible")) {
            $("#select_members").hide();
        }

        if ($("#toys").is(":visible")) {
            $("#toys").hide();
        }
        $("#reserve_btn").click(function () {
            $("#members").hide();
            $("#select_toys").hide();
            $("#select_members").hide();
            $("#toys").hide();
            $("#panel").show("slow");
            $("#home").hide();
        });

        $("#home_btn").click(function () {
            $("#members").hide();
            $("#select_toys").hide();
            $("#select_members").hide();
            $("#toys").hide();
            $("#panel").show("slow");
            $("#home").show();
        });
        $("#members_btn").click(function () {
            $("#home").hide();
            $("#select_toys").hide();
            $("#select_members").hide();
            $("#toys").hide();
            $("#panel").show("slow");
            $("#members").show();
        });
        $("#toys_btn").click(function () {
            $("#home").hide();
            $("#select_toys").hide();
            $("#select_members").hide();
            $("#members").hide();
            $("#panel").show("slow");
            $("#toys").show();
        });
        $("#members_sel_btn").click(function () {
            $("#members").hide();
            $("#home").hide();
            $("#select_toys").hide();
            $("#toys").hide();
            $("#panel").show("slow");
            $("#select_members").show();
        });
        $("#toys_sel_btn").click(function () {
            $("#members").hide();
            $("#home").hide();
            $("#select_members").hide();
            $("#toys").hide();
            $("#panel").show("slow");
            $("#select_toys").show();
        });
    });
</script>

<style> 
    #panel, #flip {
        padding: 0 0 0 0;
        background-color: #e5eecc;
    }
    #menu button {
        border-left:	1px solid #fff;
        border-top:		1px solid #CFDEFF;
        padding: 		.75em 0.75em;
        text-decoration:none;
        color: whitesmoke;
    }

    #menu button:focus,#menu button:hover,#menu button:active {
        background:		#CFDEFF;
        outline:		0;
        color:			darkcyan;
    }
    #panel_home {
        padding: 10px 0 0 0;
        height: 28px;
        display: none;
    }

</style>
<div id="menu">
    <div id="flip">
        <button id="home_btn" class="button_menu_yellow">Home</button>
        <button id="toys_btn" class="button_menu" ondblclick="location.href = '<?php echo $url_menu . 'toys/toys.php'; ?>'">Toys</button>
        <button id="members_btn" class="button_menu_red"  ondblclick="location.href = '<?php echo $url_menu . 'members/members.php'; ?>'">Members</button>
        <?php
        if (($reservations == 'Yes')&&($_SESSION['mibase_server'] == 'Yes')) {
            echo '<button id = "reserve_btn" class="button_menu_pink" onclick="location.href = \'' . $url_menu . 'reserve_toys/toys_party.php\'" >Reservations</button>';
        }
        ?>
        <button class="button_menu_yellow" onclick="location.href = '<?php

            echo $url_menu . 'loans/loan.php';
       
        ?>'">
                    <?php
          
                        echo 'Loans';
                  
                    //
                    ?>
        </button>
        <button class="button_menu_logout" onclick="location.href = '<?php echo $url_logout; ?>'">Logout</button>
    </div>
    <div id="panel" style="display:none">
        <div id="home">
            <button class="button_menu_yellow_panel" onclick="location.href = '<?php echo $url_menu . 'home/index.php'; ?>'">Home</button>
             <button class="button_menu_yellow_panel" onclick="location.href = '<?php echo $url_menu . 'home/charts/charts.php'; ?>'" >Stats</button>
            <button class="button_menu_yellow_panel" onclick="location.href = '<?php echo $url_menu . 'home/videos.php'; ?>'">Videos</button>
            <button class="button_menu_yellow_panel" onclick="location.href = '<?php echo $url_menu . 'home/upload_files/index.php'; ?>'">Upload Pictures</button>
            <button class="button_menu_yellow_panel" onclick="location.href = '<?php echo $url_menu . 'home/auto/auto.php'; ?>'">Custom Setup</button>        
         </div>
        <div id="members">
            <button class="button_menu_red_panel" onclick="location.href = '<?php echo $url_menu . 'members/update/new.php'; ?>'">New Member</button>
             <?php
            if ($_SESSION['mibase_server'] == 'Yes') {
                echo '<button id = "reserve_btn" class="button_menu_red_panel" onclick="location.href = \'' . $url_menu . 'approve/members_approve.php\'" >Approve</button>';
            }
            ?> 
            <button class="button_menu_red_panel" onclick="location.href = '<?php echo $url_menu . 'roster/roster.php'; ?>'">Duty Roster</button>

        </div>
  
        <div id="toys">
            <button class="button_menu_panel" onclick="location.href = '<?php echo $url_menu . 'toys/update/new.php'; ?>'">New</button>
            <button class="button_menu_panel" onclick="location.href = '<?php echo $url_menu . 'toys/copy/new.php'; ?>'">Copy</button>
            <button class="button_menu_panel" onclick="location.href = '<?php echo $url_menu . 'toys/parts/parts.php'; ?>'">Parts</button>
            <button class="button_menu_panel" onclick="location.href = '<?php echo $url_menu . 'toys/alerts/alerts.php'; ?>'">Alerts</button>
            <button class="button_menu_panel" onclick="location.href = '<?php echo $url_menu . 'toys/locked/locked.php'; ?>'">Locked</button>
            <button class="button_menu_panel" onclick="location.href = '<?php echo $url_menu . 'toys/locked/withdrawn.php'; ?>'">Withdrawn</button>
            <button class="button_menu_panel" onclick="location.href = '<?php echo $url_menu . 'toys/toys_nopics.php'; ?>'">No Pictures</button>
            <button class="button_menu_panel" onclick="location.href = '<?php echo $url_menu . 'overdue/toys_onloan.php'; ?>'">On Loan</button>
            <button class="button_menu_panel" onclick="location.href = '<?php echo $url_menu . 'toys/stocktake/stocktake.php'; ?>'">Stocktake</button>
         </div>

        <div id="time_status"></div>
    </div>

</div>
<?php //echo "Last Update: " . date("h:i:sa", $_SESSION['LAST_ACTIVITY']);