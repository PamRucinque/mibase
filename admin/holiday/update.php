<?php

$str_alert = '';
$start = DateTime::createFromFormat('Y-m-d', $_POST['start']);
$end = DateTime::createFromFormat('Y-m-d', $_POST['end']);
$due = DateTime::createFromFormat('Y-m-d', $_POST['due']);
$ok = 'Yes';
If ($start >= $end) {
    $str_alert .= 'Start date must be greater than the End date.';
    $ok = 'No';
    //exit;
}
If ($due < $end) {
    $str_alert .= '<br>Due date must be greater than the End date.';
    $ok = 'No';
    //exit;
}
if (($_POST['start'] == '')||($_POST['end'] == '')||($_POST['due'] == '')){
    $str_alert .= '<br><font color="red">You must select all 3 dates in the boxes.</font>';
    $ok = 'No';
}
if ($ok == 'Yes') {
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    //$pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $query = "update transaction set due = ?   
where 
due >= ? and due <= ?
and return is null;";
//and get the statment object back
    $sth = $pdo->prepare($query);

    $array = array($_POST['due'], $_POST['start'], $_POST['end']);

//execute the preparedstament
    $sth->execute($array);
    $stherr = $sth->errorInfo();
//$prefid = $sth->fetchColumn();
    if ($stherr[0] != '00000') {
        $error = "An insert error occurred.\n";
        $error .= 'Error ' . $stherr[0] . '<br>';
        $error .= 'Error ' . $stherr[1] . '<br>';
        $error .= 'Error ' . $stherr[2] . '<br>';
        $ok = 'No';
        $str_alert = $error;
    } 
}
if ($ok == 'Yes'){
    $str_alert .= '<br><br>Thank you, your Due Dates have been updated.<br>';
}
//include('../connect.php');




