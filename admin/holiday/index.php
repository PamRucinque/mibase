<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../css/mibase.css" rel="stylesheet">
    </head>
    <body  onload="setFocus()">

        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            $button_str = 'OK';
            //$java_str = 'Close';
            $java_str = "$(location).attr('href', 'index.php')";
            $str_alert = '';
            if (isset($_POST['submit'])) {
                include('update.php');
            }
            ?>
            <section class="container-fluid" style="padding: 10px;">
                <div class="row">
                    <div class="col-sm-9">
                        <h2>Update toy due dates for the Holidays</h2><br><br>

                    </div>
                    <div class="col-sm-2">
                        <br><a href='../home/index.php' class ='btn btn-colour-yellow'>Back to Home</a><br>
                    </div>
                    <div class="col-sm-1">
                        <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=select_boxes' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                    </div>
                </div>
            </section>
            <section class="container-fluid" style="padding: 10px;">
                <?php include('form.php'); ?>
            </section>
            <section class="container-fluid" style="padding: 10px;">
                <?php include('msg_form.php'); ?>
            </section>

        </div>
        <script type="text/javascript" src="../js/jquery-3.0.0.js" charset="UTF-8"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    </body>
</html>
