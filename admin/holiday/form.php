<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            format: 'dd/mm/yyyy',
            startView: 'month',
            minView: 'month',
            autoclose: true
        };
        $('#start_date').datetimepicker(pickerOpts);
        $('#end_date').datetimepicker(pickerOpts);
        $('#due_date').datetimepicker(pickerOpts);
    });

    function setFocus()
    {
        var msg = document.getElementById("msg").innerText;
        //alert(msg);
        if (msg !== '') {
            $('#myModal').modal('show');
        }

    }

</script>
<form action="index.php" method="POST">
    <div class="row">
        <div class="col-sm-10">
            <h4> Select the date range for all toys due during this Holiday period </h4>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        Select the Start Date of the Holiday period:<br>
                        <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="start" data-link-format="yyyy-mm-dd" style='width: 250px;' id='start_date'>
                            <input class="form-control" size="16" type="text" id='start_date' value="" readonly>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        <input type="hidden" id="start" value="" name="start" /><br/>
                        Select the End Date of the Holiday period:<br>
                        <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="end" data-link-format="yyyy-mm-dd" style='width: 250px;' id='end_date'>
                            <input class="form-control" size="16" type="text" id='start_date' value="" readonly>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        <input type="hidden" id="end" value="" name="end" /><br/>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <font color="red">Select the date that all toys due in the holidays will be due when the Library re-opens:</font><br>
                        <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="due" data-link-format="yyyy-mm-dd" style='width: 250px;' id='due_date'>
                            <input class="form-control" size="16" type="text" id='start_date' value="" readonly>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        <input type="hidden" id="due" value="" name="due" /><br/>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12"  id="submit_header" style="min-height:100px;">
                    <br><input type=submit id="submit" name="submit" class="btn btn-success" value="Update Due Dates"> <br>
                </div>
            </div>

        </div>
        <div class="col-sm-2"></div>
    </div>
</form>


