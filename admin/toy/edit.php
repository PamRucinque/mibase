<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../css/mibase.css" rel="stylesheet">
        <script>
            function setFocus()
            {
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }
            var idleTime = 0;
            $(document).ready(function () {
                //Increment the idle time counter every minute.
                var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

                //Zero the idle timer on mouse movement.
                $(this).mousemove(function (e) {
                    idleTime = 0;
                });
                $(this).keypress(function (e) {
                    idleTime = 0;
                });
            });

            function timerIncrement() {
                idleTime = idleTime + 1;
                if (idleTime > 14) { // 20 minutes
                    alert('Your Session Has timed out, your data has been saved.');
                    document.forms["cc"].submit();
                    //document.location.href = '../update/toy_detail.php';
                }
            }
        </script>
    </head>
    <body  onload="setFocus()">
        <div id="form_container">
            <?php
            //Get the message vars Ready:
            include( dirname(__FILE__) . '/../menu.php');

            $button_str = 'OK';
            $link = '../toys/update/toy_detail.php?idcat=' . $_SESSION['idcat'];
            $java_str = "$(location).attr('href', '" . $link . "')";
            $str_alert = '';
            include('get_toy.php');

            //When the user submits the request:
            if (isset($_POST['submit'])) {
                include('save.php');
            } else {
                if ($_SESSION['level'] == 'admin') {
                     include('edit_form.php');
                } else {
                    echo '<h4>Toys Edits have been locked for Volunteer login.</h4>';
                    echo ' <a class="button1" href="../../toys/update/toy_detail.php?idcat=' . $_GET['idcat'] . '">Back to Toy Detail</a><br>';
                    echo '<br>' . $_SESSION['level'];
                }
            }
            ?>
        </div>
        <?php include ('msg_form.php'); ?>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
        <script>
        $(document).ready(function () {
            $('.dropdown-toggle').dropdown();
        });
        </script>
    </body>
</html>