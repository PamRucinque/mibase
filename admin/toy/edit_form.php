<?php
include(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bootstrap.min.css"/>
<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "d MM yy",
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "2000:+nn"
        };
        $("#date_purchase").datepicker(pickerOpts);
        $("#lockdate").datepicker(pickerOpts);
        $("#withdrawndate").datepicker(pickerOpts);
    });

    function change_cat(str) {
        //alert(str);
        document.getElementById('category').value = str;
    }


    function validate(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault)
                theEvent.preventDefault();
        }
    }
    function update_lockdate(str) {
        var dt = new Date();
        var yyyy = dt.getFullYear().toString();
        var mm = (dt.getMonth() + 1).toString();
        var dd = dt.getDate().toString();
        end = yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]);

        if (str == 'LOCKED') {
            document.getElementById('lockdate').value = end;
            alert("Lock Date has been updated.");
            return;
        }
        if (str == 'WITHDRAWN') {
            document.getElementById('withdrawndate').value = end;
            alert("Withdrawn Date has been updated.");
            return;
        }

        //document.getElementById("user1").focus();
    }


</script>
<?php
include('get_settings.php');
$storage_label = $_SESSION['settings']['storage_label'];
if (trim($storage_label) == '') {
    $storage_label = 'Storage Type';
}
$sub_category_label = $_SESSION['settings']['sub_category_label'];
if (trim($sub_category_label) == '') {
    $sub_category_label = 'Sub Category';
}
?>
<section class="container-fluid" style="padding: 10px;">
    <div class="row">
        <div class="col-sm-8">
            <h2>Edit Toy</h2>
            <h4>Edit Toy: <font color="blue"> <?php echo $toy['idcat'] . ': ' . $toy['toyname']; ?></font></h4>
        </div>
        <div class="col-sm-2">
            <br><a href='../toys/update/toy_detail.php?idcat=<?php echo $_SESSION['idcat']; ?>' class ='btn btn-info'>Back to Toy Detail</a><br>
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-1">
            <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=roster_preferences' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
        </div>
    </div>
</section>

<section class="container-fluid" style="padding: 10px;">
    <form action="edit.php" method="post">
        <div class="row" style="background-color:whitesmoke;">
            <div class="col-sm-6"  id="contact">
                Name of Toy: 
                <input type="text" class="form-control" id="toyname" name="toyname" value="<?php echo $toy['toyname'] ?>" required>
                <div class="row">
                    <div class="col-sm-4"><br><?php
                        if ($catsort == 'Yes') {
                            echo 'Category:<br><h4>' . $toy['category'] . '</h4>';
                        } else {
                            include('get_category.php');
                        }
                        ?></div>
                    <div class="col-sm-4"><br><?php include('get_sub_category.php'); ?></div>
                    <div class="col-sm-4"><br>No Pieces: 
                        <input type="number" name="no_pieces" id ="no_pieces" class="form-control" value="<?php echo $toy['no_pieces']; ?>" onkeypress='validate(event)'>
                    </div>
                    <div class="col-sm-4">                        

                    </div>
                </div>



            </div>
            <div class="col-sm-2">
                Toy Status:
                <select id="toy_status" name="toy_status" onchange="update_lockdate(this.value)" class="form-control">
                    <option value='<?php echo $toy['toy_status']; ?>' selected="selected"><?php echo $toy['toy_status']; ?></option>
                    <option value="ACTIVE" >ACTIVE</option>
                    <option value="DAMAGED" >DAMAGED</option>
                    <option value="FIRESALE" >FIRESALE</option>
                    <option value="IN STORAGE" >IN STORAGE</option>
                    <option value="LOCKED" >LOCKED</option>
                    <option value="MISSING" >MISSING</option>
                    <option value="MISSING PARTS" >MISSING PARTS</option>
                    <option value="PACKAGING" >PACKAGING</option>
                    <option value="PENDING" >PENDING</option>
                    <option value="PROCESSING" >PROCESSING</option>
                    <option value="REPAIR" >REPAIR</option>
                    <option value="SOLD" >SOLD</option>
                    <option value="WITHDRAWN" >WITHDRAWN</option>
                </select>  
                <br>Locking Status:
                <select id="stocktake_status" name="stocktake_status" class="form-control" >
                    <option value='<?php echo $toy['stocktake_status']; ?>' selected="selected"><?php echo $toy['stocktake_status']; ?></option>
                    <option value="ACTIVE" >ACTIVE</option>
                    <option value="LOCKED" >LOCKED</option>
                </select>
            </div>
            <div class="col-sm-2">
                Return Period (days)<input type="number" name="returnperiod" min="0" max="52" class="form-control" value="<?php echo $toy['returndateperiod']; ?>">
                <br>Lock Date: 
                <input type="text" name="lockdate" class="form-control" id ="lockdate" align="LEFT" value="<?php echo $toy['datestocktake']; ?>">
            </div>
            <div class="col-sm-2">
                Reservation Toy:
                <select id="reserve" name="reserve"  class="form-control" >
                    <option value='<?php echo $reserve; ?>' selected="selected"><?php echo $reserve; ?></option>
                    <option value="Yes" >Yes</option>
                    <option value="No" >No</option>
                </select>
                <br>Withdrawn Date: 
                <input type="text" name="withdrawndate" class="form-control" id ="withdrawndate" align="LEFT" value="<?php echo $toy['withdrawndate']; ?>">

            </div>
        </div>
        <div class="row"  style="background-color:whitesmoke;">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6"><br><?php include('get_age.php'); ?></div>
                    <div class="col-sm-3"><br><?php include('get_condition.php'); ?></div>
                    <div class="col-sm-3">
                        <br>Block Image:
                        <select id="block_image" name="block_image"  class="form-control" >
                            <option value='<?php echo $toy['block_image']; ?>' selected="selected"><?php echo $toy['block_image']; ?></option>
                            <option value="Yes" >Yes</option>
                            <option value="No" >No</option>
                        </select>

                    </div>

                </div>
                <div class="row"style="background-color:whitesmoke;">
                    <div class="col-sm-4">
                        <br>Date of Purchase: 
                        <input type="text" name="date_purchase" class="form-control" id ="date_purchase" align="LEFT" value="<?php echo $toy['date_purchase']; ?>">
                    </div>

                    <div class="col-sm-4"><br>Cost: 
                        <input type="text" class="form-control" id="cost" name="cost" value="<?php echo $toy['discountcost'] ?>" onkeypress='validate(event)'>
                    </div>
                    <div class="col-sm-4"><br>Replace Cost: 
                        <input type="text" class="form-control" id="discountcost" name="discountcost" value="<?php echo $toy['cost'] ?>" onkeypress='validate(event)'>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12"><br>Lock Reason: 
                        <input type="text" class="form-control" id="lockreason" name="lockreason" value="<?php echo $toy['lockreason'] ?>">
                        <div class="row">
                            <div class="col-sm-4"><br>Rent:
                                <input type="text" name="rent" id ="rent" align="LEFT"  class="form-control" value="<?php echo $toy['rent']; ?>" onkeypress='validate(event)'>
                            </div>
                            <div class="col-sm-4"><?php include('get_location.php'); ?></div>
                            <div class="col-sm-4"><?php include('get_loan_type.php'); ?></div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="row" style="background-color:whitesmoke;">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6"><br>Supplier: 
                        <input type="text" class="form-control" id="supplier" name="supplier" value="<?php echo $toy['supplier'] ?>" >
                    </div>

                    <div class="col-sm-6"><br>Manufacturer: 
                        <input type="text" class="form-control" id="manufacturer" name="manufacturer" value="<?php echo $toy['manufacturer'] ?>" >
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-7">
                        <br>Donated by: 
                        <input type="text" class="form-control" id="sponsorname" name="sponsorname" value="<?php echo $toy['sponsorname'] ?>">
                    </div>
                    <div class="col-sm-4">
                        <?php include('get_storage.php'); ?>
                    </div>

                </div>

            </div>
        </div>
        <div class="row"  style="background-color:whitesmoke;">
            <div class="col-sm-6"><br>Keywords: 
                <input type="text" class="form-control" id="keywords" name="keywords" value="<?php echo $toy['twitter'] ?>">
            </div>

            <div class="col-sm-6"><br>Link to Product Web Page: 
                <input type="text" class="form-control" id="link" name="link" value="<?php echo $toy['link'] ?>">
            </div>
        </div>
        <div class="row" style="background-color:whitesmoke;">
            <div class="col-sm-4">
                <br>Toy Description - Column 1:
                <textarea id="desc1" name="desc1" rows="10" class="form-control"><?php echo $toy['desc1']; ?></textarea>
            </div>
            <div class="col-sm-4">
                <br>Toy Description - Column 2:
                <textarea id="desc2" name="desc2" rows="10" class="form-control"><?php echo $toy['desc2']; ?></textarea>
            </div>
            <div class="col-sm-4">
                <br>Comments:
                <textarea id="comments" name="comments" rows="10" class="form-control"><?php echo $toy['comments']; ?></textarea>
            </div>
        </div>
        <div class="row" style="background-color:whitesmoke;">
            <div class="col-sm-4">
                <br>Alerts:
                <textarea id="alert" name="alert" rows="3" class="form-control"><?php echo $toy['alert']; ?></textarea>
            </div>
            <div class="col-sm-4">
                <br>Warnings:
                <textarea id="warnings" name="warnings" rows="3" class="form-control"><?php echo $toy['warnings']; ?></textarea>
            </div>
            <div class="col-sm-4">
                <br>User1:
                <textarea id="user1" name="user1" rows="3" class="form-control"><?php echo $toy['user1']; ?></textarea>
            </div>
        </div>
        <div class="row" style="background-color:whitesmoke;padding-bottom: 15px;">
            <div class="col-sm-4">
                <br>Freight<br>
                <input type="text" name="freight" id ="freight" align="LEFT" value="<?php echo $toy['freight']; ?>" onkeypress='validate(event)'>
            </div>
            <div class="col-sm-4">
                <br>Processing Time (minutes): <br>
                <input type="number" name="time" id ="time" align="LEFT" value="<?php echo $toy['process_time']; ?>" onkeypress='validate(event)'>
            </div>
            <div class="col-sm-3">
                <br>Packaging Cost: <br>
                <input type="text" name="package" id ="package" align="LEFT" value="<?php echo $toy['packaging_cost']; ?>" onkeypress='validate(event)'><br>

            </div>
        </div>
        <div class="row" style="background-color:lightgoldenrodyellow;">
            <div class="col-sm-12"  id="submit_header" style="min-height:50px;padding-right:0px;padding-left:20px;padding-top: 5px;">
                <br><input type=submit id="submit" name="submit" class="btn btn-success" value="Save Toy"> <br>
            </div>
        </div>
        <input type="hidden" name="id" id="id" value="<?php echo $toy['idcat']; ?>">
        <input type="hidden" name="category" id="category" value="<?php echo $toy['category']; ?>">
    </form>
</section>
<?php include ('msg_form.php'); ?>




