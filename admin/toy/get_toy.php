<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');

include('../connect.php');
$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$sql = "SELECT * from toys WHERE upper(toys.idcat) = ?";

$sth = $pdo->prepare($sql);
$array = array($_SESSION['idcat']);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

for ($ri = 0; $ri < $numrows; $ri++) {
    $toy = $result[$ri];
    $toy['rent'] = get_zero($toy['rent']);
    $toy['cost'] = get_zero($toy['cost']);
    $toy['discountcost'] = get_zero($toy['discountcost']);
    if ($toy['reservecode'] == null || $toy['reservecode'] == ''){
        $reserve = 'No';
    }else{
        $reserve = 'Yes';
    }

    $_SESSION['idcat'] = $toy['idcat'];
}

if ($stherr[0] != '00000') {
    $error_msg .= "An  error occurred.\n";
    $error_msg .= 'Error' . $stherr[0] . '<br>';
    $error_msg .= 'Error' . $stherr[1] . '<br>';
    $error_msg .= 'Error' . $stherr[2] . '<br>';
}

function get_zero($val) {
    if ($val == Null) {
        $vla = 0;
    }
    return $val;
}

?>