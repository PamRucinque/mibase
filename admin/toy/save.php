<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');

if (isset($_GET['idcat'])) {
    $_SESSION['idcat'] = $_GET['idcat'];
}
$str_alert = '';
$today = date('Y-m-d');

$now = date('Y-m-d H:i:s');

$toyname = clean($_POST['toyname']);
$toy_status = clean($_POST['toy_status']);
$desc1 = clean($_POST['desc1']);
$desc2 = clean($_POST['desc2']);
if (isset($_POST['comments'])) {
    $comments = clean($_POST['comments']);
}

$supplier = clean($_POST['supplier']);
$manufacturer = clean($_POST['manufacturer']);
$block_image = $_POST['block_image'];
$now = date('Y-m-d H:i:s');
if ($_POST['no_pieces'] == '') {
    $_POST['no_pieces'] = 0;
}

//include('../../connect.php');

if ($_POST['date_purchase'] == '') {
    //$purchase = $now;
    $purchase = '1900-01-01';
} else {
    $purchase = $_POST['date_purchase'];
}
if ($_POST['discountcost'] == '') {
    $discountcost = 0;
} else {
    if (is_numeric($_POST['discountcost'])) {
        $discountcost = $_POST['discountcost'];
    } else {
        $discountcost = 0;
    }
}
if ($_POST['package'] == '') {
    $package = 0;
} else {
    if (is_numeric($_POST['package'])) {
        $package = $_POST['package'];
    } else {
        $package = 0;
    }
}

if ($_POST['cost'] == '') {
    $cost = 0;
} else {
    if (is_numeric($_POST['cost'])) {
        $cost = $_POST['cost'];
    } else {
        $cost = 0;
    }
}
if ($_POST['freight'] == '') {
    $freight = 0;
} else {
    if (is_numeric($_POST['freight'])) {
        $freight = $_POST['freight'];
    } else {
        $freight = 0;
    }
}

if ($_POST['lockdate'] == '') {
    $datestocktake = '1900-01-01';
} else {
    $datestocktake = $_POST['lockdate'];
}
if ($_POST['withdrawndate'] == '') {
    $datewithdrawn = '1900-01-01';
} else {
    $datewithdrawn = $_POST['withdrawndate'];
}
if (isset($_POST['reservecode'])) {
    if ($_POST['reservecode']) {
        $reservecode = $_POST['id'];
    } else {
        $reservecode = '';
    }
}

if (isset($_SESSION['idcat'])) {
    $idcat = $_SESSION['idcat'];
} else {
    $idcat = $_POST['idcat'];
}

if ($_POST['time'] == '') {
    $_POST['time'] = 0;
}
if ($_POST['reserve'] == 'Yes') {
    $reservecode = $idcat;
} else {
    $reservecode = null;
}


$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$query_edit = "UPDATE toys SET 
                    toyname=?,
                    toy_status=?,
                    category=?,
                    stocktake_status=?,
                    sub_category=?,
                    desc1=?,
                    desc2=?,
                    comments=?,
                    date_purchase=?,
                    datestocktake=?,
                    withdrawndate=?,
                    lockreason=?, 
                    age=?,
                    reservedfor=?,
                    alert=?,
                    user1=?, 
                    warnings=?,
                    supplier=?,
                    manufacturer=?,
                    no_pieces=?,
                    returndateperiod=?,
                    process_time=?,
                    cost=?,
                    rent=?,
                    storage=?,
                    loan_type=?,
                    sponsorname=?,
                    discountcost=?, 
                    packaging_cost=?,
                    freight=?,
                    reservecode=?,
                    block_image=?,
                    twitter=?,
                    link=?,
                    location=?,
                    modified=?,
                    changedby=?
                    WHERE idcat=?;";
//create the array of data to pass into the prepared stament
$sth = $pdo->prepare($query_edit);
$array = array($toyname, $toy_status, $_POST['category'],$_POST['stocktake_status'], $_POST['sub_cat'], $desc1, $desc2, $_POST['comments'],
    $purchase, $datestocktake, $datewithdrawn, $_POST['lockreason'], $_POST['age'], $_POST['condition'],
    $_POST['alert'], $_POST['user1'], $_POST['warnings'], $supplier,
    $manufacturer, $_POST['no_pieces'], $_POST['returnperiod'], $_POST['time'],
    $cost, $_POST['rent'], $_POST['storage'], $_POST['loan_type'], $_POST['sponsorname'],
    $discountcost, $package, $freight, $reservecode, $block_image, $_POST['keywords'], $_POST['link'], $_POST['location'], $now, $_SESSION['username'], $_POST['id']);
$sth->execute($array);
$stherr = $sth->errorInfo();


if ($stherr[0] != '00000') {
    $str_alert .= "An UPDATE query error occurred.\n";
    //echo $connect_pdo;
    $str_alert .= 'Error ' . $stherr[0] . '<br>';
    $str_alert .= 'Error ' . $stherr[1] . '<br>';
    $str_alert .= 'Error ' . $stherr[2] . '<br>';
    //exit;
} else {
    $str_alert .= '<br> Toy Number ' . $_SESSION['idcat'] . ' has been successfully update. <br>';
    $edit_url = '../toy_detail.php?id=' . $_POST['id'];
}

function clean($input) {
    $output = stripslashes($input);
    $output = str_replace("'", "`", $output);
    //$output = str_replace('/', '-', $output);
    //$output = str_replace("\", " ", $output);
    return $output;
}
?>
</div>
</body>
</html>
