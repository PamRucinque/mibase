<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EDGE,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Mibase Database Management Systems</title>
        <meta name="description" content="Vanilla JS Responsive Menu" />
        <link rel="shortcut icon" type="image/x-icon" href="https://www.xtianares.com/images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Fjalla+One|Roboto+Slab:300,400,500,700" />
        <link rel="stylesheet" href="css/menu.css" />

    </head>

    <body>
        <section class="container fluid">
            <div class="col-sm-12">
                <br />
<?php
if (iphone()) {
    echo '<h1>MiBase Online</h1>';
} else {
    echo '<a><img src="https://ttg.mibase.com.au/login_image/online2.jpg" alt="Mibase Logo" width="500"></a>';
}

function iphone() {
    if (!isset($_SERVER['HTTP_USER_AGENT'])) {
        return false;
    }
    if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPod') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad') || strstr($_SERVER['HTTP_USER_AGENT'], 'Android')) {
        return true;
    }
    //return strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false;
}
?>
                <nav id="nav">
                    <ul>
                        <li><a href="menu.php">Home</a></li>

                        <li><span class="submenu">Toys</span>
                            <ul class="submenu">
                                <li><a href="#">New Toy</a></li>
                                <li><a href="#">Copy Toy</a></li>
                                <li><a href="#">Parts</a></li>
                                <li><a href="#">Alerts</a></li>
                                <li><a href="#">Locked</a></li>
                                <li><a href="#">Withdrawn</a></li>
                                <li><a href="#">No Pictures</a></li>
                                <li><a href="#">Toys On Loan</a></li>
                                <li><a href="#">Stocktake</a></li>
                            </ul>
                        </li>
                        <li><span class="submenu">Members</span>
                            <ul class="submenu">
                                <li><a href="#">New Member</a></li>
                                <li><a href="#">Approve</a></li>
                                <li><a href="#">Expired</a></li>
                                <li><a href="#">Resigned</a></li>
                                <li><a href="#">Bulk Emails</a></li>
                                <li><a href="#">Bulk Alerts</a></li>
                                <li><a href="#">Gift Cards</a></li>

                            </ul>
                        </li> 
                        <li><span class="submenu">Setup</span>
                            <ul class="submenu">
                                <li><a href="#">Settings</a></li>
                                <li><a href="#">Edit Website</a></li>
                                <li><a href="#">Files</a></li>
                                <li><a href="#">Videos</a></li>
                                <li><a href="#">T's amd C's</a></li>
                                <li><a href="#">Upload Pictures</a></li>
                                <li><a href="#">Custom Setup</a></li>
                                <li><a href="#">Templates</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Reservations</a></li>
                        <li><span class="submenu">Analyze</span>
                            <ul class="submenu">
                                <li><a href="#">Stats</a></li>
                                <li><a href="#">Reports</a></li>
                                <li><a href="#">Export</a></li>

                            </ul>
                        </li>
                        <li><span class="submenu">Lists</span>
                            <ul class="submenu">
                                <li><a href="#">Toy Categories</a></li>
                                <li><a href="#">Toy Sub-Categories</a></li>
                                <li><a href="#">Toy Age Groups</a></li>
                                <li><a href="#">Toy Condition</a></li>
                                <li><a href="#">Toy Storage</a></li>
                                <li><a href="#">Toy Warnings</a></li>


                            </ul>

                        </li>
                        <li class="last"><a href="#">Loans</a></li>

                        <li class="right"><a href="#">Log Out</a></li>
                        <li class="onmobile"><a href="#">Only On Mobile</a></li>
                    </ul>
                </nav><!-- end of #nav -->
            </div>
        </section>
        <section class="container">

        </section>

        <script type="text/javascript" src="js/menu.js"></script>

    </body>
</html>


