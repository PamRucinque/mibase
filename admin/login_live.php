<meta name="viewport" content="width=device-width">
<link rel="stylesheet" href="css/menu.css" />
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/login.css">
<?php
//echo $_SERVER["SERVER_NAME"];
$server = $_SERVER["SERVER_NAME"];
//echo $_SERVER['SERVER_NAME'];
include('config.php');
//echo $mibase_server;
$error = '';


if (strtoupper(substr($server, 0, 4)) == 'WWW.') {

    //echo $server;
    $length = strlen($server) - 4;
    $subdomain = substr($server, 4, $length - 14);
    $headerStr = 'location:https://' . $subdomain . $domain . 'login.php';
    //echo $headerStr;
    //echo $subdomain;
    header($headerStr);
}
if ($mibase_server == 'Yes') {
    $server = $_SERVER["SERVER_NAME"];
    $length = strlen($server) - 14;
    $pos = strrpos($server, ".");
    $subdomain = substr($server, 0, $length);
    $logo = substr($server, 0, $length) . '.jpg';
    $home = 'https://' . $subdomain . '.mibase.com.au/home/index.php';
} else {

    $home = $subdomain . $domain;
}
$link = $server . '/php/new/new.php';


session_start();
if (isset($_SESSION['login_status'])) {
    $error = $_SESSION['login_status'];
}
//echo $server;
?>


<body style="background-image:url('css/images/bg.jpg');background-repeat: no-repeat;center: fixed; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;" >
    <div class="container">
        <div class="row">
            <div class="card card-container">
                <?php
                if ($mibase_server == 'Yes') {
                    echo '<img src="https://' . $subdomain . $domain . 'login_image/' . $subdomain . '.jpg' . '" alt="Library Logo" height="150" class="profile-img-card">';
                } else {
                    echo '<img src="logo.jpg" alt="Library Logo" height="150" class="profile-img-card">';
                }
                ?>
                <h1>Mibase Administrator Login</h1>
                <?php
                if ($mibase_server == 'Yes') {

                    // if (($subdomain == 'boxhill')||($subdomain == 'canterbury')) {
                    // Gregor if rollback is required from boxhill, please change the line below to action ="checklogin.php", thank you :)
                    echo '<form name="form1" method="post" action="checklogin_new.php"  class="form-signin">';
                    // } else {
                    //     echo '<form name="form1" method="post" action="checklogin.php"  class="form-signin">';
                    // }
                } else {
                    echo '<form name="form1" method="post" action="checklogin_single.php"  class="form-signin">';
                }
                ?>

                <span id="reauth-email" class="reauth-email"><font color="red"><?php echo $error ?></font></span>
                <input name="myusername" type="text" id="myusername" class="form-control" placeholder="username">
                <input name="mypassword" type="password" id="mypassword" class="form-control" placeholder="Password">
                <div class="col-md-6">
                    <a href="../home/index.php" class="btn btn-lg btn-info btn-block btn-signin">Home</a>
                </div>
                <div class="col-md-6">
                    <input type="submit" name="Submit" value="Login" class="btn btn-lg btn-primary btn-block btn-signin">  
                </div>           
                </form><!-- /form -->   
            </div><!-- /card-container -->
        </div><!-- /row -->
    </div><!-- /container -->


</body>