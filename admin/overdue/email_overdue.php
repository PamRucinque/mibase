<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');


//include ('../get_settings.php');
////include( dirname(__FILE__) . '/../connect.php');
include( dirname(__FILE__) . '/functions/functions.php');
$_SESSION['loan_status'] = ' ';


if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $_SESSION['return_alert'] = '';
    //$returntoy = return_toy($id,$recordfine,$chargerent);
    $trans = get_bor($id);
    $email_to = $trans['email'];
    $msg = msg_body($trans['borid'], $trans['longname']);
    $message_txt = $msg['message'];
 
    
    echo $email_to  . '<br>';
    echo $trans['borid'] . '<br>';
    echo $message_txt . '<br><br>';
    echo '<h4><font color="red">NOT TURNED ON YET</font></h4>';
    
    include( dirname(__FILE__) . '/send_email.php');
    
    $redirect = "Location: toys_onloan.php";
    //header($redirect);
    } 

//echo $query_return;


?>
