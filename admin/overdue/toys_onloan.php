<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<html>
    <?php
    $mibase_server = $_SESSION['mibase_server'];
    if ($mibase_server == 'Yes'){
         include( dirname(__FILE__) . '/../header.php');
    }else{
        include( dirname(__FILE__) . '/../head.php');
    }
   
    $total = 0;
    $total_str = '';
    ?>
    <body>
        <div class="container-fluid">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            include( dirname(__FILE__) . '/../header_detail/header_detail.php');
            ?>
            <div style="display: none; position: absolute; z-index: 110; left: 400; top: 1000; width: 15; height: 15" id="preview_div"></div>
            
            <div class="row" style="width: 100%;padding-top: 10px;background-color: whitesmoke;">
                <div class="col-sm-10">
                    <form method="post" action="toys_onloan.php" > 
                        <div class="row">
                            <div class="col-sm-4">
                                <input id="search" name="search"  type="text" value="" placeholder="Search.." class="form-control"/></td>
                            </div>
                            <div class="col-sm-1">
                                <input class="btn btn-danger" type="submit" name ="submit" value="Reset">
                            </div>
                            <div class="col-sm-7">
                                <input class="btn btn-warning" type="submit" name ="overdue" id="overdue" value="Overdues Only">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-2" style="text-align: right;">
                    <h2>Toys On Loan</h2>
                </div>
            </div>

            <?php  include( dirname(__FILE__) . '/result_onloan.php'); ?>

        </div>
    </body>

</html>