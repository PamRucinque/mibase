<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

function get_bor($transid) {
    //include( dirname(__FILE__) . '/../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "select * FROM transaction where id=" . $transid . " AND return Is Null;";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $borid = $row['borid'];
    $sql = "SELECT firstname, surname, emailaddress as email from borwrs WHERE id =" . $borid . ";";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $longname = $row['firstname'] . ' ' . $row['surname'];
    $email = $row['email'];
    //$trans = 4;
    return array("longname" => $longname, "email" => $email, "borid" => $borid, "status" => $status);
}


function msg_body($borid, $longname) {
    $success = 'No';
    //include( dirname(__FILE__) . '/../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $status = '';
    $overdue_toys = "SELECT * FROM transaction WHERE borid = " . $borid . " AND return is null;";
    $count = 1;
    $trans = pg_exec($conn, $overdue_toys);
    $x = pg_numrows($trans);
    $subject = 'Membership #: '. $borid . ': ' . $longname . "\r\n";
    $subject .= 'You have ' . $x . ' toys overdue' . "\r\n";
    $subject .= '-------------------------------------' . "\r\n";

//echo 'number rows' . $x;
     for ($ri = 0; $ri < $x; $ri++) {
         $row = pg_fetch_array($trans, $ri);
       
         $subject .= ' ' . $row['idcat'] . ':  ' ;
         $subject .= ' ' . $row['item'] . '  Due: ' . $row['due'] . "\r\n"; 
        //echo "<tr>\n";
    }
    $subject .= '------------------------------------- ' . "\r\n";
    return array("status" => $status, "result" => $success, "borid" => $borid, "message" => $subject);
}



?> 