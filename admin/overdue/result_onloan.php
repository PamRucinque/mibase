<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<style type="text/css">
    tr:hover { 
        color: red; }
</style>
<?php

if (isset($_POST['submit'])) {
    $_SESSION['search'] = $_POST['search'];
} else {
    if (isset($_SESSION['search'])) {
        $_POST['search'] = $_SESSION['search'];
    }
}


$search = "";
if (isset($_POST['search'])) {
    $search = $_POST['search'];
}

//error message (not found message)

$total = 0;
//include( dirname(__FILE__) . '/../connect.php');
$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);


$query_toys = "SELECT toys.*,
        transaction.borname as borname, 
        transaction.id as transid, 
        transaction.return, 
        transaction.borid as borid,
        transaction.idcat as idcat, 
        transaction.item as item,
        transaction.due as due, 
        transaction.date_loan 
FROM
     transaction 
     LEFT JOIN toys ON upper(Transaction.idcat) = upper(toys.idcat)
     WHERE (((transaction.return) Is Null) AND (
     upper(toyname) LIKE ?
     OR upper(borname) LIKE ?
     OR upper(toys.idcat) LIKE ?
     )
     
) order by due asc, borid;";

$query_toys_overdue = "SELECT toys.*,
        transaction.borname as borname, 
        transaction.id as transid, 
        transaction.idcat as idcat, 
        transaction.item as item,
        transaction.return, 
        transaction.borid as borid,
        transaction.due as due, 
        transaction.date_loan 
FROM
     transaction 
     LEFT JOIN toys ON upper(Transaction.idcat) = upper(toys.idcat) 
     WHERE (((transaction.return) Is Null) AND ((transaction.due) < Now())
     AND (
     upper(toyname) LIKE ?
     OR upper(borname) LIKE ?
     OR upper(toys.idcat) LIKE ?
     )
     
) order by due asc, borid asc;";


//echo $query_toys;

if (isset($_POST['overdue'])) {
    $query_toys = $query_toys_overdue;
    $_SESSION['search'] = $_POST['search'];
    $search = $_POST['search'];
}
$search = strtoupper($search);
$search_str = '%' . $search . '%';

//echo $search_str;

$sth = $pdo->prepare($query_toys);
//$array = array($search_str, $search_str, $search_str, $search_str,$search_str, $category_str, $age_str);
//$array = array($search_str, $search_str, $search_str, $search_str, $search_str, $category_str, $age_str);
$array = array($search_str, $search_str, $search_str);
$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

//$query = "SELECT * FROM toys ORDER by id ASC;";


$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");

if (isset($_GET['r'])) {
    $toprint = sizeof($result);
} else {
    $toprint = 100;
}
$msg_pic = '';
$onclick = '';
$total = sizeof($result);
$x = 0;
$count = 0;
if (sizeof($result) < 100) {
    $toprint = sizeof($result);
    $toprint_txt = " ";
} else {
    $link = '<a class="button_small" href="toys_onloan.php?r=' . $toprint . '">Showall</a>';
    $toprint_txt = $toprint . ' of ' . $total . " shown. " . $link;
}
$total_str = '  <font color="red">Total: ' . $total . '</font>';
include('heading.php');
for ($ri = 0; $ri < $toprint; $ri++) {
    $count = $count + 1;
    //$row = pg_fetch_array($result, $ri);
    $row = $result[$ri];
    $x = $x + 1;
    if (is_numeric($x)) {
        if ($x % 2 == 0) {
            $bg_color = 'background-color: white;';
        } else {
            $bg_color = 'background-color: whitesmoke;';
        }
    }
    $transid = $row['transid'];
    $toyname = $row["item"];
    $toy_status = $row["toy_status"];
    $borname = $row['borname'];
    $due = $row['due'];
    $trans_borid = $row['borid'];
    $due = $row['due'];
    $ts1 = strtotime($row['due']);
    $ts2 = strtotime($today);

    $overdue = -1 * round(($ts2 - $ts1) / (-60 * 60 * 24), 0);
    if ($row['due'] != '') {
        $format_date_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);
    } else {

        $format_date_due = '';
    }

    $id = $row["transid"];
    $idcat = $row["idcat"];

    if ($_SESSION['shared_server']) {
        $file_pic = $_SESSION['web_root_folder'] . '/' . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
        }
    } else {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
        }
    }
    $toy_button = '<a href="../toys/update/toy_detail.php?idcat=' . $idcat . '" onmouseover="showtrail(175,220, \'' . $pic_url . '\');" onmouseout="hidetrail();" class="btn btn-primary btn-sm" style="height: 25px;min-width:80px;"/>' . $idcat . '</a>';

    $img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';

    $status = '<font color="green">ON LOAN </font>';
    if ($row['due'] < $today) {
        $status = '<font color="red">OVERDUE (' . $overdue . ')</font>';
    }

    $member_button = '<a class="btn btn-colour-maroon btn-sm" style="height: 25px;min-width:50px;" href="../members/update/member_detail.php?borid=' . $trans_borid . '">' . $trans_borid . '</a>';
    include('row.php');
}
?>