<?php

require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<?php

include('../connect.php');

if (isset($_POST['loan_date'])) {
    $loan_date = trim($_POST['loan_date']);
    //echo $loan_date;
} else {
    $loan_date = date('yyyy-mm-dd');
}
//$loan_date = '2019-02-02';
$x = 0;
$total = 0;
$total_returns1 = 0;
$total_returns = 0;
$total_loans1 = 0;
$total_loans = 0;
$location_total = 0;
$location_total1 = 0;
$curr_location = '';
$location2 = '';
$location1 = '';
$borname = '';
$str_edit = '';
$count_new = 0;
$count_renew = 0;
$new_str = '';
$location1_total = 0;

$sql = "select firstname, surname, membertype, borwrs.id as borid,datejoined, renewed, 
       (CASE WHEN t.location is null THEN r.location ELSE t.location END) as location,
       (CASE WHEN datejoined::text = ? THEN 'Yes' ELSE 'No' END) as join_today,
       (CASE WHEN renewed::text = ? THEN 'Yes' ELSE 'No' END) as renew_today,
COALESCE(t.loans,0) as loans,COALESCE(r.returns,0) as returns 
from borwrs
left join (select count(id) as loans, borid, date_loan, min(location) as location from transaction where date_loan = ?  group by date_loan, borid) t on t.borid = borwrs.id
left join (select count(id) as returns, borid, return, min(location) as location from transaction where return = ?  group by return, borid) r on r.borid = borwrs.id 
left join (select count(id) as new, id from borwrs where datejoined = ?  group by datejoined, id) m on m.id = borwrs.id 
left join (select count(id) as renew, id from borwrs where renewed = ?  group by renewed, id) mr on mr.id = borwrs.id 
where 
(t.loans > 0 or r.returns > 0 or m.new > 0 or mr.renew > 0) 
and (t.date_loan::text = ? or r.return::text = ? or datejoined::text = ? or renewed::text = '2018-11-10')
order by surname, firstname;";

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$sth = $pdo->prepare($sql);
$array = array($loan_date, $loan_date, $loan_date, $loan_date, $loan_date, $loan_date, $loan_date, $loan_date, $loan_date);
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $_SESSION['error'] = "An  error occurred.\n";
    $_SESSION['error'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[2] . '<br>';
}
$numrows = $sth->rowCount();


for ($ri = 0; $ri < $numrows; $ri++) {
    $daily = $result[$ri];
    if ($curr_location == '') {
        $curr_location = $daily['location'];
    }
    $total_returns = $total_returns + $daily['returns'];
    $total_loans = $total_loans + $daily['loans'];
    if ($ri == 0) {
        $location1 = $daily['location'];
    }

    if ($ri == 0) {
        include('heading.php');
    }
    if ($curr_location != $daily['location']) {
        $location1_total = $location_total;
        $location_total = 0;
        $location2 = $daily['location'];
        $total_returns1 = $total_returns;
        $total_loans1 = $total_loans;
        $total_loans = 0;
        $total_returns = 0;
    }
    $borname = $daily['surname'] . ', ' . $daily['firstname'];
    $link = 'member_detail.php?borid=' . $daily['borid'];
    $onclick = 'javascript:location.href="' . $link . '"';
    $new_str = '';
    if ($daily['join_today'] == 'Yes') {
        $new_str = 'New Member';
    }
    if (($daily['join_today'] == 'No') && ($daily['renew_today'] == 'Yes')) {
        $new_str = 'Renewed Member';
    }
    include('row_bg_color.php');
    include('row.php');
    $total = $total + 1;
    $curr_location = $daily['location'];
    $location_total = $location_total + 1;
}



//$required = number_format($roster['required'],0);
?>






