<?php

require(dirname(__FILE__) . '/../mibase_check_login.php');
include('../connect.php');

if (isset($_SESSION['borid'])) {
    $memberid = $_SESSION['borid'];
} else {
    $memberid = 0;
}
$loan_date = '2019-02-02';
$x = 0;
$total = 0;
$total_returns = 0;
$total_loans = 0;
$total_new = 0;
$total_renew = 0;
$str_edit = '';
$count_new = 0;
$count_renew = 0;
$new_str = '';
$curr_month = '';
$total_members = 0;

$sql = "select  tr.date_loan, to_char(tr.date_loan, 'dd-Mon-yyyy') as session,to_char(tr.date_loan, 'Day') as weekday, to_char(tr.date_loan, 'Month') as month,
COALESCE(t.loans,0) as loans, COALESCE(r.returns,0) as returns, COALESCE(dj.new,0) as new, m.members,COALESCE(dr.renew,0) as renew 
from transaction tr 
left join (select count(id) as loans, date_loan from transaction group by date_loan) t on t.date_loan = tr.date_loan
left join (select count(id) as returns, return from transaction group by return) r on r.return = tr.date_loan
left join (select count(id) as new, datejoined from borwrs group by datejoined) dj on dj.datejoined = tr.date_loan
left join (select count(id) as renew, renewed from borwrs where renewed != datejoined group by renewed) dr on dr.renewed = tr.date_loan
left join (select count(borid) as members, date_str from ((select date_loan as date_str, borid
		from transaction 
		group by  date_loan, borid)
		UNION
		(select return as date_str,borid  
		from transaction 
		group by  return, borid)) t
		group by  date_str) m on m.date_str = tr.date_loan
where tr.date_loan >= current_date - INTERVAL '1 year'
group by tr.date_loan, t.loans, r.returns, dj.new, m.members, dr.renew 
order by tr.date_loan desc;";

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$sth = $pdo->prepare($sql);
$array = array();
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $_SESSION['error'] = "An  error occurred.\n";
    $_SESSION['error'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[2] . '<br>';
}
$numrows = $sth->rowCount();
if ($numrows > 0) {
    include('heading_session.php');
}

for ($ri = 0; $ri < $numrows; $ri++) {
    $daily = $result[$ri];
    if ($ri == 0) {
        $curr_month = $daily['month'];
    }
    if ($curr_month != $daily['month']) {
        include('row_month.php');
        $total_returns = 0;
        $total_loans = 0;
        $total_new = 0;
        $total_renew = 0;
        $total_members = 0;
    }
    $total_returns = $total_returns + $daily['returns'];
    $total_loans = $total_loans + $daily['loans'];
    $total_new = $total_new + $daily['new'];
    $total_renew = $total_renew + $daily['renew'];
    $total_members = $total_members + $daily['members'];

    $ref1 = '<form id = "form_' . $total . '" action="detail.php" method="POST"><input type="hidden" name="loan_date" value="' . $daily['date_loan'] . '">';
    $ref1 .= '</form>';

    $onclick = 'get_detail(' . $total . ');';
    $new_str = '';
    include('row_bg_color_session.php');
    include('row_session.php');
    $total = $total + 1;
    $curr_month = $daily['month'];
}



//$required = number_format($roster['required'],0);
?>






