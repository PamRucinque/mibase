<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../css/mibase.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            if (!session_id()) {
                session_start();
            }
            if (isset($_POST['loan_date'])) {
                $loan_date = trim($_POST['loan_date']);
                //echo $loan_date;
            } else {
                $loan_date = date('yyyy-mm-dd');
            }
            $format_loan_date = substr($loan_date, 8, 2) . '-' . substr($loan_date, 5, 2) . '-' . substr($loan_date, 0, 4);
            $dayofweek = date('l', strtotime($loan_date));
            ?>
            <section class="container-fluid" style="padding: 10px;">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>Member stats on <?php echo $dayofweek . ', ' . $format_loan_date; ?></h2><br><br>
                        <?php //echo $_SESSION['library_code'];   ?>
                    </div>
                    <div class="col-sm-2">
                        <br><a href='summary.php' class ='btn btn-info'>Back to List</a><br>
                    </div>
                    <div class="col-sm-1">


                    </div>
                    <div class="col-sm-1">
                        <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=roster_preferences' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <?php include 'list.php'; ?>
                    </div>
                    <div class="col-sm-4">
                        <?php include 'footer.php'; ?>
                    </div>
                </div>

            </section>
        </div>

        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.dropdown-toggle').dropdown();
            });
        </script>
    </body>
</html>