<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../css/mibase.css" rel="stylesheet">
        <script>
            function get_detail(str) {
                document.getElementById("form_" + str).submit();
            }
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            if (!session_id()) {
                session_start();
            }
            ?>
            <section class="container-fluid" style="padding: 10px;">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>Daily Stats for each Session in the last 12 months.</h2><br>
                        <p><font color="red">single click on row to go to details</font></p>
                        <?php //echo $_SESSION['library_code'];  ?>
                    </div>
                    <div class="col-sm-2">
                        <br><a href='../home/index.php' class ='btn btn-info'>Back to Home</a><br>
                    </div>
                    <div class="col-sm-1">


                    </div>
                    <div class="col-sm-1">
                        <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=roster_preferences' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <?php include 'list_session.php'; ?>
                    </div>
                    <div class="col-sm-4">
                        <?php //include 'footer_session.php'; ?>
                    </div>
                </div>

            </section>
        </div>

        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.dropdown-toggle').dropdown();
            });
        </script>
    </body>
</html>