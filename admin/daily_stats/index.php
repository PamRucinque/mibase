<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../css/mibase.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <?php include( dirname(__FILE__) . '/../menu.php'); ?>
            <div class="row">
                <div class="col-sm-9">
                    <h2>Stats Menu</h2><br><br>

                </div>
                <div class="col-sm-2">
                    <br><a href='../home/index.php' class ='btn btn-colour-yellow'>Back to Home</a><br>
                </div>
                <div class="col-sm-1">
                    <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=select_boxes' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                </div>
            </div>
            <div class="row">


                <div class="col-sm-3">
                    <h4>Daily Stats</h4>
                    <br><a style="width: 150px;" href='summary.php' class ='btn btn-success'>Member Activity</a><br>
                    <br><a style="width: 150px;" href='../home/charts/charts.php' class ='btn btn-success'>charts</a><br>

                </div>
            </div>
        </div>
    </body>
</html>
