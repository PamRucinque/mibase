<?php

/* 
 * Copyright (C) 2018 Mibase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//the database host and port
$dbhost = 'localhost';
$dbport = '5432'; 

//the protocol for the web server either http or https
$web_server_protocol = 'http';

//--------------------
//lOCATIONS IN THE WEB SERVER

// application root location on web server
$app_root_location = '/mibase';

// toy images root location on the web server
$toy_images_location = '/toy_images';

// news root location on the web server
$news_location = '/news';

// logos root location on the web server
$logos_location='/logos';

// the location of the libraries public mibase web site
//this is not the www.library.com web site but a web site that shows the toy catelog, library details
$home_location = '/home';
//--------------------

// URL to MibaseReportRunner Server
$report_server_url = 'tcp://localhost:11000';

//the filing system path of the web server root folder
//this setting is only needed for file operations ( eg uploading toy images and news files )
$web_root_folder = 'C:/Apache24/htdocs';

//password to access the database administration section of the application
//this password should be changed and needs to be a strong password.
//use a strong password !
$db_admin_password = 'CHANGE ME';

//--------------------
//Shared or Single System
//if this is a shared server that manages multiple toy libraries then set this value true
//for a non shared environment, ie one library on one server set this to false
$shared_server=false;

//for a shared server specify the domain, 
//ie a libraries url is $web_server_protocol://library_code.$shared_domain$app_root_location/login.php eg http://demo.mibase.org/mibase/login.php
$shared_domain='mibase.org';

// the postgres user password (postgres user is superuser)
//if this value is set then $dbuser $dbpasswd $toybasedbuser $toybasedbpasswd below are ignored
$db_postgres_password = 'PASSWORD';


//--------------------
//ADVANCED DATABASE SETTINGS
//Use these if for example your database server is serving other applications 
// SINGLE SERVER
// these settings point at the toy libries database
#$dbname = 'mibase';
//if $db_postgres_password is set then $dbuser $dbpasswd are ignored and the postgres user and password is used to access the database
#$dbuser = 'mibase';
#$dbpasswd = 'PASSWORD';

//SHARED SERVER
//for a shared system these variables point at the toybase database
//if this variable is not set then the default name toybase is used
#$toybasedbname = 'toybase';
//if $db_postgres_password is set then $toybasedbuser $toybasedbpasswd are ignored and the postgres user and password is used to access the toybase database
#$toybasedbuser = 'toybase';
#$toybasedbpasswd = 'PASSWORD';
//--------------------


//--------------------
// Member log database
//seperate database for collecting online membeships
$logdbname = 'member_log';
$logdbuser = 'member_log';
$logdbpasswd = 'PASSWORD';


//--------------------
//Mibase Development Settings 
//development mode of mibase feautures that are not ready yet, please set to No
$mibase_server = 'No';
