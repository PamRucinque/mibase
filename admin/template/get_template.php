<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');



$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$sql = "SELECT * from template WHERE type_template = ?";

$sth = $pdo->prepare($sql);
if (isset($_GET['t'])){
    $toyid = $_GET['t'];
}
else{
    $toyid = 0;
}
$array = array($toyid);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = $result[$ri];
    $id = $row['id'];
    $subject = $row['subject'];
    $message = $row['message'];
    $type_template = $row['type_template'];
    $template_email = $row['template_email'];
}

if ($stherr[0] != '00000') {
    $_SESSION['login_status'] .= "An  error occurred.\n";
    $_SESSION['login_status'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['login_status'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['login_status'] .= 'Error' . $stherr[2] . '<br>';
}


$status_txt = Null;

