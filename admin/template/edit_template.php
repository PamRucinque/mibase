<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>    
    <body id="main_body" >
        <div id="form_container"  style="background-color: #E0E0E0 ;">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            include( dirname(__FILE__) . '/get_template.php');
            if (isset($_POST['message'])) {
                $message_txt = clean($_POST['message']);
            } else {
                $message_txt = '';
            }

            if (isset($_POST['subject'])) {
                $subject_txt = clean($_POST['subject']);
            } else {
                $subject_txt = '';
            }

            if (isset($_POST['email'])) {
                $email_txt = clean($_POST['email']);
            } else {
                $email_txt = '';
            }

            //$subject_txt = clean($_POST['subject']);
            //$email_txt = clean($_POST['email']);
            //include( dirname(__FILE__) . '/toy_detail.php');

            if (isset($_POST['submit'])) {

                //include( dirname(__FILE__) . '/../connect.php');
                $query = "UPDATE template SET
                    message = '{$message_txt}',
                    subject= '{$subject_txt}',
                    template_email = '{$email_txt}'    
                    WHERE id=" . $_POST['id'] . ";";

                    $conn = pg_connect($_SESSION['connect_str']);   
                    $result = pg_Exec($conn, $query);
                //echo $query;
                //echo $connection_str;
                if (!$result) {
                    echo "An EDIT query error occurred.\n";
                    echo $query;
                    //exit;
                } else {

                    // echo 'saved';
                    echo "<br>The record was successfully saved. " . "<a class='button1_red' href='template.php'>OK</a>" . "<br><br>";
                }

                pg_FreeResult($result);
// Close the connection
                pg_Close($conn);
            } else {
                include( dirname(__FILE__) . '/get_template.php');
                include( dirname(__FILE__) . '/edit_form_template.php');
            }
            ?>
        </div>
    </body>
</html>

<?php

function clean($input) {
    $output = stripslashes($input);
    $output = str_replace("'", "`", $output);
    return $output;
}
?>