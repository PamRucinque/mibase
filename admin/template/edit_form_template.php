<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons | fontsizeselect",
        fontsize_formats: "8px 10px 11px 12px 14px 18px 24px 36px",
        image_advtab: true,
        relative_urls: false,
        remove_script_host: false
    });
</script>


<?php
echo "<br><a href='template.php' class ='button1_red'>Back to Templates</a>";
?>
<form action="edit_template.php" method="post" name="form1" enctype="multipart/form-data">

    <br><label>Subject: </label><br>
    <input type="Text" name="subject" align="LEFT" required="Yes" size="55" value="<?php echo $subject; ?>" STYLE="background-color: #FFFF99;"></input>
    <br><br><label>From Email (leave blank if the email address from settings): </label><br>
    <input type="Text" name="email" align="LEFT" size="55" value="<?php echo $template_email; ?>" STYLE="background-color: #FFFF99;"></input>

    <br><br><label>Message: </label><br>
    <?php
    if ((trim($type_template) == 'roster_sms') || (trim($type_template) == 'roster_reminder_sms')) {

        echo '<input type="Text" name="message" id="message" align="LEFT" size="90" value="';
        echo $message;
        echo '" STYLE="background-color: #FFFF99;"></input>';
    } else {

        echo '<textarea id="message" name="message" rows="25" cols="35" STYLE="font-size: large; font-family: sans-serif">';
        echo $message;
        echo '</textarea><br>';
    }
    ?>
    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"> 
    <input class="button1_red"  type="submit" name="submit" id="submit" value="Save"/>

</form>
