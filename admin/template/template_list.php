<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');

$conn = pg_connect($_SESSION['connect_str']);
$query = "SELECT * from template ORDER by subject";
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);


if ($numrows > 0) {
echo '<h2> Templates: </h2>';
echo '<table border="1" width="90%" style="border-collapse:collapse; border-color:grey">';
echo '<tr style="color:green"><td>id</td><td>Type</td><td>Delivery type</td><td>Email</td><td>Subject</td><td>Email</td></tr><tr>';

}

for ($ri = 0; $ri < $numrows;$ri++) {
//echo "<tr>\n";
$row = pg_fetch_array($result, $ri);

$message_txt = substr($row['message'],0,500);
echo '<td width="30px" align="center">' . $row['id'] . '</td>';
echo '<td width="20px">' . $row['type_template'] . '</td>';
echo '<td width="20px">' . $row['delivery_type'] . '</td>';
echo '<td width="20px">' . $row['template_email'] . '</td>';
echo '<td width="250px">' . $row['subject'] . '</td>';

//echo '<td width="350px" align="left">' . $message_txt . '</td>';
$ref3 = 'edit_template.php?id=' . $row['id'] . '&t=' . $row['type_template'];
$ref4 = 'preview.php?id=' . $row['id'] . '&t=' . $row['type_template'];
echo "<td width='50px' align='center'><a class ='button1_yellow' href='" . $ref3 . "'>Edit</a>";
//echo "<a class ='button1_yellow' href='" . $ref4 . "'>Preview</a>";
echo "</td>";
echo '</tr>';

}
echo '<tr height="0"></tr>';
echo '</table>';



pg_close($conn);
//include( dirname(__FILE__) . '/new_category.php');
?>

</body>


