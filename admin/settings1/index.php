<!DOCTYPE html>
<?php include('../mibase_ck.php'); ?>
<html>
    <head>
        <title>Melbourne Community Toy Library - Self Serve</title>
        <?php include('../header/head.php'); ?>
    </head>


    <body onload="on_load();">
        <div id="content" style='display: block; '>


            <?php
            include('../header/menu.php');
            ?>
            <div class="container-fluid" style="padding: 20px;">
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        if (!session_id()) {
                            session_start();
                        }
                        $s = array();
                        include('../functions/settings.php');
                        include('../connect/connect_local.php');
                        $_SESSION['settings'] = fillSettingSessionVariables($connection_str);
                        $s = $_SESSION['settings'];
                        include('check_settings.php');
                        //print_r($s);
                        ?>

                    </div>
                </div>


            </div>
    </body>

</html>



