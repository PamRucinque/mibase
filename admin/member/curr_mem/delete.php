<?php

    //Check for a valid session:
    require( dirname(__FILE__) . '/../../mibase_check_login.php');

    //Confirm that the 'id' variable has been set:
    if (isset($_POST['rt_id']) && is_numeric($_POST['rt_id'])) {
        
        //Get the 'id' variable from the URL:
        $conn = pg_connect($_SESSION['connect_str']);
        $query = "DELETE FROM borwrs WHERE id = " . $_POST['rt_id'] . ";";
        $result = pg_exec($conn, $query);

        //Check result:
        if (!$result) {
            $message = 'Invalid query: ' . "\n";
            $message = 'Whole query: ' . $query;
            echo $message;
        } else {
            echo $query;
        }

        //Redirect the user:
        $redirect = "Location: index.php";               //print $redirect;
        header($redirect);
        
    } 

?>