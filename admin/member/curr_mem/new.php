<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">
    </head>
    <body onload="setFocus()">
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');
            $button_str = 'OK';
            //$java_str = 'Close';
            $java_str = "$(location).attr('href', 'index.php')";
            $str_alert = '';

            if (isset($_POST['submit'])) {
                include('../functions.php');
                $newid = get_id('rostertypes');

                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $query = "INSERT INTO rostertypes "
                        . "(id, rostertype, description, weekday, location, nohours, volunteers) "
                        . "VALUES (?,?,?,?,?,?,?) returning id;";
//and get the statment object back
                $sth = $pdo->prepare($query);

                $array = array($newid, $_POST['rt'], $_POST['description'],
                    $_POST['weekday'], $_POST['location'], $_POST['nohours'], $_POST['volunteers']);

//execute the preparedstament
                $sth->execute($array);
                $stherr = $sth->errorInfo();
                $prefid = $sth->fetchColumn();

                if ($stherr[0] != '00000') {
                    echo "An insert error occurred.\n";
                    echo 'Error ' . $stherr[0] . '<br>';
                    echo 'Error ' . $stherr[1] . '<br>';
                    echo 'Error ' . $stherr[2] . '<br>';
                    echo '<a class="btn btn-danger" href="new.php?rt_id=' . $_POST['id'] . '">Back to New</a>';
                    exit;
                }
                //window . location . reload();
                $redirect = "Location: ../update/new_rt.php";               //print $redirect;
                //header($redirect);
                $str_alert = '<br><br>Thank you your Roster preference has been saved. Reference number: ' . $prefid . '.<br>';
                //$str_alert .= '<br><a class="btn btn-info" href="index.php">Back to Preferences</a><br><br>';
            //
            } else {
                include('new_form.php');
            }
            ?>
        </div>
        <?php include ('msg_form.php'); ?>
        <script type="text/javascript" src="../../js/jquery-3.0.0.js" charset="UTF-8"></script>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

        <script type="text/javascript">
        $('.form_datetime').datetimepicker({
            //language:  'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
        $('.form_date').datetimepicker({
            language: 'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
        $('.form_time').datetimepicker({
            language: 'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0
        });
        </script>
    </body>
</html>