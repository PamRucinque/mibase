<?php

    //Check for a valid session:
    require(dirname(__FILE__) . '/../../mibase_check_login.php');

    //Create delete and edit vars:
    $ref_delete = '';
    $ref_edit = '';

    //Create and run the query:
    $query = "SELECT * from borwrs WHERE member_status='ACTIVE' ORDER by id";
    $conn = pg_connect($_SESSION['connect_str']);
    $result = pg_exec($conn, $query);
    $numrows = pg_numrows($result);
    include('heading.php');

    //For the number of results returned:
    for ($ri = 0; $ri < $numrows; $ri++) {

        $members = pg_fetch_array($result, $ri);
        $ref_delete = '<form action="delete.php" method="POST"><input type="hidden" name="rt_id" id="rt_id" value="' . $members['id'] . '">';
        $ref_delete .= '<input id="submit" name="submit" class="btn btn-danger btn-sm"  type="submit" value="Delete" /></form>';
        $ref_edit = '<form action="edit.php" id="edit" name="edit" method="POST"><input type="hidden" name="rt_id" id="rt_id" value="' . $members['id'] . '">';
        $ref_edit .= '<input id="submit_nav" name="submit_nav" class="btn btn-primary btn-sm"  type="submit" value="Edit" /></form>';
        include('row.php');
    }

?>