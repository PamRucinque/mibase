<?php
    //Validate the session:
    require(dirname(__FILE__) . '/../../mibase_check_login.php');
    //Get the date:
    $today = date('Y-m-d');
    //$oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " + 365 day"));

?>

<body id="main_body">
    <script type="text/javascript" src="../../js/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
    <link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

    <script type="text/javascript">
        $(function () {
            var pickerOpts = {
                dateFormat: "d MM yy",
                showOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "2010:+nn"

            };
            $("#expired").datepicker(pickerOpts);
            $("#joined").datepicker(pickerOpts);
            $("#renewed").datepicker(pickerOpts);
        });
        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault)
                    theEvent.preventDefault();
            }
        }
        function get_postcode(str) {
            var input = str;

            if (str == "") {
                document.getElementById("postcode").innerHTML = "";
                return;
            }

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {

                document.getElementById("postcode").value = xmlhttp.responseText;

            }

            xmlhttp.open("GET", "getpostcode.php?q=" + str, true);
            xmlhttp.send();
        }
        function get_help(str) {
            var input = str;

            if (str == "") {
                document.getElementById("help_txt").innerHTML = "";
                return;
            } else {
                $str = document.getElementById("help_txt").value;
                //alert($str);
            }


            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                //var length = 100;
                //$str = trimFunction(100, $str);
                if ($str.length < 100) {
                    if ($str.length == 0) {
                        document.getElementById("help_txt").value = xmlhttp.responseText + $str;
                    } else {
                        document.getElementById("help_txt").value = xmlhttp.responseText + "," + $str;
                    }

                } else {
                    document.getElementById("help_txt").value = xmlhttp.responseText;
                }
            }

            xmlhttp.open("GET", "data/gethelp.php?q=" + str, true);
            xmlhttp.send();
        }
        function ClearFields() {

            document.getElementById("help_txt").value = "";
        }

    </script>
    <?php

        //include( dirname(__FILE__) . '/../../get_settings.php');
        // if ($suburb_title == '') {
        //     $suburb_title = 'Suburb';
        // }
        // if ($city_title == '') {
        //     $city_title = 'City';
        // }

    ?>

    <section class="container-fluid" style="padding: 10px;">
        <div class="row">
            <div class="col-sm-8">
                <h2>New Member</h2>
            </div>
            <div class="col-sm-2">
                <br><a href='index.php' class ='btn btn-info'>Back to Active Member List</a><br>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-1">
                <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=roster_preferences' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
            </div>
        </div>
    </section>

    <section class="container-fluid" style="padding: 10px;">
        <form action="new.php" method="post">
            <div class="row" style="background-color:whitesmoke;">
                <div class="col-sm-4"  id="contact">
                    <h3>Contact 1</h3>
                    <label for="firstname">Firstname:</label>
                    <input type="text" class="form-control" id="firstname" placeholder="" name="firstname" value="">
                    <label for="surname">Lastname:</label>
                    <input type="text" class="form-control" id="surname" placeholder="" name="surname" value="">
                    <label for="emailaddress">Email:</label>
                    <input type="text" class="form-control" id="emailaddress" placeholder="" name="emailaddress" value="">
                    <h3>Contact 2</h3>
                    <label for="partnersname">Firstname:</label>
                    <input type="text" class="form-control" id="partnersname" placeholder="" name="partnersname" value="">
                    <label for="partnerssurname">Lastname:</label>
                    <input type="text" class="form-control" id="partnerssurname" placeholder="" name="partnerssurname" value="">
                    <label for="mobile2">Mobile:</label>
                    <input type="text" class="form-control" id="mobile2" placeholder="" name="mobile2" value="">
                    <label for="email2">Email:</label>
                    <input type="text" class="form-control" id="email2" placeholder="" name="email2" value="">
                    <h3>General Info</h3>
                    <label for="address">Address:</label>
                    <input type="text" class="form-control" id="address" placeholder="" name="address" value="">
                    <label for="city">City:</label>
                    <input type="text" class="form-control" id="city" placeholder="" name="city" value="">
                    <label for="postcode">Postcode:</label>
                    <input type="text" class="form-control" id="postcode" placeholder="" name="postcode" value="">
                    <label for="state">State:</label>
                    <input type="text" class="form-control" id="state" placeholder="" name="state" value="">
                    <label for="memtype">Member Type: :</label>
                    <input type="text" class="form-control" id="memtype" placeholder="" name="memtype" value="">
                    <label for="source">Source:</label>
                    <input type="text" class="form-control" id="source" placeholder="" name="source" value="">
                    <label for="lote">Language other than English:</label>
                    <input type="text" class="form-control" id="lote" placeholder="" name="lote" value="">
                    <label for="notes">Notes:</label>
                    <textarea id="notes" name="notes" rows="3" cols="35"></textarea>
                    <label for="alertmem">Alert Member:</label>
                    <textarea id="alertmem" name="alertmem" rows="3" cols="35"></textarea>
                    <label for="skills">Skills/Occupation:</label>
                    <textarea id="skills" name="skills" rows="3" cols="35"></textarea>
                    <label for="skills">Skills/Occupation:</label>
                    <textarea id="skills" name="skills" rows="3" cols="35"></textarea>
                    <label for="help_txt">How can you help:</label>
                    <textarea id="help_txt" name="help_txt" rows="5" cols="60" maxlength="100" readonly></textarea>
                    <button type="button" >Clear</button>
                </div>
                <div class="col-sm-8"></div>
            </div>
            <div class="row" style="background-color:lightgoldenrodyellow;">
                <div class="col-sm-6"  id="submit_header" style="min-height:140px;padding-right:0px;padding-left:20px;padding-top: 5px;">
                    <br><input type=submit id="submit" name="submit" class="btn btn-success" value="Save New Preference"> <br>
                </div>
                <div class="col-sm-6"  id="submit_header" style="min-height:140px;padding-right:0px;padding-left:20px;padding-top: 5px;">
                    <br><input type=button onclick="ClearFields();" class="btn btn-danger" value="Clear"><br>
                </div>
            </div>
        </form>
    </section>
</body>




