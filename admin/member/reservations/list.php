<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../../mibase_check_login.php');

include('../../connect.php');

if (isset($_SESSION['borid'])) {
    $memberid = $_SESSION['borid'];
} else {
    $memberid = 0;
}
$str_loan_type = '';

$sql = "SELECT reserve_toy.*, t.toyname, to_char(date_start, 'dd/mm/yyyy') as start,
to_char(date_end, 'dd/mm/yyyy') as end 
from reserve_toy 
left join toys t on t.idcat = reserve_toy.idcat
 WHERE member_id = ?  ORDER BY date_start desc, idcat;";

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$sth = $pdo->prepare($sql);
$array = array($borid);
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $_SESSION['error'] = "An  error occurred.\n";
    $_SESSION['error'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[2] . '<br>';
}
$numrows = $sth->rowCount();
if ($numrows > 0){
    include('heading.php');
}

for ($ri = 0; $ri < $numrows; $ri++) {
    $reserve = $result[$ri];

    $link = '../../toys/update/toy_detail.php?idcat=' . $reserve['idcat'];
    $onclick = 'javascript:location.href="' . $link . '"';

    if ($reserve['paid'] == 'Yes') {
        $paid_str = 'Paid';
    } else {
        $paid_str = '';
        
    }
    if ($reserve['approved']){
        $str_approve = 'Yes';
    }else{
        $str_approve = '';
    }
    include('row_bg_color.php');
    include('row.php');
}


