<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>
    <body>
        <div id="form_container">
            <?php include( dirname(__FILE__) . '/../menu.php'); ?>
            <section class="container-fluid" style="padding: 10px;">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Members Menu</h2><br><br>

                    </div>
                    <div class="col-sm-5">
                        <br><a href='../home/index.php' class ='btn btn-colour-yellow'>Home</a> 
                        <a href='../toys/index.php' class ='btn btn-primary'>Toys</a> 
                        <a href='../members/index.php' class ='btn btn-colour-maroon'>Members</a> 
                        <a href='../select_boxes/index.php' class ='btn btn-success'>Select Boxes</a>
                    </div>
                    <div class="col-sm-1">
                        <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=toysa' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <h4>Member Lists</h4>
                        <br><a style="width: 150px;" href='curr_mem/index.php' class ='btn btn-colour-maroon'>Current Members</a><br>
                        <br><a style="width: 150px;" href='../members/expired_members.php' class ='btn btn-colour-maroon'>Expired Members</a><br>
                        <br><a style="width: 150px;" href='../members/members_resigned.php' class ='btn btn-colour-maroon'>Resigned Members</a><br>
                        <br><a style="width: 150px;" href='../members/members_locked.php' class ='btn btn-colour-maroon'>Locked Members</a><br>
                        <br><a style="width: 150px;" href='../emails/index.php' class ='btn btn-colour-maroon'>Bulk Emails</a><br>

                    </div>
                    <div class="col-sm-3">
                        <h4>Add a Member</h4>
                        <br><a style="width: 150px;" href='../members/update/new.php' class ='btn btn-colour-maroon'>New Member</a><br>
                        <br><a style="width: 150px;" href='../approve/members_approve.php' class ='btn btn-colour-maroon'>Approve a Member</a><br>
                        <br><a style="width: 150px;" href='../approve/gift_approve.php' class ='btn btn-colour-maroon'>Gift Card Member</a><br>
                    </div>

                    <div class="col-sm-3">
                        <h4>Duty Roster</h4>
                        <br><a style="width: 150px;" href='../roster/roster.php' class ='btn btn-colour-maroon'>Roster</a><br>
                        <br><a style="width: 150px;" href='../roster/new_roster.php' class ='btn btn-colour-maroon'>New Roster Item</a><br>
                        <br><a style="width: 150px;" href='../roster/generate_roster.php' class ='btn btn-colour-maroon'>Generate a Roster</a><br>
                        <br><a style="width: 150px;" href='../select_boxes/roster_pref/index.php' class ='btn btn-colour-maroon'>Roster Preferences</a><br>

                    </div>
                    <div class="col-sm-3">
                        <h4>Select Boxes</h4>
                        <br><a style="width: 150px;" href='../members/membertypes/membertypes.php' class ='btn btn-colour-maroon'>Member Categories</a><br>
                        <br><a style="width: 150px;" href='../select_boxes/city/index.php' class ='btn btn-colour-maroon'>Cities</a><br>
                        <br><a style="width: 150px;" href='../select_boxes/suburb/index.php' class ='btn btn-colour-maroon'>Suburbs</a><br>
                        <br><a style="width: 150px;" href='../members/update/new_discovery.php' class ='btn btn-colour-maroon'>How did you find Us?</a><br>
                        <br><a style="width: 150px;" href='../members/update/nationality/new_nationality.php' class ='btn btn-colour-maroon'>Language</a><br>
                     </div>
                </div>
            </section>
        </div>
        <script type="text/javascript" src="../js/jquery-3.0.0.js" charset="UTF-8"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    </body>
</html>