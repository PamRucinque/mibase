<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

if (isset($_POST['reset_2'])) {
    //echo 'Hello Gregor reset2';
    $_SESSION['idcat_select'] = '';
    $_SESSION['search'] = '';
    $_SESSION['category'] = '';
    $_SESSION['age'] = '';
    $_SESSION['loc_filter'] = '';
}
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>    
    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');

            $total = 0;
            ?>
            <br><h3>Locked Toys</h3><br>
            <table width=100% height="60px"  bgcolor="lightgray">
                <tr><td width="30%">
                        <form name="search_form" id="search_form" method="post" action="locked.php" > 
                            <table width=100%>        
                                <tr>
                                    <td width="40%" colspan="3"><label class="description" for="search">Search String</label><input id="search" name="search" type="text" maxlength="255" size="30px" value=""  onchange="this.form.submit();"/> </td>
                                    <td valign="bottom" ><input type="hidden" name="total" value="<?php echo $total; ?>">
                                        <input type="hidden" name="select_list" id="select_list" value=""/>

                                    </td>
                                </tr>
                                <tr>
                                    <td><label class="description" for="element_12">Category: </label><?php include( dirname(__FILE__) . '/get_category.php'); ?></td>
                                    <td colspan="2"> <?php
                                        echo 'Age: <br>';
                                        include( dirname(__FILE__) . '/get_age.php');
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </form>


                    </td>

                    <td width="5%"  valign="bottom" ><br>

                        <form name="form_reset" id="form_reset" method="post" action="locked.php" > 
                            <input type="submit" class="button1_logout" name ="reset_2" id ="reset_2" value="Reset"/> 
                        </form>

                    </td>

                </tr></table>


            <?php
            //if (isset($_POST['submit'])) {
            //include( dirname(__FILE__) . '/../../get_settings.php');
            include( dirname(__FILE__) . '/result_locked.php');
            // } else {
            //    $location = 'ALL';
            //    $assessor = '';
            //}
            //print 'hello'; }
            ?>
        </div>

    </body>

</html>