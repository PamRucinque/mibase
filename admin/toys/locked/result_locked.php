<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//get configuration data
//include(__DIR__ . '/../../config.php');
?>
<style type="text/css">
    tr:hover { 
        color: red; }
</style>

<?php

$catsort = $_SESSION['settings']['catsort'];

if (isset($_POST['location'])) {
    $password = $_POST['password'];
}
$search = "";
if (isset($_POST['search'])) {
    $search = $_POST['search'];
}
if (isset($_POST['category'])) {
    $_SESSION['category'] = $_POST['category'];
}
$category = $_SESSION['category'];

if (isset($_POST['age'])) {
    $_SESSION['age'] = $_POST['age'];
}
$age = $_SESSION['age'];
//
//"OR supplier LIKE '%" . $search . "%' " .

$search = strtoupper($search);

//error message (not found message)
if ($catsort == 'No') {
    $order_by = 'ORDER BY id ASC;';
} else {
    $order_by = 'ORDER BY category, id ASC;';
}

$query = "SELECT *
FROM toys
WHERE (upper(toyname) LIKE '%" . $search . "%' " .
        "OR upper(manufacturer) LIKE '%" . $search . "%' " .
        "OR upper(idcat) LIKE '%" . $search . "%' " .
        "OR upper(supplier) LIKE '%" . $search . "%' " .
        ") AND (category  LIKE '%{$category}%')
AND ((age  LIKE '%{$age}%') OR (age IS NULL)) AND (toy_status != 'ACTIVE') AND (toy_status != 'WITHDRAWN')
" . $order_by;

//echo $query;
//$query = "SELECT * FROM toys ORDER by id ASC;";


$XX = "No Record Found";
//print $query;
//$query = "SELECT * FROM hm_claims order by id";
$total = 0;
//include( dirname(__FILE__) . '/../../connect.php');
$conn = pg_connect($_SESSION['connect_str']);
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);
//echo $query;
//$result_txt =  '<table border="1"><tr>';
$result_txt = '';


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);

    $format_date_purchase = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);

    $id = $row["id"];
    $idcat = $row["idcat"];
    $category = $row["category"];
    $toyname = $row["toyname"];
    $age = $row["age"];
    $desc1 = $row["desc1"];
    $comments = $row["comments"];
    $toy_status = $row["toy_status"];
    $cost = $row['cost'];
    $no_pieces = $row['no_pieces'];
    $status = $row["status"];
    $supplier = $row['supplier'];
    $manufacturer = $row['manufacturer'];
    $date_purchase = $row["date_purchase"];
    $file_pic = '../../../..' . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $total = $total + 1;
        $result_txt .= '<tr border="1"><td border="1" width="50">' . $id . '</td>';
        $result_txt .= '<td width="50" align="left">' . $idcat . '</td>';
        $result_txt .= '<td width="50">' . $category . '</td>';


        $result_txt .= '<td width="80" align="center">' . $row['toy_status'] . '</td>';

        $result_txt .= '<td width="200" align="left">' . $toyname . '</td>';

        if ($_SESSION['shared_server']) {
            $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
            if (file_exists($file_pic)) {
                $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
            } else {
                $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
            }
        } else {
            $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
            if (file_exists($file_pic)) {
                $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
            } else {
                $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
            }
        }
        $result_txt .= '<td width="50" align="center"><a href="../update/toy_detail.php?idcat=' . $idcat . '" onmouseover="showtrail(175,220, \'' . $pic . '\');" onmouseout="hidetrail();" class="button_small"/>View</a>';

        //$result_txt .= '<td width="130" align="left">'. $supplier . '</td>';
        $result_txt .= '<td width="90" align="left">' . $manufacturer . '</td>';
        //$result_txt .= '<td width="90" align="left">' . $file_pic . '</td>';
        $result_txt .= '<td width="60">' . $age . '</td>';
        $result_txt .= '<td width="70">' . $format_date_purchase . '</td>';
        $result_txt .= '<td width="30">' . $no_pieces . '</td>';
        //$result_txt .= '<td width="50"><a href="toy_detail.php?idcat=' . $idcat . '">View</a></td>';

        $result_txt .= '</tr>';


        //print "<td width='50'><a href='delete_claim.php?id=" . $row['id'] . "'>Delete</a></td></tr>";
    }
}
$result_txt .= '</tr></table>';

//print '<br/>' . $query;
//print '<br/>' . $location;
//below this is the function for no record!!
//end

print '<div id="open"><table width="100%"><tr><td width= 50%><h1 align="left"></h1></td><td><h1 align="right">Total: ' . $total . '</h1></td><tr></table></div>';
print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
print '<tr style="color:green"><td>id</td><td>toy#</td><td>Category</td><td>Status</td><td>Name</td><td>Picture</td><td>Manufacturer</td><td>Age</td><td>Purchased</td><td>#pieces</td></tr>';

print $result_txt;
?>