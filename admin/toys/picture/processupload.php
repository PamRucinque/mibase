<?php

if (isset($_FILES["FileInput"]) && $_FILES["FileInput"]["error"] == UPLOAD_ERR_OK) {
    ############ Edit settings ##############
    $dir = '/home/lims/html/lims_docs/ref/';
    ##########################################

    /*
      Note : You will run into errors or blank page if "memory_limit" or "upload_max_filesize" is set to low in "php.ini".
      Open "php.ini" file, and search for "memory_limit" or "upload_max_filesize" limit
      and set them adequately, also check "post_max_size".
     */

    //check if this is an ajax request
    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
        die("ERROR: Must be called with Ajax!");
    }


    //Is file size is less than allowed size.
    if ($_FILES["FileInput"]["size"] > 3097152) {
        die("ERROR: File size is too big!");
    }
    if ($idcat == '') {
        die("ERROR: Please select a toy first!");
    }


    $File_Name = strtolower($_FILES['FileInput']['name']);
    $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //get file extention
   // $Random_Number = rand(0, 9999999999); //Random number to be added to name.
    //$NewFileName = $Random_Number . $File_Ext; //new file name
    $NewFileName = 'test.jpg';
    
    
    //delete an existing file if it exists
    if (file_exists($dir . '/' . $File_Name)) {
        unlink($dir . '/' . $File_Name);
    }
    
    
    
    if (move_uploaded_file($_FILES['FileInput']['tmp_name'], $dir . '/' . $NewFileName)) {
        //$cmd = '/usr/bin/java -Xmx50m -jar /home/marineleisure/bin/ImageSizerMLA.jar "' . $UploadDirectory . $NewFileName . '" 2>&1';
        //$cmd = '/usr/bin/java -Xmx50m -jar /home/mla/bin/ImageSizerMLA.jar "' . $UploadDirectory . $NewFileName . '" 2>&1';
        //system($cmd);

        die('Success! File Uploaded.</br>');
     //   $x = RotateImage($UploadDirectory, $NewFileName);
    } else {
        die('error uploading File!');
    }
 
} else {
    die('Something wrong with upload! Is "upload_max_filesize" set correctly?');
}

