<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');


//get configuration data
//include(__DIR__ . '/../../config.php');

if (isset($_GET['idcat'])) {
    $_SESSION['idcat'] = $_GET['idcat'];
}

$due = '';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Mibase Toy Picture Uploader</title>

        <?php
        include('../../header.php');
        ?>
    </head>

    <body>
        <div id="form_container">
            <?php
            include('../../menu.php');
            //include('../../connect.php');
            include('find/get_toy.php');
            include('../../header_detail/header_detail.php');
            include('find/find_toy.php');
            //echo '<center><h1><font color="red">UNDER CONSTRUCTION please do not use</font></h1></center>';

            echo '<br><center><h3><font color="red">WARNING: Please do not use this uploader if you have a low data usage plan!</font></h3></center>';
            echo '<center><h3><font color="blue">Keep upload size to less than 1.5MB</font></h3></center>';

            $idcat = $_SESSION['idcat'];
            $UploadDirectory = $_SESSION['web_root_folder']  .$_SESSION['toy_images_location'];
            //echo $UploadDirectory;

            if ($_SESSION['shared_server']) {
                $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
                if (file_exists($file_pic)) {
                    $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
                } else {
                    $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
                }
            } else {
                $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
                if (file_exists($file_pic)) {
                    $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
                } else {
                    $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
                }
            }


            $pic_img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';

            // echo $img;
            ?>
            <table width="100%">
                <tr>
                    <td>
                        <?php include('form_upload.php'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>