<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');


//get configuration data
//include(__DIR__ . '/../../config.php');
?>
<style type="text/css">
    tr:hover { 
        color: red; }
</style>
<?php



$overdue = 0;
if (isset($_POST['submit'])) {
    $_SESSION['search'] = $_POST['search'];
} else {
    $_POST['search'] = $_SESSION['search'];
}


$search = "";
if (isset($_POST['search'])) {
    $search = strtoupper($_POST['search']);
}

//error message (not found message)

$total = 0;
//include( dirname(__FILE__) . '/../connect.php');



$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$vol_no_toy_edits = $_SESSION['settings']['vol_no_toy_edits'];

$query_toys = "SELECT toys.*,
        transaction.borname as borname, 
        transaction.id as transid, 
        transaction.return, 
        transaction.borid as borid,
        transaction.due as due, 
        transaction.return as return,
        transaction.date_loan,
        transaction.item as item
FROM
     transaction 
     LEFT JOIN toys ON upper(transaction.idcat) = upper(toys.idcat)
     WHERE (((transaction.return) Is Not Null) AND (upper(transaction.idcat) = '" . strtoupper($_SESSION['idcat']) . "')
     AND (
     upper(toyname) LIKE ?
     OR upper(borname) LIKE ?
     OR upper(toys.idcat) LIKE ?
     )
     
) order by date_loan desc;";



//echo $query_toys;


$search = strtoupper($search);
$search_str = '%' . $search . '%';

//echo $search_str;

$sth = $pdo->prepare($query_toys);
//$array = array($search_str, $search_str, $search_str, $search_str,$search_str, $category_str, $age_str);
//$array = array($search_str, $search_str, $search_str, $search_str, $search_str, $category_str, $age_str);
$array = array($search_str, $search_str, $search_str);
$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

//$query = "SELECT * FROM toys ORDER by id ASC;";


$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");
$days_out = 0;

if (isset($_GET['r'])) {
    $toprint = sizeof($result);
} else {
    $toprint = 100;
}
$total = sizeof($result);
if (sizeof($result) < 100) {
    $toprint = sizeof($result);
    $toprint_txt = " ";
} else {
    $link = '<a class="button_small" href="toys.php?r=' . $toprint . '">Showall</a>';
    $toprint_txt = $toprint . ' of ' . $total . " shown. " . $link;
}

for ($ri = 0; $ri < $toprint; $ri++) {
    //$row = pg_fetch_array($result, $ri);
    $days_out = $days_out + $overdue;
    $row = $result[$ri];
    $category = $row["category"];
    $toyname = $row["toyname"];
    $age = $row["age"];
    $desc1 = $row["desc1"];
    $comments = $row["comments"];
    $toy_status = $row["toy_status"];
    $cost = $row['cost'];
    $no_pieces = $row['no_pieces'];
    $status = $row["status"];
    $supplier = $row['supplier'];
    $manufacturer = $row['manufacturer'];
    $date_purchase = $row["date_purchase"];
    $borname = $row['borname'];
    $due = $row['due'];
    $trans_borid = $row['borid'];
    $return = $row['return'];
    $item = $row['item'];
    $ref1 = '<td><form action="" method="POST"><input type="hidden" name="id" id="id" value="' . $row['transid'] . '">';
    $ref1 .= '<input id="delete_row" name="delete_row" class="button_small_red"  type="submit" value="Delete" /></form></td>';


    $ts1 = strtotime($row['date_loan']);
    $ts2 = strtotime($row['return']);

    $overdue = ($ts2 - $ts1) / (-60 * 60 * 24);
    $overdue = round($overdue * -1, 0);

    $due = $row['due'];

    //$total = $total + 1;
    $format_loan = substr($row['date_loan'], 8, 2) . '-' . substr($row['date_loan'], 5, 2) . '-' . substr($row['date_loan'], 0, 4);
    $format_return = substr($row['return'], 8, 2) . '-' . substr($row['return'], 5, 2) . '-' . substr($row['return'], 0, 4);

    $id = $row["transid"];
    $idcat = $row["idcat"];

    $result_txt .= '<tr border="1" class="item"><td border="1" width="50">' . $id . '</td>';
    //$result_txt .= '<tr border="1"><td border="1" width="50">' . $id . '</td>';
    //$result_txt .= '<td width="50" align="left">' . $idcat . '</td>';
    $result_txt .= '<td class="idcat" width="50" align="center">' . $idcat . '</td>';




    if ($_SESSION['shared_server']) {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
        }
    } else {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
        }
    }
    //$pic = "'../../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . ".jpg'";
    $img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';
    $loan_status = '<td BGCOLOR=lightgreen width="80" align="center">ON LOAN</td>';
    if ($row['due'] < $today) {
        $loan_status = '<td BGCOLOR=pink width="80" align="center">OVERDUE</td>';
    }

    $result_txt .= '<td width="80" align="center">' . $format_loan . '</td>';
    //$result_txt .= '<td width="130" align="left">'. $supplier . '</td>';
    $result_txt .= '<td align="left">' . $trans_borid . '</td>';
    $result_txt .= '<td align="left">' . $borname . '</td>';
    $result_txt .= '<td align="left">' . $item . '</td>';
    $result_txt .= '<td width="70">' . $format_return . '</td>';

    $result_txt .= '<td>' . $overdue . '</td>';
    if ($vol_no_toy_edits != 'Yes') {
        $result_txt .= $ref1;
    }
}
$result_txt .= '</tr></table>';
$result_txt .= $toprint_txt;

//print '<br/>' . $query;
//print '<br/>' . $location;
//below this is the function for no record!!
//end
//print $query_toys . '<br>';
//print $connect_pdo . '<br>';

if ($toprint > 0) {
    print '<div id="open"><table width="100%"><tr><td width= 50%><h1 align="left"></h1></td><td><h1 align="right">Total: ' . $total . '   Total Days out: ' . $days_out . '</h1></td><tr></table></div>';
    print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
    print '<tr style="color:green"><td>id</td><td>Toy id</td><td>date loan</td><td>id</td><td>Loaned to</td><td>Returned</td><td>Days on loan</td></tr>';

    print $result_txt;
} else {
    print '<br><strong>There is no Toy History</strong><br><br>';
}
?>
