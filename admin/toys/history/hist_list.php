<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');
//include( dirname(__FILE__) . '/../../get_settings.php');
$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);


$query_toys = "SELECT * from transaction where return is not null and idcat = ? order by created desc limit 20;";

//echo $search_str;

$sth = $pdo->prepare($query_toys);
//$array = array($search_str, $search_str, $search_str, $search_str,$search_str, $category_str, $age_str);
//$array = array($search_str, $search_str, $search_str, $search_str, $search_str, $category_str, $age_str);
$array = array($_SESSION['idcat']);
$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

//$query = "SELECT * FROM toys ORDER by id ASC;";

echo '<h2><font color="#330066">LAST 20 RETURNS:</font>';
echo '<a class="button1" href="../../toys/history/history.php?idcat=' . $_SESSION['idcat'] . '">All History</a></h2>';
  
$XX = "No Record Found";
$overdue = 0;

$result_txt = '';
$today = date("Y-m-d");
$days_out = 0;
$total = sizeof($result);


for ($ri = 0; $ri < $total; $ri++) {
    //$row = pg_fetch_array($result, $ri);
    $days_out = $days_out + $overdue;
    $row = $result[$ri];
    $borname = $row['borname'];
    $return = $row['return'];
    $borid = $row['borid'];
    $item = $row['item'];


    $ts1 = strtotime($row['date_loan']);
    $ts2 = strtotime($row['return']);

    $overdue = ($ts2 - $ts1) / (-60 * 60 * 24);
    $overdue = round($overdue * -1, 0);

    $format_return = substr($row['return'], 8, 2) . '-' . substr($row['return'], 5, 2) . '-' . substr($row['return'], 0, 4);
    $format_created = substr($row['created'], 8, 2) . '-' . substr($row['created'], 5, 2) . '-' . substr($row['created'], 0, 4);

    $id = $row["id"];
    $idcat = $row["idcat"];

    $result_txt .= '<tr border="1" class="item">';
    $result_txt .= '<td align="center">' . $format_return . '</td>';
    $result_txt .= '<td align="center">' . $format_created . '</td>';
    $result_txt .= '<td align="left" width="40%">' . $borid . ': ' . $borname . '</td>';
    //$result_txt .= '<td align="left" width="40%">' . $item . '</td>';
    $result_txt .= '<td>' . $overdue . '</td>';
}
$result_txt .= '</tr></table>';


//print '<div id="open"><table width="100%"><tr><td width= 50%><h1 align="left"></h1></td><td><h1 align="right">Total: ' . $total . '   Total Days out: ' . $days_out . '</h1></td><tr></table></div>';
print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
print '<tr style="color:green"><td>Return</td><td>Loaned</td><td>Loaned to</td><td>Days</td></tr>';

print $result_txt;
?>
