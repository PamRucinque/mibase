<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<link href="../../css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<script src="../../js/jquery-1.9.1.js"></script>
<script src="../../js/jquery-ui.js"></script>
<script>
    function setFocus()
    {
        document.getElementById("scanid").focus();
    }
</script>
<script type="text/javascript">
    $(function() {
	
        //autocomplete
        $(".auto").autocomplete({
            source: "../find/search.php",
            autoFocus: true,
            select: function(event, ui) {
                //For better understanding kindly alert the below commented code
                //alert(ui.toSource()); 
                var selectedObj = ui.item;

                //alert(selectedObj.value);
                document.getElementById('idcat').value = selectedObj.value
                document.forms["change_toy"].submit();
            }
                    
        });				

    });
</script>

<div class="row" style="padding-top: 5px;">
    <div class="col-sm-4">
        <form  id="scan" method="post" action="toy_detail.php" width="100%">
            <input align="center" type="text"   placeholder="Scan Toy" name="scanid" id ="scanid" style="max-width: 90px; background-color: lightgoldenrodyellow;" value="" onchange='this.form.submit()'></input>
        </form>      
    </div>
    <div class="col-sm-8">
        <form id="change_toy" method="post" action="toy_detail.php">

            <input type='text' style="background-color: lightgoldenrodyellow;" name='toyid' id='toyid' value='' class='auto' onKeyPress="login()" placeholder="Search Toy"></p>
            <input type="hidden" id="idcat" name ="idcat" />
        </form>
    </div>
</div>
<?php

?>

