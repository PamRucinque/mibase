<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

?>
<table width="100%" bgcolor="lightgrey">
    <tr>
        <td width ="300px">
            <?php 
            if ($find_newtoy == 'Yes'){
              include( dirname(__FILE__) . '/toy_select_find.php');  
            }else{
                include( dirname(__FILE__) . '/toy_select.php'); 
            }
             ?> 
        </td>
        <td>
            <?php 
            if ($_SESSION['idcat'] != ''){
              include( dirname(__FILE__) . '/toy_detail_find.php');  
            }
             ?>
        </td>
        <?php
        if ($find_newtoy != 'Yes') {
            echo '<td align="right">';
            //echo '<a class="button1" href="../../toys/history/history.php?idcat=' . $_SESSION['idcat'] . '">History</a>';
            echo '</td>';
        }
        ?>
        <td width="50px">
        </td>
    </tr>
</table>
