<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');



//get configuration data
//include(__DIR__ . '/../../config.php');



$idcat = $_SESSION['idcat'];

//echo $_SESSION['idcat'];
$linktotoy = '<a class="button1" href="../../admin/toys/update/toy_detail.php?idcat=' . $idcat . '">' . $idcat . '</a>';

if ($find_newtoy != 'Yes') {
    //echo '<br><h3><font color="blue">' . $idcat . '</font>  ' . $toyname . '</h3> ';
}

if ($toy_exists == 'No') {
    // echo '<h3><font color="red">This Toy Does Not Exist.</font></h3> ';
} else {
    if ($toy_status != 'ACTIVE') {
        if ($toy_status == 'PROCESSING') {
            echo '<h3><font color="red">This Toy is being PROCESSED.</font></h3> ' . $toy_status;
        } else {
            echo '<h3><font color="red">This Toy is LOCKED or WITHDRAWN.</font></h3> ' . $toy_status;
        }
    }
}
 
if ($_SESSION['shared_server']) {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    } else {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
    }
} else {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
    } else {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
    }
}

if (isset($_SESSION['idcat'])) {
    echo '<br><h3><font color="blue">' . $idcat . '</font>  ' . $toyname . ':  <font color="green">' . $category . '</font></h3> ';
    $str_pic = '<br><a>' . $pic_url . '</a>';
    if ($find_newtoy == 'Yes') {
        echo $str_pic;
    }
}
//echo $str_pic;
?>






