<?php
include( dirname(__FILE__) . '/get_index_info.php');
if ($logo_height == '') {
    $logo_height = '150';
}
$link_login = 'https://' . $_SESSION['library_code'] . '.mibase.com.au/members/checklogin2.php';
if (!session_id()) {
    session_start();
}
?>
<div class="flex-container">

    <?php
    
    echo '<div><h1 align="left">' . $libraryname . '</h1>';
    echo '<a href="index.php"  style="font-size: larger;">Back to Home Page</a>';
    echo '<h2><font color="darkblue">Member Login</font></h2>';
    echo '<p>For <a href="../admin/login.php" target="_blank">Admin</a>';
    if ($volunteer_login_off != 'Yes'){
         echo ' and <a href="../volunteer/login.php" target="_blank">Volunteer</a> ';
    }
   
    echo ' Login, select from the Toy Library Menu Item above.';
    echo '</div>';
    echo '<div style="float:right"><a align="right"><img src="https://' . $_SESSION['library_code'] . '.mibase.com.au/login_image/' . $_SESSION['library_code'] . '.jpg" alt="Library Logo" height="' . $logo_height . 'px"></a></div>';
    ?>
</div>
<div class="flex-container">

    <div style ='min-width: 300px; justify-content: right;'>

        <div><table>
                <form name = "form1" method = "post" action = "<?php echo $link_login; ?>">

                    <tr style="height: 40px;"><td>Username: </td><td><input name = "myusername" type = "text" id = "myusername" required ></td></tr>
                    <tr><td width="30%">Password: </td><td><input name = "mypassword" type = "password" id = "mypassword" required  ><br></td></tr>
                    <tr><td></td><td align="right"><br><input id = "button_red_login" type = "submit" name = "Submit" value = "Login"></td></tr>
                </form>
            </table>
            <br><font color = "red"><strong><?php echo $_SESSION['login_status']; ?></strong></font>
            <br>First time user or you have forgotten your username or password? 
            <?php echo '<a href="https://' . $_SESSION['library_code'] . '.mibase.com.au/members/email_password.php" class="button">Email Password</a>'; ?>
            <div class="blink_me">Tried everything and you still can't login? 
                <?php
//echo $email_from . '<br>';
                echo '<a class="button" href="mailto:' . $email_from . '?subject=Help Required Logging into Mibase.">Email Support</a></div>';
                ?>

            </div>
        </div>






