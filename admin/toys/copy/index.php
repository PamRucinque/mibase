<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>    
    <script>
        function display() {
            var toy = document.getElementById("toy").innerHTML;
            var category = document.getElementById("cat_div").innerHTML;
            //alert(toy);
            if (toy === '') {
                $("#category_div").hide();
                $("#number_div").hide();
                $("#pic_div").hide();
            } else {
                $("#category_div").show();
                $("#number_div").hide();
                if (category !== '') {
                    $("#number_div").show();
                }
            }
            var msg = document.getElementById("msg").innerText;
            //alert(msg);
            if (msg !== '') {
                $('#myModal').modal('show');
            }
        }
        function check_number() {
            var id = document.getElementById("get_id").value;
            if (id === "") {
                alert("You must enter a valid number > 0.");
                return false;
            }
        }
    </script>

    <?php
    //include( dirname(__FILE__) . '/../../get_settings.php');
    ?>
    <body onload="display();">
        <?php
        include( dirname(__FILE__) . '/../../menu.php');
        include( dirname(__FILE__) . '/functions.php');
        $catsort = $_SESSION['settings']['catsort'];
        $format_toyid = $_SESSION['settings']['format_toyid'];

        $button_str = 'OK';
        $java_str = "$(location).attr('href', 'index.php')";
        $str_alert = '';
        //echo $out;

        $error_txt = '';
        $idcat_new = '';
        $category_new = '';
        $out = '';


        if (isset($_POST['idcat'])) {
            if ($_POST['idcat'] != $_SESSION['idcat']) {
                $_SESSION['cat'] = '';
            }
            $_SESSION['idcat'] = $_POST['idcat'];
            $idcat = $_SESSION['idcat'];
        }
        if ($_SESSION['idcat'] == '') {
            $_SESSION['cat'] = '';
        }

        if (isset($_POST['category'])) {
            $_SESSION['cat'] = $_POST['category'];
        }

        if (isset($_SESSION['cat'])) {
            $cat_new = get_category($_SESSION['cat']);
            $unused = unused($_SESSION['cat'], $catsort);
            $id = $unused['first'];
            $_SESSION['toyid'] = $id;
        }




        if (isset($_POST['get_id'])) {
            $id = $_POST['get_id'];
            $error = check_toy_exists($id, $_SESSION['cat'], $catsort);
            if ($error == 'No') {
                $_SESSION['toyid'] = $id;
            } else {
                $error_txt = $error;
                $str_alert = $error;
                $out = 'No';
            }
        }
        if (isset($_SESSION['cat']) && isset($_SESSION['toyid'])) {
            if ($catsort == 'Yes') {
                $idcat_new = $_SESSION['cat'] . $_SESSION['toyid'];
            } else {
                $idcat_new = $_SESSION['toyid'];
            }
        }
        if (isset($_POST['copy_toy'])) {
            $subdomain = 'demo';
            $out = copy_toy($_POST['id'], $_POST['category_new'], $_POST['idcat'], $catsort, $format_toyid, $subdomain);
            if ($out['status'] != 'No') {
                $_SESSION['cat'] = '';
                $_SESSION['idcat'] = '';
                $_SESSION['error'] = '';
                $_SESSION['toyid'] = '';
            }
            $str_alert = $out['out'];
        }
        ?>
        <div  id="toy" style="display: none;"><?php echo $_SESSION['idcat']; ?></div>
        <div  id="cat_div" style="display: none;"><?php
            if (isset($_SESSION['cat'])) {
                echo $_SESSION['cat'];
            }
            ?></div>
        <div class="container-fluid">
            <h1><font color="red">Toy Copy</font></h1><br>
            <div class="row">
                <div class="col-sm-3 col-xs-12">
                    <h4>Select a Toy to Copy</h4>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php include( dirname(__FILE__) . '/toy_select.php'); ?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php
                    if (isset($_SESSION['idcat'])) {
                        $toy = get_toy($_SESSION['idcat']);
                        if (isset($toy['idcat'])) {
                            echo '<h4>' . $toy['idcat'] . ': ' . $toy['toyname'] . '</h4><br>';
                        }
                    }
                    ?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div id="pic_div">
                        <?php
                        $image = get_pic($_SESSION['idcat']);
                        echo $image . '<br>';
                        ?>
                    </div>
                </div>
            </div>

            <div class="row" id="category_div">
                <div class="col-sm-3 col-xs-12">
                    <h4>Select Category:</h4>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <form  method="post" action="index.php">
                        <?php include( dirname(__FILE__) . '/get_category.php'); ?>
                    </form>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php
                    if (isset($cat_new['category'])) {
                        echo '<h4>' . $cat_new['category'] . ': ' . $cat_new['description'] . '</h4>';
                        $category_new = $cat_new['category'];
                    }
                    ?>
                </div>
                <div class="col-sm-3 col-xs-12">

                </div>
            </div>
            <div class="row"  id="number_div">
                <div class="col-sm-3 col-xs-12">
                    <h4>Change Toy Number:</h4>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <form  method="post" action="index.php"  onsubmit="return check_number();">
                        <input type="number" name="get_id" id="get_id" class="form-control" min="1" max="50000" required value="<?php echo $_SESSION['toyid']; ?>" ></input>
                    </form>
                    <form  method="post" action="index.php"  onsubmit="return check_number();">
                        <input type="hidden" name="idcat" id="idcat" value="<?php echo $_SESSION['idcat']; ?>"> 
                        <input type="hidden" name="idcat_new" id="idcat_new" value="<?php echo $idcat_new; ?>"> 
                        <input type="hidden" name="category_new" id="category_new" value="<?php echo $_SESSION['cat']; ?>"> 
                        <input type="hidden" name="id" id="id" value="<?php echo $_SESSION['toyid']; ?>"> 
                        <?php
                        echo $error_txt;
                        if ($_SESSION['idcat'] != '') {
                            echo '<br><input id="copy_toy" name="copy_toy" class="btn btn-primary"  type="submit" name="submit" value="Copy this Toy" Required/>';
                        }
                        ?>
                    </form>


                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php
                    echo '<h4>' . $idcat_new . '</h4>';
                    ?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php
                    if (isset($unused['unused'])) {
                        echo $unused['unused'];
                    }
                    ?>
                </div>
            </div>
            <?php include('msg_form.php'); ?>
        </div>
    </body>
    <script src="../../js/bootstrap.min.js"></script>
    <script>
            $(document).ready(function () {
                $('.dropdown-toggle').dropdown();
            });
    </script>
</html>