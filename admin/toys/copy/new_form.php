<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
$format_toyid = $_SESSION['settings']['format_toyid'];
?>
<form name="category" id="category" method="post" action="" >    

    <table  bgcolor="lightgray" width="100%"><tr>

            <td>New Toy Number: <br>
                <input type="number" name="id" id="id" min="1" max="50000" required value="<?php echo $_SESSION['toyid']; ?>" onchange='this.form.submit()'></input>
                <?php echo $_SESSION['toyiderror']; ?>
            </td>
            <td>
                 <?php include( dirname(__FILE__) . '/get_category.php'); ?>
            </td>
            <td><strong><h2><?php
                        $toyid = $_SESSION['toyid'];
                        //$category = $_SESSION['category'];
                        if ($format_toyid == 'Yes') {
                            $toyid = sprintf('%02s', $toyid);
                        }
                        if ($catsort == 'Yes') {
                            $idcat_new = $category . $toyid;
                        } else {
                            $idcat_new = $toyid;
                        }
                        include( dirname(__FILE__) . '/get_category_desc.php');
                        //echo ' The following Toy Will be added when you press save <br>';
                        echo 'New Toy id: <font color="red">' . $idcat_new . '</font>';
                        echo '<br>Category: <font color="blue">' . $_SESSION['category'] . ' - ', $cat_desc . '</font><br>';
                        ?>
                        </font></h2></strong>

            </td>
            <?php
    
            echo '<td align="left"><br><input id="copytoy" name="copytoy" class="button1"  type="submit" name="submit" value="Copy this Toy" /></td>';
            
            ?>

        </tr></table>
        <input type="hidden" name="idcat" value="<?php echo $_SESSION['idcat']; ?>"> 
        <input type="hidden" name="idcat_new" value="<?php echo $idcat_new; ?>"> 
        <input type="hidden" name="category_new" value="<?php echo $_SESSION['category']; ?>"> 
  
</form>



