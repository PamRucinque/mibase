<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');

if (!session_id()) {
    session_start();
}

//$idcat = 'C02';
//$category = 'D';
//$catsort = 'Yes';
//$unused = unused($category, $catsort);
//echo $unused['unused'] . '<br>';
//echo $unused['first'];
//$cat_new = get_category('D');
//echo $cat_new['description'];
//$out = copy_toy($unused['first'], $category, 'C02', 'Yes', 'Yes');
//echo $out;

function get_toy($idcat) {
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $toy = array();
    $sql = "select toys.id,idcat,c.category,toyname, c.description, desc1, desc2,
            supplier, rent, age, no_pieces, warnings, reservedfor as condition, cost, 
            discountcost, alert, returndateperiod, location,manufacturer   
            from toys
            left join category c on c.category = toys.category
            where idcat = ?";
    $sth = $pdo->prepare($sql);

    $array = array($idcat);
    $sth->execute($array);
    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    for ($ri = 0; $ri < $numrows; $ri++) {
        $toy = $result[$ri];
    }
    return $toy;
}

function get_category($category) {
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $toy = array();
    $sql = "select * from category
            where category = ?";
    $sth = $pdo->prepare($sql);

    $array = array($category);
    $sth->execute($array);
    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    for ($ri = 0; $ri < $numrows; $ri++) {
        $category = $result[$ri];
    }
    return $category;
}

function unused($category, $catsort) {
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $first = 1;
    if ($catsort == 'Yes') {
        $sql = "select * from generate_series(1, (SELECT MAX(id) FROM toys where 
            category = ?)) as unused
            where unused not in (select id from toys Where category = ?) LIMIT 20;";
        $array = array($category, $category);
    } else {
        $sql = "select * from generate_series(1, (SELECT MAX(id) FROM toys)) as unused
            where unused not in (select id from toys) LIMIT 20;";
        $array = array();
    }
    $sth = $pdo->prepare($sql);
    $sth->execute($array);
    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    if ($numrows > 0) {
        if ($catsort == 'Yes') {
            $result_txt = '<font color="blue">Lowest 20 numbers in Category ' . $category . ': </font>  ';
        } else {
            $result_txt = '<font color="blue">Lowest 20 numbers: ' . '</font>  ';
        }
    } else {
        $result_txt = '<font color="blue">All numbers in ' .$category . ' are available.</font>  ';
    }

    for ($ri = 0; $ri < $numrows; $ri++) {
        $toy = $result[$ri];
        if ($ri == 0) {
            $first = $toy["unused"];
        }
        $result_txt .= $toy['unused'] . ' ';
    }
    return array('unused' => $result_txt, 'first' => $first);
}

function check_idcat($idcat) {
//include( dirname(__FILE__) . '/../../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "select Count(id) AS countid FROM toys where idcat='" . $idcat . "';";
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $toys = $row['countid'];
    if ($toys > 0) {
        $out = 'There is another toy with this toy code, please search for this toy number and change.';
    }
//$trans = 4;
    return $out;
}

function copy_picture($idcat, $idcat_new, $subdomain) {
    $out = '';
    $filename = strtolower($idcat) . '.jpg';
    $UploadDirectory = $_SESSION['toy_images_location'] . $subdomain . '/'; //specify upload directory ends with / (slash)
    $NewFileName = strtolower($idcat_new) . '.jpg';
    if (file_exists($UploadDirectory . $filename)) {
        $out = copy($UploadDirectory . $filename, $UploadDirectory . $NewFileName);
    }
    return $out;
}

function copy_toy($id, $category, $idcat, $catsort, $format_toyid,$subdomain) {
    $status = 'No';
    $today = date('Y-m-d');
    $idcat_new = '';
    if ($format_toyid == 'Yes') {
        $toyid = sprintf('%02s', $id);
    }else{
        $toyid = $id;
    }
    if ($catsort == 'Yes') {
        $idcat_new = $category . $toyid;
    } else {
        $idcat_new = $toyid;
    }
    $toy = get_toy($idcat);
    //echo $toy['toyname'];
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $sql = "INSERT INTO toys (id, idcat, category, toyname, 
                toy_status, status, date_purchase, 
                location, returndateperiod,desc1, desc2, manufacturer, supplier, rent,
                age, no_pieces, warnings, reservedfor, cost, discountcost, alert)
                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
    //and get the statment object back
    $sth = $pdo->prepare($sql);
    $array = array($id, $idcat_new, $category, $toy['toyname'],
        'ACTIVE', 0, $today, $toy['location'], $toy['returndateperiod'],
        $toy['desc1'], $toy['desc2'], $toy['manufacturer'], $toy['supplier'], $toy['rent'],
        $toy['age'], $toy['no_pieces'], $toy['warnings'], $toy['condition'], $toy['cost'], $toy['discountcost'], $toy['alert']);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        //$out = "An INSERT query error occurred.\n";
        $out = '<br>Error ' . $stherr[0] . ' ';
        $out .= $stherr[1] . '<br> ';
        $out .= $stherr[2] . '<br>';
        //exit;
    } else {
        $status = 'Yes';
        $out = 'Success, you have copied ' . $idcat . ' to ' . $idcat_new . ': ' . $toy['toyname'] . '<br>';
        $out .= copy_picture($idcat, $idcat_new, $subdomain);
    }
    return array('out' => $out, 'status' => $status);
}

function check_toy_exists($toyid, $category, $catsort) {
    $connect_str = $_SESSION['connect_str'];
    $toy_exists = 'No';
    if ($catsort == 'Yes') {
        $query_check = "SELECT id FROM toys WHERE category = '" . $category . "' and id = " . $toyid . ";";
    } else {
        $query_check = "SELECT id FROM toys WHERE id = " . $toyid . ";";
    }
    $dbconn = pg_connect($connect_str);
    $result_rows = pg_exec($dbconn, $query_check);
    $numrows = pg_numrows($result_rows);
    if ($numrows == 1) {
        $toy_exists = '<font color="red">The id ' . $toyid . ' in category ' . $category . ' is already being used.</font>';
    }
    return $toy_exists;
}

function get_pic($idcat) {
    if ($_SESSION['shared_server']) {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
        }
    } else {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
        }
    }
    $pic_img = '<img height="100px" src="' . $pic_url . '" alt="toy image">';
    return $pic_img;
}
