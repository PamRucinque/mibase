<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//connect to MySQL

$conn = pg_connect($_SESSION['connect_str']);

//include( dirname(__FILE__) . '/../connect.php');
$query = "SELECT id, category, description FROM category ORDER by category ASC;";
$result = pg_Exec($conn, $query);

echo '<select name="category" id="category" onchange="this.form.submit();" class="form-control" placeholder="Select Category">\n';


$numrows = pg_numrows($result);

if ($_SESSION['cat'] != '') {
    echo '<option value="' . $_SESSION['cat'] . '" selected="selected">' . $_SESSION['cat'] . '</option>';
} else {
    echo '<option value="" selected="selected">Select Category</option>';
}
//echo "<option value='" . $_SESSION['category'] . "' selected='selected'>" . $_SESSION['category'] . "</option>";
for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    echo "<option value='{$row['category']}'>{$row['category']} :{$row['description']}
    </option>\n";
}

echo "</select>\n";
?>

