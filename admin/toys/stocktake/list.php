<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');
$conn = pg_connect($_SESSION['connect_str']);

$returns_txt = '';
$total = 0;
$category_str = "'%" . $_SESSION['category'] . "%'";
$search  = strtoupper($_SESSION['search']);
$search_str = "'%"  . $search . "%'";

$query_toys = "SELECT
     toys.idcat as idcat,
     toys.toyname as toyname,
     toys.category as category,
     toys.no_pieces as no_pieces,
     toys.toy_status as toy_status,
     toys.modified as modified,
     toys.id as id,
     toys.alert as alert,
     toys.stocktake_status as stocktake_status,
     toys.lockreason as lockreason,
     transaction.due as due,
     transaction.borname as borname,
     transaction.borid as borid
FROM
     toys 

     LEFT OUTER JOIN
     (
     SELECT
          transaction.idcat,
          transaction.due,
          transaction.borname,
          transaction.borid
     FROM
          transaction where return is null
     GROUP BY
          transaction.due, transaction.idcat, transaction.borname, transaction.borid

     ) AS transaction ON upper(transaction.idcat) = upper(toys.idcat)
     WHERE toy_status = 'ACTIVE' and (stocktake_status = 'LOCKED' or stocktake_status = 'PENDING') 
     AND category LIKE " . $category_str . " 
      AND (upper(toyname) LIKE " . $search_str . "  OR upper(manufacturer) LIKE " . $search_str . " 
        OR upper(storage) LIKE " . $search_str . " OR upper(toys.idcat) LIKE " . $search_str . ")
     order by category, idcat;";

//echo $query_toys;
$trans = pg_exec($conn, $query_toys);
$numrows = pg_numrows($trans);
$total = 0;
if ($_SESSION['category'] != ''){
    echo '<p>Filtered on: ' . $_SESSION['category'] . '</p>';
}
if ($_SESSION['search'] != ''){
    echo '<p>Filtered on: ' . $_SESSION['search'] . '</p>';
}

if ($numrows > 0) {
    $returns_txt .= '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey">';
    $returns_txt .= '<tr><td>id</td><td>date</td><td>toy #</td><td>Toy name</td><td>id</td><td>Member</td><td>Location</td><td>Status</td><td></td></tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($trans, $ri);
    $total = $total + 1;
    //$weekday = date('l', strtotime($row['date_roster']));
    $toy_id = $row['id'];
    $due = $row['due'];
    //$transid = $row['transid'];
    $toyname = $row['toyname'] . '. <font color="red">' . $row['alert'] . '</font><font color="blue"> ' . $row['lockreason'] . '</font>';
    $trans_idcat = $row['idcat'];
    $trans_borname = $row['borname'];
    $trans_borid = $row['borid'];
    $modified = $row['modified'];
    $format_modified = substr($row['modified'], 8, 2) . '-' . substr($row['modified'], 5, 2) . '-' . substr($row['modified'], 0, 4);
    $format_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);
    $now = date('Y-m-d');
    $ref2 = '../toys/update/toy_detail.php?idcat=' . $row['idcat'];
    $ref = 'renew_toy.php?id=' . $row['id'];
    $status = $row['stocktake_status'];

    if ($due != '') {

        if (strtotime($now) > strtotime($due)) {
            $due_str = '<font color="red" font="strong"> OVERDUE  ' . $format_due . '</font>';
        } else {
            $due_str = '<font="strong">DUE: ' . $format_due . '</font>';
        }
    } else {
        $due_str = 'IN LIBRARY';
    }
    //<a class="button_menu" href="../../toys/update/toy_detail.php">Toy</a>
    $returns_txt .= '<td width="30px">' . $toy_id . '</td>';
    $returns_txt .= '<td width="50px">' . $format_modified . '</td>';

    //echo '<td width="30" align="left"><a class="button_small" href="../../admin/toys/update/toy_detail.php?idcat=' . $trans_idcat . '">' . $trans_idcat . '</a></td>';
    //$returns_txt .= '<td width="30px" align="left"><a class="button_small" href="../update/toy_detail.php?idcat=' . $trans_idcat . '">' . $trans_idcat . '</a></td>';
    $returns_txt .= '<td width="30px" align="left"><a class="button_small" href="stocktake.php?t=' . $trans_idcat . '">' . $trans_idcat . '</a></td>';

    $returns_txt .= '<td width="250px">' . $toyname . '</td>';
    //echo '<td width="150" align="left">' . $trans_bornmame . '</td>';
    if ($due != '') {
        $returns_txt .= '<td width="30px" align="left"><a class="button_small_red" href="../../members/update/member_detail.php?b=' . $trans_borid . '">' . $trans_borid . '</a></td>';
        $returns_txt .= '<td align="left">' . $trans_borname . '</td>';
    } else {
        $returns_txt .= '<td width="30px" align="left"></td>';
        $returns_txt .= '<td width="180px" align="left"></td>';
        
   
    }
            $returns_txt .= '<td width="70px" align="center">' . $due_str . '</td>';
            //$returns_txt .= '<td width="50px" align="left">' . $row['toy_status'] . '</td>';
            //$returns_txt .= '<td width="50px" align="left">' . $row['stocktake_status'] . '</td>';
            $returns_txt .= '<td width="70px" align="center">' . $status . '</td>';
            $returns_txt .= '<td width="30px" align="left"><a class="button_small_yellow" href="stocktake.php?unlock=' . $trans_idcat . '">Unlock</a></td>';









    $returns_txt .= '</tr>';
}

$returns_txt .= '</table>';


pg_close($conn);

echo '<font color="blue">Total locked Toys for Stock Take: ' . $total . '</font>';
echo $returns_txt;
?>

</body>


