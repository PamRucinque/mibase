<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');
//connect to MySQL

$conn = pg_connect($_SESSION['connect_str']);
$last = $_SESSION['idcat'];

$idcat = $_SESSION['idcat'];

$query_toy = "SELECT toys.*, t.due
                from toys 
                left join (select due, idcat from transaction where return is null limit 1) t on t.idcat = toys.idcat 
         WHERE upper(toys.idcat) = '" . strtoupper($idcat) . "'";
//echo $query_toy;
$result_toy = pg_Exec($conn, $query_toy);
$numrows = pg_numrows($result_toy);
$status_txt = Null;

if ($numrows == 0){
    $toy_exists = 'No';
$_SESSION['idcat'] = $last;
}else{
    $toy_exists = 'Yes';
    $_SESSION['idcat'] = $idcat;
}


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_toy, $ri);
//$desc2 = str_replace("\r\n", "</br>", $desc2);

    $toy_id = $row['id'];
    $idcat = $row['idcat'];
    $category = $row['category'];
    $toyname = str_replace("'", "`", $row['toyname']);
    $no_pieces = $row['no_pieces'];
    $stocktake_status = trim($row['stocktake_status']);
    //$date_entered = date_format($row['date_entered'], 'd/m/y');
    $due = $row['due'];
    $toy_status = $row['toy_status'];
    $lockdate = $row['lockdate'];
    $lockreason = $row['lockreason'];
    $created = $row['created'];
    $modified = $row['modified'];
    $condition = $row['reservedfor'];
    $desc1 = $row['desc1'];
    $desc2 = $row['desc2'];
    $warnings = $row['warnings'];
    $rent = $row['rent'];
    $format_purchase = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    $format_lock= substr($row['datestocktake'], 8, 2) . '-' . substr($row['datestocktake'], 5, 2) . '-' . substr($row['datestocktake'], 0, 4);
  


    if ($due != null) {
        $status_txt .= 'ON LOAN';
    } else {
        $status_txt .= 'IN LIBRARY';
    }
    if($stocktake_status == ''){
        $stocktake = 'ACTIVE';
    }
 
}
$alert_toy_txt = '';
//include ('parts.php');
if ($alert_parts != ''){
   $alert_toy_text = $alert  . '<br>' . $alert_parts_txt;
   $alert = $alert . '\n' .  $alert_parts; 

}



//echo 'Type Claim: ' . $type_claim;
//$date_submitted = $date_submitted[weekday]. $date_submitted[month] . $date_submitted[mday] . $date_submitted[year];
?>