<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
$password = $_SESSION['settings']['password'];
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include('../../header.php'); ?> 
    </head>



    <body id="main_body" >
        <div id="form_container">
            <?php
            include('../../menu.php');
           //include("../../get_settings.php");

            $ref = 'stocktake.php';
//echo "<td width='50'><a class ='button1' href='" . $ref  . "'>Edit</a></td>";
            echo "<h2><br><a href='" . $ref . "' class ='button1_logout'>Cancel</a><br><br>";

            echo '<font color="red"> WARNING - YOU ARE ABOUT TO LOCK ALL TOYS FOR STOCKTAKE, THIS PROCESS CANNOT BE UNDONE, AND TOYS CANNOT BE LOANED IF LOCKED:</font></h1><br>';
            include('delete_form.php');

            //include('toy_detail.php');
            if ($password == '') {
                $password = 'mibase';
            }

            if (isset($_POST['submit'])) {
                $category = $_POST['category'];
                //include('../../connect.php');
                if ($category != '') {
                    $query = "update toys set datestocktake = current_date, stocktake_status = 'LOCKED' where toy_status = 'ACTIVE' and upper(category) = '" . strtoupper($category) .  "';";
                } else {
                    $query = "update toys set datestocktake = current_date, stocktake_status = 'LOCKED' where toy_status = 'ACTIVE'";
                }

                if (($_POST['password'] == $password)) {
                    $result = pg_exec($conn, $query);
                    if (!$result) {
                        echo "An error has occurred. An email message has been sent to mibase.<br>";
                        $message = 'Whole query: ' . $query . '<br>';
                        //echo $message;
                        $email_to = 'michelle@mibase.com.au';
                        $email_subject = 'Error on Alert Member: ' . $_SESSION['library_code'];
                        $headers = "From: " . "MIBASE ERROR" . "<" . $email_to . ">\nReply-To: " . $email_to . "\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                        @mail($email_to, $email_subject, $message, $headers);
                        //exit;
                    } else {
                        echo "<br><p>All Toys have been successfully Locked.</p><br>";
                        echo '<a class="button1" href="stocktake.php">OK</a>';
                    }

                    pg_FreeResult($result);
                    pg_Close($conn);
                } else {
                    echo "<br><p>Incorrect Password.</p><br>";
                    echo '<a class="button1" href="../toys.php">OK</a>';
                }
            }
            ?>
        </div>
    </body>
</html>

<?php

function check_loans($idcat) {
    //include('../../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "select * from transaction where idcat='" . $idcat . "' and return is null;";
    $result = pg_Exec($conn, $sql);
    $numrows = pg_numrows($result);
    //$trans = 4;
    return $numrows;
}
