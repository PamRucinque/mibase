<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');

function unlock_toy($idcat) {
    
    $conn = pg_connect($_SESSION['connect_str']);
    $sql_trans = "update toys set stocktake_status = 'ACTIVE', datestocktake = now() where idcat = '" . $idcat . "' and stocktake_status = 'LOCKED';";

    $result_change = pg_Exec($conn, $sql_trans);
    if (!$result_change) {
        $outcome = 'cannot change stock take status:';
    }else{
        $outcome = '';
    }
        
    return $outcome;
}

function delete_rent($idcat, $borid) {
    include($url_button . 'connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "DELETE from journal WHERE 
    (datepaid::date)=(now()::date) AND ((icode)='" . $idcat . "') AND ((bcode)=" . $borid . ") AND ((category)= 'Rent');";
    $result_delete = pg_Exec($conn, $sql);
    if (!$result_delete) {
        $outcome = 'cannot delete rent:' . $query;
    }
    return $outcome;
}

?> 