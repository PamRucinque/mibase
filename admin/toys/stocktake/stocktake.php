<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
include('functions.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include('../../header.php'); ?> 
    </head>
    <body onload="setFocus()"> 
        <div id="form_container">    
            <?php
////include('../connect.php');
            if (isset($_GET['category'])) {
                $_SESSION['category'] = $_GET['category'];
            }
            if (isset($_GET['search'])) {
                $_SESSION['search'] = $_GET['search'];
            }
            if (isset($_GET['unlock'])) {
                //$_SESSION['loan_status'] = '';
            
                $_SESSION['idcat'] = $_GET['unlock'];
                $idcat = $_SESSION['idcat'];
                $unlock = unlock_toy($idcat);
                $_SESSION['loan_status'] = $unlock;
            }
            if (isset($_GET['t'])) {
                //$_SESSION['loan_status'] = '';
                $_SESSION['idcat'] = $_GET['t'];
                $idcat = $_SESSION['idcat'];
                //$unlock = unlock_toy($idcat);
                //$_SESSION['loan_status'] = $unlock;
            }

            include('../../menu.php');
         //   if ($branch == 'admin') {
                include('../../header_detail/header_detail.php');
         //   }


            if (isset($_POST['scanid'])) {

                $_POST['scanid'] = strtoupper($_POST['scanid']);
                $string = substr($_POST['scanid'], 0, 1);
                if ($string == '-') {
                    $_POST['scanid'] = rtrim(substr($_POST['scanid'], 1));
                    //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
                }
                if ($string == 'T') {
                    //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
                    $_POST['scanid'] = rtrim(substr($_POST['scanid'], 2));
                }
                $idcat = $_POST['scanid'];
                include('data/get_toy.php');
                if ($toy_exists == 'Yes') {
                    //$_SESSION['idcat'] = $idcat;
        
                    $_SESSION['idcat'] = $_POST['scanid'];
                    //$idcat = $_SESSION['idcat'];
                    $unlock = unlock_toy($idcat);
                    $_SESSION['loan_status'] = $unlock;
                } else {
                    $_SESSION['idcat'] = $last;
                    $str_detail = '<br><h3><font color="blue">' . $_POST['scanid'] . ': </font>Cannot find this Toy!</h3> ';
                }
            }
            ?>
              <table width="100%" bgcolor="lightgreen">

                <tr>
                    <td width ="300px"  valign="top" bgcolor="lightgreen"><?php include('toy_select.php'); ?>  
                    </td>        
                    <td valign="top" bgcolor="lightgreen"><?php
                        include('data/get_toy.php');
                        if (isset($_SESSION['idcat'])) {
                            include('toy_detail_print.php');
                        }
                        include 'filter_form.php';
                        if (isset($_SESSION['idcat'])) {
                            echo $str_detail;
                            echo $str_parts;
                        }
                        ?>
                    </td>
                    <td  bgcolor="lightgreen" valign="center"><?php echo $str_pic . '<br><br>'; ?></td>
                </tr>
            </table>

            <?php
            include('list.php');
             $link_to_lock = '<br><br><a class="button1_logout" title="Lock all Toys" href="locktoys.php">Lock All Toys</a>';
             echo $link_to_lock;
            ?>
        </div>
    </body>


