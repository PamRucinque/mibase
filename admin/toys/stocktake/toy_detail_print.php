<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');


//get configuration data
//include(__DIR__ . '/../../config.php');

//$id = $_GET['id'];
$idcat = $_SESSION['idcat'];
$desc1 = '';
$desc2 = '';
$stocktake_status = '';
$alert = '';
$warnings = '';
$rent = 0;

$logo = $_SESSION['logo'];

$desc1 = str_replace("\r\n", "</br>", $desc1);
$desc2 = str_replace("\r\n", "</br>", $desc2);
$str_detail = '';
$str_pic = '';




 
if ($_SESSION['shared_server']) {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    } else {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
    }
} else {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
    } else {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
    }
}


    $pic_img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';

//$linktotoy = '<a class="button1" href="../../admin/toys/update/toy_detail.php?idcat=' . $idcat . '">' . $idcat . '</a>';
if ($idcat != ''){
   $str_detail = '<br><h3><font color="blue">' . $idcat . ': </font>  ' . $toyname . '</h3> '; 
}


if ($toy_exists == 'No') {
    echo '<h3><font color="red">This Toy Does Not Exist.</font></h3> ';
} else {
    $_SESSION['idcat'] = $idcat;
    if ($toy_status != 'ACTIVE') {
        $str_detail .= '<h3><font color="red">This Toy is LOCKED or WITHDRAWN.</font></h3> ' . $toy_status;
    }
    if ($stocktake_status == 'ACTIVE') {
        $str_detail .= '<h3><font color="red">This Toy is UNLOCKED.</font></h3> ';
    }
}

include('data/get_toy.php');
if ($stocktake_status == 'LOCKED') {
    $_SESSION['loan_status'] = ' ';
    $_SESSION['return_alert'] = '';
    $ref2 = 'stocktake.php?unlock=' . $idcat;
    $str_detail .= "<td width='200px' valign='top' align='left'><br><a class ='button_yellow_loan' href='" . $ref2 . "'>Unlock</a></td>";
}
?>
<?php

$str_detail .= '<p><font color="red">' . $alert . ' ' . $warnings . '</font></p>';
if ($alert != '') {
    echo '<script>
        $(document).ready(function(){
		alert("' . $alert . '");});
        </script>';
}

//include ('parts.php');
if ($rent > 0) {
    $str_detail .= '<p><font color="red">Rent: ' . $rent . '</font></p>';
}


if (isset($_SESSION['idcat'])) {
    $str_pic = '<br><a>' . $pic_img . '</a>';
}
$_SESSION['loan_status'] = ' ';
$_SESSION['return_alert'] = '';






