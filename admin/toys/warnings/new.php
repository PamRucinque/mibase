<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include('../../header.php'); ?> 
    </head>    

    <body id="main_body" >
        <div id="form_container">
            <?php
            include('../../menu.php');

            //include('../../connect.php');
            $_SESSION['error'] = '';
            echo "<br>" . "<a class='button1' href='warnings.php'>Back to Warnings</a>" . "<br><br>";
            include('new_form_warning.php');

            if (isset($_POST['submit']) && $_SESSION['app'] != '') {
                //$toyid = sprintf("%02s", $toyid);
                include('new_id.php');
                $warning = pg_escape_string($_POST['warning']);
                $description = pg_escape_string($_POST['description']);
                
                
                

                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $query = "INSERT INTO warnings (id, warning, description)
                 VALUES (?,?,?);";
                //and get the statment object back
                $sth = $pdo->prepare($query);

                $array = array($newid, $warning, $description);

                //execute the preparedstament
                $sth->execute($array);
                $stherr = $sth->errorInfo();

                if ($stherr[0] != '00000') {
                    echo "An INSERT query error occurred.\n";
                    echo $connect_pdo;
                    echo 'Error ' . $stherr[0] . '<br>';
                    echo 'Error ' . $stherr[1] . '<br>';
                    echo 'Error ' . $stherr[2] . '<br>';
                    $edit_url = 'new.php';
                    echo '<a class="button1" href="warnings">Back Warnings</a>';
                    exit;
                }


// Get the last record inserted
// Print out the Contact ID
                else {
                    $_SESSION['error'] = '';
                    echo "<br>The record was successfully entered.<a class='button1' href='warnings.php'>OK</a>" . "<br><br>";
                }
            }
            ?>
        </div>
    </body>
</html>