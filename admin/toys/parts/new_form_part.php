<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script>
    function update_type(str) {
        if ((str === 'Found') || (str === 'Alert')) {
            document.getElementById("alert").checked = true;
        } else {
            document.getElementById("alert").checked = false;
        }
    }
</script>

<form  method="post" action="<?php echo 'new_part.php?idcat=' . $_SESSION['idcat']; ?>" style="background-color: lightgray;">
    <h2>New Missing Part:</h2>
    <input id="saveForm" class="button1"  type="submit" name="submit" value="Save" />
    <br>Missing Part Details:<br><input type="Text" name="description" align="LEFT" required  size="50" value=""></input>
    <br>Date: <br><input type="text" name="datepart" id ="datepart" align="LEFT" value="<?php echo $today; ?>"></input>
    <br>Type Part: <br><?php include( dirname(__FILE__) . '/get_part_type_new.php'); ?>
    <br>Cost: <br><input type="number" name="cost" id ="cost" align="LEFT" value="0"></input>
    <br>Member id: <br><?php include( dirname(__FILE__) . '/get_member_new.php'); ?>
      <br>Toy Number:<br><input type="Text" name="idcat" align="LEFT" required  size="10" value="<?php echo $_GET['idcat']; ?>"></input>
 
    <br>Alert User<br><input type="checkbox" id="alert" name="alert"> 
    <br>Paid for missing part: <br><input type="checkbox" id="paid" name="paid"><br> 
</form>




