<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php
        include( dirname(__FILE__) . '/../../header.php');
        ?> 

        <script type="text/javascript">
            $(function () {
                var pickerOpts = {
                    dateFormat: "d MM yy",
                    showOtherMonths: true

                };
                $("#datepart").datepicker(pickerOpts);
            });

        </script>


    </head>
    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');
            //include( dirname(__FILE__) . '/../../connect.php');
            ?>

            <?php
            $today = date('Y-m-d');
            $conn = pg_connect($_SESSION['connect_str']);


            if (isset($_POST['submit'])) {
                $_SESSION['idcat_part'] = $_POST['idcat'];

                include( dirname(__FILE__) . '/new_partid.php');

                if (isset($_POST['alert'])) {
                    if ($_POST['alert'] == 'on') {
                        $alert_txt = 'TRUE';
                    } else {
                        $alert_txt = 'FALSE';
                    }
                }
                if (isset($_POST['paid'])) {
                    if ($_POST['paid'] == 'on') {
                        $paid_txt = 'TRUE';
                    } else {
                        $paid_txt = 'FALSE';
                    }
                }

                //$toyid = sprintf("%02s", $toyid);

                $description = pg_escape_string($_POST['description']);
                $parttype = pg_escape_string($_POST['parttype']);
                $description = clean($description);
                //$idcat = $_GET['idcat'];
                //echo $idcat;
                $query = "INSERT INTO parts (id, itemno, description, type, cost, borcode, datepart, alertuser, paid)
         VALUES ({$newpartid}, 
        '{$_POST['idcat']}', 
        '{$description}', '{$parttype}', {$_POST['cost']}, {$_POST['borid']},
        '{$_POST['datepart']}', '{$alert_txt}', '{$paid_txt}')";

                $result = pg_Exec($conn, $query);


                if (!$result) {
                    echo "An INSERT query error occurred.\n";
                    echo $query;
                    //echo $connection_str;
                    exit;
                }
// Get the last record inserted
// Print out the Contact ID
                else {
                    //$newid = $newid;
                    $edit_url = '../update/toy_detail.php?idcat=' . $_SESSION['idcat_part'];

                    echo "<br>The record was successfully entered and the ID is:" . $newpartid . "<br><br>";
                    //echo 'format Toy id: ' . $format_toyid;
                    echo '<a class="button1" href="' . $edit_url . '">View Toy with parts list</a>';
                    echo '<a class="button1" href="../toys.php">Toy List</a>';



                    // print $query;
                    //include( dirname(__FILE__) . '/send_email.php');
                    // include( dirname(__FILE__) . '/send_email_insurance.php');
                }

                pg_FreeResult($result);
// Close the connection
                pg_Close($conn);
            } else {
                include( dirname(__FILE__) . '/new_partid.php');
                //include( dirname(__FILE__) . '/find/find_member.php');
                include( dirname(__FILE__) . '/new_form_part.php');
            }

            function clean($input) {
                $output = stripslashes($input);
                $output = str_replace("'", "`", $output);
                $output = str_replace("/", "-", $output);
                return $output;
            }
            ?>
        </div>
    </body>
</html>