<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
//connect to MySQL
if (isset($_GET['partid'])){
    $_SESSION['partid'] = $_GET['partid'];
}
$partid = $_SESSION['partid'];
 
//include( dirname(__FILE__) . '/../../connect.php');
$query = "SELECT parts.*, b.firstname, b.surname, parts.id as id 
            from parts 
            left join borwrs b on b.id = parts.borcode WHERE parts.id = " . $partid;
$conn = pg_connect($_SESSION['connect_str']);
$result_part = pg_Exec($conn, $query);
$numrows = pg_numrows($result_part);
$status_txt = Null;

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_part, $ri);

    $longname = $row['surname'] . ' ' . $row['firstname'];
    $datepart = $row['datepart'];
    $itemno = $row['itemno'];
    $parttype = $row['type'];
    $description = $row['description'];
    $found = $row['found'];
    $qty = $row['qty'];
    $borid = $row['borcode'];
    $alert = $row['alertuser'];
    $paid = $row['paid'];
    if ($row['cost'] == Null){
        $cost = 0;
    }else{
        $cost = $row['cost'];
    }
    //$cost = $row['cost'];

    pg_FreeResult($result_part);
// Close the connection
    pg_Close($conn);
    $alert_txt = null;
    if ($alert == 't') {
        $alert_txt .= 'CHECKED';
    } else {
        $alert_txt .= '';
    }
        $paid_txt = null;
    if ($paid == 't') {
        $paid_txt .= 'CHECKED';
    } else {
        $paid_txt .= '';
    }

}


//echo 'Type Claim: ' . $type_claim;
//$date_submitted = $date_submitted[weekday]. $date_submitted[month] . $date_submitted[mday] . $date_submitted[year];
?>