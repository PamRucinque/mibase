<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');


$query = "SELECT id, firstname, surname FROM borwrs ORDER by surname ASC;";
$result = pg_Exec($conn, $query);

//choose the database

//get data from database
include( dirname(__FILE__) . '/get_member_part.php');

echo "<select name='borid_part' id='borid_part' style='width: 250px' onchange='this.form.submit()'>\n";

$numrows = pg_numrows($result);
//echo '<option value="" selected="selected"></option>';
if ($borid == ''){
    $borid = 0;
}

echo "<option value=" . $borid . " selected='selected'>" . $longname . "</option>";

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    echo "<option value='{$row['id']}'>{$row['surname']} : {$row['firstname']}   </option>\n";
}


echo "</select>\n";


?>

