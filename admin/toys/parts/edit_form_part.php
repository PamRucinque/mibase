<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script type="text/javascript" src="../../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "yy-m-d",
            showOtherMonths: true

        };
        $("#datepart").datepicker(pickerOpts);
    });
    function update_type(str) {

        if ((str === 'Found') || (str === 'Alert')) {
            document.getElementById("alert").checked = true;
        }else{
            document.getElementById("alert").checked = false;
        }
    }

</script>

<p><font size="2" face="Arial, Helvetica, sans-serif"></font></p>

<font></font>
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'edit_part.php?idcat=' . $_SESSION['idcat']; ?>">

    <div id="form" style="background-color:lightgray;" align="left">
        <br><table align="top" width="100%"><tr>

                <td><h2>Edit a Part:</h2></td>
                <td align="right"><input id="saveForm" class="button1"  type="submit" name="submit" value="Save" /></td>
            </tr></table>

        <table><tr>
                <td>Missing Part Details:<br><input type="Text" name="description" align="LEFT"  required size="50" value="<?php echo $description ?>"></input><br></td>
            </tr>
            <tr>
                <td>Date: <br><input type="text" name="datepart" id ="datepart" align="LEFT" value="<?php echo $datepart ?>"></input><br></td>
            </tr>
            <tr><td>Type Part: <br><?php include( dirname(__FILE__) . '/get_part_type.php'); ?></td></tr>
            <tr>
                <td>cost: <br><input type="number" name="cost" id ="cost" align="LEFT" value="<?php echo $cost; ?>"></input></td>
            </tr>
            <tr><td>Member id: <br><?php include( dirname(__FILE__) . '/get_member_edit.php'); ?></td>
                <td><input id="alert" type="checkbox" name="alert" <?php echo $alert_txt; ?> > Alert User<br></td>
            </tr>
            <tr><td colspan="2"><br>
                <input id="paid" type="checkbox" name="paid" <?php echo $paid_txt; ?> > Paid deposit for missing part: <br>
                </td></tr>



            <input type="hidden" name="partid" value="<?php echo $partid; ?>"> 
        </table></br>
    </div>
</form>

