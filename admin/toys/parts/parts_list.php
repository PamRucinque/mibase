<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');


//echo $query;
$query = "select parts.*,
typepart.picture as warning
 from parts 
 LEFT JOIN typepart on (parts.type = typepart.typepart) 
    where upper(itemno) = '" . strtoupper($_SESSION['idcat']) . "' ORDER by typepart.picture desc, parts.type, parts.datepart";
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);


if ($numrows > 0) {
//echo '<h2> Missing Parts: </h2>';
    echo '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey">';
}
$page_break = 'No';

for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);
    if ($ri == 0 && $row['warning'] == '') {
        echo '<tr><td colspan="8"><font color="red"><strong>Missing</font></strong></td></tr>';
        echo '<tr><td>id</td><td>Date</td><td>type</td><td>alert</td><td>Paid</td><td>member</td></tr>';
    }


    
    $format_datepart = substr($row['datepart'], 8, 2) . '-' . substr($row['datepart'], 5, 2) . '-' . substr($row['datepart'], 0, 4);
    $alert_txt = null;
    if ($row['alertuser'] == 't') {
        $alert_txt .= 'Yes';
    } else {
        $alert_txt .= 'No';
    }
    $paid_txt = null;
    if ($row['paid'] == 't') {
        $paid_txt .= 'Yes';
    } else {
        $paid_txt .= 'No';
    }
    if (($row['warning'] != '') && ($page_break == 'No')) {
        echo '<tr><td colspan="8"><font color="red"><strong>Warnings</font></strong></td></tr>';
        echo '<tr><td>id</td><td>Date</td><td>type</td><td>alert</td><td>Paid</td><td>member</td></tr>';
        $page_break = 'Yes';
    }
    echo '<tr><td width="30" align="center">' . $row['id'] . '</td>';
    echo '<td width="30">' . $format_datepart . '</td>';
//echo '<td width="250" align="left">' . $row['description'] . '</td>';
    echo '<td width="30" align="left">' . $row['type'] . '</td>';
    echo '<td width="20" align="center">' . $alert_txt . '</td>';
    echo '<td width="20" align="center">' . $paid_txt . '</td>';

//echo '<td width="50" align="center">' . $row['itemno'] . '</td>';
    $ref = '../parts/edit_part.php?partid=' . $row['id'];
    $ref2 = '../parts/delete_part.php?partid=' . $row['id'] . '&&idcat=' . $row['itemno'];
    $ref_bor = '../../members/update/member_detail.php?borid=' . $row['borcode'];
    echo "<td width='50'><a class ='button_small_green' href='" . $ref_bor . "'>" . $row['borcode'] . "</a></td>";
    echo "<td width='50'><a class ='button_small' href='" . $ref . "'>Edit</a></td>";
    echo "<td width='50'><a class ='button_small_red' href='" . $ref2 . "'>Delete</a></td>";
    if ($row['description'] != '') {
        echo '<tr style="height:30px;"><td colspan="8" valign="top"><font color="blue">' . $row['id'] . ': ' . $row['description'] . '</font></td></tr>';
    }
//echo '<td width="50" align="center">' . $download2 . '</td>';
//echo '<td width="100">' . $row['man_html'] . '</td>';
    echo '</tr>';
}
if ($numrows > 0) {
    echo '<tr height="0"></tr>';
    echo '</table>';
}


pg_close($conn);
?>

</body>


