<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <title>Mibase - Edit Home Details</title>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?>

    </head>



    <body id="main_body" >
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');

            include( dirname(__FILE__) . '/get_part.php');
            $_SESSION['idcat'] = $itemno;
            //include( dirname(__FILE__) . '/toy_detail.php');

            if (isset($_POST['submit'])) {
                if ((isset($_POST['alert']))&&($_POST['alert'] == 'on')) {
                    $alert_result = 'TRUE';
                } else {
                    $alert_result = 'FALSE';
                }
                if ((isset($_POST['paid']))&&($_POST['paid'] == 'on')) {
                    $paid_result = 'TRUE';
                } else {
                    $paid_result = 'FALSE';
                }
                if ($_POST['borid_part'] == null) {
                    $_POST['borid_part'] = 0;
                }
                $description = pg_escape_string($_POST['description']);
                $parttype = pg_escape_string($_POST['parttype']);
                $description = clean($description);

                //include( dirname(__FILE__) . '/../../connect.php');
                $query = "UPDATE parts SET
                    description = '{$description}',
                    type = '{$_POST['parttype']}',
                    datepart = '{$_POST['datepart']}',
                    borcode = {$_POST['borid_part']},
                    cost = {$_POST['cost']},
                    paid = $paid_result,
                    alertuser = $alert_result
                    WHERE id=" . $_POST['partid'] . ";";
                 $conn = pg_connect($_SESSION['connect_str']);
                $result = pg_Exec($conn, $query);
                //echo $query;
                //echo $connection_str;
                if (!$result) {
                    echo "An INSERT query error occurred.\n";
                    echo $query;
                    //exit;
                } else {
    
                    $ref = '../update/toy_detail.php?idcat=' . $_GET['idcat'];
                    $ref2 = '../parts/parts.php';

                    echo '<br>This part has been successfully saved<br>';

                    echo "<br><a href='" . $ref . "' class ='button1'>Back to Toy Detail</a>";
                    echo "<a href='" . $ref2 . "' class ='button1'>Back to Missing Parts</a>";
                    $redirect = 'Location:toy_detail.php?idcat=' . $_SESSION['idcat'];

                }

            } else {
                include( dirname(__FILE__) . '/get_part.php');
                include( dirname(__FILE__) . '/edit_form_part.php');
            }

            function clean($input) {
                $output = stripslashes($input);
                $output = str_replace("'", "`", $output);
                $output = str_replace("/", "-", $output);
                return $output;
            }
            ?>
        </div>
    </body>
</html>