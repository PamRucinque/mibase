<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//include('../connect.php');
$sql = "SELECT * from toys order by idcat;";
$result = pg_Exec($conn, $sql);
$numrows = pg_numrows($result);
$last_idcat = '';
$count = 0;

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    $desc1 = $row['desc1'];
    $idcat = $row['idcat'];
    update_toys($idcat, $desc1);
}

function update_toys($idcat, $desc1) {
    //include('../connect.php');
    $desc1 = str_replace("'", "`", $desc1);
    $desc1 = str_replace(";", "\r\n", $desc1);
    $sql_update = "update toys set desc1 = '" . $desc1 . "' where idcat = '" . $idcat . "';";
    $result = pg_Exec($conn, $sql_update);
    echo $sql_update;
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

