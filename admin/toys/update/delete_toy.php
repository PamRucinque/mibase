<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include('../../header.php'); ?> 
    </head>



    <body id="main_body" >
        <div id="form_container">
            <?php
            include('../../menu.php');
            //include("../../get_settings.php");
            $idcat = $_GET['idcat'];
            $_SESSION['idcat'] = $idcat;
            $ref = 'toy_detail.php?idcat=' . $_SESSION['idcat'];
//echo "<td width='50'><a class ='button1' href='" . $ref  . "'>Edit</a></td>";
            echo "<h2><br><a href='" . $ref . "' class ='button1_logout'>Cancel</a>";
            $idcat = $_GET['idcat'];
            $_SESSION['idcat'] = $idcat;
            include('get_toy_single.php');

            $loans = check_loans($idcat);
            //echo 'no on loan: ' . $loans . ' <br>';
            if ($loans == 0) {
                echo '<font color="red"> WARNING - YOU ARE ABOUT TO DELETE TOY:  </font><font color="blue">' . $idcat . ': ' . $toyname . '</font></h1><br>';
                include('delete_form.php');
            } else {
                echo '<h2><font color="red">This Toy is on loan and cannot be deleted</font></h2>';
            }
            //include('toy_detail.php');
            if ($password == '') {
                $password = 'mibase';
            }


            if (isset($_POST['submit'])) {

                if (isset($_GET['idcat'])) {
                    $query = "DELETE FROM parts WHERE itemno = '" . $_POST['idcat'] . "';";
                    $query .= "DELETE FROM toys WHERE idcat = '" . $_POST['idcat'] . "';";
                    $query .= "DELETE FROM transaction WHERE idcat = '" . $_POST['idcat'] . "' and return is not null;";
                    $idcat = $_GET['idcat'];
                    if (($_POST['password'] == $password)) {
                        $result = pg_exec($conn, $query);
                        if (!$result) {
                            echo "An error has occurred. An email message has been sent to mibase.<br>";
                            $message = 'Whole query: ' . $query . '<br>';
                            $email_to = 'michelle@mibase.com.au';
                            $email_subject = 'Error on Alert Member: ' . $_SESSION['library_code'];
                            $headers = "From: " . "MIBASE ERROR" . "<" . $email_to . ">\nReply-To: " . $email_to . "\n";
                            $headers .= "MIME-Version: 1.0\r\n";
                            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                            @mail($email_to, $email_subject, $message, $headers);
                            //exit;
                        } else {
                            echo "<br><p>The record was successfully deleted.</p><br>";
                            echo '<a class="button1" href="../toys.php">OK</a>';
                        }

                        pg_FreeResult($result);
                        pg_Close($conn);
                    } else {
                        echo "<br><p>Incorrect Password.</p><br>";
                        echo '<a class="button1" href="../toys.php">OK</a>';
                    }
                }
            }
            ?>
        </div>
    </body>
</html>

<?php

function check_loans($idcat) {
    //include('../../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "select * from transaction where idcat='" . $idcat . "' and return is null;";
    $result = pg_Exec($conn, $sql);
    $numrows = pg_numrows($result);
    //$trans = 4;
    return $numrows;
}
