<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');


//get configuration data
//include(__DIR__ . '/../../config.php');

//$storage_label = $_SESSION['settings']['storage_label'];
$storage_label = 'Storage Container';
$toys_str = '';

$user_toys = $_SESSION['settings']['user_toys'];
$desc1r = str_replace("\r", "", $desc1);
$desc1 = str_replace("\n", "</br>", $desc1r);
$desc2r = str_replace("\r", "", $desc2);
$desc2 = str_replace("\n", "</br>", $desc2r);
$_SESSION['reserve_error'] = '';
$idcat = $_SESSION['idcat'];

$libraryname = $_SESSION['settings']['libraryname'];


if ($_SESSION['shared_server']) {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    } else {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
    }
} else {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
    } else {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
    }
}


$pic_img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';
?>
<div  style="padding-left: 10px; padding-right: 10px;">
    <br>
    <table width="100%"><tr>
            <td width="80%"><h1><font color="blue"><?php echo strtoupper($idcat) . ': ' . $toyname; ?></font></h1></td>
            <td align="right"><?php
               // echo '<a href="../toy/edit.php?idcat=' . $_SESSION['idcat'] . '" class="button1">Edit</a>';
               // echo '  <a href="delete_toy.php?idcat=' . $_SESSION['idcat'] . '" class="button1_logout">Delete</a>';
                ?></td>
        </tr>
    </table>


    <table width="100%"><tr><td valign='top' style="padding-right: 10px;width:65%;">

                <?php
                //echo $toy_holds;
                if (($toy_holds > 0) && ($_SESSION['mibase_server'] == 'Yes')) {
                    $toys_str = '<h1><font color="#330066">ON HOLD</font></h1>';
                } else {
                    if ($trans_due != '') {

                        include('trans_detail.php');
                    } else {
                        $toys_str = '<h1><font color="#330066">IN LIBRARY</font></h1>';
                    }
                }
                echo $toys_str . '<br>';


                echo '<br><table><tr><td valign="top" width="50%">';
                echo '<a>Toy Number: </a>' . $idcat;
                if (($_SESSION['library_code'] == 'exmouth') && ($_POST['category'] != $category)) {
                    include('../toy/edit_idcat.php');
                    echo $_SESSION['idcat_change'];
                }
                if ($reservecode != '' || $reservecode != null) {

                    echo '<br><a>This Toy Can Be reserved.</a>';
                }
                echo '<br><a>Toy Status: </a>' . $toy_status;
                if ($toy_status == 'LOCKED' || $toy_status == 'WITHDRAWN') {
                    echo '<br><a>Lock Date: </a>' . $lockdate;
                    echo '<br><a>Lock Reason: </a>' . $lockreason;
                }
                echo '<br><a>Category: </a>' . $category;
                if ($sub_category != '') {
                    echo '<a>Sub: </a>' . $sub_category;
                }
                echo format_str($loan_type, 'Loan Type');
                echo '<br><a>No Pieces: </a>' . $no_pieces;
                echo '<br><a>Rent: </a>' . $rent;
                echo format_str($date_purchase, 'Date Purchased');
                echo format_str($age, 'Age Range');
                echo format_str($condition, 'Condition');
                echo format_str($storage, $storage_label);
                echo format_str($manufacturer, 'Manufacturer');
                echo format_str($supplier, 'Supplier');
                if ($donated_label == '') {
                    $donated_label = 'Donated By';
                }
                echo format_str($sponsorname, $donated_label);
                echo format_str($discountcost, 'Cost');
                echo format_str($cost, 'Replacement Cost');
                echo format_str($returndateperiod, 'Return Period (days)');
                echo format_str($freight, 'Freight');
                echo format_str($package, 'Packaging Cost');
                echo format_str($process_time, 'Processing Time (minutes)');
                echo format_str($days_out, 'Days On Loan in the last Year');
                echo format_str($changedby, 'Changed by User');
                echo format_str($format_st, 'Last checked/locked');
                echo format_str($modified, 'Last Updated');
                if ($product_link != '') {
                    echo '<a>Product Link: </a><a  target="_blank" class="button_small_yellow" href="' . $product_link . '">More...</a>';
                }
                echo format_str($keywords, 'Keywords');


                if ($user_toys == '') {
                    $user_toys = 'User1';
                }
                echo format_str($user1, $user_toys);
                echo '</td>';
                echo '<td valign="top">';
                if ($desc1 != null || $desc1 != '' || $desc2 != '' || $desc2 != null) {
                    echo '<a>Toy Description:</a><br>';
                    echo $desc1 . '<br>' . $desc2;
                }



                echo '</td></tr></table>';
                echo '<font color="red">' . format_str($warnings, 'WARNINGS') . '</font>';
                echo format_str($comments, 'Comments');
                echo format_str($alerts, 'Alerts');
                ?>
            </td>

            <td  valign='top'>
                <?php
                //echo '<table><tr><td>';
                //include('../../reports/baglabels.php');
                // echo '</td><td valign="bottom">';
                // echo '</td></tr></table>';

                echo '<br>' . $pic_img . '<br>';


                if (file_exists($file_pic)) {
                    include ('delete_pic.php');
                }
                echo '<table width="100%"><tr><td width="70%"><h1><font color="#330066">Missing Parts </font></h1></td>';
                echo '<td><a href="../parts/new_part.php?idcat=' . $idcat . '" class="button1">New Part</a></td>';
                echo '</tr></table>';
                include('parts/part_form.php');
                include('../parts/parts_list.php');
//
                ?>
            </td>
        </tr>
    </table>
    <table width="70%"><tr><td>
                <?php include('../history/hist_list.php'); ?>
            </td></tr></table>
</div>


<?php

Function format_str($field, $heading) {
    $str = '';
    if ($field != '' || $field != null) {
        $str = '<br><a>' . $heading . ': </a>' . $field;
    }
    if (trim($field) == '--') {
        $str = '';
    }
    return $str;
}
?>


