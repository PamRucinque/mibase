<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php
        include('../../header.php');
        include ('../../functions/global.php');
        ?> 
    </head>    

    <body id="main_body"  onload="setFocus()">
        <div class="container-fluid">
            <?php
            include('../../menu.php');

            if (isset($_POST['idcat'])) {
                $_SESSION['idcat'] = $_POST['idcat'];
                $idcat = $_SESSION['idcat'];
            }
            if (isset($_POST['typepart'])) {
                include('parts/new_part.php');
            }
            $_SESSION['idcat_change'] = '';
            if (isset($_POST['change_idcat'])) {
                $_SESSION['idcat_change'] = update_idcat($_POST['toy_id'], $_POST['category']);
            }
  

            if (isset($_GET['idcat'])) {
                //$_GET['idcat'] = mibase_validate($_GET['idcat']);
                //$_GET['idcat'] = mibase_validate($_GET['idcat'], $_SESSION['myusername'], $_SESSION['mypassword'], 'idcat');
                $_SESSION['idcat'] = $_GET['idcat'];
                //echo 'get idcat: ' . $_SESSION['idcat'];
                $idcat = $_SESSION['idcat'];
            }
            $donated_label = '';
            //include('../../connect.php');
            include('../../header_detail/header_detail.php');
            //include('../../get_settings.php');

            


            include('get_toy.php');
            $find_newtoy = 'No';
            //include('toy_select.php');
            //include('../toy/get_settings.php');
            include('toy_detail_print.php');

            function update_idcat($id, $category) {
                $idcat = '';
                //include('../../connect.php');
                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $sql = "update toys set idcat = ? where id = ? and category = ?;";
                $sth = $pdo->prepare($sql);
                $str = (string) $id;
                $idcat = $category . $str;
                $array = array($idcat, $id, $category);

                $sth->execute($array);
                $stherr = $sth->errorInfo();
                if ($stherr[0] != '00000') {
                    $output = 'Error ' . $stherr[0] . '<br>';
                    $output .= 'Error ' . $stherr[1] . '<br>';
                    $output .= 'Error ' . $stherr[2] . '<br>';
                    exit;
                } else {
                    $output = 'Changed to: ' . $idcat;
                }
            }

                if (isset($_GET['idcat'])) {
                    //$_GET['idcat'] = mibase_validate($_GET['idcat']);
                    //$_GET['idcat'] = mibase_validate($_GET['idcat'], $_SESSION['myusername'], $_SESSION['mypassword'], 'idcat');
                    $_SESSION['idcat'] = $_GET['idcat'];
                    //echo 'get idcat: ' . $_SESSION['idcat'];
                    $idcat = $_SESSION['idcat'];
                }
?>
            </div>
      
    </body>

    