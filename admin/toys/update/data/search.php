<?php
$branch = substr(getcwd(),22,strpos(getcwd() . '/','/', 22+1) - 22);
include(dirname(__FILE__) . '/../../mibase_check_login.php');

if (isset($_GET['term'])) {
    $return_arr = array();

    //include('../../connect.php');
    $term = '%' . strtoupper($_REQUEST['term']) . '%';

    $sql = "SELECT idcat, toyname FROM toys WHERE ((upper(idcat) LIKE ? OR upper(user1) LIKE ? OR (upper(toyname) LIKE ?) OR (upper(toyname) LIKE ? 
            )) AND (toy_status='ACTIVE' OR 
            toy_status ='PROCESSING')) ORDER by category, id ASC;";


    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);

    $sth = $pdo->prepare($sql);
    $array = array($term,$term, $term, $term);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();

 
//echo '<option value="" selected="selected"></option>';
    $q = strtolower($_GET["term"]);
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        $return_arr[] = $row['idcat'] . ': ' . $row['toyname'];
        $results[] = array('label' => $row['idcat'] . ': ' . $row['toyname'], 'value' => $row['idcat']);
    }

    /* Toss back results as json encoded array. */
    echo json_encode($results);
}
?>