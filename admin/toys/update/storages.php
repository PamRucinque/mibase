<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include('../../connect.php');

$query = "SELECT * from storage ORDER by storage";
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);


if ($numrows > 0) {
echo '<h2> Toy Storage Options: </h2>';
echo '<table border="1" width="60%" style="border-collapse:collapse; border-color:grey">';
echo '<tr style="color:green"><td>id</td><td>Storage</td><tr>';

}


for ($ri = 0; $ri < $numrows;$ri++) {
//echo "<tr>\n";
$row = pg_fetch_array($result, $ri);


echo '<td width="10" align="center">' . $row['id'] . '</td>';
echo '<td width="30">' . $row['storage'] . '</td>';
$ref2 = 'delete_storage.php?id=' . $row['id'];
echo "<td width='50' align='center'><a class ='button_small' href='" . $ref2 . "'>Delete</a></td>";
//echo '<td width="50" align="center">' . $download2 . '</td>';
//echo '<td width="100">' . $row['man_html'] . '</td>';
echo '</tr>';

}
echo '<tr height="0"></tr>';
echo '</table>';



pg_close($conn);
//include('new_category.php');
?>

</body>


