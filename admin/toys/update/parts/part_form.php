<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../../mibase_check_login.php');
$table_parts = '';
?>
<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "yy-mm-dd",
            showOtherMonths: true

        };
        $("#date_alert").datepicker(pickerOpts);
    });

</script>
<table bgcolor="lightyellow" width ="90%">
    <tr><td colspan = "3"></td></tr>
    <tr>

        <td width ="5%"></td>
        <td valign ="top">

            <form  id="alert_part" method="post" action="" width="100%">
                <table><tr><td>Warnings - Quick Insert</td><tr>
                    <tr>
                        <td><?php include('part_type.php'); ?></td>
                        <td><input id="submit_part" class="button_small_yellow"  type="submit" name="submit_part" value="Save Part" /></td>
                    </tr></table>

            </form> 
            <?php echo $table_parts; ?>
        </td>
        <td width="10px"></td>

    </tr></table>

