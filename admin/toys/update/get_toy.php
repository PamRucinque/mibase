<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
//connect to MySQL

//include('../../connect.php');

$_SESSION['idcat'] = strtoupper($_SESSION['idcat']);

$query = "SELECT toys.*, 
toys.id as toyid,
(select sum(return::timestamp - date_loan::timestamp)
from transaction where date_loan > current_date - interval '1 year' 
and toys.idcat = transaction.idcat and return is not null) as days_out, 
transaction.due as due,
 (select count(id) as onhold from toy_holds where status = 'ACTIVE' and toy_holds.idcat = transaction.idcat) as holds 
from toys 
LEFT  JOIN transaction on (toys.idcat = transaction.idcat and return is null)   
WHERE upper(toys.idcat) = '" . strtoupper($_SESSION["idcat"]) . "' LIMIT 1";

$conn = pg_connect($_SESSION['connect_str']);
//echo $query;

$result_toy = pg_Exec($conn, $query);
$numrows = pg_numrows($result_toy);
$status_txt = Null;


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_toy, $ri);
    $toy_id = $row['toyid'];
    //$idcat = $row['idcat'];
    $days_out = $row['days_out'];
    $desc1 = $row['desc1'];
    $desc2 = $row['desc2'];
    //$desc3 = $row['desc3'];
    $category = $row['category'];
    $reservecode = $row['reservecode'];
    $toyname = $row['toyname'];
    $rent = $row['rent'];
    $age = $row['age'];
    $datestocktake = $row['datestocktake'];
    $manufacturer = $row['manufacturer'];
    $discountcost = $row['discountcost'];
    $status = $row['status'];
    $cost = $row['cost'];
    $supplier = $row['supplier'];
    $comments = $row['comments'];
    $returndateperiod = $row['returndateperiod'];
    $returndate = $row['returndate'];
    $no_pieces = $row['no_pieces'];
    $date_purchase = $row['date_purchase'];
    $sponsorname = $row['sponsorname'];
    $warnings = $row['warnings'];
    $toy_holds = $row['holds'];
    $user1 = $row['user1'];
    //$date_entered = date_format($row['date_entered'], 'd/m/y');
    $alert = $row['alert'];
    $toy_status = $row['toy_status'];
    $freight = $row['freight'];
    $stype = $row['stype'];
    $loan_type = $row['loan_type'];
    $units = $row['units'];
    $borrower = $row['borrower'];
    $lockdate = $row['datestocktake'];
    $alerts = $row['alert'];
    $lockreason = $row['lockreason'];
    $created = $row['created'];
    $modified = $row['modified'];
    $condition = $row['reservedfor'];
    $storage = $row['storage'];
    $trans_due = $row['due'];
    //$start = $row['start'];
    //$end = $row['end'];
    //$borid_reserve = $row['borid_reserve'];
    //$reserve_status = $row['reserve_status'];
    $location = $row['location'];
    $package = $row['packaging_cost'];
    $process_time = $row['process_time'];
    $history = $row['history'];
    $changedby = $row['changedby'];
    $keywords = $row['twitter'];
    $product_link = $row['link'];
    $sub_category = $row['sub_category'];

    $format_purchase = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    $format_st = substr($row['datestocktake'], 8, 2) . '-' . substr($row['datestocktake'], 5, 2) . '-' . substr($row['datestocktake'], 0, 4);

    $format_lock = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    if ($cost == Null) {
        $cost = 0;
    }
    if ($discountcost == Null) {
        $discountcost = 0;
    }
    if ($rent == Null) {
        $rent = 0;
    }
    pg_FreeResult($result_toy);
// Close the connection
    pg_Close($conn);
    if (!session_id()) {
        session_start();
    }
    
}


//echo 'Type Claim: ' . $type_claim;
//$date_submitted = $date_submitted[weekday]. $date_submitted[month] . $date_submitted[mday] . $date_submitted[year];
?>
