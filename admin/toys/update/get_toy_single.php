<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include('../../connect.php');
$status_txt = '';
$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$sql = "SELECT * from toys WHERE upper(toys.idcat) = ?";

$sth = $pdo->prepare($sql);
$array = array($_SESSION['idcat']);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = $result[$ri];
    $toy_id = $row['id'];
    $idcat = $row['idcat'];
    $desc1 = $row['desc1'];
    $desc2 = $row['desc2'];
    $category = $row['category'];
    $reservecode = $row['reservecode'];
    $toyname = $row['toyname'];
    $rent = $row['rent'];
    $age = $row['age'];
    $datestocktake = $row['datestocktake'];
    $manufacturer = $row['manufacturer'];
    $discountcost = $row['discountcost'];
    $status = $row['status'];
    $cost = $row['cost'];
    $supplier = $row['supplier'];
    $comments = $row['comments'];
    $freight = $row['freight'];
    $returndateperiod = $row['returndateperiod'];
    $returndate = $row['returndate'];
    $no_pieces = $row['no_pieces'];
    $date_purchase = $row['date_purchase'];
    $sponsorname = $row['sponsorname'];
    $warnings = $row['warnings'];
    $user1 = $row['user1'];
    $package = $row['packaging_cost'];
    $process_time = $row['process_time'];
    //$date_entered = date_format($row['date_entered'], 'd/m/y');
    $alert = $row['alert'];
    $toy_status = $row['toy_status'];
    $stype = $row['stype'];
    $units = $row['units'];
    $borrower = $row['borrower'];
    $lockdate = $row['datestocktake'];
    $alerts = $row['alert'];
    $user1 = $row['user1'];
    $warnings = $row['warnings'];
    $lockreason = $row['lockreason'];
    $created = $row['created'];
    $modified = $row['modified'];
    $condition = $row['reservedfor'];
    $storage = $row['storage'];
    $changedby = $row['changedby'];
    $keywords = $row['twitter'];
    $product_link = $row['link'];
    $sub_category = $row['sub_category'];


    $format_purchase = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    $format_lock = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    if ($cost == Null) {
        $cost = 0;
    }
    if ($discountcost == Null) {
        $discountcost = 0;
    }
    if ($rent == Null) {
        $rent = 0;
    }
    //session_start();
    $_SESSION['idcat'] = $idcat;

    if ($status == 't') {
        $status_txt .= 'ON LOAN';
    } else {
        $status_txt .= 'IN LIBRARY';
    }
}

if ($stherr[0] != '00000') {
    $error_msg .= "An  error occurred.\n";
    $error_msg .= 'Error' . $stherr[0] . '<br>';
    $error_msg .= 'Error' . $stherr[1] . '<br>';
    $error_msg .= 'Error' . $stherr[2] . '<br>';
}
//echo $error_msg . '<br>';

$status_txt = Null;
?>