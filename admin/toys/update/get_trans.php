<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//connect to MySQL
//include('../../connect.php');
if (!isset($_SESSION['idcat'])){
    $_SESSION['idcat'] = $_GET['idcat'];
} 
$conn = pg_connect($_SESSION['connect_str']);
$query_trans = "SELECT * from transaction WHERE idcat = '". $_SESSION['idcat'] . "' and return is null";
$result_trans = pg_Exec($conn, $query_trans);
$numrows = pg_numrows($result_trans);
//echo $query_trans;

for ($ri = 0; $ri < $numrows; $ri++) {
       $row = pg_fetch_array($result_trans, $ri);


    $trans_id = $row['id'];
    $bor_id = $row['borid'];
    $borname = $row['borname'];
    $location = $row['location'];
    $created_trans = $row['created'];
    $due = $row['due'];
    
     pg_FreeResult($result_trans);
// Close the connection
            pg_Close($conn);
            

}
  $format_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);
  $format_trans = substr($row['created'], 8, 2) . '-' . substr($row['created'], 5, 2) . '-' . substr($row['created'], 0, 4);


//echo 'Type Claim: ' . $type_claim;
 //$date_submitted = $date_submitted[weekday]. $date_submitted[month] . $date_submitted[mday] . $date_submitted[year];

?>