<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script src="../../js/jquery-2.0.3.min.js"></script>
<script src="../../js/jquery-ui.js"></script>
<script>
    function setFocus()
    {
        document.getElementById("scanid").focus();
        if (document.getElementById("focus").value === null) {
            alert(document.getElementById("focus").value);
        }

    }

</script>
<script type="text/javascript">
    $(function () {

        //autocomplete
        $(".auto").autocomplete({
            source: "search.php",
            autoFocus: true,
            select: function (event, ui) {
                //For better understanding kindly alert the below commented code
                //alert(ui.toSource()); 
                var selectedObj = ui.item;

                //alert(selectedObj.value);
                document.getElementById('idcat').value = selectedObj.value
                document.forms["change_toy"].submit();
            }

        });

    });
</script>

<div class="row" style="padding-top: 20px;">
    <div class="col-sm-2" style="max-width: 100px;">
        <form  id="scan" method="post" action="toy_detail.php" width="100%">
            <input align="center" type="text"   placeholder="Scan Toy" name="scanid" id ="scanid" style="max-width: 90px; background-color: lightgoldenrodyellow;" value="" onchange='this.form.submit()'></input>
        </form>      
    </div>
    <div class="col-sm-6">
        <form id="change_toy" method="post" action="toy_detail.php">

            <input type='text' style="background-color: lightgoldenrodyellow;" name='toyid' id='toyid' value='' class='auto' onKeyPress="login()" placeholder="Search Toy"></p>
            <input type="hidden" id="idcat" name ="idcat" />
        </form>
    </div>
    <div class="col-sm-2">
        <?php include('../../reports/baglabels.php'); ?>
    </div>
    
</div>

