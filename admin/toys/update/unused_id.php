<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

$category = $_SESSION['category'];
if ($_SESSION['catsort'] == 'Yes') {
    $query_unused = "select * from generate_series(" . $id . ", (SELECT MAX(id) FROM toys where 
    category = '" . $category . "')) as unused
  where unused not in (select id from toys Where category = '" . $category . "');";
    //echo $query_unused . '<br>';
} else {
    $query_unused = "select * from generate_series(" . $id . ", (SELECT MAX(id) FROM toys)) as unused
  where unused not in (select id from toys);";
    //echo 'Lowest 20 unused numbers:  ';
}

//echo $query_unused;


//include('../../connect.php');
////include('../../get_settings.php');

$result = pg_Exec($conn, $query_unused);
//echo $connection_str;

$numrows_count = pg_numrows($result);
$numrows = 20;


if ($numrows_count > 0) {
    if ($_SESSION['catsort'] == 'Yes') {
        $result_txt = '<br><font color="blue">Lowest unused 20 numbers from ' . $id . ' in Category ' . $category . '</font>  ';
    } else {
        $result_txt = '<br><font color="blue">Lowest unused 20 numbers from ' . $id . ': ';
    }
    for ($ri = 0; $ri < $numrows; $ri++) {

        $row = pg_fetch_array($result, $ri);
        if ($_SESSION['catsort'] == 'Yes') {
            $toyid = strtoupper($category) . $row["unused"];
        } else {
            $toyid = $row["unused"];
        }
        if(check_idcat($toyid) == 0 && !($row["unused"] == null)){
            $result_txt .= $toyid . '  ';
        }
        
    }
}
$result_txt .= '</font><br>';
echo $result_txt;
echo $result[0];




?>
