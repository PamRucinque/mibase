<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
//include('../toy/get_settings.php')
?>
<link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "d MM yy",
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "1990:+nn"

        };
        $("#date_purchase").datepicker(pickerOpts);
        $("#lockdate").datepicker(pickerOpts);
    });



    function validate(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault)
                theEvent.preventDefault();
        }
    }

</script>
<form  method="post" action="<?php echo 'new.php?idcat=' . $idcat; ?>">


        <table align="top" width="100%"  bgcolor="lightgray"><tr>
              <td colspan="2">Name of Toy:<br>
                    <input type="Text" name="toyname" align="LEFT"  size="50" value="" required></input>
                    <input disabled="disabled" STYLE="background-color: yellow;" type="Text" name="cat" id="cat" align="center"  size="5" value="<?php echo $category; ?>" required></input><br><br></td>
               <td align="right"><input id="saveForm" class="button1"  type="submit" name="submit" value="Save Toy" /></td>
            </tr>

            <input type="hidden" name="idcat" value="<?php echo $idcat; ?>"> 
            <input type="hidden" name="toyid" id="toyid" min="1" max="8000" value="<?php echo $_SESSION['toyid']; ?>"></input>
            <input type="hidden" name="category" value="<?php echo $_SESSION['category']; ?>"></input>

        </table>

</form>





