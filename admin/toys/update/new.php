<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');

//get used settings
$reuse_toyno = $_SESSION['settings']['reuse_toyno'];
$rent_default = $_SESSION['settings']['rent_default'];




?>
<!doctype html>
<html lang="en">
    <head>
        <?php include('../../header.php'); ?> 
        <script>

            function blinker() {
                $('.blink_me').fadeOut(500);
                $('.blink_me').fadeIn(500);
            }

            setInterval(blinker, 1000);
        </script>
    </head>    


    <?php
    //include('../../get_settings.php');
    include('../functions/functions.php');
    //$first_id = 0;



    $today = date('Y-m-d');

    //$toyid = 0;
    ?>
    <body>
        <div id="form_container">
            <?php
            include('../../menu.php');
            //include('../../connect.php');
            echo '<h2><font color="blue">Add a New Toy</font></h2>';
            $catsort = $_SESSION['settings']['catsort'];
            $loanperiod = $_SESSION['settings']['loanperiod'];
            $reserve_all = $_SESSION['settings']['reserve_all'];

            if (isset($_POST['category'])) {
                $_SESSION['category'] = strtoupper($_POST['category']);
                $_SESSION['toyiderror'] = '';
            }
            if ($_SESSION['category'] != '') {
                include('unused.php');
            }
            if ($_SESSION['category'] == '') {
                echo '<h2><span class="blink_me"><font color="red">Please Select a Category</font></span></h2>';
            }

            include('maxid.php');

            if ($_SESSION['category'] == '') {
                //include('get_category_first.php');
            }
            if (!isset($_POST['toyid']) || $_POST['toyid'] == '') {
                //$newidcat = $first_idcat;
                if ($reuse_toyno == 'Yes') {
                    $_SESSION['toyid'] = $first_id;
                    //echo $first_id;
                } else {
                    $_SESSION['toyid'] = $maxid;
                }
            } else {
                //include('checkid.php');
                ///if ($ok != 'No') {
                    $_SESSION['toyid'] = $_POST['toyid'];
                //}
            }

            //include('unused.php');
            //echo 'Toy id: ' . $_SESSION['toyid'] . ' Category: ' . $_SESSION['category'];
            include('select_cat.php');
            if ($_SESSION['category'] != '') {
                include('new_form.php');
            }
            echo $_SESSION['toyiderror'];


            if ($_SESSION['category'] != '') {
                echo $result_txt;
            }

            //echo '<h1><font color="red">New Toy Page Under Construction</font></h1>';

            if (isset($_POST['submit'])) {
                //$copyid = $_POST['copyid'];
                //include('../../get_settings.php');
                $toyid = $_POST['toyid'];
                $today = date('Y-m-d');

                if ($catsort == 'Yes') {
                    if ($format_toyid == 'Yes') {
                        if ($format_toyid_decimal != '') {
                            $str = '%0' . $format_toyid_decimal . 's';
                            $toyid = sprintf($str, $toyid);
                        } else {
                            $toyid = sprintf('%02s', $toyid);
                        }
                    }
                    $idcat = strtoupper($category) . $toyid;
                } else {
                    $idcat = $toyid;
                }

                //include('../../connect.php');
                //$loan_period = $loanperiod;
                $purchase = date('Y-m-d H:i:s');
                $toyname = clean($_POST['toyname']);
                $condition = 'New';
                $location = $_SESSION['library_code'];
                if ($_SESSION['location'] != ''){
                   $location = $_SESSION['location']; 
                }
                
                $returnperiod = $loanperiod;
                $rent = $rent_default;
                $date_purchase = $today;
                $package = 0;
                $freight = 0;
                $cost = 0;
                $discountcost = 0;
                $time = 0;
                if ($reserve_all == 'Yes'){
                    $reservecode = $idcat;
                }else{
                    $reservecode = null;
                }
                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $query = "INSERT INTO toys (id, idcat, category, toyname, 
                toy_status, status, returndateperiod, date_purchase,  rent, 
                cost, discountcost, reservedfor, 
                packaging_cost, freight, process_time, reservecode, location, changedby)
                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

                $sth = $pdo->prepare($query);

                //create the array of data to pass into the prepared stament
                $array = array($_POST['toyid'], $idcat, $_POST['category'], $toyname,
                    'ACTIVE', 0, $returnperiod, $date_purchase, $rent,
                    $cost, $discountcost, $condition,
                    $package, $freight, $time, $reservecode, $location, $_SESSION['username']);


                $sth->execute($array);
                $stherr = $sth->errorInfo();

                if ($stherr[0] != '00000') {
                    echo "An Error Occured adding this toy, there may be another toy with this number!\n";
                    //echo $query;
                    echo 'Error ' . $stherr[0] . '<br>';
                    echo 'Error ' . $stherr[1] . '<br>';
                    echo 'Error ' . $stherr[2] . '<br>';
                    $edit_url = 'new.php';
                    echo '<a class="button1" href="' . $edit_url . '">Back to New Toy</a>';
                    echo '<a class="button1" href="../toys.php">Toy List</a>';
                    //echo 'Return Period: ' . $returnperiod . '<br>';

                    exit;
                } else {
                    // Get the last record inserted
                    // Print out the Contact ID
                    $url = '../picture/get_picture.php?idcat=' . $idcat;
                    $edit_url = 'toy_detail.php?idcat=' . $idcat;
                    $edit = '../toy/edit.php?idcat=' . $idcat;

                    echo "<br>The record was successfully entered and the ID is:" . $toyid . "<br><br>";
                    //echo 'format Toy id: ' . $format_toyid;
                    echo '<a class="button1" href="../toys.php">Toy List</a>';
                    echo '<a class="button1" href="' . $url . '" target="_blank">Upload Toy picture</a>';
                    echo '<a class="button1" href="' . $edit_url . '">View New Toy</a>';
                    echo '<a class="button1" href="' . $edit . '">Edit New Toy</a>';
                    //include('../toy/edit.php');
                }
            }
            ?>
    </body>

</html>