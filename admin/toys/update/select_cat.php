<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');

//get settings
$format_toyid = $_SESSION['settings']['format_toyid'];
$catsort = $_SESSION['settings']['catsort'];


?>
<br>
<form name="category" id="category" method="post" action="new.php" >    

    <table  bgcolor="lightgray" width="100%"><tr><td width="40%">
        <tr><td>
                <label class="description" for="element_12">Select Category: </label>

                <?php include('get_category.php'); ?>
            </td>

            <td>Change Toy Number: <br>
                <input type="number" name="toyid" min="1" max="5000" value="" onchange='this.form.submit()'></input>

            </td>
            <td><strong><h2><?php
                        $toyid = $_SESSION['toyid'];
 
                        if ($catsort == 'Yes') {
                            if ($format_toyid == 'Yes') {
                                if ($format_toyid_decimal != '') {
                                    $str = '%0' . $format_toyid_decimal . 's';
                                    $toyid = sprintf($str, $toyid);
                                } else {
                                    $toyid = sprintf('%02s', $toyid);
                                }
                            }
                            $idcat = $category . $toyid;
                        } else {
                            $idcat = $toyid;
                        }
                        if ($_SESSION['category'] != ''){
                          echo '<br>New Toy No: <font color="red">' . $idcat;  
                        }

                        
                        ?>
                        </font></h2></strong>

            </td>
        </tr>


    </table>


</form>



