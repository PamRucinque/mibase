<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
//include('../../get_settings.php');
$toyid = $_POST['toyid'];

//include('../../connect.php');
////include('../../get_settings.php');

if ($catsort == 'Yes') {
    $query_check = "SELECT id FROM toys WHERE category = '" . $category . "' and id = " . $toyid . ";";
} else {
    $query_check = "SELECT id FROM toys WHERE id = " . $toyid . ";";
}
$result_rows = pg_exec($conn, $query_check);
$numrows = pg_numrows($result_rows);


if ($numrows == 1) {
    $redirect = 'location: new.php';
    $_SESSION['toyiderror'] = $category . $toyid . '<font color="red"> This number is already being used</font><br>';
    //$_SESSION['toyid'] = $newidcat;
    $ok = 'No';
}else{
    $_SESSION['toyiderror'] = '';
}
?>
