<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

$id = $_GET['id'];
//echo "id:" . $id;
include ('get_toy.php');
$logo = $_SESSION['logo'];

//echo 'user' . $_SESSION['libraryname'];
?>
<br>
<table><tr><td width ="75%">
            <table>
                <tr>
                    <td><strong>Toy Name:</strong></td>
                    <td align ="left"><?php echo $toyname; ?></td></tr>
                <tr>
                    <td><strong>Toy Number:</strong></td>
                    <td align ="left"><?php echo $idcat; ?></td></tr>
                <tr>
                    <td><strong>Category:</strong></td>
                    <td align ="left"><?php echo $category; ?></td></tr>


                <td><strong>Comments:</strong></td>
                <td align ="left"><?php echo $comments; ?></td></tr>
    <tr>
        <td><strong>Toy Status:</strong></td>
        <td align ="left"><?php echo $status_txt; ?></td></tr>
    <tr>
        <td><strong>Last or Current Borrower:</strong></td>
        <td align ="left"><?php echo $borrower; ?></td></tr>


</table>
</td>
<td><?php echo $img; ?></td>
</tr></table>
<?php
echo '<br><a href="toys.php">Back to Toy Search</a>'
?>   
