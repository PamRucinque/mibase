<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');
$catsort = $_SESSION['settings']['catsort'];
$first_id = 0;

//get settings


if(isset($_SESSION['settings']['special_newtoy'])){
$special_newtoy = $_SESSION['settings']['special_newtoy'];
} else {
    $special_newtoy = 'No';
}


if ($special_newtoy == 'Yes') {
    //include('special/get_settings.php');
    $str_function = 'special/' . $_SESSION['library_code'] . '_newtoy.php';
    include($str_function);
} else {
    $category = $_SESSION['category'];
}


if ($catsort == 'Yes') {
    $query_unused = "select * from generate_series(1, COALESCE((SELECT MAX(id) FROM toys where 
    upper(category) = '" . $category . "'),2)+20) as unused
  where unused not in (select id from toys Where category = '" . $category . "');";
    //echo $query_unused . '<br>';
} else {
    $query_unused = "select * from generate_series(1, COALESCE((SELECT MAX(id) FROM toys),2)+20) as unused
  where unused not in (select id from toys);";
    //echo 'Lowest 20 unused numbers:  ';
}

$dbconn = pg_connect($_SESSION['connect_str']);
$result = pg_Exec($dbconn, $query_unused);
//echo $connection_str;

$numrows_count = pg_numrows($result);
$numrows = 20;


if ($numrows_count > 0) {
    if ($catsort == 'Yes') {
        $result_txt = '<font color="blue">Lowest 20 numbers in Category ' . $category . '</font>  ';
    } else {
        $result_txt = 'Lowest 20 numbers: ';
    }
    $count_first = 0;
    for ($ri = 0; $ri < $numrows; $ri++) {

        $row = pg_fetch_array($result, $ri);

        if ($catsort == 'Yes') {
            $toyid = strtoupper($category) . $row["unused"];
        } else {
            $toyid = $row["unused"];
        }
        if ($count_first == 0) {
            $first_idcat = $toyid;
            $first_id = $row["unused"];
        }
        if (check_idcat($toyid) == 0 && !($row["unused"] == null)) {
            $result_txt .= $toyid . '  ';
        }
        $count_first = $count_first + 1;
    }
}

function check_idcat($idcat) {
    //include('../../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "select Count(id) AS countid FROM toys where idcat='" . $idcat . "';";
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $toys = $row['countid'];
    //$trans = 4;
    return $toys;
}

?>
