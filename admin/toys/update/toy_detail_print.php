<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../../mibase_check_login.php');

$logo = $_SESSION['logo'];
$subdomain = $_SESSION['library_code'];
$desc1r = str_replace("\r", "", $desc1);
$desc1 = str_replace("\n", "</br>", $desc1r);
$desc2r = str_replace("\r", "", $desc2);
$desc2 = str_replace("\n", "</br>", $desc2r);
$days_out =100*(int)$days_out/365;
$days_out_str = '<font color="blue">' . money($days_out) . ' %</font>';
$_SESSION['reserve_error'] = '';
$str_col2 = '';

$returnperiod = $_SESSION['settings']['loanperiod'];
$user_toys = $_SESSION['settings']['user_toys'];

$storage_label = $_SESSION['settings']['storage_label'];
if (trim($storage_label) == '') {
    $storage_label = 'Storage Container';
}
$sub_category_label = $_SESSION['settings']['sub_category_label'];
if (trim($sub_category_label) == '') {
    $sub_category_label = 'Sub Category';
}


//$img = '<img height="200px" src="../../../toy_images/' . $subdomain . '/' . strtolower($idcat) . '.jpg" alt=" ">';
//$file_pic = '../../../toy_images/' . $subdomain . '/' . strtolower($idcat) . '.jpg';

if ($_SESSION['shared_server']) {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    } else {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
    }
} else {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
    } else {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
    }
}

$pic_img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';
?>
<div  class="container-fluid">
    <div  class="row">
        <div class="col-sm-12 col-xs-12" style="padding-left: 15px;">
            <?php include('toy_select.php') ?>
        </div>
    </div>
    <div  class="row">
        <div class="col-sm-4 col-xs-12">
            <h1><font color="blue"><?php echo strtoupper($idcat) . ': ' . $toyname; ?></font></h1>
        </div>
        <div class="col-sm-8 col-xs-12">
            <?php
            if ($trans_due != '') {
                include('trans_detail.php');
            } else {
                $link_hold = '<a class="btn btn-danger" href="../../loans/hold/holds.php?idcat=' . $idcat . '">ON HOLD</a>';
                $toys_str = '<h1><font color="#330066">IN LIBRARY  ';
                $toys_str .= '</font></h1>';
                if ($toy_holds > 0) {
                    $toys_str .= ' ' . $link_hold . ' ';
                }
                $toys_str .= '</tr></table>';
                echo $toys_str;
            }
            ?>
        </div>
    </div>

    <?php
    $str_col1 = '<br><strong><font color="darkblue">Toy Number: </font></strong>' . $idcat;
    if (($subdomain == 'exmouth') && ($_POST['category'] != $category)) {
        include('../toy/edit_idcat.php');
        $str_col1 .= $_SESSION['idcat_change'];
    }
    if ($reservecode != '' || $reservecode != null) {
        $str_col1 .= '<br><strong><font color="darkblue">This Toy Can Be reserved.</font></strong>';
    }
    $str_col1 .= '<br><strong><font color="darkblue">Toy Status: </font></strong>' . $toy_status;
    if ($toy_status == 'LOCKED' || $toy_status == 'WITHDRAWN') {
        $str_col1 .= '<br><strong><font color="darkblue">Lock Date: </font></strong>' . $lockdate;
        $str_col1 .= '<br><strong><font color="darkblue">Lock Reason: </font></strong>' . $lockreason;
    }
    $str_col1 .= '<br><strong><font color="darkblue">Category: </font></strong>  ' . $category;
    if ($sub_category != '') {
        $str_col1 .= '<br><strong><font color="darkblue">' . $sub_category_label . ' </font></strong>' . $sub_category;
    }
    $str_col1 .= format_str($loan_type, 'Loan Type');
    $str_col1 .= '<br><strong><font color="darkblue">No Pieces: </font></strong>' . $no_pieces;
    $str_col1 .= '<br><strong><font color="darkblue">Rent: </font></strong>' . $rent;
    $str_col1 .= format_str($date_purchase, 'Date Purchased');
    $str_col1 .= format_str($age, 'Age Range');
    $str_col1 .= format_str($condition, 'Condition');
    $str_col1 .= format_str($storage, $storage_label);
    $str_col1 .= format_str($manufacturer, 'Manufacturer');
    $str_col1 .= format_str($supplier, 'Supplier');
    if ($donated_label == '') {
        $donated_label = 'Donated By';
    }
    $str_col1 .= format_str($sponsorname, $donated_label);
    $str_col1 .= format_str($discountcost, 'Cost');
    $str_col1 .= format_str($cost, 'Replacement Cost');
    $str_col1 .= format_str($returndateperiod, 'Return Period (days)');
    $str_col1 .= format_str($freight, 'Freight');
    $str_col1 .= format_str($package, 'Packaging Cost');
    $str_col1 .= format_str($process_time, 'Processing Time (minutes)');
    $str_col1 .= format_str($days_out_str, 'On Loan in the last Year');
    $str_col1 .= format_str($changedby, 'Changed by User');
    $str_col1 .= format_str($format_st, 'Last checked/locked');
    $str_col1 .= format_str($modified, 'Last Updated');
    if ($product_link != '') {
        $str_col1 .= '<br><strong><font color="darkblue">Product Link: </font></strong><a  target="_blank" class="button_small_yellow" href="' . $product_link . '">More...</a>';
    }
    $str_col1 .= format_str($keywords, 'Keywords');

    if ($user_toys == '') {
        $user_toys = 'User1';
    }
    $str_col1 .= format_str($user1, $user_toys);

    if ($desc1 != null || $desc1 != '' || $desc2 != '' || $desc2 != null) {
        $str_col2 = '<br><strong><font color="darkblue">Toy Description:</font></strong><br>';
        $str_col2 .= $desc1 . '<br>' . $desc2;
    }

    $str_col2 .= '<font color="red">' . format_str($warnings, 'WARNINGS') . '</font>';
    $str_col2 .= format_str($comments, 'Comments');
    $str_col2 .= format_str($alerts, 'Alerts');


//
    ?>
    <div class="row">
        <div class="col-sm-4">
            <?php echo $str_col1; ?>
        </div>
        <div class="col-sm-4">
            <?php echo $str_col2; ?>
        </div>
        <div class="col-sm-4">
            <?php
            echo '<br>' . $pic_img . '<br>';
            if (file_exists($file_pic)) {
                include ('delete_pic.php');
            }
            echo '<h1><font color="#330066">Missing Parts </font></h1>';
            //echo '<td><a href="../parts/new_part.php?idcat=' . $idcat . '" class="button1">New Part</a></td>';
            echo '</tr></table>';
            include('parts/part_form.php');
            include('../parts/parts_list.php');
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <?php include('../history/hist_list.php'); ?>
        </div>
        <div class="col-sm-4"></div>
    </div>
</div>


<?php

Function format_str($field, $heading) {
    $str = '';
    if ($field != '' || $field != null) {
        $str = '<br><strong><font color="darkblue">' . $heading . ': </font></strong>' . $field;
    }
    if (trim($field) == '--') {
        $str = '';
    }
    return $str;
}

function money($total) {
    $out = number_format($total, 0, '.', ',');
    return $out;
}
?>


