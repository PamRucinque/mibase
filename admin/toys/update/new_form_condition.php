<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<body id="main_body">

    <div id="form_container">

        <font size="2" face="Arial, Helvetica, sans-serif"></font>
        <table width="50%"><tr><td>


                    <form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'new_condition.php' ?>">

                        <div id="form" style="background-color:lightgray;" align="left">

                            <table align="top"><tr>

                                    <td align="right"><input id="saveForm" class="button1"  type="submit" name="submit" value="Add a New Toy Condition" /></td>
                                </tr>
                                <tr>
                                    <td>Toy Condition:<br>
                                        <input type="Text" name="condition" align="LEFT"  size="50" value=""></input><br></td>
                                </tr>
              

                                <input type="hidden" name="idcat" value="<?php echo $newconditionid; ?>"> 
                            </table>
                        </div>
                </td></tr></table>
    </form>
</div>
</body>




