<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include('../../header.php'); ?> 
    </head>    

    <body id="main_body" >
        <div id="form_container">
            <?php
            include('../../menu.php');
            include('new_form_age.php');
            //include('../../connect.php');

            if (isset($_POST['submit'])) {
                //$toyid = sprintf("%02s", $toyid);
                include('new_ageid.php');
                $age = pg_escape_string($_POST['age']);

                $query_new = "INSERT INTO age (id, age)
         VALUES ({$newageid}, 
        '{$age}')";

                $result_new = pg_Exec($conn, $query_new);


                if (!$result_new) {
                    echo "An INSERT query error occurred.\n";
                    //echo $query_new;
                    //echo $connection_str;
                    exit;
                }

// Get the last record inserted
// Print out the Contact ID
                else {

                    //$newid = $newid;
                    //echo "<br>The record was successfully entered and the ID is:" . $newcatid . "<br><br>";
                    pg_FreeResult($result_new);
                    pg_Close($conn);
                    include('ages.php');
                    //window . location . reload();
                    $redirect = "Location: new_age.php";               //print $redirect;
                    header($redirect);
                }
            } else {
                include('ages.php');
            }
            ?>
        </div>
    </body>
</html>