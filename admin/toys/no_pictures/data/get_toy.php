<?php
$connect_pdo_toybase = 'pgsql:port=5432;dbname=toys;host=localhost';
$dbuser_toybase = 'toys';
$dbpasswd_toybase = 'dNTB9tkhP6bWXCX';
$id = $_GET['id'];
$pdo = new PDO($connect_pdo_toybase, $dbuser_toybase, $dbpasswd_toybase);

$sql = "select * from toys where counter = ? order by category, id;";
echo $sql;
$sth = $pdo->prepare($sql);
$array = array($id);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = $result[$ri];
    //$id = $row['counter'];
    $toy_id = $row['id'];
    //$idcat = $row['toys.idcat'];
    $desc1 = $row['desc1'];
    $desc2 = $row['desc2'];
    $location = $row['loc_description'];
    $loan_type = trim($row['loan_type']);
    $category = $row['category'];
    $reservecode = $row['reservecode'];
    $toyname = $row['toyname'];
    $rent = $row['rent'];
    $age = $row['age'];
    $datestocktake = $row['datestocktake'];
    $manufacturer = $row['manufacturer'];
    $discountcost = $row['discountcost'];
    $status = $row['status'];
    $cost = $row['cost'];
    $supplier = $row['supplier'];
    $comments = $row['comments'];
    $returndateperiod = $row['returndateperiod'];
    $returndate = $row['returndate'];
    $no_pieces = $row['no_pieces'];
    $date_purchase = $row['date_purchase'];
    $sponsorname = $row['sponsorname'];
    $warnings = $row['warnings'];
    $user1 = $row['user1'];
    //$date_entered = date_format($row['date_entered'], 'd/m/y');
    $alert = $row['alert'];
    $toy_status = $row['toy_status'];
    $stype = $row['stype'];
    $units = $row['units'];
    $borrower = $row['borrower'];
    $lockdate = $row['datestocktake'];
    $alerts = $row['alert'];
    $lockreason = $row['lockreason'];
    $created = $row['created'];
    $modified = $row['modified'];
    $condition = $row['reservedfor'];
    $storage = $row['storage'];
    $trans_due = $row['due'];
    $product_link = $row['link'];
 
    $format_purchase = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    $format_lock = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    if ($cost == Null) {
        $cost = 0;
    }
    if ($discountcost == Null) {
        $discountcost = 0;
    }
    if ($rent == Null) {
        $rent = 0;
    }




    $_SESSION['idcat'] = $idcat;
}

if ($stherr[0] != '00000') {
    $_SESSION['login_status'] .= "An  error occurred.\n";
    $_SESSION['login_status'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['login_status'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['login_status'] .= 'Error' . $stherr[2] . '<br>';
}
$desc1 = str_replace("\r\n", "</br>", $desc1);
$desc2 = str_replace("\r\n", "</br>", $desc2);
//$_SESSION['reserve_status'] = '';

$file_pic = '..' . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';

if (file_exists($file_pic)) {
    $img = '<img width="230px" src="..' . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg" alt="toy image">';
    
} else {
    $img = '<img width="230px" src="../toy_images/demo/' . 'blank' . '.jpg" alt="toy image">';
}
if (($trans_due == null) || ($trans_due == '')) {
    if (($location == '') || ($location == '')){
       $str_status = '<a style="background-color: lightgreen;"><font color="#330066">IN LIBRARY</font></a>'; 
    }else{
        $str_status = '<a style="background-color: lightgreen;"><font color="#330066">' . $location . '</font></a>';
    }
    
} else {

    $str_status = '<a style="background-color: yellow;font-size: 20px;"><font color="#330066"></font>';
    if ($mem_hide_duedate == 'Yes') {
        $str_status = '</a>';
    } else {
        $str_status .= '<font color="#330066"> Due: ' . $format_due . '</font></a> ';
    }
}


//echo $img;

$str_print = '';
$str_print_reserves = '<div id="detail" style="padding-left: 10px">' . $str_status;
if ($toy_status == 'LOCKED' || $toy_status == 'WITHDRAWN') {
    $str_print .= '<strong><font color="blue">Lock Date:</font></strong>' . $lockdate . ': Reason: ' . $lockreason . '<br>';
}
//$str_print .= '<strong><font color="blue">Toy Number: </font></strong>' . $idcat . '<br>';
$str_print .= '<strong><font color="blue">Category: </font></strong>' . $category . '<br>';
$str_print_reserves .= '<strong><font color="blue">Category: </font></strong>' . $category . '<br>';
$str_print .= '<strong><font color="blue">No Pieces: </font></strong>' . $no_pieces . '<br>';
$str_print_reserves .= '<strong><font color="blue">No Pieces: </font></strong>' . $no_pieces . '<br>';
$str_print .= '<strong><font color="blue">Description: </font></strong><br>' . $desc1 . '<br>' . $desc2 . '<br>';

if ($age != '') {
    $str_print .= '<strong><font color="blue">Age: </font></strong>' . $age . '<br>';
    $str_print_reserves .= '<strong><font color="blue">Age: </font></strong>' . $age . '<br>';
}
if ($product_link != '') {
    $str_print .= 'Product Link to this Toy: <a  target="_blank"  href="' . $product_link . '">More...</a><br>';
}

if ($loan_type != '') {
    $str_print .= '<strong><font color="blue">Loan Type: </font></strong>' . $loan_type . '<br>';
}

if ($rent > 0) {
    $str_print .= '<h2><strong><font="green">Hire Charge: </font>$</strong>' . $rent .  ' per ' . $loanperiod . ' days.</font></h2>';
    $str_print .= '<h2>Hire Toys are for ' . $reserve_period . ' days ';
    $str_print_reserves .= '<h2><strong><font="green">Hire Charge: </font>$</strong>' . $rent . ' per ' .  $reserve_period .  ' days.</font>';
    $str_print_reserves .= ' Hire Toys are for ' . $reserve_period . ' days </h2>';
    $weeks = floor($reserve_period / 7);
    $dayRemainder = $days % 7;
    if ($weeks >= 1) {
        if ($weeks == 1) {
            $str_print .= '(or one week hire).</h2>';
        } else {
            if ($dayRemainder == 0) {
                $str_print .= '(or ' . $weeks . ' weeks hire).</h2>';
            } else {
                $str_print .= '.</h2>';
            }
        }
    }
}

if ($storage_label == '') {
    $storage_label = 'Storage: ';
}
if ($storage != '') {
    $str_print .= '<strong><font color="blue"> ' . $storage_label . '</font>  </strong>' . $storage . '<br>';
}

$str_print .= '<strong><font color="red">' . $alert . '<br>' . $warnings . '</font></strong>';




if (($_SESSION['memberid'] == 817) && ($_SESSION['library_code'] == 'maroondah')) {
    $mem_reserves = 'Yes';
}
if (($mem_reserves == 'Yes') && ($reservecode != '')) {
    $str_reserves = '<a id="button" style="font-size: 14px;" href="../reserves_new/index.php?idcat=' . $_SESSION['idcat'] . '">Reservations</a>';
    //echo "<h1><font color='red'>Please don't use Reservations work in progress!!</font></h1>";
    //include( dirname(__FILE__) . '/reserve_page.php');

    if ($loan_type == 'GOLD STAR') {
        $str_reserves .= '<h3><font color="red">This Toy is a Party Pack Toy or a Gold Start Toy and cannot be reserved</font><h3>';
    }
}
$str_print_reserves .= '</div>';
$str_print .= $str_reserves;



