<script>
    function reset() {
        var search = document.getElementById("searchData");
        document.getElementById("reset").focus();
        search.value = '';
        search.focus();
    }
</script>
<div class="flex-search">
    <div>
        <input type="text" name="searchData" id="searchData" value="Search"
               onblur="if (this.value == '') {
                           this.value = 'Search';
                           this.style.color = '#BBB';
                       }"
               onfocus="if (this.value == 'Search') {
                           this.value = '';
                           this.style.color = '#000';
                       }"
               style="color:#BBB;" value="<?php echo $_SESSION['search']; ?>"/>
    </div>
    <div style="padding-left: 10px;">
        <input  name="reset" id="reset" value="Clear Search Box" type="submit" style="height: 30px;" onclick="reset()">   

    </div>
</div>




