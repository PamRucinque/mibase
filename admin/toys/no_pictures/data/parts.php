<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
require( dirname(__FILE__) . '/../../../mibase_check_login.php');
//include( dirname(__FILE__) . '/../connect.php');
$str_parts = '';
$alert_parts = '';
$alert_parts_txt = '';
$table_parts = '';
$idcat = $_SESSION['idcat'];

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$sql = "select parts.*,
typepart.picture as warning
 from parts 
 LEFT JOIN typepart on (parts.type = typepart.typepart) 
    where upper(itemno) = ? ORDER by typepart.picture desc, parts.type, parts.datepart;";

$sth = $pdo->prepare($sql);
$array = array($idcat);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

if ($stherr[0] != '00000') {
    $error_msg .= "An  error occurred.\n";
    $error_msg .= 'Error' . $stherr[0] . '<br>';
    $error_msg .= 'Error' . $stherr[1] . '<br>';
    $error_msg .= 'Error' . $stherr[2] . '<br>';
}

$status_txt = Null;




if ($numrows > 0) {
//echo '<h2> Missing Parts: </h2>';
    $table_parts .= '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey; padding-left: 10px">';
}
$page_break = 'No';


for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = $result[$ri];
    $_SESSION['library_code'] = $_SESSION['subdomain'];
    $format_datepart = substr($row['datepart'], 8, 2) . '-' . substr($row['datepart'], 5, 2) . '-' . substr($row['datepart'], 0, 4);
    $alert_txt = '';
    if ($ri == 0 && $row['warning'] == '') {
        $table_parts .= '<tr><td colspan="3"><font color="#330066"><strong>Missing</font></strong></td></tr>';
        //$table_parts .= '<tr><td>id</td><td>Date</td><td>type</td><td>alert</td><td>member</td></tr>';
    }


    $_SESSION['library_code'] = $_SESSION['subdomain'];
    $format_datepart = substr($row['datepart'], 8, 2) . '-' . substr($row['datepart'], 5, 2) . '-' . substr($row['datepart'], 0, 4);
    $alert_txt = null;
    if ($row['alertuser'] == 't') {
        $alert_txt .= 'Yes';
    } else {
        $alert_txt .= 'No';
    }

    if (($row['warning'] != '') && ($page_break == 'No')) {
        $table_parts .= '<tr><td colspan="3"><font color="#330066"><strong>Warnings</font></strong></td></tr>';
        $page_break = 'Yes';
    }
    $table_parts .= '<tr>';

    if ($row['description'] != '') {
        if ($row['warning'] != '') {
            $table_parts .= '<tr><td colspan="3"><font color="red">  ' . $row['description'] . '</font></td></tr>';
        } else {
            $table_parts .= '<tr><td colspan="3"><font color="blue">  ' . $row['description'] . '</font></td></tr>';
        }
    }

    $table_parts .= '</tr>';
}
if ($numrows > 0) {
    $table_parts .= '<tr height="0"></tr>';
    $table_parts .= '</table>';
}
?>

