<script>
    function set_toy(str, subd) {
     
        var input = str;
        var subdomain = subd;
        var cell = 'desc_' + input;
        var element = document.getElementById("image_new");
        var desc = document.getElementById(cell).innerHTML;
      
        //alert(desc);
        var toy_desc = '<strong>' + input.toUpperCase() + ': ' + desc + '</strong>';
        var link_toy =  '  <a href="../../toys/update/toy_detail.php?idcat=' + input + '"><button>Go To Toy ' + input.toUpperCase() + '</button></a>';
        var button = document.getElementById("find_button");
        button.style.display = "block";
         var idcat = document.getElementById("idcat");
        idcat.value = input;
        var toy = document.getElementById("toy_desc");
        toy.innerHTML = toy_desc;
        document.getElementById("link_toy").innerHTML = link_toy;
        element.innerHTML = '<img align="center" width="100px" src="https://' + subdomain + '.mibase.com.au' . $_SESSION['toy_images_location'] . '/' + subdomain + '/' + input + '.jpg" >';
        document.getElementById("idcat").value = input;

    }
</script>
<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
require(dirname(__FILE__) . '/../mibase_check_login.php');
include( dirname(__FILE__) . '/../get_settings.php');
$str_category = '';
$category_str = '';
$location_str = '';
$count_rows = 1;

if (isset($_POST['reset'])) {
    $_SESSION['category'] = '';
    $_SESSION['age'] = '';
    $_SESSION['loc_filter'] = '';
    $_SESSION['search'] = '';
    $_SESSION['idcat_select'] = '';
}


if (isset($_POST['category'])) {
    $_SESSION['category'] = trim($_POST['category']);
}
if (isset($_SESSION['category'])) {
    $str_category = $_SESSION['category'];
}

if ($str_category == '') {
    $category_str = $str_category . '%';
} else {
    $category_str = $str_category;
}


if ((isset($_POST['search']) && ($_POST['search'] != 'search'))) {
    $_SESSION['search'] = $_POST['search'];
}

if (isset($_SESSION['catsort'])) {
    $catsort = $_SESSION['catsort'];
}


$_SESSION['showall'] = 1000;

$search = "";
if (isset($_SESSION['search'])) {
    $search = $_SESSION['search'];
    $string = substr($search, 0, 1);
    if ($string == '-') {
        //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
        $search = rtrim(substr($search, 1));
    }
}
//$search = $_SESSION['search'];
$search = strtoupper($search);
$search_str = '%' . $search . '%';
//$category_str = $_SESSION['category'] . '%';
$age = '';
if (isset($_SESSION['age'])) {
    $age = $_SESSION['age'];
}
if ($location_str == '') {
    $location_str = '%' . $location_str . '%';
}
$age_str = '%' . $age . '%';
//error message (not found message)
if ($catsort == 'No') {
    $order_by = 'ORDER BY id ASC ';
} else {
    $order_by = 'ORDER BY category, id ASC ';
}

$total = 0;
include( dirname(__FILE__) . '/../connect.php');
include( dirname(__FILE__) . '/../get_settings.php');

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);


$query_toys = "SELECT
     toys.idcat as idcat,
     toys.toyname as toyname,
     toys.status as status,
     toys.category as category,
     toys.no_pieces as no_pieces,
     toys.id as id,
     toys.age as age,
     toys.rent as rent,
     toys.storage as storage,
     toys.manufacturer as manufacturer,
     toys.color as color,
     toys.toy_status as toy_status,
     toys.twitter as twitter,
     toys.loan_type as stype,
     toys.sub_category as sub_category,
     toys.location as location,
     location.description as location_description,
     transaction.due as due,
     transaction.borname as borname,
     transaction.phone as trans_phone,
     (select count(id) as count_holds from toy_holds 
     where date_end >= current_date and status = 'ACTIVE' and toys.idcat = toy_holds.idcat) as toy_holds 
      
    FROM
        toys 
     left join location on location.location = toys.location
     LEFT OUTER JOIN
     (
     SELECT
          transaction.idcat,
          transaction.due,
          transaction.borname,
          transaction.phone
     FROM
          transaction where return is null
     GROUP BY
          transaction.due, transaction.idcat, transaction.borname , transaction.phone

     ) AS transaction ON upper(transaction.idcat) = upper(toys.idcat) 
        where (upper(toyname) LIKE ? 
        OR upper(manufacturer) LIKE ?
        OR upper(storage) LIKE ? 
        OR upper(user1) LIKE ? 
        OR upper(twitter) LIKE ? 
        OR upper(toys.idcat) LIKE ?)
        AND (toys.category LIKE ?) 
        AND ((age LIKE ?) OR (age IS NULL))
        AND ((toys.location LIKE ?) OR (toys.location IS NULL))
        AND (toy_status = 'ACTIVE' or toy_status = 'PROCESSING') " . $order_by . ";";
//echo $query_toys;
//echo $location_str;

$sth = $pdo->prepare($query_toys);
//$array = array($search_str, $search_str, $search_str, $search_str,$search_str, $category_str, $age_str);
$array = array($search_str, $search_str, $search_str, $search_str, $search_str, $search_str, $category_str, $age_str, $location_str);

$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

//$query = "SELECT * FROM toys ORDER by id ASC;";


$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");


$toprint = sizeof($result);

for ($ri = 0; $ri < $toprint; $ri++) {
    $row = $result[$ri];
    $category = $row["category"];
    $idcat = $row['idcat'];
    $toyname = $row["toyname"];
    $age = $row["age"];
    $toy_status = $row["toy_status"];
    $no_pieces = $row['no_pieces'];
    $rent = $row['rent'];
    $manufacturer = $row['manufacturer'];
    $id = $row["id"];
    $idcat = $row["idcat"];
    $link_toy = 'update/toy_detail.php?idcat=' . $idcat;
    $onclick_toy = 'set_toy("' . strtolower($idcat) . '","' . $_SESSION['library_code'] . '");';

    $row_txt = "<tr border='1' class='item' id='red' onclick='" . $onclick_toy . "'>";
    $row_txt .= '<td border="1" width="30px">' . $id . '</td>';
    $row_txt .= '<td class="idcat" width="50px" align="center">' . $idcat . '</td>';
    $row_txt .= '<td width="20px"  align="left" style="padding-left: 5px;">' . $category . '</td>';
    $row_txt .= '<td width="150px" align="left" id="desc_' . strtolower($idcat) . '">' . $toyname . '</td>';

    $file_pic = '../../..' . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';

    if (file_exists($file_pic)) {
        $pic = "'../../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . ".jpg'";
    } else {
        $pic = "'../../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . "blank.jpg'";
    }

    //$row_txt .= '<td width="50" align="center"><a href="update/toy_detail.php?idcat=' . $idcat . '" onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();" class="button_small"/>View</a>';
    $img = '<img height="200px" src="../..' . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg" alt="toy image">';
    if ($overdue_txt == '') {
        $overdue_txt = 'OVERDUE';
    }
    if ($due != '') {
        $loan_status = '<td BGCOLOR=lightgreen width="80" align="center">ON LOAN</td>';
        if ($row['due'] < $today) {
            $loan_status = '<td BGCOLOR=pink width="50px" align="center">' . $overdue_txt . '</td>';
        }
    } else {
        if ($toy_status == 'PROCESSING') {
            $loan_status = '<td BGCOLOR=yellow width="50px" align="center">PROCESSING</td>';
        } else {
            if ($toy_holds > 0) {
                $loan_status = '<td width="50px"  bgcolor="#F660AB" align="center">ON HOLD</td>';
            } else {
                $loan_status = '<td width="50px" align="center">IN LIBRARY</td>';
            }
        }
    }

    $row_txt .= $loan_status;
    $row_txt .= '<td width="70px">' . $manufacturer . '</td>';
    $row_txt .= '<td width="90px">' . substr($age, 0, 12) . '</td>';
    $row_txt .= '<td width="10px" align="center">' . $no_pieces . '</td></tr>';
    if (!file_exists($file_pic)) {
        $result_txt .= $row_txt;
        $count_rows = $count_rows + 1;
    }
}
$result_txt .= '</table>';
$result_txt .= $toprint_txt;
$search_str = '';

if ($str_category != '') {
    $search_str .= $search_str . ' Filtered on Category = ' . $str_category . ' ';
}
if ($age_str != '%%') {
    $search_str .= $search_str . ' Filtered on Age = ' . $age_str . ' ';
}
if ($search != '') {
    $search_str .= $search_str . ' Filtered on String = ' . $search . ' ';
}
$selected_toys = '';
if (isset($_SESSION['idcat_select'])) {
    $selected_toys = str_replace("'", " ", $_SESSION['idcat_select'], $count);
}







print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
print '<font color="blue"><strong>Total: ' . $count_rows . '</font></strong>';
print $result_txt;
//print '</table>';
?>