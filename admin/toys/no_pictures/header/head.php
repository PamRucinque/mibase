        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EDGE,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Vanilla JS Responsive Menu" />
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Fjalla+One|Roboto+Slab:300,400,500,700" />
        <link rel="stylesheet" href="css/menu.css" />
        <script src='js/jquery-3.0.0.js' type='text/javascript'></script>           
        <script type="text/javascript" src="js/tooltip.js"></script>
 