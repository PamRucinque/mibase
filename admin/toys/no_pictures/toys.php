<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php include( dirname(__FILE__) . '/header/head.php'); ?>
        <script>
            $(document).ready(function () {

                $.get('data/search-data_list.php?searchData= ', function (returnData) {
                    /* If the returnData is empty then display message to user
                     * else our returned data results in the table.  */
                    if (!returnData) {
                        $('#results').html('<p style="padding:5px;">Search term entered does not return any data.</p>');
                    } else {
                        $('#results').html(returnData);
                    }
                });

                $('#searchData').keyup(function () {

                    /* Get the value of the search input each time the keyup() method fires so we
                     * can pass the value to our .get() method to retrieve the data from the database */
                    var searchVal = $(this).val();

                    /* If the searchVal var is NOT empty then check the database for possible results
                     * else display message to user */
                    if (searchVal !== '') {

                        $.get('data/search-data_list.php?searchData=' + searchVal, function (returnData) {
                            /* If the returnData is empty then display message to user
                             * else our returned data results in the table.  */
                            if (!returnData) {
                                $('#results').html('<p style="padding:5px;">Search term entered does not return any data.</p>');
                            } else {
                                $('#results').html(returnData);
                            }
                        });

                    }
                });

            });
            function show_search() {
                var no_pic = document.getElementById("no_pic");
                no_pic.style.display = "none";
                var search_pictures = document.getElementById("search_pictures");
                search_pictures.style.display = "block";
                var total_pics = document.getElementById("total_pics");
                total_pics.style.display = "block";
                var results = document.getElementById("results");
                results.style.display = "block";
                var button = document.getElementById("display_button");
                button.style.display = "block";
                var button_find = document.getElementById("find_button");
                button_find.style.display = "none";
                document.getElementById("searchData").focus();


            }
            function display_list() {
                var no_pic = document.getElementById("no_pic");
                no_pic.style.display = "block";
                var search_pictures = document.getElementById("search_pictures");
                search_pictures.style.display = "none";
                var total_pics = document.getElementById("total_pics");
                total_pics.style.display = "none";
                var button = document.getElementById("display_button");
                button.style.display = "none";
                var button_find = document.getElementById("find_button");
                button_find.style.display = "block";
                var results = document.getElementById("results");
                results.style.display = "none";

            }


        </script>

    </head>
    <?php
    if (!session_id()) {
        session_start();
    }
    $msg_str = $_SESSION['msg_pic'];
    if ($msg_str != '') {
        echo "<script type='text/javascript'>alert('$msg_str');</script>";
        $msg_str = '';
        $_SESSION['msg_pic'] = '';
    }
    ?>
    <body style="overflow: hidden;">
        <section class="container fluid">

            <div class="col-sm-12">


            </div>
        </section>
        <section class="container">
            <?php
            $branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
            require( dirname(__FILE__) . '/../../mibase_check_login.php');
            include( dirname(__FILE__) . '/header/header.php');
            $_SESSION['search'] = '';
            echo '<title>' . $libraryname . '</title>';

            if (!session_id()) {
                session_start();
            }
//echo $_SESSION['search'];

            if (isset($_POST['limit'])) {
                $_SESSION['limit'] = $_POST['limit'];
            }
            if ($_POST['category'] != '') {
                $_SESSION['category'] = strtoupper($_POST['category']);
                //$_SESSION['limit'] = 'All';
            }

            if (isset($_POST['status'])) {
                $_SESSION['status'] = $_POST['status'];
                //$_SESSION['limit'] = 'All';
            }
            if (isset($_POST['category']) && ($_POST['status'])) {
                $_SESSION['category'] = strtoupper($_POST['category']);
                $_SESSION['status'] = $_POST['status'];
                // $_SESSION['limit'] = 'All';
            }
            if (isset($_POST['searchData'])) {
                $_SESSION['search'] = $_POST['searchData'];
            }
            if (isset($_POST['reset'])) {
                $_SESSION['limit'] = 100;
                $_SESSION['category'] = '%';
                $_SESSION['status'] = '';
                $_SESSION['search'] = '';
            }



//echo '<h4><font color="blue">Toys Without Pictures</font></h4>';
            ?>
            <div class="flex-search">


                <?php
//<a href="update/toy_detail.php?idcat=C01">go to toy C01</a>
                echo '<div id="source"></div>';
                echo '<div id="image"></div>';
                echo '<div id="toy_desc" style="font-size: 15px;"><font color="red">Click on the row in the list below to select the toy.</font></div>';
                echo '<div id="find_button" style="display: none;">';
                echo '<button onclick="show_search();">Find a Toy Picture</button>';

                echo '</div>';

                echo '<div id="display_button" style="display: none;"><button style="background-color: #0077c0;" onclick="display_list();">Display the Toy List</button></div>';
                echo '<div id="link_toy"></div>';
                ?>
                <div id="copy_div" style="display: none;">
                    <form action="copy_file.php" method="post">
                        <input type="hidden" id="filename" name="filename">
                        <input type="hidden" id="idcat" name = "idcat" required>
                        <input type="submit" id="copy" value="Copy this Picture" style="background-color: #900C3F;"/>
                    </form>
                </div>
            </div>

            <div id="no_pic"  style="background-color: lightyellow;">
                <?php include( dirname(__FILE__) . '/toys_nopic.php'); ?>
            </div>
            <?php
            echo '  <div id="search_pictures" style="display: none;">';
            //echo '<form>'; 
            include( dirname(__FILE__) . '/data/search.php');
            echo '</div>';
            ?>
            <div id='results' style="overflow:scroll; height:500px;display: none;"></div>

        </section>

        <script type="text/javascript" src="js/menu.js"></script>

    </body>
</html>


