<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<head>

    <?php include( dirname(__FILE__) . '/header/head.php'); ?>
</head>
<script  type="text/javascript" src="../js/jquery-2.0.3.min.js"></script>
<?php
if (!session_id()) {
    session_start();
}

//print_r($_SESSION);
?>
<body>
    <div class="flex-search">
        <?php
        $idcat = '';
        if (isset($_SESSION['idcat'])) {
            $idcat = $_SESSION['idcat'];
        }

        $toy_to_print = "'" . $idcat . "'";
        $sql_report = "select * from toys;";

        if (isset($_POST['reset_2'])) {
            //echo 'Hello Gregor reset2';
            $_SESSION['idcat_select'] = '';
            $_SESSION['search'] = '';
            $_SESSION['category'] = '';
            $_SESSION['age'] = '';
            $_SESSION['loc_filter'] = '';
        }

        //echo 'Hello Gregor' . $_POST['reset_2'] . '*';  

        $total = 0;
        ?>
        <div><form name="search_form" id="search_form" method="post" action="toys.php" > 
                <input type="text" name="search" id="search" value="search"
                       onblur="if (this.value == '') {
                                   this.value = 'search';
                                   this.style.color = '#BBB';
                               }"
                       onfocus="if (this.value == 'search') {
                                   this.value = '';
                                   this.style.color = '#000';
                               }"
                       style="color:#BBB;" onchange="this.form.submit()"/>
                <input type="hidden" name="total" value="<?php echo $total; ?>">
                </div>


                <div><label class="description" for="element_12">Category:  </label><?php include( dirname(__FILE__) . '/data/get_category.php'); ?></div>
            </form>


            <div><form name="form_reset" id="form_reset" method="post" action="toys.php" > 
                <input style = "background-color: #0077c0;" type="submit" name ="reset_2" id ="reset_2" value="Reset Toy List"/> </div>
            </form>
        </div>


        <?php
         echo '<div style="overflow:scroll; max-height:300px;background-color: lightyellow;">';
        include( dirname(__FILE__) . '/result.php');
        echo '</div>';
        ?>

</body>

</html>
