<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');


$overdue_txt = $_SESSION['settings']['overdue_txt'];

$str_category = '';
$category_str = '';
$location_str = '';



if (isset($_SESSION['category'])) {
    $str_category = $_SESSION['category'];
}

if ($str_category == '') {
    $category_str = $str_category . '%';
} else {
    $category_str = $str_category;
}



if (isset($_SESSION['age'])) {
    $age_str = $_SESSION['age'];
}

if (isset($_POST['loc_filter'])) {
    $_SESSION['loc_filter'] = trim($_POST['loc_filter']);
} else {
    $_SESSION['loc_filter'] = '';
}
if ($_SESSION['loc_filter'] != '') {
    $location_str = '%' . $_SESSION['loc_filter'] . '%';
}


if (isset($_POST['search'])) {
    $_SESSION['search'] = $_POST['search'];
}

if (isset($_SESSION['catsort'])) {
    $catsort = $_SESSION['catsort'];
}


$_SESSION['showall'] = 100;

$search = "";
if (isset($_SESSION['search'])) {
    $search = $_SESSION['search'];
    $string = substr($search, 0, 1);
    if ($string == '-') {
        $search = rtrim(substr($search, 1));
    }
}
$search = strtoupper($search);
$search_str = '%' . $search . '%';
$age = '';
if (isset($_SESSION['age'])) {
    $age = $_SESSION['age'];
}
if ($location_str == '') {
    $location_str = '%' . $location_str . '%';
}
$age_str = '%' . $age . '%';
//error message (not found message)
if ($catsort == 'No') {
    $order_by = 'ORDER BY id ASC ';
} else {
    $order_by = 'ORDER BY category, id ASC ';
}

$total = 0;
$x = 0;

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$query_toys = "SELECT
     toys.idcat as idcat,
     toys.toyname as toyname,
     toys.status as status,
     toys.category as category,
     toys.no_pieces as no_pieces,
     toys.id as id,
     toys.age as age,
     toys.rent as rent,
     toys.storage as storage,
     toys.manufacturer as manufacturer,
     toys.color as color,
     toys.toy_status as toy_status,
     toys.twitter as twitter,
     toys.loan_type as stype,
     toys.sub_category as sub_category,
     toys.location as location,
     location.description as location_description,
     transaction.due as due,
     transaction.borname as borname,
     transaction.phone as trans_phone,
     (select count(id) as count_holds from toy_holds 
     where date_end >= current_date and status = 'ACTIVE' and toys.idcat = toy_holds.idcat) as toy_holds 
      
    FROM
        toys 
     left join location on location.location = toys.location
     LEFT OUTER JOIN
     (
     SELECT
          transaction.idcat,
          transaction.due,
          transaction.borname,
          transaction.phone
     FROM
          transaction where return is null
     GROUP BY
          transaction.due, transaction.idcat, transaction.borname , transaction.phone

     ) AS transaction ON upper(transaction.idcat) = upper(toys.idcat) 
        where (upper(toyname) LIKE ? 
        OR upper(manufacturer) LIKE ?
        OR upper(storage) LIKE ? 
        OR upper(user1) LIKE ? 
        OR upper(twitter) LIKE ? 
        OR upper(toys.idcat) LIKE ?)
        AND (toys.category LIKE ?) 
        AND ((age LIKE ?) OR (age IS NULL))
        AND ((toys.location LIKE ?) OR (toys.location IS NULL))
        AND (toy_status = 'ACTIVE' or toy_status = 'PROCESSING') " . $order_by . ";";

$sth = $pdo->prepare($query_toys);

$array = array($search_str, $search_str, $search_str, $search_str, $search_str, $search_str, $category_str, $age_str, $location_str);

$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");

if (isset($_GET['r'])) {
    $toprint = sizeof($result);
} else {
    $toprint = 100;
}
$total = sizeof($result);
if (sizeof($result) < 100) {
    $toprint = sizeof($result);
    $toprint_txt = " ";
} else {
    $link = '<a class="button_small" href="toys.php?r=' . $toprint . '">Showall</a>';
    $toprint_txt = $toprint . ' of ' . $total . " shown. " . $link;
}
include('total_row.php');
include('heading.php');

for ($ri = 0; $ri < $toprint; $ri++) {
    $row = $result[$ri];
    $category = $row["category"];
    $toyname = $row["toyname"];
    $age = $row["age"];
    $toy_holds = $row['toy_holds'];
    $toy_status = $row["toy_status"];
    $no_pieces = $row['no_pieces'];
    $status = $row["status"];
    $borname = $row['borname'];
    $borrower_name = $row['borname'];

    if ($multi_location == 'Yes') {
        $location_description = $row['location_description'];
    } else {
        $location_description = '';
    }

    $rent = $row['rent'];

    $manufacturer = $row['manufacturer'];
    $sub_category = $row['sub_category'];



    if ((strpos($borname, '&') !== false)) {
        $borname_small = explode('&', $borname);
    } else {
        if ((strpos($borname, ' and ') !== false)) {
            $borname_small = explode(' and ', $borname);
        } else {
            if ((strpos($borname, 'Expires') !== false)) {
                $borname_small = explode('Expires', $borname);
            } else {

                $borname_small[0] = $borname;
            }
        }
    }
    $due = $row['due'];


    if ($row['due'] != '') {
        $format_date_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);
    } else {

        $format_date_due = '';
    }


    $id = $row["id"];
    $idcat = $row["idcat"];
    $link_toy = 'update/toy_detail.php?idcat=' . $idcat;
    $onclick_toy = 'javascript:location.href="' . $link_toy . '"';



    if ($row['stype'] != '') {
        $category_str .= '<td width="80px" align="left" style="padding-left: 5px;">' . $category . ' <font color="blue">' . $row['stype'] . '</font></td>';
    } else {
        $category_str .= '<td width="20px"  align="left" style="padding-left: 5px;">' . $category . '</td>';
    }

    if ($rent > 0) {
        $rent_str = '' . $rent . '';
    } else {
        $rent_str = '';
    }
    if ($_SESSION['shared_server']) {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
        }
    } else {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
        }
    }
    $pic_img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';
    $view_btn = ' <a href="update/toy_detail.php?idcat=' . $idcat . '" onmouseover="showtrail(175,220, \'' . $pic_url . '\');" onmouseout="hidetrail();" class="button_small"/>View</a>';
    $img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';


    $loan_status = 'IN LIBRARY';


    if ($due != '') {
        $loan_status = '<font color="green">ON LOAN: ' . $format_date_due . '</font>';
        if ($row['due'] < $today) {
            $loan_status = '<font color="red">' . $overdue_txt . ':' . $format_date_due . '</font>';
        }
    } else {
        if ($toy_status == 'PROCESSING') {
            $loan_status = '<font color="darkorange">PROCESSING</font>';
        } else {
            if ($toy_holds > 0) {
                $loan_status = '<font color="pink">ON HOLD</font>';
            } else {
                $loan_status = '';
            }
        }
    }

    $loc_str = '';

    if ($due != '') {
        $loc_str = '<font color="blue">' . $borname_small[0] . '</font>';
    } else {
        if ($location_description != '') {
            $loc_str .= ' ' . $location_description;
        }

        // add location here!! michelle
    }
    $result_txt .= '<td width="70px">' . $format_date_due . '</td>';

    if ($sub_category == '') {
        $manu_str = '' . substr($row['manufacturer'], 0, 12) . '';
    } else {
        $manu_str = '' . substr($row['sub_category'], 0, 10) . '';
    }

    $age_str = '' . substr($age, 0, 12) . '';
    //$result_txt .= '<td width="70">' . $format_date_purchase . '</td>';
    $piece_str = '';
    $piece_str = '' . $no_pieces . '';
    include('row_bg_color.php');
    include('row.php');
}

$search_str = '';

if ($str_category != '') {
    $search_str .= $search_str . ' Filtered on Category = ' . $str_category . ' ';
}
if ($age_str != '%%') {
    $search_str .= $search_str . ' Filtered on Age = ' . $age_str . ' ';
}
if ($search != '') {
    $search_str .= $search_str . ' Filtered on String = ' . $search . ' ';
}
$selected_toys = '';
if (isset($_SESSION['idcat_select'])) {
    $selected_toys = str_replace("'", " ", $_SESSION['idcat_select'], $count);
}


//print $result_txt;
?> 
