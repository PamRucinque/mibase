<?php

require(dirname(__FILE__) . '/../../mibase_check_login.php');

if (!session_id()) {
    session_start();
}

//$idcat = 'C02';
//$category = 'D';
//$catsort = 'Yes';
//$unused = unused($category, $catsort);
//echo $unused['unused'] . '<br>';
//echo $unused['first'];
//$cat_new = get_category('D');
//echo $cat_new['description'];
//$out = copy_toy($unused['first'], $category, 'C02', 'Yes', 'Yes');
//echo $out;



function get_category($category) {
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $toy = array();
    $sql = "select * from category
            where category = ?";
    $sth = $pdo->prepare($sql);

    $array = array($category);
    $sth->execute($array);
    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    for ($ri = 0; $ri < $numrows; $ri++) {
        $category = $result[$ri];
    }
    return $category;
}

function unused($category, $catsort) {
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $first = 1;
    if ($catsort == 'Yes') {
        $sql = "select * from generate_series(1, (SELECT MAX(id)+20 FROM toys where 
            category = ?)) as unused
            where unused not in (select id from toys Where category = ?) LIMIT 20;";
        $array = array($category, $category);
    } else {
        $sql = "select * from generate_series(1, (SELECT MAX(id) FROM toys)) as unused
            where unused not in (select id from toys) LIMIT 20;";
        $array = array();
    }
    $sth = $pdo->prepare($sql);
    $sth->execute($array);
    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    if ($numrows > 0) {
        if ($catsort == 'Yes') {
            $result_txt = '<font color="blue">Lowest 20 numbers in Category ' . $category . ': </font>  ';
        } else {
            $result_txt = '<font color="blue">Lowest 20 numbers: ' . '</font>  ';
        }
    } else {
        $result_txt = $numrows . '<font color="blue">All numbers in ' . $category . ' are available.</font>  ';
    }

    for ($ri = 0; $ri < $numrows; $ri++) {
        $toy = $result[$ri];
        if ($ri == 0) {
            $first = $toy["unused"];
        }
        $result_txt .= $toy['unused'] . ' ';
    }
    return array('unused' => $result_txt, 'first' => $first);
}

function check_idcat($idcat) {
//include( dirname(__FILE__) . '/../../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "select Count(id) AS countid FROM toys where idcat='" . $idcat . "';";
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $toys = $row['countid'];
    if ($toys > 0) {
        $out = 'There is another toy with this toy code, please search for this toy number and change.';
    }
//$trans = 4;
    return $out;
}

function add_toy($id, $category, $catsort, $format_toyid, $toyname, $user) {
    $status = 'No';
    //$today = date('Y-m-d');
    $idcat_new = '';
    if ($format_toyid == 'Yes') {
        $toyid = sprintf('%02s', $id);
    } else {
        $toyid = $id;
    }
    if ($catsort == 'Yes') {
        $idcat_new = $category . $toyid;
    } else {
        $idcat_new = $toyid;
    }
    //$toyid = $_POST['toyid'];
    $today = date('Y-m-d');

    if ($catsort == 'Yes') {
        if ($format_toyid == 'Yes') {
            if ($format_toyid_decimal != '') {
                $str = '%0' . $format_toyid_decimal . 's';
                $toyid = sprintf($str, $toyid);
            } else {
                $toyid = sprintf('%02s', $toyid);
            }
        }
        $idcat_new = strtoupper($category) . $toyid;
    } else {
        $idcat_new = $toyid;
    }

    //include('../../connect.php');
    //$loan_period = $loanperiod;
    $purchase = date('Y-m-d');
    $condition = 'New';
    $location = null;
    if (($_SESSION['location'] != '') || ($_SESSION['location'] != null)) {
        $location = $_SESSION['location'];
    }
    //include('get_settings.php');
    $returnperiod = $_SESSION['settings']['loanperiod'];
    if (($returnperiod == '') || ($returnperiod == null)) {
        $returnperiod = 21;
    }


    $rent = $_SESSION['settings']['rent_default'];

    if (($rent == '') || ($rent == null)) {
        $rent = 0;
    }

    $reserve_all = $_SESSION['settings']['reserve_all'];
    if ($reserve_all == 'Yes') {
        $reservecode = $idcat_new;
    } else {
        $reservecode = null;
    }


    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $sql = "INSERT INTO toys (id, idcat, category, toyname, 
                toy_status, status, returndateperiod, date_purchase,  rent, 
                cost, discountcost, reservedfor, 
                packaging_cost, freight, process_time, reservecode, location, changedby)
                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
    //and get the statment object back
    $sth = $pdo->prepare($sql);
    $array = array($id, $idcat_new, $category, $toyname,
        'ACTIVE', 0, $returnperiod, $purchase, $rent,
        0, 0, $condition,
        0, 0, 0, $reservecode, $location, $user);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        //$out = "An INSERT query error occurred.\n";
        $out = '<br>Error ' . $stherr[0] . ' ';
        $out .= $stherr[1] . '<br> ';
        $out .= $stherr[2] . '<br>';
        //exit;
    } else {
        $status = 'Yes';
        $out = 'Success, you have added ' . $idcat_new . ': ' . $toyname . '<br>';

    }
    return array('out' => $out, 'status' => $status);
}

function check_toy_exists($toyid, $category, $catsort) {
    $connect_str = $_SESSION['connect_str'];
    $toy_exists = 'No';
    if ($catsort == 'Yes') {
        $query_check = "SELECT id FROM toys WHERE category = '" . $category . "' and id = " . $toyid . ";";
    } else {
        $query_check = "SELECT id FROM toys WHERE id = " . $toyid . ";";
    }
    $dbconn = pg_connect($connect_str);
    $result_rows = pg_exec($dbconn, $query_check);
    $numrows = pg_numrows($result_rows);
    if ($numrows == 1) {
        $toy_exists = '<font color="red">The id ' . $toyid . ' in category ' . $category . ' is already being used.</font>';
    }
    return $toy_exists;
}

function get_pic($idcat) {
    if ($_SESSION['shared_server']) {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
        }
    } else {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
        }
    }
    $pic_img = '<img height="100px" src="' . $pic_url . '" alt="toy image">';
    return $pic_img;
}
