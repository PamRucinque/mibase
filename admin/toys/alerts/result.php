<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<style type="text/css">
    tr:hover { 
        color: red; }
</style>

<?php

$search = "";
if (isset($_POST['search'])) {
    $search = $_POST['search'];
}

//
//"OR supplier LIKE '%" . $search . "%' " .

$search = strtoupper($search);

$query = "SELECT *
FROM toys
WHERE 
((upper(toyname) LIKE '%" . $search . "%' )" .
"OR (upper(alert) LIKE '%" . $search . "%' ))" .
" AND alert != '' 
ORDER BY Category, id;";

//$query = "SELECT * FROM toys ORDER by id ASC;";
//echo $query;

$XX = "No Record Found";
//print $query;
//$query = "SELECT * FROM hm_claims order by id";
$total = 0;
//include( dirname(__FILE__) . '/../../connect.php');
 $conn = pg_connect($_SESSION['connect_str']);
$result_list = pg_exec($conn, $query);
$numrows = pg_numrows($result_list);
//echo $query;
//$result_txt =  '<table border="1"><tr>';
$result_txt = '';



for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_list, $ri);
    $total = $total + 1;
    
    $ref3 = '../update/toy_detail.php?idcat=' . $row['idcat'];
    $ref4 = 'delete_alert.php?idcat=' . $row['idcat'];

    $result_txt .= '<td width="30" align="center">' . $row['id'] . '</td>';
    $result_txt .= "<td width='50' align='center'><a class ='button_small' href='" . $ref3 . "'>" . $row['idcat'] ."</a></td>";
    $result_txt .= '<td width="250" align="left">' . $row['toyname'] . '</td>';
    $result_txt .= '<td width="250" align="left">' . $row['alert'] . '</td>';
    //$result_txt .= '<td width="50" align="center">' . $row['itemno'] . '</td>';


    $result_txt .= "<td width='50' align='center'><a class ='button_small_red' href='" . $ref4 . "'>Delete Alert</a></td>";

    $result_txt .= '</tr>';
}
$result_txt .= '<tr height="0"></tr>';
$result_txt .= '</tr></table>';

print '<div id="open"><table width="100%"><tr><td width= 50%><h1 align="left"></h1></td><td><h1 align="right">Total: ' . $total . '</h1></td><tr></table></div>';
print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
print '<tr style="color:green"><td>id</td><td>IDCat</td><td>Toy Name</td><td>Alert Details</td><td>Alert</td><td width="20px"></td><tr>';


print $result_txt;
?>