<div class="container-fluid">
    <div class="row">
        <div class="hidden-xs col-md-10">
            <font color="red"><p>Double click on row to go to toy</p></font>
            <?php
            $selected_toys = '';
            if (isset($_SESSION['idcat_select'])) {
                $selected_toys = str_replace("'", " ", $_SESSION['idcat_select'], $count);
                if (trim($selected_toys) != '') {
                    print '<font color="blue"><strong>' . $count / 2 . ' toys have been selected: ' . $selected_toys . '</strong></font>';
                }
            }
            ?>
        </div>
        <div class="col-xs-12 col-md-2" style="text-align: right;">
            <font color="blue"><h4>Total <?php echo $total; ?></h4></font>
        </div>
    </div>
</div>

