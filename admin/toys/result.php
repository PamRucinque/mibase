<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');


$overdue_txt = $_SESSION['settings']['overdue_txt'];

$str_category = '';
$category_str = '';
$location_str = '';

if (isset($_POST['reset'])) {
    $_SESSION['category'] = '';
    $_SESSION['age'] = '';
    $_SESSION['loc_filter'] = '';
    $_SESSION['search'] = '';
    $_SESSION['idcat_select'] = '';
}


if (isset($_POST['category'])) {
    $_SESSION['category'] = trim($_POST['category']);
}
if (isset($_SESSION['category'])) {
    $str_category = $_SESSION['category'];
}

if ($str_category == '') {
    $category_str = $str_category . '%';
} else {
    $category_str = $str_category;
}


if (isset($_POST['age'])) {
    $_SESSION['age'] = $_POST['age'];
}
if (isset($_SESSION['age'])) {
    $age_str = $_SESSION['age'];
}

if (isset($_POST['loc_filter'])) {
    $_SESSION['loc_filter'] = trim($_POST['loc_filter']);
} else {
    $_SESSION['loc_filter'] = '';
}
if ($_SESSION['loc_filter'] != '') {
    $location_str = '%' . $_SESSION['loc_filter'] . '%';
}


if (isset($_POST['search'])) {
    $_SESSION['search'] = $_POST['search'];
}

if (isset($_SESSION['catsort'])) {
    $catsort = $_SESSION['catsort'];
}


$_SESSION['showall'] = 100;

$search = "";
if (isset($_SESSION['search'])) {
    $search = $_SESSION['search'];
    $string = substr($search, 0, 1);
    if ($string == '-') {
        $search = rtrim(substr($search, 1));
    }
}
$search = strtoupper($search);
$search_str = '%' . $search . '%';
$age = '';
if (isset($_SESSION['age'])) {
    $age = $_SESSION['age'];
}
if ($location_str == '') {
    $location_str = '%' . $location_str . '%';
}
$age_str = '%' . $age . '%';
//error message (not found message)
if ($catsort == 'No') {
    $order_by = 'ORDER BY id ASC ';
} else {
    $order_by = 'ORDER BY category, id ASC ';
}

$total = 0;

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$query_toys = "SELECT
     toys.idcat as idcat,
     toys.toyname as toyname,
     toys.status as status,
     toys.category as category,
     toys.no_pieces as no_pieces,
     toys.id as id,
     toys.age as age,
     toys.rent as rent,
     toys.storage as storage,
     toys.manufacturer as manufacturer,
     toys.color as color,
     toys.toy_status as toy_status,
     toys.twitter as twitter,
     toys.loan_type as stype,
     toys.sub_category as sub_category,
     toys.location as location,
     location.description as location_description,
     transaction.due as due,
     transaction.borname as borname,
     transaction.phone as trans_phone,
     (select count(id) as count_holds from toy_holds 
     where date_end >= current_date and status = 'ACTIVE' and toys.idcat = toy_holds.idcat) as toy_holds 
      
    FROM
        toys 
     left join location on location.location = toys.location
     LEFT OUTER JOIN
     (
     SELECT
          transaction.idcat,
          transaction.due,
          transaction.borname,
          transaction.phone
     FROM
          transaction where return is null
     GROUP BY
          transaction.due, transaction.idcat, transaction.borname , transaction.phone

     ) AS transaction ON upper(transaction.idcat) = upper(toys.idcat) 
        where (upper(toyname) LIKE ? 
        OR upper(manufacturer) LIKE ?
        OR upper(storage) LIKE ? 
        OR upper(user1) LIKE ? 
        OR upper(twitter) LIKE ? 
        OR upper(toys.idcat) LIKE ?)
        AND (toys.category LIKE ?) 
        AND ((age LIKE ?) OR (age IS NULL))
        AND ((toys.location LIKE ?) OR (toys.location IS NULL))
        AND (toy_status = 'ACTIVE' or toy_status = 'PROCESSING') " . $order_by . ";";

$sth = $pdo->prepare($query_toys);

$array = array($search_str, $search_str, $search_str, $search_str, $search_str, $search_str, $category_str, $age_str, $location_str);

$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");

if (isset($_GET['r'])) {
    $toprint = sizeof($result);
} else {
    $toprint = 100;
}
$total = sizeof($result);
if (sizeof($result) < 100) {
    $toprint = sizeof($result);
    $toprint_txt = " ";
} else {
    $link = '<a class="button_small" href="toys.php?r=' . $toprint . '">Showall</a>';
    $toprint_txt = $toprint . ' of ' . $total . " shown. " . $link;
}

for ($ri = 0; $ri < $toprint; $ri++) {
    $row = $result[$ri];
    $category = $row["category"];
    $toyname = $row["toyname"];
    $age = $row["age"];
    $toy_holds = $row['toy_holds'];
    //$desc1 = $row["desc1"];
    //$comments = $row["comments"];
    $toy_status = $row["toy_status"];
    //$cost = $row['cost'];
    $no_pieces = $row['no_pieces'];
    $status = $row["status"];
    //$supplier = $row['supplier'];
    //$date_purchase = $row["date_purchase"];
    $borname = $row['borname'];
    $borrower_name = $row['borname'];
    //$trans_phone = $row['transphone'];
    if ($multi_location == 'Yes'){
      $location_description = $row['location_description'];  
    }else{
       $location_description = ''; 
    }
    
    $rent = $row['rent'];
    //$return = $row['return'];
    $manufacturer = $row['manufacturer'];
    $sub_category = $row['sub_category'];



    if ((strpos($borname, '&') !== false)) {
        $borname_small = explode('&', $borname);
    } else {
        if ((strpos($borname, ' and ') !== false)) {
            $borname_small = explode(' and ', $borname);
        } else {
            if ((strpos($borname, 'Expires') !== false)) {
                $borname_small = explode('Expires', $borname);
            } else {

                $borname_small[0] = $borname;
            }
        }
    }



    //$reserve_borname = $row['reserve_borname'];
    //$phone = $row['phone'];
    $due = $row['due'];

    //$total = $total + 1;
    //$format_date_purchase = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    //$format_date_reserve = substr($row['end'], 8, 2) . '-' . substr($row['end'], 5, 2) . '-' . substr($row['end'], 0, 4);

    if ($row['due'] != '') {
        $format_date_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);
    } else {

        $format_date_due = '';
    }


    $id = $row["id"];
    $idcat = $row["idcat"];
    $link_toy = 'update/toy_detail.php?idcat=' . $idcat;
    $onclick_toy = 'javascript:location.href="' . $link_toy . '"';

    $result_txt .= "<tr border='1' class='item' id='red' ondblclick='" . $onclick_toy . "'>";
    $result_txt .= '<td border="1" width="30px">' . $id . '</td>';
    //$result_txt .= '<tr border="1"><td border="1" width="50">' . $id . '</td>';
    //$result_txt .= '<td width="50" align="left">' . $idcat . '</td>';
    $result_txt .= '<td class="idcat" width="50px" align="center">' . $idcat . '</td><td width="50px" align="center"><input type="checkbox" ></td>';
    if ($row['stype'] != '') {
        $result_txt .= '<td width="80px" align="left" style="padding-left: 5px;">' . $category . ' <font color="blue">' . $row['stype'] . '</font></td>';
    } else {
        $result_txt .= '<td width="20px"  align="left" style="padding-left: 5px;">' . $category . '</td>';
    }



        if ($chargerent == 'Yes') {
            $result_txt .= '<td width="20px" align="center">' . $rent . '</td>';
        } else {
            $result_txt .= '<td align="center"></td>';
        }
    




    $result_txt .= '<td width="220px" align="left">' . $toyname . '</td>';

    if ($_SESSION['shared_server']) {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
        }
    } else {
        $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        if (file_exists($file_pic)) {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
        } else {
            $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
        }
    }
    $pic_img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';
    //$pic = "'../../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . ".jpg'";
    $result_txt .= '<td width="50" align="center"><a href="update/toy_detail.php?idcat=' . $idcat . '" onmouseover="showtrail(175,220, \'' . $pic_url . '\');" onmouseout="hidetrail();" class="button_small"/>View</a>';
    $img = '<img height="200px" src="' . $pic_url . '" alt="toy image">';

    if ($due != '') {
        $loan_status = '<td BGCOLOR=lightgreen width="80" align="center">ON LOAN</td>';
        if ($row['due'] < $today) {
            $loan_status = '<td BGCOLOR=pink width="80" align="center">' . $overdue_txt . '</td>';
        }
    } else {
        if ($toy_status == 'PROCESSING') {
            $loan_status = '<td BGCOLOR=yellow width="80" align="center">PROCESSING</td>';
        } else {
            if ($toy_holds > 0) {
                $loan_status = '<td width="80"  bgcolor="#F660AB" align="center">ON HOLD</td>';
            } else {
                $loan_status = '<td width="80" align="center">IN LIBRARY</td>';
            }
        }
    }

    $result_txt .= $loan_status;
    //$result_txt .= '<td width="130" align="left">'. $supplier . '</td>';
    //$result_txt .= '<td>' . $return . '</td>';
    if ($due != '') {
        $result_txt .= '<td align="left"><font color="blue">' . $borname_small[0] . '</font></td>';
    } else {
        if ($location_description != '') {
            $result_txt .= '<td align="left">' . $location_description . '</td>';
        } else {
            $result_txt .= '<td align="left" width="100px"></td>';
        }

        // add location here!! michelle
    }
    $result_txt .= '<td width="70px">' . $format_date_due . '</td>';
    if ($color == 'Yes') {
        $result_txt .= '<td align="left" width="50px">' . $row['color'] . '</td>';
    } else {
        if ($sub_category == '') {
            $result_txt .= '<td align="left" width="80px">' . substr($row['manufacturer'], 0, 12) . '</td>';
        } else {
            $result_txt .= '<td align="left" width="80px">' . substr($row['sub_category'], 0, 10) . '</td>';
        }
    }

    //$result_txt .= '<td width="90" align="left">' . $file_pic . '</td>';
    $result_txt .= '<td width="90px">' . substr($age, 0, 12) . '</td>';
    //$result_txt .= '<td width="70">' . $format_date_purchase . '</td>';

    $result_txt .= '<td width="10px" align="center">' . $no_pieces . '</td>';
}
$result_txt .= '</tr></table>';
$result_txt .= $toprint_txt;
$search_str = '';

if ($str_category != '') {
    $search_str .= $search_str . ' Filtered on Category = ' . $str_category . ' ';
}
if ($age_str != '%%') {
    $search_str .= $search_str . ' Filtered on Age = ' . $age_str . ' ';
}
if ($search != '') {
    $search_str .= $search_str . ' Filtered on String = ' . $search . ' ';
}
$selected_toys = '';
if (isset($_SESSION['idcat_select'])) {
    $selected_toys = str_replace("'", " ", $_SESSION['idcat_select'], $count);
}


if ($search_str == '') {
    print '<div id="closed"><table width="100%"><tr><td width= 50%><font color="red">Double click on row to go to toy.</font></td>';
} else {
    print '<div id="open"><table width="100%"><tr><td width= 50%><h1 align="left">' . $search_str . '</h1></td>';
}

print '<td><h1 align="right">Total: ' . $total . '</h1></td></tr>';

if ($selected_toys != '') {
    print '<tr><td width="90%"><font color="blue"><strong>' . $count / 2 . ' toys have been selected: ' . $selected_toys . '</strong></font></td></tr>';
}

print '</table></div>';

print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
if ($chargerent == 'Yes') {
    print '<tr style="color:green"><td>Toy#</td><td>Id</td><td>Select</td><td>Cat</td><td></td><td>Toyname</td><td>Picture</td><td>Status</td><td>Location</td><td>Available from</td><td></td><td>Age</td><td>No</td><tr>';
} else {
    print '<tr style="color:green"><td>Toy#</td><td>Id</td><td>Select</td><td>Cat</td><td>Toyname</td><td>Picture</td><td>Status</td><td>Location</td><td>Available from</td><td></td><td>Age</td><td>No</td><tr>';
}

print $result_txt;
?> 
