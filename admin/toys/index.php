<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>
    <body>
        <div class="container-fluid">
            <?php include( dirname(__FILE__) . '/../menu.php'); ?>
  
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Toys Menu</h2><br><br>

                    </div>
                    <div class="col-sm-5">
                        <br><a href='../home/index.php' class ='btn btn-colour-yellow'>Home</a> 
                        <a href='../toys/index.php' class ='btn btn-primary'>Toys</a> 
                        <a href='../members/index.php' class ='btn btn-colour-maroon'>Members</a> 
                        <a href='../select_boxes/index.php' class ='btn btn-success'>Select Boxes</a>
                    </div>
                    <div class="col-sm-1">
                        <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=toysa' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <h4>Toy Lists</h4>
                        <br><a style="width: 150px;" href='../toys/toys.php' class ='btn btn-primary'>Current Toy List</a><br>
                        <br><a style="width: 150px;" href='../overdue/toys_onloan.php' class ='btn btn-primary'>Toys On Loan</a><br>
                        <br><a style="width: 150px;" href='../toys/no_pictures/toys.php' class ='btn btn-primary'>Toys with no picture</a><br>
                        <br><a style="width: 150px;" href='../toys/locked/locked.php' class ='btn btn-primary'>Locked Toys</a><br>
                        <br><a style="width: 150px;" href='../toys/locked/withdrawn.php' class ='btn btn-primary'>Withdrawn Toys</a><br>
                        <br><a style="width: 150px;" href='../loans/hold/holds_list.php' class ='btn btn-primary'>Toy Holds</a><br>
                        <br><a style="width: 150px;" href='../reserve_toys/toys_party.php' class ='btn btn-primary'>Toy Reservations</a><br>
                    </div>
                    <div class="col-sm-3">
                        <h4>Add a Toy</h4>
                        <br><a style="width: 150px;" href='../toys/update/new.php' class ='btn btn-primary'>New Toy</a><br>
                        <br><a style="width: 150px;" href='../toys/copy/new.php' class ='btn btn-primary'>Copy a Toy</a><br>

                    </div>

                    <div class="col-sm-3">
                        <h4>Stocktake and Parts</h4>
                        <br><a style="width: 150px;" href='../toys/parts/parts.php' class ='btn btn-primary'>List of Toy Parts</a><br>
                        <br><a style="width: 150px;" href='../toys/alerts/alerts.php' class ='btn btn-primary'>List of Toy Alerts</a><br>
                        <br><a style="width: 150px;" href='../toys/stocktake/stocktake.php' class ='btn btn-primary'>Stock Take</a><br>

                    </div>
                    <div class="col-sm-3">
                        <h4>Select Boxes</h4>
                        <br><a style="width: 150px;" href='../toys/category/new.php' class ='btn btn-primary'>Toy Categories</a><br>
                        <br><a style="width: 150px;" href='../toys/sub_category/categories.php' class ='btn btn-primary'>Toy Sub Categories</a><br>
                        <br><a style="width: 150px;" href='../toys/age/ages.php' class ='btn btn-primary'>Toy Age Groups</a><br>
                        <br><a style="width: 150px;" href='../toys/condition/conditions.php' class ='btn btn-primary'>Toy Condition</a><br>
                        <br><a style="width: 150px;" href='../toys/storage/storages.php' class ='btn btn-primary'>Toy Storage</a><br>
                        <br><a style="width: 150px;" href='../toys/warnings/warnings.php' class ='btn btn-primary'>Toy Warnings</a><br>

                    </div>
                </div>
        </div>
        <?php include('../footer.php'); ?>
    </body>
</html>