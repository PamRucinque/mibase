<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include('../header.php');
        ?> 
        <script>
            function go() {
                var idcats = "";

                $("div.item").each(function () {
                    $this = $(this);
                    //  console.log($this);
                    var idcat = $this.find(".idcat").html();
                    var value = $this.find(":checkbox").is(':checked');
                    //   console.log(idcat + " " + value);
                    if (value) {
                        idcats += "'" + idcat + "',";
                    }

                });

                console.log(idcats);


                if (idcats.length > 0) {
                    idcats = idcats.substring(0, idcats.length - 1);
                    $("#idcats_list").val(idcats);
                    $("#select_list").val(idcats);
                    //document.forms["reports"].submit();
                    //document.reports.submit();
                    return true;
                } else {
                    alert('Select Report to Print, No toys selected!');
                    return false;
                }

                //alert(idcats);
            }
            //alert(idcats);

            function get_excel() {
                document.getElementById("export_excel").submit();
            }

            /**
             *  function to set all rows true
             */

            function select_all() {

                $("div.item").each(function () {
                    $this = $(this);
                    $this.find(".idcat").html();
                    $this.find(":checkbox").show();
                    $this.find(":checkbox").prop("checked", true);
                    //$this.find("td :checkbox").prop("checked", true);
                });
            }

            function show_print() {
                $("div.item").each(function () {
                    $this = $(this);
                    this.onclick = false;
                    $this.find(".idcat").html();
                    $this.find(":checkbox").show();
                });
            }
            function disable_click() {
                $("div.item").each(function () {
                    $this = $(this);
                    $this.find(".row").html();
                    $this.find(":row").onlclick = null;
                    //$this.find("td :checkbox").prop("checked", true);
                });
            }


        </script>
        <script  type="text/javascript" src="../js/jquery-2.0.3.min.js"></script>
    </head>
    <?php
    $multi_location = $_SESSION['settings']['multi_location'];


//print_r($_SESSION);
    ?>
    <body>
        <div class="container-fluid" style="padding-left: 0px;padding-right: 0px;">
            <?php include('../menu.php'); ?>
        </div>
        <div class="container-fluid" style="padding-left: 20px;padding-right: 20px;">
            <?php
            $idcat = '';
            include('../header_detail/header_detail.php');
            //include('../reports/baglabels_toys.php');
            $chargerent = '';
            $color = '';
            //include('../get_settings.php');
            if (isset($_POST['age'])) {
                $_SESSION['age'] = $_POST['age'];
            }
            if (isset($_POST['category'])) {
                $_SESSION['category'] = trim($_POST['category']);
            }
            if (isset($_POST['reset'])) {
                $_SESSION['category'] = '';
                $_SESSION['age'] = '';
                $_SESSION['loc_filter'] = '';
                $_SESSION['search'] = '';
                $_SESSION['idcat_select'] = '';
            }

            $link = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['report_server_url'] . '/PdfReport';
            if (isset($_SESSION['idcat'])) {
                $idcat = $_SESSION['idcat'];
            }

            $toy_to_print = "'" . $idcat . "'";
            $sql_report = "select * from toys;";

            if (isset($_POST['reset_2'])) {
                //echo 'Hello Gregor reset2';
                $_SESSION['idcat_select'] = '';
                $_SESSION['search'] = '';
                $_SESSION['category'] = '';
                $_SESSION['age'] = '';
                $_SESSION['loc_filter'] = '';
            }

            //echo 'Hello Gregor' . $_POST['reset_2'] . '*';  

            $total = 0;
            if (isset($_POST['select_list'])) {
                if ($_POST['select_list'] != '') {
                    if ($_SESSION['idcat_select'] == '') {
                        $_SESSION['idcat_select'] .= $_POST['select_list'];
                    } else {
                        $_SESSION['idcat_select'] .= ',' . $_POST['select_list'];
                    }
                }
            }
            ?>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-3">
                    <form name="search_form" id="search_form" method="post" action="toys.php"> 
                        <input id="search" name="search" type="text" placeholder="Search toys.." value=""  class="form-control" onchange="this.form.submit();"/>
                        <input type="hidden" name="total" value="<?php echo $total; ?>">
                        <input type="hidden" name="select_list" id="select_list" value=""/>
                        <div style="padding-top: 5px;">
                            <input id="add" class="btn btn-info"  type="button" name="add" value="Add" onclick="go();this.form.submit();">  
                            <input type="submit" class="btn btn-danger" name ="reset_2" id ="reset_2" value="Reset"/> 
                        </div>
                    </form>
                </div>
                <div class="col-md-2">
                    <form name="search_form" method="post" action="toys.php"> 
                        <?php include('get_category.php'); ?>
                    </form>
                </div>
                <div class="col-md-2">
                    <form name="search_form" method="post" action="toys.php"> 
                        <?php include('get_age.php'); ?>
                    </form>
                </div>
                <div class="col-md-3">
                    <?php
                    if ($_SESSION['settings']['multi_location'] == 'Yes') {
                        include('get_location.php');
                    }
                    ?> 
                </div>

                <div class="hidden-xs col-md-3" style="float: right;">
                    <?php include('../reports/baglabels_toys.php'); ?>
                </div>

            </div>
        </div>


        <?php
        include('list.php');
        ?>



    </div>


</body>

</html>
