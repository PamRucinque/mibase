<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include('../header.php'); ?> 
    </head>    

    <body id="main_body" >
        <div id="form_container">
            <?php
            $ok = 'No';
            include('../menu.php');
            $session = $_POST['roster_session'];
            //$pieces = explode(":", $session);
            $weekday = $_POST['weekday'];
            $comments = $_POST['comments'];
            $comments = str_replace("'", "`", $comments);
            //$roster_session = trim($pieces[1]);

            if (isset($_POST['submit'])) {
                include('../connect.php');
                $role = trim($_POST['role']);
                $status = trim($_POST['status']);
                $now = date('Y-m-d H:i:s');
                if ($status == 'completed'){
                    $complete = 'complete = TRUE, ';
                }else{
                    $complete = 'complete = FALSE, ';
                }
                $query_roster = "UPDATE roster SET 
                    date_roster = '{$_POST['date_roster']}',
                    type_roster = '{$_POST['rostertype']}',
                    roster_session = '$session',
                    member_id = '{$_POST['member_id']}',
                    duration = '{$_POST['duration']}',
                    weekday = '$weekday',
                    session_role = '{$role}'," . $complete . "
                    status = '{$status}',
                    comments = '{$comments}',
                    modified = '{$now}'
                    WHERE id=" . $_POST['id'] . ";";

                $result_roster = pg_Exec($conn, $query_roster);
                //echo $query;
                //echo $connection_str;
                if (!$result_roster) {
                    echo "An UPDATE query error occurred.\n";
                    echo $query_roster;
                    //exit;
                } else {
                    echo "<br><h4>This Duty Roster was successfully saved.";
                    //echo 'format Toy id: ' . $format_toyid;
                    echo '<a class="button1" href="roster.php">Back to Roster</a>';
                    echo '<a class="button1_red" href="../members/update/member_detail.php?id=' . $_SESSION['borid'] . '">Member Details</a>';
                }
            } else {
                include('data/get_roster.php');
                include('roster_form.php');
            }
            ?>

        </div> 
    </body>
</html>