<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
//include( dirname(__FILE__) . '/data/get_settings.php');
?>

<style type="text/css">
    tr:hover { 
        color: red; 
        background-color:#F3F781; }
</style>  
<script>
    function updateRoster(rid) {
        console.log("updateRoster" + rid);
        $.ajax({url: "complete.php",
            data: {roster_id: rid},
            success: function (result) {
                if (result.match("saved")) {
                    //update the member button
                    $("#r" + rid + " .cid").html("<font color='green'>completed</font>");
                    $("#r" + rid + " .complete").html("");
                    //update the member pick list for this row
                    //$("#r" + rid + " .mpl").html(memberPickList(rid, memid));
                } else if (result.match("error.*")) {
                    alert(result);
                    //put the member selection box back the way it was
                    var memid1 = parseInt($("#r" + rid + " .cid").text());
                    //$("#r" + rid + " .mpl").html(memberPickList(rid, memid1));
                } else {
                    alert(result);
                    //put the member selection box back the way it was
                    //var memid1 = parseInt($("#r" + rid + " .mid").text());
                    //$("#r" + rid + " .mpl").html(memberPickList(rid, memid1));
                }
            },
            error: function () {
                alert('error saving');
                //put the member selection box back the way it was
                //var memid1 = parseInt($("#r" + rid + " .mid").text());
                //$("#r" + rid + " .mpl").html(memberPickList(rid, memid1));
            }});
    }



</script>
<?php
if (isset($_GET["id"])){
    $id = $_GET["id"];
}

//include( dirname(__FILE__) . '/forms/db.php');
//include( dirname(__FILE__) . '/../connect.php');
$last_weekday = '';
$last_date_roster = '';
$str_heading = '';
$color = '';

//$query = "select * from holmes_files order by id;";
$query = "SELECT roster.*, borwrs.firstname as firstname, borwrs.surname as surname, borwrs.emailaddress , borwrs.phone2 as phone2, borwrs.wwc as wwc 
FROM Roster LEFT JOIN Borwrs ON Roster.member_id = Borwrs.ID 
WHERE 
  roster.date_roster::text >= '" . $start . "'
  AND roster.date_roster::text <= '" . $end . "' 
  AND roster.weekday LIKE '%" . $weekday . "%' 
  AND roster.session_role LIKE '%" . $roster_type . "%'
      AND roster.type_roster != 'Exemption'";
if ($_SESSION['member'] == 'empty') {
    $query .= " AND roster.member_id = 0 ";
}
$query .= " ORDER BY  date_roster asc, roster.location, roster_session, roster.id asc;";
//echo $query;
//$numrows = pg_numrows($result);
$count = 1;
$total = 0;
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);
$str_print = '';
$count_member = 0;
$count_coord = 0;
$count_member_blank = 0;
$count_coord_blank = 0;


for ($ri = 0; $ri < $numrows; $ri++) {
    //echo "<tr>\n";

    $row = pg_fetch_array($result, $ri);
    $weekday = date('l', strtotime($row['date_roster']));
    $month = date('M', strtotime($row['date_roster']));
    $date_roster = $row['date_roster'];
    $rowid = 'r' . $row['id'];
    $wwc = $row['wwc'];

    $session = trim($row['roster_session']);
    $status = trim($row['status']);
    if (($status != 'Closed') && ($status != 'Note')) {
        $total = $total + 1;
    }
    if (($status != 'Closed') && ($status != 'Note')) {

        if ($row['member_id'] == 0) {
            if (trim($row['session_role']) == 'Co-ordinator') {
                $count_coord_blank = $count_coord_blank + 1;
            } else {
                $count_member_blank = $count_member_blank + 1;
            }
        } else {
            if (trim($row['session_role']) == 'Co-ordinator') {
                $count_coord = $count_coord + 1;
            } else {
                $count_member = $count_member + 1;
            }
        }
    }


    if (($last_weekday == $weekday)||($last_date_roster == $date_roster) ) {
        //&& ($last_session == $session)
        $count = $count + 1;
        if ($color == 'blue') {
            $color = 'blue';
        } else {
            $color = 'white';
        }
    } else {
        $count = 1;
        if ($color == 'blue') {
            $color = 'white';
        } else {
            $color = 'blue';
        }
    }
    $format_date_roster = substr($row['date_roster'], 8, 2) . '-' . $month . '-' . substr($row['date_roster'], 0, 4);

    if ($color == 'blue') {
        $weekday_row = '<td width="10%" style="background-color:lightblue;">' . $format_date_roster . '</td>';
        $session_row = '<td width="25%" align="left" style="background-color:lightblue;">' . $row['roster_session'] . '</td>';
        if ($row['weekday'] == '') {
            $format_weekday = '<td width="10%" align="left"  style="background-color:lightblue;">' . $row['type_roster'] . '</td>';
        } else {
            $format_weekday = '<td width="7%" align="left"  style="background-color:lightblue;">' . $row['weekday'] . '</td>';
        }
    } else {
        $weekday_row = '<td width="10%" style="background-color:white;">' . $format_date_roster . '</td>';
        $session_row = '<td width="15%" align="left" style="background-color:white;">' . $row['roster_session'] . '</td>';
        if ($row['weekday'] == '') {
            $format_weekday = '<td width="10%" align="left"  style="background-color:white;">' . $row['type_roster'] . '</td>';
        } else {
            $format_weekday = '<td width="7%" align="left"  style="background-color:white;">' . $row['weekday'] . '</td>';
        }
    }

    $last_weekday = $weekday;
    $last_date_roster = $date_roster;
    $last_session = $session;
    $last_day = $row['date_roster'];
    //$ext = strtolower($path_parts["extension"]);
    if (($status == 'Closed') || ($status == 'Note')) {
        $str_print .= '<tr id="' . $rowid . '" style="background-color:#F3F781;">';
    } else {
        if ($row['approved'] == 't') {
            $approved = "Yes";
            $str_print .= '<tr id="' . $rowid . '">';
        } else {
            $approved = "No";
            $str_print .= '<tr id="' . $rowid . '" style="background-color:lightpink;">';
        }
    }
    if ($row['complete'] == 't') {
        $completed = "Yes";
        $str_print .= '<tr id="' . $rowid . '">';
    } else {
        $completed = "No";
        //echo '<tr id="' . $rowid . '" style="background-color:#F3F781;">';
        echo '<tr id="' . $rowid . '">';
        if ($row['approved'] == 'f') {
            $str_print .= '<tr id="' . $rowid . '" style="background-color:lightpink;">';
        }
    }

    //$authorised = $row['repairs_authorised'];



    $str_print .= '<td width="3%" class="rid">' . $row['id'] . '</td>';
    $str_print .= '<td width="3%">' . $count . '</td>';

    $str_print .= $weekday_row;
    //echo '<td width="50">' . $format_date_roster . '</td>';
    //echo '<td width="80" align="left">' . $row['weekday'] . '</td>';
    $str_print .= $format_weekday;
    $str_print .= $session_row;
    if ($row['location'] != '') {
        if ($row['comments'] == '') {
            if (($wwc != null)&&($wwc != '')){
                $str_print .= '<td width="5%" align="center" style="background-color:lightgreen;">' . substr($row['location'], 0, 3) . '</td>';
            }else{
               $str_print .= '<td width="5%" align="center">' . substr($row['location'], 0, 3) . '</td>'; 
            }
            
        } else {
            $str_print .= '<td width="5%"><div id="popup"><a>Notes<span>' . $row['location'] . ' ' . $row['comments'] . '</span></a></span></a></div></td>';
        }
    } else {
        if ($row['comments'] == '') {
            $str_print .= '<td width="5%"></td>';
        } else {
            if ($roster_text == 'Yes') {
                $str_print .= '<td width="5%">' . $row['comments'] . '</td>';
            } else {
                $str_print .= '<td width="5%"><div id="popup"><a>Notes<span>' . $row['comments'] . '</span></a></span></a></div></td>';
            }
        }
    }

    //echo '<td width="120px" align="left">' . $row['roster_session'] . '</td>';
    $str_print .= '<td class="mid">' . $row['member_id'] . '</td>';
    //echo '<td class="cid">' . $row['status'] . '</td>';
    if (($status == 'Closed') || ($status == 'Note')) {
        $str_print .='<td width="30%" align="center"><strong>' . $status . ': ' . $row['comments'] . '</strong></td>';
    } else {
        $str_print .='<td width="20%" class="mpl"></td>';
    }
    if (($status == 'Closed') || ($status == 'Note')) {
        $str_print .= "<td width='5%' align='center'></td>";
    } else {
        $str_print .= "<td width='5%' align='center'><a class='button_small_green' href='change_role.php?id=" . $row['id'] . "'>" . $row['session_role'] . "</a></td>";
    }

    //echo '<td width="30" align="center"> ' . $row['duration'] . '</td>';
    if ($approved == 'Yes') {
    if ($status == 'pending') {
        $str_print .= "<td class = 'complete'><button class = 'button_small' onclick = 'updateRoster(" . $row['id'] . ")' > Complete</button></td>";
    } else {
        if ($status == 'Closed'){
            $str_print .= '<td>' . $row['type_roster'] . '</td>';
        }else{
            $str_print .= '<td></td>';
        }
        
    }
    } else {
        $str_print .= "<td width = '5%'><a class = 'button_small' href = 'approve_roster.php?id=" . $row['id'] . "' > Approve</a></td>";
    }


    if (($status == 'pending') || ($status == 'no show')) {
        $status = '<font color="red">' . $row['status'] . '</font>';
    } else {
        $status = '<font color="green">' . $row['status'] . '</font>';
    }

    $str_print .= "<td align = 'center' class = 'cid'>" . $status . "</td>";

    //$result_txt .= '<td  class="mpl" width="70px" align = "center">' . $row['discoverytype'] . '</td>';

    //$str_print .= "<td width = '5%'><a class = 'button_small_red' href = 'delete_roster.php?id=" . $row['id'] . "' > Delete</a></td>";
    $str_print .= "<td width = '5%'><a class = 'button_small_green' href = 'edit_roster.php?id=" . $row['id'] . "' > Edit</a></td>";

    //echo '<td width="50" align="center">' . $download2 . '</td>';
    //echo '<td width="100">' . $row['man_html'] . '</td>';
    $str_print .= '</tr>';
}

//$str_print .= '<tr height="50"></tr>';
$str_print .= '</table>';

if ($numrows > 0) {
    $str_heading = '<font color="blue"><strong>Members Allocated: ' . $count_member . ' Members Required: ' . $count_member_blank . '</font><font color="red">  Total: ' . $total . '</font></strong><br>';
    if (($count_coord + $count_coord_blank) > 0) {
        $str_heading .= '<font color="green"><strong> Coord Allocated: ' . $count_coord . ' Coord Required: ' . $count_coord_blank . '</font></strong><br>';
    }
    $str_heading .= '<table border="1" id="res" width="100%" style="border-collapse:collapse;
                border-color:grey">';
    $str_heading .= '<tr><td>id</td><td>#</td><td>Date</td><td>Day</td><td>Time</td><td></td><td>id</td><td>Name</td><td></td><td></td><td colspan="3"></td></tr>';
}
echo $str_heading;
echo $str_print;
?>


