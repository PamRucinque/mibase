<?php

function get_end() {
    include('../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $query_count = "select max(date_roster) as max_date from roster;";
    $max = date('Y-m-d', strtotime('last day of this month', time()));
    //echo $query_count;
    $result_count = pg_Exec($conn, $query_count);
    $numrows = pg_numrows($result_count);
    if ($numrows > 0) {
        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = pg_fetch_array($result_count, $ri);
            $max = $row['max_date'];
        }
    }

    //echo $countrows;
    return $max;
}


