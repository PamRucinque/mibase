<div class = "row roster mibase" style="padding-bottom: 2px;font-size: medium;<?php echo $bg_color; ?>" 
     onclick='<?php //echo $onclick;       ?>'>
    <div class="col-sm-5 col-xs-12" style="overflow: hidden;" onclick='<?php echo $onclick;?>'>
        <div class="hidden-xs col-sm-2" style="color: grey;" id="<?php echo 'app_' . $roster['id']; ?>"><?php echo $str_approve; ?></div>
        <div class="col-sm-3 hidden-xs"><b><?php echo $roster['roster_date']; ?></b></div>
        <div class="col-sm-3 hidden-xs"><?php echo $roster['weekday']; ?></div>
        <div class="hidden-lg hidden-md hidden-sm col-xs-12" style="font-size: large;"><b><?php echo $roster['weekday'] . ', ' . $roster['roster_date_sm']; ?></b></div>
        <div class="col-sm-4 col-xs-12" style="float:right;"><?php echo $roster['roster_session']; ?></div>

    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="col-xs-1 col-sm-1"><?php echo '<strong>' . $location . '</strong>'; ?></div>
        <div class="hidden-xs col-sm-2 mid" id="<?php echo 'mid_' . $roster['id']; ?>"><?php echo $roster['member_id']; ?></div>
        <div class="col-xs-12 col-sm-8 mpl" style="align: left;" id="<?php echo 'mpl_' . $roster['id']; ?>"><?php echo $borname; ?></div>
        <div class="col-xs-12 col-sm-12"><font color="grey"><?php echo $comments; ?></font></div>
    </div>

    <div class="col-xs-12 col-sm-3" onclick='<?php echo $onclick; ?>'>
        <div class="col-xs-3 col-sm-3" id="<?php echo 'btn_' . $roster['id']; ?>"><?php echo $str_edit; ?></div>
        <div class="col-xs-3 col-sm-3"  id="<?php echo 'role_' . $roster['id']; ?>"><?php echo $str_role; ?></div>
        <div class="col-xs-3 col-sm-3" id="<?php echo 'cpl_' . $roster['id']; ?>"><?php echo $str_complete; ?></div>
        <div class="col-xs-3 col-sm-3"><?php echo $str_edit_roster; ?></div>

    </div>

</div>
