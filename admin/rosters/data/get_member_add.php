<?php

require(dirname(__FILE__) . '/../../mibase_check_login.php');

$query = "select firstname, surname, b.id, expired, substring(b.rostertype from 9 for 3) as pref1,wwc, duties,
substring(b.rostertype2 from 9 for 3) as pref2,
 (m.duties - (SELECT coalesce(sum(roster.duration),0) FROM roster WHERE roster.member_id = b.id AND (roster.date_roster >= (b.expired - (m.expiryperiod * '1 month'::INTERVAL))))) as duties 
from borwrs b
left join membertype m on m.membertype = b.membertype
where member_status = 'ACTIVE' order by surname, firstname;";

$dbconn = pg_connect($_SESSION['connect_str']);
$result = pg_Exec($dbconn, $query);

$str_pick = "<select id='borid_" . $_GET['q'] . "' name='borid' class='form-control' onchange='update_roster(this.value," . $_GET['q'] . ");'  >\n";

$numrows = pg_numrows($result);
//echo '<option value="" selected="selected"></option>';
//echo "<option value='" . $_SESSION['category'] . "' selected='selected'>" . $_SESSION['category'] . "</option>";
$str_pick .= "<option value='" . $_GET['b'] . "' selected='selected'>" . $_GET['n'] . "</option>";
$str_pick .= "<option value='0'></option>";

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    if ($row['duties'] > 0) {
        if ($row['pref1'] != '') {
            $str_duties = ' (' . strval(round($row['duties'])) . ' ' . $row['pref1'] . ' ' . $row['pref2'] . ')';
        } else {
            $str_duties = ' (' . strval(round($row['duties'])) . ')';
        }
    } else {
        $str_duties = '';
    }
    if ($row['expired'] < date('Y-m-d')) {
        $str_duties .= ' E';
    }
    if (($row['wwc'] != null) && ( $row['wwc'] != '')) {
        $str_duties .= ' W';
    }
    $borname = $row['surname'] . ', ' . $row['firstname'] . $str_duties;
    $str_pick .= "<option value='{$row['id']}'>{$borname}
    </option>\n";
}

$str_pick .= "</select>\n";

echo $str_pick;



