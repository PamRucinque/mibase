<?php

require( dirname(__FILE__) . '/../mibase_check_login.php');

include('../connect.php');
$x = 0;

$sql = "select b.surname, b.firstname, b.duties, b.expired, b.wwc, b.pref1, b.pref2,
    to_char(roster.date_roster,'dd Mon yy') as roster_date,to_char(roster.date_roster,'dd Mon yyyy') as roster_date_sm, roster.*
from roster 
left join (select firstname, surname, b.id, expired, substring(b.rostertype from 9 for 3) as pref1,wwc,
substring(b.rostertype2 from 9 for 3) as pref2,
 (m.duties - (SELECT coalesce(sum(roster.duration),0) FROM roster WHERE roster.member_id = b.id AND (roster.date_roster >= (b.expired - (m.expiryperiod * '1 month'::INTERVAL))))) as duties 
from borwrs b
left join membertype m on m.membertype = b.membertype) b on b.id = roster.member_id
where 
roster.date_roster::text >= ? 
AND roster.date_roster::text <= ?  
AND roster.weekday LIKE ? 
AND roster.session_role LIKE ? ";
if ($_SESSION['member'] == 'empty') {
    $sql .= " AND roster.member_id = 0 ";
}
$sql .= " ORDER BY  date_roster asc, roster.location, roster_session, roster.id asc;";

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$sth = $pdo->prepare($sql);
$array = array($start, $end, $weekday, $roster_type);
$sth->execute($array);
$result = $sth->fetchAll();
//echo ' size ' . sizeof($result);
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $_SESSION['error'] = "An  error occurred.\n";
    $_SESSION['error'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[2] . '<br>';
}
$numrows = $sth->rowCount();

$required = 0;
$allocated = 0;
$total = 0;
for ($ri = 0; $ri < $numrows; $ri++) {
    $tot = $result[$ri];
    if ($tot['member_id'] == 0) {
        $required = $required + 1;
    } else {
        $allocated = $allocated + 1;
    }
}
$total = $required + $allocated;

include('heading.php');
$bg_color = 'background-color: white;';


for ($ri = 0; $ri < $numrows; $ri++) {
    $roster = $result[$ri];
    if ($roster['duties'] > 0) {
        if ($roster['pref1'] != '') {
            $str_duties = ' (' . strval(round($roster['duties'])) . ' ' . $roster['pref1'] . ' ' . $roster['pref2'] . ')';
        } else {
            $str_duties = ' (' . strval(round($roster['duties'])) . ')';
        }
    } else {
        $str_duties = '';
    }
    if ($roster['expired'] < date('Y-m-d')) {
        $str_duties .= '<font color="red"> E</font>';
    }
    if (($roster['wwc'] != null) && ( $roster['wwc'] != '')) {
        $str_duties .= ' W';
    }
    if ($roster['member_id'] == 0) {
        $borname = '';
    } else {
        if ($roster['surname'] == null) {
            $borname = '<font color="red">CHECK</font>';
        } else {
            $borname = $roster['surname'] . ', ' . $roster['firstname'] . $str_duties;
        }
    }

    $weekday = date('l', strtotime($roster['date_roster']));
    $ref = '../roster/edit_roster.php?id=' . $roster['id'];
    $str_edit_roster = " <a class ='btn btn-info btn-sm' style='padding: 1px 5px;' href='" . $ref . "'>Edit</a>";

    if ($roster['member_id'] == 0) {
        $str_edit = " <a class ='btn btn-success btn-sm' style='padding: 1px 5px;' onclick='get_pickbox(" . $roster['id'] . ");'>Allocate</a>";
    } else {
        $str_edit = " <a class ='btn btn-primary btn-sm' style='padding: 1px 5px;' onclick='get_pickbox(" . $roster['id'] . ");'>Change</a>";
    }
    $onclick = 'get_pickbox(' . $roster['id'] . ');';
    $on_exit = 'on_exit(' . $roster['id'] . ');';
    if (trim($roster['status']) == 'pending') {
        $str_complete = " <a class ='btn btn-danger btn-sm' style='padding: 1px 5px;' onclick='update_complete(" . $roster['id'] . ");'>Complete</a>";
    } else {
        $str_complete = $roster['status'];
    }
    if (!$roster['approved']) {
        $str_approve = " <a class ='btn btn-success btn-sm' style='padding: 1px 5px;' onclick='update_approve(" . $roster['id'] . ");'>Approve</a>";
    } else {
        $str_approve = $roster['id'];
    }

    $str_role = " <a class ='btn btn-colour-maroon btn-sm' id='rolebtn_" . $roster['id'] . "' style='padding: 1px 5px;' onclick='update_role(" . $roster['id'] . ");'>" . $roster['session_role'] . "</a>";

    $comments = substr($roster['comments'], 0, 18);
    $location = substr($roster['location'], 0, 2);

    include('row_bg_color_old.php');
    include('row.php');

    $onclick = '';

    $last = $roster['date_roster'];
}


