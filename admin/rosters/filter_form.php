<script src="../js/jquery-1.9.1.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "yy-mm-dd",
            showOtherMonths: true

        };
        $("#start").datepicker(pickerOpts);
        $("#end").datepicker(pickerOpts);
    });


</script>
<form name="roster" id="roster" method="post" action="index.php" > 
    <div class="row">
        <div class="col-md-2 col-xs-6">
            Date From: <input type="text" class="form-control" name="start" id ="start" align="LEFT" value="<?php echo $start; ?>"  onchange="this.form.submit()"></input>
        </div>
        <div class="col-md-2 col-xs-6">
            To: <input type="text" name="end" class="form-control" id ="end" align="LEFT" value="<?php echo $end; ?>"  onchange="this.form.submit()"></input>
        </div>
        <div class="col-md-2 col-xs-6">
            <?php include('data/get_weekday_filter.php'); ?>
        </div>
        <div class="col-md-2 col-xs-6">
            <?php include('data/get_roster_type_filter.php'); ?>
        </div>
        <div class="col-md-1 col-xs-6">
            <?php
            if ($_SESSION['member'] == 'empty') {
                echo '<br><input id="all" class="btn btn-primary" type="submit" name="all" value="All" />';
            } else {
                echo '<br><input id="empty" class="btn btn-primary" type="submit" name="empty" value="Vacant" />';
            }
            ?>
        </div>
        <div class="col-md-1 col-xs-6" style="padding-left: 10px;">
            <?php
            echo '<br>  <input id="reset" class="btn btn-danger" type="submit" name="reset" value="Reset" />';
            ?>
        </div>


    </div>
</form>

