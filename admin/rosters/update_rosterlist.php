<?php

require(dirname(__FILE__) . '/../mibase_check_login.php');
if (isset($_GET['rosterid']) && isset($_GET['borid'])) {
    include('../connect.php');
    $rosterid = $_GET['rosterid'];
    $borid = $_GET['borid'];
    $sql = "Update roster SET member_id = ?, modified = now() WHERE id = ?;";
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $sth = $pdo->prepare($sql);
    $array = array($borid, $rosterid);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $error = 'Error' . $stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>';
        echo $error;
        exit;
    } else {
        echo 'saved';
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
