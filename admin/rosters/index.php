<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <script>
            function get_pickbox(str) {

                var mpl = "mpl_" + str;
                var mid = "mid_" + str;
                var check = document.getElementById(mpl).innerHTML;
                //alert(check.substring(0,7));
                if (check.substring(0, 7) === "<select") {
                    //alert('hello');
                    document.getElementById(mpl).innerHTML = check;
                    return;
                }
                borid = document.getElementById(mid).innerHTML;
                borname = document.getElementById(mpl).innerHTML;
                //alert(borname);
                if (str === '') {
                    document.getElementById(str).innerHTML = "";
                    // $("#progress").hide();
                    return;
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else { // code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    document.getElementById(mpl).innerHTML = xmlhttp.responseText;
                }

                xmlhttp.open("GET", "data/get_member_add.php?q=" + str + "&&b=" + borid + "&&n=" + borname, true);
                xmlhttp.send();
                document.getElementById(mpl).focus();
            }

            function exit_select(str, mpl) {
                alert(str);
                //var mpl = "mpl_" + str;
               // var check = document.getElementById(mpl).innerHTML;
               // alert(check.substring(0, 7));
                //if (check.substring(0, 7) === "<select") {
                //    alert('hello');
                //    document.getElementById(mpl).innerHTML = check;
                //    return;
                // }
            }
            function test() {
                alert('hello');
            }

            function update_roster(str, mpl) {
                //alert(mpl);
                var borid = str;
                var rosterid = mpl;
                mid = "mid_" + mpl;
                mpl_result = "mpl_" + mpl;
                //alert(mid);
                document.getElementById(mid).innerHTML = str;
                // var myinput = document.getElementById("myinput");
                console.log("update_roster" + rosterid + " " + borid);

                $.ajax({url: "update_rosterlist.php",
                    data: {borid: borid, rosterid: rosterid},
                    success: function (result) {
                        if (result.match("saved")) {

                        } else {
                            alert(result);
                            //alert(id);
                            //document.getElementById(id).value = 'NEW';

                        }
                    },
                    error: function () {
                        alert('error saving');
                        //document.getElementById('member_status').selectedIndex = 0;

                    }});
                var bor_select = "borid_" + mpl;
                var btn = "btn_" + mpl;
                var sel_bor = document.getElementById(bor_select);
                var bor_text = sel_bor.options[sel_bor.selectedIndex].text;
                //document.getElementById(mid).focus();
                //alert(bor_text);
                document.getElementById(mpl_result).innerHTML = bor_text;
                //alert(borid);

                if (borid > 0) {
                    document.getElementById(btn).innerHTML = " <a class ='btn btn-primary btn-sm' style='padding: 1px 5px;' onclick='get_pickbox(" + mpl + ");'>Change</a>";
                } else {
                    //alert(borid);
                    document.getElementById(btn).innerHTML = " <a class ='btn btn-success btn-sm' style='padding: 1px 5px;' onclick='get_pickbox(" + mpl + ");'>Allocate</a>";

                }
            }
            function update_complete(str) {
                cpl = "cpl_" + str;
                rosterid = str;

                console.log("update_complete" + rosterid);

                $.ajax({url: "update_complete.php",
                    data: {rosterid: rosterid},
                    success: function (result) {
                        if (result.match("saved")) {

                        } else {
                            alert(result);
                            //alert(id);
                            //document.getElementById(id).value = 'NEW';

                        }
                    },
                    error: function () {
                        alert('error saving');
                        //document.getElementById('member_status').selectedIndex = 0;

                    }});

                document.getElementById(cpl).innerHTML = "completed";

            }
            function update_role(str) {
                roleid = "role_" + str;
                btn_id = "rolebtn_" + str;
                rosterid = str;
                var btn = document.getElementById(btn_id);

                var role = $('#rolebtn_' + str).text();
                //alert(role);
                if (role === 'Member') {
                    role_new = 'Co-ordinator';
                } else {
                    role_new = 'Member';
                }
                console.log("update_complete" + rosterid);

                $.ajax({url: "update_role.php",
                    data: {rosterid: rosterid, btn: role_new},
                    success: function (result) {
                        if (result.match("saved")) {
                            btn.innerHTML = role_new;
                        } else {
                            alert(result);
                            //alert(id);
                            //document.getElementById(id).value = 'NEW';

                        }
                    },
                    error: function () {
                        alert('error saving');
                        //document.getElementById('member_status').selectedIndex = 0;

                    }});

                document.getElementById(cpl).innerHTML = "completed";

            }
            function update_approve(str) {
                //alert(str);
                appid = "app_" + str;
                rosterid = str;
                console.log("update_approve" + rosterid);

                $.ajax({url: "update_approve.php",
                    data: {rosterid: rosterid},
                    success: function (result) {
                        if (result.match("saved")) {
                            btn.innerHTML = role_new;
                        } else {
                            alert(result);
                            //alert(id);
                            //document.getElementById(id).value = 'NEW';

                        }
                    },
                    error: function () {
                        alert('error saving');
                        //document.getElementById('member_status').selectedIndex = 0;

                    }});

                document.getElementById(appid).innerHTML = rosterid;

            }


        </script>
    </head>
    <body>
        <div class="container-fluid">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            //include ('../header_detail/header_detail.php');
            include('functions.php');
            $weekday = '';
            $roster_type = '';
            $str_edit = '';
            $last_weekday = '';
            if (isset($_POST['reset'])) {
                $_SESSION['weekday'] = '';
                $_SESSION['roster_type'] = '';
                $_POST['weekday'] = '';
                $_POST['roster_type'] = '';
                $month_start = strtotime('now', time());
                $start = date('Y-m-d', $month_start);
                $_SESSION['start'] = $start;
                $_POST['start'] = $start;
                $month_end = strtotime('last day of this month', time());
                $end = date('Y-m-d', $month_end);
                $end = get_end();
                $_SESSION['end'] = $end;
                $_POST['end'] = $end;
            }
            if (isset($_SESSION['roster_type'])) {
                $roster_type = $_SESSION['roster_type'];
            }
            if (isset($_SESSION['weekday'])) {
                $weekday = $_SESSION['weekday'];
            }

            if (isset($_POST['start'])) {
                $start = $_POST['start'];
                $_SESSION['start'] = $_POST['start'];
            } else {
                if (isset($_SESSION['start'])) {
                    $start = $_SESSION['start'];
                } else {
                    //$month_start = strtotime('first day of this month', time());
                    $month_start = strtotime('now', time());
                    $start = date('Y-m-d', $month_start);
                }
            }
            if (isset($_POST['empty'])) {
                $_SESSION['member'] = 'empty';
                $weekday = $_SESSION['weekday'];
            } else {
                $_SESSION['member'] = 'all';
            }
            if (isset($_POST['weekday'])) {

                if ($_POST['weekday'] != $_SESSION['weekday']) {
                    $_SESSION['weekday'] = $_POST['weekday'];
                    $weekday = $_SESSION['weekday'];
                }
            }

            if (isset($_POST['roster_type'])) {

                if ($_POST['roster_type'] != $roster_type) {
                    $_SESSION['roster_type'] = $_POST['roster_type'];
                    $roster_type = $_SESSION['roster_type'];
                }
            }



            if (isset($_POST['end'])) {
                $end = $_POST['end'];
                $_SESSION['end'] = $_POST['end'];
            } else {
                if (isset($_SESSION['end'])) {
                    $end = $_SESSION['end'];
                } else {
                    $month_end = strtotime('last day of this month', time());
                    $end = date('Y-m-d', $month_end);
                    $end = get_end();
                }
            }
            $weekday = "%" . $weekday . "%";
            $roster_type = "%" . $roster_type . "%";
            ?>
            <section class="container-fluid" style="padding: 10px;">
                <div class="row">
                    <div class="col-sm-8">
                        <?php include('filter_form.php'); ?>
                    </div>
                    <div class="col-sm-2 hidden-xs" style="max-width: 150px;">
                        <br><a href='../roster/generate_roster.php' class ='btn btn-info'>Generate Roster</a><br>
                    </div>
                    <div class="col-sm-1  hidden-xs">
                        <br><a href='../roster/new_roster.php' class ='btn btn-info'>New Roster</a><br>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <?php //include('list.php');   ?>
            </section>
            <section class="container-fluid" style="padding: 10px;">
                <?php include('list.php'); ?>
            </section>

        </div>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.dropdown-toggle').dropdown();
            });
        </script>
    </body>
</html>