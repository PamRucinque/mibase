<?php

require(dirname(__FILE__) . '/../mibase_check_login.php');
if (isset($_GET['rosterid'])) {
    include('../connect.php');
    $rosterid = $_GET['rosterid'];


    $sql = "UPDATE roster SET approved = True WHERE id = ?;";

    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $sth = $pdo->prepare($sql);
    $array = array($rosterid);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $error = 'Error' . $stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>';
        echo $error;
        exit;
    } else {
        echo 'saved';
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
