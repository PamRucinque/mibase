<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<script>
    function goBack()
    {
        window.history.back()
        //window.location.href = window.location.href;
    }
    function refresh()
    {
        location.reload();
    }

</script>

<?php
$url_last = substr($_SERVER['PHP_SELF'], -8);
$toy_holds = $_SESSION['settings']['toy_holds'];
$reservations = $_SESSION['settings']['reservations'];
$reservecode = '';
$upgrade = 'Yes';
//echo $toy_holds;

if (isset($_SESSION['borid'])) {
    $_SESSION['last_borid'] = $_SESSION['borid'];
    //$_SESSION['last_idcat'] = $_SESSION['idcat'];
}
if (isset($_POST['button1_logout'])) {
    $_SESSION['borid'] = '';
}

$str_header = '';
$str_reserve_header = '';
$stocktake_header = '';
$str_stock_header = '';
$str_hold_header = '';

$str_header .= '<div style="background-color:lightgrey; height: 60px; ">';
$str_header .= '<br><table><tr><td></td>';

$url_button = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['app_root_location'] . '/';




if ($url_last == 'loan.php') {
    $str_header .= '<td><a class="button1_logout" title="Clear Member" href="' . $url_button . 'loans/clear_member.php">Clear</a></td>';

    //$str_header .= '<td><button title="Clear Member" class="button1_logout" href="' . $url_button . 'loans/clear_member.php">Clear</button></td>';
} else {
    $str_header .= '<td><button title="Go back to the previous page" class="button1_logout" onclick="goBack()">Go Back</button></td>';
    //$str_header .= '<td><a class="button1_yellow" title="Loans" href="' . $url_button . 'loans/loan.php">Loans</a></td>';
}


if (isset($_GET['t'])) {
    $_SESSION['idcat'] = $_GET['t'];
}
if (isset($_GET['b'])) {
    $_SESSION['borid'] = $_GET['b'];
    $_SESSION['override'] = 'No';
    $_SESSION['freerent'] = 'No';
}
if (isset($_GET['borid'])) {
    $_SESSION['borid'] = $_GET['borid'];
    $_SESSION['override'] = 'No';
    $_SESSION['freerent'] = 'No';
}
if (isset($_POST['memberid'])) {
    $_SESSION['borid'] = $_POST['memberid'];
    $_SESSION['override'] = 'No';
    $_SESSION['freerent'] = 'No';
}
if (isset($_POST['idcat'])) {
    $_SESSION['idcat'] = $_POST['idcat'];
}
if (isset($_POST['idcat_find'])) {
    $_SESSION['idcat'] = $_POST['idcat_find'];
}


if (isset($_POST['borid'])) {
    $_SESSION['borid'] = $_POST['borid'];
    $_SESSION['override'] = 'No';
    $_SESSION['freerent'] = 'No';
}
if (isset($_POST['scanid'])) {
    $_SESSION['idcat'] = strtoupper($_POST['scanid']);

    $string = substr($_SESSION['idcat'], 0, 1);
    $string_toy = substr($_SESSION['idcat'], 0, 2);
    $string_bookmark = substr($_SESSION['idcat'], 0, 5);
    if ($string == '-') {
        //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
        $_SESSION['idcat'] = rtrim(substr($_SESSION['idcat'], 1));
    }

    if ($string_bookmark == 'I5092') {
        $_SESSION['idcat'] = ltrim(substr($_SESSION['idcat'], 5, -1), '0');
        //$_POST['idcat'] = $_SESSION['idcat'];
        //$_SESSION['idcat'] = $barcode;
    }
    if ($string == '0') {
        //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
        if ($toy_barcode_notrim != 'Yes') {
            $_SESSION['idcat'] = ltrim($_SESSION['idcat'], '0');
        }
    }
    if ($string_toy == 'T/') {
        //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
        $_SESSION['idcat'] = rtrim(substr($_SESSION['idcat'], 2));
    }
    $idcat = $_SESSION['idcat'];
}

$str_header .= '<td><a class="button1" title="Search Toys" href="' . $url_button . 'toys/toys.php">Toys</a></td>';

if (isset($_SESSION['idcat'])) {
    $idcat = $_SESSION['idcat'];

    include( dirname(__FILE__) . '/get_toy_header.php');
    $str_toy_header = '';

    //$_SESSION['loan_status'] = '';

    $linktotoy_header = '<a class="button1" title="' . $toyname . '" href="' . $url_button . 'toys/update/toy_detail.php?idcat=' . $idcat . '">' . $idcat . '</a>';
    $linktopic_header = '<a class="button1" title="' . $toyname . '" href="' . $url_button . 'toys/picture/get_picture.php?idcat=' . $idcat . '">Picture</a>';
    $linktoholds_header = '<a class="button1" style="background-color:#F660AB;" title="' . $toyname . '" href="' . $url_button . 'loans/hold/holds.php?idcat=' . $idcat . '">Hold Toy</a>';

    $str_toy_header = '<td>' . $linktotoy_header . '</td>';
    $linktoreserve_header = '<a class="button1" title="' . $toyname . '" href="' . $url_button . 'reserves/reservation.php?idcat=' . $idcat . '">Bookings</a>';
    if (($reservations == 'Yes') && ($reservecode != null) && ($reservecode == $idcat)) {
        $str_reserve_header = '<td>' . $linktoreserve_header . '</td>';
    }

    $str_picture_header = '<td>' . $linktopic_header . '</td>';
    //echo $reservecode;
    if (($toy_holds == 'Yes') && (($reservecode == null) || ($reservecode == '')) && ($loan_type != 'GOLD STAR')) {
        $str_hold_header = '<td>' . $linktoholds_header . '</td>';
    }


    if ($_SESSION['idcat'] != '') {
        $str_header .= $str_toy_header;
        $str_header .= $str_reserve_header;
        $str_header .= $str_picture_header;
        $str_header .= $str_hold_header;
    }
}

$linktostock_header = '<a class="button1" title="Stock Take" href="' . $url_button . 'toys/stocktake/stocktake.php">Stocktake</a>';
if ($stocktake_header == 'Yes') {
    $str_stock_header = '<td>' . $linktostock_header . '</td>';
}
$str_header .= $str_stock_header;

include( dirname(__FILE__) . '/get_member_header.php');


$str_member_header = '';
if ($_SESSION['level'] == 'admin') {
    $str_header .= '<td><a class="button1_red" title="Search Members" href="' . $url_button . 'members/members.php">Members</a></td>';
}

if (isset($_SESSION['borid']) && ($_SESSION['borid'] != 0)) {


    if ($_SESSION['level'] == 'admin') {
        $linktomember_header = '<a class="button1_red" title="' . $longname . '" href="' . $url_button . 'members/update/member_detail.php?id=' . $_SESSION['borid'] . '">' . $_SESSION['borid'] . '</a>';
        $str_member_header = '<td>' . $linktomember_header . '</td>';
    }




    $str_header .= $str_member_header;
}

if (isset($_SESSION['borid'])) {
    $str_header .= '<td><a class="button1_red" title="Go to payments for ' . $longname_header . '" href="' . $url_button . 'payments/payments.php">Payments</a></td>';
}
//$str_header .= '<td><a class="button1_yellow" title="Loans" href="' . $url_button . 'loans/loan.php">Loans</a></td>';


$str_header .= '<td><a class="button1_green" title="Returns" href="' . $url_button . 'returns/index.php?idcat="' . $_SESSION['idcat'] . '>Returns</a></td>';

$str_header .= '<td><a class="button1_blue" title="Reload page" href="" onclick="refresh()">Refresh</a></td>';
//$_SESSION['myusername']
if ($_SESSION['username'] == 'michelle') {
    $str_header .= '<td><a class="button1_yellow" title="Reload page" href="../loans/loan.php" onclick="refresh()">Loans</a></td>';
}

$str_header .= '</tr></table>';
$str_header .= '</div>';
//echo $_SESSION['dbconn'];
//echo $str_header;
//echo 'hello';
$currentFile = $_SERVER["PHP_SELF"];
$parts = Explode('/', $currentFile);
$page = $parts[count($parts) - 1];
$borid = $_SESSION['borid'];
?>
<div class="container-fluid" style="padding-left: 15px;">
    <div class="row">
        <div class="col-sm-4" style="padding-top: 5px;">
            <?php
            if ($page == 'loan.php') {
                echo '<a href="' . $url_button . 'loans/clear_member.php' . '" class="btn btn-danger">clear</a> ';
            }

            if ($page == 'toy_detail.php') {
                echo ' <a  class="btn btn-primary" title="New Toy" href="' . $url_button . 'toys/new/index.php">New</a>';
                // echo ' <a  class="btn btn-primary" title="New Part for ' . $idcat . '" href="' . $url_button . 'toys/parts/new_part.php?idcat=' . $idcat . '">New Part</a>';
                echo ' <a  class="btn btn-primary" title="Edit ' . $idcat . '" href="' . $url_button . 'toy/edit.php?idcat=' . $idcat . '">Edit</a>';
                echo ' <a  class="btn btn-danger" title="Edit ' . $idcat . '" href="' . $url_button . 'toys/update/delete_toy.php?idcat=' . $idcat . '">Delete</a>';
            }
            ?>
            <?php
            if (($_SESSION['borid'] != '')) {
                if ($_SESSION['level'] == 'admin') {
                    if ($page != 'toy_detail.php') {
                        echo '<a  class="btn btn-colour-maroon" title="' . $longname . '" href="' . $url_button . 'members/update/member_detail.php?id=' . $_SESSION['borid'] . '">' . $_SESSION['borid'] . '</a>';
                    }
                }
                if ($page != 'toy_detail.php') {
                    echo ' <a  class="btn btn-colour-maroon" title="Go to Payments for ' . $longname . '" href="' . $url_button . 'payments/payments.php?id=' . $_SESSION['borid'] . '">Payments</a>';
                }

                if ($page == 'member_detail.php') {
                    echo ' <a  class="btn btn-colour-maroon" title="New Member" href="' . $url_button . 'members/update/new.php">New</a>';
                    echo ' <a  class="btn btn-colour-maroon" title="Edit ' . $borid . '" href="' . $url_button . 'members/update/edit.php?borid=' . $borid . '">Edit</a>';

                    echo ' <a  class="btn btn-danger" title="Edit ' . $idcat . '" href="' . $url_button . 'members/update/delete_mem.php?borid=' . $borid . '">Delete</a>';
                }
            }
            if (($page == 'loan.php') && ($_SESSION['borid'] != '')) {
                echo ' <a  class="btn btn-colour-maroon" title="Edit Alert Member ' . $borid . '" onclick="mem_alert();" >Alert</a>';
            }
            ?>
        </div>
        <div class="col-sm-5"  style="padding-top: 5px;">

            <?php
            if (($page != 'toy_detail.php') && ($_SESSION['level'] == 'admin')) {
                echo '<a  class="btn btn-colour-maroon" title="members" href="' . $url_button . 'members/members.php">Members</a> ';
            }
            if ($page != 'member_detail.php') {
                echo '<a  class="btn btn-primary" title="toys" href="' . $url_button . 'toys/toys.php">Toys</a> ';
            }

            if ($_SESSION['idcat'] != '') {
                if ($page != 'member_detail.php') {
                    echo '<a  class="btn btn-primary" title="' . $toyname . '" href="' . $url_button . 'toys/update/toy_detail.php?idcat=' . $idcat . '">' . $idcat . '</a>';
                }

                if ($page == 'toy_detail.php') {
                    echo ' <a  class="btn btn-primary" title="Go to Picture of ' . $idcat . '" href="' . $url_button . 'toys/picture/get_picture.php?idcat=' . $idcat . '">Picture</a>';
                }
            }
            if ($_SESSION['idcat'] != '') {
                if (($toy_holds == 'Yes') && (($reservecode == null) || ($reservecode == '')) && ($loan_type != 'GOLD STAR') && ($page == 'loan.php')) {
                    echo ' <a  class="btn btn-primary" title="Hold ' . $idcat . '" href="' . $url_button . 'loans/hold/holds.php?idcat=' . $idcat . '">Hold</a>';
                }
            }
            if (($reservations == 'Yes') && ($reservecode != null) && ($reservecode == $idcat) && ($_SESSION['idcat'] != '')) {
                echo ' <a  class="btn btn-primary" title="Bookings for ' . $idcat . '" href="' . $url_button . 'reserves/reservation.php?idcat=' . $idcat . '">Bookings</a>';
            }

            if (($page == 'loan.php') && ($_SESSION['idcat'] != '')) {
                echo ' <a  class="btn btn-primary" title="Add or Edit Toy Alert" onclick="toy_alert();" >Toy Alert</a>';
                echo ' <a  class="btn btn-primary" title="Add or Edit Part Alert" onclick="part_alert();" >Part Alert</a>';
            }

            if ($page == 'member_detail.php') {
                echo ' <a  class="btn btn-primary" title="toys" href="' . $url_button . 'toys/toys.php">Toys</a> ';

//echo ' <a  class="btn btn-danger" title="Edit ' . $idcat . '" href="' . $url_button . 'members/update/delete_mem.php?borid=' . $borid . '">Delete</a>';
            }
            ?>
        </div>
        <div class="col-sm-3"  style="padding-top: 5px;float: right;">
            <?php
            if ($page == 'member_detail.php') {
                if ($_SESSION['idcat'] != '') {
                    echo '<a  class="btn btn-primary" title="' . $toyname . '" href="' . $url_button . 'toys/update/toy_detail.php?idcat=' . $idcat . '">' . $idcat . '</a>';
                }
            }

            if (($page == 'loan.php')) {
                if ($upgrade == 'Yes') {
                    echo ' <a  class="btn btn-success" title="Returns" href="' . $url_button . 'returns_new/index.php?idcat=' . $idcat . '">Returns</a>';
                } else {
                    echo ' <a  class="btn btn-success" title="Returns" href="' . $url_button . 'returns/returns.php?idcat=' . $idcat . '">Returns</a>';
                }
            }
            if ($page == 'toy_detail.php') {
                if ($_SESSION['borid'] != '') {
                    echo ' <a  class="btn btn-colour-maroon" title="' . $longname . '" href="' . $url_button . 'members/update/member_detail.php?id=' . $_SESSION['borid'] . '">' . $_SESSION['borid'] . '</a>';
                }
            }
            if (($page == 'returns.php') || ($page == 'index.php') || ($page == 'payments.php') || ($page == 'toy_detail.php') || ($page == 'member_detail.php')) {
                echo ' <a  class="btn btn-colour-yellow" title="Loans" href="' . $url_button . 'loans/loan.php">Loans</a>';
            }
            if ($page == 'member_detail.php') {
                if (($new_member_email != 'Yes') && ($email != '')) {
                    echo ' <a href="send_email_new.php?borid=' . $_SESSION['borid'] . '" class="btn btn-success">Send Welcome Email</a>';
                }
            }
            ?>

        </div>

    </div>
</div>
