<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

$stats = get_stats();
$footer = '<h3>loans: <font color="blue">' . $stats['loans'] . '</font> returns: <font color="blue">' . $stats['returns'] . '</font> members: <font color="blue">' . $stats['members'] . '</font></h3>';
echo $footer;

function get_stats() {
    //include( dirname(__FILE__) . '/../connect.php');
    $sql = "select count(id) as loantoday from transaction where return is null and date_loan = current_date;";
    $connection_str = $_SESSION['connect_str'];
    $conn = pg_connect($connection_str);
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $loantoday = $row['loantoday'];
    $sql = "select count(id) as returntoday from transaction where return = now()::date;";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $returntoday = $row['returntoday'];
    $sql = "select borid from transaction where (return = now()::date or date_loan = current_date) group by borid;";
    $result_list = pg_exec($conn, $sql);
    $memberstoday = pg_num_rows($result_list);


    //$trans = 4;
    return array('loans' => $loantoday, 'returns' => $returntoday, 'members' => $memberstoday);
}

?>
