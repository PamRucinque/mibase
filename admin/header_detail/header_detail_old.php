<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

?>

<script>
    function goBack()
    {
        window.history.back()
        //window.location.href = window.location.href;
    }
    function refresh()
    {
        location.reload();
    }

</script>

<?php

$url_last = substr($_SERVER['PHP_SELF'], -8);
$toy_holds = $_SESSION['settings']['toy_holds'];
$reservations = $_SESSION['settings']['reservations'];
//echo $toy_holds;

if (isset($_SESSION['borid'])) {
    $_SESSION['last_borid'] = $_SESSION['borid'];
    //$_SESSION['last_idcat'] = $_SESSION['idcat'];
}
if (isset($_POST['button1_logout'])) {
    $_SESSION['borid'] = '';
}

$str_header = '';
$str_reserve_header = '';
$stocktake_header = '';
$str_stock_header = '';
$str_hold_header = '';

$str_header .= '<div  style="background-color:lightgrey; height: 60px; ">';
$str_header .= '<br><table><tr><td></td>';

$url_button = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['app_root_location'] . '/';




if ($url_last == 'loan.php') {
    $str_header .= '<td><a class="button1_logout" title="Clear Member" href="' . $url_button . 'loans/clear_member.php">Clear</a></td>';

    //$str_header .= '<td><button title="Clear Member" class="button1_logout" href="' . $url_button . 'loans/clear_member.php">Clear</button></td>';
} else {
    $str_header .= '<td><button title="Go back to the previous page" class="button1_logout" onclick="goBack()">Go Back</button></td>';
    //$str_header .= '<td><a class="button1_yellow" title="Loans" href="' . $url_button . 'loans/loan.php">Loans</a></td>';
}


if (isset($_GET['t'])) {
    $_SESSION['idcat'] = $_GET['t'];
}
if (isset($_GET['b'])) {
    $_SESSION['borid'] = $_GET['b'];
    $_SESSION['override'] = 'No';
    $_SESSION['freerent'] = 'No';
}
if (isset($_GET['borid'])) {
    $_SESSION['borid'] = $_GET['borid'];
    $_SESSION['override'] = 'No';
    $_SESSION['freerent'] = 'No';
}
if (isset($_POST['memberid'])) {
    $_SESSION['borid'] = $_POST['memberid'];
    $_SESSION['override'] = 'No';
    $_SESSION['freerent'] = 'No';
}
if (isset($_POST['idcat'])) {
    $_SESSION['idcat'] = $_POST['idcat'];
}
if (isset($_POST['idcat_find'])) {
    $_SESSION['idcat'] = $_POST['idcat_find'];
}


if (isset($_POST['borid'])) {
    $_SESSION['borid'] = $_POST['borid'];
    $_SESSION['override'] = 'No';
    $_SESSION['freerent'] = 'No';
}
if (isset($_POST['scanid'])) {
    $_SESSION['idcat'] = strtoupper($_POST['scanid']);

    $string = substr($_SESSION['idcat'], 0, 1);
    $string_toy = substr($_SESSION['idcat'], 0, 2);
    $string_bookmark = substr($_SESSION['idcat'], 0, 5);
    if ($string == '-') {
        //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
        $_SESSION['idcat'] = rtrim(substr($_SESSION['idcat'], 1));
    }

    if ($string_bookmark == 'I5092') {
        $_SESSION['idcat'] = ltrim(substr($_SESSION['idcat'], 5, -1), '0');
        //$_POST['idcat'] = $_SESSION['idcat'];
        //$_SESSION['idcat'] = $barcode;
    }
    if ($string == '0') {
        //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
        if ($toy_barcode_notrim != 'Yes') {
            $_SESSION['idcat'] = ltrim($_SESSION['idcat'], '0');
        }
    }
    if ($string_toy == 'T/') {
        //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
        $_SESSION['idcat'] = rtrim(substr($_SESSION['idcat'], 2));
    }
    $idcat = $_SESSION['idcat'];
}

$str_header .= '<td><a class="button1" title="Search Toys" href="' . $url_button . 'toys/toys.php">Toys</a></td>';

if (isset($_SESSION['idcat'])) {
    $idcat = $_SESSION['idcat'];

    include( dirname(__FILE__) . '/get_toy_header.php');
    $str_toy_header = '';

    //$_SESSION['loan_status'] = '';

    $linktotoy_header = '<a class="button1" title="' . $toyname . '" href="' . $url_button . 'toys/update/toy_detail.php?idcat=' . $idcat . '">' . $idcat . '</a>';
    $linktopic_header = '<a class="button1" title="' . $toyname . '" href="' . $url_button . 'toys/picture/get_picture.php?idcat=' . $idcat . '">Picture</a>';
    $linktoholds_header = '<a class="button1" style="background-color:#F660AB;" title="' . $toyname . '" href="' . $url_button . 'loans/hold/holds.php?idcat=' . $idcat . '">Hold Toy</a>';

    $str_toy_header = '<td>' . $linktotoy_header . '</td>';
    $linktoreserve_header = '<a class="button1" title="' . $toyname . '" href="' . $url_button . 'reserves/reservation.php?idcat=' . $idcat . '">Bookings</a>';
    if (($reservations == 'Yes') && ($reservecode != null) && ($reservecode == $idcat)) {
        $str_reserve_header = '<td>' . $linktoreserve_header . '</td>';
    }

    $str_picture_header = '<td>' . $linktopic_header . '</td>';
    //echo $reservecode;
    if (($toy_holds == 'Yes') && (($reservecode == null) || ($reservecode == '')) && ($loan_type != 'GOLD STAR')) {
        $str_hold_header = '<td>' . $linktoholds_header . '</td>';
    }






    if ($_SESSION['idcat'] != '') {
        $str_header .= $str_toy_header;
        $str_header .= $str_reserve_header;
        $str_header .= $str_picture_header;
        $str_header .= $str_hold_header;
    }
}

$linktostock_header = '<a class="button1" title="Stock Take" href="' . $url_button . 'toys/stocktake/stocktake.php">Stocktake</a>';
if ($stocktake_header == 'Yes') {
    $str_stock_header = '<td>' . $linktostock_header . '</td>';
}
$str_header .= $str_stock_header;

include( dirname(__FILE__) . '/get_member_header.php');


$str_member_header = '';
if ($_SESSION['level'] == 'admin') {
    $str_header .= '<td><a class="button1_red" title="Search Members" href="' . $url_button . 'members/members.php">Members</a></td>';
}

if (isset($_SESSION['borid']) && ($_SESSION['borid'] != 0)) {


    if ($_SESSION['level'] == 'admin') {
        $linktomember_header = '<a class="button1_red" title="' . $longname . '" href="' . $url_button . 'members/update/member_detail.php?id=' . $_SESSION['borid'] . '">' . $_SESSION['borid'] . '</a>';
        $str_member_header = '<td>' . $linktomember_header . '</td>';
    }




    $str_header .= $str_member_header;
}

if (isset($_SESSION['borid'])) {
    $str_header .= '<td><a class="button1_red" title="Go to payments for ' . $longname_header . '" href="' . $url_button . 'payments/payments.php">Payments</a></td>';
}
//$str_header .= '<td><a class="button1_yellow" title="Loans" href="' . $url_button . 'loans/loan.php">Loans</a></td>';


$str_header .= '<td><a class="button1_green" title="Returns" href="' . $url_button . 'returns/index.php?idcat="' . $_SESSION['idcat'] . '>Returns</a></td>';

$str_header .= '<td><a class="button1_blue" title="Reload page" href="" onclick="refresh()">Refresh</a></td>';
//$_SESSION['myusername']
if ($_SESSION['username'] == 'michelle') {
    $str_header .= '<td><a class="button1_yellow" title="Reload page" href="../loans/loan.php" onclick="refresh()">Loans</a></td>';
}

$str_header .= '</tr></table>';
$str_header .= '</div>';
//echo $_SESSION['dbconn'];
echo $str_header;
//echo 'hello';
?>
