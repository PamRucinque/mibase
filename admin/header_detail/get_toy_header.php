<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//include( dirname(__FILE__) . '/get_settings.php');
//echo $toy_holds;
if (isset($_SESSION['idcat'])) {
    $last = $_SESSION['idcat'];
}
$alert = '';
include ('parts.php');
$idcat = strtoupper($idcat);
$toyname = '';
$reservecode = '';
$loan_type = '';
$alert_toy_text = '';




try {
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    echo $_SESSION['shared_server'];
    //echo $dbuser . $dbpasswd;
    die();
}

$sql = "SELECT toys.id as id,toys.idcat as idcat,toyname,reservecode,toys.alert as alert, toy_status , h.holdid, toy_holds.borid as hold_borid,
(borwrs.firstname || ' ' || borwrs.surname) as holdname, toys.loan_type as loan_type 
from toys 
LEFT JOIN (select min(id) as holdid, idcat from toy_holds where (status = 'ACTIVE' or status = 'READY') group by idcat) h on h.idcat =toys.idcat
LEFT JOIN toy_holds on toy_holds.id = h.holdid
left join borwrs on toy_holds.borid = borwrs.id
WHERE upper(toys.idcat) = ?";

$sth = $pdo->prepare($sql);
$array = array($idcat);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();


if ($stherr[0] != '00000') {
    $error_msg .= "An  error occurred.\n";
    $error_msg .= 'Error' . $stherr[0] . '<br>';
    $error_msg .= 'Error' . $stherr[1] . '<br>';
    $error_msg .= 'Error' . $stherr[2] . '<br>';
}

$status_txt = Null;


if ($numrows == 0) {
    $toy_exists = 'No';
    $_SESSION['idcat'] = $last;
} else {
    $toy_exists = 'Yes';
    $_SESSION['idcat'] = $idcat;
}


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = $result[$ri];
    $toy_id = $row['id'];
    $idcat = $row['idcat'];
    $reservecode = $row['reservecode'];
    $toyname = str_replace("'", "`", $row['toyname']);
    $alert = $row['alert'];
    $alert_toy = $row['alert'];
    $alert_toy_text = $row['alert'];
    $loan_type = $row['loan_type'];

    $toy_status = $row['toy_status'];
    $holdid = $row['holdid'];
    $hold_borid = $row['hold_borid'];
    $holdname = $row['holdname'];
}

//
if ($alert_parts != '') {
    $alert_sml = $alert;
    //$alert_toy_text = $alert . '<br>' . $alert_parts_txt;
    $alert = $alert . '<br>' . $alert_parts;
}
