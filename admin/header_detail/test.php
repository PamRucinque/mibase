<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
//include( dirname(__FILE__) . '/../connect.php');

echo 'hello';
$_SESSION['borid'] = 100;
echo $_SESSION['borid'];
echo $dbuser . '<br>' . $dbpasswd . '<br>';
//echo '<br>link: ' . $url_css . $connect_pdo ;

$expired_flag = 'No';
$alert_mem_duty = '';
$alert_mem_txt = '';

if (isset($_POST['memberid'])) {
    $str_parts = '';

    $string = substr($_POST['memberid'], 0, 1);
    if ($string == 'B') {
        //include( dirname(__FILE__) . '/data/get_barcode_member.php');
        $_SESSION['borid'] = ltrim(substr($_POST['memberid'], 5, -1), '0');
    } else {
        $_SESSION['borid'] = $_POST['memberid'];
    }
}
if (isset($_SESSION['borid'])) {

    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    echo $connect_pdo;
    $sql = "SELECT borwrs.*, membertype.maxnoitems as max, 
        membertype.returnperiod as member_returnperiod, 
        membertype.expiryperiod as member_expiryperiod, membertype.duties as duties,
        (select max(date_roster) as next from roster where roster.member_id = borwrs.id and roster.complete = FALSE and date_roster > now()) as next,
        (SELECT coalesce(sum(roster.duration),0) FROM roster WHERE roster.member_id = borwrs.id AND (roster.date_roster >= (borwrs.expired - (membertype.expiryperiod * '1 month'::INTERVAL))) and roster.complete = TRUE)  as completed, 
array_to_string(
 array(select description from event e where typeevent = 'user_alert' and e.memberid = borwrs.id) ,
 ', '
 ) AS event_alerts
    FROM membertype RIGHT JOIN borwrs ON membertype.membertype = borwrs.membertype
    WHERE borwrs.id = 100 AND member_status = 'ACTIVE'";

    $sth = $pdo->prepare($sql);
    $array = array();
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();
    echo $numrows;
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];

        $expired = $row['expired'];
        $format_expired = substr($row['expired'], 8, 2) . '/' . substr($row['expired'], 5, 2) . '/' . substr($row['expired'], 0, 4);

        $borid = $row["id"];
        $firstname = str_replace("'", "`", $row["firstname"]);
        $member_returnperiod = $row['member_returnperiod'];
        $membertype = $row["membertype"];
        $renewed = $row['renewed'];
        $member_expiryperiod = $row['member_expiryperiod'];
        $surname = str_replace("'", "`", $row['surname']);
        $partnersname = str_replace("'", "`", $row["partnersname"]);
        $partnerssurname = str_replace("'", "`", $row['partnerssurname']);
        $member_status = $row["member_status"];
        $joined = $row['datejoined'];
        $fromDate = date("Y-m-d", strtotime("-5 weeks"));
        $duties = $row['duties'];
        $event_alerts = $row['event_alerts'];
        if ($next_duty != '') {
            $next_duty = 'Next Duty: ' . $row['next'];
        }
        $completed = $row['completed'];
        $borname = $firstname . " " . $surname;

        if (strtotime($row['datejoined']) > strtotime($fromDate)) {
            $new_member = 'Yes';
        }


        $today_ts = strtotime($todays_date);

        $max = $row['max'];
        if ($member_returnperiod != 0) {
            if ($member_returnperiod < $loanperiod) {
                $loanperiod = $member_returnperiod;
            }
        }

        $alert_mem = $row['alert'];
        if ($event_alerts != ''){
           $alert_mem_txt = $row['alert'] . '<br>' . $event_alerts; 
        }else{
           $alert_mem_txt = $row['alert'];
        }
        
        $alert_member = $row['alert'];
        if ($row['notes'] != '') {
            $notes = 'Notes: ' . $row['notes'];
        }

        $user1 = $row['location'];
        $total_value = $row['dollar_to_date'];
        if ($partnersname == null || $partnerssurname == null) {
            $longname = $firstname . ' ' . $surname;
        } else {
            $longname = $firstname . ' ' . $surname . ' and ' . $partnersname . ' ' . $partnerssurname;
        }

        $email_link = '<a class="button_small_red" href="mailto:' . $email . '">send email</a>';
        $email_link2 = '<a class="button_small_red" href="mailto:' . $email2 . '">send email</a>';
    }
    echo $firstname;


    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
    }

    $status_txt = Null;
    $last_mem = $_SESSION['borid'];


    //$numrows = 1;
}



$now = time(); // or your date as well
$exp = strtotime($expired);
$diff = ($exp - $now) / (60 * 60 * 24);


if ($diff > 0) {
    $format_expired_warning = '<h3><font color="darkgreen">Expires: ' . $format_expired . '</font></h3>';
}
if ($diff >= 0 && $diff <= 30) {
    $format_expired_warning = '<h3 id="due_alert">Due to Expire on: ' . $format_expired . '</h3>';
}
if ($diff < 0) {
    $format_expired_warning = '<h3 id="expired_alert">Expired on: ' . $format_expired . '</h3>';
    $alert_mem_expired = 'This membership has Expired!';
    $expired_flag = 'Yes';
}
if ($numrows == 0) {
    $longname = 'Member id not found!';
    $format_expired_warning = '';
    $alert_mem_expired = 'This member number is invalid!';
    $_SESSION['borid'] = 0;
}
if ($alert_mem_expired != '') {
    if ($alert_mem != '') {
        $alert_mem_txt = $alert_mem . '<br>' . $alert_mem_expired;
    } else {
        if ($vol_expired_off != 'Yes') {
            $alert_mem_txt = $alert_mem_expired;
        }
    }

    $alert_mem .= '\n' . $alert_mem_expired;
}

if ($new_member_alert == 'Yes') {
    $alert_mem_txt .= '<h3>NEW MEMBER</h3>';
}
if ($duty_alert == 'Yes') {
    include( dirname(__FILE__) . '/data/duties_reminder.php');
    //include( dirname(__FILE__) . '/data/duties_general.php');
    if ($reminder_txt != '') {
        if ($alert_mem != '') {
            $alert_mem_txt .= '<br>' . $reminder_txt;
        } else {
            $alert_mem_txt .= $reminder_txt;
        }

        $alert_mem_duty .= '\n' . $reminder_txt;
    }
    //$alert_mem .= '\n' . $reminder_txt;
}

if ($_SESSION['borid'] == 0) {
    unset($_SESSION['borid']);
    //header('Location: ' . $url_button . 'loan.php');
}
echo $alert_mem_txt;
?>
