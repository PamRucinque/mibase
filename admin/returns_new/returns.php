<?php

include('../mibase_check_login.php');
if (!session_id()) {
    session_start();
}
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include('../header.php'); ?> 
        <script>
            function overlay() {
                if ($("#overlay").is(":visible")) {
                    $("#overlay").hide();
                }
            }
        </script>
    </head>
    <body onload="setFocus()"> 
        <div id="form_container">    
            <?php
//include('../connect.php');

            //$_SESSION['return_alert'] = $alert_toy_text;
            if (isset($_GET['b'])) {
                //$_SESSION['loan_status'] = '';

                $_SESSION['borid'] = $_GET['b'];
                $borid = $_SESSION['borid'];
            }

            if (isset($_GET['t'])) {

                include('../loans/data/get_toy.php');
                $_SESSION['idcat'] = $_GET['t'];
                $idcat = $_SESSION['idcat'];
            }

            if (isset($_GET['borid'])) {

                $_SESSION['idcat'] = $_GET['borid'];
                $idcat = $_SESSION['idcat'];
            }

            $_SESSION['return_alert'] = '';

            include('../menu.php');
            include('../header_detail/header_detail.php');
            $_SESSION['return_alert'] = $alert_toy_text;



            if (isset($_POST['scanid'])) {

                $_SESSION['scanid'] = strtoupper($_POST['scanid']);
                $string = substr($_POST['scanid'], 0, 1);
                if ($string == '-') {
                    $_POST['scanid'] = rtrim(substr($_POST['scanid'], 1));
                }
                $string_toy = substr($_POST['scanid'], 0, 2);

                if ($string_toy == 'T/') {

                    $_POST['scanid'] = rtrim(substr($_POST['scanid'], 2));
                }
                $idcat = $_POST['scanid'];
                include('../loans/data/get_toy.php');

                if ($toy_exists == 'Yes') {
                    $_SESSION['idcat'] = $idcat;
                    $_SESSION['return_alert'] .= $alert_toy_text;
                    include('../loans/functions/functions.php');
                    //include ('../loans/data/get_settings.php');
                    //include('get_toy.php');
                    if ($transid != '') {
                        $returntoy = return_toy($transid, $recordfine, $chargerent, $weekly_fine, $fine_value, $grace);
                        if ($returntoy['alert'] != '') {
                            $_SESSION['return_alert'] .= $returntoy['alert'] . '\n';
                        }

                        $_SESSION['loan_status'] = $returntoy['status'];
                        $_SESSION['idcat'] = $returntoy['idcat'];
                    } else {
                        $_SESSION['loan_status'] = 'This Toy is already in the Library.';
                    }
                } else {
                    $_SESSION['idcat'] = $last;
                    $str_detail = '<br><h3><font color="blue">' . $_POST['scanid'] . ': </font>Cannot find this Toy!</h3> ';
                }
            }
            if ($shared_server == 'Yes') {
                $pic_url = $web_server_protocol . "://" . $_SESSION['host'] . $toy_images . "/" . $library_code . "/" . strtolower($idcat) . '.jpg';
            } else {
                $pic_url = $web_server_protocol . "://" . $_SESSION['host'] . $toy_images . "/" . strtolower($idcat) . '.jpg';
            }

            if ($shared_server) {
                $file_pic = $web_root_folder . '/' . $toy_images . '/' . $library_code . "/" . strtolower($idcat) . '.jpg';
            } else {
                $file_pic = $web_root_folder . '/' . $toy_images . "/" . strtolower($idcat) . '.jpg';
            }

            if (file_exists($file_pic)) {
                $img = '<img height="75px" src="' . $pic_url . '" alt="Toy Image">';
            } else {
                $img = ' ';
            }
            if (isset($_SESSION['idcat'])) {
                $str_pic = '<br><a style="padding-right:10px">' . $img . '</a>';
            }
            ?>
            <table width="100%" bgcolor="lightblue">
                <tr>

                    <td width ="30%"  valign="top" bgcolor="lightblue"><?php include('toy_select.php'); ?>  
                    </td>        
                    <td valign="top" bgcolor="lightblue"><?php
            if (isset($_SESSION['idcat'])) {

                include('../loans/data/get_toy.php');
                include('toy_detail_print.php');
                echo $str_detail;
                //echo $str_parts;
            }
            ?>
                    </td>
                    <td  bgcolor="lightblue" valign="center"><?php echo $str_pic . '<br><br>'; ?></td>
                </tr>
            </table>

<?php
include('returns_list.php');
?>
        </div>
    </body>


