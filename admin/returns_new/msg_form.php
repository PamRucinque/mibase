<script>
    function setFocus()
    {
        var msg = document.getElementById("msg").innerText;
        //alert(msg);
        if (msg !== '') {
            $('#myModal').modal('show');
        }

    }
</script>
<section class="container fluid">
    <div id='msg' style='display: none;'><?php echo $str_alert .  $str_fine; ?></div>
    <!-- Trigger the modal with a button -->
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog"  data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Mibase Alert</h4>
                </div>
                <div class="modal-body">
                    <p><?php echo $modal_str; ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</section>
