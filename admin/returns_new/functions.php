<?php

if (!session_id()) {
    session_start();
}

function return_toy($id, $idcat, $due, $toy_rent, $loan_type, $borid, $reservecode, $toyname, $borname, $days_overdue) {

    $timezone = $_SESSION['settings']['timezone'];
    $location = $_SESSION['location'];
    $rentasfine = $_SESSION['settings']['rentasfine'];
    $recordfine = $_SESSION['settings']['recordfine'];
    $chargerent = $_SESSION['settings']['chargerent'];
    $unlock_returns = $_SESSION['settings']['unlock_returns'];

    $success = 'No';
    $status_fine = '';

    $status = '';

    date_default_timezone_set($timezone);
    $currentTime = date('H:i:s');

    $query_return = "UPDATE transaction Set return = now()::date , timetrans = '" . $currentTime . "', returnby = '" . $_SESSION['username'] . "'
                         ,location = '" . $location . "' WHERE id = " . $id . ";";

    $query_return .= "UPDATE toys set STATUS = FALSE, ";
    if ($unlock_returns == 'Yes') {
        $query_return .= " stocktake_status = 'ACTIVE',";
        $status .= ' This toy has been unlocked. ';
    }
    $query_return .= " location = '" . $_SESSION['location'] . "' WHERE idcat = '" . $idcat . "';";


    if ($chargerent == 'Yes') {
        //$status .= delete_rent($idcat, $borid);
    }


    $now = date('Y-m-d');
    $result_fine = array();
    $result_fine['amount'] = 0;
    $new_id = max_jounal_id();
    //echo 'Fine value: ' . $fine_value;


    if ($days_overdue > 0) {
        //$status .= 'Days Overdue: ' . $days_overdue . '<br>';
        $result_fine = overdue($toy_rent, $due, $borid, $reservecode, $days_overdue);
        $status_fine .= $result_fine['fine_status'];
        $toyname = str_replace("'", "", $toyname);
        $toyname = str_replace('"', "", $toyname);

        if (($recordfine == 'Yes') && ($result_fine['amount'] > 0)) {
            $query_return .= "insert into journal(datepaid, bcode, icode,name, description, category, amount, type, typepayment)
                VALUES (now(),
                '{$borid}',
                '{$idcat}',
                '{$borname}',
                '{$toyname}',
                'Fine',
                {$result_fine['amount']},
                'DR', 'Debit'
            );";
        }
    }

    $conn = pg_connect($_SESSION['connect_str']);
    $result = pg_exec($conn, $query_return);

    if (!$result) {
        $message = 'Invalid query: ' . "\n";
        $message = 'Whole query: ' . $query;
        $message = $query_return;
        //die($message);
        $status .= $message;
        $success = 'No';
    } else {
        $status .= $idcat . ': ' . $toyname . ' has been returned.';
        $status_due = ' Due: ' . $due;
        $success = 'Yes';
    }
    return array("status" => $status, "status_due" => $status_due, "status_fine" => $status_fine, "result" => $success, "idcat" => $idcat, "sql" => $query_return);
}

function max_jounal_id() {

    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "SELECT MAX(id) as newid FROM journal;";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $max = $row['newid'] + 1;
    return $max;
}

function get_loantype($loan_type) {

    $amount = 0;
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "select overdue from stype where stype='" . $loan_type . "';";
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $amount = $row['overdue'];
    if ($amount == '') {
        $amount = 0;
    }
    return $amount;
}

function check_daily($borid) {

    $amount = 0;
    $conn = pg_connect($_SESSION['connect_str']);
    $sql_daily = "select sum(amount) as total from journal where datepaid = current_date 
        and category = 'Overdue Fine' and bcode = " . $borid . " ;";

    $result1 = pg_Exec($conn, $sql_daily);
    $row = pg_fetch_array($result1, 0);
    $amount = $row['total'];
    if ($amount == '') {
        $amount = 0;
    }
    //$_SESSION['loan_status'] .= $query . '<br>' . $numrows;

    return $amount;
}

function overdue($rent, $due, $borid, $reservecode, $days_overdue) {
    if (!session_id()) {
        session_start();
    }
    $fine_factor = $_SESSION['settings']['fine_factor'];
    $hire_factor = (double) $_SESSION['settings']['hire_factor'];
    $fine_value = (double) $_SESSION['settings']['fine_value'];
    $rentasfine = $_SESSION['settings']['rentasfine'];
    $weekly_fine = $_SESSION['settings']['weekly_fine'];
    $grace = (int) $_SESSION['settings']['grace'];
    $max_daily_fine = (double) $_SESSION['settings']['max_daily_fine'];
    $daily_fine = $_SESSION['settings']['daily_fine'];
    $extra_fine = $_SESSION['settings']['extra_fine'];
    $roundup_week = $_SESSION['settings']['roundup_week'];
    $rounddown_week = $_SESSION['settings']['rounddown_week'];

    $fine = 0;
    $weeks = 0;
    $daily_limit = 0;
    $returned_today = 0;
    $today = 0;
    $status = '';
    $custom = '';

    $now = date('Y-m-d');


    if (($rentasfine == 'Yes') && ($rent > 0)) {
        $fine_value = $rent;
        if ($extra_fine > 0) {
            $fine_value = $fine_value + $extra_fine;
        }
    }
    $weekly_fine = 'Yes';
    if ($weekly_fine == 'Yes') {

        $weeks = round(($days_overdue - $grace) / 7);
        if ($roundup_week == 'Yes') {
            $weeks = ceil(($days_overdue - $grace) / 7);
        }
        if ($rounddown_week == 'Yes') {
            $weeks = floor(($days_overdue - $grace) / 7);
        }

        if ($weeks < 0) {
            $weeks = 0;
        }
        //$status .= '<br>Fine Value: ' . $fine_value . '<br>';
        //$status .= 'Fine factor: ' . $fine_factor . '<br>';

        $fine = round(($weeks * $fine_value * $fine_factor), 2);
        //$status .= 'Fine Calc: ' . $fine . '<br>';

        If (($daily_fine == 'Yes') && ($returned_today > 0)) {
            $fine = 0;
        }

        if ($daily_limit > 0) {
            //$status = 'Daily limit not = 0. ';
            if ($today >= $daily_limit) {
                $fine = 0;
            } else {
                if (($fine + $today) >= $daily_limit) {
                    $fine = $daily_limit - $today;
                    $status .= '<br><font color="blue">You have reached your Daily Limit, this fine has been capped at $' . $fine . ' .</font><br>';
                }
            }
        }
        if ($custom == 'carlson') {
            if ($rent > 0) {
                $extra_custom = round($weeks * $rent, 2);
                $fine = $fine + $extra_custom;
                $status .= '<br><font color="blue">Extra Rent has been charged for this toy, $' . $rent . ' per week overdue.</font><br>';
            }
        }

        if ($fine > 0) {
            if ($daily_fine == 'Yes') {
                $status .= 'These Toy(s) are ' . $weeks . ' weeks overdue and have accrued a $' . number_format($fine, 2) . ' fine - please see the Coordinator.';
            } else {
                $status .= ' This Toy is overdue by ' . $weeks . ' weeks, your fine is $' . number_format($fine, 2) . '. <br>';
            }
        }
        if ($grace > 0 and ( $fine > 0)) {
            $status .= '  ' . $grace . ' days grace has been granted.<br> ';
        }
    } else {
        $fine = 0;
    }

    //$status .= 'No days: ' . $no_days . '. Weeks: ' . $weeks . ' Days Grace: ' . $days_grace;
    if (($hire_factor > 1) && ($reservecode != '')) {
        $fine = $hire_factor * $fine;
        if ($fine > 0) {
            $status .= 'This toy is a Hire Toy, your fine of $' . number_format($fine, 2) . ' has been multiplied by a factor of ' . $hire_factor . '!<br>';
        }
    }
    //$status .= 'Fine: ' . $fine . ' Hire Factor: ' . $hire_factor;

    return array("amount" => number_format($fine, 2), "fine_status" => $status);
}

function delete_rent($idcat, $borid) {

    $outcome = '';
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "DELETE from journal WHERE 
    (datepaid::date)= current_date AND ((icode)='" . $idcat . "') AND ((bcode)=" . $borid . ") AND ((category)= 'Rent');";
    //$result_delete = pg_Exec($conn, $sql);
    if (!$result_delete) {
        $outcome = 'cannot delete rent:' . $sql;
    }
    return $outcome;
}

function dayCount($from, $to) {
    $first_date = strtotime($from);
    $second_date = strtotime($to);
    $days_diff = $second_date - $first_date;
    return date('d', $days_diff);
}

function get_parts($idcat) {
    if (!session_id()) {
        session_start();
    }
    try {
        $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }

    $sql = "SELECT * from parts where itemno = ? and type != 'Found' ORDER by type, datepart";

    $sth = $pdo->prepare($sql);
    $array = array($idcat);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();

    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
    }


    $alert_txt = '';
    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = $result[$ri];

        if ($row['alertuser']) {
            $alert_txt .= $row['type'] . ': ' . $row['description'] . '<br>';
        }
    }
    return $alert_txt;
}
