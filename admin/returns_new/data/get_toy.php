<?php

$idcat = '';
if (!isset($_POST['scanid'])) {
    if (isset($_GET['idcat'])) {
        $idcat = $_GET['idcat'];
    }
    if (isset($_POST['idcat'])) {
        $idcat = $_POST['idcat'];
    }
}else{
    $idcat = $_POST['scanid'];
}

$idcat = strtoupper($idcat);
$toy = array();
$toy_exists = 'No';
$due_str = '';
$due_status = '';

$connect_pdo = $_SESSION['connect_pdo'];
$dbuser = $_SESSION['dbuser'];
$dbpasswd = $_SESSION['dbpasswd'];

try {
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
} catch (PDOException $e) {
    print "Error! connecting to database - returns list: " . $e->getMessage() . "<br/>";
    echo $shared_server;
    //echo $dbuser . $dbpasswd;
    die();
}

$sql = "SELECT toys.idcat as idcat,toyname,reservecode,toys.alert as alert, toys.rent as rent, toys.loan_type, 
    toy_status , h.holdid, toy_holds.borid as hold_borid, t.borname as borname, toyname, t.borid as borid,
(borwrs.firstname || ' ' || borwrs.surname) as holdname, toys.loan_type as loan_type,t.due as due, t.id as transid, t.days_overdue as days_overdue 
from toys 
left join (select id, due, CAST(extract('day' from date_trunc('day',now() - due::date)) as integer) as days_overdue, idcat, borname, borid from transaction where return is null) t on t.idcat = toys.idcat
LEFT JOIN (select min(id) as holdid, idcat from toy_holds where (status = 'ACTIVE' or status = 'READY') group by idcat) h on h.idcat =toys.idcat
LEFT JOIN toy_holds on toy_holds.id = h.holdid
left join borwrs on toy_holds.borid = borwrs.id 
WHERE upper(toys.idcat) = ?;";
//echo $sql;

$sth = $pdo->prepare($sql);
$array = array($idcat);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();


if ($stherr[0] != '00000') {
    $error_msg .= "An  error occurred.\n";
    $error_msg .= 'Error' . $stherr[0] . '<br>';
    $error_msg .= 'Error' . $stherr[1] . '<br>';
    $error_msg .= 'Error' . $stherr[2] . '<br>';
}

$status_txt = Null;
$now = date('Y-m-d');
$toy = array();

if ($numrows != 0) {
    $toy_exists = 'Yes';
    $_SESSION['idcat'] = $idcat;
}


for ($ri = 0; $ri < $numrows; $ri++) {
    $toy = $result[$ri];
}


if ($toy_exists == 'Yes') {
    if ($toy['transid'] != null) {
        $format_due = substr($toy['due'], 8, 2) . '-' . substr($toy['due'], 5, 2) . '-' . substr($toy['due'], 0, 4);
        if ($toy['due'] < $now) {
            $due_status = '<font color="red">OVERDUE: ' . $format_due . '</font>';
        } else {
            $due_status = 'DUE ON: ' . $format_due;
        }
    }
}


include('../config.php');
$library_code = $_SESSION['library_code'];
if ($shared_server == 'Yes') {
    $pic_url = $web_server_protocol . "://" . $_SESSION['host'] . '/toy_images' . "/" . $library_code . "/" . strtolower($idcat) . '.jpg';
} else {
    $pic_url = $web_server_protocol . "://" . $_SESSION['host'] . $toy_images_location . "/" . strtolower($idcat) . '.jpg';
}

if ($shared_server) {
    $file_pic = $web_root_folder . '/' . $toy_images_location . '/' . $library_code . "/" . strtolower($idcat) . '.jpg';
} else {
    $file_pic = $web_root_folder . '/' . $toy_images_location . "/" . strtolower($idcat) . '.jpg';
}

if (file_exists($file_pic)) {
    $img = '<img height="75px" src="' . $pic_url . '" alt="Toy Image">';
} else {
    $img = ' ';
}
if (isset($_SESSION['idcat'])) {
    $str_pic = '<a style="padding-right:10px">' . $img . '</a>';
}
//echo $pic_url;



