<?php
if (!session_id()) {
    session_start();
}

include('../mibase_check_login.php');

$menu_color = '#00b300';
$menu_font_color = "whitesmoke";

$heading_color = 'color:' . $menu_color . ';';
$menu_color_bg = 'background:' . $menu_color . ';';
$menu_font_color = 'color:' . $menu_font_color . ';';
$red_background = 'background: darkred;';

//onMouseOver="this.style.background='<?php echo $menu_color; 
//echo $menu_color;
?>
<nav id="nav" style="<?php echo $menu_color_bg; ?>">
    <ul>
        <li><a href="../home/index.php" style="<?php echo $menu_font_color; ?>">Home</a></li>
        <li><a href="../loans/loan.php" style="<?php echo $menu_font_color; ?>">Loans</a></li>
        <li><a  href="../toys/toys.php"  style="<?php echo $menu_font_color; ?>">Toys</a></li>
        <li><a  href="index.php" class="is-active" style="<?php echo $menu_font_color; ?>">Toy Returns</a></li>

        <li class="right"><a style="<?php echo $menu_font_color . $red_background; ?>" href="../Logout.php">Logout</a></li>
    </ul>
</nav><!-- end of #nav -->