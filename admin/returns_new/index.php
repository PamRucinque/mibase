<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">

    </head>

    <body onload="setFocus()"> 

        <?php
        if (!session_id()) {
            session_start();
        }
        $alert = '';
        if ($_SESSION['idcat'] != '') {
            $idcat = $_SESSION['idcat'];
        }
        $button_str = 'OK';
        $java_str = "$(location).attr('href', 'index.php')";
        
        
        $str_fine = '';
        include( dirname(__FILE__) . '/../menu.php');
        include ('../header_detail/header_detail.php');
        include('get_settings.php');
        include('process.php');
        include('data/get_toy.php');
        include('returns_list.php');
        $str_alert = $alert;
        //$str_alert .= get_parts($idcat);
        $str_return_btn = '';

        if ($idcat != '') {
            $toy_str = '<h4><strong>' . $idcat . ':</strong> ' . $toyname . '</h4>';
            $toy_str .= '<font color="green">' . $_SESSION['return_status'] . '</font><br>';
            $toy_str .= $due_status;
        } else {
            $toy_str = 'Select or Scan a Toy!';
        }
        if ($toy_exists == 'Yes') {
            if ($toy['transid'] != null) {
                $str_return_btn = '<a href="return_toy.php?idcat=' . $toy['idcat'] . '"><button class="btn btn-colour-yellow">Return ' . $idcat . '</button></a>';
            } else {
                if ($idcat != '') {
                    $str_return_btn = '<br>This toy is in the Library';
                }
            }
            $str_toy_alert = '' . $toy['alert'];
        } else {
            $str_toy_alert = '';
        }
        $modal_str = '<font color="red">' . $str_alert . '</font><br>';
        $modal_str .= '<font color="blue">' . $_SESSION['fine'] . '</font><br>';
        $str_fine = $_SESSION['fine'];
        ?>
    </div>


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4" style="padding-right: 5px;">
                <?php include('toy_select.php'); ?>
            </div>
            <div class="col-md-4"><?php echo $toy_str; ?></div>
            <div class="col-md-2"><?php echo $str_pic; ?></div>
            <div class="col-md-2"><?php echo $total_returns . $str_return_btn; ?></div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div id="toy_alert" class="col-md-6" style="background-color: #f2dede;"><?php echo $str_alert; ?></div>
            <div id="fine_alert" class="col-md-6" style="background-color: #d9edf7;"><?php echo $str_fine; ?></div>
        </div>
    </div>

    <?php include('msg_form.php'); ?>
    <?php
    echo $returns_txt;
    $_SESSION['fine'] = '';
    $str_fine = '';
    $_SESSION['return_status'] = '';
    $str_alert = '';
    ?>

</body>

<script>
        $(document).ready(function () {
            $('.dropdown-toggle').dropdown();
        });
</script>
</html>
