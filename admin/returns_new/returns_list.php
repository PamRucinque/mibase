<?php

//include( '../mibase_check_login.php');

$conn = pg_connect($_SESSION['connect_str']);
$returns_txt = '';

$query = "SELECT * from transaction where (return is not null) and return = current_date order by timetrans desc";
$trans = pg_exec($conn, $query);
$numrows = pg_numrows($trans);
$total = 0;
//echo $query;
$returns_txt = '<div class="container-fluid">';


for ($ri = 0; $ri < $numrows; $ri++) {
    $returns_txt .= '<div class="row">';
    //echo "<tr>\n";
    $row = pg_fetch_array($trans, $ri);
    $total = $total + 1;
    //$weekday = date('l', strtotime($row['date_roster']));
    $trans_borid = $row['borid'];
    $trans_item = $row['item'];
    $trans_idcat = $row['idcat'];
    $borname = $row['borname'];
    $format_loan = substr($row['date_loan'], 8, 2) . '-' . substr($row['date_loan'], 5, 2) . '-' . substr($row['date_loan'], 0, 4);
    $format_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);
    $now = date('Y-m-d');
    $ref2 = 'return_toy.php?id=' . $row['id'];
    $ref = 'renew_toy.php?id=' . $row['id'];

    if (strtotime($now) > strtotime($row['due'])) {
        $due_str = '<font color="red" font="strong"> OVERDUE  ' . $format_due . '</font>';
    } else {
        $due_str = $format_due;
    }
    $returns_txt .= '<div class="col-xs-12  col-md-3" style="background-color: #dff0d8;">' . $row['timetrans'] . '  ' . $due_str . '</div>';
    $returns_txt .= '<div class="col-xs-12 col-md-4"><a class="button_small_red" href="../members/update/member_detail.php?id=' . $trans_borid . '">' . $trans_borid . '</a> ' . $borname . '</div>';
    $returns_txt .= '<div class="col-xs-12 col-md-5"><a class="button_small" href="index.php?idcat=' . $row['idcat'] . '">' . $row['idcat'] . '</a> ' . $trans_item . '</div>';

    $returns_txt .= '</div>'; //end of row
}

$returns_txt .= '</div>'; // end of container-fluid


pg_close($conn);

$total_returns = '<font color="blue">Total returns today: ' . $total . '</font>';

//echo $returns_txt;


