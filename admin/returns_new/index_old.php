<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../css/mibase.css" rel="stylesheet">
        <script>
            function setFocus()
            {
                document.getElementById("scanid").focus();
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }
        </script>
    </head>

    <body onload="setFocus()"> 
        <div class="container-fluid">
            <div class="row"> 

                <div class="col-sm-12">
                    <?php
                    if ($_SESSION['settings']['weekly_fine'] == '') {
                        include('../functions.php');
                        include('../session_new.php');
                        $connect_str = $_SESSION['connect_str'];
                        //echo $connect_str;
                        $_SESSION['settings'] = fillSettingSessionVariables($connect_str);
                        //echo 'Level: ' . $_SESSION['level'] . '<br>';
                    }

                    $str_return_btn = '';

                    include('../config.php');
                    include('../menu.php');
                    include('../header_detail/header_detail.php');
                    include('process.php');
                    include('data/get_toy.php');
                    include('returns_list.php');
                    if ($_SESSION['idcat'] != '') {
                        $idcat = $_SESSION['idcat'];
                    }
                    if ($idcat != '') {
                        $toy_str = '<h4><strong>' . $idcat . ':</strong> ' . $toy['toyname'] . '</h4>';
                        $toy_str .= '<font color="green">' . $_SESSION['return_status'] . '</font><br>';
                        $toy_str .= $due_status;
                    } else {
                        $toy_str = 'Select or Scan a Toy!';
                    }
                    if ($toy_exists == 'Yes') {
                        if ($toy['transid'] != null) {
                            $str_return_btn = '<a href="return_toy.php?idcat=' . $toy['idcat'] . '"><button class="btn btn-colour-yellow">Return ' . $idcat . '</button></a>';
                        } else {
                            if ($idcat != '') {
                                $str_return_btn = '<br>This toy is in the Library';
                            }
                        }
                        $str_toy_alert = '' . $toy['alert'];
                    } else {
                        $str_toy_alert = '';
                    }
                    $modal_str = '<font color="red">' . $str_toy_alert . '</font><br>';
                    $modal_str .= '<font color="blue">' . $_SESSION['fine'] . '</font><br>';
                    ?>

                </div>
            </div>
            <div id='msg' style='display: none;'><?php echo $str_toy_alert . $_SESSION['fine']; ?></div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4" style="padding-right: 5px;">
                        <?php include('toy_select.php'); ?>
                    </div>
                    <div class="col-md-4"><?php echo $toy_str; ?></div>
                    <div class="col-md-2"><?php echo $str_pic; ?></div>
                    <div class="col-md-2"><?php echo $total_returns . $str_return_btn; ?></div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="toy_alert" class="col-md-6" style="background-color: #f2dede;"><?php echo $str_toy_alert; ?></div>
                    <div id="fine_alert" class="col-md-6" style="background-color: #d9edf7;"><?php echo $_SESSION['fine']; ?></div>
                </div>

                <!-- Trigger the modal with a button -->
                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Alerts</h4>
                            </div>
                            <div class="modal-body">
                                <p><?php echo $modal_str; ?></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>


            <div class="container-fluid">

                <?php
                echo $returns_txt;
                $_SESSION['fine'] = '';
                $_SESSION['return_status'] = '';
                ?>

            </div>
        </div>


        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
        <script>
        $(document).ready(function () {
            $('.dropdown-toggle').dropdown();
        });
        </script>

    </body>
</html>




