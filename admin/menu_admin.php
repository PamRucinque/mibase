
<?php
$url_menu = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['app_root_location'] . '/';
//$url_menu = 'http://open.localhost/mibaseopen/';
?>

<div class="container-fluid"  style="padding-right:0px;padding-left:0px;">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid" style="padding-left:10px;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand visible-xs" href="#">MiBase Online</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse mibase-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="<?php echo $url_menu . 'home/index.php'; ?>" class="dropdown-toggle  btn btn-colour-yellow" 
                           data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"
                           ondblclick="location.href = '<?php echo $url_menu . 'home/index.php'; ?>'">Home <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo $url_menu . 'home/index.php'; ?>">Home</a></li>
                            <li><a href="<?php echo $url_menu . 'settings/settings.php'; ?>">Global Settings</a></li>
                            <li><a href="<?php echo $url_menu . 'home/auto/auto.php'; ?>">My Setup</a></li>
                            <li><a href="<?php echo $url_menu . 'home/edit_home.php'; ?>">Website</a></li>
                            <li><a href="<?php echo $url_menu . 'template/template.php'; ?>">Templates</a></li>
                            <li><a href="<?php echo $url_menu . 'home/files/get_file.php'; ?>">Upload Files</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'daily_stats/index.php'; ?>">Session Stats</a></li>
                            <li><a href="<?php echo $url_menu . 'home/active_chart/chart.php'; ?>">Active Members</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'holiday/index.php'; ?>">Holiday Due Dates</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'home/menu.php'; ?>">Home Menu</a></li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle btn btn-primary mibase-menu-heading" 
                           data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" 
                           ondblclick="location.href = '<?php echo $url_menu . 'toys/toys.php'; ?>'">Toys <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo $url_menu . 'toys/toys.php'; ?>">Toys List</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'toys/new/index.php'; ?>">New Toy</a></li>
                            <li><a href="<?php echo $url_menu . 'toys/copy/index.php'; ?>">Copy Toy</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'toys/parts/parts.php'; ?>">Parts Listing</a></li>
                            <li><a href="<?php echo $url_menu . 'toys/alerts/alerts.php'; ?>">Alerts Listing</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'toys/stocktake/stocktake.php'; ?>">Stock Take</a></li>
                            <li><a href="<?php echo $url_menu . 'overdue/toys_onloan.php'; ?>">Overdue toys / Toys On Loan</a></li>
                            <li><a href="<?php echo $url_menu . 'toys/no_pictures/toys.php'; ?>">Toys Without Pictures</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'toys/locked/locked.php'; ?>">Locked</a></li>
                            <li><a href="<?php echo $url_menu . 'toys/locked/withdrawn.php'; ?>">Witdrawn</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'select_boxes/index.php'; ?>">Select Boxes</a></li>


                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle btn btn-colour-maroon" 
                           data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" 
                           ondblclick="location.href = '<?php echo $url_menu . 'members/members.php'; ?>'">Members <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo $url_menu . 'members/members.php'; ?>">Members List</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'members/update/new.php'; ?>">New</a></li>
                            <li><a href="<?php echo $url_menu . 'approve/members_approve.php'; ?>">Approve Members</a></li>
                            <li><a href="<?php echo $url_menu . 'roster/roster.php'; ?>">Duty Roster</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'members/members_resigned.php'; ?>">Resigned</a></li>
                            <li><a href="<?php echo $url_menu . 'members/members_locked.php'; ?>">Locked</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'emails/send_email.php'; ?>">Bulk Emails</a></li>
                            <li><a href="<?php echo $url_menu . 'emails_new/index.php'; ?>">New Bulk Emails</a></li>
                            <li><a href="<?php echo $url_menu . 'bulk_alerts/main.php'; ?>">Bulk Alerts</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo $url_menu . 'select_boxes/index.php'; ?>">Select Boxes</a></li>


                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo $url_menu . 'reserve_toys/toys_party.php'; ?>"  class="btn btn-payments" style="color:white;">Reservations</a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo $url_menu . 'loans/hold/holds_list.php'; ?>"  class="btn btn-holds" style="color:white;">Holds</a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo $url_menu . 'reports/reports.php'; ?>"  class="btn btn-success" style="color:white;">Reports</a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo $url_menu . 'loans/loan.php'; ?>"  class="btn btn-colour-yellow" style="color:white;">Loans</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo $url_menu . 'Logout.php'; ?>"  class="btn btn-danger" style="color:white;">Logout</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo $url_menu . 'js/jquery-3.0.0.js'; ?>"></script>
<script>window.jQuery || document.write('<script src="<?php echo $url_menu . 'js/jquery-3.0.0.js'; ?>"><\/script>')</script>
<script src="<?php echo $url_menu . 'js/bootstrap.min.js'; ?>"></script>
