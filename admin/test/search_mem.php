<?php
if (!session_id()) {
    session_start();
}
if (isset($_GET['term'])) {
    $query = '';
    $return_arr = array();
    $q = strtolower($_GET["term"]);
    $q = str_replace("`", "`", $q);

    if (preg_match('/^[0-9]*$`/', $q)) {
        $query = "SELECT id, firstname, surname, partnersname, partnerssurname"
                . " FROM borwrs WHERE to_char(id, '99999') LIKE '%" . $q . "%' AND member_status='ACTIVE' ORDER by id, surname, firstname ASC;";
    } else {
        $query = "SELECT id, firstname, surname, partnersname, partnerssurname FROM borwrs 
               WHERE 
               to_char(id, '99999') LIKE '%" . $q . "%' OR 
               ((upper(replace(surname, '''', '`')) LIKE '" . strtoupper($_REQUEST['term']) . "%') "
                . "OR (upper(firstname) LIKE '" . strtoupper($_REQUEST['term']) . "%') "
                . "OR (upper(partnerssurname) LIKE '" . strtoupper($_REQUEST['term']) . "%') "
                . "OR (upper(partnersname) LIKE '" . strtoupper($_REQUEST['term']) . "%')) 
               AND member_status='ACTIVE' ORDER by surname, firstname ASC;";
    }
    //echo $query;
    if (!session_id()) {
        session_start();
    }
    include('../connect.php');

    $dbpasswd = $_SESSION['dbpasswd'];
    $connection_str = "port=5432 dbname=" . $dbuser . " user=" . $dbuser . " password=" . $dbpasswd;
    $conn = pg_connect($connection_str);

    $result_select = pg_Exec($conn, $query);
    $numrows = pg_numrows($result_select);

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result_select, $ri);
        $return_arr[] = $row['id'] . ': ' . $row['surname'];
        $longname = $row['firstname'] . ' ' . $row['surname'];
        if (($row['surname'] != $row['partnerssurname']) && ($row['partnerssurname'] != '')) {
            $longname .= ' & ' . $row['partnersname'] . ' ' . $row['partnerssurname'];
        }

        $longname .= ' : ' . $row['id'];
        $results[] = array('label' => $longname, 'value' => $row['id']);
        echo $longname;
    }

    /* Toss back results as json encoded array. */
    echo json_encode($results);
}
?>