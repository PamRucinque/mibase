<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>



    <body id="main_body" >
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');
            //include("../../get_settings.php");
            if (isset($_GET['id'])){
                $id = $_GET['id'];
            }else{
                $id = $_SESSION['borid'];
            }


            echo "<h2><br><a href='roster.php' class ='button1_logout'>Cancel</a>";
            include( dirname(__FILE__) . '/data/get_roster.php');
          
            
                echo '<font color="red"><br> WARNING - YOU ARE ABOUT TO Roster:  </font><font color="blue">' . $borid . ': ' . $roster_session . '</font></h1><br>';
                include( dirname(__FILE__) . '/delete_form.php');
            

            //include( dirname(__FILE__) . '/toy_detail.php');
            if ($password == '') {
                $password = 'mibase';
            }


            if (isset($_POST['submit'])) {

                if (isset($_GET['borid'])) {
                    // get the 'id' variable from the URL
                    $id = $_GET['id'];
                    $conn = pg_connect($_SESSION['connect_str']);
                    $query = "DELETE FROM roster WHERE id = " . $_POST['id'] . ";";
                      if (($_POST['password'] == $password)) {
                        $result = pg_exec($conn, $query);
                        if (!$result) {
                            echo "An INSERT query error occurred.\n";
                            echo $query;
                            //exit;
                        } else {
                            $edit_url = 'roster.php';
                            echo "<br><p>The record was successfully deleted.</p><br>";
                            echo '<a class="button1" href="roster.php">OK</a>';
                            $_SESSION['del_Status'] = '';
                        }

                        pg_FreeResult($result);
                        pg_Close($conn);
                    } else {
                        $edit_url = 'member_detail.php?id=' . $_GET['borid'];
                        echo "<br><p>Incorrect Password.</p><br>";
                        echo '<a class="button1" href="../members.php">OK</a>';
                    }
                }
            }
            ?>
        </div>
    </body>
</html>



