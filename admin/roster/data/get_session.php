<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');
$query = "SELECT description FROM rostertypes WHERE description!='No Time' group by description ORDER by description ASC;";
$result = pg_Exec($conn, $query);

//choose the database

//get data from database


echo "<select name='roster_session' id='roster_session'>\n";
echo "<option value='" . $roster_session . "' selected='selected'>" . $roster_session . "</option>";
//echo "<option value=''></option>";
$numrows = pg_numrows($result);

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    $roster = $row['description'];
    echo "<option value='{$roster}'>{$roster}
    </option>\n";
}

echo "</select>\n";


?>

