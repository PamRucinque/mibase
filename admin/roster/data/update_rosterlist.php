<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php'); 
?>
<?php

if (isset($_GET['rosterid']) && isset($_GET['borid'])) {
    $roster_id = $_GET['rosterid'];
    $member_id = $_GET['borid'];
    $query_update = "Update roster SET member_id = " . $member_id . ", modified = now() WHERE id = " . $roster_id;
    //include( dirname(__FILE__) . '/../../connect.php');
    $result_update = pg_Exec($conn, $query);
     if (!$result_update) {
         $success = TRUE;
         echo 'saved';
     }
}

echo 'nothing done';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
