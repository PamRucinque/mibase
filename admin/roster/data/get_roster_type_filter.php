<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

echo "Role:<select name='roster_type' style='width: 80px;background-color:#F3F781;' id='roster_type' onchange='this.form.submit();'>\n";
echo "<option value='" . $_SESSION['roster_type'] . "' selected='selected'>" .  $_SESSION['roster_type'] . "</option>";
echo "<option value='' >Show All</option>";
echo "<option value='Member' >Member</option>";
echo "<option value='Co-ordinator'>Co-ordinator</option>";
echo "</select>\n";


?>

