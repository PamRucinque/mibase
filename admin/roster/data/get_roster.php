<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');
$query = "SELECT * from roster WHERE id = " . $_GET["id"];


$query = "SELECT roster.*, roster.status as roster_status, borwrs.id as borid, borwrs.firstname as firstname, borwrs.surname as surname, borwrs.emailaddress , borwrs.phone2 as phone2
FROM roster LEFT JOIN borwrs ON roster.member_id = borwrs.ID WHERE roster.id = " . $_GET["id"] .  " ORDER BY date_roster;";
$conn = pg_connect($_SESSION['connect_str']);
$result_roster = pg_Exec($conn, $query);
$numrows = pg_numrows($result_roster);
$status_txt = Null;
if ($numrows == 0){
    echo $query;
}

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_roster, $ri);


    $id = $row['id'];
    $date_roster = $row['date_roster'];
    $borid = $row['member_id'];
    $type_roster = $row['type_roster'];
    $roster_pickbox = $row['weekday'] . ':' . $row['roster_session'];
    $roster_session = $row['roster_session'];
    $weekday = $row['weekday'];
    $session_role = $row['session_role'];
    $duration = $row['duration'];
    $fullname = $row['surname'] . ', ' . $row['firstname'] . ' (' . $row['member_id'] . 'x)';
    $comments = $row['comments'];
    $status = $row['roster_status'];
    pg_FreeResult($result_roster);
// Close the connection
    pg_Close($conn);

}


//echo 'Type Claim: ' . $type_claim;
//$date_submitted = $date_submitted[weekday]. $date_submitted[month] . $date_submitted[mday] . $date_submitted[year];
?>