<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

echo "<select name='weekday' id='weekday'>\n";
echo "<option value='' selected='selected'></option>";
echo "<option value='Monday' >Monday</option>";
echo "<option value='Tuesday'>Tuesday</option>";
echo "<option value='Wednesday'>Wednesday</option>";
echo "<option value='Thursday'>Thursday</option>";
echo "<option value='Friday'>Friday</option>";
echo "<option value='Saturday'>Saturday</option>";
echo "<option value='Sunday'>Sunday</option>";
echo "<option value='NA'>NA</option>";
echo "</select>\n";


?>

