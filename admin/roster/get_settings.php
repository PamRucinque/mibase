<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

$conn = pg_connect($_SESSION['connect_str']);
$query_settings = "SELECT * FROM settings;";
$result_settings = pg_Exec($conn, $query_settings);
$numrows = pg_numrows($result_settings);

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_settings, $ri);
    if ($row['setting_name'] == 'fortnight') {
        $fortnight = $row['setting_value'];
    }
    //roster_coord
    if ($row['setting_name'] == 'roster_coord') {
        $roster_coord = $row['setting_value'];
    }
}

pg_FreeResult($result_settings);
// Close the connection
pg_Close($conn);
?>

