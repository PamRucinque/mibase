<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
//include( dirname(__FILE__) . '/../get_settings.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 

        <script type="text/javascript">
            $(function () {
                var pickerOpts = {
                    dateFormat: "yy-mm-dd",
                    showOtherMonths: true

                };
                $("#start").datepicker(pickerOpts);
                $("#end").datepicker(pickerOpts);
                $("#start1").datepicker(pickerOpts);
                $("#end1").datepicker(pickerOpts);

            });

        </script>

    </head>
    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            $fortnight = $_SESSION['settings']['fortnight'];
            $roster_coord = $_SESSION['settings']['roster_coord'];
            $count_coord = $_SESSION['settings']['count_coord'];
            $count_student = $_SESSION['settings']['count_student'];
            ?>
            <br>
            <script type="text/javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
            <script type="text/javascript" src="../js/jquery-1.9.1.js"></script>
            <script type="text/javascript" src="../js/jquery-ui.js"></script>


            <div id="form1234" style="background-color:whitesmoke;padding-left: 20px;" align="left">
                <br><a class="button1_blue" href="roster.php">Back to Duty Roster</a><br>

                <form id="new" method="post" action="generate_roster.php">
                    <h2><font color="blue">Generate Roster</font></h2>
                    <p>A roster with overlapping dates from a previous roster will not be generated. Please delete any roster records if in the new roster period</p><br>

                    <table align="top">

                        <tr><td  width="20%">Start Roster Date:<br>
                                <input type="text" name="start" id ="start" align="LEFT" style='width: 100px' value="<?php echo date('Y-m-d') ?>"></input></td>
                            <td  width="20%">End Roster Date:<br>
                                <input type="text" name="end" id ="end" align="LEFT" style='width: 100px' value="<?php echo date('Y-m-d') ?>"></input></td>

                        </tr></table>
                    <table>
                        <tr>
                            <td align="center"><br><input id="submit" class="button1_red"  type="submit" name="submit" value="Generate Roster" /></td>

                        </tr>


                    </table>
                </form>
                <form id="new" method="post" action="generate_roster.php">
                    <h2><font color="blue">Delete Roster</font></h2>
                    <p>Roster entries can be deleted in bulk, BUT only those records NOT allocated to members will be deleted.<br>
                        Set the date range in the generate roster section. </p><br>
                    <table>
                        <tr><td  width="20%">Start Roster Date:<br>
                                <input type="text" name="start1" id ="start1" align="LEFT" style='width: 100px' value="<?php echo date('Y-m-d') ?>"></input></td>
                            <td  width="20%">End Roster Date:<br>
                                <input type="text" name="end1" id ="end1" align="LEFT" style='width: 100px' value="<?php echo date('Y-m-d') ?>"></input></td>

                        </tr>

                        <tr>
                            <td align="center"><br><input id="delete" name="delete" class="button1_logout"  type="submit" value="Delete Roster" /></td>
                            <td width="30%"></td>

                            <td>Password<br><input type="text" name="password" id ="password" align="LEFT" style='width: 100px' value="" required></input></td>

                        </tr>
                    </table>
                </form>
                <h2><font color="red">If your Toy Library opens every second week, start date must be Sunday before/after the first week the Library is open.</font></h2>
            </div>

        </div>
    </body>
</html>

<?php
$gen = 'No';
if (isset($_POST['submit'])) {
    $gen = 'Yes';
    $date = $_POST['start'];
    //$_SESSION['start'] = $date;
    $end_date = $_POST['end'];
    //$_SESSION['end'] = $end_date;
    $count_records = 0;
    //delete_roster($date, $end_date);
    //include( dirname(__FILE__) . '/get_settings.php');
    $check_overlap = 0;
    $count_rows = 0;
    $check_overlap = check_overlap($date, $end_date);
    // echo "no Rows: " . $check_overlap;
    if ($check_overlap == 0) {
        //echo '<br>Start Date: ' . $date. "End Date: " . $end_date . "fortnight: " . $fortnight;
        $count_rows = generate($date, $end_date, $fortnight, $roster_coord);
    }
}
if (isset($_POST['delete'])) {
    //echo 'hello';
    $date = $_POST['start1'];
    //$_SESSION['start'] = $date;
    $end_date = $_POST['end1'];
    //$_SESSION['end'] = $end_date;
    $password = $_SESSION['settings']['password'];
    if ($_POST['password'] == $password) {
        $result = delete_roster($date, $end_date);
        echo '<center><h2><font color="green">' . $result . '</font></h2></center>';
    }
}
if ($gen == 'Yes') {
    if ($count_rows > 0) {
        echo '<center><h2><font color="green">' . $count_rows . " roster duties added.</font></h2></center>";
        //echo $query;
        exit;
    } else {
        echo '<center><h2><font color="blue">No Roster Duties Added.</font></h2></center>';
        if ($check_overlap > 0) {
            echo '<center><h2><font color="red">There is an overlap, please delete existing records first and then regenerate.</font></h2></center>';
        }
    }
}

function check_overlap($date, $end) {
    //include( dirname(__FILE__) . '/../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $countrows = 0;
    $query_count = "select count(id) as countid from roster where date_roster >= '" . $date . "' and date_roster <= '" . $end . "'";
    //echo $query_count;
    $result_count = pg_Exec($conn, $query_count);
    $numrows = pg_numrows($result_count);
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result_count, $ri);
        $countrows = $row['countid'];
    }
    //echo $countrows;
    return $countrows;
}

function delete_roster($date, $end_date) {
    //include( dirname(__FILE__) . '/../connect.php');

    $conn = pg_connect($_SESSION['connect_str']);
    $query = "delete from roster where date_roster >= '" . $date . "' and date_roster <= '" . $end_date . "' and member_id = 0;";
    $result = pg_Exec($conn, $query);
    if (!$result) {
        $result_txt = 'Error Deleting records.<br>';
    } else {
        $result_txt = '<br>Records from  ' . $date . ' to ' . $end_date . ' have been successfully deleted.<br>';
    }
    //$result_txt .= $query;

    return $result_txt;
}

function generate($date, $end_date, $fortnight, $roster_coord) {

    $count_rows = 0;
    $curr_year = date("Y");
    $day_count = 0;
    $week_count = 1;

    //$fortnight = 'Yes';

    while (strtotime($date) <= strtotime($end_date)) {
        $day_count = $day_count + 1;
        if ($day_count == 7) {
            $day_count = 0;
            $week_count = $week_count + 1;
        }

        $weekday = date('l', strtotime($date));
        if ($fortnight == 'Yes') {
            if ($week_count % 2 != 0) {
                $count_rows = is_open($weekday, $date, $count_rows);
            }
        } else {
            $count_coord = coord($weekday, $date, $count_coord);
            $count_student = student($weekday, $date, $count_student);
            $count_rows = is_open($weekday, $date, $count_rows);
        }

        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        //$total = $count_coord + $count_rows;
    }
    $total = $count_rows + $count_coord + $count_student;
    //echo 'Members: ' . $count_rows . ' Co-Ord: ' . $count_coord . 'Student: ' . $count_student . '<br>';


    return $total;
}

function is_open($weekday, $date, $count_rows) {
    //$count_rows = 0;
    //include( dirname(__FILE__) . '/../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $query_rostertypes = "select  * from rostertypes where trim(rostertype) = 'Roster' and weekday = '" . $weekday . "' order by id;";
    $result = pg_Exec($conn, $query_rostertypes);
    $numrows = pg_numrows($result);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
            $row = pg_fetch_array($result, $ri);
            $duration = $row['nohours'];
            $id = $row['id'];
            $description = $row['description'];
            $type_roster = 'Roster';
            $roster_session = $description;
            $no_volunteers = $row['volunteers'];
            $location = $row['location'];
            for ($i = 1; $i <= $no_volunteers; $i++) {
                add_roster($date, $roster_session, $weekday, $duration, $location, 'Member');
                $count_rows = $count_rows + 1;
                //echo $date . ': ' . $count_rows . '<br>';
            }
        }
    }
    return $count_rows;
}

function coord($weekday, $date, $count_rows) {
    //include( dirname(__FILE__) . '/../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $query_rostertypes = "select  * from rostertypes where trim(rostertype) = 'Roster Coord' and weekday = '" . $weekday . "' order by id;";
    $result = pg_Exec($conn, $query_rostertypes);
    $numrows = pg_numrows($result);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
            $row = pg_fetch_array($result, $ri);
            $duration = $row['nohours'];
            $id = $row['id'];
            $description = $row['description'];
            $type_roster = 'Roster';
            $roster_session = $description;
            $no_volunteers = $row['volunteers'];
            $location = $row['location'];
            for ($i = 1; $i <= $no_volunteers; $i++) {
                add_roster($date, $roster_session, $weekday, $duration, $location, 'Co-ordinator');
                $count_rows = $count_rows + 1;
            }
        }
    }
    return $count_rows;
}

function student($weekday, $date, $count_rows) {
    $conn = pg_connect($_SESSION['connect_str']);
    //include( dirname(__FILE__) . '/../connect.php');
    $query_rostertypes = "select  * from rostertypes where trim(rostertype) = 'Student' and weekday = '" . $weekday . "' order by id;";
    $result = pg_Exec($conn, $query_rostertypes);
    $numrows = pg_numrows($result);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
            $row = pg_fetch_array($result, $ri);
            $duration = $row['nohours'];
            $id = $row['id'];
            $description = $row['description'];
            $type_roster = 'Roster';
            $roster_session = $description;
            $no_volunteers = $row['volunteers'];
            $location = $row['location'];
            for ($i = 1; $i <= $no_volunteers; $i++) {
                add_roster($date, $roster_session, $weekday, $duration, $location, 'Student');
                $count_rows = $count_rows + 1;
            }
        }
    }
    return $count_rows;
}

function add_roster($date, $roster_session, $weekday, $duration, $location, $role) {
    $conn = pg_connect($_SESSION['connect_str']);
    //include( dirname(__FILE__) . '/../connect.php');
    $query = "INSERT INTO roster (member_id, type_roster, roster_session, weekday, date_roster, session_role, location, duration, date_created, approved, status)
                        VALUES (
                        0,
                        'Roster' , '$roster_session', '$weekday',
                        '$date',
                        '$role', '$location',
                         $duration,
                            NOW(), TRUE, 'pending');";

    $result = pg_Exec($conn, $query);
    //echo $query;
}

function check_fortnight($date, $weekday) {
    $indate = 'No';
    switch ($weekday) {
        case 'Monday':
            $date_monday = $date;
        case 'Tuesday':
            $date_monday = date("Y-m-d", strtotime("-1 day", strtotime($date)));
        case 'Wednesday':
            $date_monday = date("Y-m-d", strtotime("-2 day", strtotime($date)));
        case 'Thursday':
            $date_monday = date("Y-m-d", strtotime("-3 day", strtotime($date)));
        case 'Friday':
            $date_monday = date("Y-m-d", strtotime("-4 day", strtotime($date)));
        case 'Saturday':
            $date_monday = date("Y-m-d", strtotime("-5 day", strtotime($date)));
        case 'Sunday':
            $date_monday = date("Y-m-d", strtotime("-6 day", strtotime($date)));
            break;
    }

    $date_str = DateTime::createFromFormat("Y-m-d", $date_monday);
    $str_week_second = 'second ' . $weekday . ' of ' . $date_str->format("F") . ' ' . $date_str->format("Y");
    $str_week_forth = 'fourth ' . $weekday . ' of ' . $date_str->format("F") . ' ' . $date_str->format("Y");
    if ($date == date('Y-m-d', strtotime($str_week_second))) {
        $indate = 'Yes';
    }
    if ($date == date('Y-m-d', strtotime($str_week_forth))) {
        $indate = 'Yes';
    }
    return $indate;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
