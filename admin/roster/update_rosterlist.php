<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');
$roster_id = $_GET['roster_id'];
$member_id = $_GET['member_id'];
$conn = pg_connect($_SESSION['connect_str']);
$query = "update roster set member_id = " .  $member_id . " , approved = true, modified = now(), status = 'pending' where id = " . $roster_id . ";";
$result_roster = pg_Exec($conn, $query);
if (!$result_roster) {
    echo 'Error in saving';
    echo $query;
    //exit;
} else {
    echo 'saved';
}


