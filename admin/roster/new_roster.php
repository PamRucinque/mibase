<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>    
    <body id="main_body" >
        <div id="form_container">


            <?php
            include( dirname(__FILE__) . '/../menu.php');
            include( dirname(__FILE__) . '/../header_detail/header_detail.php');
            if (isset($_POST['submit'])) {
                //include( dirname(__FILE__) . '/../connect.php');
                $session = $_POST['roster_session'];
                $status = $_POST['status'];

                $comments = $_POST['comments'];
                $comments = str_replace("'", "`", $comments);
                //$pieces = explode(":", $session);
                $weekday = $_POST['weekday'];
                //$roster_session = $pieces[1];
                $complete = $_POST['complete'];
                $max = $_POST['generate'];
                for ($x = 1; $x <= $max; $x++) {
                    $query = "INSERT INTO roster (member_id, location, type_roster, roster_session, date_roster, session_role, weekday, complete, approved, duration, date_created, status, comments)
                        VALUES (
                        {$_POST['member_id']},
                        '{$_POST['location']}',
                        '{$_POST['rostertype']}',
                        '{$session}',
                        '{$_POST['date_roster']}',
                        '{$_POST['role']}',
                        '{$weekday}',
                         '{$complete}', TRUE,
                         {$_POST['duration']},
                            NOW(), '{$status}','{$comments}');";
                    $conn = pg_connect($_SESSION['connect_str']);       
                    $result = pg_Exec($conn, $query);
                }
                if (!$result) {
                    echo "An INSERT query error occurred.\n";
                    echo $query;
                    exit;
                } else {
                    echo '<h3><font color="red">' . $max . ' New Roster(s) Added.</font></h1>';
                    $_SESSION['borid'] = $_POST['member_id'];
                }

            } else {
                
            }


            include( dirname(__FILE__) . '/roster_new_form.php');
            ?>
        </div>

    </body>
</html>