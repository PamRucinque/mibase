<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

    //include( dirname(__FILE__) . '/../connect.php');
    $session = 'no time';

    //$pieces = explode(":", $session);
    $weekday = 'no weekday';
    //$roster_session = $pieces[1];
    $complete = TRUE;
    if ($type_hrs > 1){
        $duration = $type_hrs;
    }else{
        $duration = 1;
    }

    $query = "INSERT INTO roster (member_id, type_roster, roster_session, date_roster, session_role, weekday, complete, approved, date_created, status,comments,duration)
                        VALUES (
                        {$_SESSION['borid']},
                        'Exemption',
                        '{$session}',
                         now(), 'Member',
                        '{$weekday}',
                        TRUE, TRUE,
                         NOW(), 'completed', 'Credit Roster Levy', {$duration} );";
    
                         $conn = pg_connect($_SESSION['connect_str']);
                         $result = pg_Exec($conn, $query);

    if (!$result) {
        echo "An INSERT query error occurred.\n";
        echo $query;
        exit;
    }

$redirect = 'Location: ../members/update/member_detail.php?id=' . $_SESSION['borid'];               //print $redirect;
header($redirect);
