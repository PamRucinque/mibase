<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');



//include( dirname(__FILE__) . '/../connect.php');

$query = "SELECT
borwrs.id as id,
borwrs.surname as surname,
borwrs.firstname as firstname,
borwrs.wwc as wwc,
borwrs.expired as expired,
1 as contact,
substring(borwrs.rostertype from 9 for 3) as pref1,
substring(borwrs.rostertype2 from 9 for 3) as pref2,
(membertype.duties - (SELECT coalesce(sum(roster.duration),0) FROM roster WHERE roster.member_id = borwrs.id AND (roster.date_roster >= (borwrs.expired - (membertype.expiryperiod * '1 month'::INTERVAL))))) as duties 
FROM (membertype INNER JOIN borwrs ON membertype.membertype = borwrs.membertype)
Where borwrs.member_status = 'ACTIVE'  
UNION ALL 
SELECT
borwrs.id as id,
borwrs.partnerssurname as surname,
borwrs.partnersname as firstname,
borwrs.wwc as wwc,
borwrs.expired as expired,
2 as contact,
substring(borwrs.rostertype from 9 for 3) as pref1,
substring(borwrs.rostertype2 from 9 for 3) as pref2,
(membertype.duties - (SELECT coalesce(sum(roster.duration),0) FROM roster WHERE roster.member_id = borwrs.id AND (roster.date_roster >= (borwrs.expired - (membertype.expiryperiod * '1 month'::INTERVAL))))) as duties 
FROM (membertype INNER JOIN borwrs ON membertype.membertype = borwrs.membertype)
Where borwrs.member_status = 'ACTIVE'  and partnerssurname != '' and surname != motherssurname
order by surname,firstname;";

$conn = pg_connect($_SESSION['connect_str']);
$result_mem = pg_Exec($conn, $query);
$str_array = '[[0, \'\', 0], ';
$numrows = pg_numrows($result_mem);

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_mem, $ri);
    $duties = $row['duties'];
    $wwc = $row['wwc'];
    if ($duties > 0) {
        if ($row['pref1'] != '') {
            $str_duties = ' (' . strval(round($row['duties'])) . ' ' . $row['pref1'] . ' ' . $row['pref2'] . ')';
        } else {
            $str_duties = '(' . strval(round($row['duties'])) . ')';
        }
    } else {
        $str_duties = '';
    }
    if ($row['expired'] < date('Y-m-d')) {
        $str_duties .= ' E';
    }
    if (($wwc != null) && ( $wwc != '')) {
        $str_duties .= ' W';
    }

    $surname = substr($row['surname'], 0, 20);
    $firstname = substr($row['firstname'], 0, 10);
    $member_name = $surname . ', ' . $firstname . $str_duties;
    $member_name = str_replace("'", "`", $member_name);
    $member_name = preg_replace('/[^a-z\d\,() ]/i', '', $member_name);
    //str_array .= '[' . $row['id'] . ', \'' . $member_name . '\',' . $todo . ']';

    $str_array .= '[' . $row['id'] . ', \'' . $member_name . '\',' .  ']';
    if ($ri == $numrows - 1) {
        $str_array .= '];';
    } else {
        $str_array .= ',';
    }
}

//echo $str_array;
?>


