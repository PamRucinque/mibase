<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <link href="../css/jquery-ui.min.css" rel="stylesheet" type="text/css">
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/jquery-ui.js"></script>
    </head>    

    <body id="main_body" >
        <div id="form_container">
            <?php
            $ok = 'No';
            include( dirname(__FILE__) . '/../menu.php');
            $session = $_POST['roster_session'];
            //$pieces = explode(":", $session);
            $weekday = $_POST['weekday'];
            $comments = $_POST['comments'];
            $comments = str_replace("'", "`", $comments);
            //$roster_session = trim($pieces[1]);

            if (isset($_POST['submit'])) {
                //include( dirname(__FILE__) . '/../connect.php');
                $role = trim($_POST['role']);
                $status = trim($_POST['status']);
                $now = date('Y-m-d H:i:s');
                $query_roster = "UPDATE roster SET 
                    date_roster = '{$_POST['date_roster']}',
                    type_roster = '{$_POST['rostertype']}',
                    roster_session = '$session',
                    member_id = '{$_POST['member_id']}',
                    duration = '{$_POST['duration']}',
                    weekday = '$weekday',
                    session_role = '{$role}',
                    status = '{$status}',
                    comments = '{$comments}',
                    modified = '{$now}'
                    WHERE id=" . $_POST['id'] . ";";

                $result_roster = pg_Exec($conn, $query_roster);
                $conn = pg_connect($_SESSION['connect_str']);
                //echo $query;
                //echo $connection_str;
                if (!$result_roster) {
                    echo "An UPDATE query error occurred.\n";
                    echo $query_roster;
                    //exit;
                } else {
                    echo "<br><h4>This Duty Roster was successfully saved.";
                    //echo 'format Toy id: ' . $format_toyid;
                    echo '<a class="button1" href="roster.php">Back to Roster</a>';
                    echo '<a class="button1_red" href="../members/update/member_detail.php?id=' . $_SESSION['borid'] . '">Member Details</a>';
                }
            } else {
                include( dirname(__FILE__) . '/data/get_roster.php');
                include( dirname(__FILE__) . '/roster_form.php');
            }
            ?>

        </div> 
    </body>
</html>
