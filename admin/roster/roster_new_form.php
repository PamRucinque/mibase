<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<div id="form"  style="background-color:lightgrey;height:300px;" align="left">
    <script type="text/javascript">
        $(function () {
            var pickerOpts = {
                dateFormat: "yy-m-d",
                showOtherMonths: true

            };
            $("#date_roster").datepicker(pickerOpts);
        });

    </script>
    <form id="edit_report" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'new_roster.php'; ?>">
        <br>

        <h2>Add a New Roster Duty</h2>
        <table width="100%" style="background-color:lightgray;" align="left">


            <tr>
                <td align="left" width="15%" colspan="2"><label>Date Roster Duty: </label><input text-align="left" type='text' size="20" id='date_roster' name='date_roster' value='<?php echo date("d F Y"); ?>' /><br></td>
                <td align="left" width="15%">Select Roster Type: </td><td align="left"><?php include( dirname(__FILE__) . '/get_type_roster_new.php'); ?></td>
            </tr>  
            <tr>
                <td align="left" width="15%">Session: </td>
                <td align="left"><?php include( dirname(__FILE__) . '/data/get_session_new.php'); ?></td> 
                <td align="left">Number to Generate:</td><td align="left"><input type="Text" name="generate" id="generate" align="LEFT" size="10" value=1 min="1"></input><br></td></tr>
            <td align="left" width="15%">Location: </td><td align="left"><?php include( dirname(__FILE__) . '/data/get_location.php'); ?></td></tr>  
            <td align="left" width="15%">Weekday: </td><td align="left"><?php include( dirname(__FILE__) . '/data/get_weekday_new.php'); ?></td></tr>  
            <tr><td align="left">Select Member: </td><td align="left"><?php include( dirname(__FILE__) . '/get_member.php'); ?></td> 
                <td align="left">Duration (hrs):</td><td align="left"><input type="Text" name="duration" id="duration" align="LEFT" size="10" value=1></input><br></td>
            <tr>
                <td  align="left"><label>Role: </label></td><td align="left">
                    <select id="role" name="role">
                        <option value="Member" selected="selected">Member</option>
                        <option value="Emergency" >Emergency</option>
                        <option value="Co-ordinator" >Session Co-ordinator</option>
                        <option value="Student" >Student</option>
                        <option value="Toy Cleaning" >Toy Cleaning</option>
                        <option value="Start Closed" >Start Closed</option>
                        <option value="End Closed" >End Closed</option>
                    </select></td>
            </tr>
            <tr>
                <td  align="left"><label>Status: </label></td><td align="left">
                    <select id="status" name="status">
                        <option value="pending" selected='selected'>pending</option>
                        <option value="completed" >completed</option>
                        <option value="no show" >no show</option>
                        <option value="fee applied" >fee applied</option>
                        <option value="Closed" >Closed</option>
                        <option value="Note" >Note</option>
                    </select></td>
            </tr>
            <tr>
                <td  align="left"><label>Completed: </label></td><td align="left">
                    <select id="complete" name="complete">
                        <option value=FALSE selected="selected">No</option>
                        <option value=TRUE >Yes</option>
                    </select></td>

            </tr>
            <tr>
                <td align="left">Comments:</td><td align="left"><input type="Text" name="comments" id="comments" align="LEFT" size="75" value=""></input><br></td>

            </tr>
            <tr><td></td><td></td><td></td><td align="right"><input id="saveForm" class="button1" type="submit" name="submit" value="Generate" /></td></tr>
        </table>


    </form>

    <a class="button1_blue" href="roster.php">Back to Roster</a>  
</div>

