<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "yy-m-d",
            showOtherMonths: true

        };
        $("#date_roster").datepicker(pickerOpts);
    });

</script>
<div id="form"  style="background-color:whitesmoke;height:200px;" align="left">
    <form id="edit_report" class="appnitro" method="post" action="<?php echo 'edit_roster.php'; ?>">

        <table width="100%" style="background-color:lightgray;" align="left">
            <tr>
                <td>Date: <input type="text" name="date_roster" id ="date_roster" align="LEFT" value="<?php echo $date_roster; ?>"></input><br></td>
            </tr>
            <tr><td align="left" width="15%">Select Roster Type: </td><td align="left"><?php include( dirname(__FILE__) . '/get_type_roster.php'); ?></td></tr>  
            <?php echo $rostertype . '<br>'; ?>
            <tr><td align="left" width="15%">Session: </td><td align="left"><?php include( dirname(__FILE__) . '/data/get_session.php'); ?></td></tr>  
            <td align="left" width="15%">Weekday: </td><td align="left"><?php include( dirname(__FILE__) . '/data/get_weekday.php'); ?></td></tr>  

            <tr><td align="left">Select Member: </td><td align="left"><?php include( dirname(__FILE__) . '/data/get_member_edit.php'); ?></td> 
                <td align="left">Duration (hrs):</td><td align="left"><input type="Text" name="duration" id="duration" align="LEFT" size="10" value="<?php echo $duration; ?>"></input><br></td></tr>
            <td align="left">Comments:</td><td align="left"><input type="Text" name="comments" id="comments" align="LEFT" size="75" value="<?php echo $comments; ?>"></input><br></td></tr>

            <tr>
                <td  align="left"><label>Role: </label></td><td align="left">
                    <select id="role" name="role">
                        <option value="<?php echo $session_role; ?>" selected='selected' ><?php echo $session_role; ?></option>
                        <option value="Member" >Member</option>
                        <option value="Emergency" >Emergency</option>
                        <option value="Co-ordinator" >Session Co-ordinator</option>
                        <option value="Student" >Student</option>
                        <option value="Toy Cleaning" >Toy Cleaning</option>

                    </select></td>
            </tr>
            <tr>
                <td  align="left"><label>Status: </label></td><td align="left">
                    <select id="status" name="status">
                        <option value="<?php echo $status; ?>" selected='selected'><?php echo $status; ?></option>
                        <option value="completed" >completed</option>
                        <option value="pending" >pending</option>
                        <option value="no show" >no show</option>
                        <option value="fee applied" >fee applied</option>
                        <option value="Closed" >Closed</option>
                        <option value="Note" >Note</option>
                    </select></td>
            </tr>
            <tr><td></td><td></td><td></td><td align="right">
                    <input id="saveForm" class="button1" type="submit" name="submit" value="Change Roster Duty" /></td></tr>
        </table>
        <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>"> 

    </form>
    <?php
    echo '<a class="button1_green" href="../members/update/member_detail.php?id=' . $_SESSION['borid'] . '">Member Details</a>';
    echo '<a class="button1_green" href="roster.php">Roster</a>';
    echo '<a class="button1_logout" href="delete_roster.php?id=' . $_GET['id'] . '">Delete this Roster</a>';
    ?>
</div>
