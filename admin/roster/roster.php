<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');



function get_end() {
    //include( dirname(__FILE__) . '/../connect.php');
    
    $query_count = "select max(date_roster) as max_date from roster;";
    $max = date('Y-m-d', strtotime('last day of this month', time()));
    //echo $query_count;
    $conn = pg_connect($_SESSION['connect_str']);
    $result_count = pg_Exec($conn, $query_count);
    $numrows = pg_numrows($result_count);
    if ($numrows > 0) {
        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = pg_fetch_array($result_count, $ri);
            $max = $row['max_date'];
        }
    }

    //echo $countrows;
    return $max;
}
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <script type="text/javascript">
            $(function () {
                var pickerOpts = {
                    dateFormat: "yy-mm-dd",
                    showOtherMonths: true

                };
                $("#start").datepicker(pickerOpts);
                $("#end").datepicker(pickerOpts);
            });</script>

    </head>

    <body id="main_body" >
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            include( dirname(__FILE__) . '/../header_detail/header_detail.php');
            include( dirname(__FILE__) . '/data/get_settings.php');
            include( dirname(__FILE__) . '/members_array.php');
            $weekday = '';
            $roster_type = '';
            if (isset($_SESSION['roster_type'])) {
                $roster_type = $_SESSION['roster_type'];
            }
            if (isset($_SESSION['weekday'])) {
                $weekday = $_SESSION['weekday'];
            }

            if (isset($_POST['start'])) {
                $start = $_POST['start'];
                $_SESSION['start'] = $_POST['start'];
            } else {
                if (isset($_SESSION['start'])) {
                    $start = $_SESSION['start'];
                } else {
                    //$month_start = strtotime('first day of this month', time());
                    $month_start = strtotime('now', time());
                    $start = date('Y-m-d', $month_start);
                }
            }
            if (isset($_POST['empty'])) {
                $_SESSION['member'] = 'empty';
                $weekday = $_SESSION['weekday'];
            } else {
                $_SESSION['member'] = 'all';
                
            }
            if (isset($_POST['weekday'])) {

                if ($_POST['weekday'] != $_SESSION['weekday']) {
                    $_SESSION['weekday'] = $_POST['weekday'];
                    $weekday = $_SESSION['weekday'];
                }
            }
            if (isset($_POST['roster_type'])) {

                if ($_POST['roster_type'] != $roster_type) {
                    $_SESSION['roster_type'] = $_POST['roster_type'];
                    $roster_type = $_SESSION['roster_type'];
                }
            }


            if (isset($_POST['end'])) {
                $end = $_POST['end'];
                $_SESSION['end'] = $_POST['end'];
            } else {
                if (isset($_SESSION['end'])) {
                    $end = $_SESSION['end'];
                } else {
                    $month_end = strtotime('last day of this month', time());
                    $end = date('Y-m-d', $month_end);
                    $end = get_end();
                }
            }
            //echo $_SESSION['roster_type'];
            ?>
            <table><tr><td width="60%">
                        <?php //include( dirname(__FILE__) . '/../reports/roster.php');  ?>   
                        <form name="roster" id="roster" method="post" action="roster.php" >    
                            Date From: <input type="text" style="width: 80px;background-color:#F3F781;" name="start" id ="start" align="LEFT" value="<?php echo $start; ?>"  onchange="this.form.submit()"></input>
                            To: <input type="text" name="end" style="width: 80px;background-color:#F3F781;" id ="end" align="LEFT" value="<?php echo $end; ?>"  onchange="this.form.submit()"></input>
                            <?php
                            include( dirname(__FILE__) . '/data/get_weekday_filter.php');
                            include( dirname(__FILE__) . '/data/get_roster_type_filter.php');
                            if ($_SESSION['member'] == 'empty') {
                                echo '<input id="all" class="button1" type="submit" name="all" value="Show All" />';
                            } else {
                                echo '<input id="empty" class="button1" type="submit" name="empty" value="Vacant" />';
                            }
                            ?>
                        </form>
                    </td>


                    <?php
                   //if ($branch == 'admin') {
                        //echo '<td width="10%" align="right"><a class="button1_blue" href="new_roster.php">New Roster</a></td>';
                        echo '<td width="10%" align="right"><a class="button1_blue" href="generate_roster.php">Generate/Delete Roster</a></td>';
                   //}
                    echo '<td width="10%" align="right"><a class="button1_blue" href="new_roster.php">New Roster</a></td>';
                    ?>


                </tr></table>

            <?php
            include( dirname(__FILE__) . '/rosters.php');
//include( dirname(__FILE__) . '/roster_edit_form.php');
            ?>

        </div>

        <script type="text/javascript">

            //array of all members and their id

            var members = <?php echo $str_array; ?>

            //  var spaces = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            /*function to return the contents of the pick list table cell
             Param mid = the currently selected member id
             */
            function memberPickList(rid, memid) {
                //alert('member picklist '+rid+ ' ' +memid);
                var h = "<select onchange=\"alocateRoster(" + rid + ", this.value)\">";
                for (var i = 0; i < members.length; i++) {
                    h += "<option  value=\"" + members[i][0] + "\"";
                    if (memid == members[i][0]) {
                        h += "\" selected=\"selected\" ";
                    }
                    h += " \">" + members[i][1] + "</option>";
                }
                h += "</select>";
                return h;
            }


            //initially fill in all member pick lists for all rows of the table
            $(document).ready(fillMemPickLists());


            function fillMemPickLists() {
                $("#res tr:gt(0)").each(function (i, tr) {

                    var rrid = parseInt($(".rid", tr).text());
                    var rmemid = parseInt($(".mid", tr).text());
                    $(".mpl", tr).html(memberPickList(rrid, rmemid));
                    $(".mid", tr).html("<a class=\"button_small_red\" href=\"../members/update/member_detail.php?borid=" + rmemid + "\" >" + rmemid + "</a>");

                });
            }


            /*
             * Allocate a member to a roster 
             * rid = roster id
             * mid = member id
             */
            function alocateRoster(rid, memid) {
                console.log("alocateRoster" + rid + " " + memid);
                $.ajax({url: "update_rosterlist.php",
                    data: {roster_id: rid, member_id: memid},
                    success: function (result) {
                        if (result.match("saved")) {
                            //update the member button
                            $("#r" + rid + " .mid").html("<a class=\"button_small_green\" href=\"../members/update/member_detail.php?borid=" + memid + "\" >" + memid + "</a>");
                            //update the member pick list for this row
                            $("#r" + rid + " .mpl").html(memberPickList(rid, memid));
                        } else if (result.match("error.*")) {
                            alert(result);
                            //put the member selection box back the way it was
                            var memid1 = parseInt($("#r" + rid + " .mid").text());
                            $("#r" + rid + " .mpl").html(memberPickList(rid, memid1));
                        } else {
                            alert(result);
                            //put the member selection box back the way it was
                            var memid1 = parseInt($("#r" + rid + " .mid").text());
                            $("#r" + rid + " .mpl").html(memberPickList(rid, memid1));
                        }
                    },
                    error: function () {
                        alert('error saving');
                        //put the member selection box back the way it was
                        var memid1 = parseInt($("#r" + rid + " .mid").text());
                        $("#r" + rid + " .mpl").html(memberPickList(rid, memid1));
                    }});
            }

        </script>
    </body>
</html>

