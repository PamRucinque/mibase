<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 

        <script type="text/javascript">
            $(function () {
                var pickerOpts = {
                    dateFormat: "d MM yy",
                    showOtherMonths: true

                };
                $("#start").datepicker(pickerOpts);
                $("#end").datepicker(pickerOpts);
            });

        </script>
    </head>
    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            ?>
            <br>
            <h2>Custom Roster Generation - for extra session per week.</h2>
            <form id="new" method="post" action="generate_roster_special.php">
                <div id="form1234" style="background-color:lightgray;" align="left">
                    <table align="top">

                        <tr><td  width="20%">Start Roster Date:<br>
                                <input type="text" name="start" id ="start" align="LEFT" style='width: 100px' value="<?php echo date('Y-m-d') ?>"></input></td>
                            <td  width="20%">End Roster Date:<br>
                                <input type="text" name="end" id ="end" align="LEFT" style='width: 100px' value="<?php echo date('Y-m-d') ?>"></input></td>

                        </tr></table>
                    <br>
                    <table><tr>
                            <td align="center"><input id="submit" class="button1_red"  type="submit" name="submit" value="Generate Roster" /></td>
                        </tr>

                    </table>
                    <h2><font color="red">If your Toy Library opens every second week, start date must be Sunday before/after the first week the Library is open.</font></h2>
                </div>
            </form>
        </div>
    </body>
</html>

<?php
if (isset($_POST['submit'])) {

    $date = $_POST['start'];
    $end_date = $_POST['end'];
    $count_records = 0;
    //delete_roster($date, $end_date);
    //include( dirname(__FILE__) . '/get_settings.php');
    $check_overlap = 0;
    $count_rows = 0;
    //$check_overlap = check_overlap($date, $end_date);

//    if ($check_overlap == 0) {
        //echo '<br>Start Date: ' . $date. "End Date: " . $end_date . "fortnight: " . $fortnight;
        $count_rows = generate($date, $end_date, $fortnight, $roster_coord);
 //   }
}

if ($count_rows > 0) {
    echo '<center><h2><font color="green">' . $count_rows . " roster duties added.</font></h2></center>";
    //echo $query;
    exit;
} else {
    echo '<center><h2><font color="blue">No Roster Duties Added.</font></h2></center>';
    if ($check_overlap > 0) {
        echo '<center><h2><font color="red">There is an overlap, please delete existing records first and then regenerate.</font></h2></center>';
    }
}

function check_overlap($date, $end) {
    //include( dirname(__FILE__) . '/../connect.php');
    $countrows = 0;
    $query_count = "select count(id) as countid from roster where date_roster >= '" . $date . "' and date_roster <= '" . $end . "'";
    //echo $query_count;
    $result_count = pg_Exec($conn, $query_count);
    $numrows = pg_numrows($result_count);
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result_count, $ri);
        $countrows = $row['countid'];
    }
    //echo $countrows;
    return $countrows;
}

function delete_roster($date, $end_date) {
    //include( dirname(__FILE__) . '/../connect.php');
    $query = "delete from roster where date_roster >= '" . $date . "' and date_roster <= '" . $end_date . "' and member_id = 0;";
    $result = pg_Exec($conn, $query);
    if (!$result) {
        echo 'Error Deleting records.<br>';
    }
}

function generate($date, $end_date, $fortnight, $roster_coord) {


    $curr_year = date("Y");
    $day_count = 0;
    $week_count = 1;

    //$fortnight = 'Yes';

    while (strtotime($date) < strtotime($end_date)) {
        $day_count = $day_count + 1;
        if ($day_count == 7) {
            $day_count = 0;
            $week_count = $week_count + 1;
        }
        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        $weekday = date('l', strtotime($date));
        if ($fortnight == 'Yes') {
            if ($week_count % 2 != 0) {
                $count_rows = is_open($weekday, $date, $count_rows,$roster_coord);
            }
        } else {
            $count_rows = is_open($weekday, $date, $count_rows,$roster_coord);
        }
    }
    return $count_rows;
}

function is_open($weekday, $date, $count_rows,$roster_coord) {
    //include( dirname(__FILE__) . '/../connect.php');
    $query_rostertypes = "select  * from rostertypes where rostertype = 'Roster' and weekday = '" . $weekday . "' order by id;";
    $result = pg_Exec($conn, $query_rostertypes);
    $numrows = pg_numrows($result);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
            $row = pg_fetch_array($result, $ri);
            $duration = $row['nohours'];
            $id = $row['id'];
            $description = $row['description'];
            $type_roster = 'Roster';
            $roster_session = $description;
            $no_volunteers = $row['volunteers'];
            $no_volunteers = 1;
            $location = $row['location'];
            for ($i = 1; $i <= $no_volunteers; $i++) {
                if (($i == 1)&&($roster_coord == 'Yes')) {
                    add_roster($date, $roster_session, $weekday, $duration, $location,'Member');
                } else {
                    //add_roster($date, $roster_session, $weekday, $duration, $location,'Member');
                }

                $count_rows = $count_rows + 1;
            }
        }
    }
    return $count_rows;
}

function add_roster($date, $roster_session, $weekday, $duration, $location,$role) {

    //include( dirname(__FILE__) . '/../connect.php');
    $query = "INSERT INTO roster (member_id, type_roster, roster_session, weekday, date_roster, session_role, location, duration, date_created, approved, status)
                        VALUES (
                        0,
                        'Roster' , '$roster_session', '$weekday',
                        '$date',
                        '$role', '$location',
                         $duration,
                            NOW(), TRUE, 'pending');";

    $result = pg_Exec($conn, $query);
    //echo $query;
}

function check_fortnight($date, $weekday) {
    $indate = 'No';
    switch ($weekday) {
        case 'Monday':
            $date_monday = $date;
        case 'Tuesday':
            $date_monday = date("Y-m-d", strtotime("-1 day", strtotime($date)));
        case 'Wednesday':
            $date_monday = date("Y-m-d", strtotime("-2 day", strtotime($date)));
        case 'Thursday':
            $date_monday = date("Y-m-d", strtotime("-3 day", strtotime($date)));
        case 'Friday':
            $date_monday = date("Y-m-d", strtotime("-4 day", strtotime($date)));
        case 'Saturday':
            $date_monday = date("Y-m-d", strtotime("-5 day", strtotime($date)));
        case 'Sunday':
            $date_monday = date("Y-m-d", strtotime("-6 day", strtotime($date)));
            break;
    }

    $date_str = DateTime::createFromFormat("Y-m-d", $date_monday);
    $str_week_second = 'second ' . $weekday . ' of ' . $date_str->format("F") . ' ' . $date_str->format("Y");
    $str_week_forth = 'fourth ' . $weekday . ' of ' . $date_str->format("F") . ' ' . $date_str->format("Y");
    if ($date == date('Y-m-d', strtotime($str_week_second))) {
        $indate = 'Yes';
    }
    if ($date == date('Y-m-d', strtotime($str_week_forth))) {
        $indate = 'Yes';
    }
    return $indate;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
