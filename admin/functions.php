<?php

function fillOtherSessionVariables() {
    $_SESSION['category'] = '';
    $_SESSION['toyiderror'] = '';
    $_SESSION['partid'] = '';
    $_SESSION['borid'] = '';
    $_SESSION['override'] = 'No';
    $_SESSION['logo'] = '';
    $_SESSION['toyid'] = '';
    $_SESSION['location'] = '';
    $_SESSION['age'] = '';
    $_SESSION['weekday'] = '';
    $_SESSION['roster_type'] = '';
    $_SESSION['template'] = '';
    $_SESSION['error'] = '';
    $_SESSION['search'] = '';
    $_SESSION['idcat'] = '';
    $_SESSION['idcat_select'] = '';
    $_SESSION['mibase'] = '';
    $_SESSION['search_box'] = '';
    $_SESSION['party'] = date('Y-m-d');
    $_SESSION['timezone'] = 'Australia/ACT';
    $_SESSION['return_status'] = '';
    $_SESSION['fine'] = '';
}

function fillSettingSessionVariables($connect_str) {

    $array = array();

    //set default values
    $array['selectbox'] = 'No';
    $array['overdue_txt'] = 'OVERDUE';
    $array['storage_label'] = 'No';
    $array['show_password'] = 'No';
    $array['roster_alerts'] = 'Yes';
    $array['holiday_override'] = 'No';
    $array['toy_holds'] = 'No';
    $array['hide_balance'] = 'No';
    $array['vol_balance'] = 'No';
    $array['display_overdues'] = 'Yes';
    $array['admin_notes'] = 'Yes';
    $array['receipt_printer'] = 'No';
    $array['receipt_email'] = 'No';
    $array['hide_value'] = 'No';
    $array['show_desc'] = 'No';
    $array['display_overdues'] = 'Yes';
    $array['free_rent_btn'] = 'No';
    $array['disable_returnall'] = 'No';
    $array['checked'] = 'No';
    $array['loan_restrictions'] = 'No';
    $array['loanperiod'] = 14;
    $array['address'] = '';
    $array['user_toys'] = 'User1';
    $array['sub_category_loans'] = 'No';
    $array['format_toyid'] = 'No';
    $array['rent_default'] = 0;
    $array['reserve_all'] = 'No';
    $array['sub_category_loans'] = 'No';
    $array['stocktake_override'] = 'No';
    $array['loan_receipt'] = 'No';
    $array['receipt_email'] = 'No';
    $array['invoices'] = 'No';
    $array['default_paymenttype'] = 'Cash';
    $array['timezone'] = 'Australia/ACT';
    $array['roster'] = 'Yes';
    $array['nationality_label'] = '';
    $array['reservations'] = 'No';
    $array['special_loan'] = 'No';
    $array['rent_factor'] = 'No';
    $array['extra_rent'] = 'No';
    $array['charge_rent'] = 'No';
    $array['hide_city'] = 'No';
    $array['global_rent'] = 0;
    $array['grace'] = 0;
    $array['speical_return'] = 'No';
    $array['max_daily_fine'] = 100;
    $array['unlock_returns'] = 'No';
    $array['special_return'] = 'No';
    $array['rentasfine'] = 'No';
    $array['fine_factor'] = 1;
    $array['fine_value'] = 0;
    $array['daily_fine'] = 'No';
    $array['extra_fine'] = 'No';
    $array['roundup_week'] = 'No';
    $array['hire_factor'] = 1;
    $array['email_from'] = 'info@mibase.com.au';
    $array['disable_cc_email'] = 'No';
    $array['loan_default_missing_paid'] = 'No';
    $array['auto_debit_missing'] = 'No';
    $array['loan_default_missing_alert'] = 'Yes';
    $array['admin_renewal_list'] = 'No';
    $array['toy_holds'] = 'No';
    $array['simple_new_member'] = 'Yes';
    $array['vol_no_toy_edits'] = 'Yes';
    $array['suburb_title'] = 'Suburb:';
    $array['city_title'] = 'City:';
    $array['renew_button'] = 'Yes';
    $array['hide_city'] = 'Yes';
    $array['selectbox'] = 'No';
    $array['user1_borwrs'] = '';
    $array['nationality_label'] = '';
    $array['override_date_loan'] = 'No';
    $array['storage_label'] = 'Storage: ';
    $array['donated_label'] = 'Donated by:';
    $array['format_toyid'] = 'No';
    $array['idcat_noedit'] = 'Yes';
    $array['catsort'] = 'Yes';
    $array['automatic_debit_off'] = 'Yes';
    $array['renew_member_email'] = 'No';
    $array['settings_expired'] = '';
    $array['new_member_email'] = 'No';
    $array['password'] = 'mibase';
    $array['reuse_memberno'] = 'No';
    $array['expired'] = '';
    $array['online_conduct'] = 'No';
    $array['weekly_fine'] = 'Yes';
    $array['total_value'] = 'No';
    $array['logo_height'] = '150px';
    $array['library_heading_off'] = 'No';
    $array['menu_member_options'] = 'No';
    $array['menu_faq'] = 'No';
    $array['sub_category_label'] = 'Sub Category';
    $array['loan_weighting'] = 'No';
    $array['freerent'] = 'No';
    $array['set_due_date'] = 'No';
    $array['set_loan_date'] = 'No';



    $array['monday'] = 0;
    $array['tuesday'] = 0;
    $array['wednesday'] = 0;
    $array['thursday'] = 0;
    $array['friday'] = 0;
    $array['saturday'] = 0;
    $array['sunday'] = 0;


    //$_SESSION['renew_member_off'] = 'No';


    $query_settings = "SELECT setting_name, setting_value FROM settings;";

    $dbconn = pg_connect($connect_str);

    $result_settings = pg_Exec($dbconn, $query_settings);
    $numrows = pg_numrows($result_settings);

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result_settings, $ri);
        $array[$row['setting_name']] = $row['setting_value'];
    }

    pg_FreeResult($result_settings);
    pg_Close($dbconn);
    return $array;
}

function get_host_name($fqn, $shared_domain) {
    return substr($fqn, 0, strlen($fqn) - strlen($shared_domain) - 1);
}

function getLibraryPostgresPassword($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd, $library_code) {

    try {
        //does the toybase db exist
        $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=template1";
        $pdo = new PDO($connect_pdo, $toybasedbuser, $toybasedbpasswd);
        $sql = "SELECT datname FROM pg_catalog.pg_database WHERE datname = ? ;";
        $sth = $pdo->prepare($sql);
        $array = array($toybasedbname);
        $sth->execute($array);
        $result = $sth->fetchAll();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            //throw new Exception($stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>');
            return '';
        }

        if (sizeof($result) == 1) {
            //we found the toybase database
            //now get the database password for the library database
            $toybase_connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $toybasedbname;
            $toybase_pdo = new PDO($toybase_connect_pdo, $toybasedbuser, $toybasedbpasswd);
            $toybase_query_login = "select dbpassword FROM libraries WHERE library_code = ? ;";
            $toybase_sth = $toybase_pdo->prepare($toybase_query_login);
            $toybase_array = array($library_code);
            $toybase_sth->execute($toybase_array);

            $toybase_result = $toybase_sth->fetchAll();
            $toybase_numrows = $toybase_sth->rowCount();
            $toybase_stherr = $toybase_sth->errorInfo();
            if ($toybase_stherr[0] != '00000') {
               // return "An error occurred checking the login: " . $toybase_stherr[0] . " " . $toybase_stherr[1] . "" . $toybase_stherr[2];
                return '';
            }

            if ($toybase_numrows > 0) {
                $row = $toybase_result[0];
                return $row['dbpassword'];
            } else {
                return '';
            }
        } else {
            //the toybase database does not exist.
            return '';
        }
    } catch (Exception $e) {
        return '';
        
    }
}

function getLoginType($username, $password, $connect_pdo, $dbuser, $dbpasswd) {

    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $query_login = "select username, password, level, location 
                    FROM users 
                    WHERE username = ? 
                    AND password = ? ;";
    $sth = $pdo->prepare($query_login);
    $array = array($username, $password);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        return "An  error occurred checking the login type: " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
    }
// 0    SQLSTATE error code (a five characters alphanumeric identifier defined in the ANSI SQL standard).   
// 1    Driver-specific error code.
// 2    Driver-specific error message.

    if ($numrows > 0) {
        $row = $result[0];
        return array('level' => $row['level'], 'location' => $row['location']);
    } else {
        return array('level' => '', 'location' => '');
    }
}
