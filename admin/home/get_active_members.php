<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//connect to MySQL

//include( dirname(__FILE__) . '/../connect.php');
$query = "select borid, count(1) as loans, 
borwrs.member_status as status 
from transaction 
LEFT JOIN borwrs on borwrs.id = transaction.borid
where 
date_loan >= (current_date - ('12 months'::INTERVAL))
and borwrs.member_status = 'ACTIVE' 
group by borid, borwrs.member_status";
$result = pg_Exec($conn, $query);
$total = 0;

$numrows = pg_numrows($result);

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    if ($row['loans'] > 3){
        $active_members = $active_members + 1;
    }
}
$_SESSION['active'] = $active_members;
?>

