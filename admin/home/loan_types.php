<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>
    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            include( dirname(__FILE__) . '/../header_detail/header_detail.php');
            //include( dirname(__FILE__) . '/../get_settings.php');

            $borid = 758;

            $loan = count_loans($borid);
            echo $loan['status'];

            function count_loans($borid) {
                include ('../connect.php');
                $status = '';
                $count_hire = 0;
                $sql = "select t.idcat as idcat, y.reservecode as hiretoy, y.category as category,
                coalesce(l.weight, 1) as weight,
                coalesce(l.free,0) as free,
                coalesce(r.member_id,0) as roster,
                c.description as cat_desc,
                (select free from loan_restrictions where category = 'DUTY') as free_duty,
                (select weight from loan_restrictions where category = 'HIRE') as hire_weight
                from transaction t
                left join toys y on (y.idcat = t.idcat) 
                left join loan_restrictions l on (y.category = l.category)
                left join category c on (y.category = c.category)
                LEFT JOIN (SELECT member_id FROM roster WHERE date_roster = current_date LIMIT 1) r ON t.borid = r.member_id
                where borid = ? and return is null
                order by category;";
                //$sql = "select * from toys;";

                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

                $sth = $pdo->prepare($sql);
                $array = array($borid);
                $sth->execute($array);

                $result = $sth->fetchAll();
                $stherr = $sth->errorInfo();
                $numrows = $sth->rowCount();
                //echo $numrows;
                $total = 0;
                $cat_total = 0;
                for ($ri = 0; $ri < $numrows; $ri++) {
                    $row = $result[$ri];
                    if ($row['hiretoy'] == null) {
                        $weight = $row['weight'];
                    } else {
                        if ($row['hire_weight'] == null) {
                            $weight = 1;
                            //echo 'hello';
                        } else {
                            $weight = $row['hire_weight'];
                            //$weight = 0;
                            if ($weight == 0) {
                                $count_hire = $count_hire + 1;
                                if ($count_hire == 1) {
                                    $status .= 'Hire Toys are extra toys. ';
                                }
                            }
                        }
                    }
                    $category = $row['category'];

                    if ($category != $last_category) {
                        $cat_total = $weight;
                    } else {
                        $cat_total = $cat_total + $weight;
                    }
                    if ($cat_total > $row['free']) {
                        $total = $total + $weight;
                    } else {
                        if ($row['hiretoy'] == null) {
                            $status .= 'Toy from Category ' . $row['cat_desc'] . ' is an extra toy. ';
                        }
                    }


                    $last_category = $category;
                    echo $row['idcat'] . ' Total: ' . $total . '<br>';
                }
                if ($row['roster'] > 0) {
                    $total = $total - $row['free_duty'];
                    $status .= 'Extra Toy for Duty member. ';
                }
                return array("status" => $status, "total" => $total);
            }
            ?>
        </div>
    </body>
</html>

