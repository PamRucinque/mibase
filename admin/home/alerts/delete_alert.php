<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');

//$numrows = pg_numrows($result);
// confirm that the 'id' variable has been set
if (isset($_GET['alertid']) && is_numeric($_GET['alertid'])) {
    // get the 'id' variable from the URL


    $query = "DELETE FROM event WHERE id = " . $_GET['alertid'] . ";";

    $result = pg_exec($conn, $query);

    // Check result
    // This shows the actual query sent to MySQL, and the error. Useful for debugging.
    if (!$result) {
        $message = 'Invalid query: ' . "\n";
        $message = 'Whole query: ' . $query;
        //die($message);
        echo $message;
    } else {

        //echo $query;
    }
    $redirect = "Location: ../index.php";
    header($redirect);
} else {
// if the 'id' variable isn't set, redirect the user
    //header("Location: claims_info.php");
}
?>
