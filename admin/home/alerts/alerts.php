<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
//include( dirname(__FILE__) . '/../connect.php');
$table_events = '';

$query = "SELECT * from event where typeevent = 'system' ORDER by typeevent, event_date;";
//$table_events .= $query;
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);

if ($numrows > 0) {
    $table_events .= '<h2><font color="red"><span style="text-decoration: underline;">System Alerts</span></font></h2>';
    $table_events .= '<table border="1" width="90%" style="border-collapse:collapse; border-color:lightgrey;background-color: whitesmoke;">';
    $table_events .= '<tr style="border-color:lightgrey;background-color: #CCFF99;"><td>id</td><td>Date</td><td>Desc</td><td>User</td><td>Delete</td></tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);
    $complete = $row['completed'];
    
    $format_dateevent = substr($row['event_date'], 8, 2) . '-' . substr($row['event_date'], 5, 2) . '-' . substr($row['event_date'], 0, 4);


    $ref2 = 'alerts/update_alert.php?alertid=' . $row['id'] . '&alert=' . $alert_txt;
    $ref3 = 'alerts/delete_alert.php?alertid=' . $row['id'];


    $table_events .= '<tr id="red"><td align="center">' . $row['id'] . '</td>';
    $table_events .= '<td align="center">' . $format_dateevent . '</td>';
    $table_events .= '<td align="left" width="55%">' . $row['description'] . '</td>';
    $table_events .= '<td align="left" width="10%">' . $row['username'] . '</td>';
    //if ($branch == 'admin') {
        $table_events .= '<td><a class ="button_small_red" href="' . $ref3 . '">DELETE</a></td>';
        if ($complete == 'f') {
            $table_events .= '<td><a class="button_small" href="alerts/complete_alert.php?id=' . $row['id'] . '">Complete</a></td>';
        } else {
            $table_events .= '<td align="center"></td>';
        }
    //}



    $table_events .= '</tr>';
}

if ($numrows > 0) {
    //$table_events .= '<tr style="border-color:lightgrey;background-color: #CCFF99;"><td>id</td><td></td><td></td><td></td><td></td></tr>';
    $table_events .= '</table><br><br>';
}

echo $table_events;
?>

