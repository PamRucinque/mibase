<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            dateFormat: "yy-mm-dd",
            showOtherMonths: true

        };
        $("#date_alert").datepicker(pickerOpts);
    });

</script>
<table bgcolor="#CCFF99" width ="90%">
    <tr><td colspan = "3"></td></tr>
    <tr>

        <td width ="5%"></td>
        <td valign ="top">
            <h2>Add a new System Alert:</h2>
            <form  id="alert_system" method="post" action="" width="100%">
                <table><tr><td>Date</td>
                        <td><input type="text" name="date_alert" id ="date_alert" align="LEFT" size="10px" value="<?php echo date('Y-m-d'); ?>"  style="background-color:#F3F781;"></input></td>
                       <td align="right"><input id="submit_alert_system" class="button_small_yellow"  type="submit" name="submit_alert_system" value="Save System Alert" /></td>
                    
                    </tr>
                        <tr><td valign="top">Description</td>
                            <td colspan="2"><textarea id="description" name="description"  style="background-color:#F3F781;"  rows="2" cols="50"></textarea></td>
                     </tr></table>

            </form> 
            <?php echo $table_parts; ?>
        </td>
        <td width="10px"></td>

    </tr></table>

