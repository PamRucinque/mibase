<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');


if (isset($_POST['enable'])){
    $query = "UPDATE libraries set run_script = 'Yes'  where trim(subdomain) = '" . $_SESSION['library_code'] . "'";
    $result = pg_Exec($conn, $query);
    //echo $query;
}
if (isset($_POST['disable'])){
    $query = "UPDATE libraries set run_script = 'No'  where trim(subdomain) = '" . $_SESSION['library_code'] . "'";
    $result = pg_Exec($conn, $query);
    //echo $query;
}
$query = "SELECT rostertype, expired, 
            borwrs.membertype as membertype,
            borwrs.payment_status as status,
            borwrs.emailaddress as email,
            borwrs.active as active,
            borwrs.current as current,
            borwrs.custom_settings as custom_settings,
            membertype.description as mem_description,
            membertype.category as mem_category,
            files.amount as amount,
            files.due as due,
            files.filename as filename,
            files.date_invoice as date_invoice,
            libraries.run_script as run_script,
            libraries.share_toys as share_toys 
            from borwrs 
            left join files on (files.subdomain = borwrs.rostertype and status = 'Not Paid')
            Left JOIN membertype ON membertype.membertype = borwrs.membertype 
            Left JOIN libraries ON borwrs.id = libraries.id 
            WHERE trim(rostertype) = '" . $_SESSION['library_code'] . "';";
$result = pg_Exec($conn, $query);
//echo $query;

$numrows = pg_numrows($result);

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    $library_membertype = $row['membertype'];
    $library_type = $row['membertype'] . ': ' . $row['mem_description'];
    $library_expired = $row['expired'];
    $library_amount = $row['amount'];
    $library_due = $row['due'];
    $library_active = $row['active'];
    $library_current = $row['current'];
    $library_category = $row['mem_category'];
    $library_date = $row['date_invoice'];
    $library_status = $row['status'];
    $library_filename = $row['filename'];
    $library_email = $row['email'];
    $library_script = $row['run_script'];
    $library_custom_settings = $row['custom_settings'];
    $library_share_toys = $row['share_toys'];
}

$now = time(); // or your date as well
$exp = strtotime($library_expired);
$diff = ($exp - $now) / (60 * 60 * 24);
$format_expired = substr($library_expired, 8, 2) . '-' . substr($library_expired, 5, 2) . '-' . substr($library_expired, 0, 4);
$format_due = substr($library_due, 8, 2) . '-' . substr($library_due, 5, 2) . '-' . substr($library_due, 0, 4);
$format_date = substr($library_date, 8, 2) . '-' . substr($library_date, 5, 2) . '-' . substr($library_date, 0, 4);
$format_expired = substr($library_expired, 8, 2) . '-' . substr($library_expired, 5, 2) . '-' . substr($library_expired, 0, 4);

$enable ='<form method="post" action="index.php"><input id="enable" name="enable" class="button_small_green"  type="submit" value="enable" /></form>';
$disable ='<form method="post" action="index.php"><input id="disable" name="disable" class="button_small_red"  type="submit" value="disable" /></form>';

if ($library_script == 'Yes') {
    $script_str = '<table><tr><td><br><font color="green">Automatic SMS and Emails are ACTIVE</font></td><td valign="bottom"> ' . $disable . '</td></tr></table>';
} else {
    $script_str = '<table><tr><td><br><font color="red">Automatic SMS and Emails are DISABLED</font></td><td valign="bottom"> ' .$enable  . '</td></tr></table>';
}
$script_str .= '<br>' . $library_share_toys;
if ($diff > 0) {
    $format_expired_warning = '<h3><font color="darkgreen">Your Hosting Expires on: ' . $format_expired . '</font></h3>';
}
if ($diff >= 0 && $diff <= 30) {
    $format_expired_warning = '<h3 id="due_alert">Your Hosting is due to Expire on: ' . $format_expired . '</h3>';
}
if ($diff < 0) {
    $format_expired_warning = '<h3 id="expired_alert">Your Hosting Expired on: ' . $format_expired . '</h3>';
    $alert_mem_expired = 'Your Hosting has Expired!';
    $expired_flag = 'Yes';
}

//include( dirname(__FILE__) . '/../connect_toybase.php');
$query = "SELECT id as new_members  
FROM borwrs
WHERE library = '" .  $_SESSION['library_code'] . "' and spam != 'Yes'";
$result = pg_Exec($conn, $query);
$numrows = pg_numrows($result);
if ($numrows > 0){
    $str_approve = '<h4><font color="blue">' . $numrows . ' Members have joined up online and require approval. </font></h2>';
}
?>

