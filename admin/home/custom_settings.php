<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */

require(dirname(__FILE__) . '/../mibase_check_login.php');


$loanperiod = set_loan_period($_SESSION['library_code']);
$status = update_settings($loanperiod, 'loanperiod');
if ($status == 'ok') {
    $str_alert = '<h4><font color="green">Loan Period has been changed to ' . $loanperiod . ' days.</font></h4>';
}

function set_loan_period($library_code) {
    date_default_timezone_set($_SESSION['settings']['timezone']);
    if (trim($library_code) == 'stonnington') {
        $loanperiod = 21;
        $dayoftheweek = trim(date("l"));
        if ($dayoftheweek == 'Thursday') {
            $loanperiod = 23;
        }
        if ($dayoftheweek == 'Sunday') {
            $loanperiod = 21;
        }
        if ($dayoftheweek == 'Friday') {
            $loanperiod = 22;
        }
        if ($dayoftheweek == 'Tuesday') {
            $loanperiod = 25;
        }
        if ($dayoftheweek == 'Wednesday') {
            $loanperiod = 24;
        }
        if ($dayoftheweek == 'Monday') {
            $loanperiod = 26;
        }
    }
    return $loanperiod;
}

function update_settings($setting_value, $setting_name) {
    $status = 'ok';
    //include( dirname(__FILE__) . '/../connect.php');
    $sql = "update settings set setting_value = ? where setting_name = ?;";
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $sth = $pdo->prepare($sql);
    $array = array($setting_value, $setting_name);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $status = 'fail<br>';
        $status .= "An UPDATE query error occurred.\n";
        //echo $sql;
        //echo $connect_pdo;
    }
    return $status;
}
