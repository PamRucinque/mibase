<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//connect to MySQL


$conn = pg_connect($_SESSION['connect_str']);
$query = "SELECT * from homepage WHERE id = 1;";
$result = pg_Exec($conn, $query);


$numrows = pg_numrows($result);

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    $index_page = $row['frontpage'];
    $open_hours_frontpage = $row['open_hours'];
    $webpage = $row['webpage'];
    $contact_info = $row['contact_info'];
    $conditions = $row['conditions'];
    $payment_info = $row['payment_info'];
    $mem_homepage = $row['member_homepage'];
    $payment_info_join = $row['payment_info_join'];
    $castle = $row['castle'];
    $castle_link = $row['castle_link'];
    $helmet = $row['helmet_waiver'];
    $wwc = $row['wwc'];
    $sponsor = $row['sponsor_page'];
    $member_options = $row['member_options'];
}
?>

