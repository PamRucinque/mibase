<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../mibase_check_login.php');
global $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd;

$library_expired = '';
$library_script = 'No';
$library_share_toys = '';
//now get the database password for the library database

$connection_str = "port=" . $dbport . " dbname=" . $toybasedbname . " user=" . $toybasedbname . " password=" . $toybasedbpasswd;
$conn = pg_Connect($connection_str);
$query = "SELECT * from libraries 
            WHERE trim(library_code) = '" . $_SESSION['library_code'] . "';";
$result = pg_Exec($conn, $query);
//echo $connection_str;

$numrows = pg_numrows($result);
//echo $numrows;

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    $library_membertype = $row['membertype'];
    $library_expired = $row['expired'];
    $library_status = $row['status'];
    $library_script = $row['run_script'];
    $library_share_toys = $row['share_toys'];
}

$now = time(); // or your date as well
$exp = strtotime($library_expired);
$diff = ($exp - $now) / (60 * 60 * 24);
$format_expired = substr($library_expired, 8, 2) . '-' . substr($library_expired, 5, 2) . '-' . substr($library_expired, 0, 4);
$format_expired = substr($library_expired, 8, 2) . '-' . substr($library_expired, 5, 2) . '-' . substr($library_expired, 0, 4);
if ($library_script == 'Yes') {
    $script_str = '<br><font color="green">Automatic SMS and Emails are ACTIVE</font>';
} else {
    $script_str = '<br><font color="red">Automatic SMS and Emails are DISABLED</font>';
}
if ($diff > 0) {
    $format_expired_warning = '<h3><font color="darkgreen">Your Hosting Expires on: ' . $format_expired . '</font></h3>';
}
if ($diff >= 0 && $diff <= 30) {
    $format_expired_warning = '<h3 id="due_alert">Your Hosting is due to Expire on: ' . $format_expired . '</h3>';
}
if ($diff < 0) {
    $format_expired_warning = '<h3 id="expired_alert">Your Hosting Expired on: ' . $format_expired . '</h3>';
    $alert_mem_expired = 'Your Hosting has Expired!';
    $expired_flag = 'Yes';
}

//echo $format_expired;
//echo $library_share_toys;
?>

