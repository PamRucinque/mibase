<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');

//get settings
$timezone = $_SESSION['settings']['timezone'];


//include( dirname(__FILE__) . '/../connect.php');

$memberid = $_SESSION['borid'];
date_default_timezone_set($timezone);  
$tomorrow = date("Y-m-d", strtotime("+ 0 day"));
//echo 'loan period: ' . $loanperiod;
$reserve_dates = array();


$sql = "select * from contact_log where borid = 0 "
        . "and created > current_date - interval '8 days' order by created desc";

//echo $trans;
//$numrows = pg_numrows($result);
$count = 1;
$dbconn = pg_connect($_SESSION['connect_str']);
$trans = pg_exec($dbconn, $sql);
$x = pg_numrows($trans);
//echo 'number rows' . $x;
$due = strtotime($trans_due);


if ($x > 0) {
    echo '<br><h2><font color="black">Auto Emails sent in the last week.</font></h2>';
    echo '<table border="1" width="90%" style="border-collapse:collapse; border-color:grey;">';
    echo '<tr><td>id</td><td>Created</td><td>Template</td><td>subject</td></tr>';
}


$now = date('Y-m-d');

for ($ri = 0; $ri < $x; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($trans, $ri);
    //$weekday = date('l', strtotime($row['date_roster']));
    $id = $row['id'];
    $created = $row['created'];
    $template = $row['template'];
    $subject = $row['subject'];
    //$format_start = date($row['date_start'], 'Y-m-d');
    //echo $format_start . ' ' . $now . '<br>';

    echo '<tr id="red">';
    //echo '<td>' . $reserve_id . '</td>';
    //echo '<td>' . $format_start . '</td>';
    echo '<td>' . $id . '</td>';
    echo '<td>' . $created . '</td>';
    echo '<td>' . $template . '</td>';
    echo '<td>' . $subject . '</td>';
    echo '</tr>';


    //<a class="button_menu" href="../../toys/update/toy_detail.php">Toy</a>
}
if ($x > 0) {
    echo '</table>';
}

//echo '<br>' . $start . ' Reserve Dates: ' . implode('|',$reserve_dates) . '<br>';
//pg_close($link);
?>


