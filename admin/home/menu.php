<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>
    <body>
        <div class="flex-container">
            <?php include( dirname(__FILE__) . '/../menu.php'); ?>
            <section class="container-fluid" style="padding: 10px;">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Home Menu</h2>
                    </div>
                    <div class="col-sm-5">
                        <br><a href='index.php' class ='btn btn-colour-yellow'>Home</a> 
                        <a href='../toys/index.php' class ='btn btn-primary'>Toys</a> 
                        <a href='../member/index.php' class ='btn btn-colour-maroon'>Members</a> 
                        <a href='../select_boxes/index.php' class ='btn btn-success'>Select Boxes</a>
                    </div>
                    <div class="col-sm-1">
                        <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=toysa' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <h4>Settings</h4>
                        <br><a style="width: 150px;" href='../settings/settings.php' class ='btn btn-colour-yellow'>Global Settings</a><br>
                        <br><a style="width: 150px;" href='/home/auto/auto.php' class ='btn btn-colour-yellow'>My Setup</a><br>
                        <br><a style="width: 150px;" href='../home/edit_home.php' class ='btn btn-colour-yellow'>Website</a><br>
                        <br><a style="width: 150px;" href='../template/template.php' class ='btn btn-colour-yellow'>Templates</a><br>
                        <br><a style="width: 150px;" href='../home/files/files.php' class ='btn btn-colour-yellow'>Upload Files</a><br>

                    </div>
                    <div class="col-sm-3">
                        <h4>Stats</h4>
                        <br><a style="width: 150px;" href='../home/charts/charts.php' class ='btn btn-colour-yellow'>Session Stats</a><br>
                        <br><a style="width: 150px;" href='../home/active_chart/chart.php' class ='btn btn-colour-yellow'>Active Members</a><br>
                    </div>

                    <div class="col-sm-3">
                        <h4>Update Data</h4>
                        <br><a style="width: 150px;" href='../holiday/index.php' class ='btn btn-colour-yellow'>Holiday Due Dates</a><br>


                    </div>
                    <div class="col-sm-3">
                        <h4></h4>
                    </div>
                </div>
           </div>

    </body>
    <?php include('../footer.php'); ?>
</html>