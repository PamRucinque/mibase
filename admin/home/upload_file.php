<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

$output = ' ';
$filename = basename($_FILES['file']['name']);
if ($_FILES["file"]["size"] < 5000000)
  {
  if ($_FILES["file"]["error"] > 0)
    {
    $output = $output . "Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
  else
    {
    $output = $output . 'Results from File Upload: <br >';
    $output = $output . ' ';
    $output = $output . "Uploaded File: " . $_FILES["file"]["name"] . "<br />";
    $output = $output . "Type: " . $_FILES["file"]["type"] . "<br />";
    $output = $output . "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
    //echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";
    echo '';
    $thisdir = getcwd();
   
    //$newdir = $thisdir . "/files/" . $_POST['dir'];
    //$newdir = '/home/marineleisure/html/mla_files/' . $hm_claimno . "/";
    //$newdir = "/home/holmesma/public_html/share" . "/files/" . $hm_claimno . "/";
    //echo $newdir;
    //strtolower($str)
   
        if (file_exists($target . $_FILES["file"]["name"]))
      {
      
      move_uploaded_file($_FILES["file"]["tmp_name"],
      $newdir . $_FILES["file"]["name"]);
      $output = $output . "File has been overwritten Stored in: " . $target . $_FILES["file"]["name"];
      $output = $output . "The File " . $_FILES["file"]["name"] . " already exists. ";
      //include( dirname(__FILE__) . '/db.php');
      //$query = "UPDATE specification SET sp_pdf='{$_FILES["file"]["name"]}' WHERE prod_id={$_GET['pID']}";
      //$r = mysql_query($query);

      }
    else
      {
      move_uploaded_file($_FILES["file"]["tmp_name"],
      $target . $_FILES["file"]["name"]);
      $output = $output . "Stored in: " . $target . $_FILES["file"]["name"];
      //include( dirname(__FILE__) . '/db.php');
      //$query = "UPDATE specification SET sp_pdf='{$_FILES["file"]["name"]}' WHERE prod_id={$_GET['pID']}";
      //$r = mysql_query($query);
      //print $r;
      }
    }
  }
else
  {
  $output = $output . "Invalid file";
  }

?>