<?php

include( dirname(__FILE__) . '/functions.php');
$string_r = '';
$array = array(
    array('Time', 'LOANS', 'RETURNS'),
);
$array_last = array(
    array('Time', 'LOANS', 'RETURNS'),
);
$array_year = array(
    array('Month', 'Members'),
);
$array_reserve = array(
    array('Month', 'Reservations'),
);
array_push($array, array('0', 0, 0));
array_push($array_last, array('0', 0, 0));
//array_push($array_year, array('0',0));
$max = 0;
$min = 10000;
$max_r = 0;


//$array = array("Time", "Loans", "Returns");


//include( dirname(__FILE__) . '/../../connect.php');


$query = "select l.time_int, loans, r.returns from
(SELECT count(id) as loans,   date_loan as date_ch,
round((CASE WHEN (date_part('minute',date_round(created, '15 minutes'))=0)
THEN extract('hour' from created)+1
ELSE extract('hour' from created) +(date_part('minute',date_round(created, '15 minutes'))/100) END)::numeric,2) as time_int
FROM transaction
WHERE date_loan  = '" . $start . "'
group by
 date_loan, time_int
order by  time_int) l
LEFT OUTER JOIN
(SELECT count(id) as returns,
round((CASE WHEN (date_part('minute',date_round((return  + timetrans), '15 minutes'))=0)
THEN extract('hour' from timetrans)+1
ELSE extract('hour' from timetrans) +(date_part('minute',date_round(created, '15 minutes'))/100) END)::numeric,2) as time_int
FROM transaction
WHERE return =  '" . $start . "'
group by
time_int
order by  time_int) r
on r.time_int = l.time_int;";


$conn = pg_connect($_SESSION['connect_str']);
$result = pg_Exec($conn, $query);
$numrows = pg_numrows($result);
for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    if ($row['loans'] == null) {
        $row['loans'] = 0;
    }
    if ($row['returns'] == null) {
        $row['returns'] = 0;
    }
    if (trim($_SESSION['timezone']) == 'Pacific/Auckland') {
        $time_int = get_hour($row['time_int']);
    } else {
        $time_int = $row['time_int'];
    }


    array_push($array, array($time_int, $row['loans'], $row['returns']));
    //$string_r .= "array('" . $row['time_int'] . "'," . $row['loans'] . "," . $row['returns'] . "),";
}
$query_last = "select l.time_int, (loans/4) as loans, (r.returns/4) as returns  
from
(SELECT count(id) as loans, date_part('isodow',date_loan) as weekday,
round((CASE WHEN (date_part('minute',date_round(created, '15 minutes'))=0)
THEN extract('hour' from created)+1
ELSE extract('hour' from created) +(date_part('minute',date_round(created, '15 minutes'))/100) END)::numeric,2) as time_int
FROM transaction
WHERE date_loan  <= current_date 
and date_loan >= current_date - interval '31 days' 
and date_part('dow',date_loan) = " . $dow . "
group by
 weekday, time_int
order by  time_int) l
LEFT OUTER JOIN
(SELECT count(id) as returns,
round((CASE WHEN (date_part('minute',date_round((return  + timetrans), '15 minutes'))=0)
THEN extract('hour' from timetrans)+1
ELSE extract('hour' from timetrans) +(date_part('minute',date_round(created, '15 minutes'))/100) END)::numeric,2) as time_int
FROM transaction
WHERE return  <= current_date 
and return >= current_date - interval '31 days' 
and date_part('dow',return) = " . $dow . "
group by
time_int
order by  time_int) r
on r.time_int = l.time_int;";

$result_last = pg_Exec($conn, $query_last);
$numrows_last = pg_numrows($result_last);
for ($ri = 0; $ri < $numrows_last; $ri++) {
    $row = pg_fetch_array($result_last, $ri);
    if ($row['loans'] == null) {
        $row['loans'] = 0;
    }
    if ($row['returns'] == null) {
        $row['returns'] = 0;
    }
    if (trim($_SESSION['timezone']) == 'Pacific/Auckland') {
        $time_int = get_hour($row['time_int']);
    } else {
        $time_int = $row['time_int'];
    }


    array_push($array_last, array($time_int, $row['loans'], $row['returns']));
    //$string_r .= "array('" . $row['time_int'] . "'," . $row['loans'] . "," . $row['returns'] . "),";
}
//echo $query;

$query_year = "select count(distinct borid) as members , date_part('month', created) as month, date_part('year' ,created) as year,
 date_part('year', created) || '-' || to_char(created,'Mon') as month_str
 from transaction 
where created > current_date - interval '14 months' 
group by year, month , month_str
order by year, month";

$result_year = pg_Exec($conn, $query_year);
$numrows_year = pg_numrows($result_year);
for ($ri = 0; $ri < $numrows_year; $ri++) {
    $row = pg_fetch_array($result_year, $ri);
    if ($row['members'] == null) {
        $row['members'] = 0;
    }
    $month_str = $row['month_str'];
    $members = $row['members'];
    if ($max < $row['members']) {
        $max = $row['members'];
    }
    if ($min > $row['members']) {
        $min = $row['members'];
    }
    //echo $min . '<br>';

    array_push($array_year, array($month_str, (int) $members));

    //echo $row['month'] . ' : ' . date('m');
    //$string_r .= "array('" . $row['time_int'] . "'," . $row['loans'] . "," . $row['returns'] . "),";
}
$min = $min - 0.05 * $min;

$query_reserve = "select count(id) as reserves , date_part('month', date_start) as month, date_part('year' ,date_start) as year,
 date_part('year', date_start) || '-' || to_char(date_start,'Mon') as month_str
 from reserve_toy
where date_start > current_date - interval '14 months' 
group by year, month , month_str
order by year, month";
$result_reserve = pg_Exec($conn, $query_reserve);
$numrows_reserve = pg_numrows($result_reserve);
for ($ri = 0; $ri < $numrows_reserve; $ri++) {
    $row = pg_fetch_array($result_reserve, $ri);
    if ($row['reserves'] == null) {
        $row['reserves'] = 0;
    }
    $month_str = $row['month_str'];
    $reserves = $row['reserves'];
    if ($max_r < $row['reserves']) {
        $max_r = $row['reserves'];
    }

    //echo $min . '<br>';

    array_push($array_reserve, array($month_str, (int) $reserves));

    //echo $row['month'] . ' : ' . date('m');
    //$string_r .= "array('" . $row['time_int'] . "'," . $row['loans'] . "," . $row['returns'] . "),";
}

