<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');
//date_default_timezone_set($_SESSION['timezone']);
if (isset($_POST['start'])) {
    $start = $_POST['start'];
    $start_str = date('d M Y', strtotime($start));
} else {

    $start = date('Y-m-d');
    $start_str = date('d M Y');
}
$dayofweek = date('l', strtotime($start));
$dow = date('w', strtotime($start));

include( dirname(__FILE__) . '/data.php');
//include( dirname(__FILE__) . '/../../get_settings.php');
$heading = '<font color="red"><h2>' . $dayofweek . ', ' . $start_str . '</font></h2>';
$heading_month = '<font color="red"><h2>Average for ' . $dayofweek . '\'s over the last month. </font></h2>';
$heading_year = '<font color="red"><h2>Number of active Members by Month. </font></h2>';
//echo sizeof($array);
?>
<html>
    <head>

        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="../../js/jquery-1.9.0.js"></script>
        <script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
        <link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />
    </head>
    <body>
        <script type="text/javascript">
            google.charts.load('current', {'packages': ['bar']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var data = google.visualization.arrayToDataTable(<?php echo json_encode($array); ?>);

                var options = {
                    chart: {
                        title: 'Session Activity Chart',
                        subtitle: 'Single Day',
                    }
                };

                var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
                chart.draw(data, google.charts.Bar.convertOptions(options));
                var data = google.visualization.arrayToDataTable(<?php echo json_encode($array_last); ?>);

                var options = {
                    chart: {
                        title: 'Session Activity Chart',
                        subtitle: 'Average over last month',
                    }
                };

                var chart = new google.charts.Bar(document.getElementById('columnchart_material2'));

                chart.draw(data, google.charts.Bar.convertOptions(options));

                var data = google.visualization.arrayToDataTable(<?php echo json_encode($array_year); ?>);

                var options = {
                    chart: {
                        title: 'Active Members by Month.',
                        subtitle: '',
                    },
                    vAxis: {
                        viewWindowMode: 'explicit',
                        viewWindow: {
                            max:<?php echo $max; ?>,
                            min:<?php echo $min; ?>
                        }
                    }
                };
                var chart = new google.charts.Bar(document.getElementById('columnchart_material3'));

                chart.draw(data, google.charts.Bar.convertOptions(options));
                var data = google.visualization.arrayToDataTable(<?php echo json_encode($array_reserve); ?>);

                var options = {
                    chart: {
                        title: 'Number of Reservations per Month.',
                        subtitle: '',
                    },
                    vAxis: {
                        viewWindowMode: 'explicit',
                        viewWindow: {
                            max:<?php echo $max_r; ?>,
                            min: 0
                        }
                    }
                };
                var chart = new google.charts.Bar(document.getElementById('columnchart_material4'));

                chart.draw(data, google.charts.Bar.convertOptions(options));
            }

        </script>
        <script type="text/javascript">
            $(function () {
                var pickerOpts = {
                    dateFormat: "dd MM yy",
                    showOtherMonths: true,
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "1990:+nn"

                };
                $("#start").datepicker(pickerOpts);
            });
        </script>

        <div id="form_container">
<?php include( dirname(__FILE__) . '/../../menu.php'); ?>
            <table width = "100%"><tr><td width='80%'>
            <?php
            if (sizeof($array) > 2) {
                echo $heading;
            } else {
                echo 'No Data for this day.<br>';
                echo $_SESSION['settings']['timezone'];
            }
            ?>
                    </td>
                    <td>
                        <form name="filter" id="filter" method="post" action="charts.php" >    
                            Change Date:<br> <input type="text" style="width: 120px;background-color:#F3F781;" name="start" id ="start" align="LEFT" value="<?php echo $start; ?>"  onchange="this.form.submit()"></input></td>
                    </form>
                    </td></tr>
            </table>
<?php
if (sizeof($array) > 2) {
    echo '<div id="columnchart_material" style="width: 800px; height: 500px; padding-left: 10px;"></div>';
}
//echo $numrows;
?>

            <table width = "100%"><tr><td width='80%'>
<?php
if (sizeof($array) > 2) {
    echo $heading_month;
} else {
    // echo 'No Data for this day.';
}
?>
                    </td>
                    <td>

                    </td></tr>
            </table>
            <div id="columnchart_material2" style="width: 800px; height: 500px; padding-left: 10px;"></div>
            <table width = "100%"><tr><td width='80%'>
<?php
if (sizeof($array) > 2) {
    echo $heading_year;
} else {
    // echo 'No Data for this day.';
}
?>
                    </td>
                    <td>

                    </td></tr>
            </table>
            <div id="columnchart_material3" style="width: 800px; height: 500px; padding-left: 10px;"></div>
            <?php 
            if ($reservations == 'Yes'){
                echo '<div id="columnchart_material4" style="width: 800px; height: 500px; padding-left: 10px;"></div>';
            }
            ?>
            
        </div>
    </body>

</html>
