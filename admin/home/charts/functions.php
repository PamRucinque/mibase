<?php

function get_hour($time_int, $timezone) {

    switch ($time_int) {
        case '7.00':
            $out = '9.00';
            break;
        case '7.15':
            $out = '9.15';
            break;
        case '7.30':
            $out = '09.30';
            break;
        case '7.45':
            $out = '9.45';
            break;
        case '8.00':
            $out = '10.00';
            break;
        case '8.15':
            $out = '10.15';
            break;
        case '8.30':
            $out = '10.30';
            break;
        case '8.45':
            $out = '10.45';
            break;
        case '9.00':
            $out = '11.00';
            break;
        case '9.15':
            $out = '11.15';
            break;
        case '9.30':
            $out = '11.30';
            break;
        case '9.45':
            $out = '11.45';
            break;
        case '10.00':
            $out = '12.00';
            break;
        case '10.15':
            $out = '12.15';
            break;
        case '10.30':
            $out = '12.30';
            break;
        case '10.45':
            $out = '12.45';
            break;
        case '11.00':
            $out = '13.00';
            break;
        case '11.15':
            $out = '13.15';
            break;
        case '11.30':
            $out = '13.30';
            break;
        case '11.45':
            $out = '13.45';
            break;

        default:
            $out = $time_int;
    }

    return $out;
}
