<script>
    function specify()
    {
        alert('If you have played for another club or School, please specify in the next box.');
    }
    function specify_sponsor()
    {
        alert('If you have selected Sponsorship, please specify details in the next box.');
    }


</script>
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="new.php" onsubmit="parent.scrollTo(0, 0); return true">
    <font size="4" face="Arial, Helvetica, sans-serif">
    <div id="form" style="background-color:lightgray;">
        <?php //include( dirname(__FILE__) . '/get_library.php');  ?>

        <table style="background-color:lightgray;"><tr><td>
                    <h2>Player Details:</h2>

                    <table>
                        <tr><td width="35%">First Name:<br>
                                <input type="Text" name="firstname" align="LEFT" required="Yes" size="35" maxlength="35" value=""></input></td>
                            <td width="35%">Surname:<br>
                                <input type="Text" name="surname" align="LEFT" required="Yes" size="35" maxlength="35" value=""></input><br></td>

                            <td>Mobile:<br>
                                <input type="Text" name="mobile1" align="LEFT" required="Yes" size="20" maxlength="20" value=""></input><br></td>
                        </tr>
                        <tr>
                            <td>Date of Birth:<br>
                                <input type="Text" name="dob" id="dob" align="LEFT" required="Yes" size="20" value=""></input><br></td>
                            <td>Age Group:<br>
                                <?php include( dirname(__FILE__) . '/get_memtype.php'); ?>
                            <td><br>VNA Number:<br>
                                <input type="Text" name="vna" align="LEFT" size="20" value="" maxlength="20"></input><br><br></td></tr>
                    </table>
                    <table>
                        <tr>
                            <td colspan="3"><br><strong>Where did you play last year?</strong> <br>  <input type="checkbox" name="menc"> Mt Eliza Netball Club
                                <input type="checkbox" name="another" value="" onchange="specify();"> Another Club
                                <input type="checkbox" name="school" value=""  onchange="specify();"> School
                                <input type="checkbox" name="new" value=""> New<br><br></td></tr>

                        <tr>

                            <td colspan="2"><strong>Please Specify if you played for another club or School:</strong><br>
                                <input type="Text" name="other" id="other" align="LEFT" size="60" maxlength="100" value=""></input><br><br></td>
                        </tr>
                        <tr>

                            <td colspan="3"><br><strong>Preferred Positions:</strong> <br>  
                                <input type="checkbox" name="defence"> Defence
                                <input type="checkbox" name="mid" value=""> Mid Court
                                <input type="checkbox" name="goals" value=""> Goals
                                <input type="checkbox" name="never" value=""> Never Played<br><br></td>
                        </tr>
                        <tr>

                            <td colspan="3"><br><strong>I am interested in assisting the club with the following:</strong> <br>  
                                <input type="checkbox" name="coaching"> Coaching (min 14 yrs of age)
                                <input type="checkbox" name="umpire" value=""> Umpire (min 12 years of age - paid)
                                <input type="checkbox" name="team" value=""> Team Manager
                                <input type="checkbox" name="sponsor" value=""  onchange="specify_sponsor();"> Sponsorship<br><br></td>
                        </tr>
                        <tr>  <td colspan="2"><strong>Please Specify Sponsorship Details:</strong><br>
                                <input type="Text" name="sponsor_detail" id="sponsor_detail" align="LEFT" size="60" value=""></input><br><br></td>


                        </tr></table>

                    <table> 
                        <tr><td><label>Please detail any medical conditions and/or Food Allergies: </label><br>
                                <textarea id="medical" name="medical" rows="2" cols="80"></textarea></td></tr>
                        <tr><td><label>Comments and/or Netball playing experience: </label><br>
                                <textarea id="comments" name="comments" rows="2" cols="80"></textarea></td></tr>

                    </table>
                    <h2>Emergency Contact Details:</h2>
                    <table><tr><td width="35%">Mother/Guardian (Full Name): <br>
                                <input type="Text" name="name1" align="LEFT" required="Yes" size="35" maxlength="35" value=""></input></td>

                            <td width="25%">Mobile:<br>
                                <input type="Text" name="phone2" align="LEFT" required="Yes" size="20" value="" maxlength="20"></input><br></td></tr>
                        <tr><td width="35%">Father/Guardian (Full Name): <br>
                                <input type="Text" name="name2" align="LEFT" required="Yes" size="35" value="" maxlength="30"></input><br></td>
                            <td width="25%">Mobile:<br>
                                <input type="Text" name="mobile2" align="LEFT" required="Yes" size="20" value="" maxlength="20"></input><br></td></tr>

                    </table>
                    <h2>Contact Details:</h2>
                    <table>
                        <tr><td width="45%">Address:<br>
                                <input type="Text" name="address" align="LEFT" required="Yes" size="35" value="" maxlength="35"></input><br>
                                <input type="Text" name="address2" align="LEFT" size="35" value=""></input></td>
                            <td width="40%" align='top'>Suburb:<br>
                                <input type="Text" name="suburb" align="LEFT" required="Yes" size="35" value="" maxlength="25"></input><br><br></td>
                            <td  align='top'>Postcode:<br>
                                <input type="Text" name="postcode" align="LEFT" required="Yes" size="4" value="" maxlength="5"></input><br><br></td></tr>

                        <tr><td width="35%">Email:<br>
                                <input type="Text" name="emailaddress" align="LEFT" required="Yes" size="35" value=""></input></td>
                            <td width="35%">Home Phone:<br>
                                <input type="Text" name="phone" align="LEFT" required="Yes" size="35" value="" maxlength="20"></input><br></td></tr>

                        <tr>
                            <td valign='top'><br> <h3><input type="checkbox" id="photo" name="photo" value="Yes"> I give permission for my child's photo/video image to be
                                    used for publication on the Club website and or facebook page.</h3></input><br>
                            </td>
                            <td valign='top'><br>
                                <h3><input type="checkbox" id="wwc" name="wwc" value="Yes"> Yes, I have a current working 
                                    with children's card. </h3></input><br>
                            </td>

                        </tr>

                    </table>
                </td></tr>
        </table>
        <table  width="95%">

            <tr><td align="right"><br><input id="saveForm" class="button1_yellow" type="submit" name="submit" value="Submit Registration" onsubmit="aftersubmit();" style ="color:black;"/>
                </td></tr>
        </table>



    </div>
</form>
