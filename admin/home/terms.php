<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>
    <?php
    if (isset($_POST['submit_alert_system'])) {
        include( dirname(__FILE__) . '/alerts/new_alert.php');
    }

    $total = 0;
    $libraryname = $_SESSION['settings']['libraryname'];
    
    $logo = $_SESSION['logo'];
    include( dirname(__FILE__) . '/get_index_info.php');
    //$url_news = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . '/php/upload_news.php';
    ?>
    <body>
        <div id="form_container">

            <?php
            include( dirname(__FILE__) . '/../menu.php');
            include( dirname(__FILE__) . '/../header_detail/header_detail.php');
            ?>
            <div id="terms" width="80%" style="padding-left: 20px; padding-right: 20px; padding-top: 20px">
                <?php
                if (isset($_POST['agree'])) {
                    $query = "UPDATE libraries set agree = 'Yes' where subdomain = '" . $_SESSION['library_code'] . "';";
                    $result = pg_Exec($conn, $query);
                }
                include( dirname(__FILE__) . '/get_terms.php');

                if ($agree == 'Yes') {
                    //echo '<div id="jump_to_me"></div>';
                    echo '<h3><font color="red">You have agreed to the Mibase Live Terms and Conditions - see below</font></h3>';
                    echo $conditions;
                    //include ('new_membid.php');
                    //include( dirname(__FILE__) . '/new_form.php');
                    //$_SESSION['show_conditions'] = "No";
                } else {
                    echo '<h3><font color="red">Please Read and agree to the Mibase Live Terms and Conditions!</font></h3><br>';
                    include( dirname(__FILE__) . '/agree_form.php');
                }
                ?>
            </div>
        </div>
    </body>

</html>

