<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

$timezone = $_SESSION['settings']['timezone'];


//include( dirname(__FILE__) . '/../connect.php');
$today_str = '<font color="blue">' . date('l jS \of F Y') . '</font>';
if (isset($_SESSION['borid'])) {
    $memberid = $_SESSION['borid'];
}
$trans_due = '';

date_default_timezone_set($timezone);  
$tomorrow = date("Y-m-d", strtotime("+ 0 day"));
//echo 'loan period: ' . $loanperiod;
$reserve_dates = array();


$sql = "SELECT roster.*,
borwrs.id || ': ' || borwrs.firstname || ' ' || borwrs.surname as borname, roster.location as loc_des,
roster.date_roster as date_roster
from roster
left join borwrs on borwrs.id = roster.member_id
 where  borwrs.member_status = 'ACTIVE'  and date_roster = current_date order by date_roster, roster_session, session_role;";

//echo $trans;
//$numrows = pg_numrows($result);
$count = 1;
$conn = pg_connect($_SESSION['connect_str']);
$trans = pg_exec($conn, $sql);
$x = pg_numrows($trans);
//echo 'number rows' . $x;
$due = strtotime($trans_due);


if ($x > 0) {
    echo '<br><h2><font color="black">Members on Duty Today:  </font>' . $today_str . '</h2>';
    echo '<table border="1" width="90%" style="border-collapse:collapse; border-color:grey;">';
    //echo '<tr><td>Member</td><td>Location</td><td>Member</td></tr>';
}


$now = date('Y-m-d');

for ($ri = 0; $ri < $x; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($trans, $ri);
    //$weekday = date('l', strtotime($row['date_roster']));
    $date_roster = $row['date_roster'];
    $location = $row['loc_des'];

    $borname = $row['borname'];

    echo '<tr id="red">';
    //echo '<td>' . $reserve_id . '</td>';
    //echo '<td>' . $format_start . '</td>';
    echo '<td>' . $borname . '</td>';
    if ($location != '') {
        echo '<td>' . $location . '</td>';
    }

    echo '<td>' . $row['session_role'] . '</td>';
    echo '<td>' . $row['roster_session'] . '</td>';

    echo '</tr>';
    if ($row['comments'] != '') {
        if ($location == '') {
            echo '<tr><td colspan="4"><font color="blue">*** ' . $row['comments'] . '</font></td></tr>';
        } else {
            echo '<tr><td colspan="5"><font color="blue">*** ' . $row['comments'] . '</font></td></tr>';
        }
    }


    //<a class="button_menu" href="../../toys/update/toy_detail.php">Toy</a>
}
if ($x > 0) {
    echo '</table>';
}

//echo '<br>' . $start . ' Reserve Dates: ' . implode('|',$reserve_dates) . '<br>';
//pg_close($link);
?>


