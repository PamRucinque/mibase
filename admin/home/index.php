<?php
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php
        include( dirname(__FILE__) . '/../header.php');
        ?> 
        <script>
            function overlay() {
                if ($("#overlay").is(":visible")) {
                    $("#overlay").hide();
                    document.getElementById("scanid").focus();
                }
            }
            function blinker() {
                $('.blink_me').fadeOut(500);
                $('.blink_me').fadeIn(500);
            }

            setInterval(blinker, 1000);
        </script>

    </head>

    <?php
    if (isset($_POST['submit_alert_system'])) {
        include( dirname(__FILE__) . '/alerts/new_alert.php');
    }
    $total = 0;
    $str_approve = '';
    $system_alert = '';
    // $url_news = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . '/php/upload_news.php';
    ?>
    <body>
        <div class="container-fluid">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            include( dirname(__FILE__) . '/../header_detail/header_detail.php');


            //include( dirname(__FILE__) . '/get_library.php');
            //include( dirname(__FILE__) . '/share_toys.php');

            $str_alert = '';
            ?>
            <table>
                <tr>
                    <td width ="60%" align="center" valign="top">
                        <?php
                        if ($_SESSION['shared_server']) {
                            if (file_exists($_SESSION['web_root_folder'] . '/' . $_SESSION['logos_location'] . '/' . $_SESSION['library_code'] . '/logo.jpg')) {
                                echo '<img src="' . $_SESSION['logos_location'] . '/' . $_SESSION['library_code'] . '/logo.jpg' . '" alt="Library Logo" class="profile-img-card" height="200px">';
                            } else {
                                echo '<img src="../logo.jpg" alt="Library Logo" class="profile-img-card" height="200px">';
                            }
                        } else {
                            if (file_exists($_SESSION['web_root_folder'] . '/' . $_SESSION['logos_location'] . '/logo.jpg')) {
                                echo '<img src="' . $_SESSION['logos_location'] . '/logo.jpg" alt="Library Logo" class="profile-img-card" height="200px">';
                            } else {
                                echo '<img src="../logo.jpg" alt="Library Logo" class="profile-img-card" height="200px">';
                            }
                        }
                        include( dirname(__FILE__) . '/rosters.php');
                        //include( dirname(__FILE__) . '/reserves.php');
                        include( dirname(__FILE__) . '/holds.php');
                        if ($_SESSION['settings']['admin_renewal_list'] == 'Yes') {
                            include( dirname(__FILE__) . '/renewals.php');
                        }
                        // echo $admin_renewal_list;
                        echo $str_approve;
                        //include( dirname(__FILE__) . '/auto_emails.php');
                        ?>
                    </td>
                    <td width= "30%" align="left" valign="top" style="padding-right: 10px;">  
                        <br>
                        <table>
                            <tr><td><?php include( dirname(__FILE__) . '/files.php'); ?></td>
                            </tr></table>
                    </td>
                </tr>
            </table>
            <table><tr>
<?php
if ($system_alert == 'Yes') {
    include( dirname(__FILE__) . '/alerts/alerts.php');
    include( dirname(__FILE__) . '/alerts/alert_form.php');
}
?>
                </tr></table>
        </div>
    </body>
</html>

