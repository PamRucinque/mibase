<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//get settings
$timezone = $_SESSION['settings']['timezone'];


//include( dirname(__FILE__) . '/../connect.php');
$today_str = '<font color="blue">' . date('l jS \of F Y') . '</font>';
if (isset($_SESSION['borid'])) {
    $memberid = $_SESSION['borid'];
}
$trans_due = '';

date_default_timezone_set($timezone);  
$tomorrow = date("Y-m-d", strtotime("+ 0 day"));
//echo 'loan period: ' . $loanperiod;
$reserve_dates = array();


$sql = "select date_start, date_end , borid,
(borwrs.firstname || ' ' || borwrs.surname) as membername, string_agg(idcat, ', ') AS toy_list 
from toy_holds 
left join borwrs on borwrs.id = toy_holds.borid
where status = 'ACTIVE' and (date_end >= current_date or date_start >= current_date)
group by borid, date_start, date_end, membername
order by date_start";

//echo $trans;
//$numrows = pg_numrows($result);
$count = 1;
$dbconn = pg_connect($_SESSION['connect_str']);
$trans = pg_exec($dbconn, $sql);
$x = pg_numrows($trans);
//echo 'number rows' . $x;
$due = strtotime($trans_due);


if ($x > 0) {
    echo '<br><h2><font color="black">Members with Toy Holds:  </font></h2>';
    echo '<table border="1" width="90%" style="border-collapse:collapse; border-color:grey;">';
    //echo '<tr><td>Member</td><td>Location</td><td>Member</td></tr>';
}


$now = date('Y-m-d');

for ($ri = 0; $ri < $x; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($trans, $ri);
   $date_start = substr($row['date_start'], 8, 2) . '-' . substr($row['date_start'], 5, 2) . '-' . substr($row['date_start'], 0, 4);
   $date_end = substr($row['date_end'], 8, 2) . '-' . substr($row['date_end'], 5, 2) . '-' . substr($row['date_end'], 0, 4);


    $borname = $row['membername'];

    echo '<tr id="red">';
    echo '<td>' . $date_start . '</td>';
    echo '<td>' . $date_end . '</td>';
    echo '<td>' . $row['toy_list'] . '</td>';
    //echo '<td>' . $row['borid'] . '</td>';
     echo '<td>' . $borname . '</td>';
    echo '<td><a class="button_small_red" href="../members/update/member_detail.php?id=' .$row['borid'] . '">' . $row['borid'] .'</a></td>';
   

    echo '</tr>';

    //<a class="button_menu" href="../../toys/update/toy_detail.php">Toy</a>
}
if ($x > 0) {
    echo '</table>';
}

//echo '<br>' . $start . ' Reserve Dates: ' . implode('|',$reserve_dates) . '<br>';
//pg_close($link);
?>


