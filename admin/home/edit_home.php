<!doctype html>
<html lang="en">
    <head>
        <?php
        /*
         * Copyright (C) 2018 Michelle Baird
         *
         * This program is free software: you can redistribute it and/or modify
         * it under the terms of the GNU General Public License as published by
         * the Free Software Foundation, either version 3 of the License, or
         * (at your option) any later version.
         *
         * This program is distributed in the hope that it will be useful,
         * but WITHOUT ANY WARRANTY; without even the implied warranty of
         * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
         * GNU General Public License for more details.
         *
         * You should have received a copy of the GNU General Public License
         * along with this program.  If not, see <http://www.gnu.org/licenses/>.
         */

        /*
         * This page should be included in the top of all pages.
         * so if a user is not authenticated they are redirected back to the login page
         */
        ?>   

    </head>
    <body id="main_body" >
        <div id="form_container">
            <?php
            require( dirname(__FILE__) . '/../mibase_check_login.php');
            $conn = pg_connect($_SESSION['connect_str']);
            
            
            
            include( dirname(__FILE__) . '/../header.php');

            include( dirname(__FILE__) . '/../menu.php');

            if (isset($_POST['submit'])) {
                //include( dirname(__FILE__) . '/../connect.php');

                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $query_edit = "UPDATE homepage SET 
                    frontpage = ?,
                    open_hours = ?,
                    webpage = ?,
                    conditions = ?,
                    payment_info = ?,
                    payment_info_join = ?,
                    castle = ?,
                    castle_link = ?,
                    helmet_waiver = ?,
                    member_homepage = ?,
                    member_options = ?,
                    sponsor_page = ?,
                    wwc = ?,
                    lastupdated = NOW() 
                    WHERE id=1;";
                $sth = $pdo->prepare($query_edit);
                $array = array($_POST['frontpage'], $_POST['open_hours'], $_POST['webpage'], $_POST['conditions'], $_POST['payment_info'], $_POST['payment_info_join'], $_POST['castle'], $_POST['castle_link'], $_POST['helmet'], $_POST['mem_homepage'],$_POST['member_options'], $_POST['sponsor'], $_POST['wwc']);
                $sth->execute($array);
                $stherr = $sth->errorInfo();

                if ($stherr[0] != '00000') {
                    echo "An INSERT query error occurred.\n";
                    echo 'Error ' . $stherr[0] . '<br>';
                    echo 'Error ' . $stherr[1] . '<br>';
                    echo 'Error ' . $stherr[2] . '<br>';
                    echo $query_edit;
                    echo $connect_pdo;
                }
                echo 'The Website information has been saved! <br>';
                $link = '<a class="button_small" href="index.php">Back to Home Page</a><br>';
                echo $link;
                $redirect = "Location: index.php";
                //header($redirect);
            } else {
                include( dirname(__FILE__) . '/get_index_info.php');
                include( dirname(__FILE__) . '/edit_home_form.php');
            }
            ?>
        </div>
    </body>
</html>
