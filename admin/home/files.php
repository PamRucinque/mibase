<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');

$result_txt = '';

//get settings


$sql = "select * from files order by type_file , lastupdate desc;";
$dbconn = pg_connect($_SESSION['connect_str']);
$result_list = pg_exec($dbconn, $sql);
$numrows = pg_numrows($result_list);
if ($numrows > 0) {
    $result_txt = '<h2>Uploaded Files</h2>';
}

    $result_txt .= '<table border="1" width="100%">';
for ($ri = 0; $ri < $numrows; $ri++) {
    $filerow = pg_fetch_array($result_list, $ri);
    $filerow['filename'] = trim($filerow['filename']);

    $result_txt .= '<tr>';
    $result_txt .= '<td width = "70%">' . $filerow['description'] . '</td>';
    $ref2 = 'delete.php?id=' . $filerow['filename'];

    if ($_SESSION['shared_server']) {
        $file_url =  $_SESSION['news_location'] . '/' . $_SESSION['library_code'] . '/' . $filerow['filename'];
    } else {
        $file_url =  $_SESSION['news_location'] . '/' . $filerow['filename'];
    }
    $result_txt .= '<td width="10%"><a class="button_small_yellow" target="_blank" href="' . $file_url . '">Open</a></td>';
    $result_txt .= '</tr>';
}

$result_txt .= '</table>';
echo $result_txt;

