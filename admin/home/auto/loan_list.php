<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../../connect.php');


$conn = pg_connect($_SESSION['connect_str']);
$query = "SELECT * from loan_restrictions ORDER by membertype";
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);



if ($numrows > 0) {
    echo '<h2> Loan and Rent Restrictions: </h2>';
    echo '<table border="1" width="80%" style="border-collapse:collapse; border-color:grey">';
    echo '<tr style="color:green"><td>id</td><td>Member Type</td><td>Category</td><td>free</td>';
    echo '<td>weight</td><td>loan_type</td><td>notes</td><td>Active</td><tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);
    $ref1 = '<td><form action="" method="POST"><input type="hidden" name="id" value="' . $row['id'] . '">';
    $ref1 .='<input id="submit" name="submit" class="button_small_red"  type="submit" value="Delete" /></form></td>';

    echo '<td width="30" align="center">' . $row['id'] . '</td>';
    echo '<td width="250px">' . $row['membertype'] . '</td>';
    echo '<td width="150" align="left">' . $row['category'] . '</td>';

    echo '<td width="50" align="left">' . $row['free'] . '</td>';
    echo '<td width="50" align="left">' . $row['weight'] . '</td>';
    echo '<td width="100" align="left">' . $row['loan_type'] . '</td>';
    echo '<td width="250" align="left">' . $row['notes'] . '</td>';
    echo '<td width="50" align="center">Yes</td>';
    $enable = '<form method="post" action="auto.php"><input type="hidden" id ="nid" name="nid" value=' . $row['id'] . '>';
    $enable .='<input id="enable" name="enable" class="button_small_green"  type="submit" value="enable" /></form>';
    $disable = '<form method="post" action="auto.php"><input type="hidden" id="nid" name="nid" value=' . $row['id'] . '>';
    $disable .='<input id="disable" name="disable" class="button_small_red"  type="submit" value="disable" /></form>';
    if ($row['active'] == 'Yes'){
      //echo '<td width="250" align="center">' . $disable . '</td>';  
    }else{
       //echo '<td width="250" align="center">' . $enable . '</td>';   
    } 
//echo '<td width="30"  align="center">' . $row['toys'] . '</td>';
    $ref2 = 'delete_cat.php?id=' . $row['id'];
    $ref3 = 'edit_category.php?id=' . $row['id'];
//echo $ref1;

    echo '</tr>';
}
echo '<tr height="0"></tr>';
echo '</table>';



pg_close($conn);
//include( dirname(__FILE__) . '/new_category.php');
?>

</body>


