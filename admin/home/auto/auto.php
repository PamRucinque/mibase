<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>    


    <body id="main_body" >
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');

            if (isset($_POST['submit'])) {
                //echo 'hello';
                include( dirname(__FILE__) . '/delete_auto.php');
            }
            include( dirname(__FILE__) . '/auto_list.php');
            include( dirname(__FILE__) . '/loan_list.php');
            include( dirname(__FILE__) . '/overdue_list.php');
            ?>
        </div>
    </body>
</html>