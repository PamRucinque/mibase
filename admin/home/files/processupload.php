<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');

//get configuration data
//include( dirname(__FILE__) . '/../../config.php');



if (isset($_SESSION['idcat'])) {
    $idcat = $_SESSION['idcat'];
}
//$idcat = 'AAA';

if (isset($_FILES["FileInput"]) && $_FILES["FileInput"]["error"] == UPLOAD_ERR_OK) {
    ############ Edit settings ##############

    if ($_SESSION['shared_server']) {
        $UploadDirectory = $_SESSION['web_root_folder'] . $_SESSION['news_location'] . '/' . $_SESSION['library_code']; //specify upload directory ends with / (slash)
    } else {
        $UploadDirectory = $_SESSION['web_root_folder'] . $_SESSION['news_location'];
    }
    ##########################################

    /*
      Note : You will run into errors or blank page if "memory_limit" or "upload_max_filesize" is set to low in "php.ini".
      Open "php.ini" file, and search for "memory_limit" or "upload_max_filesize" limit
      and set them adequately, also check "post_max_size".
     */

    //check if this is an ajax request
    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
        die("ERROR: Must be called with Ajax!");
    }


    //Is file size is less than allowed size.
    if ($_FILES["FileInput"]["size"] > 2097152) {
        die("ERROR: File size is too big!");
    }

    //allowed file type Server side check


    $File_Name = strtolower($_FILES['FileInput']['name']);
    $File_Ext = substr($File_Name, strrpos($File_Name, '.') + 1); //get file extention
    //$Random_Number = rand(0, 9999999999); //Random number to be added to name.
    //$NewFileName = $Random_Number . $File_Ext; //new file name
    $description = $_POST['description'];
    $access = $_POST['access'];

    //delete an existing file if it exists
    if (file_exists($UploadDirectory . '/' . $File_Name)) {
        unlink($UploadDirectory . '/' . $File_Name);
    }
    
    $filename = add_file($File_Ext, $description, $access,$File_Name);
    if ($filename['result'] == 'ok') {
        if (move_uploaded_file($_FILES['FileInput']['tmp_name'], $UploadDirectory . '/' . $filename['filename'])) {
            die('Success! File Uploaded.</br>');
        } else {
            die('error uploading File!');
        }
    } else {
        die('Something wrong with upload! Is "upload_max_filesize" set correctly?');
    }
}

function add_file($type_file, $description, $access, $filename) {
    
    
    
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);


    $query_new = "SELECT coalesce(max(id), 0) as maxid FROM files;";
    
    $conn = pg_connect($_SESSION['connect_str']);
    $nextval = pg_Exec($conn, $query_new);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $newid = $row['maxid'] + 1;
   // $filename = $newid . "." . $type_file;
    date_default_timezone_set($_SESSION['settings']['timezone']);
    $lastupdate = date("Y-m-d H:i:s");

    $query = "INSERT INTO files (id, type_file, description, lastupdate, access,
                filename)
                 VALUES (?,?,?,?,?,?);";
    //and get the statment object back
    $sth = $pdo->prepare($query);

    $array = array($newid, $type_file, $description, $lastupdate, $access, $filename);
    //execute the preparedstament
    $sth->execute($array);
    $stherr = $sth->errorInfo();

    if ($stherr[0] != '00000') {
        $output = "An INSERT query error occurred.\n";
        $output .= 'Error ' . $stherr[0] . '<br>';
        $output .= 'Error ' . $stherr[1] . '<br>';
        $output .= 'Error ' . $stherr[2] . '<br>';
        exit;
    } else {
        $output = 'ok';
    }
    return array('result' => $output, 'filename' => $filename);
}
