<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include( dirname(__FILE__) . '/../../connect.php');
$sql = "select * from images where code is not null order by id;";
$result_list = pg_exec($conn, $sql);
$numrows = pg_numrows($result_list);

if ($_SESSION['shared_server']) {
    $UploadDirectory = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'];
} else {
    $UploadDirectory = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'];
}
//$numrows = 2;

echo '<h2>Renaming Files</h2>';
for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_list, $ri);

    $old_name = $UploadDirectory . '/' . $row['filename'];
    // $old_name = $UploadDirectory . 'ZN4Qv0503MTb.jpg';
    $new_name = $UploadDirectory . '/' . strtolower($row['code']) . '.jpg';
    //echo 'Renamed ' . $old_name . ' to ' . $new_name . '<br>';
    if (!rename($UploadDirectory . '/' . $row['filename'], $new_name)) {

        //if (!copy($UploadDirectory . $row['filename'], $new_name)) {
        echo '<br>' . $row['filename'];
        echo '<br>' . $row['code'] . '.jpg';
        //echo "failed to copy $old_name...<br>";
        $errors = error_get_last();
        echo "<br>COPY ERROR: " . $errors['type'];
        echo "<br />\n" . $errors['message'];
    } else {
        echo 'Renamed ' . $old_name . ' to ' . $new_name . '<br>';
    }



    // rename($old_name, $new_name);
}

echo 'Finished <br>';
