<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');





//$numrows = pg_numrows($result);
// confirm that the 'id' variable has been set
if (isset($_GET['id'])) {
    $filename = $_GET['id'];
    $sql = "DELETE FROM files WHERE filename = ?;";
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $sth = $pdo->prepare($sql);
    $array = array($_GET['id']);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    
    if ($stherr[0] != '00000') {
        $message = "An  error occurred.\n";
        $message .= 'Error' . $stherr[0] . '<br>';
        $message .= 'Error' . $stherr[1] . '<br>';
        $message .= 'Error' . $stherr[2] . '<br>';
    } else {
        //the delete from the database was successful so now delte the file
        if ($_SESSION['shared_server']) {
            unlink($_SESSION['web_root_folder'] . '/' . $_SESSION['news_location'] . '/' . $_SESSION['library_code'] . '/' . $_GET['id']);
        } else {
            unlink($_SESSION['web_root_folder'] . '/' . $_SESSION['news_location'] . '/' . $_GET['id']);
        }
        $_SESSION['error'] = 'You have successfully deleted a record';
    }
    $redirect = "Location: get_file.php";               //print $redirect;
    header($redirect);
} else {
// if the 'id' variable isn't set, redirect the user
    //header("Location: claims_info.php");
}

echo $query;
?>
