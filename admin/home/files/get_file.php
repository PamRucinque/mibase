<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Mibase File Uploader</title>
 
        <?php
        include( dirname(__FILE__) . '/../../header.php');
        ?>
        
        <script type="text/javascript" src="<?php echo $_SESSION['app_root_location']; ?>/home/files/js/jquery.form.min.js"></script>
        
    </head>

    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');
            //include( dirname(__FILE__) . '/../../connect.php');
        //    include( dirname(__FILE__) . '/../../toys/update/get_toy.php');
        //    include( dirname(__FILE__) . '/../../header_detail/header_detail.php');
     
            echo '<br><center><h3><font color="red">WARNING: Please do not use this uploader if you have a low data usage plan!</font></h3></center>';
            echo '<center><h3><font color="blue">Keep upload size to less than 1.5MB</font></h3></center>';

            $idcat = $_SESSION['idcat'];
            //echo '<br><h2>' . $idcat . ': ' . $toyname . '</h2><br>';
            
            //echo $img;
            ?>
            <table width="100%">
                <tr>
                     <td>
                        <?php include( dirname(__FILE__) . '/form_upload.php'); ?>
                    </td>
                </tr>
            </table>
          </div>
    </body>
</html>