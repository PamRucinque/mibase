<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

//get configuration data
//include( dirname(__FILE__) . '/../../config.php');
$conn = pg_connect($_SESSION['connect_str']);

if ($_SESSION['shared_server']) {
    $UploadDirectory = $_SESSION['news_location'] . '/' . $_SESSION['library_code'] ; //specify upload directory ends with / (slash)
} else {
    $UploadDirectory = $_SESSION['news_location']  ;
}





$sql = "select * from files order by type_file , lastupdate desc;";
$result_list = pg_exec($conn, $sql);
$numrows = pg_numrows($result_list);

$result_txt = '<h2>Uploaded Files</h2>';
    $result_txt .= '<table border="1" width="100%">';
for ($ri = 0; $ri < $numrows; $ri++) {
    $filename = pg_fetch_array($result_list, $ri);
    $filename['filename'] = trim($filename['filename']);
    $result_txt .= '<tr>';
  //  $result_txt .= '<td width = "5%">' . $filename['id'] . '</td>';
    $result_txt .= '<td width = "10%">' . $filename['filename'] . '</td>';
    $result_txt .= '<td width = "40%">' . $filename['description'] . '</td>';
    $result_txt .= '<td width = "10%">' . $filename['type_file'] . '</td>';
    $result_txt .= '<td width = "10%">' . $filename['access'] . '</td>';
    $pic =  $UploadDirectory . "/" . $filename['filename'] ;
    $file_path = '<td width="10%"><img height="50px" width="50px" src="' . $UploadDirectory . '/' . $filename['filename'] . '" onmouseover="showtrail(175,220, \'' . $pic . '\');" onmouseout="hidetrail();"></td>';
    if ($filename['type_file'] == 'jpg') {
        $result_txt .= $file_path;
    } else {
        $result_txt .= '<td width="10%"></td>';
    }
    $ref2 = 'delete.php?id=' . $filename['filename'];
    $result_txt .= '<td><a class="button_small_yellow" target="_blank" href="' .$UploadDirectory . '/' . $filename['filename'] . '">Open</a></td>';
    $result_txt .= '<td align="center"><a class ="button_small_red" href="' . $ref2 . '">Delete</a></td>';
    $result_txt .= '</tr>';
}

$result_txt .= '</table>';
echo $result_txt;

