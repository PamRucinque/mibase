<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');


$dir = $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'];
$count = 0;

chdir($dir);
//echo 'Current dir in files:  ' . getcwd();
//chdir('Q130113');
$print_str .= '<table border="1" style="border-collapse:collapse" width="100%">';
if ($handle = opendir('.')) {

    array_multisort(array_map('filemtime', ($file = glob("*.*"))), SORT_DESC, $file);

    $print_str .= '</table>';
    $print_str2 .= '<table border="1" style="border-collapse:collapse" width="100%">';
    foreach ($file as $filename) {

        if ($filename != "." && $file != ".." && $filename != ".htaccess" && $filename != "image_index.csv" && $filename != "del.php") {
            $userfile_extn = substr($filename, strrpos($filename, '.') + 1);
             $print_str2 .= '<tr>';
            $print_str2 .= '<td align="left">' . $count . '</td>';
            $print_str2 .= '<td align="left">' . $filename . '</td>';
            $temp = explode('.', $filename);
            $ext = array_pop($temp);
            $idcat = strtolower(implode('.', $temp));
            $intoys = find_toy($idcat);
            $print_str2 .= '<td align="left">' . $intoys . '</td>';
            if ($intoys == 1) {
                $print_str2 .= '<td align="left">Yes</td>';
            } else {
                $print_str2 .= '<td align="left">No</td>';
                unlink($filename);
            }
            $print_str2 .= '<td align="left">' . $idcat . '</td>';
            $print_str2 .= '<td align="left" width="20px">' . $linkimage . '</td>';
            $print_str2 .= '</tr>';
        }
        $count = $count + 1;
    }


    $print_str2 .= '</table>';
    echo $print_str2;
    //echo $print_str;
}

function find_toy($idcat) {
    
    
    $connection_str = "port=5432 dbname=" . $_SESSION['library_code'] . " user=" . $_SESSION['library_code'] . " password=" . $dbpasswd;
    $conn = pg_connect($connection_str);
    //include( dirname(__FILE__) . '/../connect.php');
    $count = 0;
    $sql = "select * from toys where lower(idcat) = '" . $idcat . "';";
    $result_list = pg_exec($conn, $sql);
    $count = pg_numrows($result_list);
    return $count;
}
?>
