<?php
/*
 * Copyright (C) 2018 SqualaDesign ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
include('../../mibase_check_login.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MiBaseNZ - Daily Stat</title>
        <?php include('../../header.php'); ?> 
        <script type="text/javascript">
           $( function() {
                var dateFormat = "yy-mm-dd",
                  start = $( "#start" )
                    .datepicker({dateFormat: "yy-mm-dd", showOtherMonths: true, changeMonth: true, changeYear: true })
                    .on( "change", function() {
                      end.datepicker( "option", "minDate", getDate( this ) );
                    }),
                  end = $( "#end" ).datepicker({dateFormat: "yy-mm-dd", showOtherMonths: true, changeMonth: true, changeYear: true })
                  .on( "change", function() {
                    start.datepicker( "option", "maxDate", getDate( this ) );
                  });
                function getDate( element ) {
                  var date;
                  try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                  } catch( error ) {
                    date = null;
                  }
                  return date;
                }
              } );
        </script>
    </head>
    <body id="main_body" >    
        <div class="container">
            <?php //include('../header-logo.php'); ?>
            <?php
                echo '<div class="row">';
                include('../menu.php');
                echo '</div><div class="row">';
                include('../header_detail/header_detail.php');
                echo '</div>';
                include('get_library.php');
                include('get_active_members.php');
                if(isset($_POST['start']))
                    $start = $_POST['start'];
                else
                    $start = date ("Y-m-01");
                    
                if(isset($_POST['end']))
                    $end = $_POST['end'];
                else
                    $end = date ("Y-m-d");
            ?>
        </div><!-- /container -->
        <div class="container main" id="form_container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Daily Stat</h2>
                    <div class="row bk-grey">
                        <form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action= "daily_stat.php">
                            <div class="form-group col-md-2">
                                <label>Start Day:</label>
                                <input type="text" name="start" id ="start" value="<?php echo $start; ?>"></input>
                            </div>
                            <div class="form-group col-md-2">
                                <label>End Day:</label>
                                <input type="text" name="end" id ="end" value="<?php echo $end; ?>"></input>
                            </div>
                            <div class="form-group col-md-1 button-max-margin">
                                <span class="icon-input-btn">
                                    <span class="glyphicon glyphicon-search"></span> 
                                    <input id="saveForm" class="btn btn-default btn-violet" type="submit" name="submit" value="Search" />
                                </span>
                            </div>
                            <div class="form-group col-md-1 button-max-margin">
                                <a href='daily_stat.php' class ='btn btn-default btn-orange'><i class="fas fa-redo-alt"></i> Reset</a>
                            </div>                        
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-sort"><strong>Info!</strong> Click on the table header to sort the table data. If you want to export the data you can filter and/or sort before dowloanding the pdf or excel.</div>
                </div>
            </div>
            <div class="row btn-pdf-ex">
                <div class="col-lg-offset-7 col-lg-5 col-md-offset-6 col-md-6 col-sm-offset-2 col-sm-10 col-xs-12">
                    <button id="exportButtonpdf" class="btn btn-default btn-danger clearfix"><i class="fas fa-file-pdf"></i> Export to PDF</button>
                    <button id="exportButtonexcel" class="btn btn-default btn-success clearfix"><i class="fas fa-file-excel"></i> Export to Excel</button>
                    <button class="btn btn-default btn-info clearfix" onclick="openNav()"><i class="fas fa-file-alt"></i> Documentation</button>
                </div>
            </div>


            <?php $query = "SELECT tr.date_loan as date_loan,
                (SELECT COUNT(*) FROM transaction WHERE date_loan = tr.date_loan) loans,
                (SELECT COUNT(return) FROM transaction WHERE return = tr.date_loan) toy_returned,
                (SELECT COUNT(distinct borname) FROM transaction WHERE date_loan = tr.date_loan) members,
                (SELECT SUM(amount) FROM Journal where datepaid = tr.date_loan and type = 'CR') amount,
                (SELECT string_agg(r.member_id::TEXT || ': ' ||b.firstname || ' ' || b.surname, ', ') FROM roster R INNER JOIN borwrs B ON B.id = R.member_id where R.date_roster = tr.date_loan and R.status = 'completed') roster,
                (SELECT COUNT(*) FROM reserve_toy where date(date_created) = tr.date_loan) reserved,
                (SELECT COUNT(*) FROM borwrs where date(datejoined) = tr.date_loan) new_members,
                (SELECT COUNT(*) FROM borwrs where date(renewed) = tr.date_loan and renewed <> datejoined) members_renewed
                from transaction tr
                WHERE tr.date_loan > now()::date - 365
                AND tr.date_loan >= '" . $start . "'
                AND tr.date_loan <= '" . $end . "'
                group by tr.date_loan
                order by tr.date_loan DESC";

                $result = pg_exec($conn, $query);
                $numrows = pg_numrows($result);
                if ($numrows > 0) {
                    echo '<table id="provapdf" class="table table-striped table-responsive tablesorter ordinamentotabelle all-right">';
                }
                for ($ri = 0; $ri < $numrows; $ri++) {
                    $row = pg_fetch_array($result, $ri);

                    $format_date_loan = substr($row['date_loan'], 8, 2) . '-' . substr($row['date_loan'], 5, 2) . '-' . substr($row['date_loan'], 0, 4);
                    $amount = number_format($row['amount'], 2, '.', ',');

                    if ($ri == 0) {
                        echo '<thead><tr><th class="sorter-shortDate dateFormat-ddmmyyyy">Date</th><th>Loans</th><th>Returns</th><th>Members</th><th>Reserved</th><th>NewMembers</th><th>Renewed</th><th>Amount</th><th>Roster</th></tr></thead><tbody>';
                    }
                    echo '<tr>';
                    echo '<td>' . $format_date_loan . '</td>';
                    echo '<td>' . $row['loans'] . '</td>';
                    echo '<td>' . $row['toy_returned'] . '</td>';
                    echo '<td>' . $row['members'] . '</td>';
                    echo '<td>' . $row['reserved'] . '</td>';
                    echo '<td>' . $row['new_members'] . '</td>';
                    echo '<td>' . $row['members_renewed'] . '</td>';
                    echo '<td>$' . $amount . '</td>';
                    echo '<td style="max-width:300px; word-wrap="break-word;">' . $row['roster'] . '</td>';
                    echo '</tr>';
                }
                if ($numrows > 0) {
                    echo '</tbody></table>';
                }
                pg_close($conn);
            ?>
        <?php include('../footer.php'); ?>
        </div> <!-- /container -->
        <!-- Modal Box for instruction -->
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <h2>Page instruction</h2>
            <p>The page <strong>"Daily Stat"</strong> has been created to see all statistics information about every day/session.</p>
            <p>You can click on the table header to sort the table data and you are also able to download/print the PDF and Excel before of after using the filter.</p><p><strong>Loans:</strong> Number of toys loaned, <strong>Returns:</strong> Number of toys returned, <strong>Members:</strong> Number of members come in the toy library, <strong>Reserved:</strong> Number of toys reserved in the day, <strong>NewMembers:</strong> Number of members joined, <strong>Renewed:</strong> Number of members renewed the membership.</p> 
        </div>
        <!-- script for PDF and Excel button -->
        <script type="text/javascript">
        jQuery(function ($) {
                $("#exportButtonexcel").click(function () {
                    // parse the HTML table element having an id=exportTable
                    var dataSource = shield.DataSource.create({
                        data: "#provapdf",
                        schema: {
                            type: "table",
                            fields: {
                                Date: { type: String },
                                Loans: { type: String },
                                Returns: { type: String },
                                Members: { type: String },
                                Reserved: { type: String },
                                NewMembers: { type: String },
                                Renewed: { type: String },
                                Amount: { type: String },
                                Roster: { type: String }
                            }
                        }
                    });
                    // when parsing is done, export the data to Excel
                    dataSource.read().then(function (data) {
                        new shield.exp.OOXMLWorkbook({
                            author: "MibaseNZ",
                            worksheets: [
                                {
                                    name: "PrepBootstrap Table",
                                    rows: [
                                        {
                                            cells: [
                                                 { style: { bold: true }, type: String, value: "Date"},
                                                 { style: { bold: true }, type: String, value: "Loans"},
                                                 { style: { bold: true }, type: String, value: "Returns"},
                                                 { style: { bold: true }, type: String, value: "Members"},
                                                 { style: { bold: true }, type: String, value: "Reserved"},
                                                 { style: { bold: true }, type: String, value: "NewMembers"},
                                                 { style: { bold: true }, type: String, value: "Renewed"},
                                                 { style: { bold: true }, type: String, value: "Amount"},
                                                 { style: { bold: true }, type: String, value: "Roster"}
                                            ]
                                        }
                                    ].concat($.map(data, function(item) {
                                        return {
                                            cells: [
                                                { type: String, value: item.Date },
                                                { type: String, value: item.Loans },
                                                { type: String, value: item.Returns },
                                                { type: String, value: item.Members },
                                                { type: String, value: item.Reserved },
                                                { type: String, value: item.NewMembers },
                                                { type: String, value: item.Renewed },
                                                { type: String, value: item.Amount },
                                                { type: String, value: item.Roster }
                                            ]
                                        };
                                    }))
                                }
                            ]
                        }).saveAs({
                            fileName: "DailyStatExcel"
                        });
                    });
                });
            });
            jQuery(function ($) {
                $("#exportButtonpdf").click(function () {
                    // parse the HTML table element having an id=exportTable
                    var dataSource = shield.DataSource.create({
                        data: "#provapdf",
                        schema: {
                            type: "table",
                            fields: {
                                Date: { type: String },
                                Loans: { type: String },
                                Returns: { type: String },
                                Members: { type: String },
                                Reserved: { type: String },
                                NewMembers: { type: String },
                                Renewed: { type: String },
                                Amount: { type: String },
                                Roster: { type: String }
                            }
                        }
                    });

                    // when parsing is done, export the data to PDF
                    dataSource.read().then(function (data) {
                        var pdf = new shield.exp.PDFDocument({
                            author: "MibaseNZ",
                            created: new Date(),
                            fontSize: 10
                        });

                        pdf.addPage("a4", "landscape");

                        pdf.table(
                            40,
                            40,
                            data,
                            [
                                { field: "Date", title: "Date", width: 60 },
                                { field: "Loans", title: "Loans", width: 50 },
                                { field: "Returns", title: "Returns", width: 55 },
                                { field: "Members", title: "Members", width: 60 },
                                { field: "Reserved", title: "Reserved", width: 60 },
                                { field: "NewMembers", title: "New Members", width: 60 },
                                { field: "Renewed", title: "Renewed", width: 60 },
                                { field: "Amount", title: "Amount", width: 70 },
                                { field: "Roster", title: "Roster", width: 250 }
                            ],
                            {
                                margins: {
                                    left: 20,
                                    top: 20,
                                    bottom: 20
                                }
                            }
                        );
                        pdf.saveAs({
                            fileName: "DailyStatPDF"
                        });
                    });
                });
            });
        </script>
    </body>
</html>