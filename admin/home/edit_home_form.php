<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons | fontsizeselect",
        fontsize_formats: "8px 10px 11px 12px 14px 18px 24px 36px",
        image_advtab: true,
        relative_urls: false,
        remove_script_host: false
    });
</script>

<p><font size="2" face="Arial, Helvetica, sans-serif"></font></p>

<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>

<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="edit_home.php">

    <div id="form" style="background-color:lightgray;" align="left">
        <font><h2>Edit Website Information:</h2></font>

        <table width="80%"><tr>
            </tr>
            <tr><td align="right"><input id="saveForm" class="button1" type="submit" name="submit" value="Save Web Info" /></td></tr>

            <tr><td><label><br>Front Page Comments: </label><br>
                    <textarea id="frontpage" name="frontpage" rows="15" cols="65"><?php echo $index_page; ?></textarea></td></tr>
            <tr><td><label><br>FAQ Page on Website:</label><br>
                    <textarea id="webpage" name="webpage" rows="15" cols="65"><?php echo $webpage; ?></textarea></td></tr>
            <tr><td><label><br>Conditions of Membership: </label><br>
                    <textarea id="conditions" name="conditions" rows="15" cols="65"><?php echo $conditions; ?></textarea></td></tr>

            <tr><td><label><br>Opening hours: </label><br>
                    <textarea id="open_hours" name="open_hours" rows="15" cols="65"><?php echo $open_hours_frontpage; ?></textarea></td></tr>
            <tr><td><label><br>Members Home Page: </label><br>
                    <textarea id="mem_homepage" name="mem_homepage" rows="15" cols="65"><?php echo $mem_homepage; ?></textarea></td></tr>
            <tr><td><label><br>Sponsors Page on Website: </label><br>
                    <textarea id="sponsor" name="sponsor" rows="15" cols="65"><?php echo $sponsor; ?></textarea></td></tr>
            <tr><td><label><br>Member Options on Website: </label><br>
                    <textarea id="member_options" name="member_options" rows="15" cols="65"><?php echo $member_options; ?></textarea></td></tr>

            <tr><td><label><br>Payment Information: Instructions for members - Joining: (BSB, account number): </label><br>
                    <textarea id="payment_info_join" name="payment_info_join" rows="15" cols="65"><?php echo $payment_info_join; ?></textarea></td></tr>

            <tr><td><label><br>Payment Information: Instructions for members - Renewing: (BSB, account number): </label><br>
                    <textarea id="payment_info" name="payment_info" rows="15" cols="65"><?php echo $payment_info; ?></textarea></td></tr>
            <tr><td><label><br>Bouncing Castle Terms and Conditions: (leave blank if you don't have one) </label><br>
                    <textarea id="castle" name="castle" rows="15" cols="65"><?php echo $castle; ?></textarea></td></tr>
            <tr><td>Jumping Castle comments for popup:(don't use formatting, just a popup message)<br>
                    <textarea id="castle_link" name="castle_link" rows="2" cols="65"><?php echo $castle_link; ?></textarea>
            <tr><td><label><br>Helmet Waiver: (leave blank if you don't have one) </label><br>
                    <textarea id="helmet" name="helmet" rows="15" cols="65"><?php echo $helmet; ?></textarea></td></tr>
            <tr><td><label><br>Child Safe Policy & Code of Conduct: (leave blank if you don't have one) </label><br>
                    <textarea id="wwc" name="wwc" rows="15" cols="65"><?php echo $wwc; ?></textarea></td></tr>

        </table>
    </div>
</form></p>

