<?php
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>

    <body id="main_body" >
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            $password = $_SESSION['settings']['password'];

            $ref = 'members_approve.php';

            echo "<h2><br><a href='" . $ref . "' class ='button1_logout'>Cancel</a>";

            echo '<font color="red"><br> WARNING - YOU ARE ABOUT TO DELETE A MEMBER</font></h1><br>';
            include( dirname(__FILE__) . '/delete_form.php');
            if ($password == '') {
                $password = 'mibase';
            }

            if (isset($_POST['submit'])) {
                if (isset($_POST['id']) && is_numeric($_POST['id'])) {
                    // get the 'id' variable from the URL
                    $id = $_GET['id'];
                    $query = "DELETE FROM borwrs WHERE id = " . $_POST['id'] . ";";
                    if (($_POST['password'] == $password)) {
                        $connection_str = "port=" . $dbport . " dbname=" . $logdbname . " user=" . $logdbuser . " password=" . $logdbpasswd;
                        $conn = pg_Connect($connection_str);
                        $result = pg_exec($conn, $query);
                        if (!$result) {
                            echo "An INSERT query error occurred.\n";
                            echo $query;
                            //exit;
                        } else {
                            echo "<br><p>The record was successfully deleted.</p><br>";
                            echo '<a class="button1" href="members_approve.php">OK</a>';
                            $_SESSION['del_Status'] = '';
                        }
                    } else {
                        echo "<br><p>Incorrect Password.</p><br>";
                        echo '<a class="button1" href="../members.php">OK</a>';
                    }
                }
            }
            ?>
        </div>
    </body>
</html>



