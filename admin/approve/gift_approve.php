<?php
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <style type="text/css">
            tr:hover { 
                color: red; }
            </style>
        </head>
        <?php $total = 0; ?>
        <body>
            <div id="form_container">
            <?php include( dirname(__FILE__) . '/../menu.php'); ?>

            <?php
            //get the connection object
            $conn = pg_connect($_SESSION['connect_str']);

            //display pending gift cards
            $query_pending = "SELECT gift_cards.*, date_part('day', now()::timestamp - created::timestamp) as days
                    FROM gift_cards 
                    WHERE library = '" . $_SESSION['library_code'] . "'
                    ORDER BY created asc;";

            $result_list_pending = pg_exec($conn, $query_pending);
            $numrows_pending = pg_numrows($result_list_pending);

            $result_txt_pending = '';

            for ($ri = 0; $ri < $numrows; $ri++) {
                $row = pg_fetch_array($result_list_pending, $ri);
                $id = $row['id'];
                $format_entered = substr($row['created'], 8, 2) . '-' . substr($row['created'], 5, 2) . '-' . substr($row['created'], 0, 4);
                $p_name = $row["p_name"];
                $email = $row["email"];
                $mobile = $row["mobile"];
                $email_sent = $row['email_sent'];
                $expired = $row['expired'];
                $format_expired = substr($expired, 8, 2) . '-' . substr($expired, 5, 2) . '-' . substr($expired, 0, 4);
                $result_txt_pending .= '<tr border="1"><td border="1" width="50">' . $format_entered . '</td>';
                $result_txt_pending .= '<td align="center">' . $row['id'] . '</td>';
                $result_txt_pending .= '<td align="left">' . $p_name . '</td>';
                $result_txt_pending .= '<td align="left">' . $email . '</td>';
                $result_txt_pending .= '<td  width="100">' . $mobile . '</td>';
                $result_txt_pending .= '<td  width="100">' . $email_sent . '</td>';
                $result_txt_pending .= '<td><a class="button_small_green" href="update/new_gift.php?id=' . $id . '"/>Details</a>';
                $result_txt_pending .= '<td><a class="button_small_red" href="delete_gift.php?id=' . $id . '"/>Delete</a>';
            }
            $result_txt_pending .= '</tr></table>';

            if ($numrows_pending > 0) {
                print '<br><div id="open"><table width="100%"><tr><td width= 50%><h1 align="left">Approve Gift Cards</h1></td><td><h1 align="right">Total: ' . $numrows_pending . '</h1></td><tr></table></div>';
                print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
                print '<tr style="color:green"><td>entered</td><td>id</td><td>Name</td><td>Email</td><td width="110">Mobile</td><td>Card Emailed</td><td></td></tr>';
                print $result_txt_pending;
            } else {
                echo '<br><br><strong>There are no Gift Cards pending approval.</strong><br><br>';
            }


            //display redeemed git cards
            $query_redeemed = "SELECT gift_cards.*, date_part('day', now()::timestamp - gift_cards.created::timestamp) as days,
                    (b.surname || ', ' || b.firstname || ': ' || b.id) as redeemed_by
                    FROM gift_cards 
                    LEFT OUTER borwrs b on (gift_cards.borid = b.id)
                    ORDER BY expired;";

            $result_list_redeemed = pg_exec($conn, $query_redeemed);
            $numrows_redeemed = pg_numrows($result_list_redeemed);

            $result_txt_redeemed = '';

            for ($ri = 0; $ri < $numrows; $ri++) {
                $row = pg_fetch_array($result_list, $ri);
                $total = $total + 1;
                $id = $row['id'];
                $format_entered = substr($row['created'], 8, 2) . '-' . substr($row['created'], 5, 2) . '-' . substr($row['created'], 0, 4);
                $p_name = $row["p_name"];
                $email = $row["email"];
                $mobile = $row["mobile"];
                $email_sent = $row['email_sent'];
                $expired = $row['expired'];
                $redeemed_by = $row['redeemed_by'];
                $format_expired = substr($expired, 8, 2) . '-' . substr($expired, 5, 2) . '-' . substr($expired, 0, 4);
                $result_txt_redeemed .= '<tr border="1"><td border="1" width="50">' . $format_expired . '</td>';
                $result_txt_redeemed .= '<td align="center">' . $row['id'] . '</td>';
                $result_txt_redeemed .= '<td align="left">' . $p_name . '</td>';

                $result_txt_redeemed .= '<td align="left">' . $email . '</td>';
                $result_txt_redeemed .= '<td>' . $mobile . '</td>';
                $result_txt_redeemed .= '<td>' . $email_sent . '</td>';
                $result_txt_redeemed .= '<td><font color="blue">' . $redeemed_by . '</font></td>';
            }
            $result_txt_redeemed .= '</tr></table>';

            if ($numrows > 0) {
                print '<br><div id="open"><table width="100%"><tr><td width= 50%><h1 align="left">Redeemed Gift Cards</h1></td><td><h1 align="right">Total: ' . $numrows_redeemed . '</h1></td><tr></table></div>';
                print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
                print '<tr style="color:green"><td>Expires</td><td>id</td><td>Name</td><td>Email</td><td>Mobile</td><td>Card Emailed</td><td>Redeemed By</td></tr>';
                print $result_txt_redeemed;
            } else {
                echo '<br><br><strong>There are no Redeemed Gift Cards.</strong><br><br>';
            }
            ?>
    </body>
</html>