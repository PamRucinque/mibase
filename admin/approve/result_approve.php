<?php
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>

<style type="text/css">
    tr:hover { 
        color: red; }
</style>

<?php
if (!session_id()) {
    session_start();
}

$query = "SELECT borwrs.*, date_part('day', now()::timestamp - created::timestamp) as days
FROM borwrs
WHERE library = '" . $_SESSION['library_code'] . "'
AND spam = 'No' 
ORDER BY created asc, surname;";

$XX = "No Record Found";

$total = 0;

$conn = pg_connect($_SESSION['connect_str']);
$result_list = pg_exec($conn, $query);
$numrows = pg_numrows($result_list);

$result_txt = '';

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_list, $ri);
    $total = $total + 1;
    $expired = $row['expired'];
    $format_expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
    $format_entered = substr($row['created'], 8, 2) . '-' . substr($row['created'], 5, 2) . '-' . substr($row['created'], 0, 4);
    $borid = $row["id"];
    $firstname = $row["firstname"];
    $membertype = $row["membertype"];
    $surname = $row["surname"];
    $partnersname = $row["partnersname"];
    $email = $row["emailaddress"];
    $phone = $row["phone"];
    $mobile1 = $row["mobile1"];
    $member_status = $row["member_status"];
    $address = $row['address'];
    $suburb = $row['suburb'];
    $email_link = '<a href="mailto:' . $email . '">send</a>';
    $date_expired = date_create_from_format('d-m-Y', $format_expired);
    $today = date('Y-m-d');
    $result_txt .= '<tr border="1"><td border="1" width="50">' . $format_entered . '</td>';
    $result_txt .= '<td align="center">' . $row['days'] . '</td>';
    $result_txt .= '<td align="center">' . $row['id'] . '</td>';
    $result_txt .= '<td align="left">' . $surname . '</td>';
    $result_txt .= '<td align="left">' . $firstname . '</td>';
    $result_txt .= '<td align="left">' . $email . '</td>';
    $result_txt .= '<td align="left">' . $address . ' ' . $suburb . '</td>';
    $result_txt .= '<td  width="100">' . $mobile1 . '</td>';
    $result_txt .= '<td><a class="button_small_green" href="update/new_approve.php?id=' . $borid . '"/>Approve</a>';
    $result_txt .= '<td><a class="button_small_red" href="delete_mem.php?id=' . $borid . '"/>Delete</a>';
}
$result_txt .= '</tr></table>';

if ($numrows > 0) {
   print '<br><div id="open"><table width="100%"><tr><td width= 50%><h1 align="left">Approve Members List</h1></td><td><h1 align="right">Total: ' . $total . '</h1></td><tr></table></div>';
     print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
    print '<tr style="color:green"><td>entered</td><td>days</td><td>id</td><td>Surname</td><td>Firstname</td><td>email</td><td>Address</td><td width="110">Mobile</td><td></td><td></td><tr>';

    print $result_txt;
}else{
    echo '<br><br><strong>There are no Members pending approval.</strong><br><br>';
}
?>