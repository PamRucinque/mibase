<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <script type="text/javascript">
        $(function () {
            var pickerOpts = {
                dateFormat: "d MM yy",
                showOtherMonths: true

            };
            $("#submitted").datepicker(pickerOpts);
            $("#closed").datepicker(pickerOpts);
        });

    </script>


</head>
<body>
    <div id="form_container">
        <?php
        include( dirname(__FILE__) . '/../../menu.php');
        include( dirname(__FILE__) . '/../../connect.php');
        include( dirname(__FILE__) . '/../../get_settings.php');
        include( dirname(__FILE__) . '/functions.php');
        include( dirname(__FILE__) . '/get_gift_approve.php');


        if (isset($_POST['submit'])) {
            $p_name = pg_escape_string($_POST['p_name']);
            $email = pg_escape_string($_POST['email']);
            $mobile = pg_escape_string($_POST['mobile']);
            $description = pg_escape_string($_POST['description']);
            $created_new = $_POST['created'];
            $newsletter = $_POST['newsletter'];
            $now = date('Y-m-d');
            $category = 'Gift Card';
            $amount = get_gift_card($category);
            //$amount = 11;
            $longname = get_member($_POST['borid']);
            $borid = $_POST['borid'];
            //include( dirname(__FILE__) . '/get_gift_approve.php');
            $payment_str = add_to_journal($now, $_POST['borid'], $_POST['id'], $longname, $description, $category, $amount, 'CR', 'Credit');
            $add = add_to_gift_table($p_name, $mobile, $email, $newsletter, $created_new, 'Yes', $expired, 'Yes', $amount, $_POST['id'], $borid);
            //echo 'Added to gift table: ' . $add;
            echo '<br><br>';
            echo '<strong>' . $payment_str . '</strong><br><br>';
            echo '<a class="button1_green" align="right" title="Back to Gift Cards" href="../gift_approve.php">Back to Gift Cards List</a><br><br>';
        }


        if (isset($_POST['send_email'])) {
            include( dirname(__FILE__) . '/send_email_gift.php');
        }

        //include( dirname(__FILE__) . '/get_gift_approve_new.php');
        if (($email_sent == 'Yes') && ($add != 'success')) {
            include( dirname(__FILE__) . '/new_form_gift.php');
        } else {
            if ($_GET['id'] != '') {
                echo '<h2>Gift Card Details:</h2>';
                echo 'Gift Card id: ' . $id_new . '<br>';
                echo 'Purchaser Name: ' . $p_name_new . '<br>';
                echo 'Email: ' . $email_new . '<br>';
                echo 'Mobile: ' . $mobile_new . '<br><br>';
                echo '<form method="post" action="new_gift.php?id=' . $id_new . '">';
                echo '<input id="send_email" class="button1_green" type="submit" name="send_email" value="Approve and Email Gift Card" />';
                echo '<input type="hidden" name="id" value="' . $id_new . '">';
                echo '</form>';
            }
        }
        if ($add == 'success') {
            $delete = delete_gift_card($_POST['id']);
        }

        function get_gift_card($category) {
            include( dirname(__FILE__) . '/../../connect.php');
            $fee = 0;
            $sql = "SELECT * FROM paymentoptions  
          WHERE upper(paymentoptions)='" . strtoupper($category) . "';";
            //echo $sql;
            $nextval = pg_Exec($conn, $sql);
            $row = pg_fetch_array($nextval, 0);
            $fee = $row['amount'];
            return $fee;
        }

        function get_member($borid) {
            include( dirname(__FILE__) . '/../../connect.php');
            $sql = "SELECT * FROM borwrs   
          WHERE id =" . $borid . ";";
            $nextval = pg_Exec($conn, $sql);
            $row = pg_fetch_array($nextval, 0);
            $longname = $row['firstname'] . ' ' . $row['surname'];
            //echo $longname;
            return $longname;
        }

        function delete_gift_card($id) {
            $conn = pg_Connect("port=5432 dbname=new_members user=new_members password=WSjuJ9YLj43JUNL");
            $sql = "DELETE FROM gift_cards    
          WHERE id =" . $id . ";";
            $nextval = pg_Exec($conn, $sql);
            //echo $sql;
        }

        function add_to_gift_table($p_name, $mobile, $email, $newsletter, $created, $email_sent, $expired, $credit_sent, $amount, $online_id, $borid) {
            include( dirname(__FILE__) . '/../../connect.php');
            //echo $connection_str;
            $sql = "INSERT INTO gift_cards (p_name, mobile, email, newsletter, created, email_sent, expired, credit_sent, amount, online_id, borid) "
                    . " VALUES (?,?,?,?,?,?,?,?,?,?,?);";
            $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
            $sth = $pdo->prepare($sql);
            $array = array($p_name, $mobile, $email, $newsletter,
                $created, $email_sent, $expired, $credit_sent, $amount, $online_id, $borid);
            $sth->execute($array);
            $stherr = $sth->errorInfo();
            if ($stherr[0] != '00000') {
                $output = "An INSERT query error occurred.\n";
                $output .= 'Error' . $stherr[0] . '<br>';
                $output .= 'Error' . $stherr[1] . '<br>';
                $output .= 'Error' . $stherr[2] . '<br>';
                $output .= $query;
            } else {
                $output = 'success';
            }

            return $output;
        }
        ?>
    </div>
</body>
</html>



