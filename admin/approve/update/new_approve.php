<?php
require( dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <script type="text/javascript">
        $(function () {
            var pickerOpts = {
                dateFormat: "d MM yy",
                showOtherMonths: true

            };
            $("#submitted").datepicker(pickerOpts);
            $("#closed").datepicker(pickerOpts);
        });

    </script>


</head>
<body>
    <div id="form_container">
        <?php
        include( dirname(__FILE__) . '/../../menu.php');

        include( dirname(__FILE__) . '/get_member_approve.php');
        $reuse_memberno = $_SESSION['settings']['reuse_memberno'];
        $settings_expired = $_SESSION['settings']['expired'];
        $libraryname = $_SESSION['settings']['libraryname'];
        $catsort = $_SESSION['settings']['catsort'];
        $online_conduct = $_SESSION['settings']['online_conduct'];

        //echo $levy_new;
        if ($levy_new == 't') {
            $alert_msg = 'This member would like to pay the levy, please change/update the member type.';
            echo '<script> $(document).ready(function(){ alert("' . $alert_msg . '");});</script>';
            $alert_msg = '';
        }

        include( dirname(__FILE__) . '/new_memid.php');
        include( dirname(__FILE__) . '/functions.php');


        if (isset($_POST['submit'])) {


            $firstname = pg_escape_string($_POST['firstname']);
            $surname = pg_escape_string($_POST['surname']);
            $notes = pg_escape_string($_POST['notes']);
            $partnersname = pg_escape_string($_POST['partnersname']);
            $partnerssurname = pg_escape_string($_POST['partnerssurname']);
            $membertype = pg_escape_string($_POST['membertype']);
            $suburb = pg_escape_string($_POST['suburb']);
            $address = pg_escape_string($_POST['address']);
            $address2 = pg_escape_string($_POST['address2']);
            $postcode = pg_escape_string($_POST['postcode']);
            $state = pg_escape_string($_POST['state']);
            $mobile1 = pg_escape_string($_POST['mobile1']);
            $mobile2 = pg_escape_string($_POST['mobile2']);
            $email = pg_escape_string($_POST['emailaddress']);
            $email2 = pg_escape_string($_POST['email2']);
            $phone = pg_escape_string($_POST['phone']);
            $created = pg_escape_string($_POST['created']);
            $skills2 = pg_escape_string($_POST['skills2']);
            $discoverytype = pg_escape_string($_POST['discoverytype']);
            $wwc = pg_escape_string($_POST['wwc']);
            $rostertype = $_POST['rostertype1'];
            $rostertype2 = $_POST['rostertype2'];
            $rostertype3 = $_POST['rostertype3'];
            $childname1 = $_POST['childname1'];
            $childname2 = $_POST['childname2'];
            $childname3 = $_POST['childname3'];
            $dob1 = $_POST['dob1'];
            $dob2 = $_POST['dob2'];
            $dob3 = $_POST['dob3'];
            $sex1 = $_POST['sex1'];
            $sex2 = $_POST['sex2'];
            $sex3 = $_POST['sex3'];
            $helmet_new = $_POST['helmet'];
            $old_id = $_POST['old_id'];
            $id_license = $_POST['id_license'];
            $now = date('Y-m-d');
            $joined = $now;
            if ($settings_expired != '') {
                if ($settings_expired_year != '') {
                    $expired = trim($settings_expired_year) . "-" . trim($settings_expired);
                } else {
                    $expired = date('Y', strtotime('+1 year')) . "-" . trim($settings_expired);
                }
            } else {
                $expired = date_expired($membertype, $joined);
            }
            if (isset($_POST['levy'])) {
                $levy = 'TRUE';
            } else {
                $levy = 'FALSE';
            }

            $str = $surname;
            $length_surname = strlen($str);
            if ($length_surname < 10) {
                $str = pad_surname($str);
            } else {
                $str = substr($str, 0, 10);
                $length_surname = 10;
            }
            $str = strtolower($str);
            $username = rtrim($str, "%") . $newmemid;
            $username = str_replace(" ", "", $username);
            $username = str_replace("-", "", $username);
            $username = str_replace("_", "", $username);
            $pwd = generateStrongPassword();
            if ($online_conduct == 'Yes') {
                $conduct = 'Yes';
            }
            $helmet = 'FALSE';
            if ($helmet_new == 't') {
                $helmet_date = date('Y-m-d H:i:s');
                $helmet = 'TRUE';
            }



            
            
            

            try {
                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
            } catch (PDOException $e) {
                print "Error! add approved member : " . $e->getMessage() . "<br/>";
                die();
            }
            $query = "INSERT INTO borwrs (id, firstname, surname, membertype, notes, partnersname, 
        partnerssurname, suburb, address, address2, postcode,state, datejoined, renewed, expired, rostertype5, pwd, id_license,
        phone2, mobile1, emailaddress, email2, phone, discoverytype, skills2, rostertype, rostertype2, rostertype3, rosternotes, member_status,created, levy, helmet, wwc, conduct, rego_id, helmet_date)
                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
            //$query .= $child_sql;

            $sth = $pdo->prepare($query);

            //create the array of data to pass into the prepared stament
            $array = array($newmemid, $firstname, $surname, $membertype, $notes, $partnersname,
                $partnerssurname, $suburb, $address, $address2, $postcode, $state, $now, $now, $expired, $username, $pwd, $id_license, $mobile1, $mobile2,
                $email, $email2, $phone, $discoverytype, $skills2, $rostertype, $rostertype2, $rostertype3, $_POST['rosternotes'], 'ACTIVE', $created, $levy, $helmet, $wwc, $conduct, $old_id, $helmet_date);

            //execute the preparedstament
            $sth->execute($array);

            //get the error info for the statment just executed.
            //0 	SQLSTATE error code (a five characters alphanumeric identifier defined in the ANSI SQL standard).
            //1 	Driver specific error code.
            //2 	Driver specific error message.
            $stherr = $sth->errorInfo();



            if ($stherr[0] != '00000') {
                echo "An INSERT query error occurred.\n";
                //echo $query;
                if ($stherr[0] != '00000') {
                    $error_msg .= "An  error occurred.\n";
                    $error_msg .= 'Error' . $stherr[0] . '<br>';
                    $error_msg .= 'Error' . $stherr[1] . '<br>';
                    $error_msg .= 'Error' . $stherr[2] . '<br>';
                }
                echo $error_msg;
                exit;
            } else {
                if ($childname1 != '') {
                    $result = add_child($_POST['id'], $childname1, $dob1, $sex1);
                    echo '<br>' . $result;
                }
                if ($childname2 != '') {
                    $result = add_child($_POST['id'], $childname2, $dob2, $sex2);
                    echo $result;
                }
                if ($childname3 != '') {
                    $result = add_child($_POST['id'], $childname3, $dob3, $sex3);
                    echo $result;
                }
                if (($_SESSION['library_code'] != 'diammond') && ($_SESSION['library_code'] != 'demo')) {
                    $delete_new = delete_approved($old_id);
                    if ($delete_new != '') {
                        echo $delete_new;
                    }
                }

                if ($automatic_debit_off != 'Yes') {
                    $type = get_type($membertype);
                    $amount = $type['renewal'];
                    $description = 'Membership fees';
                    $desc_levy = 'Membership Levy';
                    $category = $type['description'];
                    $levy = $type['levy'];
                    if ($category == '') {
                        $category = $membertype;
                    }
                    //include( dirname(__FILE__) . '/new_paymentid.php');
                    $payment_str = add_to_journal($joined, $_POST['id'], 0, $longname, $description, $category, $amount, 'DR', 'SUBS');
                    if ($levy != 0) {
                        $str = add_to_journal($joined, $_POST['id'], 0, $longname, $desc_levy, $category, $levy, 'DR', 'SUBS');
                        $payment_str .= '<br>The Membership Levy of ' . $levy . ' has been added.';
                    }
                    echo '<br>' . $payment_str;
                }

                //$newid = $newid;
                $edit_url = '../../members/update/member_detail.php?id=' . $_POST['id'];

                echo "<br>The record was successfully entered and the ID is:" . $_POST['id'] . "<br><br>";
                echo '<a class="button1_red" href="' . $edit_url . '">View New Member</a>';
                echo '<a class="button1_red" href="../../members/members.php">Member List</a>';
            }
        } else {
            //include( dirname(__FILE__) . '/newidcat.php');


            include( dirname(__FILE__) . '/new_form_approve.php');
        }
        ?>
    </div>
</body>
</html>
<?php

function add_child($id, $childname, $dob, $sex) {

    include( dirname(__FILE__) . '/new_childid.php');
    if ($sex == '') {
        $sex = 'X';
    }

    
    
    

    try {
        $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    } catch (PDOException $e) {
        print "Error! toys : " . $e->getMessage() . "<br/>";
        die();
    }
    $child_sql = 'insert into children (childid, id, child_name, sex, d_o_b) values (?,?,?,?,?);';
    $array = array($newchildid, $id, $childname, $sex, $dob);
    $sth = $pdo->prepare($child_sql);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $error = 'Error' . $stherr[0] . '<br>';
        $error .= 'Error' . $stherr[1] . '<br>';
        $error .= 'Error' . $stherr[2] . '<br>';
    } else {
        $error = $childname . ' was successfully added.<br>';
    }
    return $error;
}

function delete_approved($id) {
    $connection_str = "port=" . $dbport . " dbname=" . $logdbname . " user=" . $logdbuser . " password=" . $logdbpasswd;
    $conn = pg_Connect($connection_str);

    $connection_str = "port=" . $dbport . " dbname=" . $logdbname . " user=" . $logdbuser . " password=" . $logdbpasswd;
    $conn = pg_Connect($connection_str);
    $query = "DELETE from borwrs WHERE id = " . $id . ";";
    $result = pg_Exec($conn, $query);
    if (!$result) {
        $message = 'Invalid query: ' . "\n";
        $message .= 'Whole query: ' . $query;
        $message .= 'There was an error deleting the member from the new member database.';
        //die($message);
        $error = $message;
    } else {
        $error = '';
        //$_SESSION['error'] = '<br>You have successfully deleted a record';
    }
    return $error;
}
