<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
require(dirname(__FILE__) . '/../../mibase_check_login.php');
if (isset($_POST['id'])){
    $id = $_POST['id'];
}else{
    $id=0;
}
?>

<body id="main_body">
    <script>
        $(function () {
            var pickerOpts = {
                dateFormat: "d MM yy",
                showOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "2000:+nn"

            };
            $("#dob1").datepicker(pickerOpts);
            $("#dob2").datepicker(pickerOpts);
            $("#dob3").datepicker(pickerOpts);
        });
    </script>

    <div id="form_container">

        <font size="2" face="Arial, Helvetica, sans-serif"></font>


        <form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'new_approve.php?id=' . $id; ?>">

            <div id="form" style="background-color:lightgray;" align="left">
                <table width="100%"><tr><td>
                            <h2>Member Details:</h2></td>
                        <td align="right"><input id="saveForm" class="button1_red" type="submit" name="submit" value="Save New Member" />


                        </td></tr>

                    <tr><td><table>
                                <tr><td width="35%">Contact 1 - First Name:<br>
                                        <input type="Text" name="firstname" align="LEFT" required="Yes" size="35" value="<?php echo $firstname_new; ?>"></input></td>
                                    <td width="35%">Surname:<br>
                                        <input type="Text" name="surname" align="LEFT" required="Yes" size="35" value="<?php echo $surname_new; ?>"></input><br></td>

                                    <td>Mobile:<br>
                                        <input type="Text" name="mobile1" align="LEFT" required="Yes" size="20" value="<?php echo $mobile1_new; ?>"></input><br></td>
                                <tr><td width="35%">Contact 2 - First Name:<br>
                                        <input type="Text" name="partnersname" align="LEFT" size="35" value="<?php echo $partnersname_new; ?>"></input></td>
                                    <td width="35%">Surname:<br>
                                        <input type="Text" name="partnerssurname" align="LEFT"  size="35" value="<?php echo $partnerssurname_new; ?>"></input><br></td>

                                    <td width="25%">Mobile:<br>
                                        <input type="Text" name="mobile2" align="LEFT"  size="20" value="<?php echo $mobile2_new; ?>"></input><br></td>
                            </table>
                            <h2>Contact Details:</h2>
                            <table>
                                <tr><td width="45%">Address:<br>
                                        <input type="Text" name="address" align="LEFT"  size="35" value="<?php echo $address_new; ?>"></input><br>
                                        <input type="Text" name="address2" align="LEFT"  size="35" value="<?php echo $address2_new; ?>"></input><br>
                                    <td width="40%" align='top'>Suburb / City:<br>
                                        <input type="Text" name="suburb" align="LEFT"  size="35" value="<?php echo $suburb_new; ?>"></input><br>
                                        State: <input type="Text" name="state" align="LEFT"  size="5" value="<?php echo $state_new; ?>"></input></td>

                                    <td  align='top'>Postcode:<br>
                                        <input type="Text" name="postcode" align="LEFT"  size="4" value="<?php echo $postcode_new; ?>"></input><br><br></td></tr>

                                <TR><td><br>Member Type: <br><?php include( dirname(__FILE__) . '/get_memtype.php'); ?></td></tr>
                                <tr><td width="35%">Email:<br>
                                        <input type="Text" name="emailaddress" align="LEFT"  size="35" value="<?php echo $email_new; ?>"></input></td></tr>
                                <tr><td width="35%">Email second contact:<br>
                                        <input type="Text" name="email2" align="LEFT"  size="35" value="<?php echo $email2_new; ?>"></input></td></tr>

                                <tr> <td width="35%">Home Phone:<br>
                                        <input type="Text" name="phone" align="LEFT"  size="35" value="<?php echo $phone_new; ?>"></input><br></td></tr>
                                <tr> <td width="35%">Skills:<br>
                                        <input type="Text" name="skills2" align="LEFT"  size="50" value="<?php echo $skills2_new; ?>"></input><br></td></tr>
                                <tr> <td width="35%">How did you find us?:<br>
                                        <input type="Text" name="discoverytype" align="LEFT"  size="50" value="<?php echo $discovery_new; ?>"></input><br></td></tr>
                                <tr> <td width="35%">Notes:<br>
                                        <input type="Text" name="notes" align="LEFT"  size="50" value="<?php echo $notes_new; ?>"></input><br></td></tr>
                                <tr> <td width="35%">Proof of id (license No):<br>
                                        <input type="Text" name="id_license" align="LEFT"  size="50" value="<?php echo $id_license_new; ?>"></input><br></td></tr>
                                <tr> <td width="35%">Working with Children Card Number:<br>
                                        <input type="Text" name="wwc" align="LEFT"  size="50" value="<?php echo $wwc_new; ?>"></input><br></td></tr>

                                <tr>
                                    <td><br>Roster Pref 1: <br><?php include( dirname(__FILE__) . '/get_rostertype1.php'); ?></td>
                                    <td><br>Roster Pref 2: <br><?php include( dirname(__FILE__) . '/get_rostertype2.php'); ?></td>
                                    <td><br>Nationality: <br><?php include( dirname(__FILE__) . '/get_rostertype3.php'); ?></td></tr>

                                <tr><td><label>Roster Duty Days?: </label><br>
                                        <textarea id="rosternotes" name="rosternotes" rows="2" cols="30"><?php echo $rosternotes; ?></textarea></td></tr>

                                <tr><td valign='top'>Would like to pay the Levy: 
                                        <?php
                                        //echo $levy_new;
                                        //echo '<input type="checkbox" name="levy" id="levy" checked="checked"/>';

                                        if ($levy_new == 't') {
                                            echo '<input type="checkbox" name="levy" id="levy" checked="checked"/>';
                                        } else {
                                            echo '<input type="checkbox" name="levy" id="levy"/>';
                                        }
                                        echo $helmet_new;
                                        ?>
                                    </td></tr>


                            </table>
                            <table width="100%">
                                <tr>
                                    <td valign='top'>Child Name:<br>
                                        <input type="Text" name="childname1" id="childname1" align="LEFT"  size="15" value="<?php echo $childname1; ?>"></input></td>
                                    </td>
                                    <td valign='top'>Gender: <br>
                                        <select id="sex" name="sex1" style="width: 70px;">
                                            <option value='<?php echo $sex1; ?>' selected="selected"><?php echo $sex1; ?></option>
                                            <option value="M" >M</option>
                                            <option value="F" >F</option>
                                            <option value="X" >X</option>
                                        </select></td>    

                                    <td>Date of Birth:<br>
                                        <input type="text" name="dob1" id ="dob1" align="LEFT" value="<?php echo $dob1; ?>"></input><br></td>
                                </tr>
                                <tr>
                                    <td valign='top'>Child Name:<br>
                                        <input type="Text" name="childname2" id="childname2" align="LEFT"  size="15" value="<?php echo $childname2; ?>"></input></td>
                                    </td>
                                    <td valign='top'>Gender: <br>
                                        <select id="sex2" name="sex2" style="width: 70px;">
                                            <option value='<?php echo $sex2; ?>' selected="selected"><?php echo $sex2; ?></option>
                                            <option value="M" >M</option>
                                            <option value="F" >F</option>
                                            <option value="X" >X</option>
                                        </select></td>    
                                    <td>Date of Birth:<br>
                                        <input type="text" name="dob2" id ="dob2" align="LEFT" value="<?php echo $dob2; ?>"></input><br></td>
                                </tr>
                                <tr>
                                    <td valign='top'>Child Name:<br>
                                        <input type="Text" name="childname3" id="childname3" align="LEFT"  size="15" value="<?php echo $childname3; ?>"></input></td>
                                    </td>
                                    <td valign='top'>Gender: <br>
                                        <select id="sex" name="sex3" style="width: 70px;">
                                            <option value='<?php echo $sex3; ?>' selected="selected"><?php echo $sex3; ?></option>
                                            <option value="M" >M</option>
                                            <option value="F" >F</option>
                                            <option value="X" >X</option>
                                        </select></td>    
                                    <td>Date of Birth:<br>
                                        <input type="text" name="dob3" id ="dob3" align="LEFT" value="<?php echo $dob3; ?>"></input><br></td>
                                </tr>
                            </table>
                        </td></tr>
                </table>
                <input type="hidden" name="created" value="<?php echo $created_new; ?>"> 
                <input type="hidden" name="id" value="<?php echo $newmemid; ?>"> 
                <input type="hidden" name="helmet" value="<?php echo $helmet_new; ?>"> 
                <input type="hidden" name="old_id" value="<?php echo $borid_new; ?>"> 

            </div>
        </form>
    </div>
</body>




