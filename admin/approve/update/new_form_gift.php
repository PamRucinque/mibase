<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>

<body id="main_body">
    <script>
        $(function () {
            var pickerOpts = {
                dateFormat: "d MM yy",
                showOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "2000:+nn"

            };
            $("#dob1").datepicker(pickerOpts);
            $("#dob2").datepicker(pickerOpts);
            $("#dob3").datepicker(pickerOpts);
        });
    </script>

    <div id="form_container">

        <font size="2" face="Arial, Helvetica, sans-serif"></font>


        <form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'new_gift.php?id=' . $_POST['id']; ?>">

            <div id="form" style="background-color:lightgray;" align="left">
                <table width="100%"><tr><td>
                            <h2>Gift Card Credit need to be assigned to a Member:</h2></td>
                        <td align="right">

                        </td>
                        <?php
                        if ($email_sent == 'Yes') {
                            echo '<td align="right"><input id="submit" class="button1_red" type="submit" name="submit" value="Add Gift Card Credit to Member Account" /></td>';
                        }
                        ?>


                    </tr>
                </table>

                <table width="100%">
                    <tr><td width="55%">Name:<br>
                            <input type="Text" name="p_name" align="LEFT" required="Yes" size="35" value="<?php echo $p_name_new; ?>"></input></td>
                        <td>Mobile:<br>
                            <input type="Text" name="mobile" align="LEFT" required="Yes" size="20" value="<?php echo $mobile_new; ?>"></input><br></td></tr>
                    <tr><td width="35%">Email:<br>
                            <input type="Text" name="email" align="LEFT" size="35" value="<?php echo $email_new; ?>"></input></td>
                        <?php
                        if ($email_sent == 'Yes') {
                            echo '<td>Credit Member Account:<br>';
                            include( dirname(__FILE__) . '/get_member_select.php');
                            echo '</td>';
                        }
                        ?>
                    </tr>
                    <tr><td colspan="2">Description: <br>
                            <input type="Text" name="description" align="LEFT" required="Yes" size="100" value="<?php echo $description_gift; ?>"></input></td>
                    </tr>

                </table>

                <input type="hidden" name="created" value="<?php echo $created_new; ?>"> 
                <input type="hidden" name="newsletter" value="<?php echo $newsletter; ?>"> 
                <input type="hidden" name="id" value="<?php echo $id_new; ?>"> 


            </div>
        </form>

        <?php
        echo '<strong><font color="green">This Gift Card has been sent by Email to: ' . $email_new . '.</font></strong><br><br>';
        echo '<a class="button1_green" align="right" title="Michelle Baird" href="../gift_approve.php">Back to Gift Cards List</a><br><br>';

        ?>


    </div>
</body>




