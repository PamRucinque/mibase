<?php

require( dirname(__FILE__) . '/../mibase_check_login.php');
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);

include( dirname(__FILE__) . '/connect_public.php');

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    // get the 'id' variable from the URL

    $query = "DELETE FROM gift_cards WHERE id = " . $_GET['id'] . ";";
    $result = pg_exec($conn, $query);

    // Check result
    // This shows the actual query sent to MySQL, and the error. Useful for debugging.
    if (!$result) {
        $message = 'Invalid query: ' . "\n";
        $message = 'Whole query: ' . $query;
        $message = 'There was an error deleting this member, check to see if it has related records in the Members table';
        //die($message);
        echo $message;
        $_SESSION['error'] = $message;
    } else {
        echo $query;
        $_SESSION['error'] = 'You have successfully deleted a record';
    }
    $redirect = "Location: gift_approve.php";               //print $redirect;
    header($redirect);
} else {
// if the 'id' variable isn't set, redirect the user
    //header("Location: claims_info.php");
}
echo $query;
?>
