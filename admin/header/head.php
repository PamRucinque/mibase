<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=EDGE,chrome=1"/>
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" href="../css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link rel="shortcut icon" type="image/x-icon" href="../home/favicon.ico" />
<link rel="stylesheet" href="../css/menu.css" />
<link rel="stylesheet" href="../css/style.css" />
<script src='../js/jquery-3.0.0.js' type='text/javascript'></script> 
<script type="text/javascript" src="../js/tooltip.js"></script>

