<?php
if (!session_id()) {
    session_start();
}
$menu_color = $_SESSION['settings']['menu_color'];
$menu_font_color = $_SESSION['settings']['menu_font_color'];
$online_hire = $_SESSION['settings']['online_hire'];
$online_rego = $_SESSION['settings']['online_rego'];
$libraryname = $_SESSION['settings']['libraryname'];
$logo_height = $_SESSION['settings']['logo_height'];
$library_heading_off = $_SESSION['settings']['library_heading_off'];

//if ($menu_color == '') {
    $menu_color = 'darkblue';
//}
if ($menu_font_color == '') {
    $menu_font_color = "yellow";
}
$heading_color = 'color:' . $menu_color . ';';
$menu_color_bg = 'background:' . $menu_color . ';';
$menu_font_color = 'color:' . $menu_font_color . ';';
$menu_member_options = $_SESSION['settings']['menu_member_options'];
$menu_faq = $_SESSION['settings']['menu_faq'];

//onMouseOver="this.style.background='<?php echo $menu_color; 
//echo $menu_color;
?>
<nav id="nav" style="<?php echo $menu_color_bg; ?>">
    <ul>
        <li><a href="../home/index.php" style="<?php echo $menu_font_color; ?>">Home</a></li>
        <li><a href="../mylibrary/index.php" style="<?php echo $menu_font_color; ?>">My Library</a></li>
               <li><span class="submenu" style="<?php echo $menu_font_color; ?>">Toys</span>
            <ul class="submenu" style="<?php echo $menu_color_bg; ?>">

                <li><a href="../toys/toys.php" style="<?php echo $menu_font_color; ?>">Toys</a></li>
                <li><a href="../toys/popular.php" style="<?php echo $menu_font_color; ?>">Popular Toys</a></li>
                <li><a href="../toys/new.php" style="<?php echo $menu_font_color; ?>">New Toys</a></li>
            </ul>
        </li> 

        <?php
        if ($online_hire == 'Yes') {
            echo '<li><a href="../toys/toys_hire.php" ' . 'style="' . $menu_font_color . '" >Toy Hire</a></li>';
        }

        echo '<li><a href="../roster/index.php" ' . 'style="' . $menu_font_color . '" >Roster</a></li>';

        echo '<li><a href="../password/index.php" ' . 'style="' . $menu_font_color . '" >Reset Password</a></li>';

        echo '<li class="right"><a href="../Logout.php" ' . 'style="' . $menu_font_color . '" >Logout</a></li>';


        //echo '<li class="onmobile"><a href="#">Only On Mobile</a></li>';
        ?>
    </ul>
</nav><!-- end of #nav -->