<?php
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>

<!doctype html>
<html lang="en">
    <head>
        <script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
        <script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
        <link type="text/css" href="../js/themes/base/jquery.ui.all.css" rel="stylesheet" />
        <?php include( dirname(__FILE__) . '/../header.php');
        ?> 

        <script>
            $(function () {
                var pickerOpts = {
                    dateFormat: "yy-mm-dd",
                    showOtherMonths: true

                };
                $("#party").datepicker(pickerOpts);
            });
        </script>
    </head>

    <body>
        <div id="form_container">
            <?php include( dirname(__FILE__) . '/../menu.php'); ?>
            <?php
            include( dirname(__FILE__) . '/../header_detail/header_detail.php');
            $sResults = '';



            if (isset($_POST['reset_2'])) {
                $_SESSION['search_box'] = '';
            }
            if (isset($_POST['search_box'])) {
                $_SESSION['search_box'] = $_POST['search_box'];
            }


            if (isset($_POST['party'])) {
                $_SESSION['party'] = $_POST['party'];
            }


            //$party_date = '2017-05-25';
            //echo 'Hello Gregor' . $_POST['reset_2'] . '*';  

            $total = 0;
            ?>

            <table width=100% height="60px"  bgcolor="lightgray">
                <tr><td width="25%">
                        <form name="search_form" id="search_form" method="post" action="toys_party.php" > 
                            <table width=100%>        
                                <tr>
                                    <td width="30%" colspan="2"><label class="description" for="search">Search String</label><input id="search_box" name="search_box" type="text" maxlength="255" size="30px" value=""  onchange="this.form.submit();"/> </td>
                                </tr>
                            </table>
                        </form>

                    </td>
                    <td>
                        <?php
                        $str_form = '<form name="search_form" id="search_form" method="post" action="toys_party.php" >';
                        echo $str_form;
                        echo '<font color="green">Select Party Date:</font><br><input text-align="left" type="text" size="15" id="party" name="party"  onchange="this.form.submit();" value="' . $_SESSION['party'] . '"/>';
                        //echo "<input class='button' type='submit' name ='reset' id='reset' align='right' value='Clear Filters' onchange='this.form.submit();'>";
                        echo '</form>';
                        ?>
                    </td>


                    <td width="5%"  valign="bottom" ><br>

                        <form name="form_reset" id="form_reset" method="post" action="toys_party.php" > 
                            <input type="submit" class="button1_logout" name ="reset_2" id ="reset_2" value="Reset"/> 
                        </form>

                    </td>

                </tr>
                <tr><td colspan="2">
                        <?php
                        if ($_SESSION['party'] != '') {
                            date_default_timezone_set($_SESSION['timezone']);
                            $party_date = $_SESSION['party'];
                            $day = date('l', strtotime($_SESSION['party']));
                            $format_party = $format_expired = $day . ', ' . substr($party_date, 8, 2) . '-' . substr($party_date, 5, 2) . '-' . substr($party_date, 0, 4);
                            echo '<font color="blue"><h2>Reserve Date: </font>' . $format_party . '</h2>';
                        }
                        ?>

                    </td></tr></table>


            <?php
            include( dirname(__FILE__) . '/search-data_list_party.php');
            ?>

        </div>


    </body>
     <script>
                                        $(document).ready(function () {
                                            $('.dropdown-toggle').dropdown();
                                        });
    </script>

</html>
