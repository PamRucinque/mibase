<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <script  type="text/javascript" src="../js/jquery-2.0.3.min.js"></script>
    </head>
    <?php
    session_start();

//print_r($_SESSION);
    ?>
    <body>
        <div id="form_container">
            <?php include( dirname(__FILE__) . '/../menu.php'); ?>
            <?php
            include( dirname(__FILE__) . '/../header_detail/header_detail.php');
            //include( dirname(__FILE__) . '/../reports/baglabels_toys.php');
            include( dirname(__FILE__) . '/../get_settings.php');
            $idcat = $_SESSION['idcat'];


            if (isset($_POST['reset_2'])) {
                //echo 'Hello Gregor reset2';
                $_SESSION['search'] = '';
            }
            if (isset($_POST['add'])) {
                include( dirname(__FILE__) . '/../connect.php');
                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $query_edit = "UPDATE toys SET 
                    reservecode=? 
                    WHERE idcat=?;";
                //create the array of data to pass into the prepared stament
                $sth = $pdo->prepare($query_edit);
                $array = array($_POST['reserve'], $_POST['toy']);
                $sth->execute($array);
                $stherr = $sth->errorInfo();


                if ($stherr[0] != '00000') {
                    echo "An INSERT query error occurred.\n";
                    //echo $query_edit;
                    echo $connect_pdo;
                    echo 'Error ' . $stherr[0] . '<br>';
                    echo 'Error ' . $stherr[1] . '<br>';
                    echo 'Error ' . $stherr[2] . '<br>';
                    exit;
                }
            }


            //echo 'Hello Gregor' . $_POST['reset_2'] . '*';  

            $total = 0;
            if ($_POST['select_list'] != '') {
                if ($_SESSION['idcat_select'] == '') {
                    $_SESSION['idcat_select'] .= $_POST['select_list'];
                } else {
                    $_SESSION['idcat_select'] .= ',' . $_POST['select_list'];
                }
            }
            ?>
            <table width=100% height="60px"  bgcolor="lightgray">
                <tr><td width="65%">
                        <form name="search_form" id="search_form" method="post" action="reservations.php" > 
                            <table>        
                                <tr>
                                    <td width="30%"><label class="description" for="element_12">Reservation Number: </label><?php include( dirname(__FILE__) . '/get_reservation.php'); ?></td>
                                    <td width="30%"><label class="description" for="assessor">Toy Number: </label><?php include( dirname(__FILE__) . '/get_toy.php'); ?></td>
                                    <td  width="10%" valign="bottom"><input id="add" class="button1_green"  type="submit" name="add" value="Add" /></td>
                                    <td valign="bottom" ><input type="hidden" name="total" value="<?php echo $total; ?>">

                                    </td>
                                </tr>


                            </table>
                        </form>


                    </td>

                    <td width="45%"  valign="top" ><br>

                        <form name="form_reset" id="form_reset" method="post" action="reservations.php" > 
                            <label class="description" for="search">Search String</label><input id="search" name="search" type="text" maxlength="255" size="20" value=""  onchange="this.form.submit();"/>
                            <input type="submit" class="button1_blue" name ="reset_2" id ="reset_2" value="Show All"/> 
                        </form>

                    </td>

                </tr></table>

            <?php
            //if (isset($_POST['submit'])) {
            //include( dirname(__FILE__) . '/../header_detail/header_detail.php');



            include( dirname(__FILE__) . '/result.php');
            // } else {
            //    $location = 'ALL';
            //    $assessor = '';
            //}
            //print 'hello'; }
            ?>

        </div>


    </body>

</html>
