<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
require(dirname(__FILE__) . '/../mibase_check_login.php');

if (isset($_POST['reset'])) {
    $_SESSION['category'] = '';
    $_SESSION['age'] = '';
    $_SESSION['search_box'] = '';
    $_SESSION['idcat_select'] = '';
}



if (isset($_POST['category'])) {
    $_SESSION['category'] = $_POST['category'];
}
if (isset($_POST['age'])) {
    $_SESSION['age'] = $_POST['age'];
}
if (isset($_POST['search'])) {
    $_SESSION['search_box'] = $_POST['search'];
}



$catsort = $_SESSION['catsort'];
$_SESSION['showall'] = 100;

$search = "";
if (isset($_SESSION['search'])) {
    $search = $_SESSION['search'];
    $string = substr($search, 0, 1);
    if ($string == '-') {
        //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
        $search = rtrim(substr($search, 1));
    }
}
$search = $_SESSION['search'];
$search = strtoupper($search);
$search_str = '%' . $search . '%';
$category_str = $_SESSION['category'] . '%';
$age_str = '%' . $_SESSION['age'] . '%';
//error message (not found message)
if ($catsort == 'No') {
    $order_by = 'ORDER BY id ASC, reservecode DESC;';
} else {
    $order_by = 'ORDER BY category, id ASC, reservecode DESC;';
}

$total = 0;
include( dirname(__FILE__) . '/../connect.php');
include( dirname(__FILE__) . '/../get_settings.php');
$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);


$query_toys = "SELECT
     toys.idcat as idcat,
     toys.reservecode as reservecode,
     toys.toyname as toyname,
     toys.status as status,
     toys.category as category,
     toys.no_pieces as no_pieces,
     toys.id as id,
     toys.age as age,
     toys.storage as storage,
     toys.toy_status as toy_status,
     transaction.due as due,
     transaction.borname as borname,
     transaction.phone as transphone,
    (select max(date_end) from reserve_toy where upper(reserve_toy.idcat) = upper(toys.idcat) and status = 'ACTIVE' and date_end > current_date) as reserve_from,
    (select max(date_start) from reserve_toy where reserve_toy.idcat = toys.idcat and status = 'ACTIVE' and date_end > current_date) as reserve_start
FROM
     toys 

     LEFT OUTER JOIN
     (
     SELECT
          transaction.idcat,
          transaction.due,
          transaction.borname,
          transaction.phone
     FROM
          transaction where return is null
     GROUP BY
          transaction.due, transaction.idcat, transaction.borname , transaction.phone

     ) AS transaction ON upper(transaction.idcat) = upper(toys.idcat) 
        where (upper(toyname) LIKE ? 
        OR upper(manufacturer) LIKE ?
        OR upper(storage) LIKE ? 
        OR upper(user1) LIKE ? 
        OR upper(toys.idcat) LIKE ?)
        AND reservecode != '' AND reservecode is not null AND (toy_status = 'ACTIVE' or toy_status = 'PROCESSING') " . $order_by;
//echo $query_toys;

$sth = $pdo->prepare($query_toys);
//$array = array($search_str, $search_str, $search_str, $search_str,$search_str, $category_str, $age_str);
$array = array($search_str, $search_str, $search_str, $search_str, $search_str);

$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

//$query = "SELECT * FROM toys ORDER by id ASC;";


$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");

if (isset($_GET['r'])) {
    $toprint = sizeof($result);
} else {
    $toprint = 150;
}
$total = sizeof($result);
if (sizeof($result) < 150) {
    $toprint = sizeof($result);
    $toprint_txt = " ";
} else {
    $link = '<a class="button_small" href="reservations.php?r=' . $toprint . '">Showall</a>';
    $toprint_txt = $toprint . ' of ' . $total . " shown. " . $link;
}

for ($ri = 0; $ri < $toprint; $ri++) {
    //$row = pg_fetch_array($result, $ri);
    $row = $result[$ri];
    $category = $row["category"];
    $toyname = $row["toyname"];
    $age = $row["age"];
    $reservecode = $row['reservecode'];
    $desc1 = $row["desc1"];
    $comments = $row["comments"];
    $toy_status = $row["toy_status"];
    $cost = $row['cost'];
    $no_pieces = $row['no_pieces'];
    $status = $row["status"];
    $supplier = $row['supplier'];
    $manufacturer = $row['manufacturer'];
    $date_purchase = $row["date_purchase"];
    $borname = $row['borname'];
    $borrower_name = $row['borname'];
    $trans_phone = $row['transphone'];
    $location = $row['location'];
    $reserve_from = $row['reserve_from'];
    //echo date('Y-m-d', strtotime("+30 days"));

    $new_reserve_end = date('Y-m-d', strtotime("+ 7 days", strtotime($reserve_from)));

    //$reserve_start = $row['reserve_start'];
    $reserve_start = substr($row['reserve_start'], 8, 2) . '-' . substr($row['reserve_start'], 5, 2) . '-' . substr($row['reserve_start'], 0, 4);
    $new_reserve_end_format = substr($new_reserve_end, 8, 2) . '-' . substr($new_reserve_end, 5, 2) . '-' . substr($new_reserve_end, 0, 4);
    $reserve_from_format = substr($reserve_from, 8, 2) . '-' . substr($reserve_from, 5, 2) . '-' . substr($reserve_from, 0, 4);


    if ((strpos($borname, '&') !== false)) {
        $borname_small = explode('&', $borname);
    } else {
        if ((strpos($borname, ' and ') !== false)) {
            $borname_small = explode(' and ', $borname);
        } else {
            if ((strpos($borname, 'Expires') !== false)) {
                $borname_small = explode('Expires', $borname);
            } else {

                $borname_small[0] = $borname;
            }
        }
    }


    $reserve_borname = $row['reserve_borname'];
    $phone = $row['phone'];
    $due = $row['due'];

    //$total = $total + 1;
    $format_date_purchase = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    $format_date_reserve = substr($row['end'], 8, 2) . '-' . substr($row['end'], 5, 2) . '-' . substr($row['end'], 0, 4);

    if ($row['due'] != '') {
        $format_date_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);
        $due_end = date('Y-m-d', strtotime("+ 7 days", strtotime($row['due'])));
        $format_due_end = substr($due_end, 8, 2) . '-' . substr($due_end, 5, 2) . '-' . substr($due_end, 0, 4);
 
    } else {
        if ($row['reserve_from'] != '') {
            $format_date_due = substr($row['reserve_from'], 8, 2) . '-' . substr($row['reserve_from'], 5, 2) . '-' . substr($row['reserve_from'], 0, 4);
        } else {

            $format_date_due = '';
        }
    }

    $id = $row["id"];
    $idcat = $row["idcat"];

    $result_txt .= '<tr border="1" class="item" id="red"><td border="1" width="30px">' . $id . '</td>';
    //$result_txt .= '<tr border="1"><td border="1" width="50">' . $id . '</td>';
    //$result_txt .= '<td width="50" align="left">' . $idcat . '</td>';
    $result_txt .= '<td class="idcat" width="50" align="center">' . $reservecode . '</td>';

    $result_txt .= '<td class="idcat" width="50" align="center">' . $idcat . '</td>';

    $result_txt .= '<td width="20px" align="center">' . $category . '</td>';
    $result_txt .= '<td width="20px" align="center">' . $location . '</td>';

    $result_txt .= '<td width="320px" align="left">' . $toyname . '</td>';
    $file_pic = 'http://' . $_SERVER["SERVER_NAME"] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    //$result_txt .= $file_pic;
    $file_pic = '../..' . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';

    if (file_exists($file_pic)) {
        $pic = "'../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . ".jpg'";
    } else {
        $pic = "'../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . "blank.jpg'";
    }
    //$pic = "'../../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . ".jpg'";
    if ($reservecode == $idcat) {
        $result_txt .= '<td width="50" align="center"><a href="../reserves/reservation.php?idcat=' . $idcat . '" onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();" class="button_small"/>Bookings</a></td>';
    } else {
        $result_txt .= '<td width="50" align="center"></td>';
    }
    //$img = '<img height="200px" src="../..' . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg" alt="toy image">';
    if ($overdue_txt == '') {
        $overdue_txt = 'OVERDUE';
    }
    if ($due != '') {
        $loan_status = '<td BGCOLOR=lightgreen width="80" align="center">ON LOAN</td>';
        if ($row['due'] < $today) {
            $loan_status = '<td BGCOLOR=pink width="80" align="center">' . $overdue_txt . '</td>';
        }
    } else {
        if ($toy_status == 'PROCESSING') {
            $loan_status = '<td BGCOLOR=yellow width="80" align="center">PROCESSING</td>';
        } else {
            if ($reserve_from != '') {
                $loan_status = '<td width="80" BGCOLOR=lightblue align="center">RESERVED</td>';
            } else {
                $loan_status = '<td width="80" align="center">IN LIBRARY</td>';
            }
        }
    }

    $result_txt .= $loan_status;
    //$result_txt .= '<td width="130" align="left">'. $supplier . '</td>';
    if ($status == 't') {
        if ($show_phone == 'Yes') {
            //$result_txt .= '<td align="left">' . $borname . ': ' . $trans_phone . '</td>';
        } else {
            //$result_txt .= '<td align="left">' . $borname_small[0] . '</td>';
        }
        $result_txt .= '<td width="70px" colspan="2">' . $format_date_due . ' - ' . $format_due_end .'</td>';
    } else {
        if ($row['end'] != '') {
            $result_txt .= '<td align="left">IN LIBRARY</td>';
        } else {
            if ($reserve_from != '') {
                $result_txt .= '<td align="left" colspan="2"><font color="blue">' . $reserve_from_format . '</font> - <font color="blue">' . $new_reserve_end_format . '</font></td>';
            } else {
                $result_txt .= '<td align="left" colspan="2"></td>';
            }
        }
    }

    //$result_txt .= '<td width="70px">' . $format_date_due . '</td>';

    //$result_txt .= '<td align="left" width="50px">' . $row['storage'] . '</td>';
    //$result_txt .= '<td width="90" align="left">' . $file_pic . '</td>';
    //$result_txt .= '<td width="70">' . $format_date_purchase . '</td>';

    $result_txt .= '<td width="10px" align="center">' . $no_pieces . '</td>';
    $ref2 = 'delete_reserve.php?reservecode=' . $row['idcat'];
    $result_txt .= "<td width='50'><a class ='button_small_red' href='" . $ref2 . "'>Delete</a></td>";
}
$result_txt .= '</tr></table>';
$result_txt .= $toprint_txt;
$search_str = '';

if ($_SESSION['search'] != '') {
    $search_str .= $search_str . ' Filtered on String = ' . $_SESSION['search'] . ' ';
}
$selected_toys = str_replace("'", " ", $_SESSION['idcat_select'], $count);


print '<div id="open"><table width="100%"><tr><td width= 50%><h1 align="left">' . $search_str . '</h1></td>';
print '<td><h1 align="right">Total: ' . $total . '</h1></td></tr>';
if ($_SESSION['idcat_select'] != '') {
    print '<tr><td width="90%"><font color="blue"><strong>' . $count / 2 . ' toys have been selected: ' . $selected_toys . '</strong></font></td></tr>';
}

print '</table></div>';

print '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey;">';
print '<tr style="color:green"><td>ID</td><td>Reserve Code</td><td>idcat</td><td>Cat</td><td>Branch</td><td>Toyname</td><td>Picture</td><td>Status</td><td colspan="2">Next Reservation</td><td>Storage</td><td>No</td><tr>';

print $result_txt;
?>