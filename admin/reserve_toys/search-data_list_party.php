<?php

require( dirname(__FILE__) . '/../mibase_check_login.php');

if (!session_id()) {
    session_start();
}

$conn = pg_connect($_SESSION['connect_str']);

$party_date = $_SESSION['party'];
$param = '';
$sResults = '';
$sql = "Select toys.idcat as idcat, toyname, toys.id, toys.category, age, toys.no_pieces as no_pieces, manufacturer, reservecode,
transaction.due as due, toys.rent as rent, 
(select count(id) from reserve_toy where toys.idcat =  reserve_toy.idcat and ? >= date_start and  ? <= date_end) as reserved,
(select TO_CHAR(date_start, 'Day dd-Mon-YYYY') as next from reserve_toy 
where date_start >= current_date and status = 'ACTIVE' and toys.idcat = reserve_toy.idcat  order by date_start LIMIT 1) as next 
from toys 
LEFT JOIN transaction ON (toys.idcat = transaction.idcat and return is null) ";


$sql .= "where toy_status = 'ACTIVE' ";
$sql .= "and (toys.reservecode is not null and toys.reservecode != '') ";

$sql_where = "and (upper(toyname) LIKE ? "
        . "or upper(toys.idcat) LIKE ? "
        . ") order by category, id ;";



$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
//$_POST['search_form'] = clean_search($_GET['search_form']);
$param = '%' . strtoupper($_SESSION['search_box']) . '%';

//$party_date = '2016-04-14';
$sql = $sql . $sql_where;
//echo $sql;
//echo '<br>' . $param;
if ($_SESSION['library_code'] == 'demo') {
    $sResults .= '<font color="red">Hire Toys / Reservations are not enabled for the Demo login</font>';
}

$array = array($party_date, $party_date, $param, $param);

$sth = $pdo->prepare($sql);
//$array = array($search_str, $search_str, $search_str, $search_str,$search_str, $category_str, $age_str);
//$category_str = strtoupper($category_str);
//$array = array($category_str, $param, $param);
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

if ($stherr[0] != '00000') {
    $_SESSION['error'] = "An  error occurred.\n";
    $_SESSION['error'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[2] . '<br>';
}
$c = 0;
$n = 5;
$sResults .= '<table>';
$sResults .= '<tr><td>id</td><td>Toy Name</td><td>Cat</td><td>Status</td><td></td><td>Manufacturer</td><td>Rent</td><td>Next Hire</td></tr>';
$sResults .= '<tr>';
for ($ri = 0; $ri < $numrows; $ri++) {

    $row = $result[$ri];
    if ($c % $n == 0 && $c != 0) { // If $c is divisible by $n...
        // New table row
        $sResults .= '</tr><tr>';
    }
    $c++;

    $idcat = $row['idcat'];
    $rent = sprintf('%01.2f', $row['rent']);


    $server = $_SERVER["SERVER_NAME"];
    $length = strlen($server) - 14;
    // previous code
    $file_pic = '../..' . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';

    if (file_exists($file_pic)) {
        $pic = "'../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . ".jpg'";
    } else {
        $pic = "'../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . "blank.jpg'";
    }


    $link = 'index.php?v=1&idcat=' . $idcat;
    $onclick = 'javascript:location.href="' . $link . '"';
    $format_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);


    if (file_exists($file_pic)) {
        if ($_SESSION['library_code'] == 'demo') {
            //$pic = "'../../toybase/" . strtolower($idcat) . ".jpg" . "'";
            // $img = ' onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();';
        } else {
            //$pic = "'../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . ".jpg" . "'";
            //$img = ' onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();';
        }
    } else {
        $img = '';
        $pic = '';
    }
    //$pic = "'../.." . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . ".jpg" . "'";
    // $img = ' onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();';
    //$img = '';
    //$pic = '';

    $link = '../reserves/reservation.php?idcat=' . $idcat;
    $onclick = 'javascript:location.href="' . $link . '"';

    if ($row['reserved'] == 1) {
        $sResults .= "<tr  id='red' style='background-color:lightblue' onclick='" . $onclick . "' >";
        $status = 'RESERVED';
    } else {
        if ($row['due']) {
            $sResults .= "<tr  id='red' style='background-color:#F3F781' onclick='" . $onclick . "' >";
            $status = 'ON LOAN';
            if ($party_date != '') {
                $status = $party_date;
                $status = 'ON LOAN';
            }
        } else {

            $sResults .= "<tr  id='red' onclick='" . $onclick . "' >";
            $status = '';
        }
    }



    $sResults .= '<td width="70px">' . $row['idcat'] . '</td>';


    $sResults .= '<td width="350px">' . $row['toyname'] . '</td>';
    if ($row['reservecode'] == '') {
        $sResults .= '<td width="50px">' . $row['category'] . '</td>';
    } else {
        if ($reservations == 'Yes') {
            $sResults .= '<td width="50px">' . $row['category'] . '<font color="blue"> R</font></td>';
        } else {
            $sResults .= '<td width="50px">' . $row['category'] . '</td>';
        }
    }
    //$sResults .= '<td>' . $row['category'] . '</td>';


    $sResults .= '<td width="70px">' . $status . '</td>';
    $sResults .= '<td width="60px" align="center"><a id="button" href="../reserves/reservation.php?idcat=' . $idcat . '" '
            . 'onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();" '
            . 'class="button_small_yellow">Hire Toy</a></td>';

    $sResults .= '<td width="150px" style="padding-left: 5px;">' . $row['manufacturer'] . '</td>';
    $sResults .= '<td width="40px" align="center" style="padding-right: 5px;">' . $rent . '</td>';

    if ($row['due'] != null) {
        $sResults .= '<td  width="160px">Due: ' . $format_due . '</td>';
    } else {
        $sResults .= '<td  width="160px">' . $row['next'] . '</td>';
    }


    //$sResults .= '<td width="100px">' . $row['age'] . '</td></tr>';
    //$sResults .= '<td width="120px" style="padding-left:20px;">' . $row['idcat'] . ':' . $row['toyname'] . '<br>' . $img . '</td>';
    //<img height="50px" onmouseout="hidetrail();" onmouseover="showtrail(350,440, '../../mla_files/MLA1501003/A ID.jpg');" src="../../mla_files/MLA1501003/A ID.jpg">
}
$sResults .= '<tr><td colspan="4" align="right"></td><td><font color="red">Total: ' . $numrows . ' </font></td></tr>';
$sResults .= '</table>';
//$_SESSION['total'] = $numrows;
//$stotal = $numrows;
echo $sResults . $party_date;
//echo $sheader_left;
$total = 'Total: ' . $numrows . '<br>';
//echo $sql;

//$total .= $sql;




