<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>

<link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

<script type="text/javascript">
    $(function() {
        var pickerOpts = {
            dateFormat: "yy-m-d",
            showOtherMonths: true

        };
        $("#due").datepicker(pickerOpts);
    });
    function myFunction()
    {
        alert("This Due has been saved!");
    }

</script>

<p><font size="2" face="Arial, Helvetica, sans-serif"></font></p>

<font></font>
<?php
echo "<br><a href='../loan.php' class ='btn btn-primary'>Back to Loans</a>";
$id = 0;
if (isset($_POST['id'])){
    $id = $_POST['id'];
}
?>
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'edit_trans.php?id=' . $id; ?>">
    <br>
    <div id="form" style="background-color:whitesmoke;" align="left">

        <br><table align="top">
            <tr>
                <td><br>Select New Due Date:<br>
                    <input type="text" name="due" id ="due" align="LEFT" value="<?php echo $due; ?>"></input></td>
             </tr>
            <tr><td><br><input id="saveForm" class="btn btn-success"  type="submit" name="submit" value="Save" /></td></tr>
            <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"> 
        </table>
    </div>
</form></p>

