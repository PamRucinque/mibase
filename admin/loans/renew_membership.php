<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

include( dirname(__FILE__) . '/data/get_member.php');
//include( dirname(__FILE__) . '/../get_settings.php');
$_SESSION['loan_status'] = '';

    date_default_timezone_set($_SESSION['settings']['timezone']);


$now = date('Y-m-d H:i:s');

$today = date('Y-m-d');
$membertype = $_POST['membertype'];
if ($_POST['expired'] == '') {
    $oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " + 365 day"));
} else {
    $expired = date_expired($membertype, $_POST['expired']);
}

$renewed = $today;

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$query_renew = "UPDATE borwrs SET
                    renewed =?,
                    expired =?,
                    modified =?     
                    WHERE id=?;";

//create the array of data to pass into the prepared stament
$sth = $pdo->prepare($query_renew);
$array = array($renewed, $expired,
    $now, $_POST['renew_borid']);
$sth->execute($array);
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $_SESSION['loan_status'] = "An UPDATE query error occurred.\n";
    $_SESSION['loan_status'] .= $query_renew;
    $_SESSION['loan_status'] .= $connect_pdo;
} else {


    $type = get_type($membertype);
    $amount = $type['renewal'];
    $description = 'Membership fees';
    $category = $type['description'];
    if ($category == '') {
        $category = $membertype;
    }
    $desc_levy = 'Membership Levy';
    $levy = $type['levy'];

    $longname = $firstname . ' ' . $surname;

    $edit_url = 'member_detail.php?borid=' . $_POST['renew_borid'];
    $_SESSION['borid'] = $_POST['renew_borid'];

    //$_SESSION['loan_status'] .= "This member has been renewed and the ID is:" . $_POST['renew_borid'] . "<br>";
    $_SESSION['loan_status'] .= "<font color='blue'>The Expired Date has been updated to: " . $expired . "<br></font>";
    //$_SESSION['loan_status'] .= "<font color='red'>" . $msg_blankdate . "</font><br>";
    if ($automatic_debit_off != 'Yes') {
        //include( dirname(__FILE__) . '/new_paymentid.php');
        $str_alert = $_SESSION['loan_status'];
        $str_alert .= "<font color='green'>" . add_to_journal($today, $_POST['renew_borid'], 0, $longname, $description, $category, $amount, 'DR', 'SUBS');
        //$_SESSION['loan_status'] .= $payment_str;
        if ($levy != 0) {
            $str = add_to_journal($today, $_POST['renew_borid'], 0, $longname, $desc_levy, $category, $levy, 'DR', 'SUBS');
            $str_alert .= '<br>The Membership Levy of ' . $levy . ' has been added.<br></font>';
        }

        if (trim($str_alert) != '') {
            include( dirname(__FILE__) . '/data/overlay.php');
        }
        $str_alert = '';

        //$_SESSION['loan_status'] .= "<br><font color='blue'>The membership fee has been debited.<br>";
    } else {
        $_SESSION['loan_status'] .= "<br><font color='blue'><br>";
    }
    //$_SESSION['loan_status'] .= "<font color='red'>" . $_POST['membertype'] . "</font><br>";
    //$_SESSION['loan_status'] .= '<a class="button1" href="../update/member_detail.php?borid=' . $_SESSION['borid'] . '">OK </a>';
}

function date_expired($membertype, $expire) {

        date_default_timezone_set($_SESSION['settings']['timezone']);


    $sql = "SELECT expiryperiod AS expires, due FROM membertype 
                WHERE (membertype)='" . $membertype . "'";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $expired_months = $row['expires'];
    $due = $row['due'];
    // $expired_months = 12;
    $str_month = "+" . $expired_months . " months";
    //$str_month = "+ 3 months";
    $expires = strtotime(date("Y-m-d", strtotime($expire)) . $str_month);
    //$expires = $joined + $expire_months;
    $expires_str = date("Y-m-d", $expires);
      if ($due != ''){
       $expires_str = $due; 
    }

    return $expires_str;
}

function add_to_journal($datepaid, $bcode, $icode, $name, $description, $category, $amount, $type, $typepayment) {

    //include( dirname(__FILE__) . '/../connect.php');
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $query_payment = "INSERT INTO journal (datepaid, bcode, icode, name, description, category, amount, type, typepayment)
                 VALUES (?,?,?,?,?,?,?,?,?);";

    $sth = $pdo->prepare($query_payment);
    $array = array($datepaid, $bcode, $icode, $name, $description, $category, $amount,
        $type, $typepayment);

    //$result_payment = pg_Exec($conn, $query_payment);
    if ($amount > 0) {
        $sth->execute($array);
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {

            $result = 'Error' . $stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>' . $query_payment;
            exit;
        } else {
            $result = 'The Membership fee of ' . $amount . ' has been added';
        }
    } else {
        $result = 'Amount needs to be greater than 0';
    }

    return $result;
}

function get_type($membertype) {
    //include( dirname(__FILE__) . '/../connect.php');
    $sql = "SELECT * FROM membertype 
          WHERE (membertype)='" . $membertype . "'";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $renewal_fee = $row['renewal_fee'];
    $description = $row['description'];
    $levy = $row['bond'];
    return array('renewal' => $renewal_fee, 'description' => $description, 'levy' => $levy);
}
