<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */

//included in loans/loans.php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>

<script>
    $(function () {

        //autocomplete
        $(".auto2").autocomplete({
            source: "data/search_mem.php",
            autoFocus: true,
            select: function (event, ui) {

                var selectedObj = ui.item;

                document.getElementById('borid').value = selectedObj.value
                document.forms["change"].submit();
            }

        });

    });

    function login() {
        if (event.keyCode == 13) {
            alert("You hit enter!");
        }
    }

</script>
<div class="row" style="padding-top: 5px;">
    <div class="col-sm-4">
        <form  id="scan_mem" method="post" action="" width="100%">
            <input align="center" type="text"   placeholder="Mem ID" name="memberid" id ="memberid" style="max-width: 90px; background-color: lightgoldenrodyellow;" value="" onchange='this.form.submit()'></input>
        </form>      
    </div>
    <div class="col-sm-8">
        <form id="change" method="post" action="loan.php">

            <input type='text' style="background-color: lightgoldenrodyellow;" name='str' id='str' value='' class='auto2' onKeyPress="login()" placeholder="Search Member"></p>
            <input type="hidden" id="borid" name ="borid" />
        </form>
    </div>
</div>
