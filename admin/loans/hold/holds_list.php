<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <body>
        <div id="form_container">
            <?php include( dirname(__FILE__) . '/../../menu.php'); ?>
            <section class="container-fluid" style="padding: 10px;">
                <div class="row">
                    <div class="col-sm-9">
                        <h2>Holds - Under construction</h2><br><br>

                    </div>
                    <div class="col-sm-2">
                        <br><a href='../../home/index.php' class ='btn btn-info'>Back to Home</a><br>
                    </div>
                    <div class="col-sm-1">
                        <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=toysa' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                    </div>
                </div>
            </section>
        </div>
        <script type="text/javascript" src="../js/jquery-3.0.0.js" charset="UTF-8"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    </body>
</html>