<script>
    function overlay() {
        if ($("#overlay").is(":visible")) {
            $("#overlay").hide();
        }
    }
</script>

<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');

$special_return = $_SESSION['settings']['special_return'];
$recordfine = $_SESSION['settings']['recordfine'];
$chargerent = $_SESSION['settings']['chargerent'];
$weekly_fine = $_SESSION['settings']['weekly_fine'];
$fine_value = $_SESSION['settings']['fine_value'];
$grace = $_SESSION['settings']['grace'];
$max_daily_fine = $_SESSION['settings']['max_daily_fine'];
$unlock_returns = $_SESSION['settings']['unlock_returns'];
$timezone = $_SESSION['settings']['timezone'];
$rentasfine = $_SESSION['settings']['rentasfine'];



include( dirname(__FILE__) . '/functions/functions.php');
include( dirname(__FILE__) . '/functions/overdue.php');
$_SESSION['loan_status'] = ' ';
if ($special_return == 'Yes') {
    if ($_SESSION['shared_server'] == 'Yes'){
       $str_function = 'special/' . $_SESSION['library_code'] . '_return.php'; 
    }else{
        $str_function = 'special/special_return.php'; 
    }
    
    //include( dirname(__FILE__) . '/special/get_settings.php');
    include($str_function);
    //echo 'hello';
}



if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $_SESSION['return_alert'] = '';


    //$weekly_fine = 'Yes';
    $returntoy = return_toy($id, $recordfine, $chargerent, $weekly_fine, $fine_value, $grace, $rentasfine, $max_daily_fine, $unlock_returns);
    //$_SESSION['loan_status'] = $unlock_returns;
    $_SESSION['loan_status'] = '<font color="green"> ' . $returntoy['status'] . $returntoy['status_due'] . '</font><br>';
    //$_SESSION['loan_status'] .= $unlock_returns;
    if ($returntoy['status_fine'] != '') {

        $_SESSION['loan_status'] .= '<font color="red"> ' . $returntoy['status_fine'] . '</font><br>';
        $_SESSION['missing'] .= '<font color="blue"> ' . $returntoy['status_fine'] . '</font><br>';
    }
    if ($returntoy['alert'] != '') {
        $_SESSION['missing'] .= '<font color="red"> ' . $returntoy['idcat'] . ':  </font>' . '<font color="green"> ' . $returntoy['alert'] . '</font><br>';
    }    
    if (($reservations == 'Yes') && ($reservation_alert == 'Yes')) {
        $reserve = check_reserve($returntoy['idcat']);
        if ($reserve['borid'] > 0){
            $_SESSION['missing'] .= '<h2>There are reservations on this toy, please set <font color="blue">' . $returntoy['idcat'] .  ' : ' . $reserve['toyname'] . '</font> aside.</h2>'; 
        }
       
    }
    

    $_SESSION['idcat'] = $returntoy['idcat'];
    //$redirect = "Location: loan.php?t=" . $returntoy['idcat'];
    $redirect = "Location: loan.php";
    header($redirect);
}

function check_reserve($idcat) {
    $reserve_memid = 0;
    $conn = pg_connect($_SESSION['connect_str']);
    $query = "select reserve_toy.*,
            toys.toyname
            from reserve_toy 
            left join toys on (toys.idcat = reserve_toy.idcat) WHERE "
            . "reserve_toy.idcat = '" . $idcat . "' and date_end >= current_date order by date_start;";
    $numrows = 0;
    $result1 = pg_Exec($conn, $query);
    $numrows = pg_numrows($result1);
    if ($numrows > 0) {
        $row = pg_fetch_array($result1, 0);
        $reserve_memid = $row['member_id'];
        $toyname = $row['toyname'];
    }
      return array('borid' => $reserve_memid, 'toyname' => $toyname, 'sql' => $query);

}

//echo $query_return;
?>
