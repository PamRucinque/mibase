<?php

require( dirname(__FILE__) . '/../mibase_check_login.php');


$_SESSION['loan_status'] = '';
$_SESSION['error'] = '';
$borid = $_SESSION['borid'];
$loan_restrictions = $_SESSION['settings']['loan_restrictions'];
$special_loan = $_SESSION['settings']['special_loan'];
$stocktake_override = $_SESSION['settings']['stocktake_override'];
$loanperiod = $_SESSION['settings']['loanperiod'];
$set_due_date = $_SESSION['settings']['set_due_date'];
if ($set_due_date == 'Yes') {
    if (isset($_SESSION['due'])) {
        $set_due = $_SESSION['due'];
    }
}
$set_loan_date = $_SESSION['settings']['set_loan_date'];;

$last_idcat = $_SESSION['idcat'];
include( dirname(__FILE__) . '/data/get_toy.php');
include( dirname(__FILE__) . '/data/get_member.php');
//include( dirname(__FILE__) . '/data/get_settings.php');
include( dirname(__FILE__) . '/data/get_reserve.php');
include( dirname(__FILE__) . '/functions/functions.php');

if ($toy_holds == 'Yes') {
    include( dirname(__FILE__) . '/hold/get_toy_holds.php');
} else {
    $count_toy_holds = 0;
}
$ok = 'Yes';
$extra_toys = 'No';


if (($toy_exists == 'No')) {
    $_SESSION['loan_status'] .= '<br> Error Adding Record, this toy does not exist.';
    $_SESSION['error'] .= 'Error Adding Record, this toy ' . $_POST['scanid'] . ' does not exist.';
    $_SESSION['idcat'] = $last_idcat;
    $ok = 'No';
} else {
    if ($toy_status != 'ACTIVE') {
        if ($toy_status == 'PROCESSING') {
            $ok = 'Yes';
        } else {
            $_SESSION['loan_status'] .= '<br> Error Adding Record, This toy is Locked.';
            $_SESSION['error'] .= '<br> Error Adding Record, This toy is Locked or Withdrawn.';
            $ok = 'No';
        }
    }


    if ($stocktake_status == 'LOCKED') {
        if ($stocktake_override != 'Yes') {
            $_SESSION['loan_status'] .= '<br> Error Adding Record, This toy is Locked for Stock Take.';
            $_SESSION['error'] .= '<br> Error Adding Record, This toy is Locked.';
            $ok = 'No';
        }
    }
}

if ((($idcat == null) || ($borid == Null)) && ($ok == 'Yes')) {
    $_SESSION['loan_status'] .= '<br> Error Adding Record, please make sure you have selected a Toy and a member.';
    $_SESSION['error'] .= 'Error Adding Record, please make sure you have selected a Toy and a member.';
    $ok = 'No';
}


$longname = clean($longname);
//$r_desc = clean($r_desc);
$reserve_borname = clean($reserve_borname);
$toyname = clean($toyname);
$borname = clean($borname);
$location = $_SESSION['location'];


if ($holiday_override != 'Yes') {
    if ($member_returnperiod != 0) {
        $loanperiod = $member_returnperiod;
    }
}

if ($toy_loanperiod != 0) {
    if ($loanperiod > $toy_loanperiod) {
        if ($holiday_override != 'Yes') {
            if ($member_returnperiod != 0) {
                $loanperiod = $member_returnperiod;
            }
        }
    }
}
//$_SESSION['loan_status'] .= 'Toy ' . $toy_loanperiod . ' mem ' . $member_returnperiod . ' loan ' . $loanperiod;
if (($reservecode != '') && ($loanperiod > $toy_loanperiod)) {
    $loanperiod = $toy_loanperiod;
}
//if (($reservecode != '') && ($holiday_override != 'Yes')) {
//    $loanperiod = $toy_loanperiod;
//}



$date_start = date('Y-m-d');

$query_loan = '';

// if reservations setting = Yes then check for reservations

$checktrans = check_trans($idcat);
if ($checktrans > 0) {
    $ok = 'No';
    $_SESSION['loan_status'] = ' This Toy is on loan to ' . get_borid($idcat);
    $_SESSION['error'] = 'This toy is on loan';
}
$xx = $ok;
$max = max_toys($membertype);

if (($member_gold_star > 0) && (trim($loan_type) == 'Gold Star')) {
    //$_SESSION['loan_status'] .= 'Gold star ' . $member_gold_star . ' loan type: ' . $loan_type . ' Override: ' . $_SESSION['override'];
    $gold_star_status = count_gold_star($borid, $member_gold_star, $_SESSION['override']);
    if ($gold_star_status['ok'] != 'ok') {
        //$_SESSION['loan_status'] .= $gold_star_status['msg'];
        $ok = 'No';
    }
    $_SESSION['error'] .= $gold_star_status['msg'];
}



if ($loan_restrictions == 'Yes') {
    if ($loan_special == 'Yes') {
        $toys_onloan = loan_special($borid, $idcat, $_SESSION['library_code'], $category, $expired);
        if ($toys_onloan['visits'] != 'ok') {
            $_SESSION['error'] .= $toys_onloan['visits'];
            $ok = 'No';
        }
    } else {
        $toys_onloan = count_by_cat($borid, $idcat, $weight, $roster_today);
        if ((trim($toys_onloan['gold_ok']) == 'No') && ($_SESSION['override'] == 'No')) {
            $ok = 'No';
            $_SESSION['error'] .= $toys_onloan['status'] . '<br> Turn override ON to loan this toy.';
        }
    }
} else {
    $toys_onloan = count_loans($borid);
}


if (($toys_onloan['total'] > $max) && ($ok == 'Yes')) {
    if ($toys_onloan['loan_type'] == 'Special') {
        $_SESSION['override'] = 'Yes';
    }

    //$extra_toys = 'Yes';
    $_SESSION['loan_status'] .= ' To loan, activate the overide function. <font color="red"> Too many toys.</font><br>';
    //$_SESSION['loan_status'] .= '<br>' .$toys_onloan['status'];
    if ($special_loan == 'Yes') {
        //$_SESSION['error'] = 'This member has reached the max number of toys.</font> <br>';
    } else {
        $_SESSION['error'] = 'This member has reached the max number of toys. To loan, activate the overide function.</font> <br>';
    }
    $ok = 'No';
    If ($_SESSION['override'] == 'Yes') {
        $_SESSION['loan_status'] .= ' Max Allowed Toys: ' . $max . '. OVERRIDE IS ON, this Toy has been Borrowed<br>';
        $_SESSION['error'] .= 'This member has reached the max number of toys. OVERRIDE is On.';
        $ok = 'Yes';
    }
}

if (($special_loan == 'Yes') && ($ok == 'Yes')) {
    //include( dirname(__FILE__) . '/special/get_settings.php');
    if ($_SESSION['shared_server'] == 'Yes') {
        $str_function = trim('special/' . $_SESSION['library_code'] . '.php');
    } else {
        $str_function = trim('special/special.php');
    }

    //echo $str_function;
    include($str_function);
    //$visits = count_visits($expired, $member_expiryperiod, $borid);
}
if (($holiday_override == 'Yes') && ($holiday_due != '')) {
    $due = Date('Y-m-d', strtotime($holiday_due));
} else {
    $return_str = '+' . $loanperiod . ' days';
    //$_SESSION['loan_status'] .= ' ' . $return_str;
    $due = Date('Y-m-d', strtotime($return_str));
}
if (($member_due != Null) && ($member_due != '') && ($holiday_override == 'Yes')) {
    $due = $member_due;
}
if ($set_due_date == 'Yes') {
    if ($_SESSION['due'] != ''){
        $due = $_SESSION['due'];
    }
}
if ($set_loan_date == 'Yes') {
    if ($_SESSION['loan'] != ''){
        $loan = $_SESSION['loan'];
    }
}

include( dirname(__FILE__) . '/check_reservation.php');
if (isset($_GET['rid'])) {
    $rid = $_GET['rid'];
    include( dirname(__FILE__) . '/data/get_reserve_fromid.php');
    if ($rend != '') {
        $due = $rend;
    }

    $query_loan .= "UPDATE reserve_toy SET status = 'LOANED'  WHERE id = " . $rid . ";";
}
include( dirname(__FILE__) . '/check_today.php');

$query_loan .= "UPDATE toys SET status = TRUE, toy_status = 'ACTIVE' WHERE idcat = '" . strtoupper($idcat) . "';";
// check record fines


include( dirname(__FILE__) . '/charge_rent.php');
if ($toy_holds == 'Yes') {
    $query_loan .= "UPDATE toy_holds SET status = 'LOANED' where idcat = '" . strtoupper($idcat) . "' AND borid=" . $borid . ";";
}

if ($ok == 'Yes') {

    //include( dirname(__FILE__) . '/../connect.php');
    $result_loan = pg_exec($conn, $query_loan);
    //$r = pg_last_error();
    //$_SESSION['loan_status'] .= $query_loan;
    // Check result
    // This shows the actual query sent to MySQL, and the error. Useful for debugging.
    if (!$result_loan) {
        $message = 'Invalid query: ' . "\n";
        $message .= 'Whole query: ' . $query_loan;
        $_SESSION['loan_status'] .= 'There was an error adding this loan. ' . $message;
    } else {
        $_SESSION['loan_status'] .= $idcat . ' ' . $toyname . ' has been succesfully added. ';
        $_SESSION['loan_status'] .= $rent_status;
        //$_SESSION['loan_status'] .= '<br>' . $query_loan;
    }
    $redirect = "Location: loan.php";
} else {
    
}
$error_out = $_SESSION['error'];

//echo '<p><font color="red">' . $alert_mem . '</font></p>';
//$_SESSION['loan_status'] .= '<br>Error Adding Record, please make sure you have selected a Toy and a member.';
?>
