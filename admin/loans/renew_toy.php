<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');

$renew_loan_period = $_SESSION['settings']['renew_loan_period'];
$times_renew = $_SESSION['settings']['times_renew'];
$chargerent = $_SESSION['settings']['chargerent'];
$global_rent = $_SESSION['settings']['global_rent'];
$holiday_override = $_SESSION['settings']['holiday_override'];
$holiday_due = $_SESSION['settings']['holiday_due'];



$_SESSION['loan_status'] = ' ';
$ok = 'Yes';

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $conn = pg_connect($_SESSION['connect_str']);
    include( dirname(__FILE__) . '/functions/functions.php');
    include( dirname(__FILE__) . '/data/get_member.php');
    if ($member_returnperiod != 0) {
        $loanperiod = $member_returnperiod;
    }

    $loanperiod = $renew_loan_period;


    $_SESSION['loan_status'] = ' ';
    $alert = $loanperiod;

    $renewtoy = renew_toy($id, $loanperiod, $times_renew, $chargerent, $global_rent, $holiday_override, $holiday_due);

    $_SESSION['loan_status'] .= $renewtoy['status'] .   ' Renew period: ' .  $renew_loan_period . '<br>';
    if ($renewtoy['alert'] != '') {
        $alert .= $renewtoy['alert'] . '\n';
    }

    if ($alert != '') {
        echo '<script>
        $(document).ready(function(){
		alert("' . $alert . '");});
        </script>';
    }
    $_SESSION['idcat'] = $renewtoy['idcat'];
    $redirect = "Location: loan.php?t=" . $renewtoy['idcat'];
    header($redirect);
}
?>
