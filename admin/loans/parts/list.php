<?php
include(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script>
    function update_alert(str) {
        divid = "partdiv_" + str;
        btn_id = "partbtn_" + str;
        desc_id = "desc_" + str;
        id = str;

        var btn_str = $('#partbtn_' + str).text();
        //alert(role);
        if (btn_str === 'Remove Alert') {
            btn_new = 'Set Alert';
            status = 'No';
        } else {
            btn_new = 'Remove Alert';
            status = 'Yes';
        }
        console.log("update_alert " + id);

        $.ajax({url: "parts/update_alert.php",
            data: {partid: id,status: status},
            success: function (result) {
                if (result.match("saved")) {
                    var description = document.getElementById(desc_id).innerHTML;
                    //alert(description);
                    //btn.innerHTML = btn_new;
                    document.getElementById(divid).innerHTML = " <a class ='btn btn-colour-maroon btn-sm' id='partbtn_" + id + "' style='padding: 1px 5px;' onclick='update_alert(" + id + ");'>" + btn_new + "</a>";
                    if (btn_new !== 'Set Alert') {
                        document.getElementById("part_" + id).innerHTML = '<font color="red">' + description + '</font>';
                        document.getElementById(divid).innerHTML = " <a class ='btn btn-danger btn-sm' id='partbtn_" + id + "' style='padding: 1px 5px;' onclick='update_alert(" + id + ");'>Remove Alert</a>";

                    } else {
                        document.getElementById("part_" + id).innerHTML = '<font color="black">' + description + '</font>';
                        document.getElementById(divid).innerHTML = " <a class ='btn btn-default btn-sm' id='partbtn_" + id + "' style='padding: 1px 5px;' onclick='update_alert(" + id + ");'>Set Alert</a>";

                    }
                } else {
                    alert(result);
                    //alert(id);
                    //document.getElementById(id).value = 'NEW';

                }
            },
            error: function () {
                alert('error saving');
                //document.getElementById('member_status').selectedIndex = 0;

            }});



    }

</script>
<?php
include('../connect.php');
$conn = pg_connect($_SESSION['connect_str']);


//echo $query;
$query = "select parts.*,
typepart.picture as warning
 from parts 
 LEFT JOIN typepart on (parts.type = typepart.typepart) 
    where upper(itemno) = '" . strtoupper($_SESSION['idcat']) . "' and type != 'Found' ORDER by parts.datepart desc;";
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);


if ($numrows > 0) {
//echo '<h2> Missing Parts: </h2>';
    //include('heading.php');
}
$page_break = 'No';

for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);
    $subdomain = $_SESSION['library_code'];
    $format_datepart = substr($row['datepart'], 8, 2) . '-' . substr($row['datepart'], 5, 2) . '-' . substr($row['datepart'], 0, 4);
    $alert_txt = null;
    if ($row['alertuser'] == 't') {
        $alert_txt .= 'Remove Alert';
        $description = '<font color="red"><b>' . $row['type'] . '</b>: ' . $row['description'] . '</font>';
        $str_alert = " <a class ='btn btn-danger btn-sm' id='partbtn_" . $row['id'] . "' style='padding: 1px 5px;' onclick='update_alert(" . $row['id'] . ");'>Remove Alert</a>";
    } else {
        $alert_txt .= 'Set Alert';
        $description = '<b>' . $row['type'] . '</b>: ' . $row['description'];
        $str_alert = " <a class ='btn btn-default btn-sm' id='partbtn_" . $row['id'] . "' style='padding: 1px 5px;' onclick='update_alert(" . $row['id'] . ");'>Set Alert</a>";
    }
    $paid_txt = null;
    if ($row['paid'] == 't') {
        $paid_txt .= 'Yes';
    } else {
        $paid_txt .= 'No';
    }
    $type_str = substr($row['type'], 0, 3);

    $ref = '../parts/edit_part.php?partid=' . $row['id'];
    $ref2 = '../parts/delete_part.php?partid=' . $row['id'] . '&&idcat=' . $row['itemno'];
    $ref_bor = '../../members/update/member_detail.php?borid=' . $row['borcode'];
    if ($row['borcode'] != 0) {
        $bor_link = "<a class ='btn btn-colour-yellow btn-sm' href='" . $ref_bor . "'>" . $row['borcode'] . "</a>";
    } else {
        $bor_link = "NA";
    }
    $edit_link = "<a class ='btn btn-primary btn-sm' href='" . $ref . "'>Edit</a>";
    $delete_link = "<a class ='btn btn-danger btn-sm' href='" . $ref2 . "'>Delete</a>";
    include('row_bg_color.php');
    include('row.php');
}
?>

</body>


