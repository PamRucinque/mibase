<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

function calculate_fine_special($due, $sun, $mon, $tues, $wed, $thurs, $fri, $sat, $weekly, $fine, $grace, $max_daily_fine) {
    $days = array();
    
    //$weekly = 'Yes';
    //$fine = 2;
    for ($i = 0; $i <= 6; $i++) {
        $days[$i] = 0;
    }
    // $days 0 to 6 are the days of the week starting sunday, $days[7] = total days
    // days[8] = total sessions, days[9] = total fine
    $now = date('Y-m-d');
    $total = 0;
    $session = 0;
    $date_start = date_create_from_format('Y-m-d', $due);
    $date_end = date_create_from_format('Y-m-d', $now);

    $interval = $date_start->diff($date_end);
    $no_days = $interval->format('%a');

    for ($xi = 0; $xi <= $no_days; $xi++) {
        $str = '+' . $xi . 'day';
        $curr = strtotime($str, strtotime($due));
        $str_array = date("Y-m-d", $curr);
        $dw = date("w", $curr);
        $days[$dw] = $days[$dw] + 1;
        //echo $str_array . ' ' . $days[$dw] . ' ' . date( "l", $curr). '<br>';
        $total = $total + 1;
        //echo $days[$dw] . '<br>';
        array_push($overdue, $str_array);
    }
    $days[7] = $total;

    if ($sun > 0) {
        $session = $session + $days[0] * $sun;
    }
    if ($mon > 0) {
        $session = $session + $days[1] * $mon;
    }
    if ($tues > 0) {
        $session = $session + $days[2] * $tues;
    }
    if ($wed > 0) {
        $session = $session + $days[3] * $wed;
    }
    if ($thurs > 0) {
        $session = $session + $days[4] * $thurs;
    }
    if ($fri > 0) {
        $session = $session + $days[5] * $fri;
    }
    if ($sat > 0) {
        $session = $session + $days[6] * $sat;
    }
    if ($grace > 0) {
        $days[8] = $session - $grace;
        if ($days[8] < 0) {
            $days[8] = 0;
        }
    }

    //echo 'Sessions Overdue: ' . $days[8] . '<br>'; 
    //echo 'Total Days: ' . $days[7] . '<br>'; 

    if ($weekly == 'Yes') {
        $weeks = round(($total - $grace) / 7);
        if ($weeks < 0) {
            $weeks = 0;
        }
        $total_fine = round($weeks * $fine, 2);
        //$total_fine = round($fine*round($days[7]/7),2);
    } else {
        $total_fine = round($fine * $session, 2);
    }

 
    $days[9] = $total_fine;
    $days[10] = $weeks;
    //if ($total_fine > 0) {
    if ($weekly == 'Yes') {
        $output .= '<font color="blue">This Toy is overdue by ' . $weeks . ' weeks, your fine is $' . $total_fine . ' .</font><br>';
        if ($grace > 0) {
            $output .= '<font color="red">' . $grace . ' weeks grace has been granted. </font><br>';
        }
    } else {
        $output .= '<font color="red">This Toy is overdue by ' . $session . ' sessions, your fine is $' . $total_fine . '</font><br>';
        if ($grace > 0) {
            $output .= '<font color="red">' . $grace . ' days grace has been granted. </font><br>';
        }
    }

    //$output .= 'total fine: ' . $total_fine . ' Max daily fine: ' . $max_daily_fine;


    return array('fine_status' => $output, 'amount' => $total_fine);
}



?>