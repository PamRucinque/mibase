<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
$loan_type = trim($loan_type);
$message = '';
$desc_extra = '';
$max_pg = ($max / 3);
$max_t = 2 * ($max / 3);
//$ok = 'Yes';



if ($loan_type == 'GD') {
    $loan_type = 'T';
}



$toy_count = count_gd($borid);
if (($loan_type == 'T') || ($loan_type == '3') || ($loan_type == '12')) {
    if (($toy_count['toy']) >= ($max_t)) {
        //$ok = 'No';
        $message = '<br><font color="green">Can only borrow a max of ' . $max_t . ' T toys.</font>';
        $extra_toys = 'Yes';
        $_SESSION['error'] .= $message;
    }
}
if (($loan_type == 'G') || ($loan_type == 'P')) {

    if ((($toy_count['puz'] + $toy_count['toy'])) >= ($max)) {
        //$ok = 'No';
        $message = '<br><font color="green">Can only borrow a max of ' . $max . ' Games,  Puzzles and Toys.</font>';
        $extra_toys = 'Yes';
        $_SESSION['error'] .= $message;
    }
}


if ($loan_type == '12') {
    if (count_12($borid, $idcat) > 0) {
        //$ok = 'No';
        $message = get_stype($loan_type);
        $_SESSION['error'] .= '<br><font color="blue">' . $message . '</font>';
        $extra_toys = 'Yes';
    }
    ;
}

if ($loan_type == '3') {
    if (count_3($borid, $idcat) > 0) {
        //$ok = 'No';
        $message = get_stype($loan_type);
        $_SESSION['error'] .= '<br><font color="blue">' . $message . '</font>';
        $extra_toys = 'Yes';
    }
    
}
$_SESSION['loan_status'] .= $message . '<br>';


if (($extra_toys == 'Yes')&&($ok == 'Yes')) {
    //$ok = 'Yes';
    $_SESSION['override'] = 'Yes';
    $amount = get_stype_amount($loan_type);
    if ($amount > 0) {
        $desc_extra = 'Extra Toy: ' . $loan_type;
        $query_loan .= "insert into journal( datepaid, bcode, icode,name, description, category, amount, type, typepayment)
            VALUES (now(),
            '{$borid}',
            '{$idcat}',
            '{$longname}',
            '{$toyname}',
            '{$desc_extra}',
            {$amount},
            'DR', 'Debit'
        );";
        $_SESSION['error'] .= '<br>Rent of ' . $amount . ' has been charged for this toy.';
    }
}
$max = $max_t + $max_g;

function count_toys($borid) {
    $count = 2;
    $count_pg = 0;
    $sql = 'hello';
    return array('toy' => $count, 'puz' => $count_pg, 'sql' => $sql);
}

function count_12($borid, $toyid) {
    //include( dirname(__FILE__) . '/../connect.php');
    $numrows = 0;
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $sql = "select * from transaction where idcat =? "
            . " and borid =? "
            . " and date_loan + INTERVAL '12 months' > now()";

    $sth = $pdo->prepare($sql);
    $array = array($toyid, $borid);
    $sth->execute($array);

    $numrows = $sth->rowCount();
    return $numrows;
}

function count_3($borid, $toyid) {
    //include( dirname(__FILE__) . '/../connect.php');
    $numrows = 0;
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $sql = "select * from transaction where idcat =? "
            . " and borid =? "
            . " and date_loan + INTERVAL '3 months' > now()";

    $sth = $pdo->prepare($sql);
    $array = array($toyid, $borid);
    $sth->execute($array);

    $numrows = $sth->rowCount();
    return $numrows;
}

function get_stype($stype) {
    $stype = trim($stype);
    //include( dirname(__FILE__) . '/../connect.php');
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $sql = "SELECT * from stype WHERE stype = ?";

    $sth = $pdo->prepare($sql);
    $array = array($stype);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        $message = $row['description'];
    }

    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
    }
    return $message;
}

function get_stype_amount($stype) {
    $stype = trim($stype);
    //include( dirname(__FILE__) . '/../connect.php');
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $sql = "SELECT * from stype WHERE stype = ?";

    $sth = $pdo->prepare($sql);
    $array = array($stype);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        $amount = $row['amount'];
    }

    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
    }
    return $amount;
}

function count_gd($borid) {
    //include( dirname(__FILE__) . '/../connect.php');
    $count = 0;
    $count_pg = 0;
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $sql = "SELECT transaction.*, toys.datestocktake as st, toys.loan_type as stype
        FROM transaction 
        LEFT JOIN toys on transaction.idcat = toys.idcat 
        WHERE borid = ? AND return Is Null ORDER BY created DESC;";

    $sth = $pdo->prepare($sql);
    $array = array($borid);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        if (($row['stype'] == 'T') || ($row['stype'] == '3') || ($row['stype'] == '12') || ($row['stype'] == 'GD')) {
            $count = $count + 1;
        }
        if (($row['stype'] == 'P') || ($row['stype'] == 'G')) {
            $count_pg = $count_pg + 1;
        }
    }

    //$output = $count . $max;

    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
    }
    $_SESSION['error'] .= $error_msg;
    //$count = 2;
    //$count_pg = 1;

    return array('toy' => $count, 'puz' => $count_pg, 'sql' => $sql);
}

?>