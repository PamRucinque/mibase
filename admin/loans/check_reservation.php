<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
$reservations = $_SESSION['settings']['reservations']; 

if ($reservations == 'Yes') {

    //echo $settings_reservations;
    $reserve_array = check_reserve_loans($idcat, $loanperiod, $borid);
    $check_reserved_array = check_if_reserved($idcat, $loanperiod);
    //echo 'Reserves OK: ' . $reserve_array['reserve_ok'] . '<br>';
    //echo 'Delete Reserved: ' . $reserve_array['delete_reserve'] . '<br>';
    //echo 'Due: ' . $reserve_array['due'] . '<br>';
 
    if ($check_reserved_array['reserved'] == 'Yes') {

        if ($reserve_array['reserve_ok'] != 'Yes') {
            $_SESSION['loan_status'] .= '<font color="red">Error Adding Record, this toy has reservations, available after ' . $check_reserved_array['due'] . '.</font> Please book a Reservation to loan this toy.<br>';
            $_SESSION['error'] .= 'Error Adding Record, this toy has reservations, available after ' . $check_reserved_array['due'] . '. Please book a Reservation to loan this toy.';
            $ok = 'No';
        }
    } 
    if (($reserve_array['reserve_ok'] == 'Yes') && ($check_reserved_array['reserved'] == 'Yes')) {
        $due = $check_reserved_array['due'];
        include( dirname(__FILE__) . '/delete_reserve.php');
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

