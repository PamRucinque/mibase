<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>

<link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

<script type="text/javascript">
    $(function () {
        var pickerOpts = {
            //dateFormat: "yy-mm-dd",
            dateFormat: "dd M yy",
            showOtherMonths: true

        };
        $("#set_due").datepicker(pickerOpts);
    });
    function reset_due() {
        document.getElementById("set_due").value = "";
        this.form.submit();
    }
</script>
<?php
if (isset($_SESSION['due'])) {
    if ($_SESSION['due'] != '') {
        $set_due = $_SESSION['due'];
    } else {
        $set_due = '';
    }
}
//$due = '2018-01-01';
?>
<div class="row">
    <form id="form_99824" enctype="multipart/form-data" method="post" action="loan.php">
        <div class="col-sm-3">
            <input id="clear_due_date" class="btn btn-danger btn-sm"  style="padding: 5px 5px;" type="submit" name="clear_due_date" value="Reset" /></td></tr>

        </div>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="set_due" id ="set_due" placeholder = "Select Due Date" align="LEFT" value="<?php echo $set_due; ?>" onchange="this.form.submit();" />
        </div>
    </form>
</div>


