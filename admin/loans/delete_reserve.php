<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../mibase_check_login.php');

$rid = $_SESSION['rid'];
$due = $reserve_array['due'];
$query_loan .= "UPDATE reserve_toy SET status = 'LOANED' WHERE id = " . $rid . ";";
//$_SESSION['loan_status'] .= $query_loan;

if ($reserve_fee > 0) {
    $r_desc = 'Reservation Fee for Toy No: ' . $idcat;
    $r_id = $paymentid;
    $journal_id = max_jounal_id();
    //$journal_id = 500;
    $query_loan .= "insert into journal(datepaid, bcode, icode, name, description, category, amount, type, typepayment)
            VALUES ( now(),
            '{$borid}',
            '{$idcat}',
            '{$longname}',
            '{$r_desc}',
            'TOY RESERVATION',
            {$reserve_fee},
            'DR', 'Debit'
        );";
}




