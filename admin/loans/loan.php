<?php
require( dirname(__FILE__) . '/../mibase_check_login.php');
$alert_member = '';
$alert_toy = '';
$table_events = '';
$table_parts = '';
$set_due = '';

$max = 0;
$max_mtype = 0;


$roster_alerts = $_SESSION['settings']['roster_alerts'];
$loanperiod = $_SESSION['settings']['loanperiod'];
$toy_holds = $_SESSION['settings']['toy_holds'];
$receipt_printer = $_SESSION['settings']['receipt_printer'];
$receipt_email = $_SESSION['settings']['receipt_email'];
$show_desc = $_SESSION['settings']['show_desc'];
$disable_returnall = $_SESSION['settings']['disable_returnall'];
$checked = $_SESSION['settings']['checked'];
$x = 0;
$holiday_override = $_SESSION['settings']['holiday_override'];
$reservations = $_SESSION['settings']['reservations'];
$rentasfine = $_SESSION['settings']['rentasfine'];
$set_due_date = $_SESSION['settings']['set_due_date'];


$idcat = $_SESSION['idcat'];

$str_detail = '';
$str_pic = '';
$str_loan_button = '';
$str_detail_reserve = '';
$reserved = 'No';
?>

<!doctype html>
<html lang="en">
    <head>
        <?php
        $str_alert = '';
        $str_pic = '';
        $alert = '';
        include( dirname(__FILE__) . '/../header.php');
        include ('../functions/global.php');
        $toys_on_loan = array();
        $toys_onloan['status'] = '';


        //include( dirname(__FILE__) . '/../functions/functions.php');
        ?> 
        <script>
            function overlay() {
                if ($("#overlay").is(":visible")) {
                    $("#overlay").hide();
                    document.getElementById("scanid").focus();
                }
            }
            function blinker() {
                $('.blink_me').fadeOut(500);
                $('.blink_me').fadeIn(500);
            }
            setInterval(blinker, 1000);

            function printDiv(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                document.body.innerHTML = printContents;

                window.print();
                setTimeout("closePrintView()", 3000);
                document.body.innerHTML = originalContents;
                //window.location.replace('loan.php');
            }

            function closePrintView() {
                document.location.href = 'loan.php';
                //document.getElementById("str").focus();
            }
            function email_receipt(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                document.body.innerHTML = printContents;

                document.body.innerHTML = originalContents;
                alert(originalContents);
            }
            function mem_alert() {
                if ($("#mem_alert").is(":visible")) {
                    $("#mem_alert").hide();
                } else {
                    $("#mem_alert").show();
                }

            }
            function toy_alert() {
                if ($("#toy_alert").is(":visible")) {
                    $("#toy_alert").hide();
                } else {
                    $("#toy_alert").show();
                }

            }
            function part_alert() {
                if ($("#part").is(":visible")) {
                    $("#part").hide();
                } else {
                    $("#part").show();
                }

            }

        </script>
    </head>
    <body onload="setFocus()"> 
        <div class="container-fluid">          
            <?php
            $str_roster = '';


            //include( dirname(__FILE__) . '/data/get_settings.php');
            include( dirname(__FILE__) . '/../menu.php');

            //if ($branch == 'volunteer') {
            //    include( dirname(__FILE__) . '/../header_detail/header_detail.php');
            //}
            //include( dirname(__FILE__) . '/../connect.php');
            if (isset($_POST['clear_due_date'])) {
                $_SESSION['due'] = '';
            } else {
                if (isset($_POST['set_due'])) {
                    $_SESSION['due'] = $_POST['set_due'];
                }
            }



            $_SESSION['focus'] = 'u';
            if (isset($_SESSION['idcat'])) {
                $idcat = $_SESSION['idcat'];
            }

            if (isset($_POST['renew_member'])) {
                include( dirname(__FILE__) . '/renew_membership.php');
            }
            //if (($branch == 'admin')) {

            include ('../header_detail/header_detail.php');
            if (($roster_alerts == 'Yes')) {
                include( dirname(__FILE__) . '/data/duties_reminder.php');
                include( dirname(__FILE__) . '/data/roster_alerts.php');
            }
            //} else {
            //    include ('../header_detail/get_member_header.php');
            //}
            if (isset($_SESSION['fine'])) {
                $str_alert = $_SESSION['fine'];
            }

            //$str_alert .= $_SESSION['error'];

            if (isset($_POST['submit_alert'])) {
                if (isset($_SESSION['borid'])) {
                    include( dirname(__FILE__) . '/data/update_alert_mem.php');
                    include ('../header_detail/get_member_header.php');

                    $saved = 'Alert Saved!';
                    check_alert($saved);
                    $saved = '';
                }
            }

            if (isset($_POST['submit_alert_toy'])) {
                if (isset($_SESSION['idcat'])) {
                    include( dirname(__FILE__) . '/data/update_alert_toy.php');
                    $saved = 'Alert Saved!';
                    check_alert($saved);
                    $saved = '';
                }
            }

            if (isset($_POST['delete_event'])) {
                include( dirname(__FILE__) . '/data/delete_event.php');
            }
            //delete_event


            if (isset($_POST['submit_alert_part'])) {
                if (isset($_SESSION['idcat'])) {

                    $saved = 'Alert Saved!';
                    include( dirname(__FILE__) . '/data/insert_alert_part.php');
                    check_alert($saved);
                    $saved = '';
                } else {
                    $saved = 'Please select a toy!';
                    echo '<script>
                    $(document).ready(function(){
                    alert("' . $saved . '");});
                    </script>';
                    $saved = '';
                }
            }

            //$_SESSION['fine'] = 'Hello';
            // echo "<br><p align='center'><font color='red'> THIS SECTION IS UNDER CONSTRUCTION, PLEASE DONT USE IT TO STORE LOANS</font></p>";

            if (isset($_POST['loan'])) {
                $str_alert = '';
                include( dirname(__FILE__) . '/loan_toy.php');
                $_SESSION['focus'] = 'l';
                $str_alert .= $_SESSION['error'];
                if (trim($str_alert) != '') {
                    include( dirname(__FILE__) . '/data/overlay.php');
                }
                $str_alert = '';
                $_SESSION['error'] = '';
            }


            if (isset($_POST['borid'])) {
                $str_alert = '';
                $_SESSION['idcat'] = null;
                $_SESSION['loan_status'] = null;
                $_SESSION['group'] = null;
                $_SESSION['borid'] = $_POST['borid'];
                $_SESSION['focus'] = 'm';

                $str_alert .= $alert_mem_txt;

                if (($roster_alerts == 'Yes')) {
                    //include( dirname(__FILE__) . '/data/roster_alerts.php');
                    $str_alert .= $str_roster;
                }
                if (trim($str_alert) != '') {
                    include( dirname(__FILE__) . '/data/overlay.php');
                }
                $str_alert = '';
            }

            if (isset($_POST['memberid'])) {
                //$_SESSION['borid'] = $_POST['memberid'];
                $str_alert = '';
                $_SESSION['idcat'] = null;
                $_SESSION['loan_status'] = null;
                $borid = $_SESSION['borid'];
                $_SESSION['focus'] = 'm';

                $str_alert .= $alert_mem_txt;

                if (($roster_alerts == 'Yes')) {
                    //include( dirname(__FILE__) . '/data/roster_alerts.php');
                    $str_alert .= $str_roster;
                }
                if (trim($str_alert) != '') {
                    include( dirname(__FILE__) . '/data/overlay.php');
                }

                if ($_SESSION['fine_alert'] != '') {
                    echo '<script> $(document).ready(function(){ alert("' . $_SESSION['fine'] . '");});</script>';
                    $_SESSION['fine_alert'] = '';
                }
                $str_alert = '';
                //check_alert($mem_alert);
            }

            if (isset($_POST['scanid'])) {
                $_SESSION['focus'] = 's';
                $_SESSION['idcat'] = strtoupper($_POST['scanid']);

                $string = substr($_SESSION['idcat'], 0, 1);
                $string_toy = substr($_SESSION['idcat'], 0, 2);
                $string_bookmark = substr($_SESSION['idcat'], 0, 5);
                if ($string == '-') {
                    //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
                    $_SESSION['idcat'] = rtrim(substr($_SESSION['idcat'], 1));
                }
                if ($string_bookmark == 'I5092') {
                    $_SESSION['idcat'] = ltrim(substr($_SESSION['idcat'], 5, -1), '0');
                    //$_POST['idcat'] = $_SESSION['idcat'];
                    //$_SESSION['idcat'] = $barcode;
                }


                if ($string_bookmark == '00000') {
                    $user1_input = $_POST['scanid'];
                    //echo 'hello';
                    include ('data/get_user1.php');
                    //$_POST['idcat'] = $_SESSION['idcat'];
                    //$_SESSION['idcat'] = $barcode;
                }
                if ($string_toy == 'T/') {
                    //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
                    $_SESSION['idcat'] = rtrim(substr($_SESSION['idcat'], 2));
                }

                if ($string == '0') {
                    //echo '<br>hello: ' . substr($_SESSION['idcat'], 1);
                    if ($toy_barcode_notrim != 'Yes') {
                        $_SESSION['idcat'] = ltrim($_SESSION['idcat'], '0');
                    }
                }
                $idcat = $_SESSION['idcat'];

                if (isset($_SESSION['borid']) && ($_SESSION['borid'] != 0)) {
                    $alert = '';
                    include( dirname(__FILE__) . '/loan_toy.php');

                    if ($_SESSION['error'] != '') {
                        //echo '<script> $(document).ready(function(){ alert("' . $_SESSION['error'] . '");});</script>';
                        $alert .= '<font color="red">' . $_SESSION['error'] . '</font>';
                        $_SESSION['error'] = '';
                    }
                } else {
                    $alert .= '<br><font color="red">Cannot Loan to Invalid Member!</font>';
                }
                if ($alert != '') {
                    $str_alert = $alert;
                    include( dirname(__FILE__) . '/data/overlay.php');
                    $alert = '';
                    $str_alert = '';
                    //   
                }
            }

            if (isset($_POST['idcat'])) {
                $_SESSION['idcat'] = $_POST['idcat'];
                $_SESSION['focus'] = 't';
                $idcat = $_POST['idcat'];


                if ($alert != '') {
                    $str_alert = $alert;
                    include( dirname(__FILE__) . '/data/overlay.php');
                    $alert = '';
                    $str_alert = '';
                    //   
                }
            }


            if (isset($_GET['b'])) {
                $_SESSION['borid'] = mibase_validate($_GET['b'], $_SESSION['username'], $_SESSION['password'], 'b');
                $borid = $_SESSION['borid'];
            }

            if (isset($_GET['t'])) {
                $alert = '';
                $str_alert = '';
                $_SESSION['idcat'] = $_GET['t'];
                $idcat = $_SESSION['idcat'];
                //include( dirname(__FILE__) . '/data/get_toy.php');
            }
            if (isset($_GET['r'])) {

                $_SESSION['idcat'] = $_GET['r'];
                $_SESSION['rid'] = $_GET['rid'];
                $idcat = $_SESSION['idcat'];
                //include( dirname(__FILE__) . '/update_reserve.php');
                include( dirname(__FILE__) . '/loan_toy.php');
                //check_alert($_SESSION['error']);
                $_SESSION['focus'] = 'l';
            }


            if (isset($_POST['return_all'])) {
                include( dirname(__FILE__) . '/return_all.php');
                $str_alert = $alert;
                if (trim($str_alert) != '') {
                    include( dirname(__FILE__) . '/data/overlay.php');
                }
                $str_alert = '';
            }
            if (isset($_POST['or'])) {
                if ($_SESSION['override'] == 'No') {
                    $_SESSION['override'] = 'Yes';
                } else {
                    $_SESSION['override'] = 'No';
                }
            }
            if (isset($_POST['fr'])) {
                if ($_SESSION['freerent'] == 'No') {
                    $_SESSION['freerent'] = 'Yes';
                } else {
                    $_SESSION['freerent'] = 'No';
                }
            }


            if (isset($_POST['renew_all'])) {
                include( dirname(__FILE__) . '/renew_all.php');
            }
            if (isset($_POST['email_receipt'])) {
                include( dirname(__FILE__) . '/email_receipt.php');
            }
            if (isset($_SESSION['idcat'])) {
                include( dirname(__FILE__) . '/data/get_toy.php');
                if ($toyname != '') {
                    include( dirname(__FILE__) . '/data/toy_detail_print.php');
                }
            }
            if (isset($_SESSION['missing'])) {
                $str_alert .= $_SESSION['missing'];
            }

            //$str_alert .= $_SESSION['fine_alert'];
            if ($str_alert != '') {
                //$str_alert = $_SESSION['missing'];
                if (trim($str_alert) != '') {
                    include( dirname(__FILE__) . '/data/overlay.php');
                }
                $str_alert = '';
                $_SESSION['missing'] = null;
            }
            $str_alert = '';
            ?>

            <section class="container-fluid" style="padding: 10px;">


                <div class="row" style="background-color: whitesmoke;">
                    <div class="col-sm-4 col-xs-12">
                        <?php
                        include( dirname(__FILE__) . '/mem_select.php');
                        if (isset($_SESSION['borid'])) {
                            include( dirname(__FILE__) . '/data/member_detail_print.php');
                            include( dirname(__FILE__) . '/data/payments_detail_print.php');
                        }
                        ?>
                        <div id="mem_alert" style="display: none;">
                            <br><form  id="alert" method="post" action="" width="100%">
                                <textarea id="member_alert" name="member_alert"  style="background-color:lightgoldenrodyellow;padding-bottom: 5px;"  rows="2" cols="30" class="form-control" placeholder="Enter New Member Alert"><?php echo $alert_member; ?></textarea>
                                <input id="submit_alert" class="btn btn-colour-maroon"  type="submit" name="submit_alert" value="Save Member Alert" />

                            </form>  
                        </div>
                    </div>      
                    <div class="col-sm-3 col-xs-12">
                        <div class="row">
                            <div class="col-sm-11">
                                <?php
                                include( dirname(__FILE__) . '/toy_select.php');
                                if (isset($_SESSION['idcat'])) {
                                    echo $str_detail;

                                    echo $str_detail_reserve;
                                    if ($reserved == 'Yes') {
                                        //echo get_reserve($_SESSION['borid'], $_SESSION['idcat']);
                                    }
                                    //echo $str_parts;
                                    if (isset($_SESSION['borid']) && ($_SESSION['borid'] != 0)) {
                                        echo $str_loan_button;
                                    }
                                }
                                ?>

                            </div>

                        </div>
                    </div>

                    <div class="col-sm-3">
                        <?php include('parts/list.php'); ?>
                    </div>
                    <div class="col-sm-2" style="max-width: 200px;float: right;">
                        <?php
                        if (isset($_SESSION['idcat'])) {
                            echo $str_pic . '<br>';
                        }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4">
                        <div id="toy_alert" style="display: none;">
                            <br><form  id="alert_toy" method="post" action="">
                                <textarea id="toy_alert" name="toy_alert"  class="form-control" style="background-color:lightgoldenrodyellow;padding-bottom: 5px;"  placeholder="Enter New Toy Alert" rows="2" cols="30"><?php echo $alert_toy; ?></textarea>
                                <input id="submit_alert_toy" class="btn btn-primary"  type="submit" name="submit_alert_toy" value="Save Toy Alert" />

                            </form>  
                        </div>
                        <div id="toy_alert" style="display: none;">
                            <br><form  id="alert_part" method="post" action="" width="100%">
                                <input type="Text" id="part_alert" name="part_alert"  align="LEFT"  size="30" style="background-color:lightgoldenrodyellow;"   placeholder="Enter New Part Alert" class="form-control"></input>
                                <input id="submit_alert_part" class="btn btn-primary"  type="submit" name="submit_alert_part" value="Save Part Alert" />
                            </form> <br>
                        </div>
                        <div id="part" style="display: none;">
                            <br><form  id="alert_part" method="post" action="" width="100%">
                                <input type="Text" id="part_alert" name="part_alert"  align="LEFT"  size="30" style="background-color:lightgoldenrodyellow;"   placeholder="Enter New Part Alert" class="form-control"></input>
                                <input id="submit_alert_part" class="btn btn-primary"  type="submit" name="submit_alert_part" value="Save Part Alert" />
                            </form> <br>
                        </div>
                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            </section>


            <?php
//include( dirname(__FILE__) . '/data/get_settings.php');
            if (isset($_SESSION['loan_status'])) {
                $session_status = $_SESSION['loan_status'];
            } else {
                $session_status = '';
            }
            if ($holiday_override != 'Yes') {
                if ($member_returnperiod != 0) {
                    if ($member_returnperiod < $loanperiod) {
                        $loanperiod = $member_returnperiod;
                    }
                }
            }
            $str_btn_return_date = '<a href="../settings/edit_setting.php?id=21" class="btn btn-success btn-sm"  style="padding: 5px 5px;">Change</a>';
            $str_return_date = $str_btn_return_date . ' <font color="blue"> Return Period: ' . $loanperiod . ' days.</font></td>';
            ?>
            <div class="row">
                <div class="col-sm-8 col-xs-12"><?php echo $session_status; ?></div>
                <div class="col-sm-2 col-xs-6">
                    <?php
                    if ($set_due_date == 'Yes') {
                        include ('set_due_date/due_form.php');
                    }
                    ?>
                </div>
                <div class="col-sm-2 col-xs-6" style = "align: right;">
                    <?php echo $str_return_date; ?>
                </div>
            </div>
            <?php
            if (isset($_SESSION['borid'])) {
                include( dirname(__FILE__) . '/loans.php');
                include('onloan/list.php');
            }


            $no_loans = $x;
            if ((isset($_SESSION['borid']) && ($_SESSION['borid'] != 0))) {

                $total_str_loan = '<h2>Total Toys on Loan: ' . $x;

                $total_str_loan .= ' of <font color="blue">' . $max_mtype . '</font></h2>';
                echo $total_str_loan;
                echo $toys_onloan['status'];
            }
            if ($_SESSION['settings']['reservations'] == 'Yes') {
                include( dirname(__FILE__) . '/reserves.php');
            }
            echo '<div class="row">';
            if (isset($_SESSION['borid'])) {

                if (isset($_SESSION['renew_member_off']) && $_SESSION['renew_member_off'] != 'Yes') {

                    echo '<div class="col-sm-1">';
                    include( dirname(__FILE__) . '/renew_member_form.php');
                    echo '</div>';
                }
                echo '<div class="col-sm-1">';
                include( dirname(__FILE__) . '/override_form.php');
                echo '</div>';
                echo '<div class="col-sm-1">';
                include( dirname(__FILE__) . '/freerent_form.php');
                echo '</div>';

                if ($no_loans > 0) {

                    if ($receipt_printer != 'Yes') {
                        echo '<div class="col-sm-1">';
                        include( dirname(__FILE__) . '/../reports/loan_receipt.php');
                        echo '</div>';
                    }

                    if ($disable_returnall != 'Yes') {
                        echo '<div class="col-sm-1" style="min-width:90px;">';
                        include( dirname(__FILE__) . '/return_all_form.php');
                        echo '</div>';
                    }

                    echo '<div class="col-sm-2">';
                    include( dirname(__FILE__) . '/renew_all_form.php');
                    echo '</div>';
                }


                if ($receipt_printer == 'Yes') {
                    echo '<div class="col-sm-1">';
                    ?>
                    <input type="button" class="btn btn-colour-maroon" onclick="printDiv('printableArea')" value="Receipt" />
                    <?php
                    echo '</div>';
                }
                if ($receipt_email == 'Yes') {
                    echo '<div class="col-sm-1">';
                    ?>
                    <form id="email" method="post" action="loan.php"> 
                        <input id="email_receipt" name = "email_receipt" type="submit" class="btn btn-colour-yellow" value="Email Receipt" />
                    </form>
                    <?php
                    echo '</div>';
                }

                //echo '<div class="col-sm-4"></div>';
                echo '</div><br>';
            } else {
                echo '<h2><font color="blue">Select a member from the Search member box!</font></h2>';
            }



            include( dirname(__FILE__) . '/../header_detail/stats.php');
//include( dirname(__FILE__) . '/data/alert_form.php');
            if (($show_desc == 'Yes') && ($_SESSION['idcat'] != '')) {
                include( dirname(__FILE__) . '/data/pieces_print.php');
            }

            $_SESSION['loan_status'] = '';


//include( dirname(__FILE__) . '/../header_detail/header_detail.php');
//include( dirname(__FILE__) . '/test.php');
            ?>
        </div>
        <?php
        if (($receipt_printer == 'Yes') || ($receipt_email == 'Yes')) {
            include( dirname(__FILE__) . '/data/receipt_print.php');
        }
        ?>
    </body>
</html>
