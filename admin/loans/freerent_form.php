<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
$free_rent_btn =  $_SESSION['settings']['free_rent_btn'];
$freerent = $_SESSION['settings']['freerent'];

//$_SESSION['freerent'] = 'Yes';
if ($free_rent_btn == 'Yes') {

    echo '<br><form id="freerent" method="post" action="loan.php">';

    if ($_SESSION['settings']['freerent'] == 'Yes') {
        echo '<input align="center" id="fr" class="button1_green" type="submit" name="fr" value="Free Rent ON" />';
    } else {
        echo '<input align="center" id="fr" class="button1_logout" type="submit" name="fr" value="Free Rent OFF" />';
    }
    //$str_header .= '<td><a class="button1_logout" title="Override Loan limit is OFF" href="" onclick="override()">Override OFF</a></td>';


    echo '</form>';
}
?>
