<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');

$renew_loan_period = $_SESSION['settings']['renew_loan_period'];
$times_renew = $_SESSION['settings']['times_renew'];
$chargerent = $_SESSION['settings']['chargerent'];
$global_rent = $_SESSION['settings']['global_rent'];
$holiday_override = $_SESSION['settings']['holiday_override'];
$holiday_due = $_SESSION['settings']['holiday_due'];

$query_renew = '';

//include 'data/get_settings.php';
////include( dirname(__FILE__) . '/../connect.php');
include( dirname(__FILE__) . '/functions/functions.php');
$_SESSION['loan_status'] = ' ';
$count = 0;
$alert = '';
//$_SESSION['idcat'] = Null;

if (isset($_SESSION['borid'])) {
    $borid = $_SESSION['borid'];
 
    $query_renewall = "Select * from  transaction WHERE return is null AND 
                         borid = " . $borid . ";";
    
    $result = pg_exec($conn, $query_renewall);
    $numrows = pg_numrows($result);
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result, $ri);
        $transid = $row['id'];
        $renewtoy = renew_toy($transid, $loanperiod, $times_renew, $chargerent, $global_rent,$holiday_override,$holiday_due);
        if ($renewtoy['result'] =='Yes'){
            $count = $count + 1;
        }
        $_SESSION['loan_status'] .= $renewtoy['status'] . '<br>';
        if ($renewtoy['alert'] != ''){
            $alert .= $renewtoy['alert'] . '\n';
        }
       
   }
   $_SESSION['loan_status'] .= '<h3><font color="blue">' . $count . ' toys renewed</font></h3>';
   if ($alert != '') {
            echo '<script>
        $(document).ready(function(){
		alert("' . $alert . '");});
        </script>';
        }
}

?>
