<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */

require( dirname(__FILE__) . '/../mibase_check_login.php');
//echo 'Rent FActor: ' . $rent_factor;
$totalrent = 0;
$rent_status = '';
$timezone = $_SESSION['settings']['timezone'];
date_default_timezone_set($timezone);
$rent_factor = $_SESSION['settings']['rent_factor'];
$extra_rent = $_SESSION['settings']['extra_rent'];
$chargerent = $_SESSION['settings']['chargerent'];
$member_alerts = $_SESSION['settings']['member_alerts'];
$global_rent = $_SESSION['settings']['global_rent'];

$currentDateTime = date('m/d/Y H:i:s');
if ($member_alerts == 'Yes') {
    //include( dirname(__FILE__) . '/../connect.php');
    $query_event = "SELECT * from  event where typeevent = 'Levy Free Toy Hire' and memberid = " . $borid . ";";
    //$_SESSION['payment_status'] .= $query_event;
    //$_SESSION['loan_status'] .= $query_event;
    $result_event = pg_Exec($conn, $query_event);
    $numrows = pg_numrows($result_event);
    if ($numrows > 0) {
        //$_SESSION['loan_status'] .= 'Alerts: ' . $numrows;
        $_SESSION['freerent'] = 'Yes';
    }
}
if ($rent_factor == 'Yes') {
    $rent_memberype = rent_by_membertype($membertype, $rent);
    //$rent_status .= $rent_memberype['status'];
    if ($rent_memberype['rent'] == 0) {
        $_SESSION['freerent'] = 'Yes';
        $rent_status .= '<font color="blue">No rent has been charged for this Toy.</font><br>';
    } else {
        $rent = $rent_memberype['rent'];
    }
}
if ($extra_rent == 'Yes') {
    $rent_extra = extra_rent($rent);
    $rent = $rent_extra['rent'];
    $rent_status .= $rent_extra['status'];
}
//$_SESSION['loan_status'] .= 'Charging Rent: ' . $chargerent . ' Free Rent: ' . $_SESSION['freerent'];
if (($chargerent == 'Yes') && ($_SESSION['freerent'] == 'No')) {

    if ($global_rent == 0) {
        if ($rent > 0) {
            ///$totalrent = round(($rent * $loanperiod / 7), 2);
            $totalrent = $rent;
            $query_loan .= "insert into journal( datepaid, bcode, icode,name, description, category, amount, type, typepayment, debitdate)
            VALUES (now(),
            '{$borid}',
            '{$idcat}',
            '{$longname}',
            '{$toyname}',
            'Rent',
            {$totalrent},
            'DR', 'Debit', '{$currentDateTime}'
        );";
        }
    } else {
        //$totalrent = round(($global_rent * $loanperiod / 7), 2);
        $totalrent = $global_rent;
        $query_loan .= "insert into journal( datepaid, bcode, icode,name, description, category, amount, type, typepayment, debitdate)
            VALUES (now(),
            '{$borid}',
            '{$idcat}',
            '{$longname}',
            '{$toyname}',
            'Rent',
            {$totalrent},
            'DR', 'Debit','{$currentDateTime}'
        );";
    }

    $rent_status = '<font color="blue">$' . $totalrent . ' rent has been charged for this Toy.</font><br>';
}

   

