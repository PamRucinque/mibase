<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../mibase_check_login.php');
$return_today = return_today($idcat, $borid);
$timezone = $_SESSION['settings']['timezone'];
date_default_timezone_set($timezone);
$currentDateTime = date('m/d/Y H:i:s');
$session = date('A', strtotime($currentDateTime));
//$group = $_SESSION['group'];
$today = date("Y-m-d H:i:s");
$mobile1 = str_replace("'", "`", $mobile1);


if ($return_today > 0) {
    
    //$ok = 'No';
    $_SESSION['loan_status'] .= ' This Toy has been returned today.<br>';
    $query_loan .= "UPDATE transaction set return = Null,due = '"
            . $due . "' WHERE (date_loan::date)=(now()::date) AND ((idcat)='" . $idcat . "') AND ((borid)=" . $borid . ");";
} else {

    $query_loan .= "insert into transaction (idcat, borid , due, borname, item, location,session, phone, loanby ) 
            VALUES (
            '{$idcat}',
            {$borid},
            '{$due}',
            '{$longname}',
            '{$toyname}',
            '{$location}',
            '{$session}',
            '{$mobile1}', '{$_SESSION['username']}');";
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

