<?php
require( dirname(__FILE__) . '/../../mibase_check_login.php');

include('../connect.php');

if (isset($_SESSION['borid'])) {
    $memberid = $_SESSION['borid'];
} else {
    $memberid = 0;
}
$weight_str = '';
$str_loan_type = '';

$sql = "select t.created, t.id, t.idcat, date_loan, item as toyname, to_char(t.due, 'dd/mm/yyyy') as due, t.location, to_char(t.date_loan, 'dd/mm/yyyy') as date_loan, 
toys.rent, toys.loan_type, toys.sub_category, toys.no_pieces, 
coalesce(l.weight, 1) as weight,
(  CASE
    WHEN t.due < current_date THEN 'Yes'
    ELSE 'No'
  END ) as overdue,
  (select count(id) from reserve_toy where reserve_toy.idcat = t.idcat and date_end > current_date)  as count_reserve 
from transaction t
left join toys on toys.idcat = t.idcat
left join loan_restrictions l on (l.category = toys.category) 
where 
borid = ?  
and return is null
ORDER BY created DESC;";

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$sth = $pdo->prepare($sql);
$array = array($memberid);
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $_SESSION['error'] = "An  error occurred.\n";
    $_SESSION['error'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[2] . '<br>';
}
$numrows = $sth->rowCount();
include('heading.php');
for ($ri = 0; $ri < $numrows; $ri++) {
    $toy = $result[$ri];
    $ref = 'renew_toy.php?id=' . $toy['id'];
    $ref_return = 'return_toy.php?id=' . $toy['id'];
    $ref_edit = 'trans/edit_trans.php?id=' . $toy['id'];
    $ref_checked = 'checked.php?idcat=' . $toy['idcat'];
    $link = 'loan.php?t=' . $toy['idcat'];
    $onclick = 'javascript:location.href="' . $link . '"';
    $rent_str = '0';
    $location_str = '';

    if ($toy['overdue'] == 'Yes') {
        $due_str = '<font color="red">' . $toy['due'] . '</font>';
    } else {
        $due_str = $toy['due'];
    }
    if ($toy['rent'] > 0) {
        $rent_str = $toy['rent'];
    }
    if (($loan_restrictions == 'Yes') || ($loan_weighting == 'Yes')) {
        $weight_str = $toy['weight'];
    }
    if ($multi_location == 'Yes') {
        $location_str = $toy['location'];
    }
    if ($toy['count_reserve'] == 0) {
        $str_reserve = '<font color="red">RESERVED</font>';
    }
    if ($toy['loan_type'] != 'GOLD STAR') {
        $str_renew = " <a class ='btn btn-success btn-sm' href='" . $ref . "'>Renew</a>";
    } else {
        $str_renew = "<font color='red'> LOCKED</font>";
        $str_renew = '';
    }
    $str_return = " <a class ='btn btn-colour-yellow btn-sm' href='" . $ref_return . "'>Return</a>";
    $str_edit = " <a class ='btn btn-primary btn-sm' href='" . $ref_edit . "'>Edit</a>";
    $str_checked = " <a class ='btn btn-danger btn-sm' href='" . $ref_checked . "'>Checked</a>";
    $str_buttons = $str_return . $str_edit . $str_renew;

    if ($toy['loan_type'] != '') {
        $str_loan_type = substr($toy['loan_type'], 0, 4);
    } else {
        if ($checked == 'Yes') {
            $str_loan_type = $str_checked;
        }
    }
    include('row_bg_color.php');
    include('row.php');
}


