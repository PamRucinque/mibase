<?php 
$chargerent = $_SESSION['settings']['chargerent'];
$multi_location = $_SESSION['settings']['multi_location'];
$loan_weighting = $_SESSION['settings']['loan_weighting'];

$str_rent_heading = '';
$str_location_heading = '';
$str_weight_heading = '';

if ($chargerent == 'Yes'){
    $str_rent_heading = 'Rent';
}
if ($multi_location == 'Yes'){
    $str_location_heading = 'Location';
}
if (($loan_restrictions == 'Yes') || ($loan_weighting == 'Yes')) {
    $str_weight_heading = 'Weight';
}
?>
<div class = "row hidden-xs" style="padding-bottom: 2px;font-weight: bold;padding-top: 5px;color: darkred;">
    <div class="col-xs-12 col-sm-3" style="overflow: hidden;">
        <div class="hidden-xs col-sm-3" style="color: grey;">id</div>
        <div class="col-xs-8 col-sm-3">Loaned</div>
        <div class="col-xs-8 col-sm-1"><?php echo $str_rent_heading; ?></div>
        <div class="col-xs-8 col-sm-1"><?php echo $str_location_heading; ?></div>
        <div class="col-xs-8 col-sm-1"><?php echo $str_weight_heading; ?></div>
        <div class="col-xs-8 col-sm-1"></div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="col-xs-4 col-sm-2">Toy #</div>
        <div class="col-xs-8 col-sm-9">Toy Name</div>
        <div class="col-xs-8 col-sm-1">#Pieces</div>
    </div>

    <div class="hidden-xs col-sm-3">
        <div class="col-xs-8 col-sm-4">Due</div>
        <div class="col-xs-8 col-sm-8"></div>
    </div>
</div>

