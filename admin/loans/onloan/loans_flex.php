<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');

include('../connect.php');
if (isset($_SESSION['borid'])) {
    $memberid = $_SESSION['borid'];
} else {
    $memberid = 0;
}


$trans = "SELECT transaction.*, toys.datestocktake as st, toys.sub_category as sub_category, toys.sponsorname as sponsorname,
toys.stype as stype, toys.loan_type as loan_type, no_pieces as pieces, toys.stocktake_status as stocktake, toys.category,
    toys.desc1 as desc1, toys.desc2 as desc2, toys.rent as rent,toys.cost as cost,
    coalesce(l.weight, 1) as weight,
(select count(id) from reserve_toy where reserve_toy.idcat = transaction.idcat and date_end > current_date)  as count_reserve,
array_to_string(
 array(  SELECT (p.type || ': ' || p.description || '<br>')
  FROM parts p
  WHERE p.itemno = toys.idcat and (p.alertuser)) ,
 ' '
 ) AS missing,
 array_to_string(
 array(  SELECT (p.type || ': ' || p.description || '<br>')
  FROM parts p
  WHERE p.itemno = toys.idcat and (p.type = 'Missing' or p.type = 'Broken')) ,
 ' '
 ) AS missing_stoke
FROM transaction 
LEFT JOIN toys on transaction.idcat = toys.idcat 
left join loan_restrictions l on (l.category = toys.category) 
WHERE borid = " . $memberid . " AND return Is Null ORDER BY created DESC;";

$x = 0;
$count = 1;
$trans = pg_exec($conn, $trans);
$x = pg_numrows($trans);

$str_loan = '';

$str_receipt = '<br>';
$str_receipt_text = '<br>';
$str_receipt_long = '<h2>Toys On Loan</h2>';
$str_receipt_elwood = '<br>';
$str_receipt_stoke = '<br>';
$str_receipt_short = '<h2>Toys On Loan</h2>';
$str_receipt_pieces = '';
$str_receipt_pieces_missing = '';
$str_receipt_pieces_missing_nsp = '';
$str_receipt_pieces_missing_pic = '';
$missing = '';
if ($x > 0) {
    $str_loan .= '<table border="1" width="100%" style="border-collapse:collapse; border-color:grey">';
    $str_loan .= '<tr><td>id</td><td></td><td>date</td><td>toy #</td><td>Toy name</td><td>#</td><td>Due</td><td></td></tr>';
    $str_receipt .= '<table width="100%" style="border-collapse:collapse; border-color:grey;">';
    $str_receipt_short .= '<table width="100%" style="border-collapse:collapse; border-color:grey;">';
    $str_receipt_long .= '<table width="100%" style="border-collapse:collapse; border-color:grey;">';
    $str_receipt_long .= '<strong><tr><td>Due</td><td>Item</td><td>Description</td><td>Pieces</td></tr></strong>';
    $str_receipt_elwood .= '<table width="100%" style="border-collapse:collapse; border-color:grey; font-size: 8pt;">';
    $str_receipt_stoke .= '<table width="100%" style="border-collapse:collapse; border-color:grey; font-size: 8pt;">';

    $str_receipt_pieces .= '<table width="100%" style="border-collapse:collapse; border-color:grey">';
    // $str_receipt_pieces_missing .= '<table width="100%" style="border-collapse:collapse; border-color:grey">';
} else {
    if (isset($_SESSION['borid'])) {
        //echo '<br>This member has no Toys on loan';  
    }
}
$total_weight = 0;
$cost = 0;

for ($ri = 0; $ri < $x; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($trans, $ri);

    //$weekday = date('l', strtotime($row['date_roster']));
    $trans_id = $row['id'];
    $trans_borid = $row['borid'];
    $stocktake = $row['stocktake'];
    $trans_item = $row['item'];
    $trans_idcat = $row['idcat'];
    $trans_bornmame = $row['borname'];
    $trans_due = $row['due'];
    $pieces = $row['pieces'];
    $desc1 = $row['desc1'];

    $desc2 = $row['desc2'];
    $stype = $row['stype'];
    $group = $row['group_trans'];
    $sub_category = $row['sub_category'];
    $rent = $row['rent'];
    $missing_pieces = $row['missing'];
    $missing_stoke = $row['missing_stoke'];
    if ($rent == '') {
        $rent = 0;
    }

    $loan_type = $row['loan_type'];
    $due_long = strftime('l jS \of F Y', strtotime($trans_due));
    $format_loan = substr($row['date_loan'], 8, 2) . '-' . substr($row['date_loan'], 5, 2) . '-' . substr($row['date_loan'], 0, 4);
    $format_due = substr($row['due'], 8, 2) . '/' . substr($row['due'], 5, 2) . '/' . substr($row['due'], 0, 4);
    $now = date('Y-m-d');
    $cost = $cost + $row['cost'];
    $trans_loantype = $row['loan_type'];
    $ref2 = 'return_toy.php?id=' . $row['id'];
    $ref4 = 'checked.php?idcat=' . $row['idcat'];
    $ref = 'renew_toy.php?id=' . $row['id'];
    $ref3 = 'trans/edit_trans.php?id=' . $row['id'];
    $ref_group = 'group.php?idcat=' . $row['idcat'];
    $ref_group_clear = 'group.php?idcat=clear';
    $st = $row['st'];
    $location = $row['location'];
    $weight = $row['weight'];
    $total_weight = $total_weight + $weight;
    


    if (strtotime($now) > strtotime($trans_due)) {
        $due_str = '<font color="red" font="strong"> OVERDUE  ' . $format_due . '</font>';
    } else {
        $due_str = $format_due;
    }
    $clean_str = '';
    if ($checked == 'Yes') {
        //echo $checked;
        if (strtotime('-1 year', time()) > strtotime($st)) {
            $clean_str .= "<br><a class ='button_small_red' href='" . $ref4 . "'>Checked</a>";
        } else {

            $clean_str .= '';
        }
        if (trim($stocktake) == 'LOCKED') {
            $clean_str .= '<a class="button_small_return" href="checked.php?idcat=' . $row['idcat'] . '">Unlock</a></td>';
        } else {
            $clean_str .= '';
        }
    }
    //<a class="button_menu" href="../../toys/update/toy_detail.php">Toy</a>
    $str_loan .= '<tr id="red">';
    $str_loan .= '<td width="5%">' . $trans_id . '</td>';
    if (($loan_type != '')) {
        $str_loan .= '<td width="3%" align="center">' . $loan_type . '</td>';
    } else {
        if ($group_trans == 'Yes') {
            if ($group == '') {
                if ($_SESSION['group'] == '') {
                    $str_loan .= "<td width='3%' align='center'><a class ='button_small_renew' href='" . $ref_group . "'>Group</a></td>";
                } else {
                    $str_loan .= "<td width='3%' align='center'><a class ='button1_logout' href='" . $ref_group_clear . "'>Clear</a></td>";
                }
            } else {
                $str_loan .= '<td width="3%" align="center">' . strtoupper($group) . '</td>';
            }
        } else {
            if ($rent > 0) {
                $str_loan .= '<td width="3%">' . $rent . '</td>';
            } else {
                if (($loan_restrictions == 'Yes')||($loan_weighting == 'Yes')) {
                    $str_loan .= '<td width="3%">' . $weight . '</td>';
                } else {
                    $str_loan .= '<td width="3%">' . substr($location, 0, 3) . '</td>';
                }
            }
        }
    }
    $str_receipt .= '<tr>';
    $str_receipt .= '<td>' . $trans_idcat . ': ' . $trans_item . '<br></td></tr>';
    $str_receipt_text .= $trans_idcat . ': ' . $trans_item . '<br>';
    $str_receipt_text .= '<strong>Due: </strong>' . $format_due . '<br><br>';
    //$str_receipt .= '<tr><td>Due: </strong>' . $format_due . ' <br>No Pieces: ' . $pieces . '<br></td></tr>';
    $str_receipt .= '<tr><td>Due: </strong>' . $format_due . '</td></tr>';

    $str_receipt_stoke .= '<tr>';
    $str_receipt_stoke .= '<td><br>' . $trans_idcat . ': ' . $trans_item . '<br></td></tr>';

    //$str_receipt .= '<tr><td>Due: </strong>' . $format_due . ' <br>No Pieces: ' . $pieces . '<br></td></tr>';
    $str_receipt_stoke .= '<tr><td>Due: </strong>' . $format_due . '<br>Pieces: ' . $pieces . '<br></td></tr>';

    $str_receipt_elwood .= '<tr>';
    $str_receipt_elwood .= '<td>' . $trans_idcat . ': ' . $trans_item . '<br></td></tr>';
    $str_receipt_elwood .= '<tr><td>Due: </strong>' . $format_due . ' <br>No Pieces: ' . $pieces . '</td></tr>';



    if ($missing_pieces != '') {
        $str_receipt .= '<tr><td colspan="1"><font color="green">' . $missing_pieces . ' </font></td></tr>';
        $str_receipt_elwood .= '<tr><td colspan="1">' . $missing_pieces . '</td></tr>';
    }
    if ($missing_stoke != '') {
        $str_receipt_stoke .= '<tr><td colspan="1"><font color="green">' . $missing_stoke . ' </font></td></tr>';
    }
    $str_receipt_elwood .= '<tr><td><br></td></tr>';

    $str_receipt_long .= '<tr>';
    $str_receipt_long .= '<td style="padding-left: 3px;">' . $format_due . '</td>';
    $str_receipt_long .= '<td style="padding-left: 3px;">' . $trans_idcat . '</td>';
    $str_receipt_long .= '<td width="45%" style="padding-left: 3px;">' . strtoupper($trans_item) . '</td>';
    $str_receipt_long .= '<td style="padding-left: 3px;">No: ' . $pieces . '</td>';




    if ($missing_pieces != '') {
        $str_receipt_long .= '<tr><td colspan="4"><font color="green">' . $missing_pieces . '</font></td></tr>';
    }
    $str_receipt_short .= '<tr><td width="70px" style="padding-left: 3px;">' . $format_due . '</td>';
    $str_receipt_short .= '<td style="padding-left: 3px;width: 50px;">' . $trans_idcat . '</td>';
    $str_receipt_short .= '<td style="padding-left: 3px;">' . strtoupper($trans_item) . '</td></tr>';
    //$str_receipt_ .=
    //$str_receipt_short .= '<td style="padding-left: 3px;">' . $pieces . '</td></tr>';
    $str_temp = '<tr><td style="padding-left: 3px;" width="100%"><h4>' . $trans_idcat . ': ' . strtoupper($trans_item) . '<br>  Due: ' . $format_due . '  No Pieces: ' . $pieces . '</h4></td></tr></table>';

    $str_temp .= '<table><tr><td width="50%" style="padding-left: 3px;padding-right: 10px;">' . $desc1 . '</td><td valign="top">' . $desc2 . '</td><td></td></tr></table>';
    //$str_temp .= '</td></tr>';



    $str_temp = str_replace("\n", "<br>", $str_temp);
    $str_receipt_pieces .= $str_temp;
    $str_receipt_pieces_missing .= $str_temp;
    $str_receipt_pieces_missing_nsp .= $str_temp;
    $str_receipt_pieces_missing_pic .= $str_temp;

    if ($missing_pieces != '') {
        $str_receipt_pieces_missing .= '<tr><td colspan="4"><font color="green">' . $missing_pieces . '</font></td></tr>';
        $str_receipt_pieces .= '<tr><td colspan="4"><font color="green">' . $missing_pieces . '</font></td></tr>';
    }

    if ($missing_stoke != '') {
        $str_receipt_pieces_missing_nsp .= '<tr><td colspan="4"><font color="green">' . $missing_stoke . ' </font></td></tr>';
        $str_receipt_pieces_missing_pic .= '<tr><td colspan="4"><font color="green">' . $missing_stoke . ' </font></td></tr>';
    }
    $file_pic = '../../toy_images/' . $subdomain . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $str_receipt_pieces_missing_pic .= '<img height="150px" src="http://' . $subdomain . $domain . $subdomain . '/' . strtolower($row['idcat']) . '.jpg" alt=""><br>';
    }

    $str_loan .= '<td width="9%">' . $format_loan . '</td>';
    $str_loan .= "<td width='14%'><a class ='button_small_return' href='" . $ref2 . "'>Return</a>";
    $str_loan .= '<a class="button_small_toys" href="loan.php?t=' . $trans_idcat . '">' . $trans_idcat . '</a>';
    $str_loan .= '</td>';

    $str_loan .= '<td width="28%">' . $trans_item;
    if ($sub_category_loans == 'Yes') {
        $str_loan .= '<font color="blue"> ' . $sub_category . '</font>';
    }
    $str_loan .= '</td>';
    $str_loan .= '<td width="2%" align="center">' . $pieces . '</td>';
    //echo '<td width="150" align="left">' . $trans_bornmame . '</td>';
    $str_loan .= '<td width="7%" align="left">' . $due_str . '</td>';

    $str_loan .= "<td width='14%'><a class ='button_small_toys' href='" . $ref3 . "'>Edit</a>";

    if ($row['count_reserve'] == 0) {
        if ($trans_loantype != 'GOLD STAR') {
            $str_loan .= "<a class ='button_small_renew' href='" . $ref . "'>Renew</a>";
        } else {
            $str_loan .= "GOLD STAR";
        }
    } else {
        $str_loan .= '<font color="red"> RESERVED</font>';
    }


    $str_loan .= "</td>";
    if ($checked == 'Yes') {
        //echo $clean_str;
        $str_loan .= '<td width="3%">' . $clean_str . '</td>';
    }

//echo "<td width='50' align='center'><a class ='button_small_red' href='" . $ref2 . "'>Checked</a></td>";





    $str_loan .= '</tr>';
    $str_receipt .= '</tr>';
    $str_receipt_elwood .= '</tr>';
    $str_receipt_pieces_missing_nsp .= '</tr>';
    $str_receipt_pieces_missing_pic .= '</tr>';
    $str_receipt_pieces_missing .= '</tr>';
    //$str_receipt_long .= '</tr>';
}

$cost = ' $' . sprintf('%01.2f', $cost);

$str_loan .= '</table>';
$str_receipt .= '</table><br>';
$str_receipt_long .= '</table><br>';
$str_receipt_elwood .= '</table><br>';
$str_receipt_short .= '</table><br>';
$str_receipt_stoke .= '</table><br>';
$str_receipt_pieces_missing_nsp .= '</table><br>';
$str_receipt_pieces_missing_pic .= '</table><br>';
$str_receipt_pieces_missing .= '</table><br>';

echo $str_loan;

//pg_close($link);
?>


