<?php


require(dirname(__FILE__) . '/../mibase_check_login.php');

//include ('data/get_settings.php');
include( dirname(__FILE__) . '/functions/functions.php');
include( dirname(__FILE__) . '/functions/overdue.php');
include( dirname(__FILE__) . '/special/special_return.php');

$_SESSION['loan_status'] = ' ';

$special_return = $_SESSION['settings']['special_return'];
$recordfine = $_SESSION['settings']['recordfine'];
$chargerent = $_SESSION['settings']['chargerent'];
$weekly_fine = $_SESSION['settings']['weekly_fine'];
$fine_value = $_SESSION['settings']['fine_value'];
$grace = $_SESSION['settings']['grace'];
$max_daily_fine = $_SESSION['settings']['max_daily_fine'];
$unlock_returns = $_SESSION['settings']['unlock_returns'];
$timezone = $_SESSION['settings']['timezone'];
$rentasfine = $_SESSION['settings']['rentasfine'];

if (isset($_GET['transid'])) {
    $transid = $_GET['transid'];

    $_SESSION['return_alert'] = '';
    $returntoy = return_toy($transid, $recordfine, $chargerent, $weekly_fine, $fine_value, $grace, $rentasfine, $max_daily_fine, $unlock_returns);

    //$returntoy = return_toy($transid, $recordfine, $chargerent, $weekly_fine, $fine_value, $grace,$rentasfine);
    if ($returntoy['alert'] != '') {
        $_SESSION['return_alert'] .= $returntoy['alert'] . '\n';
    }

    $_SESSION['loan_status'] = $returntoy['status'];
}

$redirect = 'Location: loan.php';
header($redirect);
?>
