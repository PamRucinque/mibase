<?php

require( dirname(__FILE__) . '/../../mibase_check_login.php');

function check_daily($borid) {

    $amount = 0;
    $conn = pg_connect($_SESSION['connect_str']);
    $sql_daily = "select sum(amount) as total from journal where datepaid = current_date 
        and category = 'Overdue Fine' and bcode = " . $borid . " ;";

    $result1 = pg_Exec($conn, $sql_daily);
    $row = pg_fetch_array($result1, 0);
    $amount = $row['total'];
    if ($amount == '') {
        $amount = 0;
    }
    //$_SESSION['loan_status'] .= $query . '<br>' . $numrows;

    return $amount;
}

function calculate_session_fine($due) {
    $output = '';
    $overdue_array = array();
    $year_array = array();
    $now = date('Y-m-d');
    include( dirname(__FILE__) . '/data/get_settings.php');
    $date = $due;
    //array_push($overdue_array, $date);
    $lastyear = date("Y-m-d", strtotime("-1 year", strtotime($now)));
    //echo '<br>' . $lastyear . '<br>';
    $date_year = $lastyear;
    array_push($year_array, $date_year);
    $day_count = 0;
    $week_count = 1;
    if ($fortnight == '') {
        $fortnight = 'No';
    }
    //$fortnight = 'Yes';

    while (strtotime($date_year) < strtotime($now)) {
        $day_count = $day_count + 1;

        if ($day_count == 7) {
            $day_count = 0;
            $week_count = $week_count + 1;
        }

        $date_year = date("Y-m-d", strtotime("+1 day", strtotime($date_year)));
        if ($fortnight == 'Yes') {
            if ($week_count % 2 == 0) {
                array_push($year_array, $date_year);
            }
        } else {
            array_push($year_array, $date_year);
        }
    }
    while (strtotime($date) < strtotime($now)) {
        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        array_push($overdue_array, $date);
    }
    $output .= print_r($overdue_array);
    $count = 0;

    for ($i = 0; $i < sizeof($overdue_array); $i++) {
        if (in_array($overdue_array[$i], $year_array)) {
            //echo $overdue_array [$i] . ': ';
            $day = date('D', strtotime($overdue_array[$i]));
            //echo $day . '<br>';
            if (($day == 'Mon') && ($monday > 0)) {
                $count = $count + $monday;
            }
            if (($day == 'Tues' ) && ($tuesday > 0)) {
                $count = $count + $tuesday;
            }
            if (($day == 'Wed' ) && ($wednesday > 0)) {
                $count = $count + $wednesday;
            }
            if (($day == 'Thu' ) && ($thursday > 0)) {
                $count = $count + $thursday;
            }
            if (($day == 'Fri') && ($friday > 0)) {
                $count = $count + $friday;
            }
            if (($day == 'Sat' ) && ($saturday > 0)) {
                $count = $count + $saturday;
            }
            if (($day == 'Sun') && ($sunday > 0)) {
                $count = $count + $sunday;
            }
        }
    }

    $total_fine = $count * $fine_value;
    if ($total_fine > 0) {
        $output = 'This  toy is overdue by ' . $count . ' sessions. Your fine amount is $' . $total_fine . '.';
    }
    return array('fine_status' => $output, 'amount' => $total_fine, 'days' => $count);
}

function check_reserve_loans($idcat, $loanperiod, $borid) {

    $conn = pg_connect($_SESSION['connect_str']);
    $query = "select * from reserve_toy where idcat = '" . $idcat . "' and date_end > current_date order by date_start;";
    $numrows = 0;
    $reserve_ok = 'Yes';
    $due = '';
    $delete_reserve = 'No';
    $result1 = pg_Exec($conn, $query);
    $numrows = pg_numrows($result1);
    //$_SESSION['loan_status'] .= $query . '<br>' . $numrows;
    if ($numrows > 0) {

        $reserve_ok = 'No';
        $loan_days_str = '';
        $date_start = date('Y-m-d');


        for ($xi = 0; $xi <= $loanperiod; $xi++) {
            $str = '+' . $xi . 'day';
            $curr = strtotime($str, strtotime($date_start));
            $str_date = date("Y-m-d", $curr);
            if ($xi != ($loanperiod)) {
                $loan_days_str .= "'" . $str_date . "',";
            } else {
                $loan_days_str .= "'" . $str_date . "'";
            }
        }
        //       date_end IN (" . $loan_days_str . ") and "
        $query_res = "select * from reserve_toy where "
                . " idcat = '" . $idcat . "' and member_id = " . $borid . " and status = 'ACTIVE';";
        $result2 = pg_Exec($conn, $query_res);
        // echo $query_res;
        $check_date_range = pg_numrows($result2);
        if ($check_date_range == 1) {
            $row = pg_fetch_array($result2, 0);
            $due = $row['date_end'];
            $reserve_ok = 'Yes';
            $delete_reserve = 'Yes';
            $_SESSION['loan_status'] .= 'The reservation status on this Toy has been updated.<br>';
        } else {
            $reserve_ok = 'No';
        }
        $_SESSION['loan_status'] .= $nummrows;
    }
    return array('reserve_ok' => $reserve_ok, 'delete_reserve' => $delete_reserve, 'due' => $due);
}

function calculate_fine($due, $sun, $mon, $tues, $wed, $thurs, $fri, $sat, $weekly, $fine, $grace) {
    
    $timezone = $_SESSION['settings']['timezone'];
    date_default_timezone_set($timezone);
    $days = array();
    $output = '';
    //$fine = 2;
    for ($i = 0; $i <= 6; $i++) {
        $days[$i] = 0;
    }
    // $days 0 to 6 are the days of the week starting sunday, $days[7] = total days
    // days[8] = total sessions, days[9] = total fine
    $now = date('Y-m-d');

    $total = 0;
    $session = 0;
    $date_start = date_create_from_format('Y-m-d', $due);
    $date_end = date_create_from_format('Y-m-d', $now);

    $interval = $date_start->diff($date_end);
    $no_days = $interval->format('%a');

    for ($xi = 0; $xi <= $no_days; $xi++) {
        $str = '+' . $xi . 'day';
        $curr = strtotime($str, strtotime($due));
        $str_array = date("Y-m-d", $curr);
        $dw = date("w", $curr);
        $days[$dw] = $days[$dw] + 1;
        //echo $str_array . ' ' . $days[$dw] . ' ' . date( "l", $curr). '<br>';
        $total = $total + 1;
        //echo $days[$dw] . '<br>';
        array_push($overdue, $str_array);
    }
    $days[7] = $total;

    if ($sun > 0) {
        $session = $session + $days[0] * $sun;
    }
    if ($mon > 0) {
        $session = $session + $days[1] * $mon;
    }
    if ($tues > 0) {
        $session = $session + $days[2] * $tues;
    }
    if ($wed > 0) {
        $session = $session + $days[3] * $wed;
    }
    if ($thurs > 0) {
        $session = $session + $days[4] * $thurs;
    }
    if ($fri > 0) {
        $session = $session + $days[5] * $fri;
    }
    if ($sat > 0) {
        $session = $session + $days[6] * $sat;
    }
  if ($grace > 0) {
    
        $session = $session - $grace;

    }

    //echo 'Sessions Overdue: ' . $days[8] . '<br>'; 
    //echo 'Total Days: ' . $days[7] . '<br>'; 
    //echo 'weeks' . $weekly . ' fine ' . $fine . ' grace ' . $grace;
    if ($weekly == 'Yes') {
        $weeks = round(($total - $grace) / 7);
        if ($weeks < 0) {
            $weeks = 0;
        }
        $total_fine = round($weeks * $fine, 2);
        //$total_fine = round($fine*round($days[7]/7),2);
    } else {
        $total_fine = round($fine * $session, 2);
    }


    $days[9] = $total_fine;
    $days[10] = $weeks;
    //echo 'total fine: ' . $total_fine;

    if ($total_fine > 0) {
        if ($weekly == 'Yes') {
            $output .= 'This Toy is overdue by ' . $weeks . ' weeks, your fine is $' . $total_fine . '. ';
            if ($grace > 0) {
                $output .= '' . $grace . ' days grace has been granted. ';
            }
        } else {
            $output .= 'This Toy is overdue by ' . $session . ' sessions, your fine is $' . $total_fine . '.  ';
            if ($grace > 0) {

                $output .= '' . $grace . ' sessions grace has been granted. ';
            }
        }
    }

    return array('fine_status' => $output, 'amount' => $total_fine);
}

function fill_loan_array($idcat, $loanperiod, $date_start) {
    $c = array();

    for ($xi = 0; $xi <= $loanperiod; $xi++) {
        $str = '+' . $xi . 'day';
        $curr = strtotime($str, strtotime($date_start));
        $str_array = date("Y-m-d", $curr);
        //echo $str_array;
        array_push($c, $str_array);
    }
    return $c;
}

function fill_reservations_array($idcat) {

    $conn = pg_connect($_SESSION['connect_str']);

    $query = "select * from reserve_toy where idcat = '" . $idcat . "' order by date_start;";

    $a1 = array();

    $result1 = pg_Exec($conn, $query);
    $numrows = pg_numrows($result1);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = pg_fetch_array($result1, $ri);
            $date_end = date_create_from_format('Y-m-d', $row['date_end']);
            $date_start = date_create_from_format('Y-m-d', $row['date_start']);
            $interval = $date_start->diff($date_end);
            $no_days = $interval->format('%a');

            for ($xi = 0; $xi <= $no_days; $xi++) {
                $str = '+' . $xi . 'day';
                $curr = strtotime($str, strtotime($row['date_start']));
                $str_array = date("Y-m-d", $curr);
                array_push($a1, $str_array);
            }
        }
    }
    return $a1;

    pg_FreeResult($result1);

    pg_Close($conn);
}

function return_today($idcat, $borid) {
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "SELECT Count(id) AS countid FROM transaction 
            WHERE (date_loan::date)=(now()::date) AND ((idcat)='" . $idcat . "') AND ((borid)=" . $borid . ')';
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $return = $row['countid'];
    return $return;
}

function max_toys($membertype) {
    $conn = pg_connect($_SESSION['connect_str']);

    $sql = "SELECT maxnoitems AS max FROM membertype 
          WHERE (membertype)='" . $membertype . "'";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $max_toys = $row['max'];
    return $max_toys;
}

function max_jounal_id() {

    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "SELECT MAX(id) as newid FROM journal;";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $max = $row['newid'] + 1;
    return $max;
}

function count_loans($borid) {
    //include( dirname(__FILE__) . '/../connect.php');
    $conn = pg_connect($_SESSION['connect_str']);
    $loan_type = 'Standard';

    $sql = "SELECT Count(id) AS noloans FROM transaction
    WHERE borid=" . $borid . " AND return Is Null;";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $total = $row['noloans'];
    $total = $total + 1;
    $status = '';
    return array("status" => $status, "total" => $total, "loan_type" => $loan_type);
}

function check_trans($idcat) {

    $conn = pg_connect($_SESSION['connect_str']);

    $sql = "select Count(id) AS countid FROM transaction where idcat='" . $idcat . "' AND return Is Null;";
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $trans = $row['countid'];
    //$trans = 4;
    return $trans;
}

function get_borid($idcat) {

    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "select borid AS borid FROM transaction where idcat='" . $idcat . "' AND return Is Null;";
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $trans_borid = $row['borid'];
    $sql = "SELECT * From Borwrs WHERE id =" . $trans_borid . ";";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $link = '<a class="button_small_red" href="loan.php?b=' . $row['id'] . '"> ' . $row['id'] . ' </a>';
    $longname = $row['firstname'] . ' ' . $row['surname'] . ' (' . $link . ')';
    //$trans = 4;
    return $longname;
}

function return_toy($id, $recordfine, $chargerent, $weekly_fine, $fine_value, $grace, $rentasfine, $max_daily_fine, $unlock_returns) {
    $timezone = $_SESSION['settings']['timezone'];
    $location = $_SESSION['location'];
    $monday = $_SESSION['settings']['monday'];
    $tuesday = $_SESSION['settings']['tuesday'];
    $wednesday = $_SESSION['settings']['wednesday'];
    $thursday = $_SESSION['settings']['thursday'];
    $friday = $_SESSION['settings']['friday'];
    $saturday = $_SESSION['settings']['saturday'];
    $sunday = $_SESSION['settings']['sunday'];
    $fine_factor = $_SESSION['settings']['fine_factor'];
    $overdue_fine = $_SESSION['settings']['overdue_fine'];

    $special_return = $_SESSION['settings']['special_return'];

    $success = 'No';
    $status_fine = '';

    $status = '';
    $toy = get_toy($id);
    $parts = get_part($toy['idcat']);

    $idcat = $toy['idcat'];
    $alert = $toy['alert'];
    if ($parts['alertparts'] != '') {
        $alert .= $parts['alertparts'] . '<br>';
    }
    
    date_default_timezone_set($timezone);
    $currentTime = date('H:i:s');

    $query_return = "UPDATE transaction Set return = now()::date , timetrans = '" . $currentTime . "', returnby = '" . $_SESSION['username'] . "'
                         ,location = '" . $location . "' WHERE id = " . $id . ";";

    $query_return .= "UPDATE toys set STATUS = FALSE, ";
    if ($unlock_returns == 'Yes') {
        $query_return .= " stocktake_status = 'ACTIVE',";
        $status .= ' This toy has been unlocked. ';
    }
    $query_return .= " location = '" . $_SESSION['location'] . "' WHERE idcat = '" . $toy['idcat'] . "';";

    //$id = $_GET['id'];

    if ($chargerent == 'Yes') {
        $status .= delete_rent($toy['idcat'], $toy['borid']);
    }


    $now = date('Y-m-d');
    $start = date_create_from_format('Y-m-d', $toy['due']);
    $end = date_create_from_format('Y-m-d', $now);
    $result_fine = array();
    $result_fine['amount'] = 0;
    $new_id = max_jounal_id();
    echo 'Fine value: ' . $fine_value;
    //include( dirname(__FILE__) . '/data/get_settings.php');
    //include( dirname(__FILE__) . '/special/get_settings.php');
    //$status .= 'Special Return: ' . $special_return;
    if ($special_return == 'Yes') {

        if ($rentasfine == 'Yes') {
            if ($toy['rent'] > 0) {
                if ($fine_value > 0) {
                    $fine_value = $fine_value * $toy['rent'];
                } else {
                    $fine_value = $toy['rent'];
                }
            }
            if ($fine_factor != '') {
                $fine_value = $fine_factor * $fine_value;
            }
        }


        if ($toy['loan_type'] != '') {
            $fine_amount = get_loantype($toy['loan_type']);
            //$status .= '<br>Amount: ' . $fine_amount;
            if ($fine_amount > 0) {
                $fine_value = $fine_amount;
            }
        }
        if ($start < $end) {
            $daily_total = 0;
            $daily_total = check_daily($toy['borid']);
            $daily_start = $daily_total;
            if ($max_daily_fine != 0) {
                if ($daily_total < $max_daily_fine) {
                    $result_fine = calculate_fine_special($toy['due'], $sunday, $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $weekly_fine, $fine_value, $grace, $max_daily_fine);
                    $status_fine = $result_fine['fine_status'];
                    $daily_total = $daily_total + $result_fine['amount'];
                    if ($daily_total >= $max_daily_fine) {
                        $result_fine['amount'] = $max_daily_fine - $daily_start;
                        $status_fine .= '<br><font color="blue">You have reached your Daily Limit, this fine has been capped at $' . $result_fine['amount'] . ' .</font><br>';
                    }
                }
            } else {
                $result_fine = calculate_fine_special($toy['due'], $sunday, $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $weekly_fine, $fine_value, $grace, $max_daily_fine);
                $status_fine = $result_fine['fine_status'];
                if ($daily_min_fine > $result_fine['amount']) {
                    $result_fine['amount'] = 0;
                    $status .= '<font color="red">The daily fine Value is less than $' . $daily_min_fine . ' , fine has been waivered.</font><br>';
                    //$daily_total = $daily_min_fine;
                    $status_fine = null;
                }
                if (($fine_per_member == 'Yes') && ($daily_total > 0)) {
                    $result_fine['amount'] = 0;
                    $status_fine = null;
                }
            }

            if (($recordfine == 'Yes' ) && ($result_fine['amount'] > 0)) {
                $query_return .= "insert into journal(datepaid, bcode, icode,name, description, category, amount, type, typepayment)
                VALUES (now(),
                '{$toy['borid']}',
                '{$toy['idcat']}',
                '{$toy['longname']}',
                '{$toy['toyname']}',
                'Overdue Fine',
                {$result_fine['amount']},
                'DR', 'Debit'
            );";
            }
        }
    }

    if (($start <= $end) && ($special_return != 'Yes')) {
        if ($rentasfine == 'Yes') {
            if ($toy['rent'] > 0) {
                if ($fine_value > 0) {
                    $fine_value = $fine_value * $toy['rent'];
                } else {
                    $fine_value = $toy['rent'];
                }
            }
            if ($fine_factor != '') {
                $fine_value = $fine_factor * $fine_value;
            }
        }
        if ($overdue_fine == 'Yes') {
            $result_fine = overdue($toy['rent'], $toy['due'], $toy['borid'], $toy['reservecode']);
        } else {
            $result_fine = calculate_fine($toy['due'], $sunday, $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $weekly_fine, $fine_value, $grace);
        }

        $status_fine .= $result_fine['fine_status'];
        if (($recordfine == 'Yes') && ($result_fine['amount'] > 0)) {
            $query_return .= "insert into journal(datepaid, bcode, icode,name, description, category, amount, type, typepayment)
                VALUES (now(),
                '{$toy['borid']}',
                '{$toy['idcat']}',
                '{$toy['longname']}',
                '{$toy['toyname']}',
                'Fine',
                {$result_fine['amount']},
                'DR', 'Debit'
            );";
        }
    }

    $conn = pg_connect($_SESSION['connect_str']);
    $result = pg_exec($conn, $query_return);

    if (!$result) {
        $message = 'Invalid query: ' . "\n";
        $message = 'Whole query: ' . $query;
        $message = $query_return;
        //die($message);
        $status .= $message;
        $success = 'No';
    } else {
        $status .= $toy ['idcat'] . ': ' . $toy ['toyname'] . ' has been returned.';
        $status_due = ' Due: ' . $toy['due'];
        $success = 'Yes';
    }
    return array("status" => $status, "status_due" => $status_due, "status_fine" => $status_fine, "result" => $success, "idcat" => $idcat, "alert" =>
        $alert, "sql" => $query_return);
}

function get_toy($id) {
    $conn = pg_connect($_SESSION['connect_str']);
    $sql_trans = "SELECT transaction.*, toys.alert as alert, toys.rent as rent, toys.reservecode as reservecode, toys.returndateperiod as loanperiod, toys.loan_type as loan_type FROM transaction 
    left join toys on toys.idcat = transaction.idcat WHERE transaction.id=" . $id . ";";

    $nextval = pg_Exec($conn, $sql_trans);
    $trans = pg_fetch_array($nextval, 0);
    $toyname = clean($trans['item']);
    $due = $trans['due'];
    $borid = $trans['borid'];
    $reservecode = $trans['reservecode'];
    if ($reservecode == null) {
        $reservecode = '';
    }
    $longname = clean($trans['borname']);
    $idcat = $trans['idcat'];
    $alert = clean($trans['alert']);
    $date_loan = $trans['date_loan'];
    $rent = $trans['rent'];
    $loanperiod = $trans['loanperiod'];
    $loan_type = $trans['loan_type'];

    //$idcat = $['noloans'];
    //return array('var1'=>$var1,'var2'=>$var2);
    return array('toyname' => $toyname, 'due' => $due, 'borid' => $borid, 'idcat' => $idcat, 'loan_type' => $loan_type,
        'longname' => $longname, 'alert' => $alert, 'date_loan' => $date_loan, 'rent' => $rent, 'loanperiod' =>
        $loanperiod, 'reservecode' => $reservecode);
}

function get_part($id) {
    $alertparts = '';

    $conn = pg_connect($_SESSION['connect_str']);
    $sql_trans = "SELECT * from parts where itemno = '" . $id . "' and alertuser = TRUE ORDER by type, datepart;";
    $result = pg_Exec($conn, $sql_trans);
    $numrows = pg_numrows($result);
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result, $ri);
        $alertparts .= '<br>' . $row ['type'] . ': ' . $row['description'];
    }
    return array('alertparts' =>
        $alertparts);
}

function get_loantype($loan_type) {

    $amount = 0;
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "select overdue from stype where stype='" . $loan_type . "';";
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $amount = $row['overdue'];
    if ($amount == '') {
        $amount = 0;
    }
    return $amount;
}

function delete_rent($idcat, $borid) {

    $outcome = '';
    $conn = pg_connect($_SESSION['connect_str']);
    $sql = "DELETE from journal WHERE 
    (datepaid::date)=(now()::date) AND ((icode)='" . $idcat . "') AND ((bcode)=" . $borid . ") AND ((category)= 'Rent');";
    $result_delete = pg_Exec($conn, $sql);
    if (!$result_delete) {
        $outcome = 'cannot delete rent:' . $query;
    }
    return $outcome;
}

function charge_rent($global_rent, $loanperiod, $id) {
    $toy = get_toy($id);
    $idcat = $toy['idcat'];
    $totalrent = 0;
    $conn = pg_connect($_SESSION['connect_str']);
    $status = '';
    $query_renew = '';

    if ($global_rent == 0) {
        if ($toy['rent'] > 0) {
            //$totalrent = round(( $toy['rent'] * $loanperiod / 7), 2);
            $totalrent = $toy['rent'];
            $query_renew .= "insert into journal( datepaid, bcode, icode,name, description, category, amount, type, typepayment)
            VALUES (now(),
            '{$toy['borid']}',
            '{$toy['idcat']}',
            '{$toy['longname']}',
            '{$toy['toyname']}',
            'Rent Renewal',
            {$totalrent},
            'DR', 'Debit'
        );";
        }
    } else {
        $totalrent = round(( $global_rent * $loanperiod / 7), 2);
        $query_renew .= "insert into journal( datepaid, bcode, icode, name, description, category, amount, type, typepayment)
            VALUES (now(),
            '{$toy['borid']}',
            '{$toy['idcat']}',
            '{$toy['longname']}',
            '{$toy['toyname']}',
            'Rent',
            {$totalrent},
            'DR', 'Debit'
        );";
    }
    if ($totalrent > 0) {
        $status = ' Rent of $' . $totalrent . ' has been charged.';
    }
    return array("status" => $status, "idcat" => $idcat, 'sql_str' => $query_renew);
}

function renew_toy($id, $loanperiod, $times_renew, $chargerent, $global_rent, $holiday_override, $holiday_due) {
    $query_renew = '';
    $success = 'No';
    $conn = pg_connect($_SESSION['connect_str']);
    $status = '';
    $toy = get_toy($id);
    $idcat = $toy['idcat'];
    $alert = $toy['alert'];
    $rent = $toy['rent'];
    $ts1 = strtotime($toy['due']);
    $ts2 = strtotime($toy['date_loan']);
    $days_diff = -1 * round(( $ts2 - $ts1 ) / ( 60 * 60 * 24), 0);
    $renew = 'No';

    if ($toy['loanperiod'] != 0) {
        if ($loanperiod > $toy['loanperiod']) {
            $loanperiod = $toy['loanperiod'];
        }
    }

    $max = $loanperiod * $times_renew + $loanperiod;
    //$str_due = strtotime($due + 'interval ' . $loanperiod . ' days');
    $str_due = strtotime("+" . $loanperiod . " days", strtotime($toy['due']));
    //$format_str_due = substr($str_due, 8, 2) . '-' . substr($str_due, 5, 2) . '-' . substr($str_due, 0, 4);

    $format_str_due = date('d M Y', $str_due);

    if ($days_diff < $max) {
        $query_renew = "UPDATE transaction SET due = (due + interval '" . $loanperiod . " days'), "
                . " timetrans = now()::time where id= " . $id . ";";
        if ($chargerent == 'Yes') {
            $rent = charge_rent($global_rent, $loanperiod, $id);
            $query_renew .= $rent['sql_str'];
        }
        $renew = 'Yes';
    }

    if (($holiday_override == 'Yes') && ($holiday_due != '')) {
        $query_renew = "UPDATE transaction SET due =  '" . $holiday_due . "', "
                . " timetrans = now()::time where id= " . $id . ";";
        $status = '';
        $format_str_due = $holiday_due;
    }

    if ($renew == 'Yes') {


        $result = pg_exec($conn, $query_renew);

        if (!$result) {
            $message = 'Invalid query: ' . "\n";
            $message = $query_renew;
            //die($message);
            $status .= $message;
            $success = 'No';
        } else {
            $status .= $toy ['idcat'] . ': ' . $toy ['toyname'] . ' has been renewed. Due: ' . $format_str_due . '.  <font color="red">' . $toy ['alert'] . '</font>';
            $success = 'Yes';
        }
    }
    if ($renew == 'No') {
        $status .= 'Cannot renew ' . $idcat . ': ' . $toy ['toyname'] . ', it has already been renewed ' . $times_renew . ' time(s).';
    }

    return array("status" => $status, "result" => $success, "idcat" => $idcat, "alert" =>
        $alert);
}

function clean($input) {
    $output = stripslashes($input);
    $output = str_replace("'", "`", $output);
    $output = str_replace('/', '-', $output);
    //$output = str_replace("\", " ", $output);
    return

            $output;
}

Function check_alert($alert) {
    if ($alert != '') {
        echo '<script>
        $(document).ready(function(){
		alert("' . $alert .
        '");});
        </script>';
    }
}

function check_if_reserved($idcat, $loanperiod) {
    $conn = pg_connect($_SESSION['connect_str']);
    $query = "select * from reserve_toy where idcat = '" . $idcat . "' and date_end > current_date order by date_start;";
    $numrows = 0;
    $reserve_ok = 'Yes';
    $reserved = 'No';
    $due = '';
    $delete_reserve = 'No';
    $result1 = pg_Exec($conn, $query);
    $numrows = pg_numrows($result1);
    //$_SESSION['loan_status'] .= $query . '<br>' . $numrows;
    if ($numrows > 0) {

        $reserve_ok = 'No';
        $loan_days_str = '';
        $date_start = date('Y-m-d');
        for ($xi = 0; $xi <= $loanperiod; $xi++) {
            $str = '+' . $xi . 'day';
            $curr = strtotime($str, strtotime($date_start));
            $str_date = date("Y-m-d", $curr);
            if ($xi != ($loanperiod)) {
                $loan_days_str .= "'" . $str_date . "',";
            } else {
                $loan_days_str .= "'" . $str_date . "'";
            }
        }
        $query_res = "select * from reserve_toy where date_start IN (" . $loan_days_str . ") 
            and date_end IN (" . $loan_days_str . ") and idcat = '" . $idcat . "';";
        $result2 = pg_Exec($conn, $query_res);
        $check_date_range = pg_numrows($result2);
        if ($check_date_range == 1) {
            $row = pg_fetch_array($result2, 0);
            $due = $row['date_end'];
            $reserved = 'Yes';
            //$delete_reserve = 'Yes';
            //$_SESSION['loan_status'] .= 'The reservation on this Toy has been deleted<br>';
        }
        $_SESSION['loan_status'] .= $nummrows;
    }
    return array('reserved' => $reserved, 'delete_reserve' => $delete_reserve, 'due' => $due);
}

function term_duties($term_dates, $borid, $expired, $member_expiryperiod) {
    $conn = pg_connect($_SESSION['connect_str']);
    $str_month = '-' . $member_expiryperiod . ' months';
    $start = $expired . $str_month;
    $sql_total = "select sum(duration) as total from roster where "
            . "member_id = " . $borid . " and status != 'no show' and "
            . "date_roster >= '" . $expired . "' and date_roster <= '" . $expired . "' group by member_id;";

    $result = pg_exec($conn, $sql_total);
    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = pg_fetch_array($result, $ri);
        $duties_completed = $row['total'];
    }
    return array('sql' => $sql_total);
}

function loan_special($borid, $idcat, $library_code, $category, $expired) {
    
    
    
    $max = 0;
    $status = '';
    $count_extra = 0;
    $total = 0;
    $loan_type = 'Special';

    $sql = "select transaction.idcat as idcat, borid, toys.category, borwrs.membertype as membertype, membertype.maxnoitems as max,
            membertype.extra, loan_restrictions.loan_type
            from transaction 
            left join toys on toys.idcat = transaction.idcat
            left join borwrs on borwrs.id = transaction.borid
            left join membertype on borwrs.membertype = membertype.membertype
            left join loan_restrictions on toys.category = loan_restrictions.category
            where return is null
            and borid = ? 
            UNION
            select ?, borwrs.id as borid, ?, borwrs.membertype as membertype, membertype.maxnoitems as max,
            membertype.extra, loan_restrictions.loan_type 
            from borwrs
            left join membertype on borwrs.membertype = membertype.membertype
            left join loan_restrictions on loan_restrictions.category = ?
            where borwrs.id = ?;";
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $sth = $pdo->prepare($sql);
    $array = array($borid, $idcat, $category, $category, $borid);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        if ($row['loan_type'] == 'EXTRA') {
            $count_extra = $count_extra + 1;
            $status .= '** Toy ' . $row['idcat'] . ' from Category ' . $row['category'] . ' is an extra toy. ';
        }
        $max = $row['max'];
        $extra = $row['extra'];
        $membertype = $row['membertype'];
    }

    if ($count_extra > $extra) {
        $count_extra = $extra;
        $status .= '** Limit of  ' . $extra . ' extra toy(s). ';
    }
    $total = $numrows - $count_extra;
    $adjust_total = $numrows - $count_extra;
    $total_status .= '<br>(Adjusted Total: ' . $adjust_total . ')';
    if ($library_code == 'bayside') {
        $visits = 'ok';
        if (($membertype == 'Grandparent') || ($membertype == 'School Kids')) {
            $visits = count_visits($expired, $borid);
            $status .= $visits;
        }
    }

    return array("total" => $total, "status" => $status, "sql" => $sql, "total_status" => $total_status, "visits" => $visits,"loan_type" => $loan_type);
}

function count_visits($expired, $borid) {
    
    
    
    $status = 'ok';
    //echo $expired;
    $sql = "select date_loan, count(id) as toys from transaction 
        where return is not null 
        and borid = ?
        and date_loan > date '" . $expired . "' - interval '12 months'
        group by date_loan order by date_loan";
    //echo $sql . ' <br>';
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $count_loans = 0;
    $sth = $pdo->prepare($sql);
    $array = array($borid);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        $count_loans = $row['toys'] + $count_loans;
    }
    //echo $numrows;
    if ($numrows > 5) {
        $status = "This member has visited the library <font color='blue'>" . $numrows . "</font> times, which exceeds the number of allowed visits (5), and has loaned <font color='blue'>" . $count_loans . "</font> toys since joining or renewing.<br>";
    } else {
        $status = 'ok';
    }
    //$status = $sql;
    return $status;
}

function count_gold_star($borid, $max_gold_star, $override) {
    
    
    
    $msg = '';
    $status = 'No';
    $sql = "select transaction.id as id  
    from transaction 
    left join toys on toys.idcat = transaction.idcat
    where return is null 
    and trim(loan_type) = 'Gold Star'
    and borid = ?;";
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $sth = $pdo->prepare($sql);
    $array = array($borid);
    $sth->execute($array);
    $numrows = $sth->rowCount();
    if ($numrows >= $max_gold_star) {
        if ($override != 'Yes') {
            $msg = "This member has <font color='blue'>" . $numrows . "</font> Gold Star toys, cannot borrow any more Gold Star toys.<br>";
        } else {
            $status = 'ok';
            $msg = "This member has <font color='blue'>" . $numrows . "</font> Gold Star toys.<br><font color='red'>OVERRIDE IS ON, this Toy has been Borrowed</font><br>";
        }
    } else {
        $status = 'ok';
    }
    return array('ok' => $status, 'msg' => $msg);
}

function count_by_cat($borid, $idcat, $weight, $roster_today) {
    
    
    
    $status = '';
    $count_hire = 0;
    $total_gold = 0;
    $gold_ok = 'Yes';

    $sql = "select t.idcat as idcat, 
    y.reservecode as hiretoy, 
    y.category as category, 
    y.loan_type as toy_loan_type,
    coalesce(l.weight, 1) as weight, 
    coalesce(l.free,0) as free, 
    coalesce(y.rent) as rent,
    l.loan_type as loan_type, 
      c.description as cat_desc,
    (select free from loan_restrictions where category = 'DUTY') as free_duty, 
    (select weight from loan_restrictions where category = 'HIRE') as hire_weight, 
    (select free from loan_restrictions where category = 'LIMIT') as limit_extra,
    (select free from loan_restrictions where loan_type = 'GOLD') as limit_gold 
    from transaction
    t left join toys y on (y.idcat = t.idcat) 
    left join loan_restrictions l on (y.category = l.category) 
    left join category c on (y.category = c.category) 
     where borid = ? and return is null
     UNION 
    select t.idcat as idcat, t.reservecode as hiretoy, t.category as category, 
    t.loan_type as toy_loan_type,
    coalesce(l.weight, 1) as weight, 
    coalesce(l.free,0) as free, 
    coalesce(t.rent) as rent,
    l.loan_type as loan_type, 
    c.description as cat_desc, 
      (select free from loan_restrictions where category = 'DUTY') as free_duty, 
    (select weight from loan_restrictions where category = 'HIRE') as hire_weight, 
    (select free from loan_restrictions where category = 'LIMIT') as limit_extra,
    (select free from loan_restrictions where loan_type = 'GOLD') as limit_gold 
   from toys t left join loan_restrictions l on (t.category = l.category) 
    left join category c on (t.category = c.category) 
    where idcat = ? order by category;";
//$sql = "select * from toys;";

    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $sth = $pdo->prepare($sql);
    $array = array($borid, $idcat);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();
    //echo $numrows;
    $total = 0;
    $cat_total = 0;
    $count_extra = 0;
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        if ($row['hiretoy'] == null) {
            $weight = $row['weight'];
        } else {
            if ($row['rent'] == 0) {
                $weight = $row['weight'];
                $weight = 1;
                //echo 'hello';
            } else {
                $weight = $row['hire_weight'];
                //$weight = 0;
                if (($weight == 0) && ($row['rent'] > 0)) {
                    $count_hire = $count_hire + 1;
                    if ($count_hire == 1) {
                        $status .= ' ** Hire Toys are extra toys. ';
                    }
                }
            }
        }
        $category = $row['category'];

        if ($category != $last_category) {
            $cat_total = $weight;
        } else {
            $cat_total = $cat_total + $weight;
        }
        if ($cat_total > $row['free']) {
            $total = $total + $weight;
        } else {
            if ($row['hiretoy'] == null) {
                $status .= '** Toy from Category ' . $row['cat_desc'] . ' is an extra toy. ';
            }
        }
        if (trim($row['loan_type']) == 'extra') {
            if ($row['limit_extra'] != 0) {
                if ($row['limit_extra'] <= $count_extra) {
                    $total = $total + $weight;
                    $status .= '** Limit of  ' . $row['limit_extra'] . ' extra toy(s). ';
                } else {
                    $count_extra = $count_extra + $weight;
                }
            }
        }
        if (trim($row['toy_loan_type']) == 'GOLD') {
            if ($row['limit_gold'] > 0) {
                $total_gold = $total_gold + $weight;
                if ($total_gold > $row['limit_gold']) {
                    $status .= '** Limit of  ' . $row['limit_gold'] . ' GOLD toy(s). ';
                    $gold_ok = 'No';
                }
            }
        }



        $last_category = $category;
        //echo $row['idcat'] . ' Total: ' . $total . '<br>';
    }
    if ($row['free_duty'] > 0) {
        $total = $total - $row['free_duty'];
        //$total_out = $total - $row['free_duty'];
        $status .= ' ** Extra Toy has been added for Duty member. ';
    }

    $total_status .= ' (Adjusted Total: ' . $total . ')';
    return array("status" => $status, "total" => $total, "gold_ok" => $gold_ok, "total_status" => $total_status, "sql" => $sql);
}

function rent_by_membertype($membertype, $rent_in) {
    $rent = 0;
    
    
    
    $sql = "select * from loan_restrictions where "
            . " membertype = ? and (loan_type = 'RENT' or loan_type = 'FREE RENT') LIMIT 1";
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $sth = $pdo->prepare($sql);
    $array = array($membertype);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();


    if ($numrows > 0) {
        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = $result[$ri];
            $rent = $row['weight'];
            $loan_type = $row['loan_type'];
            //$status = 'hello';
        }
    } else {
        $rent = $rent_in;
    }
    if ($rent_in > $rent) {
        $rent = $rent_in;
    }
    if ($loan_type == 'FREE RENT') {
        $rent = 0;
        $status = 'FREE RENT - no Rent has been charged for member type ' . $membertype . '.';
    }

    //$status = $rent_in . ' ' . $rent;
    //$status .= $numrows;
    return array("rent" => $rent, "status" => $status, "sql" => $sql, "rent_in" => $rent_in);
}

function extra_rent($rent_in) {
    
    
    
    $rent = 0;
    $rent_extra = 0;

    $sql = "select * from loan_restrictions where "
            . "  loan_type = 'RENT_EXTRA' LIMIT 1";
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $sth = $pdo->prepare($sql);
    $array = array();
    $sth->execute($array);

    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();


    if ($numrows > 0) {
        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = $result[$ri];
            $rent_extra = $row['weight'];
        }
    }
    $rent = $rent_in + $rent_extra;
    if ($rent_extra > 0){
          $status = 'Extra $' . $rent_extra . ' has been charged.';  
    }


    //$status = $rent_in . ' ' . $rent;
    return array("rent" => $rent, "status" => $status, "sql" => $sql, "rent_in" => $rent_in);
}

?> 
