<?php

require(dirname(__FILE__) . '/../../mibase_check_login.php');

function overdue($rent, $due, $borid, $reservecode) {

    $fine = 0;
    $weekly_fine = 'Yes';
    $rent_as_fine = 'No';
    $weeks = 0;
    $fine_value = 0;
    $days_grace = 0;
    $hire_factor = 0;
    $fine_factor = 1;
    $daily_limit = 0;
    $extra_fine = 0;
    $returned_today = 0;
    $today = 0;
    $status = '';
    $custom = '';

    $now = date('Y-m-d');
    $date_start = date_create_from_format('Y-m-d', $due);
    $date_end = date_create_from_format('Y-m-d', $now);
    $interval = $date_start->diff($date_end);
    $no_days = $interval->format('%a');

    $sql = "select overdue.*,
            coalesce((select sum(amount) as total from journal where datepaid = current_date 
            and category = 'Fine' and bcode = ?),0) as today,
            coalesce((select count(id) from transaction 
            where return = current_date and due < current_date and borid = ?),0) as returned_today 
            from overdue where id = 1";
    
    
    

    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    $sth = $pdo->prepare($sql);
    $array = array($borid, $borid);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    if ($numrows > 0) {
        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = $result[$ri];
            $weekly_fine = $row['weekly_fine'];
            $rent_as_fine = $row['rent_as_fine'];
            $fine_value = $row['fine_value'];
            $days_grace = $row['days_grace'];
            $fine_factor = $row['fine_factor'];
            $hire_factor = $row['hire_factor'];
            $daily_limit = $row['daily_limit'];
            $daily_fine = $row['daily_fine'];
            $today = $row['today'];
            $extra_fine = $row['extra_fine'];
            $roundup_week = $row['roundup_week'];
            $returned_today = $row['returned_today'];
            $custom = $row['custom'];
        }
    }
    if (($rent_as_fine == 'Yes') && ($rent > 0)) {
        $fine_value = $rent;
        if ($extra_fine > 0) {
            $fine_value = $fine_value + $extra_fine;
        }
    }
    if ($weekly_fine == 'Yes') {
        if ($roundup_week == 'Yes') {
            $weeks = ceil(($no_days - $days_grace) / 7);
        } else {
            $weeks = round(($no_days - $days_grace) / 7);
        }

        if ($weeks < 0) {
            $weeks = 0;
        }
        $fine = round($weeks * $fine_value * $fine_factor, 2);

        If (($daily_fine == 'Yes') && ($returned_today > 0)) {
            $fine = 0;
        }
        if ($daily_limit > 0) {
            //$status = 'Daily limit not = 0. ';
            if ($today >= $daily_limit) {
                $fine = 0;
            } else {
                if (($fine + $today) >= $daily_limit) {
                    $fine = $daily_limit - $today;
                    $status .= '<br><font color="blue">You have reached your Daily Limit, this fine has been capped at $' . $fine . ' .</font><br>';
                }
            }
        }
        if ($custom == 'carlson') {
            if ($rent > 0) {
                $extra_custom = round($weeks * $rent, 2);
                $fine = $fine + $extra_custom;
                $status .= '<br><font color="blue">Extra Rent has been charged for this toy, $' . $rent . ' per week overdue.</font><br>';
            }
        }

        if ($fine > 0) {
            if ($daily_fine == 'Yes') {
                $status .= 'These Toy(s) are ' . $weeks . ' weeks overdue and have accrued a $' . $fine . ' fine - please see the Coordinator.';
            } else {
                $status .= ' This Toy is overdue by ' . $weeks . ' weeks, your fine is $' . $fine . '. <br>';
            }
        }
        if ($days_grace > 0 and ( $fine > 0)) {
            $status .= '  ' . $days_grace . ' days grace has been granted.<br> ';
        }
    } else {
        $fine = 0;
    }

    //$status .= 'No days: ' . $no_days . '. Weeks: ' . $weeks . ' Days Grace: ' . $days_grace;
    if (($hire_factor > 1) && ($reservecode != '')) {
        $fine = $hire_factor * $fine;
        if ($fine > 0) {
            $status .= 'This toy is a Hire Toy, your fine of $' . $fine . ' has been multiplied by a factor of ' . $hire_factor . '!<br>';
        }
    }
    //$status .= 'Fine: ' . $fine . ' Hire Factor: ' . $hire_factor;

    return array("amount" => $fine, "fine_status" => $status, "sql" => $sql);
}
