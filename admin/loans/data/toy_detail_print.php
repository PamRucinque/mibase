<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

//get configuration data
//include( dirname(__FILE__) . '/../config.php');


//$id = $_GET['id'];
$idcat = $_SESSION['idcat'];
$hold_str = '';
$alert_sml = '';
//echo "id:" . $id;
//include 'get_toy.php';


$sub_category_loans = $_SESSION['settings']['sub_category_loans'];
$stocktake_override = $_SESSION['settings']['stocktake_override'];

$desc1 = str_replace("\r\n", "</br>", $desc1);
$desc2 = str_replace("\r\n", "</br>", $desc2);

$check_reserve = check_reserve($idcat);


if ($check_reserve > 0) {
    $str_detail_reserve = '<h3 id="reserved">RESERVED</h3>';

    if ($_SESSION['borid'] != $check_reserve) {
        $reserved = 'Yes';
    }
    //$reserved = 'Yes';
}

if ($_SESSION['shared_server']) {
    $pic_url = $_SESSION['web_server_protocol'] . "://" . $_SESSION['host'] . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . '.jpg';
} else {
    $pic_url = $_SESSION['web_server_protocol'] . "://" . $_SESSION['host'] . $_SESSION['toy_images_location'] . "/" . strtolower($idcat) . '.jpg';
}

if ($_SESSION['shared_server']) {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . "/" . strtolower($idcat) . '.jpg';
} else {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . "/" . strtolower($idcat) . '.jpg';
}

if (file_exists($file_pic)) {
    $img = '<img height="75px" src="' . $pic_url . '" alt="Toy Image">';
} else {
    $img = ' ';
}

$str_detail = '<h3><font color="blue">' . $idcat;
if ($sub_category_loans == 'Yes') {
    $str_detail .= ' ' . $sub_category;
}
$str_detail .= ': </font>  ' . $toyname . '</h3> ';

if ($toy_exists == 'No') {
    // echo '<h3><font color="red">This Toy Does Not Exist.</font></h3> ';
} else {
    if ($toy_status != 'ACTIVE') {
        if ($toy_status == 'PROCESSING') {
            $str_detail .= '<h3><font color="red">This Toy is being PROCESSED.</font></h3> ' . $toy_status;
        } else {
            $str_detail .= '<h3><font color="red">This Toy is LOCKED or WITHDRAWN.</font></h3> ' . $toy_status;
        }
    }
    if ($stocktake_status == 'LOCKED') {
        if ($stocktake_override != 'Yes') {
            $str_detail .= '<h3><font color="red">This Toy is LOCKED for Stocktake.</font></h3> ' . $stocktake_status;
        } else {
            $str_detail .= $stocktake_status . ' ';
            $linktotoy_unlock = '  <a class="button_small_yellow" href="checked.php?idcat=' . $idcat . '">Unlock ' . $idcat . '</a>';
            $str_detail .= $linktotoy_unlock . '<br>';
        }
    }
    $str_detail .= $hold_str;
}

include( dirname(__FILE__) . '/get_trans.php');

//$reserved = check_reserve($idcat);
$link = '<a class="button_small_red" href="loan.php?b=' . $bor_id . '"> ' . $bor_id . ' </a>';
$loan_to_current = ' <a class="button_small_yellow" href="loan_to_current.php?transid=' . $trans_id . '">Return ' . $_SESSION['idcat'] . '</a>';

if (($returndate == Null && $numrows > 0)) {
    $str_detail .= $trans_id . ' <font color="red">On Loan: </font> ' . $borname . ': ' . $link;
    $str_detail .= $loan_to_current;
    // $str_loan_button = '<td bgcolor="lightblue">';
} else {
    $loan_btn = '<td bgcolor="lightblue">
                <form id="loantoy" method="post" action="loan.php">
                    <input align="center" id="loan" class="btn btn-colour-yellow" type="submit" name="loan" value="Loan Toy" />
                  </form>';
    // <input type="hidden" name="subject" value="' . $error_out . '" />



    $str_loan_button = $loan_btn;
}


$str_loan_button .= '</td>';
if ($reserved == 'Yes') {
    //$str_loan_button = '<td bgcolor="lightblue">USE THE LOAN TOY BUTTON <br>IN RESERVATIONS LIST<br> OR MAKE A RESERVATION <br> TO LOAN THIS TOY</td>';
}
?>
<?php

$str_detail .= '<p><font color="red">' . $alert_sml . '</font></p>';


if ($rent > 0) {
    if ($rentasfine == 'Yes') {
        if ($chargerent != 'Yes') {
            if ($disable_strrent != 'Yes') {
                $str_detail .= '<p><font color="green">Overdue Amount: ' . $rent . '</font></p>';
            }
        }
    } else {
        $str_detail .= '<p><font color="red">Rent: ' . $rent . '</font></p>';
    }
}


if (isset($_SESSION['idcat'])) {
    $str_pic = '<br><a style="padding-right:10px">' . $img . '</a>';
}

function check_reserve($idcat) {
    $reserve_memid = 0;
    $conn = pg_connect($_SESSION['connect_str']);
    $query = "select * from reserve_toy where idcat = '" . $idcat . "' and date_end >= current_date order by date_start;";
    $numrows = 0;
    $result1 = pg_Exec($conn, $query);
    $numrows = pg_numrows($result1);
    if ($numrows > 0) {
        $row = pg_fetch_array($result1, 0);
        $reserve_memid = $row['member_id'];
    }
    return $reserve_memid;
}
?>





