<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

?>
<br>
<table bgcolor="lightgreen" width ="100%">
    <tr><td colspan = "3"><h3>Update Alerts</h3></td></tr>
    <tr>
        <td width="10px"></td>
        <td valign ="top">
            <form  id="alert" method="post" action="" width="100%">
                <textarea id="member_alert" name="member_alert"  style="background-color:#F3F781;"  rows="2" cols="30"><?php echo $alert_member; ?></textarea>
                <br><input id="submit_alert" class="button1_yellow"  type="submit" name="submit_alert" value="Save Member Alert" />

            </form>  
            <?php echo $table_events; ?>
        </td>
        <td width ="5%"></td>
        <td valign ="top">
            <form  id="alert_toy" method="post" action="" width="100%">
                <textarea id="toy_alert" name="toy_alert"  style="background-color:#F3F781;"  rows="2" cols="30"><?php echo $alert_toy; ?></textarea>
                <br><input id="submit_alert_toy" class="button1_yellow"  type="submit" name="submit_alert_toy" value="Save Toy Alert" />

            </form>    

        </td>
        <td width ="5%"></td>
        <td valign ="top">

            <form  id="alert_part" method="post" action="" width="100%">

                <input type="Text" id="part_alert" name="part_alert"  align="LEFT"  size="30" style="background-color:#F3F781;"></input>
                <br><br><input id="submit_alert_part" class="button1_yellow"  type="submit" name="submit_alert_part" value="Save Part Alert" />

            </form> 
            <?php echo $table_parts; ?>
        </td>
        <td width="10px"></td>

    </tr></table>

