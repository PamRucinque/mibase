<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');
$conn = pg_connect($_SESSION['connect_str']);
$query_settings = "SELECT * FROM settings;";
$result_settings = pg_Exec($conn, $query_settings);

$numrows = pg_numrows($result_settings);
$unlock_returns = 'No';
$max_toys_pa = 0;
$grace = 0;
$fine_factor = 1;
$rent_factor = 1;
$extra_rent = 0;
$roster_alerts = '';
$toy_holds = '';
$sub_category_loans = '';
$hide_balance = '';
$vol_balance = '';
$display_overdues = '';
$admin_notes = '';
$holiday_override = '';
$renew_member_off = '';
$group_trans = '';
$disable_returnall = '';
$special_loan = 'No';
$special_return = 'No';
$auto_debit_missing = 'No';
$max_daily_fine = 0;


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_settings, $ri);
    if ($row['setting_name'] == 'free_rent_btn') {
        $free_rent_btn = $row['setting_value'];
    }

    if ($row['setting_name'] == 'toy_holds') {
        $toy_holds = $row['setting_value'];
    }
    if ($row['setting_name'] == 'toy_hold_period') {
        $toy_hold_period = $row['setting_value'];
    }

    if ($row['setting_name'] == 'sub_category_loans') {
        $sub_category_loans = $row['setting_value'];
    }
    if ($row['setting_name'] == 'toy_hold_fee') {
        $toy_hold_fee = $row['setting_value'];
    }
    //$toy_hold_fee

    if ($row['setting_name'] == 'loan_special') {
        $loan_special = $row['setting_value'];
    }
    if ($row['setting_name'] == 'loan_default_missing_paid') {
        $loan_default_missing_paid = $row['setting_value'];
    }
        if ($row['setting_name'] == 'loan_default_missing_alert') {
        $loan_default_missing_alert = $row['setting_value'];
    }
    //
    //renew_loan_period
    //stocktake_override
    if ($row['setting_name'] == 'stocktake_override') {
        $stocktake_override = $row['setting_value'];
    }
    //$show_desc
    if ($row['setting_name'] == 'show_desc') {
        $show_desc = $row['setting_value'];
    }
    if ($row['setting_name'] == 'loanperiod') {
        $loanperiod = $row['setting_value'];
        $renew_loan_period = $loanperiod;
    }
    if ($row['setting_name'] == 'renew_loan_period') {
        $renew_loan_period = $row['setting_value'];
        if ($renew_loan_period == 0) {
            $renew_loan_period = $loanperiod;
        }
    }
    if ($row['setting_name'] == 'loan_restrictions') {
        $loan_restrictions = $row['setting_value'];
    }
    if ($row['setting_name'] == 'rent_factor') {
        $rent_factor = $row['setting_value'];
    }
        if ($row['setting_name'] == 'extra_rent') {
        $extra_rent = $row['setting_value'];
    }
    if ($row['setting_name'] == 'hide_value') {
        $hide_value = $row['setting_value'];
    }
    //hide_value

    if ($row['setting_name'] == 'overdue_fine') {
        $overdue_fine = $row['setting_value'];
    }
    //reservation_alert
    if ($row['setting_name'] == 'reservation_alert') {
        $reservation_alert = $row['setting_value'];
    }
    if ($row['setting_name'] == 'reservations') {
        $reservations = $row['setting_value'];
    }

    if ($row['setting_name'] == 'group_trans') {
        $group_trans = $row['setting_value'];
    }
//hide_balance
    if ($row['setting_name'] == 'disable_returnall') {
        $disable_returnall = $row['setting_value'];
    }
    if ($row['setting_name'] == 'hide_balance') {
        $hide_balance = $row['setting_value'];
    }
    if ($row['setting_name'] == 'receipt_email') {
        $receipt_email = $row['setting_value'];
    }
    if ($row['setting_name'] == 'email_from') {
        $email_from = $row['setting_value'];
    }
    if ($row['setting_name'] == 'disable_strrent') {
        $disable_strrent = $row['setting_value'];
    }
    if ($row['setting_name'] == 'renew_member_off') {
        $renew_member_off = $row['setting_value'];
    }

    if ($row['setting_name'] == 'display_overdues') {
        $display_overdues = $row['setting_value'];
    }

    if ($row['setting_name'] == 'duty_alert') {
        $duty_alert = $row['setting_value'];
    }
    if ($row['setting_name'] == 'special') {
        $special = $row['setting_value'];
    }
    if ($row['setting_name'] == 'receipt_printer') {
        $receipt_printer = $row['setting_value'];
    }
    if ($row['setting_name'] == 'special_loan') {
        $special_loan = $row['setting_value'];
    }
    if ($row['setting_name'] == 'special_return') {
        $special_return = $row['setting_value'];
    }
    if ($row['setting_name'] == 'term_dates') {
        $term_dates = $row['setting_value'];
    }
    if ($row['setting_name'] == 'term_alert') {
        $term_alert = $row['setting_value'];
    }
    if ($row['setting_name'] == 'next_term_alert') {
        $next_term_alert = $row['setting_value'];
    }
    if ($row['setting_name'] == 'roster_alerts') {
        $roster_alerts = $row['setting_value'];
    }
    if ($row['setting_name'] == 'weekly_fine') {
        $weekly_fine = $row['setting_value'];
    }
    if ($row['setting_name'] == 'fine_value') {
        $fine_value = $row['setting_value'];
    }
    if ($row['setting_name'] == 'catsort') {
        $catsort = $row['setting_value'];
    }
    if ($row['setting_name'] == 'libraryname') {
        $libraryname = $row['setting_value'];
    }
    if ($row['setting_name'] == 'toy_reserve') {
        $toy_reserve = $row['setting_value'];
    }
    if ($row['setting_name'] == 'format_toyid') {
        $format_toyid = $row['setting_value'];
    }
    if ($row['setting_name'] == 'loanperiod') {
        $loanperiod = $row['setting_value'];
    }
    if ($row['setting_name'] == 'recordfine') {
        $recordfine = $row['setting_value'];
    }
    if ($row['setting_name'] == 'rentasfine') {
        $rentasfine = $row['setting_value'];
    }
    if ($row['setting_name'] == 'monday') {
        $monday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'tuesday') {
        $tuesday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'wednesday') {
        $wednesday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'thursday') {
        $thursday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'monday') {
        $friday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'tuesday') {
        $saturday = $row['setting_value'];
    }

    if ($row['setting_name'] == 'wednesday') {
        $sunday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'rent') {
        $global_rent = $row['setting_value'];
    }
    if ($row['setting_name'] == 'chargerent') {
        $chargerent = $row['setting_value'];
    }
    if ($row['setting_name'] == 'reserve_fee') {
        $reserve_fee = $row['setting_value'];
    }

    if ($row['setting_name'] == 'loan_receipt') {
        $loan_receipt = $row['setting_value'];
    }
    if ($row['setting_name'] == 'times_renew') {
        $times_renew = $row['setting_value'];
    }
    if ($row['setting_name'] == 'grace') {
        $grace = $row['setting_value'];
    }
    if ($row['setting_name'] == 'new_member_alert') {
        $new_member_alert = $row['setting_value'];
    }
    if ($row['setting_name'] == 'fortnight') {
        $fortnight = $row['setting_value'];
    }
    if ($row['setting_name'] == 'max_toys_pa') {
        $max_toys_pa = $row['setting_value'];
    }
    if ($row['setting_name'] == 'reservations') {
        $settings_reservations = $row['setting_value'];
    }
    if ($row['setting_name'] == 'checked') {
        $checked = $row['setting_value'];
    }
    if ($row['setting_name'] == 'member_alerts') {
        $member_alerts = $row['setting_value'];
    }
    if ($row['setting_name'] == 'holiday_override') {
        $holiday_override = $row['setting_value'];
    }
    if ($row['setting_name'] == 'vol_expired_off') {
        $vol_expired_off = $row['setting_value'];
    }
    if ($row['setting_name'] == 'vol_notes') {
        $vol_notes = $row['setting_value'];
    }
    if ($row['setting_name'] == 'vol_balance') {
        $vol_balance = $row['setting_value'];
    }
    if ($row['setting_name'] == 'vol_membertype') {
        $vol_membertype = $row['setting_value'];
    }
    if ($row['setting_name'] == 'admin_notes') {
        $admin_notes = $row['setting_value'];
    }
    //toy_barcode_notrim
    if ($row['setting_name'] == 'toy_barcode_notrim') {
        $toy_barcode_notrim = $row['setting_value'];
    }

    if ($row['setting_name'] == 'max_daily_fine') {
        $max_daily_fine = $row['setting_value'];
    }
    if ($row['setting_name'] == 'fine_factor') {
        $fine_factor = $row['setting_value'];
    }
    if ($row['setting_name'] == 'holiday_due') {
        $holiday_due = $row['setting_value'];
    }
    //$unlock_returns
    if ($row['setting_name'] == 'unlock_returns') {
        $unlock_returns = $row['setting_value'];
    }
    //auto_debit_missing
    if ($row['setting_name'] == 'auto_debit_missing') {
        $auto_debit_missing = $row['setting_value'];
    }
    //$disable_cc_email

    if ($row['setting_name'] == 'disable_cc_email') {
        $disable_cc_email = $row['setting_value'];
    }
}

//echo 'cat sort from settings: ' . $catsort . '<br>';/
pg_FreeResult($result_settings);
// Close the connection
pg_Close($conn);
?>

