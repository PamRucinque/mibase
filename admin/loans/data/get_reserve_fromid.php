<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');
//connect to MySQL

$conn = pg_connect($_SESSION['connect_str']);
$reservation = 'No';
$query = "SELECT * from reserve_toy WHERE id = " . $rid . ";";
//$_SESSION['loan_status'] .= $query;
$result_reserve = pg_Exec($conn, $query);
$numrows = pg_numrows($result_reserve);
if ($numrows > 0) {
    $reservation = 'Yes';
}
//echo $query;

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_reserve, $ri);
    $reserve_id = $row['id'];
    $reserve_borid = $row['member_id'];
    $reserve_borname = $row['borname'];
    $reserve_start = $row['date_start'];
    $rend = $row['date_end'];


    pg_FreeResult($result_reserve);
// Close the connection
    pg_Close($conn);
}
?>