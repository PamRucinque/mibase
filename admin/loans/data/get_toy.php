<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) .  '/../../mibase_check_login.php');
$toy_holds = $_SESSION['settings']['toy_holds'];

//include( dirname(__FILE__) . '/../connect.php');
$last = $_SESSION['idcat'];

$idcat = $_SESSION['idcat'];

$query_toy = "SELECT toys.*,
            coalesce(l.weight, 1) as weight ,coalesce(t.onloan,0) as onloan, t.id as transid 
            from toys 
            left join loan_restrictions l on (toys.category = l.category) 
            left join (select count(id) as onloan, id, idcat from transaction where return is null group by idcat,id) t on t.idcat = toys.idcat 
            WHERE upper(toys.idcat) = '" . strtoupper($idcat) . "'";
$conn = pg_connect($_SESSION['connect_str']);
$result_toy = pg_Exec($conn, $query_toy);
$numrows = pg_numrows($result_toy);
$status_txt = Null;

if ($numrows == 0) {
    $toy_exists = 'No';
    $_SESSION['idcat'] = $last;
} else {
    $toy_exists = 'Yes';
    $_SESSION['idcat'] = $idcat;
}


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_toy, $ri);
//$desc2 = str_replace("\r\n", "</br>", $desc2);

    $toy_id = $row['id'];
    $weight = $row['weight'];
    $idcat = $row['idcat'];
    $category = $row['category'];
    $onloan = $row['onloan'];
    $transid = $row['transid'];
    $toyname = str_replace("'", "`", $row['toyname']);
    $rent = $row['rent'];
    $reservecode = $row['reservecode'];
    $status = $row['status'];
    $stocktake_status = trim($row['stocktake_status']);
    $returndateperiod = $row['returndateperiod'];
    $toy_loanperiod = $row['returndateperiod'];
    $returndate = $row['returndate'];
    $no_pieces = $row['no_pieces'];
    $warnings = $row['warnings'];
    $user1 = $row['user1'];
    //$date_entered = date_format($row['date_entered'], 'd/m/y');
    $alert = $row['alert'];
    $alert_toy = $row['alert'];
    $alert_toy_text = $row['alert'];

    $toy_status = $row['toy_status'];
    $stype = $row['stype'];
    $loan_type = $row['loan_type'];
    $units = $row['units'];
    $borrower = $row['borrower'];
    $lockdate = $row['datestocktake'];
    $sub_category = $row['sub_category'];
    $lockreason = $row['lockreason'];
    $created = $row['created'];
    $modified = $row['modified'];
    $condition = $row['reservedfor'];
    $desc1 = $row['desc1'];
    $desc2 = $row['desc2'];
    $format_purchase = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    $format_lock = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);

    pg_FreeResult($result_toy);
// Close the connection
    pg_Close($conn);



    if ($status == 't') {
        $status_txt .= 'ON LOAN';
    } else {
        $status_txt .= 'IN LIBRARY';
    }
}
include ('../header_detail/parts.php');
if ($alert_parts != '') {
    $alert_toy_text = $alert . '<br>' . $alert_parts_txt;
    $alert = $alert . '<br>' . $alert_parts;
}
if (($toy_holds == 'Yes')&&($_SESSION['mibase_server'] == 'Yes')) {
    include( dirname(__FILE__) . '/hold/get_toy_holds.php');
}


//echo $extra;
//echo 'Type Claim: ' . $type_claim;
//$date_submitted = $date_submitted[weekday]. $date_submitted[month] . $date_submitted[mday] . $date_submitted[year];
?>