<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//get configuration data
//include(__DIR__ . '/../../config.php');

//get settings


$memberid = $_SESSION['borid'];

$sql = "SELECT transaction.*, toys.no_pieces as pieces,
toys.desc1 as desc1,
toys.desc2 as desc2,
toys.alert as toy_alert,
array_to_string(
 array(  SELECT (coalesce(p.qty,1) || ' X ' || p.description)
  FROM parts p
  WHERE p.itemno = toys.idcat and p.type = 'Missing') ,
 ','
 ) AS missing
FROM transaction 
LEFT JOIN toys on transaction.idcat = toys.idcat 
WHERE borid = " . $memberid . " AND return Is Null ORDER BY created DESC;";

$x = 0;
$count = 1;
$dbconn = pg_connect($_SESSION['connect_str']);
$trans = pg_exec($dbconn, $sql);
$x = pg_numrows($trans);
$toyslist = '';
$str_receipt_short = '';
$toyslist_pieces = '';


for ($ri = 0; $ri < $x; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($trans, $ri);
    $idcat = $row['idcat'];
    $pieces = $row['pieces'];
    $desc1 = $row['desc1'];
    $desc2 = $row['desc2'];
    $toy_alert = $row['toy_alert'];
    $missing = $row['missing'];
    $format_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);

    $toyslist .= '<font color="blue">' . $row['idcat'] . ':  ' . $row['item'] . '    </font><font color="red">DUE: ' . $format_due . '</font>  No Pieces: ' . $pieces . '<br>';
    
    $str_receipt_short .= '<font color="blue">' . $row['idcat'] . ':  ' . $row['item'] . '    </font><font color="red">DUE: ' . $format_due . '</font><br>';




 
if ($_SESSION['shared_server']) {
    $pic_url = $_SESSION['web_server_protocol'] . "://" . $_SESSION['host'] . $_SESSION['toy_images_location'] . "/" . $_SESSION['library_code'] . "/" . strtolower($idcat) . '.jpg';
} else {
    $pic_url = $_SESSION['web_server_protocol'] . "://" . $_SESSION['host'] . $_SESSION['toy_images_location'] . "/" . strtolower($idcat) . '.jpg';
}

if ($_SESSION['shared_server']) {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . "/" . strtolower($idcat) . '.jpg';
} else {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . "/" . strtolower($idcat) . '.jpg';
}
    if (file_exists($file_pic)) {
        $toyslist .= '<img height="100px" src="'.$pic_url. '" alt=""><br>';
    } else {
        $toyslist .= '';
    }
    if (file_exists($file_pic)) {
        $str_receipt_short .= '<img height="100px" src="'.$pic_url. '" alt=""><br>';
    } else {
        $str_receipt_short .= '';
    }

    if ($missing != '') {
        $toyslist .= '<font color="green">Missing: ' . $missing . '</font><br>';
    }
    $toyslist_pieces .= '<font color="blue">' . $row['idcat'] . ':  ' . $row['item'] . '    </font><font color="red">DUE: ' . $format_due . '</font>  No Pieces: ' . $pieces . '<br>';

    if (file_exists($file_pic)) {
        $toyslist_pieces .= '<img height="100px" src="'.$pic_url. '" alt=""><br>';
    } else {
        $toyslist_pieces .= '';
    }
    if ($missing != '') {
        $toyslist_pieces .= '<font color="green">Missing: ' . $missing . '</font><br>';
    }
    $toyslist_pieces .= $desc1 . '<br>' . $desc2 . '<br>' . '<font color="red">' . $toy_alert . '</font><br>';
}

$toyslist_pieces = str_replace("\n", "<br>", $toyslist_pieces);

//pg_close($link);
?>


