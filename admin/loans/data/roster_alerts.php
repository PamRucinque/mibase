<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

$id = $_SESSION['borid'];
$ref2 = '';

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$sql = "SELECT * from event where memberid = ? and alertuser = TRUE and "
        . "(typeevent = 'roster_event' or typeevent = 'alert' or typeevent = 'term_alert' "
        . "or typeevent = 'helmet_alert' or typeevent = 'user_alert')";

$sth = $pdo->prepare($sql);
$array = array($id);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

if ($stherr[0] != '00000') {
    $error_msg .= "An  error occurred.\n";
    $error_msg .= 'Error' . $stherr[0] . '<br>';
    $error_msg .= 'Error' . $stherr[1] . '<br>';
    $error_msg .= 'Error' . $stherr[2] . '<br>';
}

//$str_roster = '';
$table_events = '';

if ($numrows > 0) {
    $table_events .= '<table border="1" width="100%" style="border-collapse:collapse; border-color:lightgrey;background-color: whitesmoke;">';
    //$table_events .= '<tr style="border-color:lightgrey;background-color: lightgreen;"><td>id</td><td>Desc</td><td>Alert</td></tr>';

    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = $result[$ri];
        if ($row['description'] != '') {
            if (($row['typeevent'] == 'term_alert')) {
                $str_roster .= '<font color="darkgreen">' . $row['description'] . '</font>';
            }

            if (($row['typeevent'] == 'roster_event')) {
                $str_roster .= '<br><font color="darkgreen">' . $row['description'] . '</font>';
            }
            if ($row['typeevent'] == 'user_alert') {
                $str_roster .= '<font color="blue">' . $row['description'] . '</font><br>';
                $ref1 = '<td width="60px"><form action="" method="POST"><input type="hidden" name="id" value="' . $row['id'] . '">';
                $ref1 .='<input id="delete_event" name="delete_event" class="button_small_red"  type="submit" value="Delete" /></form></td>';
                $table_events .= '<tr id="red"><td align="center">' . $row['id'] . '</td>';
                //$table_events .= '<td align="left">' . $row['typeevent'] . '</td>';
                $table_events .= '<td align="left">' . $row['description'] . '</td>';
                $table_events .= $ref1;
                $table_events .= '</tr>';
            }
        }
    }
    $table_events .= '</table>';
}



