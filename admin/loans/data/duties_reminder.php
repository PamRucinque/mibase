<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

//get settings



$reminder = '';
$reminder_txt = '';


$query_duties = "select id, date_roster, weekday, roster_session from roster where member_id = " . $_SESSION['borid'] .
        " and date_roster <= (current_date + INTERVAL '21 days') 
    and date_roster >= (current_date)  and type_roster != 'Exemption' order by date_roster desc;";
//echo $query_duties;

$conn = pg_connect($_SESSION['connect_str']);
$result = pg_exec($conn, $query_duties);
$numrows = pg_num_rows($result);

if ($numrows > 0) {
    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = pg_fetch_array($result, $ri);
        $reminder = substr($row['date_roster'], 8, 2) . '-' . substr($row['date_roster'], 5, 2) . '-' . substr($row['date_roster'], 0, 4);
        $weekday = $row['weekday'];
        $session = $row['roster_session'];
    }
    if ($_SESSION['borid'] != 0) {
        $reminder_txt = '<br><font color="blue">This member has a Roster Duty on  ' . $weekday . '  ' . $reminder . ' from ' . $session . '.</font><br>';
    }
    $receipt_roster = 'Reminder: Roster Duty on  ' . $weekday . '  ' . $reminder . ' from ' . $session . '.<br>';

    $extra_duty = 'Yes';
    $str_roster .= $reminder_txt;
}






