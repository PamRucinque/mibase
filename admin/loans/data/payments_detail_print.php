<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
$hide_balance = $_SESSION['settings']['hide_balance'];
$vol_balance = $_SESSION['settings']['vol_balance'];
$hide_value =  $_SESSION['settings']['hide_value'];
$admin_notes =  $_SESSION['settings']['admin_notes'];
$display_overdues =  $_SESSION['settings']['display_overdues'];
$type_str = '';


if ($hide_balance != 'Yes') {
    if (isset($membertype)){
           $type_str = '<br><font color="blue"> ' . $membertype . '</font>';
    }
  
    $balance = balance($_SESSION['borid']);
    $total = $balance['total'];
    $total_fine = $balance['overdue_fine'];
    if ($total < 0) {
        $total_str = '<font color="red">$' . sprintf('%01.2f', $total) . '</font>';
     } else {
        $total_str = '<font color="black">$' . sprintf('%01.2f', $total) . '</font>';
    }

    if ($hide_value != 'Yes'){
           
           $value_str = ' <font color="green"> $ ' . sprintf('%01.2f', $total_value) . '</font>';
           $total_str .= '<font color="green">  Value: </font>' . $value_str;
    }
 
    if ($total_fine < 0) {
        $totalfine_str = '<font color="red">$' . sprintf('%01.2f', $total_fine) . '</font>';
    } else {
        $totalfine_str = '<font color="black">$' . sprintf('%01.2f', $total_fine). '</font>';
    }
    if ($vol_balance != 'Yes') {
        //echo '<h3><font color="black">Balance: </font>' . $total_str . $type_str . '</h3>';
    }
    //if (($branch == 'admin')) {
        echo '<h3><font color="black">Balance: </font>' . $total_str . $type_str . '</h3>';
        $receipt_balance = '<h3><font color="black">Balance: </font>' . $total_str . '</h3>';
        $payments = $balance['payments'];

    //}
}else{
    $type_str = '<font color="blue"> ' . $membertype . '</font><br>';
    $type_str .= ' <font color="green">     Value $ ' . sprintf('%01.2f', $total_value) . '</font>';
    echo '<h3>' .  $type_str . '</h3>';
}


if ($admin_notes != 'Yes') {
    echo '<font color="black">' . $notes . '</font><br>';
}
echo '<font color="red">' . $alert_mem_txt . '</font>';
echo $str_roster;

//echo '<a class="button1_orange" href="../payments/payments.php">Payments</a>';

function balance($borid) {
    $connection_str = $_SESSION['connect_str'];
    $conn = pg_connect($connection_str);

    $total = 0;
    $overdue_fine = 0;
    $sql = "select id, surname, firstname,
    (SELECT COALESCE(sum(amount),0)  FROM journal
        WHERE type='CR' and upper(category) = 'OVERDUES' and journal.bcode = borwrs.id) as cr_fine,
     (SELECT COALESCE(sum(amount),0)  FROM journal
        WHERE type='DR' and upper(category) = 'FINE' and journal.bcode = borwrs.id) as dr_fine,  
     (SELECT COALESCE(sum(amount),0)  FROM journal
        WHERE type='CR' and journal.bcode = borwrs.id) as cr,
     (SELECT COALESCE(sum(amount),0)  FROM journal
        WHERE type='DR' and journal.bcode = borwrs.id) as dr
    from borwrs where id = " . $borid . ";";
    $result1 = pg_Exec($conn, $sql);
    $row = pg_fetch_array($result1);

    $total = $row['cr'] - $row['dr'];
    $overdue_fine = $row['cr_fine'] - $row['dr_fine'];

    $sql = "select * from journal where type = 'CR' and datepaid = current_date";
    $result = pg_Exec($conn, $sql);
    $todays_payments = '';
    $numrows = pg_numrows($result);

    if ($numrows > 0) {
        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = pg_fetch_array($result, $ri);
            $todays_payments .= $row['category'] . ' ' . $row['description'] . ': $' . sprintf('%01.2f', $row['amount']) . ' ' . $row['typepayment'] . '<br>';
        }
    }
    return array('total' => $total, 'payments' => $todays_payments, 'overdue_fine' => $overdue_fine);
}

?>
