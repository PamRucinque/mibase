<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

if (isset($_GET['term'])) {
    $return_arr = array();

    //include( dirname(__FILE__) . '/../../connect.php');
    $term = '%' . strtoupper($_REQUEST['term']) . '%';

    $sql = "SELECT idcat, toyname FROM toys WHERE ((upper(idcat) LIKE ? OR upper(user1) LIKE ? OR (upper(toyname) LIKE ?) OR (upper(toyname) LIKE ? 
            )) AND (toy_status='ACTIVE' OR 
            toy_status ='PROCESSING')) ORDER by category, id ASC;";

    //
    //
    //
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $sth = $pdo->prepare($sql);
    $array = array($term, $term, $term, $term);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();


//echo '<option value="" selected="selected"></option>';
    $q = strtolower($_GET["term"]);
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        $return_arr[] = $row['idcat'] . ': ' . $row['toyname'];
        $results[] = array('label' => $row['idcat'] . ': ' . $row['toyname'], 'value' => $row['idcat']);
    }

    /* Toss back results as json encoded array. */
    echo json_encode($results);
}
?>