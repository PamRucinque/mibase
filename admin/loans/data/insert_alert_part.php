<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');
$loan_default_missing_paid = $_SESSION['settings']['loan_default_missing_paid'];
$auto_debit_missing = $_SESSION['settings']['auto_debit_missing'];
$loan_default_missing_alert = $_SESSION['settings']['loan_default_missing_alert'];


include( dirname(__FILE__) . '/../functions/functions.php');
include( dirname(__FILE__) . '/new_partid.php');
include('../../connect.php');
//include( dirname(__FILE__) . '/data/get_member.php');
$conn = pg_connect($_SESSION['connect_str']);
$parttype = 'Missing';
if ($_SESSION['borid'] == '') {
    $borid = 0;
} else {
    $borid = $_SESSION['borid'];
}
$now = date('Y-m-d');
$_POST['part_alert'] = clean($_POST['part_alert']);
$description = $_POST['part_alert'];
$paid_str = 'FALSE';
if ($loan_default_missing_paid == 'Yes') {
    $paid_str = 'TRUE';
}
$alert_str = 'FALSE';
if ($loan_default_missing_alert == 'Yes') {
    $alert_str = 'TRUE';
}

$query_alert = "INSERT INTO parts (id, itemno, description, type, cost, borcode, datepart, alertuser, paid)
         VALUES ({$newpartid}, 
        '{$_SESSION['idcat']}', 
        '{$description}', '{$parttype}', 0, {$borid},
        now(), " . $alert_str . ", " . $paid_str . ");";

//echo $query_alert;

//$query_alert = "insert into parts set alert = '" . $_POST['toy_alert'] . "' where idcat = '" . $_SESSION['idcat'] . "';";
$result = pg_Exec($conn, $query_alert);
if (($auto_debit_missing == 'Yes') && ($borid != 0)) {
    // $payment_amount = get_missing_part();
    $payment_amount = 1;
    $payment_amount = get_missing_part();

    if ($idcat != '') {
        if ($description == '') {
            $description = $idcat . ': ' . $toyname;
        } else {
            $description = $idcat . ': ' . $toyname . ': ' . $description;
        }
    } else {
        $description = 'No Current Toy';
    }

    $category = 'Fine: Missing Part';

    if (isset($_POST['borname'])) {
        $borname = $_POST['borname'];
    }
    //$borname = str_replace("'", "`", $_POST['borname']);
    $saved .= '\n' . 'Missing Fine has been debited';
    $_SESSION['loan_status'] .= 'Missing Part amount debited: ' . add_to_journal($now, $_SESSION['borid'], $_SESSION['idcat'], $longname, $description, $category, $payment_amount, 'DR', 'Cash');
}

function add_to_journal($datepaid, $bcode, $icode, $borname, $description, $category, $amount, $type, $typepayment) {

    
    
    
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

    $query_payment = "INSERT INTO journal (datepaid, bcode, icode, name, description, category, amount, type, typepayment)
                 VALUES (?,?,?,?,?,?,?,?,?);";

    $sth = $pdo->prepare($query_payment);
    $array = array($datepaid, $bcode, $icode, $borname, $description, $category, $amount,
        $type, $typepayment);

    //$result_payment = pg_Exec($conn, $query_payment);
    if ($amount > 0) {
        $sth->execute($array);
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {

            $result = 'Error' . $stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>' . $query_payment;
            exit;
        } else {
            $result = 'The Membership fee of ' . $amount . ' has been added';
        }
    } else {
        $result = 'Amount needs to be greater than 0';
    }

    return $result;
}

function get_missing_part() {
    //include( dirname(__FILE__) . '/../connect.php');
    $sql = "SELECT * FROM paymentoptions  
          WHERE upper(paymentoptions)='MISSING PART';";
    $conn = pg_connect($_SESSION['connect_str']);
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $fee = $row['amount'];
    return $fee;
}
