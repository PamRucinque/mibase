<?php
$alert_parts = '';
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');
include(get_include_path() . $path . 'connect.php');
$str_parts = '';
$alert_parts = '';
$alert_parts_txt = '';
$table_parts = '';
$idcat = $_SESSION['idcat'];

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$sql = "SELECT * from parts where itemno = ? ORDER by type, datepart";

$sth = $pdo->prepare($sql);
$array = array($idcat);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

if ($stherr[0] != '00000') {
    $error_msg .= "An  error occurred.\n";
    $error_msg .= 'Error' . $stherr[0] . '<br>';
    $error_msg .= 'Error' . $stherr[1] . '<br>';
    $error_msg .= 'Error' . $stherr[2] . '<br>';
}

$status_txt = Null;




if ($numrows > 0) {
//echo '<h2> Missing Parts: </h2>';
    $str_parts .= '<table border="0" width="100%" style="border-collapse:collapse; border-color:grey">';
//echo '<tr><td>id</td><td>Date</td><td>Details</td><td>type</td><td>alert</td><td>cost</td><td></td><tr>';
    $table_parts .= '<table border="1" width="100%" style="border-collapse:collapse; border-color:lightgrey;background-color: whitesmoke;">';
    $table_parts .= '<tr style="border-color:lightgrey;background-color: lightgreen;"><td>Alert</td><td>Description</td></tr>';
}else{
    $alert_return = '';
}


for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = $result[$ri];

    
    $format_datepart = substr($row['datepart'], 8, 2) . '-' . substr($row['datepart'], 5, 2) . '-' . substr($row['datepart'], 0, 4);
    $alert_txt = '';
    if ($row['alertuser']) {
        $alert_txt .= 'Yes';
    } else {
        $alert_txt .= 'No';
    }


    $str_parts .= '<tr><td width="30" align="center">' . $row['id'] . '</td>';
    //echo '<td width="30">' . $format_datepart . '</td>';
    $str_parts .= '<td width="70" align="left">' . $row['type'] . '</td>';
    $str_parts .= '<td align="left">' . $row['description'] . '</td>';
    if ($row['alertuser']) {
        $alert_parts .= $row['type'] . ': ' . $row['description'] . '<br>';
        $alert_parts_txt .= $row['type'] . ': ' . $row['description'] . '<br>';
        $alert_return .= $row['type'] . ': ' . $row['description'] . '<br>';
    }

    $str_parts .= '</tr>';


    $ref2 = 'delete_part.php?partid=' . $row['id'] . '&alert=' . $alert_txt;


    //$table_parts .= '<tr id="red"><td align="center">' . $row['id'] . '</td>';
    $table_parts .= '<tr><td>';
       $table_parts .= '<a class ="button_small" href="' . $ref2 . '">' . $alert_txt . '</a></td>';
    $table_parts .= '<td align="left"><font color="red">' . $row['description'] . '</font></td>';
 
    $table_parts .= '</tr>';
}
if ($numrows > 0) {

    $str_parts .= '</table>';
    $table_parts .= '</table>';
}
//echo $table_parts;
?>

