<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');

$query = "SELECT borwrs.*, membertype.maxnoitems as max, membertype.duties as duties, 
    membertype.returnperiod as member_returnperiod,
    membertype.expiryperiod as member_expiryperiod,
    membertype.gold_star as gold_star,
    membertype.due as member_due, membertype.extra as extra,
          (select max(date_roster) as next from roster where roster.member_id = borwrs.id and roster.complete = FALSE and date_roster > now()) as next,
        (SELECT coalesce(sum(roster.duration),0) FROM roster WHERE roster.member_id = borwrs.id AND (roster.date_roster >= (borwrs.expired - (membertype.expiryperiod * '1 month'::INTERVAL))) and roster.complete = TRUE)  as completed, 
    (select count(id) from event where borwrs.id = event.memberid and typeevent = 'No Overdue Fine') as alert_off,
    (SELECT COALESCE(sum(amount),0)  FROM journal WHERE type='CR' and journal.bcode = borwrs.id) as cr,  
    (SELECT COALESCE(sum(amount),0)  FROM journal WHERE type='DR' and journal.bcode = borwrs.id) as dr,
    (select count(id) from roster where date_roster = current_date and member_id = borwrs.id) as roster_today
    FROM membertype RIGHT JOIN borwrs ON membertype.membertype = borwrs.membertype 
    WHERE borwrs.id = " . $_SESSION['borid'] . " AND member_status = 'ACTIVE'";

$conn = pg_connect($_SESSION['connect_str']);
$result_member = pg_Exec($conn, $query);
$numrows = pg_numrows($result_member);
$last_mem = $_SESSION['borid'];
$alert_mem = '';
$alert_member = '';
$member_returnperiod = 0;
$notes = '';
$next_duty = '';
$alert_mem_expired = '';
//echo $query;



for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_member, $ri);

    $expired = $row['expired'];
    $roster_today = $row['roster_today'];
    $balance = $row['cr'] - $row['dr'];
    if ($balance > 0){
     $balance = '$ ' . sprintf('%01.2f', $row['cr'] - $row['dr']);   
     //sprintf('%01.2f', $total_value)
    }else{
       $balance = '<font color="red"> $ ' . sprintf('%01.2f', $row['cr'] - $row['dr']) . '</font>';   
       //sprintf('%01.2f', $row['cr'] - $row['dr'])
    }
    
    $format_expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
    $format_expired_receipt = substr($row['expired'], 8, 2) . '/' . substr($row['expired'], 5, 2) . '/' . substr($row['expired'], 0, 4);

    $borid = $row["id"];
    $firstname = str_replace("'", "`", $row["firstname"]);
    if ($row['emailaddress'] == ''){
        $email = 'No Email';
    }else{
        $email = $row['emailaddress'];
    }
    
    $membertype = $row["membertype"];
    $surname = str_replace("'", "`", $row['surname']);
    $partnersname = str_replace("'", "`", $row["partnersname"]);
    $partnerssurname = str_replace("'", "`", $row['partnerssurname']);
    $member_returnperiod = $row['member_returnperiod'];
    $member_expiryperiod = $row['member_expiryperiod'];
    $member_gold_star = $row['gold_star'];
    $duties = $row['duties'];
    $email2 = $row['email2'];
    $mobile1 = $row['phone2'];
    $completed = $row['completed'];
    $member_due = $row['member_due'];
    $max = $row['max'];
    $extra = $row['extra'];
    if ($next_duty != ''){
       $next_duty = 'Next Duty: ' . $row['next']; 
    }
    
    $completed = $row['completed'];
    if ($member_returnperiod != 0){
        $loanperiod = $member_returnperiod;
    }

    $alert_mem = $row['alert'];
    $alert_mem_txt = $row['alert'];
    $alert_member = $row['alert'];
    if ($row['notes'] != '') {
        $notes = 'Notes: ' . $row['notes'];
    }

    $user1 = $row['location'];
    $member_status = $row['member_status'];
    if ($partnersname == null || $partnerssurname == null) {
        $longname = $firstname . ' ' . $surname;
    } else {
        $longname = $firstname . ' ' . $surname . ' and ' . $partnersname . ' ' . $partnerssurname;
    }
    //$longname = $borid . ': ' . $longname;

    $email_link = '<a class="button_small_red" href="mailto:' . $email . '">send email</a>';
    $email_link2 = '<a class="button_small_red" href="mailto:' . $email2 . '">send email</a>';
    $alert_off = $row['alert_off'];
}

$now = date('Y-m-d');

$now = time(); // or your date as well
$exp = strtotime($expired);
$diff = ($exp - $now) / (60 * 60 * 24);

if ($diff > 0) {
    $format_expired_warning = '<h3><font color="darkgreen">Expires: ' . $format_expired . '</font></h3>';
}
if ($diff >= 0 && $diff <= 30) {
    $format_expired_warning = '<h3 id="due_alert">Due to Expire on: ' . $format_expired . '</h3>';
}
if ($diff < 0) {
    $format_expired_warning = '<h3 id="expired_alert">Expired on: ' . $format_expired . '</h3>';
    $alert_mem_expired = 'This membership has Expired!';
}
if ($numrows == 0) {
    $longname = 'Member id not found!';
    //$longname = $query;
    $format_expired_warning = ' ';
    $alert_mem_expired = 'This member number is invalid ! ' . $_SESSION['borid'];
    $_SESSION['borid'] = 0;
    //$_SESSION['error'] = $query;
}
if ($alert_mem_expired != '') {
    if ($alert_mem != '') {
        $alert_mem_txt = $alert_mem . ' ' . $alert_mem_expired;
    } else {
        $alert_mem_txt = $alert_mem_expired;
    }

    $alert_mem .= '\n' . $alert_mem_expired;
}
//include( dirname(__FILE__) . '/events.php');



?>
