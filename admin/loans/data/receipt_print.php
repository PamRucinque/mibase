
<div id="printableArea" media="print" style = "display: none; visibility: hidden; padding-left: 20px;">
    <table width="65%"><tr><td>
                <?php
                include( dirname(__FILE__) . '/data/get_template.php');
                //style = "display: none; visibility: hidden;"
                //echo '<h1>' . $_SESSION['libraryname'] .  '</h1><br>';
                //$str_receipt_elwood = '<p style="font-size: 8px;">' . $str_receipt_elwood . '</p>';
                $str_now = date("Y-m-d H:i:s");
                $str_now_short = date("d/m/Y");
                $to_print = '';
                $value_str = '';
                $to_print .= $template_message;
                $to_print = str_replace("[toylist]", $str_receipt, $to_print);
                $to_print = str_replace("[toylist_long]", $str_receipt_long, $to_print);
                $to_print = str_replace("[toylist_elwood]", $str_receipt_elwood, $to_print);
                $to_print = str_replace("[toylist_stoke]", $str_receipt_stoke, $to_print);
                $to_print = str_replace("[toylist_pieces_missing]", $str_receipt_pieces_missing, $to_print);
                $to_print = str_replace("[toylist_pieces_missing_nsp]", $str_receipt_pieces_missing_nsp, $to_print);
                $to_print = str_replace("[toylist_pieces_missing_pic]", $str_receipt_pieces_missing_pic, $to_print);
               $to_print = str_replace("[toylist_short]", $str_receipt_short, $to_print);
                $to_print = str_replace("[toylist_pieces]", $str_receipt_pieces, $to_print);
                $to_print = str_replace("[longname]", $longname, $to_print);
                $to_print = str_replace("[expired]", $format_expired, $to_print);
                //$format_expired_receipt
                //$to_print = str_replace("[expired_receipt]", $format_expired_receipt, $to_print);
                $to_print = str_replace("[now]", $str_now, $to_print);
                $to_print = str_replace("[now_short]", $str_now_short, $to_print);
                $to_print = str_replace("[borid]", $borid, $to_print);
                $to_print = str_replace("[duties]", $duties, $to_print);
                //$to_print = str_replace("[roster]", $receipt_roster, $to_print);
                $to_print = str_replace("[next_duty]", $next_duty, $to_print);
                $to_print = str_replace("[completed]", $completed, $to_print);
                $to_print = str_replace("[balance]", $receipt_balance, $to_print);
                $to_print = str_replace("[payments]", $payments, $to_print);
                $to_print = str_replace("[missing]", $missing, $to_print);
                $to_print = str_replace("[member_alert]", $alert_mem_txt, $to_print);
                $to_print = str_replace("[cost]", $cost, $to_print);
                $to_print = str_replace("[toy_value]", $value_str, $to_print);

                if (strpos($to_print, '[mem_missing]') !== false) {
                    include( dirname(__FILE__) . '/data/parts_mem.php');
                    $to_print = str_replace("[mem_missing]", $str_parts, $to_print);
                }
                echo $to_print;
                ?>
            </td></tr>
    </table>
</div>

