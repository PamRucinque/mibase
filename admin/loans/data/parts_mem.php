<?php
require(dirname(__FILE__) . '/../../../mibase_check_login.php');
$alert_parts = '';
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */

$str_parts = '';
$alert_parts = '';
$alert_parts_txt = '';
$table_parts = '';

$borid = $_SESSION['borid'];

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$sql = "SELECT parts.*,
toys.toyname as toyname  
from parts 
left join toys on parts.itemno = toys.idcat  where borcode = ? ORDER by type, datepart";

$sth = $pdo->prepare($sql);
$array = array($borid);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

if ($stherr[0] != '00000') {
    $error_msg .= "An  error occurred.\n";
    $error_msg .= 'Error' . $stherr[0] . '<br>';
    $error_msg .= 'Error' . $stherr[1] . '<br>';
    $error_msg .= 'Error' . $stherr[2] . '<br>';
}

$status_txt = Null;




if ($numrows > 0) {
//echo '<h2> Missing Parts: </h2>';
    //$str_parts .= '<br><strong><font color="blue">Missing Pieces: </font></strong>';
    $str_parts .= '<table border="1" width="90%" style="border-collapse:collapse; border-color:grey">';
    $str_parts .= '<tr><td><strong>Toy</strong></td><td><strong>Missing Part</strong></td></tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = $result[$ri];
    $str_parts .= '<tr><td align="left">' . $row['toyname'] . '</td>';
    $str_parts .= '<td align="left">' . $row['description'] . '</td>';
    $str_parts .= '</tr>';
}
if ($numrows > 0) {

    $str_parts .= '</table><br>';
 
}
?>

