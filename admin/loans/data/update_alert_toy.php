<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

include( dirname(__FILE__) . '/functions/functions.php');
$conn = pg_connect($_SESSION['connect_str']);
$_POST['toy_alert'] = clean($_POST['toy_alert']);
$query_alert = "update toys set alert = '" . $_POST['toy_alert'] . "' where idcat = '" . $_SESSION['idcat'] . "';";
$result = pg_Exec($conn, $query_alert);
if (!$result) {
    $message = 'Whole query: ' . $query . '<br>';
    $email_to = 'michelle@mibase.com.au';
    $email_subject = 'Error on Alert Toy: ' . $_SESSION['library_code'];
    $headers = "From: " . "MIBASE ERROR"  . "<" . $email_to . ">\nReply-To: " . $email_to . "\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    @mail($email_to, $email_subject, $message, $headers);
}



