<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');

if ($_SESSION['borid'] != '') {


    $str_month = date('m');
    $str_year = date('Y');
    $str_next_year = date('Y', strtotime('+1 year'));
    $str_duties_completed = '';
    $duties_complete = 0;
    $remind = 'No';


    $todays_date = date("Y-m-d");
//$todays_date = '2014-10-22';

    $today_ts = strtotime($todays_date); //unix timestamp value for today
    $query_duty = "SELECT
borwrs.id as borid,
borwrs.surname as surname,
borwrs.firstname as firstname,
borwrs.membertype as membertype,
(borwrs.expired - (membertype.expiryperiod * '1 month'::INTERVAL)) as start,
membertype.duties as required,
membertype.expiryperiod as mem_period,
(membertype.duties - (SELECT coalesce(sum(roster.duration),0) FROM roster WHERE roster.member_id = borwrs.id AND (roster.date_roster >= (borwrs.expired - (membertype.expiryperiod * '1 month'::INTERVAL))))) as to_allocate 
FROM (membertype INNER JOIN borwrs ON membertype.membertype = borwrs.membertype)
Where  borwrs.id = " . $_SESSION['borid'] . ";";



    $result = pg_exec($conn, $query_duty);

    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = pg_fetch_array($result, $ri);
        $duties_req = $row['required'];
        $duties_to_allocate = $row['to_allocate'];
        $duties_todo = $duties_req - $duties_allocated;
    }


    if ($duties_req > 0) {
        if ($duties_req == $duties_to_allocate) {
            $str_duties_completed = 'This member must sign up for roster duty before borrowing';
        }
    }
} 





