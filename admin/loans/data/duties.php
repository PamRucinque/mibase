<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');


$str_month = date('m');
$str_year = date('Y');
$str_next_year = date('Y', strtotime('+1 year'));
$str_duties_completed = '';
$duties_complete = 0;
$remind = 'No';
//$term_dates = '04-11;07-02;10-02';
$str = 'term dates' .  $term_dates . '<br>';
$end_term1 = $str_year . '-' . substr($term_dates, 0, 5);

$end_term1_next = $str_next_year . '-' . substr($term_dates, 0, 5);
$end_term2 = $str_year . '-' . substr($term_dates, 6, 5);
$end_term3 = $str_year . '-' . substr($term_dates, 12, 5);
$start_term1 = $str_year . '-' . '01-01';
$start_term1_next = $str_next_year . '-' . '01-01';
$end_term4 = $str_year . '-' . '12-31';
//$_SESSION['borid'] = 811;
//$str .= 'End Term: ' . $end_term1 . '<br>';

$todays_date = date("Y-m-d");
//$todays_date = '2014-10-22';
$str .=  'today: ' . $todays_date;
$str .=  'Start: ' . $start_term1 . 'End: ' . $end_term1 . '<br>';

$today_ts = strtotime($todays_date); //unix timestamp value for today


if ($today_ts >= strtotime($start_term1) && $today_ts <= strtotime($end_term1)) { //condition to check which is greater
    $str_current = 'Member must sign up for Term 1 roster duty.';
    $str_next = 'Member must sign up for Term 2 roster duty.';
    $query_current = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster >= (date '" . $start_term1 . "'))"
            . " AND (date_roster <= (date '" . $end_term1 . "'));";
    $query_next = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term1 . "'))"
            . " AND (date_roster <= (date '" . $end_term2 . "'));";
}
if ($today_ts > strtotime($end_term1) && $today_ts <= strtotime($end_term2)) { //condition to check which is greater
    $str_current = 'Member must sign up for Term 2 roster duty.';
    $str_next = 'Member must sign up for Term 3 roster duty.';
    $query_current = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term1 . "'))"
            . " AND (date_roster <= (date '" . $end_term2 . "'));";
    $query_next = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term2 . "'))"
            . " AND (date_roster <= (date '" . $end_term3 . "'));";
}
if ($today_ts > strtotime($end_term2) && $today_ts <= strtotime($end_term3)) { //condition to check which is greater
    $str_current = 'Member must sign up for Term 3 roster duty.';
    $str_next = 'Member must sign up for Term 4 roster duty.';
    $query_current = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term2 . "'))"
            . " AND (date_roster <= (date '" . $end_term3 . "'));";
    $query_next = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term3 . "'))"
            . " AND (date_roster <= (date '" . $end_term4 . "'));";
}
if ($today_ts > strtotime($end_term3) && $today_ts <= strtotime($end_term4)) { //condition to check which is greater
    $str_current = 'Member must sign up for Term 4 roster duty.';
    $str_next = 'Member must sign up for Term 1 roster duty next year.';
    $query_current = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term3 . "'))"
            . " AND (date_roster <= (date '" . $end_term4 . "'));";
    $query_next = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster >= (date '" . $start_term1_next . "'))"
            . " AND (date_roster <= (date '" . $end_term1_next . "'));";
}


//include(get_include_path() . $path . 'connect.php');
$str_duties_completed = '';


$query_req = "SELECT 
  duties 
FROM 
  membertype
  WHERE membertype = '" . $membertype . "';";


$result = pg_exec($conn, $query_req);

for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);
    $duties_req = $row['duties'];
}

//echo 'Duties Required: ' . $duties_req . '<br>';


if ($duties_req > 0) {


    $result = pg_exec($conn, $query_current);

    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = pg_fetch_array($result, $ri);
        $duties_this_term = $row['duties_complete'];
    }


    $result = pg_exec($conn, $query_next);

    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = pg_fetch_array($result, $ri);
        $duties_next_term = $row['duties_complete'];
    }



    if ($duties_this_term == 0) {
        $str_duties_completed .= ' ' . $str_current . ' ' . ' Done: ' . $duties_done;
    }
    if ($next_term_alert == 'Yes') {
        if ($duties_next_term == 0) {
            if ($duties_this_term == 0) {
                $str_duties_completed .= '\n ';
            }
            $str_duties_completed .= $str_next;
        }
    }
}
$str .=  $query_current . '<br>';
$str .= 'Duties Completed: ' . $str_duties_completed;
$str .= 'Duties Required: ' . $str_duties_completed;
//echo $str;



