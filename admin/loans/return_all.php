<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');

//include 'data/get_settings.php';
////include( dirname(__FILE__) . '/../connect.php');
include( dirname(__FILE__) . '/functions/functions.php');
include( dirname(__FILE__) . '/functions/overdue.php');
$special_return = $_SESSION['settings']['special_return'];
$recordfine = $_SESSION['settings']['recordfine'];
$chargerent = $_SESSION['settings']['chargerent'];
$weekly_fine = $_SESSION['settings']['weekly_fine'];
$fine_value = $_SESSION['settings']['fine_value'];
$grace = $_SESSION['settings']['grace'];
$rentasfine = $_SESSION['settings']['rentasfine'];
$max_daily_fine = $_SESSION['settings']['max_daily_fine'];
$unlock_returns = $_SESSION['settings']['unlock_returns'];

if ($special_return == 'Yes') {
    if ($_SESSION['shared_server'] == 'Yes'){
       $str_function = 'special/' . $_SESSION['library_code'] . '_return.php'; 
    }else{
        $str_function = 'special/special_return.php';
    }
    
    //include( dirname(__FILE__) . '/special/get_settings.php');
    include($str_function);
}



$_SESSION['loan_status'] = ' ';
$count = 0;
$alert = '';
//$_SESSION['idcat'] = Null;

if (isset($_SESSION['borid'])) {
    $borid = $_SESSION['borid'];
    //include("../connect.php");
    $query_returnall = "Select * from  transaction WHERE return is null AND 
                         borid = " . $borid . ";";
    $result = pg_exec($conn, $query_returnall);
    $numrows = pg_numrows($result);
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result, $ri);
        $transid = $row['id'];
        //$returntoy = return_toy($transid, $recordfine, $chargerent, $weekly_fine, $fine_value, $grace, $rentasfine);
        $returntoy = return_toy($transid, $recordfine, $chargerent, $weekly_fine, $fine_value, $grace, $rentasfine, $max_daily_fine, $unlock_returns);
        
        
        if ($returntoy['result'] == 'Yes') {
            $count = $count + 1;
        }
        $_SESSION['loan_status'] .= $returntoy['status'] . '<br>';
        if ($returntoy['alert'] != '') {
            $alert .= '<font color="red"> ' . $returntoy['idcat'] . ':  </font>' . '<font color="green"> ' . $returntoy['alert'] . '</font><br>';
        }
        if ($returntoy['status_fine'] != '') {
            $alert .= '<font color="red"> ' . $returntoy['idcat'] . ':  </font>' . $returntoy['status_fine'] . '<br>';
        }
    }
    $_SESSION['loan_status'] .= '<h3><font color="blue">' . $count . ' toys returned</font></h3>';

}
?>
