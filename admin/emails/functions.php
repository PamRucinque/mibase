<?php

function get_header($organisation, $email_from) {
    $reply_to = $email_from;
    if ($email_from == '') {
        $email_from = 'noreply@mibase.com.au';
    }
    $mibase_email = 'noreply@mibase.com.au';
    $email_from = $mibase_email;
    $headers = '';
    $headers .= "From: " . $organisation . " <" . $mibase_email . ">\r\n";
    $headers .= "Reply-To: " . $organisation . " <" . $reply_to . ">\r\n";
    //$headers .= "Cc: " . $organisation . " <" . $reply_to . ">\r\n";
    $headers .= "Return-Path: " . $email_from . "\r\n";
    $headers .= "Organization: " . $organisation . "\r\n";
    //$headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    $param = "-f" . $email_from;

    return array('header' => $headers, 'param' => $param);
}

function send_email($to, $subject, $message, $header, $param) {
    if ($to == '') {
        $output = '<br>Email not sent, email is blank.<br>';
    } else {
        $script_start = '<html><body>';
        $script_end = '</body></html>';
        $message = $script_start . $message . $script_end;
        $output = @mail($to, $subject, $message, $header, $param);
        //@mail('michelle@mibase.com.au',$template['subject'], $template['message'], $email['header'], $email['param']);
    }
    return $output;
}
