<?php
require( dirname(__FILE__) . '/../mibase_check_login.php');
if (!session_id()) {
    session_start();
}
$email_from = $_SESSION['settings']['email_from'];
$libraryname = $_SESSION['settings']['libraryname'];


$conn = pg_connect($_SESSION['connect_str']);
//$email_from = 'support@marinesurveyors.net.au';

include( dirname(__FILE__) . '/functions.php');

echo '<br>';
$result_txt = '';


$email_from = trim($_POST["txtEmail"]);
if ($email_from == '') {
    $email_from = 'noreply@mibase.com.au';
}

$strSubject = $_POST["txtSubject"];
$strSubject = str_replace("[organisation]", $organisation, $strSubject);
$strSubject = str_replace("[libraryname]", $libraryname, $strSubject);

$message = $_POST["txtDescription"];
$message = str_replace("'", "`", $message);
$message = str_replace("\'", "`", $message);
$message = str_replace("/'", "`", $message);
$message = str_replace('`', '\'', $message);
$message .= '<br><p>This message was sent by the ' . $libraryname . ' with MiBase.</p><br>';
$strMessage = $message;


$borlist = $_POST['borid_list'];


if (isset($_POST['send'])) {

    
    
    
    try {
        $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
    } catch (PDOException $e) {
        print "Error! member login : " . $e->getMessage() . "<br/>";
        die();
    }

    // $query = "select * from borwrs where member_status = 'ACTIVE' and id IN (" . $borlist . ") order by surname,firstname;";

    $query = "select surname, firstname, borwrs.id as id, emailaddress, key, email2, rostertype, rostertype2, rostertype4, rostertype5 as username,
      partnerssurname,partnersname, pwd, rostertype5,expired, address, suburb, postcode,    
      borwrs.id as borid, phone2 as mobile, borwrs.membertype as membertype, membertype.renewal_fee as renewal_fee,
      coalesce(y.ytd_count,0) as ytd_count,coalesce(y.ytd_value,0) as ytd_value,
    (SELECT coalesce(sum(roster.duration),0) 
    FROM roster WHERE roster.member_id = borwrs.id 
    AND (roster.date_roster >= (borwrs.expired + interval '-1 year'))) as done,
    array_to_string(
     array(select idcat || ': ' || item || ' Due: ' || to_char(due, 'dd-MM-YYYY')  from transaction t where return is null and t.borid = borwrs.id order by due) ,
     '<br> '
     ) AS toys_on_loan,
    array_to_string(
     array(select child_name || ' '  from children c where c.id = borwrs.id order by age) ,
     ',') as children, 
      array_to_string(
     array(select to_char(date_roster, 'dd-MM-YYYY') || ': ' || weekday || ' : '|| roster_session || '. Role: ' || session_role from roster r where date_roster > current_date and r.member_id = borwrs.id order by date_roster) ,
     '<br> '
     ) AS rosters,
    membertype.duties as required ";
    //$query .= $missing_str;

    $query .= "from borwrs 
    left join membertype on membertype.membertype = borwrs.membertype
    left join 
    (select count(t.id)as ytd_count, sum(toys.cost) as ytd_value, borid 
    from transaction t
    left join toys on toys.idcat = t.idcat
    where date_loan > '2017-01-01'
    group by borid) y on borwrs.id = y.borid
    where member_status = 'ACTIVE' 
    and borwrs.id IN (" . $borlist . ") order by surname,firstname;";


    $sth = $pdo->prepare($query);
    $array = array();
    $sth->execute($array);
    $result_send = $sth->fetchAll();
    $numrows = $sth->rowCount();

    //echo '<br>rows' . $numrows . '<br>';
    //echo $query;
    //echo $borlist;
    $result_header = '<font color="blue">The below Message: <br></font>';
    $result_header .= $strMessage . '<br>';
    $result_header .= '<font color="blue">Will be sent to the following members.<br>';

    for ($ri = 0; $ri < $numrows; $ri++) {

        $row = $result_send[$ri];
        $email_to = $row['emailaddress'];
        $email = $row['emailaddress'];
        $username = $row['username'];
        $firstname = $row['firstname'];
        $membertype = $row['membertype'];
        $surname = $row['surname'];
        $longname = $firstname . ' ' . $surname;
        $pwd = $row['pwd'];
        $key = $row['key'];
        $expired = $row['expired'];
        $borid = $row['id'];
        $mobile = $row['mobile'];

        $format_expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
        $message = $strMessage;
        include( dirname(__FILE__) . '/merge_fields.php');

        //$email_to = 'michelle@marineleisure.com.au';
        $header = get_header($libraryname, $email_from);
        $out = send_email($email_to, $strSubject, $message, $header['header'], $header['param']);
        //echo $message;
        if ($out) {
            //echo "Mail send completed.<br>";
            $result_txt .= $row['id'] . ' ' . $row['firstname'] . '  ' . $row['surname'] . ' ' . $row['emailaddress'] . ' <font color="green">Success</font><br>';
        } else {
            $result_txt .= $row['id'] . ' ' . $row['firstname'] . '  ' . $row['surname'] . $row['email'] . ' <font color="red">FAIL</font><br>';
        }
    }
    $result_txt .= '<br>';

    echo '<br><a href="index.php"><button>Back to Bulk Emails</button></a><br><br>';
    $result_header .= ' </font>';

    //echo $borlist;
    //echo $query;

    echo $result_header;
    echo $result_txt;

    $strSubject = 'REPORT: Bulk Email: ' . $strSubject;
    //$email_from = 'michelle@mibase.com.au';
    $header = get_header($libraryname, $email_from);
    $flgSend = send_email($email_from, $strSubject, $result_txt, $header['header'], $header['param']);
}
?>

</section>
</body>
<?php

function update_hash($memberid, $username, $password) {
    
    $conn = pg_connect($_SESSION['connect_str']);
    $now = DATE('Y');
    $hash = $memberid . $username . $password . 'swanmibase99' . $now;
    $key = md5($hash);
    $sql = "update borwrs set key = '" . $key . "' where id = " . $memberid . ";";
    $result = pg_Exec($conn, $sql);
    return $key;
}
?>

