<?php

require( dirname(__FILE__) . '/../mibase_check_login.php');

if ($key == null) {
    $key = update_hash($memberid, $username, $pwd);
}
$correct_link = 'http://' . $_SESSION['library_code'] . $domain . 'php/public/members/update.php?borid=' . $borid . '&&username=' . $username . '&&key=' . $key . '&&ok=Yes';
$correct_link = '<a href="' . $correct_link . '">Yes, my details are correct</a>';
$helmet_link = 'http://' . $_SESSION['library_code'] . $domain . 'php/public/members/update_helmet.php?key=' . $key . '&&helmet=Yes';
$helmet_link = '<a style="font-weight: bold; letter-spacing: normal; line-height: 100%;text-decoration: none; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block;" href="' . $helmet_link . '">I have read and I understand these Conditions of Use and agree to be bound by them.</a>';
$agree_txt = 'http://' . $_SESSION['library_code'] . $domain . 'php/public/members/update_agree.php?borid=' . $borid . '&&key=' . $key . '&&agree=Yes';
$agree_txt = '<a href="' . $agree_txt . '">Yes, I agree to Terms and Conditions</a>';
$photo = 'http://' . $_SESSION['library_code'] . $domain . 'php/public/members/update.php?borid=' . $borid . '&&username=' . $username . '&&pwd=' . $pwd . '&&photos=Yes';
$photo = '<a href="' . $photo . '">I give permission to take photos.</a>';
$terms = 'http://' . $_SESSION['library_code'] . $domain . 'php/website/index.php?v=10';
$terms = '<a href="' . $terms . '">Terms and Conditions</a>';



$message = str_replace("[username]", $username, $message);
$message = str_replace("[libraryname]", $libraryname, $message);
$message = str_replace("[Libraryname]", $libraryname, $message);
$message = str_replace("[pwd]", $pwd, $message);
$message = str_replace("[firstname]", $firstname, $message);
$message = str_replace("[subdomain]", $_SESSION['library_code'], $message);
$message = str_replace("[longname]", $longname, $message);
$message = str_replace("[duties_owing]", $duties_owing, $message);
$message = str_replace("[children]", $row['children'], $message);
$message = str_replace("[mobile]", $mobile, $message);
$message = str_replace("[address]", $row['address'], $message);
$message = str_replace("[email]", $email, $message);
$message = str_replace("[membertype]", $row['membertype'], $message);
$message = str_replace("[borid]", $row['id'], $message);
$message = str_replace("[correct_link]", $correct_link, $message);
$message = str_replace("[helmet_link]", $helmet_link, $message);
$message = str_replace("[link_tcs]", $terms, $message);
$message = str_replace("[agree_link]", $agree_txt, $message);
$message = str_replace("[agree]", $agree_txt, $message);
$message = str_replace("[photos]", $photo, $message);
$message = str_replace("[expired]", $format_expired, $message);
$message = str_replace("[toylist]", $toys_on_loan, $message);
$message = str_replace("[rosterlist]", $rosters, $message);
$message = str_replace("[renewal_fee]", $renewal_fee, $message);
$message = str_replace("[rosterpref]", $roster_pref, $message);
$message = str_replace("[missing]", $missing, $message);
$message = str_replace("[ytd_count]", $ytd_count, $message);
$message = str_replace("[ytd_value]", $ytd_value, $message);




