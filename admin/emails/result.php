<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include( dirname(__FILE__) . '/../header/head.php');
        ?>

    </head>

    <body width="300px">
        <section class="container fluid">

            <div class="col-sm-12">
                <?php
                if (!session_id()) {
                    session_start();
                }

                include( dirname(__FILE__) . '/menu/menu.php');
                ?>
            </div>
        </section>
        <section class="container">
            <?php 
            if (isset($_POST['send'])) {
                include( dirname(__FILE__) . '/php_send.php');
            }

            ?>
        </section>

        <script type="text/javascript" src="../js/menu.js"></script>

    </body>
</html>


