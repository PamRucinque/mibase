<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE,chrome=1"/>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Fjalla+One|Roboto+Slab:300,400,500,700" />
    <link rel="stylesheet" href="css/menu.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src='../js/jquery-3.0.0.js' type='text/javascript'></script> 
    <script type="text/javascript" src="../js/tooltip.js"></script>
</head>
<?php
require( dirname(__FILE__) . '/../mibase_check_login.php');

$search = '';
if (isset($_POST['search'])) {
    $search = $_POST['search'];
    $_SESSION['search'] = $_POST['search'];
}



if (isset($_POST['reset'])) {
    $search = "";
    $membertype = "";
    $_SESSION['search'] = '';
}

if (isset($_POST['membertype'])) {
    $membertype = $_POST['membertype'];
    $_SESSION['membertype'] = $_POST['membertype'];
}



if ($template_email != '') {
    $email_from = $template_email;
}
$search = strtoupper($search);
$search_str = '%' . $search . '%';

$membertype_str = '%' . strtoupper($_SESSION['membertype']) . '%';



$XX = "No Record Found";
$total = 0;



try {
    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
} catch (PDOException $e) {
    print "Error! member login : " . $e->getMessage() . "<br/>";
    die();
}

$query_members = "SELECT * FROM borwrs  
    WHERE (upper(surname) LIKE ? OR upper(firstname) LIKE ?) AND upper(membertype) LIKE ? 
        and member_status = 'ACTIVE' and (trim(emailaddress) is not null or trim(emailaddress) != '') 
         ORDER BY surname, firstname ASC;";

//$query_members = "SELECT * from members;";

//echo $query_members;
$sth = $pdo->prepare($query_members);
$array = array($search_str, $search_str, $membertype_str);
$sth->execute($array);
$result = $sth->fetchAll();
$numrows = $sth->rowCount();


$result_txt = '';

if ($numrows > 0) {
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        $total = $total + 1;
        $expired = $row['expired'];
        $borid = $row["id"];
        $firstname = $row["firstname"];
        $membertype = $row["membertype"];
        $surname = $row["surname"];

        $email = $row["emailaddress"];
         $member_status = $row["member_status"];
        $email_link = '<a href="mailto:' . $email . '">send</a>';
        $format_expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
        $date_expired = date_create_from_format('d-m-Y', $format_expired);
        $link = '../members/update/member_detail.php?borid=' . $borid;
        $onclick = 'javascript:location.href="' . $link . '"';
        $result_txt .= '<tr border="1" class="member" ondblclick=' . $onclick . '>';
        $result_txt .= '<td class="borid" width="50px" align="center">' . $borid . '</td><td width="30px" align="center"><input type="checkbox" ></td>';

//$result_txt .= '<tr border="1"><td border="1" width="50">' . $borid . '</td>';
        $result_txt .= '<td align="left">' . $surname . '</td>';
        $result_txt .= '<td align="left">' . $firstname . '</td>';

        $result_txt .= '<td>' . $email . '</td>';
//$result_txt .= '<td>' . $email2 . '</td>';

        $result_txt .= '<td width="180px">' . $membertype . '</td>';
    }
}
$result_txt .= '</tr></table>';



$search_str = '';
$membertype_str = '';
$search_mem_str = '';
?>
<style type="text/css">
    tr:hover { 
        color: red; 
        background-color: lightgoldenrodyellow;}
</style>

<?php
print '<div id="open"><table width="100%"><tr><td width= 50%><h1 align="left">' . $search_str . '</h1></td><td><h4 align="right">Total: ' . $total . '</h4></td><tr></table></div>';
print '<table border="1" width="100%">';
print '<tr style="color:green"><td>id</td><td>Select</td><td>Last Name</td><td>Firstname</td><td>Email</td><td>Member Type</td><tr>';
echo $result_txt;

?>
