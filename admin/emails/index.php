<?php
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>

<html>
    <head>
        <title>Mibase Bulk Email</title>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: "textarea",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons",
                image_advtab: true,
                relative_urls: false,
                remove_script_host: false
            });
        </script>
        <script type="text/javascript">
            $(function () {
                var pickerOpts = {
                    dateFormat: "yy-mm-dd",
                    showOtherMonths: true,
                    changeMonth: true,
                    changeYear: true

                };
                $("#today").datepicker(pickerOpts);
            });</script>
        <script>
            function go() {
                var borids = "";

                $("tr.member").each(function () {
                    $this = $(this);
                    var borid = $this.find("td.borid").html();
                    var value = $this.find("td :checkbox").is(':checked');
                    if (value) {
                        borids += borid + ",";
                    }
                });

                if (borids.length > 0) {
                    borids = borids.substring(0, borids.length - 1);
                    $("#borid_list").val(borids);
                    //document.forms["reports"].submit();
                    //document.reports.submit();
                    //alert(borids);
                    return true;
                } else {
                    alert('No members selected!');
                    return false;
                }

                //alert(idcats);
            }
            function select_all() {
                var borids = "";
                $("tr.member").each(function () {
                    $this = $(this);
                    $this.find("td :checkbox").prop("checked", true);
                });
            }

        </script>
        <?php
        include( dirname(__FILE__) . '/../header/head.php');
        ?>
    </head>
    <body>
        <section class="container fluid">
            <div class="col-sm-12">
                <?php
                if (!session_id()) {
                    session_start();
                }
                include( dirname(__FILE__) . '/menu/menu.php');
                ?>

            </div>
        </section>
        <section class="container">
            <?php
            if (!session_id()) {
                session_start();
            }


            if ($_SESSION['settings']['weekly_fine'] == '') {
                include( dirname(__FILE__) . '/../functions.php');
                include( dirname(__FILE__) . '/../session_new.php');
                
                //echo $_SESSION['connect_str'];
                $_SESSION['settings'] = fillSettingSessionVariables($_SESSION['connect_str']);
                //echo 'Fine Value: ' . $_SESSION['settings']['fine_value'] . '<br>';
            }
            //echo 
            if (isset($_POST['template'])) {
                $_SESSION['template'] = trim($_POST['template']);
            }
            include( dirname(__FILE__) . '/get_template.php');

            if ($_SESSION['settings']['email_from'] == '') {
                $email_from = 'noreply@mibase.com.au';
            }

            $email_subject = 'Message from the ' . $_SESSION['settings']['libraryname'] . '.';
            //include( dirname(__FILE__) . '/get_template.php');
            include( dirname(__FILE__) . '/search_form.php');
            include( dirname(__FILE__) . '/form.php');
            ?>
        </section>
    </body>
</html>
<!--- This file download from www.shotdev.com -->