<?php 
if (!session_id()) {
    session_start();
}
//echo 'hello';
//include( dirname(__FILE__) . '/config.php');

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */

//add the config values. this works because the include is relative to this file

if (!isset($_SESSION['app'])) {
    // there is a session but no session variable app 
    // redirect the user to the login page
    header('location:' . $_SESSION['app_root_location'] . '/login.php');
    exit();
} else if ($_SESSION['app'] != 'admin') {
    // there is a session with session variable app but the app session variable is not admin 
    // ( ie they are not logged in to the admin app ) then send them to the login page.
    header('location:' . $_SESSION['app_root_location']  . '/login.php');
    exit();
}
if (isset($_SESSION['dbuser'])){
    
    
}
