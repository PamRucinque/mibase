<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');


$query_settings = "SELECT * FROM settings;";

$result_settings = pg_Exec($conn, $query_settings);

$numrows = pg_numrows($result_settings);

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_settings, $ri);

    if ($row['setting_name'] == 'catsort') {
        $catsort = $row['setting_value'];
    }
   if ($row['setting_name'] == 'address') {
        $library_address = $row['setting_value'];
    }
    if ($row['setting_name'] == 'toy_reserve') {
        $toy_reserve = $row['setting_value'];
    }
    if ($row['setting_name'] == 'loanperiod') {
        $loanperiod = $row['setting_value'];
    }
    if ($row['setting_name'] == 'format_toyid') {
        $format_toyid = $row['setting_value'];
    }
    if ($row['setting_name'] == 'loanperiod') {
        $loanperiod = $row['setting_value'];
    }
    if ($row['setting_name'] == 'recordfine') {
        $recordfine = $row['setting_value'];
    }
    if ($row['setting_name'] == 'rentasfine') {
        $rentasfine = $row['setting_value'];
    }
    if ($row['setting_name'] == 'monday') {
        $monday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'tuesday') {
        $tuesday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'wednesday') {
        $wednesday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'thursday') {
        $thursday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'monday') {
        $friday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'tuesday') {
        $saturday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'wednesday') {
        $sunday = $row['setting_value'];
    }
    if ($row['setting_name'] == 'rent') {
        $global_rent = $row['setting_value'];
    }
}
//echo 'cat sort from settings: ' . $catsort . '<br>';/
pg_FreeResult($result_settings);
// Close the connection
pg_Close($conn);
?>

