<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');

//include( dirname(__FILE__) . '/../connect.php');
$conn = pg_connect($_SESSION['connect_str']);
$query = "SELECT * from settings order by id";
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);


if ($numrows > 0) {

echo '<table border="1" width="90%" style="border-collapse:collapse; border-color:grey">';
echo '<tr><td></td><td>id</td><td>Name</td><td>Description</td><td>Value</td><td></td></tr>';

}

$count = 0;
for ($ri = 0; $ri < $numrows;$ri++) {
//echo "<tr>\n";
$row = pg_fetch_array($result, $ri);


$count = $count +1;
echo '<tr id="red"><td width="30" align="center"><font color="orange">' . $count . '</font></td>';
echo '<td width="30" align="center">' . $row['id'] . '</td>';
echo '<td align="left">' . $row['setting_name'] . '</td>';
echo '<td align="left" width="400px">' . $row['description'] . '</td>';
echo '<td align="left">' . $row['setting_value'] . '</td>';
if ($row['setting_type'] == 'textarea'){
    echo '<td><a class="button_small_red" href="edit_setting_ta.php?id=' . $row['id'] . '"/>Edit</a>';
}else{
    echo '<td><a class="button_small_red" href="edit_setting.php?id=' . $row['id'] . '"/>Edit</a>';
}


//echo '<td width="50" align="center">' . $download2 . '</td>';
//echo '<td width="100">' . $row['man_html'] . '</td>';
echo '</tr>';

}
echo '<tr height="0"></tr>';
echo '</table>';



pg_close($conn);
?>

</body>


