<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" href="../js/themes/base/jquery.ui.all.css" rel="stylesheet" />

<script type="text/javascript">
    $(function(){
        var pickerOpts = {
            dateFormat:"yy-m-d",
            showOtherMonths: true
              
        }; 
        $("#dob").datepicker(pickerOpts);
    });
    function myFunction()
    {
        alert("This Setting has been saved!");
    }
      
</script>

<p><font size="2" face="Arial, Helvetica, sans-serif"></font></p>

<font></font>
<?php
//echo '<br><a href="../toy_detail.php" class="button1">Back to Toy Detail</a>'; 
$ref = 'member_detail.php?id=' . $_SESSION['borid'];
//echo "<td width='50'><a class ='button1' href='" . $ref  . "'>Edit</a></td>";
echo "<br><a href='settings.php' class ='button1_red'>Back to Settings</a>";
?>
<form id="form_99824" class="appnitro" enctype="multipart/form-data" method="post" action="<?php echo 'edit_setting_ta.php'; ?>">

    <div id="form" style="background-color:lightgray;" align="left">

        <table align="top"><tr>
                 <td><h2>Edit a Setting:</h2></td>
                <td align="right"><input id="saveForm" class="button1_red"  type="submit" name="submit" value="Save" /></td>
            </tr>
            <tr>
                <td><h4>Setting Name: <?php echo $setting_name ?><br></h4></td>
            </tr>
            <tr>
              <td align="top"><label><br>Setting Value: </label><br>
                    <textarea id="setting_value" name="setting_value" rows="10" cols="35"><?php echo $setting_value; ?></textarea></td>
 
            <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"> 
        </table>
    </div>
</form></p>

