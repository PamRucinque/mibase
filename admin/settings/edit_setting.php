<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
    </head>    


    <body id="main_body" >
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../menu.php');
            //include( dirname(__FILE__) . '/get_setting.php');
            //include( dirname(__FILE__) . '/toy_detail.php');


            if (isset($_POST['submit'])) {
                $setting = str_replace("'", "`", $_POST['setting_value']);
                
                
                

                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $query_edit = "UPDATE settings SET
                    setting_value = ? 
                    WHERE id=?;";

                //create the array of data to pass into the prepared stament
                $sth = $pdo->prepare($query_edit);
                $array = array($setting, $_POST['id']);
                $sth->execute($array);
                $stherr = $sth->errorInfo();


                if ($stherr[0] != '00000') {
                    echo "A query error occurred.\n";
                    //echo $query_edit;
                    echo $connect_pdo;
                    echo 'Error ' . $stherr[0] . '<br>';
                    echo 'Error ' . $stherr[1] . '<br>';
                    echo 'Error ' . $stherr[2] . '<br>';
                    exit;
                } else {
                    echo '<br><font color="red">Setting saved.</font>';
                    include( dirname(__FILE__) . '/settings_list.php');
                }




                //$redirect = 'location:settings.php';
                //header($redirect);
            } else {
                include( dirname(__FILE__) . '/get_setting.php');
                $_SESSION['library_code'] = $_SESSION['dbuser'];
                include( dirname(__FILE__) . '/edit_form_setting.php');
                //include( dirname(__FILE__) . '/settings.php');
            }
            ?>
        </div>
    </body>
</html>