<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../header.php'); ?> 
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../css/mibase.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <?php include( dirname(__FILE__) . '/../menu.php'); ?>
            <div class="row">
                <div class="col-sm-9">
                    <h2>Select Boxes</h2><br><br>

                </div>
                <div class="col-sm-2">
                    <br><a href='../home/index.php' class ='btn btn-colour-yellow'>Back to Home</a><br>
                </div>
                <div class="col-sm-1">
                    <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=select_boxes' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <h4>Members</h4>
                    <?php
                    if ($_SESSION['library_code'] == $_SESSION['username']) {
                        echo '<br><a style="width: 150px;" href="users/index.php" class ="btn btn-colour-maroon">Users</a><br>';
                    }
                    ?>


                    <br><a style="width: 150px;" href='memcat/index.php' class ='btn btn-colour-maroon'>Member Categories</a><br>
                    <br><a style="width: 150px;" href='city/index.php' class ='btn btn-colour-maroon'>Cities</a><br>
                    <br><a style="width: 150px;" href='suburb/index.php' class ='btn btn-colour-maroon'>Suburbs</a><br>
                    <br><a style="width: 150px;" href='discover/index.php' class ='btn btn-colour-maroon'>How did you find Us?</a><br>
                    <br><a style="width: 150px;" href='lang/index.php' class ='btn btn-colour-maroon'>Language</a><br>
                </div>
                <div class="col-sm-3">
                    <h4>Toys</h4>
                    <br><a style="width: 150px;" href='toycat/index.php' class ='btn btn-primary'>Toy Categories</a><br>
                    <br><a style="width: 150px;" href='toysubcat/index.php' class ='btn btn-primary'>Toy Sub Categories</a><br>
                    <br><a style="width: 150px;" href='age/index.php' class ='btn btn-primary'>Toy Age Groups</a><br>
                    <br><a style="width: 150px;" href='condition/index.php' class ='btn btn-primary'>Toy Condition</a><br>
                    <br><a style="width: 150px;" href='storage/index.php' class ='btn btn-primary'>Toy Storage</a><br>
                    <br><a style="width: 150px;" href='warning/index.php' class ='btn btn-primary'>Toy Warnings</a><br>
                    <br><a style="width: 150px;" href='manu/index.php' class ='btn btn-primary'>Manufacturers</a><br>
                </div>

                <div class="col-sm-3">
                    <h4>Roster/Events</h4>
                    <br><a style="width: 150px;" href='roster_pref/index.php' class ='btn btn-success'>Roster Preferences</a><br>
                    <br><a style="width: 150px;" href='eventtype/index.php' class ='btn btn-success'>Event Types</a><br>

                </div>
                <div class="col-sm-3">
                    <h4>Payments</h4>
                    <br><a style="width: 150px;" href='payops/index.php' class ='btn btn-payments'>Payment Options</a><br>
                    <br><a style="width: 150px;" href='typepay/index.php' class ='btn btn-payments'>Payment Types</a><br>

                </div>
            </div>
        </div>
    </body>
</html>
