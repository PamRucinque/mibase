<?php
    $branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
    include(dirname(__FILE__) . '/../../mibase_check_login.php');
?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../css/bootstrap.css"/>
<section class="container-fluid" style="padding: 10px;">
    <div class="row">
        <div class="col-sm-8">
            <h2>Edit Payment Option</h2>
        </div>
        <div class="col-sm-2">
            <br><a href='index.php' class ='btn btn-info'>Back to Payment Options List</a><br>
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-1">
            <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=roster_preferences' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
        </div>
    </div>
</section>

<section class="container-fluid" style="padding: 10px;">
    <form action="edit.php" method="post">
        <div class="row" style="background-color:whitesmoke;">
            <div class="col-sm-4"  id="contact">
                <label for="paymentoptions">P ayment Option:</label>
                <input type="text" class="form-control" id="paymentoptions" placeholder="" name="paymentoptions" value="<?php echo $paymentoptions['paymentoptions']?>">
                <label for="accountcode">Account Code:</label>
                <input type="text" class="form-control" id="accountcode" placeholder="" name="accountcode" value="<?php echo $paymentoptions['accountcode']?>">
                <label for="amount">Amount:</label>
                <input type="text" class="form-control" id="amount" placeholder="" name="amount" value="<?php echo $paymentoptions['amount']?>">
                <label for="typepayment">Type Payment:</label>
                <input type="text" class="form-control" id="typepayment" placeholder="" name="typepayment" value="<?php echo $paymentoptions['typepayment']?>">
                <label for="description">Description:</label>
                <input type="text" class="form-control" id="description" placeholder="" name="description" value="<?php echo $paymentoptions['description']?>">
                <!-- <label for="group">Group:</label>
                <input type="text" class="form-control" id="group" placeholder="" name="group" value="<?php //echo $paymentoptions['group']?>"> -->
                <input type="hidden" id="id"  name="id" value="<?php echo $paymentoptions['id']; ?>">
            </div>
            <div class="col-sm-8"></div>
        </div>
        <div class="row" style="background-color:lightgoldenrodyellow;">
            <div class="col-sm-12"  id="submit_header" style="min-height:140px;padding-right:0px;padding-left:20px;padding-top: 5px;">
                <br><input type=submit id="submit" name="submit" class="btn btn-success" value="Save Payment Option"> <br>
            </div>
        </div>
    </form>
</section>
<?php include ('msg_form.php'); ?>




