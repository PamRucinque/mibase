<?php
    require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">
    </head>
    <body onload="setFocus()">
        <div id="form_container">
            <?php

                //Setup message vals:
                include( dirname(__FILE__) . '/../../menu.php');
                $button_str = 'OK';
                $java_str = "$(location).attr('href', 'index.php')";
                $str_alert = '';

                //If the request was submitted:
                if (isset($_POST['submit'])) {

                    //Get the next available ID:
                    include('../functions.php');
                    $newid = get_id('paymentoptions');

                    //Create PDO and query:
                    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                    // $query = "INSERT INTO paymentoptions (id, paymentoptions, accountcode, amount, typepayment, description, group)
                    //           VALUES (?,?,?,?,?,?,?) returning id;";

                    $query = "INSERT INTO paymentoptions "
                    . "(id, paymentoptions, accountcode, amount, typepayment, description, group) "
                    . "VALUES (?,?,?,?,?,?,?) returning id;";

                    //Add parameters to the query:
                    $sth = $pdo->prepare($query);
                    $array = array($newid, $_POST['paymentoptions'], $_POST['accountcode'], $_POST['amount'], $_POST['typepayment'], $_POST['description'], $_POST['group']);

                    //Execute the query:
                    $sth->execute($array);
                    $stherr = $sth->errorInfo();
                    $prefid = $sth->fetchColumn();

                    //If there was an error:
                    if ($stherr[0] != '00000') {
                        echo "An insert error occurred.\n";
                        echo 'Error ' . $stherr[0] . '<br>';
                        echo 'Error ' . $stherr[1] . '<br>';
                        echo 'Error ' . $stherr[2] . '<br>';
                        echo '<a class="btn btn-danger" href="new.php?rt_id=' . $_POST['id'] . '">Back to New</a>';
                        exit;
                    }

                    //Redirect the user:
                    $redirect = "Location: ../update/new_rt.php";
                    $str_alert = '<br><br>Payment Option has been added successfully.<br>';
  
                } else {
                    include('new_form.php');
                }
            ?>
        </div>
        <?php include ('msg_form.php'); ?>
        <script type="text/javascript" src="../../js/jquery-3.0.0.js" charset="UTF-8"></script>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    </body>
</html>