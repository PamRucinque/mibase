<?php
    require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">
    </head>
    <body>
        <div id="form_container">
            <?php include( dirname(__FILE__) . '/../../menu.php'); ?>
            <section class="container-fluid" style="padding: 10px;">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>List of Payment Options</h2><br><br>
                    </div>
                    <div class="col-sm-2">
                        <br><a href='../index.php' class ='btn btn-info'>Back to Select Boxes</a><br>
                    </div>
                    <div class="col-sm-1">
                        <br><a href='new.php' class ='btn btn-warning'>New</a><br>
                    </div>
                    <div class="col-sm-1">
                        <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=roster_preferences' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                    </div>
                </div>
                <?php include('list.php'); ?>
            </section>
        </div>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.dropdown-toggle').dropdown();
            });
        </script>
    </body>
</html>