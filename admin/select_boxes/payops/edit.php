<?php
    require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">
        <script>
            function setFocus()
            {
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }
        </script>
    </head>
    <body  onload="setFocus()">
        <div id="form_container">
            <?php

                //Setup message vals:
                include( dirname(__FILE__) . '/../../menu.php');
                $button_str = 'OK';
                $java_str = "$(location).attr('href', 'index.php')";
                $str_alert = '';

                //When the user submits the request:
                if (isset($_POST['submit'])) {

                    //Create and run the query:
                    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                    $query = "update paymentoptions "
                            . "set paymentoptions=?, accountcode=?, amount=?, typepayment=?, description=?"
                            . "WHERE id = ?;";
                    $sth = $pdo->prepare($query);
                    $array = array($_POST['paymentoptions'], $_POST['accountcode'], $_POST['amount'], $_POST['typepayment'], $_POST['description'], $_POST['id']);

                    //Execute the query:
                    $sth->execute($array);
                    $stherr = $sth->errorInfo();

                    //If there is an error:
                    if ($stherr[0] != '00000') {
                        $str_alert .= "An update query error occurred.\n";
                        $str_alert .= 'Error ' . $stherr[0] . '<br>';
                        $str_alert .= 'Error ' . $stherr[1] . '<br>';
                        $str_alert .= 'Error ' . $stherr[2] . '<br>';
                        $str_alert .= '<a class="button1_red" href="index.php?rt_id=' . $_POST['id'] . '">Back to Edit form</a>';
                        exit;
                    }

                    //Add text to the message body:
                    $str_alert .= '<br><br>The Payment Option has been updated.<br>';

                } else {
                    include('data/get_payop.php');
                    include('edit_form.php');
                }
            ?>
        </div>
        <?php include ('msg_form.php'); ?>
        <script type="text/javascript" src="../../js/jquery-3.0.0.js" charset="UTF-8"></script>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    </body>
</html>