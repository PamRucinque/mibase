<?php
    //Validate the session:
    require( dirname(__FILE__) .  '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <body>
        <div id="form_container">
            <?php

                include( dirname(__FILE__) . '/../../menu.php');
                include( dirname(__FILE__) . '/new_rtid.php');
                include( dirname(__FILE__) . '/new_form_rt.php');

                //If the user tries to add a new suburb:
                if (isset($_POST['submit'])) {

                    //Get the parameters:
                    $suburb = pg_escape_string($_POST['suburb']);
                    $postcode = pg_escape_string($_POST['postcode']);
                    $exclude = 'No';

                    $query_new = "INSERT INTO city (id, city, postcode, exclude)
                                VALUES ({$newrtid}, '{$suburb}', '{$postcode}', '{$exclude}')";
                    $conn = pg_connect($_SESSION['connect_str']);
                    $result_new = pg_Exec($conn, $query_new);

                    //If the query fails:
                    if (!$result_new) {
                        echo "An INSERT query error occurred.\n";
                        echo $query_new;
                        //echo $connection_str;
                        exit;
                    } else {

                        //Setup the redirect:
                        pg_FreeResult($result_new);
                        pg_Close($conn);
                        include( dirname(__FILE__) . '/rt.php');
                        $redirect = "Location: new_rt.php";               //print $redirect;

                    }
                } else {
                    include( dirname(__FILE__) . '/rt.php');
                }

            ?>
        </div>
    </body>
</html>