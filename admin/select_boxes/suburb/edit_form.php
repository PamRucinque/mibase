<?php
    $branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
    include(dirname(__FILE__) . '/../../mibase_check_login.php');
?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../css/bootstrap.css"/>
<section class="container-fluid" style="padding: 10px;">
    <div class="row">
        <div class="col-sm-8">
            <h2>Edit Suburb</h2>
        </div>
        <div class="col-sm-2">
            <br><a href='index.php' class ='btn btn-info'>Back to Suburb List</a><br>
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-1">
            <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=roster_preferences' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
        </div>
    </div>
</section>

<section class="container-fluid" style="padding: 10px;">
    <form action="edit.php" method="post">
        <div class="row" style="background-color:whitesmoke;">
            <div class="col-sm-4"  id="contact">
                <label for="description">Postcode:</label>
                <input type="text" class="form-control" id="description" placeholder="" name="postcode" value="<?php echo $suburbs['postcode']; ?>">
                <input type="hidden" id="id"  name="id" value="<?php echo $suburbs['id']; ?>">
            </div>
            <div class="col-sm-8"></div>
        </div>
        <div class="row" style="background-color:lightgoldenrodyellow;">
            <div class="col-sm-12"  id="submit_header" style="min-height:140px;padding-right:0px;padding-left:20px;padding-top: 5px;">
                <br><input type=submit id="submit" name="submit" class="btn btn-success" value="Save Suburb"> <br>
            </div>
        </div>
    </form>
</section>
<?php include ('msg_form.php'); ?>




