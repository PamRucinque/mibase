<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">
    </head>
    <body onload="setFocus()">
        <div id="form_container">
            <?php
            //Setup the alert message:
            include( dirname(__FILE__) . '/../../menu.php');
            //include( dirname(__FILE__) . '/../functions.php');

            $button_str = 'OK';
            $java_str = "$(location).attr('href', 'index.php')";
            $str_alert = '';
            $newid = 0;
            $ok = 'Yes';

            //If the data has been submitted:
            if (isset($_POST['submit'])) {
                $check = 'ok';
                if (isset($_POST['check'])) {
                    $check = $_POST['check'];
                }
                if ($check != 'ok') {
                    $str_alert = '<br><br>Cannot save this login, the password is not strong enough. <br>';
                    $str_alert .= 'Please check the red writing (validation text) below the password box before saving.<br>';
                    $str_alert .= 'For strong passwords, use the Generate password button.<br>';
                    $java_str = "$(location).attr('href', 'new.php')";
                } else {
                    include('../functions.php');
                    $newid = get_id('users');

                    //Create the PDO and query:
                    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                    $query = "INSERT INTO users "
                            . "(id, username, location, subdomain,password, login_type, level)"
                            . "VALUES (?,?,?,?,?,?,?) returning id;";

                    //Send params to query:
                    $sth = $pdo->prepare($query);
                    $array = array($newid, $_POST['username'], $_POST['location'], $_SESSION['library_code'],
                        $_POST['password'], $_POST['login_type'], $_POST['login_type']);

                    //Execute the query:
                    $sth->execute($array);
                    $stherr = $sth->errorInfo();
                    $prefid = $sth->fetchColumn();

                    //If there is an error:
                    if ($stherr[0] != '00000') {
                        echo "An insert error occurred.\n";
                        echo 'Error ' . $stherr[0] . '<br>';
                        echo 'Error ' . $stherr[1] . '<br>';
                        echo 'Error ' . $stherr[2] . '<br>';
                        echo '<a class="btn btn-danger" href="new.php">Back to New</a>';
                        exit;
                    }

                    $str_alert = '<br><br>New User added successfully.<br>';
                }


                //Calculate the ID:
            } else {

                include('new_form.php');
            }
            ?>

        </div>
<?php include ('msg_form.php'); ?>
        <script type="text/javascript" src="../../js/jquery-3.0.0.js" charset="UTF-8"></script>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    </body>
</html>