<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">
        <script>
            function setFocus()
            {
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }
        </script>
    </head>
    <body  onload="setFocus()">
        <div id="form_container">
            <?php
            //Get the message vars Ready:
            include( dirname(__FILE__) . '/../../menu.php');
            $button_str = 'OK';
            $java_str = "$(location).attr('href', 'index.php')";
            $str_alert = '';


            //When the user submits the request:
            if (isset($_POST['submit'])) {
                $check = 'ok';
                if (isset($_POST['check'])) {
                    $check = $_POST['check'];
                }
                if ($check != 'ok') {
                    $str_alert = '<br><br>Cannot save this login, the password is not strong enough. <br>';
                    $str_alert .= 'Please check the red writing (validation text) below the password box before saving.<br>';
                    $str_alert .= 'For strong passwords, use the Generate password button.<br>';
                    $java_str = "$(location).attr('href', 'index.php')";
                } else {
                    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                    $query = "update users "
                            . "set username=?, password=?, login_type=?, location=?, level=?  "
                            . " WHERE id = ?;";
                    //Add parameters ti the quer:
                    $sth = $pdo->prepare($query);
                    $array = array($_POST['username'], $_POST['password'], $_POST['login_type'], $_POST['location'],
                        $_POST['login_type'], $_POST['id']);

                    //Execute the query:
                    $sth->execute($array);
                    $stherr = $sth->errorInfo();

                    //If there was an error:
                    if ($stherr[0] != '00000') {
                        $str_alert .= "An update query error occurred.\n";
                        $str_alert .= 'Error ' . $stherr[0] . '<br>';
                        $str_alert .= 'Error ' . $stherr[1] . '<br>';
                        $str_alert .= 'Error ' . $stherr[2] . '<br>';
                        $str_alert .= '<a class="button1_red" href="index.php?rt_id=' . $_POST['id'] . '">Back to Edit form</a>';
                        exit;
                    }

                    //Add body to the message:
                    $str_alert .= '<br><br>This user has been updated.<br>';
                }

                //Create and run the query:
            } else {

                include('data/get_user.php');
                include('edit_form.php');
            }
            ?>
        </div>
        <?php include ('msg_form.php'); ?>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
        <script>
        $(document).ready(function () {
            $('.dropdown-toggle').dropdown();
        });
        </script>
    </body>
</html>