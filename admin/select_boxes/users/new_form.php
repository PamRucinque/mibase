<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../css/bootstrap.css"/>
<script>
    function show_pwd() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
    function get_pwd() {
        var str = '';
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            //document.getElementById("michelle").innerHTML = xmlhttp.responseText;
            document.getElementById("password").value = xmlhttp.responseText;
        }
        xmlhttp.open("GET", "generate.php", true);
        xmlhttp.send();
        document.getElementById("password").type = "text";
    }
    function check_pwd(str) {
        //var str = '';
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            document.getElementById("michelle").innerHTML = xmlhttp.responseText;
            if (document.getElementById("michelle").innerHTML !== '') {
                document.getElementById("check").value = "Fail";
           }

        }
        xmlhttp.open("GET", "check.php?q=" + str, true);
        xmlhttp.send();
        document.getElementById("password").type = "text";
   }

</script>
<section class="container-fluid">

    <div class="row">
        <div class="col-sm-11">
            <h2>Add New User</h2>
        </div>

        <div class="col-sm-1">
            <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=users' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
        </div>

    </div>

</section>

<section class="container-fluid">
    <form action="new.php" method="post">
        <div class="row" style="background-color: lightgoldenrodyellow;padding-bottom: 10px;">
            <div class="col-sm-2">
                <br><a href='index.php' class ='btn btn-danger'>Cancel</a><br>
            </div>
            <div class="col-sm-8"></div>
            <div class="col-sm-2"  id="submit_header">
                <br><input type=submit id="submit" name="submit" class="btn btn-success" value="Save User"> <br>
            </div>
        </div>

        <div class="row" style="background-color:whitesmoke;padding-bottom: 20px;">
            <div class="col-sm-3"  id="contact">
                <label for="username">Username:</label>
                <input type="text" class="form-control" id="username" placeholder="" name="username" value="" required>
            </div>
            <div class="col-sm-2">
                <label for="location">Location:</label>
                <input type="text" class="form-control" id="location" placeholder="Location" name="location" value="">
            </div>
            <div class="col-sm-3">
                <label for="username">Password:</label>
                <input type="password" class="form-control" id="password" placeholder="" name="password" value="" onchange="check_pwd(this.value);" required>
                <input type="checkbox"  onclick="show_pwd()">  Show Password 
                <button class="btn btn-primary btn-sm" id="generate" name ="generate" onclick="get_pwd();" value="">Generate Password</button>
                <div id="michelle" style="color: red;"></div>
                <input type="hidden" class="form-control" id="check" placeholder="" name="check" value="ok" required>
                <br> Insert the password minimum 15 and maximum 36 characters with numbers and capital letters.<br>

            </div>
            <div class="col-sm-2">
                <label for="login_type">Type of Login:</label>
                <select id="login_type" name="login_type"  class="form-control" required>
                    <option value='' selected="selected"></option>
                    <option value="admin" >admin</option>
                    <option value="volunteer" >volunteer</option>


                </select>
            </div>
        </div>
        </div>

    </form>
</section>
<?php include ('msg_form.php'); ?>
