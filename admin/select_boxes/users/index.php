<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">
    </head>
    <body>
        <div id="form_container">
            <?php
            include( dirname(__FILE__) . '/../../menu.php');
            if (!session_id()) {
                session_start();
            }
            ?>
            <section class="container-fluid" style="padding: 10px;">
                <div class="row">
                    <div class="col-sm-8">
                        <h1>Administrator Users</h1><br><br>
                        <h2><font color="red">UNDER CONSTRUCTION PLEASE DO NOT USE (except Ashrut - Boxhill)</font></h2><br>
                        <?php //echo $_SESSION['library_code'];  ?>
                    </div>
                    <div class="col-sm-2">
                        <br><a href='../index.php' class ='btn btn-info'>Back to Select Boxes</a><br>
                    </div>
                    <div class="col-sm-1">
                        <?php
                        if ($_SESSION['library_code'] == $_SESSION['username']) {
                            echo '<br><a href="new.php" class ="btn btn-warning">New</a><br>';
                        }
                        ?>

                    </div>
                    <div class="col-sm-1">
                        <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=roster_preferences' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
                    </div>
                </div>
                <?php
                if ($_SESSION['library_code'] == $_SESSION['username']) {
                    include('list.php');
                } else {
                    echo 'This function is disabled, for this user.<br>';
                }
                ?>
            </section>
        </div>

        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.dropdown-toggle').dropdown();
            });
        </script>
    </body>
</html>