<?php
    require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">
        <script>
            function setFocus()
            {
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }
        </script>
    </head>
    <body  onload="setFocus()">
        <div id="form_container">
            <?php

                //Get the message vars Ready:
                include( dirname(__FILE__) . '/../../menu.php');
                $button_str = 'OK';
                $java_str = "$(location).attr('href', 'index.php')";
                $str_alert = '';

                //When the user submits the request:
                if (isset($_POST['submit'])) {

                    //Create and run the query:
                    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                    $query = "update membertype "
                            . "set membertype=?, description=?, maxnoitems=?, renewal_fee=?, duties=?, bond=?, expiryperiod=?, returnperiod=?, exclude=?,  "
                            . "gold_star=? WHERE id = ?;";
                    //Add parameters ti the quer:
                    $sth = $pdo->prepare($query);
                    $array = array($_POST['membertype'], $_POST['description'],$_POST['maxnoitems'], $_POST['renewal_fee'], 
                        $_POST['duties'],$_POST['bond'], $_POST['expiryperiod'], $_POST['returnperiod'], $_POST['exclude'], 
                        $_POST['gold_star'], $_POST['id']);

                    //Execute the query:
                    $sth->execute($array);
                    $stherr = $sth->errorInfo();

                    //If there was an error:
                    if ($stherr[0] != '00000') {
                        $str_alert .= "An update query error occurred.\n";
                        $str_alert .= 'Error ' . $stherr[0] . '<br>';
                        $str_alert .= 'Error ' . $stherr[1] . '<br>';
                        $str_alert .= 'Error ' . $stherr[2] . '<br>';
                        $str_alert .= '<a class="button1_red" href="index.php?rt_id=' . $_POST['id'] . '">Back to Edit form</a>';
                        exit;
                    }
                    
                    //Add body to the message:
                    $str_alert .= '<br><br>The Member Type has been updated.<br>';

                } else {

                    include('data/get_memcat.php');
                    include('edit_form.php');
                    
                }
            ?>
        </div>
        <?php include ('msg_form.php'); ?>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
         <script>
            $(document).ready(function () {
                $('.dropdown-toggle').dropdown();
            });
        </script>
    </body>
</html>