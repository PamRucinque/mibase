<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="utf-8">

        <title>Bootstrap 3 Responsive Layout Example</title>

        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">

        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

        <script src="../../js/bootstrap.min.js"></script>

    </head>

    <body>

        <div class="container">

            <div class="row">

                <div class="col-xs-4  col-md-1">
                    <p>id</p>
                </div>

                <div class="col-xs-4  col-md-2">
                    <p>Category</p>
                </div>
                <div class="col-xs-4  col-md-1">
                    <p>No Toys</p>
                </div>
                <div class="col-xs-4  col-md-1">
                    <p>Fee</p>
                </div>
                <div class="col-xs-4  col-md-1">
                    <p>Duties</p>
                </div>
                <div class="col-xs-4  col-md-1">
                    <p>Return</p>
                </div>
                <div class="col-xs-4  col-md-1">
                    <p>Expiry</p>
                </div>
                <div class="col-xs-4  col-md-1">
                    <p>Gold Star</p>
                </div>
                <div class="col-xs-4  col-md-1">
                    <p>Delete</p>
                </div>
                <div class="col-xs-4  col-md-1">
                    <p>Edit</p>
                </div>
            </div>

        </div>

