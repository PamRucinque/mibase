<?php
    $branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
    include(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../css/bootstrap.css"/>
<section class="container-fluid" style="padding: 10px;">
    <div class="row">
        <div class="col-sm-8">
            <h2>Edit Member Type</h2>
        </div>
        <div class="col-sm-2">
            <br><a href='index.php' class ='btn btn-info'>Back to Member Category List</a><br>
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-1">
            <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=roster_preferences' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
        </div>
    </div>
</section>

<section class="container-fluid" style="padding: 10px;">
    <form action="edit.php" method="post">
        <div class="row" style="background-color:whitesmoke;">
            <div class="col-sm-6"  id="contact">
                <label for="membertype">Member Type: (example family, committee)</label>
                <input type="text" class="form-control" id="description" placeholder="" name="membertype" value="<?php echo $membertype['membertype'] ?>">
                <label for="description">Description:</label>
                <input type="text" class="form-control" id="description" placeholder="" name="description" value="<?php echo $membertype['description'] ?>">
                <label for="maxnoitems">Maximum no of toys:</label><br><input type="number" name="maxnoitems" min="0" max="200" value="<?php echo $membertype['maxnoitems']; ?>">
                <input type="hidden" id="id"  name="id" value="<?php echo $membertype['id']; ?>">
                <br><label for="maxnoitems">Exclude from New Member Online Form:</label><br>
                <select id="exclude" name="exclude" style="width: 70px;">
                    <option value='<?php echo $membertype['exclude']; ?>' selected="selected"><?php echo $membertype['exclude']; ?></option>
                    <option value="Yes" >Yes</option>
                    <option value="No" >No</option>
                </select>
            </div>
            <div class="col-sm-3">
                <label for="duties">Required Duties:</label><br><input type="Number" name="duties" step ="0.1" min="0" max="20"size="10"  value="<?php echo $membertype['duties']; ?>">
                <br><label for="renewal_fee">Renewal Fee:</label><br><input type="number" name="renewal_fee" step ="0.1" min="0" max="1000" value="<?php echo $membertype['renewal_fee']; ?>">
                <br><label for="expiryperiod">Expiry Period (months): </label><br><input type="number" name="expiryperiod" min="0" max="120" value="<?php echo $membertype['expiryperiod']; ?>">
            </div>
            <div class="col-sm-3">
                <label for="returnperiod">Return Period (days): </label><br><input type="number" name="returnperiod" min="0" max="52" value="<?php echo $membertype['returnperiod']; ?>">
                <br><label for="bond">Bond / Levy: </label><br><input type="number" name="bond" min="0" max="1000" id="bond" value="<?php echo $membertype['bond']; ?>">
                <br><label for="gold_star">Gold Star Toys: </label><br><input type="Number" name="gold_star" step ="0.1" min="0" max="20"size="10"  value="<?php echo $membertype['gold_star']; ?>">
            </div>
        </div>
        <div class="row" style="background-color:lightgoldenrodyellow;">
            <div class="col-sm-12"  id="submit_header" style="min-height:140px;padding-right:0px;padding-left:20px;padding-top: 5px;">
                <br><input type=submit id="submit" name="submit" class="btn btn-success" value="Save Member Type"> <br>
            </div>
        </div>
    </form>
</section>
<?php include ('msg_form.php'); ?>




