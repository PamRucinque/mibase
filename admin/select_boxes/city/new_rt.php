<?php
    require( dirname(__FILE__) .  '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <body>
        <div id="form_container">
            <?php
            
                include( dirname(__FILE__) . '/../../menu.php');
                include( dirname(__FILE__) . '/new_rtid.php');
                include( dirname(__FILE__) . '/new_form_rt.php');

                //Check if the request was submitted:
                if (isset($_POST['submit'])) {

                    //Create and execute the query:
                    $suburb = pg_escape_string($_POST['suburb']);
                    $query_new = "INSERT INTO suburb (id, suburb)
                                VALUES ({$newrtid}, '{$suburb}')";
                    $conn = pg_connect($_SESSION['connect_str']);
                    $result_new = pg_Exec($conn, $query_new);

                    //If the query fails:
                    if (!$result_new) {

                        //Return an error message:
                        echo "An INSERT query error occurred.\n";
                        echo $query_new;
                        exit;

                    } else {

                        pg_FreeResult($result_new);
                        pg_Close($conn);
                        include( dirname(__FILE__) . '/rt.php');
                        $redirect = "Location: new_rt.php";               //print $redirect;

                    }

                } else {

                    //Execute the file called rt.php:
                    include( dirname(__FILE__) . '/rt.php');

                }

            ?>        
        </div>
    </body>
</html>