<?php
    require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">
    </head>
    <body onload="setFocus()">
        <div id="form_container">
            <?php

                //Prepare message vals:
                include( dirname(__FILE__) . '/../../menu.php');
                $button_str = 'OK';
                $java_str = "$(location).attr('href', 'index.php')";
                $str_alert = '';

                //If the request was submitted:
                if (isset($_POST['submit'])) {
                    include('../functions.php');
                    $newid = get_id('suburb');

                    //Create and execute the query:
                    $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                    $query = "INSERT INTO suburb "
                            . "(id,suburb) "
                            . "VALUES (?,?) returning id;";
                    $sth = $pdo->prepare($query);
                    $array = array($newid,$_POST['suburb']);

                    //Execute the query:
                    $sth->execute($array);
                    $stherr = $sth->errorInfo();
                    $prefid = $sth->fetchColumn();

                    //If there is an error:
                    if ($stherr[0] != '00000') {

                        //Create and echo an error message:
                        echo "An insert error occurred.\n";
                        echo 'Error ' . $stherr[0] . '<br>';
                        echo 'Error ' . $stherr[1] . '<br>';
                        echo 'Error ' . $stherr[2] . '<br>';
                        echo '<br><a class="btn btn-info" href="index.php">Back to Cities</a>';
                        exit;
                    }

                    //Set the redirect variable:
                    $redirect = "Location: ../update/new_rt.php";               //print $redirect;
                    $str_alert = '<br><br>City added successfully.' . '<br>';
                } else {

                    //Execute new_form.php:
                    include('new_form.php');
                }

            ?>
        </div>
        <?php include ('msg_form.php'); ?>
        <script type="text/javascript" src="../../js/jquery-3.0.0.js" charset="UTF-8"></script>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    </body>
</html>