<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="../../css/mibase.css" rel="stylesheet">
        <script>
            function setFocus()
            {
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }
        </script>
    </head>
    <body  onload="setFocus()">
        <div id="form_container">

            <?php
            include( dirname(__FILE__) . '/../../menu.php');
            $button_str = 'OK';
            //$java_str = 'Close';
            $java_str = "$(location).attr('href', 'index.php')";
            $str_alert = '';

            if (isset($_POST['submit'])) {

                $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
                $query = "update rostertypes "
                        . "set rostertype=?, description=?, weekday=?,location=?, nohours = ?, volunteers=? "
                        . "WHERE id = ?;";
//and get the statment object back
                $sth = $pdo->prepare($query);

                $array = array($_POST['rt'], $_POST['description'],
                    $_POST['weekday'], $_POST['location'], $_POST['nohours'], $_POST['volunteers'], $_POST['id']);

//execute the preparedstament
                $sth->execute($array);
                $stherr = $sth->errorInfo();

                if ($stherr[0] != '00000') {
                    $str_alert .= "An update query error occurred.\n";
                    $str_alert .= 'Error ' . $stherr[0] . '<br>';
                    $str_alert .= 'Error ' . $stherr[1] . '<br>';
                    $str_alert .= 'Error ' . $stherr[2] . '<br>';
                    $str_alert .= '<a class="button1_red" href="index.php?rt_id=' . $_POST['id'] . '">Back to Edit form</a>';
                    exit;
                }

                $str_alert .= '<br><br>Thank you, your Roster preference has been saved.<br>';
                //echo '<a class="button1_red" href="../update/new_rt.php">Back to Preferences</a><br><br>';
            } else {
                include('data/get_pref.php');
                include('edit_form.php');
            }
            ?>
        </div>
        <?php include ('msg_form.php'); ?>
        <script type="text/javascript" src="../../js/jquery-3.0.0.js" charset="UTF-8"></script>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

        <script type="text/javascript">
        $('.form_datetime').datetimepicker({
            //language:  'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
        $('.form_date').datetimepicker({
            language: 'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
        $('.form_time').datetimepicker({
            language: 'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0
        });
        </script>
    </body>
</html>