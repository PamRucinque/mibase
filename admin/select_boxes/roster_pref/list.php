
<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require(dirname(__FILE__) . '/../../mibase_check_login.php');

$ref_delete = '';
$ref_edit = '';

$query = "SELECT * from rostertypes ORDER by rostertype";
$conn = pg_connect($_SESSION['connect_str']);
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);
include('heading.php');

for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $rostertype = pg_fetch_array($result, $ri);
    //$link = 'edit.php?rt_id=' . $rostertype['id'];
    //$onclick = 'javascript:location.href="' . $link . '"';
    $onclick = 'edit_pref(' . $rostertype['id'] . ');';
    $ref_delete = '<form action="delete.php" method="POST"><input type="hidden" name="rt_id" id="rt_id" value="' . $rostertype['id'] . '">';
    $ref_delete .= '<input id="submit" name="submit" class="btn btn-danger btn-sm"  type="submit" value="Delete" /></form>';
    $ref_edit = '<form action="edit.php" id="edit" name="edit" method="POST"><input type="hidden" name="rt_id" id="rt_id" value="' . $rostertype['id'] . '">';
    $ref_edit .= '<input id="submit_nav" name="submit_nav" class="btn btn-primary btn-sm"  type="submit" value="Edit" /></form>';
    include('row.php');
}




