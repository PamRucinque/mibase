<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../css/bootstrap.css"/>
<section class="container-fluid" style="padding: 10px;">
    <div class="row">
        <div class="col-sm-8">
            <h2>New Roster Preference</h2>
        </div>
        <div class="col-sm-2">
            <br><a href='index.php' class ='btn btn-info'>Back to Preference List</a><br>
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-1">
            <br><a target="target _blank" href='https://www.wiki.mibase.org/doku.php?id=roster_preferences' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
        </div>
    </div>
</section>

<section class="container-fluid" style="padding: 10px;">
    <form action="new.php" method="post">

        <div class="row" style="background-color:whitesmoke;">

            <div class="col-sm-4"  id="contact">
                <label for="description">Roster Period: (example 09.15 - 11.30am )</label>
                <input type="text" class="form-control" id="description" placeholder="" name="description" value="">
                <label for="rt">Roster Group:</label>
                <select id="rt" name="rt" class="form-control" >
                    <option value="Roster" selected="selected">Roster</option>
                    <option value="Roster Coord" >Roster Coord</option>
                    <option value="Student" >Student</option>
                    <option value="Emergency" >Emergency</option>
                    <option value="Extra" >Extra</option>
                </select>
                <label for="weekday">Weekday:</label>
                <select id="weekday" name="weekday" class="form-control" required="required">
                    <option value="" ></option>
                    <option value="Monday" >Monday</option>
                    <option value="Tuesday" >Tuesday</option>
                    <option value="Wednesday" >Wednesday</option>
                    <option value="Thursday" >Thursday</option>
                    <option value="Friday" >Friday</option>
                    <option value="Saturday" >Saturday</option>
                    <option value="Sunday" >Sunday</option>
                    <option value="No Weekday" >No Weekday</option>
                </select> 
                <label for="hours">No Hours:</label>
                <input type="number" class="form-control" id="nohours" placeholder="hours" name="nohours" value="0" style="max-width: 100px;">
                <label for="hours">No Volunteers:</label>
                <input type="number" class="form-control" id="volunteers" placeholder="no vols" name="volunteers" value="0"  style="max-width: 100px;">
                <label for="location">Location:</label>
                <input type="text" class="form-control" id="location" placeholder="" name="location" value="" style="max-width: 150px;"><br>
            </div>
            <div class="col-sm-8"></div>
        </div>
        <div class="row" style="background-color:lightgoldenrodyellow;">

            <div class="col-sm-12"  id="submit_header" style="min-height:140px;padding-right:0px;padding-left:20px;padding-top: 5px;">
                <br><input type=submit id="submit" name="submit" class="btn btn-success" value="Save New Preference"> <br>
            </div>
        </div>
    </form>
</section>





