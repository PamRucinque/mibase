<?php

    //Validate the session:
    require( dirname(__FILE__) . '/../../mibase_check_login.php');

    //Confirm that the id variable has been set:
    if (isset($_POST['rt_id']) && is_numeric($_POST['rt_id'])) {
        //Get the id variable from the URL:
        $conn = pg_connect($_SESSION['connect_str']);
        //Create and execute the query:
        $query = "DELETE FROM discovery WHERE id = " . $_POST['rt_id'] . ";";
        $result = pg_exec($conn, $query);

        //Check result:
        if(!$result){

            //Create error message for user:
            $message = 'Invalid query: ' . "\n";
            $message = 'Whole query: ' . $query;
            echo $message;

        } else {

            //Return the query:
            echo $query;

        }

        //Redirect the user back to the lsit:
        $redirect = "Location: index.php";               //print $redirect;
        header($redirect);
        
    } 

?>