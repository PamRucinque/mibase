<?php
    require( dirname(__FILE__) .  '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <body>
        <div id="form_container">
            <?php

                //Include the relevant files:
                include( dirname(__FILE__) . '/../../menu.php');
                include( dirname(__FILE__) . '/new_rtid.php');
                include( dirname(__FILE__) . '/new_form_rt.php');

                //include( dirname(__FILE__) . '/../../connect.php');
                if (isset($_POST['submit'])) {

                    //Create and run the query:
                    $storage = pg_escape_string($_POST['storage']);
                    $query_new = "INSERT INTO storage (id, storage)
                                VALUES ({$newrtid}, '{$storage}')";
                    $conn = pg_connect($_SESSION['connect_str']);
                    $result_new = pg_Exec($conn, $query_new);

                    //If the query fails:
                    if (!$result_new) {

                        //Return an error message:
                        echo "An INSERT query error occurred.\n";
                        echo $query_new;
                        exit;

                    } else {

                        //Redirect the user:
                        pg_FreeResult($result_new);
                        pg_Close($conn);
                        include( dirname(__FILE__) . '/rt.php');
                        $redirect = "Location: new_rt.php";               //print $redirect;

                    }

                } else {

                    //Execute the file called rt.php:
                    include( dirname(__FILE__) . '/rt.php');

                }
            ?>
            
        </div>
    </body>
</html>