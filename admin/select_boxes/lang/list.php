<?php

    //Check for a valid session:
    require(dirname(__FILE__) . '/../../mibase_check_login.php');

    //Create the del variable:
    $ref_edit = '';
    $ref_delete = '';

    //Setup and run the request:
    $query = "SELECT * from nationality ORDER by id";
    $conn = pg_connect($_SESSION['connect_str']);
    $result = pg_exec($conn, $query);
    $numrows = pg_numrows($result);

    //Include the heading.php file:
    include('heading.php');

    //For the number of results:
    for ($ri = 0; $ri < $numrows; $ri++) {

        //Extract the data:
        $nationality = pg_fetch_array($result, $ri);
        //Setup the delete portion:
        $ref_delete = '<form action="delete.php" method="POST"><input type="hidden" name="rt_id" id="rt_id" value="' . $nationality['id'] . '">';
        $ref_delete .= '<input id="submit" name="submit" class="btn btn-danger btn-sm"  type="submit" value="Delete" /></form>';
        $ref_edit = '<form action="edit.php" id="edit" name="edit" method="POST"><input type="hidden" name="rt_id" id="rt_id" value="' . $nationality['id'] . '">';
        $ref_edit .= '<input id="submit_nav" name="submit_nav" class="btn btn-primary btn-sm"  type="submit" value="Edit" /></form>';
        //Include the row.php file:
        include('row.php');

    }

?>