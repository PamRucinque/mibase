<?php

    //Validate the session:
    require( dirname(__FILE__) .  '/../../mibase_check_login.php');

?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <body>
        <div id="form_container">
            <?php

                include( dirname(__FILE__) . '/../../menu.php');
                include( dirname(__FILE__) . '/new_rtid.php');
                include( dirname(__FILE__) . '/new_form_rt.php');

                //If the request was submitted:
                if (isset($_POST['submit'])) {

                    //Get User data:
                    $nationality = pg_escape_string($_POST['nationality']);
                    $description = pg_escape_string($_POST['description']);

                    //Construct the query:
                    $query_new = "INSERT INTO nationality (id, nationality, description)
                                  VALUES ({$newid}, '{$nationality}', '{$description}')";

                    //Run the query:
                    $conn = pg_connect($_SESSION['connect_str']);
                    $result_new = pg_Exec($conn, $query_new);

                    //If the query fails:
                    if (!$result_new) {

                        //Display an error message:
                        echo "An INSERT query error occurred.\n";
                        echo $query_new;
                        exit;
                        
                    } else {

                        //Redirect the user:
                        pg_FreeResult($result_new);
                        pg_Close($conn);
                        include( dirname(__FILE__) . '/rt.php');
                        $redirect = "Location: new_rt.php";

                    }
                } else {
                    include( dirname(__FILE__) . '/rt.php');
                }

            ?>
        </div>
    </body>
</html>