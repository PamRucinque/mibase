<?php

    //Check for a valid session:
    require( dirname(__FILE__) . '/../../mibase_check_login.php');

    //Confirm that the 'id' variable has been set:
    if (isset($_POST['rt_id']) && is_numeric($_POST['rt_id'])) {

        // get the 'id' variable from the URL
        $conn = pg_connect($_SESSION['connect_str']);
        $query = "DELETE FROM sub_category WHERE id = " . $_POST['rt_id'] . ";";
        $result = pg_exec($conn, $query);

        //Check result:
        if (!$result) {

            //Create the error message:
            $message = 'Invalid query: ' . "\n";
            $message = 'Whole query: ' . $query;
            echo $message;

        } else {
            echo $query;
        }

        //Redirect back to the lsit:
        $redirect = "Location: index.php";               //print $redirect;
        header($redirect);
        
    } 
?>