<?php
    require( dirname(__FILE__) .  '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include( dirname(__FILE__) . '/../../header.php'); ?> 
    </head>
    <body>
        <div id="form_container">
            <?php

                include( dirname(__FILE__) . '/../../menu.php');
                include( dirname(__FILE__) . '/new_rtid.php');
                include( dirname(__FILE__) . '/new_form_rt.php');

                //If the user tries to add a new suburb:
                if (isset($_POST['submit'])) {

                    //Get parameters:
                    $sub_category = pg_escape_string($_POST['sub_category']);
                    $description = pg_escape_string($_POST['description']);

                    //Create and run the query:
                    $query_new = "INSERT INTO sub_category (id, sub_category, description)
                                VALUES ({$newrtid}, '{$sub_category}', '{$description}')";
                    $conn = pg_connect($_SESSION['connect_str']);
                    $result_new = pg_Exec($conn, $query_new);

                    //If the query fails:
                    if (!$result_new) {

                        //Alert the user that there was an error:
                        echo "An INSERT query error occurred.\n";
                        echo $query_new;
                        exit;

                    } else {

                        //Setup the redirect:
                        pg_FreeResult($result_new);
                        pg_Close($conn);
                        include( dirname(__FILE__) . '/rt.php');
                        $redirect = "Location: new_rt.php";

                    }
                } else {
                    include( dirname(__FILE__) . '/rt.php');
                }
                
            ?>
        </div>
    </body>
</html>