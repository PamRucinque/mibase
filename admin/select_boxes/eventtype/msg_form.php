<script>
    function setFocus()
    {
        var msg = document.getElementById("msg").innerText;
        //alert(msg);
        if (msg !== '') {
            $('#myModal').modal('show');
        }

    }
</script>
<section class="container fluid">
    <div id='msg' style='display: none;'><?php echo $str_alert; ?></div>
    <!-- Trigger the modal with a button -->
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog"  data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Mibase Alert</h4>
                </div>
                <div class="modal-body">
                    <p><?php echo $str_alert; ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="update_terms" name="update_terms" onclick="<?php echo $java_str; ?>"><?php echo $button_str; ?></button>
                </div>
            </div>
        </div>
    </div>
</section>
