<?php

    //Check for a valid session:
    require(dirname(__FILE__) . '/../../mibase_check_login.php');

    //Create the del variable:
    $ref_delete = '';

    //Setup and run the query:
    $query = "SELECT * from age ORDER by age";
    $conn = pg_connect($_SESSION['connect_str']);
    $result = pg_exec($conn, $query);
    $numrows = pg_numrows($result);

    //Include the heading.php file:
    include('heading.php');

    //For the number of results:
    for ($ri = 0; $ri < $numrows; $ri++) {

        //Extract the data:
        $age = pg_fetch_array($result, $ri);
        //Setup the delete portion:
        $ref_delete = '<form action="delete.php" method="POST"><input type="hidden" name="rt_id" id="rt_id" value="' . $age['id'] . '">';
        $ref_delete .= '<input id="submit" name="submit" class="btn btn-danger btn-sm"  type="submit" value="Delete" /></form>';
        //Include the row.php file:
        include('row.php');

    }

?>