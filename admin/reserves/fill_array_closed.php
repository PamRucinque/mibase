<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');

$query_loan = "select date_roster, max(duration) as duration from roster "
        . "where status = 'Closed' group by date_roster, duration order by date_roster";
$closed = array();

include('../connect.php');

$result_loan = pg_Exec($conn, $query_loan);
$numrows = pg_numrows($result_loan);
if ($numrows > 0) {

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result_loan, $ri);
        $no_days = $row['duration'];
        $date_start = date_create_from_format('Y-m-d', $row['date_roster']);

        //echo $no_days . "<br>";

        for ($xi = 0; $xi <= $no_days - 1; $xi++) {
            $str = '+' . $xi . 'day';
            $curr = strtotime($str, strtotime($row['date_roster']));
            $str_array = date("Y-m-d", $curr);
            //echo $str_array . ' ';
            array_push($closed, $str_array);
        }
    }
}


