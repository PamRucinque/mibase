<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');
if (isset($_GET['idcat'])) {
    $_SESSION['idcat'] = $_GET['idcat'];
}
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include('../header.php'); ?> 
        <link href="../css/jquery-ui.min.css" rel="stylesheet" type="text/css">
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/jquery-ui.js"></script>
     
    </head>
    <body id="main_body" onload="setFocus()" >
        <div id="form_container">
            <?php
            include('../menu.php');
            if (($branch == 'admin') || ($branch == 'dev')) {
                include ('../header_detail/header_detail.php');
            } else {
                include ('../header_detail/get_member_header.php');
            }
            include('find/find_member.php');

            include('find/get_toy.php');
            include('find/find_toy.php');

          //include('fill_array_reserve.php');

            if (!isset($_SESSION['borid'])) {
                echo '<font color="red">Please Select a member!</font>';
            }
      

            //include('calendar.php');
            include('reservations.php');
            // include('partypack_form.php');
            ?>

        </div>

    </body>
</html>

