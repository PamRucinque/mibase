<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script src="../js/jquery-2.0.3.min.js"></script>
<script src="../js/jquery-ui.js"></script>
<script>
    function setFocus()
    {
        document.getElementById("memberid").focus();
    }
    $(function () {

        //autocomplete
        $(".auto2").autocomplete({
            source: "find/search_mem.php",
            autoFocus: true,
            select: function (event, ui) {

                var selectedObj = ui.item;

                document.getElementById('borid').value = selectedObj.value
                document.forms["change"].submit();
            }

        });

    });
</script>

<div class="row">
    <div class="col-sm-2 col-xs-2">
        <form  id="scan_mem" method="post" action="reservation.php" width="100%"><br>
            <input align="center" type="text" name="memberid" id ="memberid" class="form-control" placeholder="Member id" onchange='this.form.submit()'></input>
        </form>    
    </div>
    <div class="col-sm-3 col-xs-8">            
        <form id="change" method="get" action="reservation.php">
            <br>
            <input type='text' name='longname' id='longname' placeholder="Search for member" class='auto2 form-control' onKeyPress="login()"></p>
            <input type="hidden" id="borid" name ="borid" />
        </form></div>
    <div class="col-sm-7 col-xs-6"><?php include('member_detail_find.php'); ?></div>
</div>