<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<script>
    function setFocus()
    {
        document.getElementById("scanid").focus();
    }
</script>
<script type="text/javascript">
    $(function () {

        //autocomplete
        $(".auto").autocomplete({
            source: "find/search.php",
            autoFocus: true,
            select: function (event, ui) {
                //For better understanding kindly alert the below commented code
                //alert(ui.toSource()); 
                var selectedObj = ui.item;

                //alert(selectedObj.value);
                document.getElementById('idcat').value = selectedObj.value
                document.forms["change_toy"].submit();
            }

        });

    });
</script>
<script src="../js/jquery-2.0.3.min.js"></script>
<script src="../js/jquery-ui.js"></script>
<?php 
include('get_toy.php');
include('toy_detail_find.php');
?>
<div class="row">
    <div class="col-sm-2 col-xs-6">
        <form  id="scan" method="post"  action="reservation.php" width="100%"><br>
            <input align="center" type="text" name="scanid" id ="scanid" placeholder="Scan toy" class="form-control" onchange='this.form.submit()'></input>
        </form>   
    </div>
    <div class="col-sm-3 col-xs-6">
        <form id="change_toy" method="post" action="reservation.php" width="100%">
            <br>
            <input type='text' name='toyid' id='toyid' value='' class='auto form-control' placeholder="Search for a toy">
            <input type="hidden" id="idcat" name ="idcat" />
        </form>
    </div>

    <div class="col-sm-4 col-xs-6"><?php echo $str_toy; ?></div>
    <div class="col-sm-3 col-xs-6"><?php echo $str_pic; ?></div>
</div>


