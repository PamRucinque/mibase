<?php

function draw_calendar($month, $year, $reserve_to, $reserve_from, $longname, $reserve_period) {
    include('get_settings.php');
    //echo 'Reserve Period: ' . $reserve_period;
    include('fill_array.php');
    include('fill_array_loan.php');
    include('fill_array_reserve.php');
    include('fill_array_closed.php');
    include('fill_array_open.php');
    if ($holiday_hire == 'Yes') {
        $holiday = get_holiday_length($reserve_period, $reserve_from);
    }
    if ($reserve_period == 0) {
        $reserve_period = 14;
    }
    /* draw table */
    $calendar = '<table cellpadding="0" cellspacing="0" class="calendar" width="50%">';

    /* table headings */
    $headings = array('Sun', 'Monday', 'Tuesday', 'Wed', 'Thurs', 'Friday', 'Saturday');
    $calendar.= '<tr class="calendar-row"><td class="calendar-day-head">' . implode('</td><td class="calendar-day-head">', $headings) . '</td></tr>';

    /* days and weeks vars now ... */
    $running_day = date('w', mktime(0, 0, 0, $month, 1, $year));
    $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
    $days_in_this_week = 1;
    $day_counter = 0;
    $dates_array = array();

    /* row for week one */
    $calendar.= '<tr class="calendar-row">';

    /* print "blank" days until the first of the current week */
    for ($x = 0; $x < $running_day; $x++):
        $calendar.= '<td class="calendar-day-np"> </td>';
        $days_in_this_week++;
    endfor;

    /* keep going with days.... */
    for ($list_day = 1; $list_day <= $days_in_month; $list_day++):
        //$s = $list_day . '/' . $month . '/' . $year;
        $s = $year . '-' . sprintf("%02d", $month) . '-' . sprintf("%02d", $list_day);

        if ($list_day < 10) {
            $day = "0" . $list_day;
        } else {
            $day = $list_day;
        }
        $id = $year . $month . $list_day;
        $id_str = $year . '-' . sprintf("%02d", $month) . '-' . sprintf("%02d", $list_day);
        $sel_date = $year . '-' . $month . '-' . $day;
        if ($reserve_period == '') {
            $reserve_period = 14;
        }
        $id_str = $year . '-' . $month . '-' . $list_day;
        if ($id_str == $holiday['start']) {
            $reserve_period = $holiday['duration'];
            //$reserve_period = 20;
        }
        $dw = trim(date('l', strtotime($s)));
        $calendar_str = '<td id="' . $id . '" class="calendar-day" name="free"  onclick="get_start(&quot;' . $sel_date . '&quot;,' . $reserve_period . ');">';

        $status = 'open';

        if (count($weekday_array) != 0) {
            if (!in_array($dw, $weekday_array)) {
                $calendar_str = '<td id="' . $id . '" class="calendar-day-closed" name="free">';
            }
        }

        if (date($s) < date('Y-m-d')) {
            $calendar_str = '<td id="' . $id . '" class="calendar-day-closed" name="closed">';
        }
        if (in_array($s, $closed)) {
            $calendar_str = '<td id="' . $id . '" class="calendar-day-closed" name="booked">';
        }



        if (date($s) == date('Y-m-d')) {

            $calendar_str = '<td id="' . $id . '" class="calendar-day-today" name="free"   onclick="get_start(&quot;' . $sel_date . '&quot;,' . $reserve_period . ');">';
        }


        if (in_array($s, $a1)) {
            $key = array_search($s, $a1);
            $mem_str = $b1[$key];
            if (date($s) == date('Y-m-d')) {
                $calendar_str = '<td id="' . $id . '" class="calendar-day-today" name="free">' . $mem_str;
            } else {
                $calendar_str = '<td id="' . $id . '"  class="calendar-day-reserve"  name="booked" >' . $mem_str;
            }
        }

        if (in_array($s, $c)) {
            $key = array_search($s, $c);
            $mem_str = $d[$key];
            if (date($s) == date('Y-m-d')) {
                $calendar_str = '<td id="' . $id . '" class="calendar-day-today" name="free">' . $mem_str;
            } else {
                $calendar_str = '<td id="' . $id . '"  class="calendar-day-loan"  name="booked" >' . $mem_str;
            }
        }
        $calendar .= $calendar_str;


        if (in_array($s, $a1)) {
            $calendar.= '<div class="day-number" style="color:#0000FF">' . $list_day . '</div>';
        } else {
            if (in_array($s, $c)) {
                $calendar.= '<div class="day-number"  style="color:#0000FF">' . $list_day . '</div>';
            } else {
                $calendar.= '<div class="day-number">' . $list_day . '</div>';
            }

            //echo implode(",", $a1) . "<br>";
        }
        /** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! * */
        $calendar.= str_repeat('', 2);

        $calendar.= '</td>';
        if ($running_day == 6):
            $calendar.= '</tr>';
            if (($day_counter + 1) != $days_in_month):
                $calendar.= '<tr class="calendar-row">';
            endif;
            $running_day = -1;
            $days_in_this_week = 0;
        endif;
        $days_in_this_week++;
        $running_day++;
        $day_counter++;
    endfor;

    /* finish the rest of the days in the week */
    if ($days_in_this_week < 8):
        for ($x = 1; $x <= (8 - $days_in_this_week); $x++):
            $calendar.= '<td class="calendar-day-np"> </td>';
        endfor;
    endif;

    /* final row */
    $calendar.= '</tr>';

    /* end the table */
    $calendar.= '</table>';

    /* all done, return result */
    return $calendar;
}

function get_holiday_length($reserve_period, $reserve_from) {

    include('../connect.php');
    $ok = 'No';
    $query = "select * from roster where date_roster > current_date and type_roster = 'Start' and status = 'Closed' LIMIT 1;";
    $nextval = pg_Exec($conn, $query);
//echo $connection_str;
    $row = pg_fetch_array($nextval);
    $duration = $row['duration'] + 2;
    $reserve_start = $row['date_roster'];
    $date_hol = date("Y-m-d", strtotime("-1 day", strtotime($row['date_roster'])));


    return array('duration' => $duration, 'start' => $date_hol);
}
