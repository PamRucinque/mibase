<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');
if (isset($_GET['idcat'])) {
    $_SESSION['idcat'] = $_GET['idcat'];
}
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include('../header.php'); ?> 
        <link href="../css/jquery-ui.min.css" rel="stylesheet" type="text/css">
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script type="text/javascript">
            function overlay() {
                if ($("#overlay").is(":visible")) {
                    $("#overlay").hide();
                    document.getElementById("date_end").focus();
                }
            }
            function overlay_success() {
                if ($("#overlay").is(":visible")) {
                    $("#overlay").hide();
                    $("#reserve_list").load(window.location.href + " #reserve_list");
                    document.getElementById("scanid").focus();
                }
            }
            $(function () {
                var pickerOpts = {
                    dateFormat: "yy-m-d",
                    showOtherMonths: true,
                    onSelect: function (date) {
                        //alert(date);
                        var x = document.getElementsByName("free");
                        for (i = 0; i < x.length; i++) {
                            x[i].style.backgroundColor = "white"
                            x[i].style = "day-np";
                        }

                        var end = document.getElementById('date_end').value;
                        var start = document.getElementById('date_start').value;
                        var str = '<h4><font color="red">The Reservation Start date is set to ' + start + ' and ends ' + end;
                        str = str + '</font></h4>';
                        document.getElementById('michelle').innerHTML = str;
                    },
                };
                $("#date_start").datepicker(pickerOpts);
                $("#date_end").datepicker(pickerOpts);
            });
            function myFunction()
            {
                alert("This Reservation has been saved!");
            }
            function show_history() {
                if ($("#history").is(":visible")) {
                    $("#history").hide();
                    document.getElementById('show').innerHTML = 'Show Reservation History';
                } else {
                    $("#history").show();
                    document.getElementById('show').innerHTML = 'Hide Reservation History';
                }
            }



        </script>

    </head>
    <body id="main_body" onload="setFocus()" >
        <div class="container-fluid" style="padding-left: 15px;">
            <?php
            include('../menu.php');
            include ('../header_detail/header_detail.php');

            include('find/find_member.php');

            include('find/toy_select.php');
            include('reservation_edit_form.php');
            include ('reservations.php');
            include ('reservations_loan.php');
            if ($mibase_server == 'Yes') {
                include ('reservations_nm.php');
            }
            include('data/check_dates.php');
            include('fill_array_closed.php');
            include('fill_array_open.php');
            include('fill_array_loan.php');
            include('get_settings.php');



            if (isset($_POST['submit']) && isset($_SESSION['borid'])) {
                include('../connect.php');
                $success = '';
                $idcat = $_SESSION['idcat'];
                $end = $_POST['date_end'];
                $str_alert .= $_SESSION['error'];

                $query = "INSERT INTO reserve_toy (member_id, approved, date_start, date_end, idcat, date_created, status)
                        VALUES (
                        {$_SESSION['borid']}, TRUE,
                        '{$_POST['date_start']}',
                        '{$_POST['date_end']}',
                        '{$idcat}',
                            NOW(), 'ACTIVE');";


                $dw = trim(date('l', strtotime($end)));



                if (count($weekday_array) != 0) {
                    If ($hire_everyday != 'Yes') {
                        if (!in_array($dw, $weekday_array)) {
                            //$success = print_r($weekday_array);
                            $success .= 'Please change the end date, the Library is closed on a ' . $dw . '.<br>';
                        }
                    }
                }
                if (in_array($end, $closed)) {
                    $success .= 'Please change the end date, the Library is closed on the ' . $end . '.<br>';
                }


                //$error = '';
                if ($success == '') {
                    $success = check_date($_POST['date_start'], $_POST['date_end'], $idcat);
                }
                if ($success == '') {
                    $success = check_date_loans($_POST['date_start'], $_POST['date_end'], $idcat);
                }

                //  $success = check_date_loans($_POST['date_start'], $_POST['date_end'], $idcat);
                if ($success != '') {
                    $str_alert = $success;
                    if (trim($str_alert) != '') {
                        include('data/overlay.php');
                    }
                    $str_alert = '';

                    //exit;
                } else {


                    // echo $query;
                    $result = pg_Exec($conn, $query);

                    if (!$result) {
                        echo "An INSERT query error occurred.\n";
                        echo $query;
                        //exit;
                    } else {
                        $str_alert = 'Success! your reservation has been saved.<br>';
                        if (trim($str_alert) != '') {
                            include('data/overlay_success.php');
                        }
                        $str_alert = '';
                    }
                }
                //pg_FreeResult($result);
// Close the connection
                //pg_Close($conn);
            }
            $str_alert = $_SESSION['error_nm'];
            if (trim($str_alert) != '') {
                include('data/overlay_success.php');
            }
            $str_alert = '';
            $_SESSION['error_nm'] = '';

            echo $error;
            include('fill_array.php');

            //print_r($c);

            $today = date("Y-m-d");

            //echo 'today: ' . $today;
            $x = 30;
            for ($xi = 1; $xi <= 90; $xi++) {
                $str = '+' . $xi . 'day';
                $str_end = '+' . $xi + $reserve_period - 1 . 'days';
                $curr = strtotime($str, strtotime($today));
                $curr_end = strtotime($str_end, strtotime($today));
                $str_day = date("Y-m-d", $curr);
                $str_day_end = date("Y-m-d", $curr_end);
                $p = check_date($str_day, $str_day_end, $idcat);
                if (in_array($str_day, $c)) {
                    $p .= 'On Loan';
                }
                if (in_array($str_day_end, $c)) {
                    $p .= 'On Loan';
                }
                //$p .= check_date_loans($str_day, $str_day_end, $idcat);

                $dw = trim(date('l', strtotime($str_day)));
                if ((in_array($dw, $weekday_array) && ($p == ''))) {
                    $stat = ' OK';
                    //echo $str_day . ' ' . $str_day_end . $p;
                    $reserve_from = $str_day;
                    $reserve_to = $str_day_end;
                    break;
                }
            }


            //include('fill_array_loan.php');
            //include('fill_array_reserve.php');

            if (!isset($_SESSION['borid'])) {
                echo '<font color="red">Please Select a member!</font>';
            }



            //echo $_SESSION['borid'];




            include('calendar.php');
            $legend_txt = '';
            $legend_txt .= '<table width="40%"><tr>';
            $legend_txt .= '<td align="center">Legend: </td>';
            $legend_txt .= '<td align="center" bgcolor="#fc7f8e">On Loan</td>';
            $legend_txt .= '<td align="center" bgcolor="#e6ec8b">Reserved</td>';
            $legend_txt .= '<td align="center" bgcolor="#CEF6F5">Selected</td>';
            $legend_txt .= '<td align="center" bgcolor="lightgrey">Closed</td>';
            $legend_txt .= '<td align="center" bgcolor="#ff9980">Today</td>';

            $legend_txt .= '<td align="right"><h2><font color="red"></font></h2></td>';
            //$legend_txt .= '<td align="right"><a class="button1" title="List of Reserves" href="reservation_list.php">List View</a></td>';
//echo '<td bgcolor="lightblue">New Reservation (default period)</td>';
            $legend_txt .= '</tr></table>';
            echo $legend_txt;
            echo $calendar_text;
            //include('reservations.php');
            // include('partypack_form.php');
            ?>

        </div>

    </body>
</html>

<?php

function get_reserve_dates($idcat) {
    //$max_date = $max_due;
    include('../connect.php');

    $query = "select max(date_end) as max_date from reserve_toy where idcat = '" . $idcat . "' and date_end > current_date;";
    $a1 = array();

    $result1 = pg_Exec($conn, $query);
    $numrows = pg_numrows($result1);
    //echo $max_due;
    if ($numrows > 0) {
        $row = pg_fetch_array($result1, $ri);
        $max_date = $row['max_date'];
    }
    if ($max_date == '') {
        $max_date = 'no reserves';
    }


    pg_FreeResult($result1);

    pg_Close($conn);
    //echo $max_due . '<br>';
    return $max_date;
}
?>