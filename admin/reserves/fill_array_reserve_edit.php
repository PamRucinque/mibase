<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');
include('../connect.php');



$query_array = " SELECT reserve_toy.*,
 (select firstname from borwrs where reserve_toy.member_id = Borwrs.id) as firstname,
 (select surname from borwrs where reserve_toy.member_id = Borwrs.id) as surname,
 (select toyname from toys where reserve_toy.idcat = toys.idcat) as toyname 
 from reserve_toy where reserve_toy.idcat = '" . $_SESSION['idcat'] . "' and date_end > current_date 
 and status = 'ACTIVE' and id != " . $reserve_id . " ORDER BY reserve_toy.date_start;";

$a1 = array();
$b1 = array();


$result_array = pg_Exec($conn, $query_array);
$x = pg_numrows($result_array);
if ($x > 0) {

    for ($ri = 0; $ri < $x; $ri++) {
        $row = pg_fetch_array($result_array, $ri);
        $date_end = date_create_from_format('Y-m-d', $row['date_end']);
        $date_start = date_create_from_format('Y-m-d', $row['date_start']);
        $date_diff = round(($row['date_end'] - $row['date_end']));
        if (strlen($row['firstname']) > 8) {
            $firstname = substr($row['firstname'], 0, 6)  . '..';
        } else {
            $firstname = $row['firstname'];
        }
        if (strlen($row['surname']) > 8) {
            $surname = substr($row['surname'], 0, 6)  . '..';
        } else {
            $surname = $row['surname'];
        }
        $member = $firstname . ' ' . $surname . '<br>' . $row['member_id'];
        $interval = $date_start->diff($date_end);
        //echo $interval->format('%a');
        $no_days = $interval->format('%a');
        //echo $no_days . "<br>";

        for ($xi = 0; $xi <= $no_days; $xi++) {
            $str = '+' . $xi . 'day';
            $curr = strtotime($str, strtotime($row['date_start']));
            $str_array = date("Y-m-d", $curr);
            array_push($a1, $str_array);
            array_push($b1, $member);
        }
    }
}

pg_FreeResult($result1);
// Close the connection
pg_Close($conn);
?> 

