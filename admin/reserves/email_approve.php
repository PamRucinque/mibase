<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');

$_SESSION['loan_status'] = ' ';

if (isset($_SESSION['borid'])) {
    include 'data/get_settings.php';
    include('data/get_template_email.php');
    include('data/get_member.php');
    include('data/get_loans.php');

    $str_now = date("Y-m-d H:i:s");
    $str_now_short = date("d/m/Y");
    $to_print = '';
    $to_print .= $template_message;
    $to_print = str_replace("[toylist]", $toyslist, $to_print);
    $to_print = str_replace("[toylist_long]", $str_receipt_long, $to_print);
    $to_print = str_replace("[toyslist_pieces]", $toyslist_pieces , $to_print);
    $to_print = str_replace("[longname]", $longname, $to_print);
    $to_print = str_replace("[expired]", $format_expired, $to_print);
    $to_print = str_replace("[now]", $str_now, $to_print);
    $to_print = str_replace("[now_short]", $str_now_short, $to_print);
    $to_print = str_replace("[borid]", $borid, $to_print);
    $to_print = str_replace("[roster]", $receipt_roster, $to_print);
    $to_print = str_replace("[next_duty]", $next_duty, $to_print);
    $to_print = str_replace("[balance]", $receipt_balance, $to_print);
    $to_print = str_replace("[payments]", $payments, $to_print);
    $to_print = str_replace("[completed]", $completed, $to_print);
    $to_print = str_replace("[duties]", $duties, $to_print);
    $to_print = str_replace("[member_alert]", $alert_mem_txt, $to_print);
    
     //$alert_mem_txt
     
    
    $subject = $libraryname  . ' Loan Receipt.';
    $header = get_header($libraryname, $email_from);
    
    $result = send_email($email, $subject, $to_print, $header['header'], $header['param']);
    if ($result){
        $status = '<h3><font color="blue">EMail sent to ' . $email . '</font></h3>';
    }else{
        $status = '<h3><font color="blue">EMail failed to send to ' . $email . ' ' . $libraryname .  '</font></h3>';
    }

    $borid = $_SESSION['borid'];

    $_SESSION['loan_status'] .= '<h3><font color="blue">' . $status . '</font></h3>';
}
function send_email($to, $subject, $message, $header, $param) {
    if ($to == '') {
        $output = '<br>Email not sent, email is blank.<br>';
    } else {
        $script_start = '<html>
<body>
<style>
p {
    font-size: 14px;
    margin: 0;
    padding: 0;
    border: 0;
}
</style>';
        $script_end = '</body></html>';
        $message = $script_start . $message . $script_end;
        $output = @mail($to, $subject, $message, $header, $param);
        //@mail('michelle@mibase.com.au',$template['subject'], $template['message'], $email['header'], $email['param']);
    }
    return $output;
}

function get_header($libraryname, $email_from) {
    if ($email_from == '') {
        $email_from = 'noreply@mibase.com.au';
    }
    $headers = '';
    $headers .= "From: " . $libraryname . " <" . $email_from . ">\r\n";
    $headers .= "Reply-To: " . $libraryname . " <" . $email_from . ">\r\n";
    $headers .= "Cc: " . $libraryname . " <" . $email_from . ">\r\n";
    $headers .= "Return-Path: " . $email_from . "\r\n";
    $headers .= "Organization: " . $libraryname . "\r\n";
    $headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    $headers .= "Content-Transfer-Encoding: 7bit\n\n";
    $param = "-f" . $email_from;

    return array('header' => $headers, 'param' => $param);
}
?>
