<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<link rel="stylesheet" type="text/css" href="../css/calendar.css" media="screen"></link>
<script  type="text/javascript" src="js/calendar.js"></script>
<script>



</script>
<h2>Reservation Calendar: <font color="red">Click on the Calendar start date to select the dates.</font></h2>
<?php
include('functions.php');

if ($_SESSION['timezone'] == '') {
    date_default_timezone_set('Australia/ACT');
} else {
    date_default_timezone_set($_SESSION['timezone']);
}


$curr_month = date("F");
$curr_month_2 = date("m");
$curr_year = date("Y");

$calendar_text = '';


//echo 'Current Month: ' . $curr_month;
//echo $curr_year  . $curr_name;
$calendar_text = '<table><tr><td valign="top">';
$calendar_text .= '<h2>' . $curr_month . ' ' . $curr_year . '</h2>';
$curr_name = $firstname . ' ' . $surname;
//function draw_calendar($month, $year, $reserve_to, $reserve_from, $longname, $reserve_period) {
$calendar_text .= draw_calendar($curr_month_2, $curr_year, $reserve_to, $reserve_from, $curr_name, $reserve_period);
$calendar_text .= '</td>';

//echo date("Y:F",strtotime("-1 Months")); 
$date = date("Y-m-01");
//$newdate = strtotime ( '+1 month' , strtotime ($date) ) ;
$curr_month = date("F", strtotime('+1 month', strtotime($date)));
$curr_month_2 = date("m", strtotime('+1 month', strtotime($date)));
$curr_year = date("Y", strtotime('+1 month', strtotime($date)));

//echo $curr_month_2;
//$curr_year = '2017';

$calendar_text .= '<td valign="top">';
$calendar_text .= '<h2>' . $curr_month . ' ' . $curr_year . '</h2>';
$calendar_text .= draw_calendar($curr_month_2, $curr_year, $reserve_to, $reserve_from, $curr_name, $reserve_period);
$calendar_text .= '</td></tr>';



$calendar_text .= '<tr><td valign="top">';
$curr_month = date("F", strtotime('+2 months', strtotime($date)));
$curr_month_2 = date("m", strtotime('+2 months', strtotime($date)));
$curr_year = date("Y", strtotime('+2 months', strtotime($date)));

$calendar_text .= '<h2>' . $curr_month . ' ' . $curr_year . '</h2>';
$calendar_text .= draw_calendar($curr_month_2, $curr_year, $reserve_to, $reserve_from, $curr_name, $reserve_period);
$calendar_text .= '</td>';
$month_today = date("F");
//echo $month_today;
//$curr_month = date("F", strtotime("+90 Days"));
$calendar_text .= '<td valign="top">';
$curr_month = date("F", strtotime('+3 months', strtotime($date)));
$curr_month_2 = date("m", strtotime('+3 months', strtotime($date)));
$curr_year = date("Y", strtotime('+3 months', strtotime($date)));

$calendar_text .= '<h2>' . $curr_month . ' ' . $curr_year . '</h2>';
$calendar_text .= draw_calendar($curr_month_2, $curr_year, $reserve_to, $reserve_from, $curr_name, $reserve_period);
$calendar_text .= '</td></tr>';

$curr_month = date("F", strtotime('+4 months', strtotime($date)));
$curr_month_2 = date("m", strtotime('+4 months', strtotime($date)));
$curr_year = date("Y", strtotime('+4 months', strtotime($date)));

$calendar_text .= '<tr><td valign="top">';
$calendar_text .= '<h2>' . $curr_month . ' ' . $curr_year . '</h2>';
$calendar_text .= draw_calendar($curr_month_2, $curr_year, $reserve_to, $reserve_from, $curr_name, $reserve_period);
$calendar_text .= '</td>';

$curr_month = date("F", strtotime('+5 months', strtotime($date)));
$curr_month_2 = date("m", strtotime('+5 months', strtotime($date)));
$curr_year = date("Y", strtotime('+5 months', strtotime($date)));

$calendar_text .= '<td valign="top">';
$calendar_text .= '<h2>' . $curr_month . ' ' . $curr_year . '</h2>';
$calendar_text .= draw_calendar($curr_month_2, $curr_year, $reserve_to, $reserve_from, $curr_name, $reserve_period);
$calendar_text .= '</td></tr></table>';
?>