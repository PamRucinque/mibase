<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');
include('../get_settings.php');

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $_SESSION['error_nm'] = '';

    $conn = pg_Connect("port=5432 dbname=new_members user=new_members password=WSjuJ9YLj43JUNL");
    $query = "SELECT reservations.*, 
firstname || ' ' || surname  as longname 
from reservations 
 WHERE id = " . $_GET['id'] . " ORDER BY date_start;";
    //echo $query;

    $result = pg_exec($conn, $query);
    $numrows = pg_numrows($result);
    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = pg_fetch_array($result, $ri);
        $date_start_nm = $row['date_start'];
        $date_end_nm = $row['date_end'];
        $firstname = $row['firstname'];
        $surname = $row['surname'];
        $suburb = $row['city'];
        $postcode = $row['postcode'];
        $borid_nm = $row['id'];
        $borname = $row['longname'];
        $idcat_nm = $row['idcat'];
        $address_nm = $row['address'];
        $contact_nm = $row['contact'];
        $email_nm = $row['emailaddress'];
        $mobile_nm = $row['mobile'];
        $contact_address = $row['contact_address'];
        $contact_phone = $row['contact_phone'];
        $levy_nm = $row['levy'];
        $created_nm = $row['created'];
    }
    //$expired = date($date_start_nm, ;
    
    include('../connect.php');
    if ($levy_nm == 't') {
        $alert_msg = 'This member would like to pay the levy, please change/update the member type.';
        echo '<script> $(document).ready(function(){ alert("' . $alert_msg . '");});</script>';
        $alert_msg = '';
    }
    // get the 'id' variable from the URL
    //check dates don't overlap
    include('data/check_dates_nm.php');
    $output = check_date_nm($date_start_nm, $date_end_nm, $idcat_nm);
    include('get_membertype_hire.php');
    if ($membertype_hire != 'NA') {
        $membertype = $membertype_hire;
    }
    $plus_period = '+' . $expiryperiod . ' month';
    $plus_period = $date_start_nm .  " +" . $expiryperiod . " month";

    //echo date( "Y-m-d", strtotime( $plus_period));
    $expired = date( "Y-m-d", strtotime( $plus_period));;
    //echo $expired;
    //echo $output;
    //$type = get_type($membertype);
    //$output = 'NA';
    //echo 'Renewal Fee: ' . $type['renewal'] . '<br>';
    if ($output == 'ok') {
        $id = $_GET['id'];
        include('new_memid.php');
        $query = "INSERT INTO reserve_toy (member_id, approved, date_start, date_end, idcat, borname, 
            emailaddress, contact, phone, address, date_created, status)
                            VALUES (
                            {$newmemid}, TRUE,
                            '{$date_start_nm}',
                            '{$date_end_nm}',
                            '{$idcat_nm}', '{$borname}',
                            '{$email_nm}', '{$contact_nm}',   
                            '{$mobile_nm}', '{$address_nm}',  
                                NOW(), 'ACTIVE');";
        $str = $surname;
        $length_surname = strlen($str);
        if ($length_surname < 10) {
            $str = pad_surname($str);
        } else {
            $str = substr($str, 0, 10);
            $length_surname = 10;
        }
        $str = strtolower($str);
        $username = rtrim($str, "%") . $newmemid;
        $username = str_replace(" ", "", $username);
        $username = str_replace("-", "", $username);
        $username = str_replace("_", "", $username);
        $longname = $firstname . ' ' . $surname;
        $pwd = generateStrongPassword();

        $query .= "INSERT INTO borwrs (id, firstname, surname, partnersname, partnerssurname, expired, phone2, address, membertype, member_status, suburb, postcode,datejoined, renewed, "
                . "emailaddress, mobile1, pwd, rostertype5, date_application) VALUES ("
                . "{$newmemid}, '{$firstname}', '{$surname}', '{$contact_nm}', '{$contact_address}','{$expired}', '{$mobile_nm}',"
                . "'{$address_nm}', '{$membertype}', 'ACTIVE','{$suburb}','{$postcode}', NOW(), NOW(), '{$email_nm}', '{$contact_phone}', '{$pwd}', '{$username}', '{$created_nm}' )";
        //echo $query;
                $result = pg_exec($conn, $query);

        if (!$result) {
            $message = 'Invalid query: ' . "\n";
            $message = 'Whole query: ' . $query;
            //die($message);
            echo $message;
        } else {
            include('data/get_template_email.php');
            $borid = $newmemid;

            include('send_email_new.php');
            if ($automatic_debit_off != 'Yes') {
                $joined = date('Y-m-d');
                $type = get_type($membertype);
                $amount = $type['renewal'];
                $description = 'Membership fees';
                $desc_levy = 'Membership Levy';
                $category = $type['description'];
                $levy = $type['levy'];
                if ($category == '') {
                    $category = $membertype;
                }
                //include('new_paymentid.php');
                $payment_str = add_to_journal($joined, $newmemid, 0, $longname, $description, $category, $amount, 'DR', 'SUBS');
                if ($levy != 0) {
                    $str = add_to_journal($joined, $newmemid, 0, $longname, $desc_levy, $desc_levy, $levy, 'DR', 'SUBS');
                    $payment_str .= '<br>The Membership Levy of ' . $levy . ' has been added.';
                }
                $_SESSION['error_nm'] = '<br>' . $payment_str;
            }

             include('../connect_toybase.php');
            $query = "DELETE from reservations WHERE id = " . $_GET['id'] . ";";
            $result = pg_exec($conn, $query);
        }
    } else {
        $_SESSION['error_nm'] = 'This reservation cannot be approved, ' . $output;

        //$str_alert = '';
    }
    $redirect = 'Location: reservation.php?idcat=' . $_SESSION['idcat'];
    //echo $redirect;
  
    header($redirect);
    
}

function generateStrongPassword($length = 10, $add_dashes = false, $available_sets = 'luds') {
    $sets = array();
    if (strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if (strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if (strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    if (strpos($available_sets, 's') == false)
        $sets[] = '!@#$%&*?';
    $all = '';
    $password = '';
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for ($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if (!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while (strlen($password) > $dash_len) {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

function pad_surname($string) {
    $length = 10;
    $out_string = '';
    for ($x = 0; $x < $length; $x++) {
        $char = substr($string, $x, 1);
        if ($char != '') {
            $out_string .= $char;
        } else {
            $out_string .= '%';
        }
    }

    return $out_string;
}

function add_to_journal($datepaid, $bcode, $icode, $name, $description, $category, $amount, $type, $typepayment) {

    include('../connect.php');
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);

    $query_payment = "INSERT INTO journal (datepaid, bcode, icode, name, description, category, amount, type, typepayment)
                 VALUES (?,?,?,?,?,?,?,?,?);";

    $sth = $pdo->prepare($query_payment);
    $array = array($datepaid, $bcode, $icode, $name, $description, $category, $amount,
        $type, $typepayment);

    //$result_payment = pg_Exec($conn, $query_payment);
    if ($amount > 0) {
        $sth->execute($array);
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {

            $result = 'Error' . $stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>' . $query_payment;
            exit;
        } else {
            $result = 'The Membership fee of ' . $amount . ' has been added';
        }
    } else {
        $result = 'Amount needs to be greater than 0';
    }

    pg_FreeResult($result_payment);
    return $result;
}

function get_type($membertype) {
    include('../connect.php');
    $sql = "SELECT * FROM membertype 
          WHERE (membertype)='" . $membertype . "'";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $renewal_fee = $row['renewal_fee'];
    $description = $row['description'];
    $levy = $row['bond'];
    return array('renewal' => $renewal_fee, 'description' => $description, 'levy' => $levy);
}

?>
