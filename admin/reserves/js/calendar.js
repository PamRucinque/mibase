document.getElementsByName("pending").onmousedown = function (event) {
    if (event.which == 3) {
        alert("right clicked!");
    }
}
function addDays(theDate, days) {
    return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
}

function get_start(s, days) {
    var input = s;
    var x = document.getElementsByName("free");
    //var y = document.getElementsByName("closed");
    var i;
    var d = new Date(s);
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    // x[i].style = "day-np";

    var elements = document.getElementsByName("free");
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.background = "white";
        elements[i].style = "day-np";
    }
    var elements = document.getElementsByName("closed");
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.background = "lightgrey";
        elements[i].style = "day-np";
    }

    var dt = addDays(new Date(s), days - 1);
    // var d = addDays(new Date(s), days);
    var yyyy = dt.getFullYear().toString();
    var mm = (dt.getMonth() + 1).toString();
    var dd = dt.getDate().toString();
    var s_yyyy = d.getFullYear().toString();
    var s_mm = (d.getMonth() + 1).toString();
    var s_dd = d.getDate().toString();
    var day_start = weekday[d.getDay()];
    var count = 0;

    if ((dt.getMonth() + 1) < 10) {
        var start = s_yyyy + "0" + s_mm + s_dd;
        var end_id = yyyy + "0" + mm + dd;
    } else {
        var start = s_yyyy + s_mm + s_dd;
        var end_id = yyyy + mm + dd;
    }
    start_format = s_dd + "/" + s_mm + "/" + s_yyyy;
    //alert(start_format);

    end = yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]);

    endname = document.getElementById(end_id).getAttribute("Name");
    
    //alert(end_id);
    if (endname == 'closed') {
        var alert_end = 'Closed';
        alert('The Library is closed when this toy is due to be returned, please select the end date manually with the calendar select box above.');
        //document.getElementById("date_end").focus();
    }
    if (endname == 'booked') {
        var alert_end = 'Closed';
        alert('This toy is reserved by another member on the date you are due to return it, please select an earlier end date manually from the calendar select box above.');
        document.getElementById("michelle").focus();
    }

    //''

    start = s;
    for (i = 0; i < days; i++) {
        var id = addDays(new Date(s), i);
        var yyyy_id = id.getFullYear().toString();
        var mm_id = (id.getMonth() + 1).toString();
        var dd_id = id.getDate().toString();
        if ((id.getMonth() + 1) < 10) {
            color = yyyy_id + "0" + mm_id + dd_id;
        } else {
            color = yyyy_id + mm_id + dd_id;
        }
        status = document.getElementById(color).getAttribute("Name");
        document.getElementById(color).style.backgroundColor = '#CEF6F5';
        //document.getElementById(color).setAttribute("name", 'pending');
        end = yyyy_id + "-" + (mm_id[1] ? mm_id : "0" + mm_id[0]) + "-" + (dd_id[1] ? dd_id : "0" + dd_id[0]);
        count = count + 1;
        if (alert_end == 'Closed') {
            break;
        }
    }



    reserve_status = '<strong><font color="red">A Reservation from the ' + start_format + ' for ' + count + ' ';
    reserve_status = reserve_status + ' days has been selected.</font></strong>';
    document.getElementById("date_start").value = s;
    document.getElementById("date_end").value = end;
    var div = document.getElementById('michelle').innerHTML = reserve_status;
    document.getElementById("scanid").focus();

}
function get_new(s, days) {
    msg = 'Not Finished yet ' + s;
    alert(msg);
}



