<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <title>Mibase - Edit Reservation Details</title>
        <?php include('../header.php'); ?>
        <script>
            function overlay_success() {
                if ($("#overlay").is(":visible")) {
                    $("#overlay").hide();
                }
                $( "#reserve_list" ).load(window.location.href + "reservation.php" );
            }
        </script>

    </head>



    <body id="main_body" >
        <div id="form_container">

            <?php
            include('../menu.php');
            include('get_reservation.php');
            echo '<br><a href="reservation.php" class="button1_red">Back to Reservations</a>';

            if (isset($_POST['submit'])) {

                include('get_reservation.php');
                      include('../connect.php');
                include('data/check_dates_edit.php');
                include('fill_array_reserve_edit.php');
                include('fill_array_open.php');
                include('fill_array_closed.php');
                include('fill_array_loan.php');

                $start_date = $_POST['date_start'];
                $end_date = $_POST['date_end'];
                $idcat = $_SESSION['idcat'];
                $memberid = $_POST['borid_part'];
                $reserve_id = $_POST['id'];

                $success = '';
                $warning = '';
                $ok = check_date_edit($start_date, $end_date, $idcat, $reserve_id, $same_day);
                if ($ok['status'] == 'OK') {
                    //echo 'Dates are ok.<br>';
                    if ($ok['msg'] != '') {
                        $alert = $ok['msg'];
                        //$alert .= '<br><a class="button1_green" href="reservation.php">OK</a>';
                    }
                } 

                
                //echo print_r($closed);
                $dw = trim(date('l', strtotime($start_date)));
                if (count($weekday_array) != 0) {
                    if (!in_array($dw, $weekday_array)) {
                        //$success = print_r($weekday_array);
                        $warning .= 'WARNING: Please change the start date, the Library is closed on a ' . $dw . '.<br>';
                    }
                }
                $dw = trim(date('l', strtotime($end_date)));
                if (count($weekday_array) != 0) {
                    if (!in_array($dw, $weekday_array)) {
                        //$success = print_r($weekday_array);
                        $warning .= 'WARNING: Please change the end date, the Library is closed on a ' . $dw . '.<br>';
                    }
                }

                if (in_array($start_date, $closed)) {
                    $warning .= 'WARNING: Please change the start date, the Library is closed on the ' . $start_date . '.<br>';
                }
                if (in_array($end_date, $closed)) {
                    $warning .= 'WARNING: Please change the end date, the Library is closed on the ' . $end_date . '.<br>';
                }
                if ($success == '') {
                    $success .= check_date_loans($_POST['date_start'], $_POST['date_end'], $idcat);
                }
                if ($success != '') {
                    //echo $success;
                }
                $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
                $sql = "UPDATE reserve_toy SET
                    date_start=?,
                    date_end=?,
                    member_id=?,
                    status=?
                    WHERE id=?;";
                if ($success == '') {
                    $sth = $pdo->prepare($sql);
                    $array = array($start_date, $end_date, $memberid, $_POST['status'], $reserve_id);
                    $sth->execute($array);
                    $stherr = $sth->errorInfo();
                    if ($stherr[0] != '00000') {
                        echo "An UPDATE query error occurred.\n";
                        echo $sql;
                        //echo $connect_pdo;
                    } else {
                        $str_alert = $ok['msg'];
                        if ($warning != '') {
                            $str_alert = '<font color="red">' . $warning . '</font>';
                        }
                        $str_alert .= 'Success! your reservation has been saved.<br>';
                        $str_alert .= '<br><a class="button1_green" href="reservation.php">OK</a>';

                        if (trim($str_alert) != '') {
                            include('data/overlay_success.php');
                        }

                        $str_alert = '';
                        //$headerStr = 'location: reservation.php?idcat=' . $idcat;
                        //header($headerStr);
                    }
                } else {
                    $str_alert = $success;
                    $str_alert .= '<br><a class="button1_green" href="edit.php?id=' . $reserve_id . '">OK</a>';

                    if (trim($str_alert) != '') {
                        include('data/overlay_success.php');
                    }
                    $str_alert = '';
                }
            } else {
                include('get_reservation.php');
                include('edit_form.php');
            }
            ?>
        </div>
    </body>
</html>