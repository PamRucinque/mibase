<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../../mibase_check_login.php');

date_default_timezone_set('Australia/Melbourne');

function check_date_nm($start_date, $end_date, $idcat) {
    $output = 'ok';
    //  $success = '';
    $query = "(SELECT date_loan as date_start, borid, due as date_end FROM transaction  
            WHERE idcat = '" . $idcat . "' AND return is null)
            UNION 
(SELECT date_start, member_id, date_end FROM reserve_toy WHERE status = 'ACTIVE' AND idcat = '". $idcat . "')
order by date_start";
    include('../connect.php');
    //echo $query;
    //$button = '<a class="button1" href="reservation.php?idcat=' . $_SESSION['idcat'] . '">OK</a>';
    $result = pg_Exec($conn, $query);
    //$now = new DateTime();
    $start = strtotime($start_date);  //the entered start date for the reservation
    $end = strtotime($end_date);  //the netered end date for the reservatiosn

    $today = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
    if ($start > $end) {
        $output = "Start date cannot be after than End date. <br>";
    }
    if (($start < $today) || ($end < $today)) {
        $output = 'Start or End date cannot be before than today.<br>';
    }


    $numrows = pg_numrows($result);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = pg_fetch_array($result, $ri);

            $date_end = strtotime($row['date_end']);
            $date_start = strtotime($row['date_start']);

            if ($end > $date_start AND $end < $date_end) {
                $output = 'Dates overlap, reservation failed. error code 1:<br>';
                //$success = false;
            } elseif ($start >= $date_start AND $start < $date_end) {
                $output = 'Dates overlap, reservation failed. error code 2.<br>';
                //$success = false;
            } elseif ($start < $date_start AND $end >= $date_end) {
                $output = 'Dates overlap, reservation failed. error code 3.<br>';
                //$success = false;
            }
        }
    }
    return $output;
}
?>


