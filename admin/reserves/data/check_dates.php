<?php
$branch = substr(getcwd(),22,strpos(getcwd() . '/','/', 22+1) - 22);
include(dirname(__FILE__) . '/../../mibase_check_login.php');

date_default_timezone_set('Australia/Melbourne');

function check_date($start_date, $end_date, $idcat) {
  //  $success = '';
    $query = "SELECT date_start, member_id, date_end FROM reserve_toy  
            WHERE status = 'ACTIVE' AND idcat = '" . $idcat . "' ORDER by date_start;";
    include('../connect.php');
    $button = '<a class="button1" href="reservation.php?idcat=' . $_SESSION['idcat'] . '">OK</a>';
    $result = pg_Exec($conn, $query);

   $start = strtotime($start_date);  //the entered start date for the reservation
    $end = strtotime($end_date);  //the netered end date for the reservatiosn

    $today = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
    if($start > $end) {
        return "Start date cannot be after than End date. <br>";
    }elseif (($start < $today) || ($end < $today)) {
                 return 'Start and or End date cannot be before than today.' . $button . '<br>';
    }
    
    
    $numrows = pg_numrows($result);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = pg_fetch_array($result, $ri);

            $date_end = strtotime($row['date_end']);
            $date_start = strtotime($row['date_start']);
 
            if ($end > $date_start AND $end < $date_end) {
                 return 'Dates overlap, reservation failed. error code 1: ' . $button . '<br>';
                //$success = false;
            } elseif ($start >= $date_start AND $start < $date_end) {
                 return 'Dates overlap, reservation failed. error code 2. '  . $button . '<br>';
                //$success = false;
            } elseif ($start < $date_start AND $end >= $date_end) {
                 return 'Dates overlap, reservation failed. error code 3. '  . $button . '<br>';
                //$success = false;
            }
        }
    }
    return '';
}

function check_date_loans($start_date, $end_date, $idcat) {
    $success = '';
    $query = "SELECT date_loan as date_start, due as date_end FROM transaction  
            WHERE idcat = '" . $idcat . "' AND return is null ORDER by date_start;";
    include('../connect.php');
    //echo $query;
    $result = pg_Exec($conn, $query);


    $start = date_create_from_format('Y-m-d', $start_date);
    $end = date_create_from_format('Y-m-d', $end_date);
    //echo date_format($date, 'Y-m-d');
    //echo "Start Date: " . date_format($start, 'd/m/Y' ) . " and end date is: " . date_format($end, 'd/m/Y') . "<br>";
    $now = date('Y-m-d');
    $today = date_create_from_format('Y-m-d', $now);
    $numrows = pg_numrows($result);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = pg_fetch_array($result, $ri);
            //echo "Start: " . $row['date_start'] . " End Date: " . $row['date_end'] . "<br>";
            //echo "Input Start: " . $start_date . " End Date: " . $end_date . "<br>";
            $date_end = date_create_from_format('Y-m-d', $row['date_end']);
            $date_start = date_create_from_format('Y-m-d', $row['date_start']);
            if (($start < $today) || ($end < $today)) {
                $success = 'Start and or End date cannot be less than today. <br>';
            }
            if (($end < $start)) {
                $success = 'End date cannot be less than Start date. <br>';
            }

            If (($start < $date_start) AND ($end < $date_end) AND ($end > $date_start)) {
                //AND (date_format('Y-m-d', $start) < $row['date_end'])
                // AND (date_format('Y-m-d', $end) > $row['date_end']))
                $success = "Dates overlap with current Loan, reservation failed <br>";
            }
            If (($start > $date_start) && ($start < $date_end) && ($end < $date_end)) {
                $success = "Dates overlap with current Loan, reservation failed <br>";
                //$success = false;
            }

            If ($start > $date_start AND $start < $date_end AND $end > $date_end) {
                $success = "Dates overlap with current Loan, reservation failed <br>";
                //$success = false;
            }
        }
    }
    return $success;
}
?>


