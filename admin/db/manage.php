<?php
/* 
 * Copyright (C) 2019 Mibase
 *
 * This program is provided under the terms of the GNU General Public License version 3
 * refer licence.html in root folder for full text.
 */

if (!session_id()) {
    session_start();
}

if (!isset($_SESSION['dbadmin'])) {
    // there is a session but no session variable app 
    // redirect the user to the login page
    header('location:' . $_SESSION['app_root_location'] . '/db/index.php');
    exit();
} else if ($_SESSION['dbadmin'] != 'true') {
    // there is a session with session variable app but the app session variable is not admin 
    // ( ie they are not logged in to the admin app ) then send them to the login page.
    header('location:' . $_SESSION['app_root_location'] . '/db/index.php');
    exit();
}

require( dirname(__FILE__) . '/../config.php');
require( dirname(__FILE__) . '/../version.php');
require(dirname(__FILE__) . '/db_functions.php');

//if the shared server is not defined then set it to be a single instance server
if (!isset($shared_server)) {
    $shared_server = false;
}

//if the $db_postgres_password is defined then set this user and password for the database and toybase user and password
if (isset($db_postgres_password)) {
    if ($shared_server) {
        $toybasedbuser = 'postgres';
        $toybasedbpasswd = $db_postgres_password;
        if (!isset($toybasedbname)) {
            $toybasedbname = 'toybase';
        }
    } else {
        if (!isset($dbname)) {
            $dbname = 'mibase';
        }
        $dbuser = 'postgres';
        $dbpasswd = $db_postgres_password;
    }
}

if (!$shared_server) {

    $s = get_state($dbhost, $dbport, $dbname, $dbuser, $dbpasswd, $code_version_minor);
    $state = $s['state'];
    $note = $s['note'];
    ?>
    <!DOCTYPE html>
    <head>
        <meta name="viewport" content="width=device-width">
        <link href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src='../js/jquery-3.0.0.js' type='text/javascript'></script> 
    </head>

    <body>
        <script>
            function go() {
                console.log("create");
                $("#action").val("create");
                $("#form1").submit();
                console.log("done");
            }
        </script>

        <div class="container">
            <div class="row">
                <div class="card card-container">
                    <h1>Mibase Database Administration</h1>
                    <div><?php echo $note ?></div>
                    <?php if ($state == NoDB) { ?>
                        <a href="create_db.php" class="btn btn-info" role="button"> Create Empty Database</a><br>
                    <?php } else if ($state == EmptyDB) { ?>
                        <a href="build_db.php" class="btn btn-info" role="button"> Build Database ( create tables, indexes etc )</a><br>
                    <?php } else if ($state == VersionMismatch) { ?>
                        <a href="upgrade_db.php" class="btn btn-info" role="button"> Upgrade Database</a><br>
                    <?php } else if ($state == NoDataDB) { ?>
                        <a href="fill_demo_data_db.php" class="btn btn-info" role="button"> Fill Database with Demo Data</a><br>
                    <?php } ?>
                    <br><a href="../login.php" class="btn btn-info" role="button"> Return to Login</a><br>
                </div><!-- /card-container -->
            </div><!-- /row -->
        </div><!-- /container -->

    </body>

    <?php
} else {
    //shared system code
    $s = get_toybase_state($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd);
    $state = $s['state'];
    $note = $s['note'];
    ?>
    <!DOCTYPE html>
    <head>
        <meta name="viewport" content="width=device-width">
        <link href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src='../js/jquery-3.0.0.js' type='text/javascript'></script> 
    </head>

    <body>
        <script>
            function go() {
                console.log("create");
                $("#action").val("create");
                $("#form1").submit();
                console.log("done");
            }
        </script>

        <div class="container">
            <div class="row">
                <div class="card card-container">
                    <h1>Mibase Database Administration</h1>
                    <div><?php echo $note ?></div>
                    <?php if ($state == NoDB) { ?>
                        <a href="create_db.php?toybase=true" class="btn btn-info" role="button"> Create Empty toybase Database</a><br>
                    <?php } else if ($state == EmptyDB) { ?>
                        <a href="build_db.php?toybase=true" class="btn btn-info" role="button"> Build toybase Database ( create tables etc )</a><br>
                    <?php } else if ($state == NoDataDB) {
                        ?>
                        <h3>Add a Library</h3>
                        <div>
                            <form name="form" method="post" action="add_db.php" >
                                <input name="library_code" type="text" id="library_code" class="form-control" placeholder="library_code" >
                                <input type="submit" name="Submit" value="Save" class="btn btn-info" role="button">  
                            </form><br>
                        </div>
                        <?php
                    } else {
                        echo '<table class="table">';
                        echo '<tr><th>Database</th><th>Details</th><th>Actions</th></tr>';
                        $results = get_libraries($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd);
                        for ($ri = 0; $ri < sizeof($results); $ri++) {
                            $row = $results[$ri];
                            $library_code = $row['library_code'];
                            $dbpassword = $row['dbpassword'];

                            $r = get_state($dbhost, $dbport, $library_code, $library_code, $dbpassword, $code_version_minor);
                            $state = $r['state'];
                            echo '<tr><td>' . $library_code . ' </td><td> ' . $r['note'] . '</td><td>';
                            if ($state == EmptyDB) {
                                echo '<a href="build_db.php?library_code=' . $library_code . '" class="btn btn-info" role="button"> Build Database ( create tables, indexes etc )</a><br>';
                            } else if ($state == VersionMismatch) {
                                echo '<a href="upgrade_db.php?library_code=' . $library_code . '" class="btn btn-info" role="button"> Upgrade Database</a><br>';
                            } else if ($state == NoDataDB) {
                                echo '<a href="fill_demo_data_db.php?library_code=' . $library_code . '" class="btn btn-info" role="button"> Fill Database with Demo Data</a><br>';
                            }
                            echo '</td></tr>';
                        }
                        echo '</table>';
                        ?>
                        <h3>Add a Library</h3>
                        <form name="form" method="post" action="add_db.php" >
                            <div class="col-md-6">
                                <input name="library_code" type="text" id="library_code" class="form-control" placeholder="library_code">
                                <input type="submit" name="Submit" value="Save" class="btn btn-info" role="button">  
                            </div>           
                        </form><br>
                        <?php
                    }
                    ?>
                    <br><a href="../login.php" class="btn btn-info" role="button"> Return to Login</a><br>
                </div><!-- /card-container -->
            </div><!-- /row -->
        </div><!-- /container -->

    </body>

    <?php
}

function get_libraries($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd) {
    // global $dbhost, $dbport, $dbname, $dbuser, $dbpasswd;
    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $toybasedbname;
    $pdo = new PDO($connect_pdo, $toybasedbuser, $toybasedbpasswd);
    $sql = "SELECT library_code, dbpassword FROM libraries ;";
    $sth = $pdo->prepare($sql);
    $array = array();
    $sth->execute($array);
    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        echo "A query error occurred.\n";
        echo $connect_pdo;
        echo 'Error' . $stherr[0] . '<br>';
        echo 'Error' . $stherr[1] . '<br>';
        echo 'Error' . $stherr[2] . '<br>';
    }
    return $result;
}
