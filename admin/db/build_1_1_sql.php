<?php
/* 
 * Copyright (C) 2019 Mibase
 *
 * This program is provided under the terms of the GNU General Public License version 3
 * refer licence.html in root folder for full text.
 */

$query = "--Mibase Database Version 1.1 --


SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

CREATE FUNCTION public.date_round(base_date timestamp with time zone, round_interval interval) RETURNS timestamp with time zone
    LANGUAGE sql STABLE
    AS \$_\$
SELECT TO_TIMESTAMP((EXTRACT(epoch FROM \$1)::INTEGER + EXTRACT(epoch FROM \$2)::INTEGER / 2)
                / EXTRACT(epoch FROM \$2)::INTEGER * EXTRACT(epoch FROM \$2)::INTEGER)
\$_\$;

CREATE SEQUENCE public.age_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE TABLE public.age (
    id bigint DEFAULT nextval('public.age_id_seq'::regclass) NOT NULL,
    age text NOT NULL
);


CREATE TABLE public.borwrs (
    id bigint NOT NULL,
    firstname character varying(100),
    surname character varying(100),
    membertype character varying(50),
    motherssurname character varying(100),
    partnersname character varying(100),
    partnerssurname character varying(100),
    address character varying(100),
    address2 character varying(100),
    address3 character varying(100),
    suburb character varying(100),
    city character varying(100),
    state character varying(100),
    postcode character varying(50),
    country character varying(50),
    work character varying(50),
    phone character varying(50),
    phone2 character varying(50),
    mobile1 character varying(50),
    mobile2 character varying(40),
    email boolean,
    email2 character varying(100),
    emailaddress character varying(100),
    rostertype character varying(100),
    rostertype2 character varying(100),
    rostertype3 character varying(40),
    rostertype4 character varying(40),
    rostertype5 character varying(40),
    id_license character varying,
    skills text,
    skills2 text,
    specialneeds text,
    alert text,
    notes text,
    rosternotes text,
    datejoined date,
    renewed date,
    expired date,
    resigneddate date,
    created timestamp with time zone DEFAULT now(),
    modified timestamp with time zone DEFAULT now(),
    flag boolean,
    lockdate date,
    levy boolean,
    lockreason character varying(200),
    resignedreason character varying(200),
    balance double precision,
    discoverytype character varying(50),
    lastarchive date,
    location character(50),
    member_status character varying(50),
    printbarcode boolean DEFAULT false,
    library character varying(50),
    pwd character varying(50) DEFAULT 'mibase'::character varying,
    helmet boolean,
    user1 character varying,
    rostertype6 character varying(100),
    marital_status character varying(20),
    age date,
    member_update character varying(50),
    changedby character varying(20),
    agree character varying(5),
    photos character varying(5),
    key character varying,
    helmet_date timestamp with time zone,
    dollar_to_date numeric(12,2),
    agree_date timestamp with time zone,
    wwc character varying(50),
    value_ytd numeric(12,2) DEFAULT 0,
    date_application date,
    conduct character varying,
    conduct_date timestamp with time zone,
    rego_id bigint,
    wwc2 character varying(50)
);



CREATE TABLE public.category (
    id bigint NOT NULL,
    category character varying(3) NOT NULL,
    description text,
    toys integer DEFAULT 1
);

CREATE TABLE public.children (
    childid bigint NOT NULL,
    child_name character varying(50),
    surname character varying(50),
    sex character varying(8),
    d_o_b date,
    notes character varying(1000),
    alert boolean,
    id bigint,
    changedby character varying(20)
);

CREATE SEQUENCE public.city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.city (
    id bigint DEFAULT nextval('public.city_id_seq'::regclass) NOT NULL,
    city character varying(50) NOT NULL,
    postcode character varying(10),
    exclude character varying(5) DEFAULT 'No'::character varying
);

CREATE SEQUENCE public.condition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.condition (
    id bigint DEFAULT nextval('public.condition_id_seq'::regclass) NOT NULL,
    condition character varying(50) NOT NULL
);

CREATE SEQUENCE public.contact_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.contact_log (
    id bigint DEFAULT nextval('public.contact_log_id_seq'::regclass) NOT NULL,
    type_contact character varying,
    details character varying,
    datetime time with time zone[],
    borid bigint,
    template_id bigint,
    template character varying(20),
    subject character varying,
    email character varying,
    created timestamp with time zone DEFAULT now()
);

CREATE SEQUENCE public.discovery_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.discovery (
    id bigint DEFAULT nextval('public.discovery_id_seq'::regclass) NOT NULL,
    discovery character varying(50) NOT NULL
);

CREATE SEQUENCE public.event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.event (
    id bigint DEFAULT nextval('public.event_id_seq'::regclass) NOT NULL,
    description text,
    memberid bigint,
    typeevent character varying(30),
    rostertype character varying(30),
    nohours numeric(12,1),
    completed boolean,
    alertuser boolean,
    event_date timestamp with time zone DEFAULT now(),
    amount double precision,
    username character varying(20)
);

CREATE SEQUENCE public.feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.feedback (
    id integer DEFAULT nextval('public.feedback_id_seq'::regclass) NOT NULL,
    library character varying(50),
    firstname character varying(25),
    lastname character varying(25),
    contact character(5),
    rating character varying(2),
    feedback character varying(1000)
);

CREATE SEQUENCE public.files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.files (
    id bigint DEFAULT nextval('public.files_id_seq'::regclass) NOT NULL,
    filename character varying,
    description character varying,
    type_file character varying(20),
    lastupdate time with time zone,
    access character varying(20)
);

CREATE SEQUENCE public.gift_cards_sq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.gift_cards (
    id bigint DEFAULT nextval('public.gift_cards_sq'::regclass) NOT NULL,
    p_name character varying,
    mobile character varying,
    email character varying,
    newsletter character varying(5),
    created timestamp with time zone,
    library character varying,
    email_sent character varying(5) DEFAULT 'No'::character varying,
    expired date,
    credit_sent character varying(5),
    amount numeric(12,2),
    borid bigint,
    online_id bigint
);

CREATE SEQUENCE public.holds_id_seq
    START WITH 126
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.homepage (
    id bigint NOT NULL,
    frontpage text,
    open_hours text,
    lastupdated date,
    webpage text,
    contact_info text,
    conditions text,
    payment_info character varying,
    castle character varying,
    castle_link character varying,
    helmet_waiver character varying,
    payment_info_join character varying,
    member_homepage character varying,
    wwc character varying,
    rego_id bigint,
    sponsor_page character varying,
    member_options character varying
);

CREATE SEQUENCE public.id_seq_users
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.journal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.journal (
    id bigint DEFAULT nextval('public.journal_id_seq'::regclass) NOT NULL,
    description text,
    name text,
    category text,
    cat2 text,
    typepayment text,
    bcode bigint,
    gst numeric(12,1),
    icode character varying(10),
    type character varying(2),
    amount numeric(12,1),
    datepaid date DEFAULT now(),
    debitdate timestamp with time zone DEFAULT now(),
    location character varying,
    changedby character varying(20),
    holdid bigint
);

CREATE TABLE public.loan_restrictions (
    id bigint NOT NULL,
    membertype character varying,
    category character varying,
    free bigint DEFAULT 0,
    weight numeric(12,2) DEFAULT 0,
    loan_type character varying NOT NULL,
    notes character varying(50)
);

CREATE TABLE public.location (
    id bigint NOT NULL,
    location character(15),
    description character varying(50)
);

CREATE SEQUENCE public.log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.log (
    ip character varying(20),
    id bigint DEFAULT nextval('public.log_id_seq'::regclass) NOT NULL,
    subdomain character varying(50),
    username character varying,
    password character varying,
    logintype character varying(10),
    status character varying,
    created timestamp without time zone DEFAULT now(),
    email character varying
);

CREATE SEQUENCE public.manufacturer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.manufacturer (
    id bigint DEFAULT nextval('public.manufacturer_id_seq'::regclass) NOT NULL,
    manufacturer text,
    address text,
    phone text,
    fax text,
    email text,
    contact text,
    discount numeric(12,1)
);

CREATE SEQUENCE public.membertype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.membertype (
    id bigint DEFAULT nextval('public.membertype_id_seq'::regclass) NOT NULL,
    membertype character varying,
    maxnoitems bigint,
    description text,
    renewal_fee numeric(12,1),
    duties numeric(12,2),
    units bigint,
    bond numeric(12,1),
    rent numeric(12,1),
    returnperiod bigint,
    expiryperiod bigint,
    paypal character varying,
    exclude character varying(3),
    due date,
    jc character varying,
    self_checkout character varying(5),
    extra bigint,
    gold_star integer DEFAULT 0
);

CREATE SEQUENCE public.nation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.nationality (
    id bigint DEFAULT nextval('public.nation_id_seq'::regclass) NOT NULL,
    nationality character varying NOT NULL,
    description character varying
);

CREATE TABLE public.notifications (
    id bigint NOT NULL,
    notification_type character(10) DEFAULT 'email'::bpchar NOT NULL,
    notification_name character varying(50) NOT NULL,
    days_before integer DEFAULT 0,
    weekday character varying(15),
    username character varying,
    password character varying,
    source character varying,
    description character varying,
    active character varying(3)
);

CREATE TABLE public.overdue (
    id integer NOT NULL,
    weekly_fine character varying(3) DEFAULT 'Yes'::character varying,
    rent_as_fine character varying(3) DEFAULT 'No'::character varying,
    fine_value numeric(5,2) DEFAULT 0,
    days_grace integer DEFAULT 0,
    record_fine character varying(3) DEFAULT 'No'::character varying,
    fine_factor numeric(5,2) DEFAULT 1,
    daily_limit numeric DEFAULT 0,
    daily_fine character varying(3),
    roundup_week character varying(3),
    hire_factor bigint DEFAULT 0 NOT NULL,
    extra_fine numeric(5,2) DEFAULT 0,
    custom character varying
);

CREATE SEQUENCE public.parts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.parts (
    id bigint DEFAULT nextval('public.parts_id_seq'::regclass) NOT NULL,
    datepart date,
    itemno text,
    itemname text,
    description text NOT NULL,
    type text,
    qty bigint,
    found boolean,
    borcode bigint,
    cost numeric(12,1),
    foundby text,
    alertuser boolean,
    changedby character varying(20),
    borname character varying,
    comments character varying,
    paid boolean DEFAULT false
);

CREATE TABLE public.partypack (
    toyname character varying,
    id bigint NOT NULL,
    desc1 text,
    desc2 text,
    cost numeric(10,2),
    lastupdated date
);

CREATE SEQUENCE public.partypack_id_seq
    START WITH 114
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.paymentoptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.paymentoptions (
    id bigint DEFAULT nextval('public.paymentoptions_id_seq'::regclass) NOT NULL,
    paymentoptions text NOT NULL,
    crdr text,
    accountcode text,
    amount numeric(12,1),
    typepayment text,
    group1 bigint,
    description character varying,
    \"group\" character varying,
    payment_group character varying(20)
);

CREATE SEQUENCE public.postcode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.postcode (
    id bigint DEFAULT nextval('public.postcode_id_seq'::regclass) NOT NULL,
    suburb text,
    postcode text
);

CREATE SEQUENCE public.report_id_seq
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.reports (
    id bigint DEFAULT nextval('public.report_id_seq'::regclass) NOT NULL,
    reportname character varying,
    category character varying,
    sql character varying,
    code character varying(20)
);

CREATE SEQUENCE public.reserve_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.reservations (
    approved boolean,
    member_id bigint,
    date_start date,
    date_end date,
    date_created timestamp without time zone,
    partypack_id bigint,
    id bigint DEFAULT nextval('public.reserve_id_seq'::regclass) NOT NULL
);

CREATE SEQUENCE public.reserve_toy_id_seq
    START WITH 118
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.reserve_toy (
    approved boolean,
    member_id bigint,
    date_start date,
    date_end date,
    date_created timestamp without time zone,
    partypack_id bigint,
    id bigint DEFAULT nextval('public.reserve_toy_id_seq'::regclass) NOT NULL,
    idcat character varying(50),
    borname text,
    phone text,
    status character varying(10),
    paid character varying(5),
    contact character varying,
    address character varying,
    emailaddress character varying,
    install_location character varying
);

CREATE SEQUENCE public.roster_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.roster (
    id bigint DEFAULT nextval('public.roster_id_seq'::regclass),
    date_roster date,
    member_id bigint,
    duration double precision DEFAULT 1,
    type_roster character varying(100),
    approved boolean DEFAULT false,
    session_role character varying(50),
    roster_session character varying(50),
    weekday character varying(100),
    date_created date,
    complete boolean DEFAULT false,
    location character varying,
    comments character varying,
    status character varying(10),
    modified timestamp with time zone,
    contact bigint DEFAULT 1,
    delete_comments character varying
);

CREATE SEQUENCE public.rostertypes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

SET default_with_oids = false;

CREATE TABLE public.rostertypes (
    id bigint DEFAULT nextval('public.rostertypes_id_seq'::regclass) NOT NULL,
    rostertype text,
    description character varying(50),
    nohours numeric DEFAULT 1,
    volunteers bigint,
    weekday character varying(10),
    location character varying,
    reservations character varying(3) DEFAULT 'Yes'::character varying
);

CREATE SEQUENCE public.select_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.selectbox (
    id bigint DEFAULT nextval('public.select_id_seq'::regclass) NOT NULL,
    selectbox character varying
);

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.settings (
    setting_name character varying NOT NULL,
    setting_value character varying,
    id bigint DEFAULT nextval('public.settings_id_seq'::regclass),
    description character varying,
    setting_type character varying
);

CREATE SEQUENCE public.stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.stats (
    id bigint DEFAULT nextval('public.stats_id_seq'::regclass) NOT NULL,
    current bigint,
    new bigint,
    locked bigint,
    date_stats date,
    timezone character varying,
    children bigint,
    week bigint,
    loans bigint,
    returns bigint,
    location character varying(20),
    members bigint,
    renewed bigint,
    weekday character varying(20),
    expired bigint,
    resigned bigint,
    total bigint,
    volunteer bigint
);

CREATE TABLE public.storage (
    id bigint NOT NULL,
    storage character varying(50)
);

CREATE SEQUENCE public.storage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.storage_id_seq OWNED BY public.storage.id;

CREATE TABLE public.stype (
    id bigint NOT NULL,
    stype character varying(5),
    description character varying,
    amount numeric(12,2) DEFAULT 0,
    overdue numeric(12,2) DEFAULT 0
);

CREATE TABLE public.sub_category (
    id bigint NOT NULL,
    sub_category character varying(3) NOT NULL,
    description text,
    category character varying(5)
);

CREATE SEQUENCE public.suburb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.suburb (
    id bigint DEFAULT nextval('public.suburb_id_seq'::regclass) NOT NULL,
    suburb character varying
);

CREATE SEQUENCE public.supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.supplier (
    id bigint DEFAULT nextval('public.supplier_id_seq'::regclass) NOT NULL,
    supplier text,
    address text,
    phone text,
    fax text,
    email text,
    contact text,
    discount numeric(12,1)
);

CREATE TABLE public.template (
    id bigint NOT NULL,
    datetime time with time zone DEFAULT now(),
    subject character varying(100),
    message text,
    type_template character varying,
    template_email character varying(50),
    delivery_type character varying(15)
);

CREATE SEQUENCE public.template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.template_id_seq OWNED BY public.template.id;

CREATE TABLE public.toy_holds (
    approved character varying,
    borid bigint,
    date_start date,
    date_end date,
    created timestamp without time zone,
    id bigint DEFAULT nextval('public.holds_id_seq'::regclass) NOT NULL,
    idcat character varying,
    status character varying(10),
    paid character varying,
    notify_date date,
    transid bigint,
    reminder_date date
);

CREATE SEQUENCE public.toy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.toys (
    counter bigint DEFAULT nextval('public.toy_id_seq'::regclass) NOT NULL,
    id bigint NOT NULL,
    category character varying(3) NOT NULL,
    idcat character varying(50),
    reservecode character varying(50),
    toyname character varying(150),
    rent numeric(10,2),
    age character varying(30),
    datestocktake date,
    withdrawn boolean,
    withdrawndate date,
    manufacturer character varying(50),
    discountcost numeric(12,2),
    cost numeric(12,2),
    supplier character varying(50),
    desc1 character varying(2000),
    desc2 character varying(2000),
    comments character varying(1000),
    status boolean,
    returndateperiod integer DEFAULT 14,
    lockreason character varying(50),
    returndate timestamp with time zone,
    no_pieces integer DEFAULT 0,
    date_purchase date,
    sponsorname character varying(200),
    warnings character varying(1000),
    printbarcode boolean,
    user1 character varying(200),
    alert character varying(2000),
    toy_status character varying(20),
    stype character varying(20),
    units integer DEFAULT 1,
    borrower character varying(200),
    withdrawnreason text,
    reservedfor text,
    missingpieces text,
    reserved boolean,
    lockdate date,
    created timestamp with time zone DEFAULT now() NOT NULL,
    modified timestamp with time zone DEFAULT now(),
    storage character varying,
    helmet boolean,
    copy integer DEFAULT 1,
    location character varying(20),
    stocktake_status character(20),
    packaging_cost money,
    process_time numeric(12,2),
    loan_type character varying(20),
    freight numeric(10,2) DEFAULT 0,
    color character varying,
    changedby character varying(20),
    history character varying,
    link character varying(100),
    twitter character varying(100),
    sub_category character varying(10),
    count_loans bigint,
    no_pic character varying(3),
    block_image character varying(10)
);

CREATE SEQUENCE public.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.transaction (
    created timestamp with time zone DEFAULT now(),
    id bigint DEFAULT nextval('public.transaction_id_seq'::regclass) NOT NULL,
    idcat character varying(50) NOT NULL,
    borid bigint NOT NULL,
    due date,
    return date,
    borname character varying(100),
    item character varying(100),
    location character varying(50),
    units integer,
    timetrans time without time zone,
    date_loan date DEFAULT (now())::date NOT NULL,
    phone text,
    email_status date,
    contact_status bigint,
    session character varying(3),
    group_trans character varying(50),
    returnby character varying(20),
    loanby character varying(20),
    renew integer,
    times_renew bigint DEFAULT 0
);

CREATE SEQUENCE public.typeevent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.typeevent (
    id bigint DEFAULT nextval('public.typeevent_id_seq'::regclass) NOT NULL,
    type text,
    nohours bigint,
    description character varying(100),
    amount double precision
);

CREATE SEQUENCE public.typepart_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.typepart (
    id bigint DEFAULT nextval('public.typepart_id_seq'::regclass) NOT NULL,
    typepart text,
    description character varying(100),
    picture character varying
);

CREATE SEQUENCE public.typepayment_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.typepayment (
    id bigint DEFAULT nextval('public.typepayment_id'::regclass) NOT NULL,
    typepayment character varying
);

CREATE TABLE public.users (
    username character varying(25),
    password character varying(32),
    id bigint DEFAULT nextval('public.id_seq_users'::regclass) NOT NULL,
    login_type character varying(15) DEFAULT 'member'::bpchar NOT NULL,
    subdomain character varying(25),
    location character varying(20),
    libraryid bigint,
    lock boolean DEFAULT false NOT NULL,
    email character varying,
    mobile character varying(30),
    created date DEFAULT now(),
    level character varying
);

CREATE TABLE public.version (
    item varchar(20) NOT NULL,
    major int NOT NULL,
    minor int NOT NULL
);

INSERT INTO public.version VALUES ('db', 1, 1);

CREATE SEQUENCE public.volunteer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.volunteer (
    id bigint DEFAULT nextval('public.volunteer_id_seq'::regclass) NOT NULL,
    volunteer character varying NOT NULL
);

CREATE SEQUENCE public.warning_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.warnings (
    warning character varying(10),
    description character varying,
    id bigint DEFAULT nextval('public.warning_id'::regclass) NOT NULL
);

ALTER TABLE ONLY public.storage ALTER COLUMN id SET DEFAULT nextval('public.storage_id_seq'::regclass);

ALTER TABLE ONLY public.template ALTER COLUMN id SET DEFAULT nextval('public.template_id_seq'::regclass);

INSERT INTO public.age VALUES (57, '12-18 months, 18 mnths-3 ');
INSERT INTO public.age VALUES (58, '12-18 months, 3-5 years');
INSERT INTO public.age VALUES (59, '18 mnths-3 yrs');
INSERT INTO public.age VALUES (60, '18 mnths-3 yrs, 3-5 years');
INSERT INTO public.age VALUES (61, '18 months - 4 years');
INSERT INTO public.age VALUES (62, '18 months - 5 years');
INSERT INTO public.age VALUES (63, '18 months - 6 years');
INSERT INTO public.age VALUES (64, '18 months +');
INSERT INTO public.age VALUES (65, '2 - 3 years');
INSERT INTO public.age VALUES (66, '2 - 5 years');
INSERT INTO public.age VALUES (67, '2 - 6 years');
INSERT INTO public.age VALUES (68, '2 - 7 years');
INSERT INTO public.age VALUES (69, '2+ years');
INSERT INTO public.age VALUES (70, '3 - 10 years');
INSERT INTO public.age VALUES (71, '3 - 24 months');
INSERT INTO public.age VALUES (72, '3 - 5 years');
INSERT INTO public.age VALUES (73, '3 - 6 years');
INSERT INTO public.age VALUES (74, '3 - 7 years');
INSERT INTO public.age VALUES (75, '3 - 8 years');
INSERT INTO public.age VALUES (76, '3+ years');
INSERT INTO public.age VALUES (77, '3-5 years');
INSERT INTO public.age VALUES (78, '3-5 years, 5+ years');
INSERT INTO public.age VALUES (79, '3-6 months');
INSERT INTO public.age VALUES (80, '3-6 months, 6-9 months, 9');
INSERT INTO public.age VALUES (81, '4 - 5 years');
INSERT INTO public.age VALUES (82, '4 - 6 years');
INSERT INTO public.age VALUES (83, '4 - 7 years');
INSERT INTO public.age VALUES (84, '4 - 8 years');
INSERT INTO public.age VALUES (85, '4 months to walking');
INSERT INTO public.age VALUES (86, '4+ years');
INSERT INTO public.age VALUES (87, '5 - 7 years');
INSERT INTO public.age VALUES (88, '5 - 8 years');
INSERT INTO public.age VALUES (89, '5+ years');
INSERT INTO public.age VALUES (90, '6 - 10 years');
INSERT INTO public.age VALUES (91, '6 - 24 months');
INSERT INTO public.age VALUES (92, '6 - 36 months');
INSERT INTO public.age VALUES (93, '6 - 8 years');
INSERT INTO public.age VALUES (94, '6 months +');
INSERT INTO public.age VALUES (95, '6+ years');
INSERT INTO public.age VALUES (96, '6-9 months');
INSERT INTO public.age VALUES (97, '6-9 months, 12-18 months');
INSERT INTO public.age VALUES (98, '6-9 months, 9-12 months');
INSERT INTO public.age VALUES (99, '6-9 months, 9-12 months, ');
INSERT INTO public.age VALUES (100, '7+ years');
INSERT INTO public.age VALUES (101, '8 - 30 months');
INSERT INTO public.age VALUES (102, '8+ years');
INSERT INTO public.age VALUES (103, '9 - 36 months');
INSERT INTO public.age VALUES (104, '9-12 months');
INSERT INTO public.age VALUES (105, '9-12 months, 12-18 months');
INSERT INTO public.age VALUES (106, 'Exempt (babies)');
INSERT INTO public.age VALUES (107, 'G rating');
INSERT INTO public.age VALUES (108, 'Parents');
INSERT INTO public.age VALUES (109, 'PG rating');
INSERT INTO public.age VALUES (110, 'Infant');


INSERT INTO public.category VALUES (1, 'C', 'Games', 1);
INSERT INTO public.category VALUES (2, 'D', 'Dress Ups', 1);
INSERT INTO public.category VALUES (3, 'F', 'Fine Motor', 1);
INSERT INTO public.category VALUES (4, 'G', 'Gross Motor', 1);
INSERT INTO public.category VALUES (5, 'I', 'Imaginery Play', 1);
INSERT INTO public.category VALUES (6, 'P', 'Puzzles', 1);
INSERT INTO public.category VALUES (7, 'S', 'Sports and Outdoors', 1);
INSERT INTO public.category VALUES (9, 'Y', 'Young', 1);
INSERT INTO public.category VALUES (10, 'DVD', 'DVD''s', 1);
INSERT INTO public.category VALUES (11, 'M', 'Music', 1);
INSERT INTO public.category VALUES (12, 'RO', 'Ride-On', 1);
INSERT INTO public.category VALUES (13, 'DL', 'Doll', 1);
INSERT INTO public.category VALUES (14, 'A', 'Arts & Crafts', 1);
INSERT INTO public.category VALUES (15, 'JC', 'Jumping Castle Hire', 1);

INSERT INTO public.condition VALUES (1, 'Excellent');
INSERT INTO public.condition VALUES (3, 'Fair');
INSERT INTO public.condition VALUES (5, 'Very Poor');
INSERT INTO public.condition VALUES (2, 'Good');
INSERT INTO public.condition VALUES (4, 'Poor');

INSERT INTO public.discovery VALUES (10, 'Neighbour');
INSERT INTO public.discovery VALUES (13, 'Playgroup');
INSERT INTO public.discovery VALUES (28, 'Previous Member');
INSERT INTO public.discovery VALUES (41, 'Mothers Group');
INSERT INTO public.discovery VALUES (37, 'Member');
INSERT INTO public.discovery VALUES (39, 'Newspaper');
INSERT INTO public.discovery VALUES (40, 'Other');


INSERT INTO public.homepage VALUES (1, '<div class=\"logo\">
<p><img style=\"width: 100%; height: auto;\" src=\"https://scontent.fchc1-1.fna.fbcdn.net/v/t1.0-9/33618709_1794680290593877_3822180036245454848_n.png?_nc_cat=0&amp;oh=d59e2fa63a4c4b9fa7953ad5f4cb1b61&amp;oe=5BB90F7E\" alt=\"Good reason to join a Toy Library\" /></p>
</div>
<h2 style=\"color: #43935a;\">Welcome to the <strong>Librarian Test Toy Library</strong>!</h2>
<p>We offer over 500 toys for hire from as little as \$1 a fortnight. Guaranteed to delight, entertain and distract boys and girls of all ages from newborn up.</p>
<div style=\"width: 100%; float: none; height: auto; min-height: 330px;\">
<h3 style=\"color: #e75129;\">Party Pack</h3>
<p>Do you want to organise a party for your child but you don''t have table and chair? Don''t worry! We can provide everything that you need.</p>
<div style=\"width: 40%; margin-right: 10px; float: left;\"><img style=\"width: 100%; height: auto;\" src=\"https://northotago.mibase.com.au/toy_images/northotago/81298.jpg\" alt=\"Party Pack\" /></div>
<div style=\"width: 57%; float: left;\">
<h4 style=\"margin-top: 0;\">Party Pack - Option 1</h4>
<p>Up to 5 toys for 1 week and ONLY <strong>\$5</strong>.</p>
<h4>Party Pack - Option 2</h4>
<p>1 Tables, 7 Chairs and up to 5 toys for 1 week and ONLY <strong>\$10</strong>.</p>
<h4>Party Pack - Option 3</h4>
<p>2 Tables, 14 Chairs and up to 5 toys for 1 week and ONLY <strong>\$12</strong>.</p>
</div>
<p>ll Party Packs can be reserved.</p>
</div>
<p style=\"font-size: large;\">This home page can be changed to suit, the content is edited in Home - Edit Website. There are 2 columns, the right hand column is usually used for Open times, links to website and facebook page, and location, there is also a link to the terms and conditions of membership at the bottom of the left hand column.</p>
<p style=\"font-size: large;\">Images can be set to hide in mobile phone view, with div tag class=\"logo\"</p>
<p style=\"font-size: large;\">The menu bar color and font can be changed in Home - settings.</p>
<div class=\"logo\">&nbsp;</div>', '<h3>LIBRARY HOURS</h3>
<p><strong>Monday&gt;</strong> 4:30 pm - 6:00 pm<br /><strong>Saturday&gt;</strong> 10:30 am - 12:30 pm</p>
<h3>LOCATION</h3>
<address>301 Tuam Street, Christchurch 8015</address>
<p><iframe style=\"border: 0;\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2892.434919011204!2d172.64536791549395!3d-43.534975179125524!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d318a201ae471e7%3A0x52f4f8d6aabd2142!2s301+Tuam+St%2C+Christchurch+Central%2C+Christchurch+8011%2C+New+Zealand!5e0!3m2!1sen!2sau!4v1535075676369\" width=\"277\" height=\"208\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe></p>
<h3>CONTACT DETAILS</h3>
<p style=\"margin: 0;\">Phone/Text: <a title=\"Click to call\" href=\"tel:+61481043040\">0481 043 040</a></p>
<ul style=\"margin: 0; padding: 0;\">
<li style=\"display: inline-block; margin: 0; margin-right: 10px;\"><a style=\"font-size: 40px; color: #3b5998;\" title=\"NOTL on Facebook\" href=\"https://www.facebook.com/mibasetoylibrarysoftwarenz\" target=\"_blank\" data-toggle=\"tooltip\" data-original-title=\"Facebook\"> <span class=\"fab fa-facebook-square\">&nbsp;</span> </a></li>
<li style=\"display: inline-block; margin: 0; margin-right: 10px;\"><a style=\"font-size: 40px; color: #ec368f;\" title=\"Send us an email\" href=\"mailto:michelle@mibase.com.au\" target=\"_blank\"> <span class=\"fas fa-envelope\">&nbsp;</span> </a></li>
</ul>
<h3>FOLLOW US</h3>
<p><iframe style=\"border: none; overflow: hidden;\" src=\"https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2mibasetoylibrarysoftwarenz&amp;tabs=timeline&amp;width=280&amp;height=600px&amp;small_header=true&amp;adapt_container_width=true&amp;hide_cover=true&amp;show_facepile=true&amp;appId=655058854690170&amp;data-tabs=timeline,events,messages width=\" width=\"280\" height=\"600px\" frameborder=\"0\" scrolling=\"no\"></iframe></p>', '2018-09-01', '<h2 style=\"color: #43935a;\">Frequently Asked Questions (FAQ)</h2>
<h4>Who runs the library?</h4>
<p>We are a non-profit organization run by a voluntary committee with support from our members and their families. We are always in need of new committee members. From each new committee member, we gain new ideas and a fresh look into what can be improved. We are not a council or government organisation or a private company. We are a group of parents working together to benefit our children. The library is run by the members and a volunteer committee, who looks after the day-to-day running of the library. The library also one employ paid librarian.</p>
<hr />
<h4>What happens if we lose a piece to a toy?</h4>
<p>If you lose a piece and you are not able to find the piece, you will be charged a replacement fee generally \$5.00. If a missing piece causes the toy to be incomplete, you are welcome to buy the toy (and we hope you find the piece at home), or pay to replace it.</p>
<hr />
<h4>What happens if my child breaks a toy?</h4>
<p>You will charge the replacement cost. Every toy has a different cost.</p>
<hr />
<h4>How many toys may I hire?</h4>
<p>You can hire up to 5 toys out at a time. The maximum checkout for any set of toys is 42 days.</p>
<hr />
<h4>Can my children stay and play in The Toy Library?</h4>
<p>Yes! Your children are welcome to &ldquo;stay and play&rdquo; in the play area when you come to check out toys.</p>', NULL, '<p><!
<p><!
<p class=\"MsoNormal\">Please see the membership page on our website:</p>
<p class=\"MsoNormal\">http://scarboroughtoylibrary.weebly.com/membership.html</p>', '', '', '', '', '', '<p><strong>Next toy library committee meeting: </strong>Late August, date tba, 2pm at the Elwood St Kilda Neighbourhood Learning Centre (which is where the toy library is).</p>
<p><strong>Did you know</strong> that the toy library has a very nice cargo bike available for you to borrow? It is big, red and seats four kids (or two kids and a load of shopping) and is very fun to ride. Two weeks'' hire costs \$10 - you can book by clicking <a href=\"https://elwood.mibase.com.au/members/toys/index.php?v=6\">Hire Toys</a></p>
<p><strong>Roster etiquette: </strong>If you can''t make your roster shift, do take your name off the list, but please also find a future shift for yourself.</p>', '', NULL, '', '');


INSERT INTO public.manufacturer VALUES (1, 'Donated', NULL, NULL, NULL, NULL, NULL, 0.0);

INSERT INTO public.membertype VALUES (20, 'GENERAL MEMBER', 10, 'Full Membership', 55.0, 4.00, 4, 0.0, 0.0, 0, 12, NULL, 'No', NULL, NULL, NULL, NULL, 0);
INSERT INTO public.membertype VALUES (27, 'QUT Tester', 10, 'Testiing the members App', 100.0, 4.00, NULL, 0.0, NULL, 0, 12, NULL, 'No', NULL, NULL, NULL, NULL, 0);
INSERT INTO public.membertype VALUES (1, 'COMMITTEE', 10, 'Committee Membership', 25.0, 0.00, 4, 0.0, 0.0, 0, 6, NULL, 'Yes', NULL, NULL, NULL, NULL, 0);
INSERT INTO public.membertype VALUES (25, 'Full Term Membership', 5, '3 months', 19.0, 0.00, NULL, 0.0, NULL, 14, 3, NULL, '', NULL, NULL, NULL, NULL, 0);
INSERT INTO public.membertype VALUES (28, 'QUT admin', 4, 'QUT admin', 0.0, 0.00, NULL, NULL, NULL, 0, 12, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO public.membertype VALUES (26, 'Bouncy Castle Hire', 4, 'Bouncy Castle Hire', 0.0, 0.00, NULL, 0.0, NULL, 0, 12, NULL, 'Yes', NULL, NULL, NULL, NULL, 0);

INSERT INTO public.nationality VALUES (2, 'Australian', NULL);
INSERT INTO public.nationality VALUES (3, 'Dutch', NULL);
INSERT INTO public.nationality VALUES (4, 'German', NULL);

INSERT INTO public.notifications VALUES (1, 'email     ', 'birthday', 0, NULL, NULL, NULL, NULL, NULL, 'Yes');
INSERT INTO public.notifications VALUES (2, 'email     ', 'roster', 10, 'Everyday', NULL, NULL, NULL, NULL, 'Yes');

INSERT INTO public.overdue VALUES (1, 'Yes', 'No', 0.00, 0, 'No', 1.00, 0.00, 'No', NULL, 0, 0.00, NULL);

INSERT INTO public.paymentoptions VALUES (17, 'Missed Roster Duty Fine', 'DR', '0', 10.0, 'Fine', 0, NULL, NULL, NULL);
INSERT INTO public.paymentoptions VALUES (18, 'Missing Part Refund', NULL, NULL, 0.0, 'Cash', NULL, NULL, NULL, NULL);
INSERT INTO public.paymentoptions VALUES (20, 'Missing Bag Fine', NULL, NULL, 5.0, 'Cash', NULL, NULL, NULL, NULL);
INSERT INTO public.paymentoptions VALUES (22, 'Membership Fees', NULL, NULL, 50.0, 'Cash', NULL, NULL, NULL, NULL);
INSERT INTO public.paymentoptions VALUES (23, 'Missing Part', NULL, NULL, 2.0, 'Cash', NULL, NULL, NULL, NULL);

INSERT INTO public.reports VALUES (12, 'BagLabel', 'baglabel', NULL, NULL);
INSERT INTO public.reports VALUES (13, 'Member Cards', 'member', NULL, NULL);
INSERT INTO public.reports VALUES (14, 'Current Members by Member Type', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (15, 'All Toys', 'excel', 'select toys.idcat as idcat, toyname, category, cost, discountcost, toy_status,t.due

from toys 
left join 
(select * from transaction where return is null) t on t.idcat = toys.idcat 
order by toys.id', 'all_toys');
INSERT INTO public.reports VALUES (16, 'Fonts', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (1, 'BagLabel 4 per page', 'baglabel', NULL, NULL);
INSERT INTO public.reports VALUES (2, 'BagLabel with warnings full width', 'baglabel', NULL, NULL);
INSERT INTO public.reports VALUES (3, 'BagLabel with warnings', 'baglabel', NULL, NULL);
INSERT INTO public.reports VALUES (4, 'Toy Barcodes all toys', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (5, 'Current Members', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (6, 'Master Stocktake List - short', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (7, 'Mibase version 4 Daily Transactions', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (8, 'Buyers Report - Useage Statistics', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (9, 'Roster for Members - 3 Column', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (10, 'Unpopular Toys', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (11, 'BagLabel with picture', 'baglabel', NULL, NULL);


INSERT INTO public.rostertypes VALUES (6, 'Roster Coord', '9.00-10.45am', 1, 1, 'Tuesday', '', 'Yes');
INSERT INTO public.rostertypes VALUES (5, 'Roster Coord', '9.00-11.30am', 1, 1, 'Saturday', '', 'Yes');
INSERT INTO public.rostertypes VALUES (3, 'Roster', '9.15 - 11.30am', 1, 2, 'Saturday', '', 'Yes');
INSERT INTO public.rostertypes VALUES (4, 'Roster', '9.15-10.45am', 1, 2, 'Tuesday', 'AH', 'Yes');

INSERT INTO public.settings VALUES ('inlibrary', 'Toy Library', 24, ' Library status for the Toy to the Library location', NULL);
INSERT INTO public.settings VALUES ('tuesday', '0', 6, 'Is the Toy Library open on Tuesday? - for fine calculations', NULL);
INSERT INTO public.settings VALUES ('monday', '0', 5, 'Is the Toy Library open on Monday - for fine calculations', NULL);
INSERT INTO public.settings VALUES ('sunday', '0', 11, 'Is the Toy Library open on Sunday - for fine calculations', NULL);
INSERT INTO public.settings VALUES ('reserve_from', '', 22, 'Fixed date to reserve toys, leave blank if default to today.', NULL);
INSERT INTO public.settings VALUES ('user_borwrs', 'Moved from:', 28, 'Changes the User defined field label in Members', NULL);
INSERT INTO public.settings VALUES ('idcat_noedit', 'Yes', 29, 'If migrating from Libraratu don''t change the toy number', NULL);
INSERT INTO public.settings VALUES ('export_toys', 'id, category, idcat, toyname , alert, date_purchase, manufacturer, storage ', 40, 'Columns to export when exporting toys, seperated by a comma.', 'textarea');
INSERT INTO public.settings VALUES ('fine_value', '1', 20, 'Weekly or session fine amount', NULL);
INSERT INTO public.settings VALUES ('new_toys', 'Yes', 41, 'Display new toys on index page', NULL);
INSERT INTO public.settings VALUES ('password', 'mibase', 16, NULL, NULL);
INSERT INTO public.settings VALUES ('days_overdue', '5', 43, 'Email Reminder is sent > 0 days overdue', NULL);
INSERT INTO public.settings VALUES ('member_alerts', 'Yes', 48, 'Activate member alerts for emails', NULL);
INSERT INTO public.settings VALUES ('checked', 'Yes', 50, 'Add checked button to loans', NULL);
INSERT INTO public.settings VALUES ('open_hours', 'OPENING HOURS

Tuesday: 9:30 am - 12:00 pm
Thursday: 9:30 am - 12:00 pm and
Thursday: 6.30 to 8.30pm
Saturday: 9:30 am - 12:00 pm', 31, 'Toy Library Open Hours, to print on bag label', 'textarea');
INSERT INTO public.settings VALUES ('email_from', 'info@mibase.com.au', 42, 'Email reply address', NULL);
INSERT INTO public.settings VALUES ('reserve_fee', '2.50', 27, 'Does your Library charge a fee for Toy Reservations, 0 = No', NULL);
INSERT INTO public.settings VALUES ('toy_reserve', 'Yes', 13, 'Do you use Toy Reservations', NULL);
INSERT INTO public.settings VALUES ('new_member_email', 'Yes', 51, 'Sends email when a member joins', NULL);
INSERT INTO public.settings VALUES ('renew_button', 'Yes', 130, 'Include Renew Button in Edit member mode', NULL);
INSERT INTO public.settings VALUES ('show_phone', 'No', 26, 'Show the member mobile phone on Toy search screen', NULL);
INSERT INTO public.settings VALUES ('catsort', 'Yes', 1, 'Does your Toy Library use the category in the Toy Number?', NULL);
INSERT INTO public.settings VALUES ('loanperiod', '21', 21, 'Toy Loan Period, also Reservation period in days.', NULL);
INSERT INTO public.settings VALUES ('simple_new_member', 'Yes', 120, 'Simple new member form', NULL);
INSERT INTO public.settings VALUES ('mem_reserves', 'Yes', 47, 'Allow Members to do their own reservations', NULL);
INSERT INTO public.settings VALUES ('timezone', 'Pacific', 25, 'time zone', NULL);
INSERT INTO public.settings VALUES ('holiday_due', '', 145, 'Setting to set due date for all the toys when the toy library opens after the holidays', NULL);
INSERT INTO public.settings VALUES ('times_renew', '1', 33, 'Number of times member is allowed to renew a toy', NULL);
INSERT INTO public.settings VALUES ('multi_location', 'No', 150, 'Does this Toy Library have another branch?', NULL);
INSERT INTO public.settings VALUES ('automatic_debit_off', 'No', 52, NULL, NULL);
INSERT INTO public.settings VALUES ('format_toyid', 'No', 3, 'Numbering Toys with leading zeros, e.g. A001', NULL);
INSERT INTO public.settings VALUES ('recordfine', 'Yes', 4, 'Do you want mibase to record overdue fines', NULL);
INSERT INTO public.settings VALUES ('mem_times_renew', '5', 44, 'No of times member can renew', NULL);
INSERT INTO public.settings VALUES ('chargerent', 'Yes', 12, 'Charge rent for toy hire', NULL);
INSERT INTO public.settings VALUES ('rentasfine', 'Yes', 14, 'Do you use the Rent value as the fine amount if the Toy is Overdue', NULL);
INSERT INTO public.settings VALUES ('weekly_fine', 'Yes', 17, 'Fine per week or per session?', NULL);
INSERT INTO public.settings VALUES ('overdue', 'OVERDUE', 32, 'Description in list if a Toy is Overdue, e.g. OVERDUE or AVAILABLE', NULL);
INSERT INTO public.settings VALUES ('reservations', 'Yes', 45, 'Activate reservations', NULL);
INSERT INTO public.settings VALUES ('overdue_fine', 'Yes', 116, NULL, NULL);
INSERT INTO public.settings VALUES ('rent', '0.5', 15, 'Rent amount for all Toys', NULL);
INSERT INTO public.settings VALUES ('days_grace', '5', 34, 'Days Grace', NULL);
INSERT INTO public.settings VALUES ('online_hire', 'No', 140, 'Allow the Jumping castels to be hired by non-members', NULL);
INSERT INTO public.settings VALUES ('saturday', '0', 10, 'Is the Toy Library open on Saturday - for fine calculations', NULL);
INSERT INTO public.settings VALUES ('wednesday', '0', 7, 'Is the Toy Library open on Wednesday - for fine calculations', NULL);
INSERT INTO public.settings VALUES ('thursday', '1', 8, 'Is the Toy Library open on Thursday - for fine calculations', NULL);
INSERT INTO public.settings VALUES ('friday', '1', 9, 'Is the Toy Library open on Friday - for fine calculations', NULL);
INSERT INTO public.settings VALUES ('partypack', 'No', 18, 'Do you have a party pack for hire?', NULL);
INSERT INTO public.settings VALUES ('address', 'Namoi Toy Library
51 Tibbereena Street 
Narrabri NSW
ph: 0267923562', 19, 'Toy Library Address for Bag Label and reports', 'textarea');
INSERT INTO public.settings VALUES ('loan_receipt', 'Yes', 30, 'Comments to print on the loan receipt', 'textarea');
INSERT INTO public.settings VALUES ('receipt_email', 'Yes', 150, 'Add Email Receipt functionality to loans, Set to Yes to switch on email loan receipts', NULL);
INSERT INTO public.settings VALUES ('receipt_printer', 'Yes', 100, 'Enables direct printing to receipt printer', NULL);
INSERT INTO public.settings VALUES ('reuse_toyno', 'No', 115, 'Select Lowest toy number as default for new Toy Number.', NULL);
INSERT INTO public.settings VALUES ('online_renew', 'Yes', 117, 'Allow Members to check their details by email', NULL);
INSERT INTO public.settings VALUES ('jc_reserve_period', '5', 118, 'Reservation period for the Bouncy Castle', NULL);
INSERT INTO public.settings VALUES ('hide_value', 'Yes', 200, 'Hides the total value of toys loaned by this member in loans page', NULL);
INSERT INTO public.settings VALUES ('disable_cc_email', 'Yes', 301, 'Do not send CC email to Library for loan receipts.', NULL);
INSERT INTO public.settings VALUES ('auto_approve_roster', 'No', 302, 'Auto approve Roster duties from member login.', NULL);
INSERT INTO public.settings VALUES ('online_roster_pref', 'No', 304, 'Add Roster Preferences to Online signup form.', NULL);
INSERT INTO public.settings VALUES ('member_update', 'Yes', 306, 'Allow email membership updates.', NULL);
INSERT INTO public.settings VALUES ('exclude_mem_lote', 'No', 308, 'Exclude Member Nationality from online signup form', NULL);
INSERT INTO public.settings VALUES ('disable_auto_report', 'No', 309, 'Dont send Summary reports for overdue and due toys reports.', NULL);
INSERT INTO public.settings VALUES ('mobile_login_off', 'No', 310, 'Turn off Mobile Login on website menu.', NULL);
INSERT INTO public.settings VALUES ('hiretoys_label', 'Hire Toys', 311, 'Customize Hire toys label and link.', NULL);
INSERT INTO public.settings VALUES ('email_second_contact', 'No', 312, 'Emails second contact for all email reminders and bulk emailing.', NULL);
INSERT INTO public.settings VALUES ('online_wwc', 'No', 313, 'Add Working with children card number to Online signup form', NULL);
INSERT INTO public.settings VALUES ('online_id', 'No', 314, 'Add licence or concession card id to Online signup form', NULL);
INSERT INTO public.settings VALUES ('state', 'VIC', 316, 'Default State in Australia.', NULL);
INSERT INTO public.settings VALUES ('suburb_title', 'Suburb: ', 317, 'Default Suburb Title.', NULL);
INSERT INTO public.settings VALUES ('online_children', 'No', 319, 'Add Children to Online signup form.', NULL);
INSERT INTO public.settings VALUES ('reserve_period', '8', 23, 'Reserve Period - No days the toy is reserved', NULL);
INSERT INTO public.settings VALUES ('libraryname', 'Librarian Test Toy Library', 2, 'Toy Library Name', NULL);
INSERT INTO public.settings VALUES ('online_second_contact', 'No', 318, 'Add Second Contact to Online signup form.', NULL);
INSERT INTO public.settings VALUES ('online_help_off', 'Yes', 321, 'Exclude How can I help from Online signup form.', NULL);
INSERT INTO public.settings VALUES ('mem_levy', '0', 322, 'Charge Members a levy.', NULL);
INSERT INTO public.settings VALUES ('loan_restrictions', 'No', 324, 'Apply Loan restrictions.', NULL);
INSERT INTO public.settings VALUES ('hiretoys_label_button', 'Hire Toy', 325, 'Label for Hire Toy Button in Member login.', NULL);
INSERT INTO public.settings VALUES ('free_rent_btn', 'No', 326, 'Switch off rent charge in loans.', NULL);
INSERT INTO public.settings VALUES ('vol_missing_off', 'No', 327, 'Switch off missing parts in Loans for volunteer login.', NULL);
INSERT INTO public.settings VALUES ('mem_roster_show_name', 'No', 329, 'Show members name on roster.', NULL);
INSERT INTO public.settings VALUES ('mem_private', 'Yes', 330, 'Show members name on roster.', NULL);
INSERT INTO public.settings VALUES ('online_address', 'Yes', 331, 'Show members address on signup form.', NULL);
INSERT INTO public.settings VALUES ('online_findus', 'Yes', 332, 'Show Find Us on signup form.', NULL);
INSERT INTO public.settings VALUES ('online_membertype', 'Yes', 333, 'Show Member Category on signup form.', NULL);
INSERT INTO public.settings VALUES ('renew_loan_period', '0', 334, 'Renew period if differnet from loan period, leave as 0 if not.', NULL);
INSERT INTO public.settings VALUES ('online_rego', 'Yes', 303, 'Allow online registrations through website.', NULL);
INSERT INTO public.settings VALUES ('online_conduct', 'Yes', 335, 'Include Code of Conduct in member signup form.', NULL);
INSERT INTO public.settings VALUES ('show_desc', 'No', 336, 'Show Piece Description in Loans Page.', NULL);
INSERT INTO public.settings VALUES ('mem_self_checkout', 'No', 337, 'Allow members to loan and return toys.', NULL);
INSERT INTO public.settings VALUES ('online_gift', 'No', 340, 'Online Membership Gift Cards.', NULL);
INSERT INTO public.settings VALUES ('mem_loanperiod', '0', 341, 'Member login renew loan period.', NULL);
INSERT INTO public.settings VALUES ('holiday_override', 'No', 342, 'Override loan period when the toy library is closed over the holdiays.', NULL);
INSERT INTO public.settings VALUES ('roster', 'Yes', 343, 'Include Roster system.', NULL);
INSERT INTO public.settings VALUES ('online_helmet_waiver', 'No', 344, 'Include Helmet waiver in online signup form.', NULL);
INSERT INTO public.settings VALUES ('menu_font_color', 'yellow', 346, 'Menu color for home page.', NULL);
INSERT INTO public.settings VALUES ('toy_hold_fee', '5', 345, 'Value as an integer for fee to charge member for adding a hold to a toy.', NULL);
INSERT INTO public.settings VALUES ('mem_toy_holds_off', 'No', 348, 'Disable Holds in member login.', NULL);
INSERT INTO public.settings VALUES ('gtmcode', 'No', 349, 'Google Code', NULL);
INSERT INTO public.settings VALUES ('system_alert', 'No', 350, 'Enable System alerts for communication, appear in home page.', NULL);
INSERT INTO public.settings VALUES ('menu_faq', 'Yes', 352, 'Enable Menu item faq on website.', NULL);
INSERT INTO public.settings VALUES ('menu_member_options', 'Yes', 351, 'Enable Menu item menu options on website.', NULL);
INSERT INTO public.settings VALUES ('sponsor', 'No', 353, 'Enable Menu item sponsor on website.', NULL);
INSERT INTO public.settings VALUES ('library_heading_off', 'No', 355, 'Home page default Toy Library name switched off.', NULL);
INSERT INTO public.settings VALUES ('rounddown_week', 'No', 357, 'days overdue is rounded down to the nearest week.', NULL);
INSERT INTO public.settings VALUES ('roundup_week', 'No', 356, 'Fine is charged if OVER a week late.', NULL);
INSERT INTO public.settings VALUES ('menu_color', 'darkblue', 347, 'Menu color for home page.', NULL);
INSERT INTO public.settings VALUES ('toy_holds', 'Yes', 338, 'Allow members to place toys on hold.', NULL);
INSERT INTO public.settings VALUES ('mem_edit', 'No', 358, 'Allow member to edit their details online.', NULL);
INSERT INTO public.settings VALUES ('toy_hold_period', '8', 339, 'Number of days to pick up toys on hold.', NULL);
INSERT INTO public.settings VALUES ('upgrade', 'Yes', 354, 'Update to access the New returns and Loans Pages.', NULL);

INSERT INTO public.storage VALUES (1, 'Box and Lid');
INSERT INTO public.storage VALUES (3, 'Raeco Bag');
INSERT INTO public.storage VALUES (2, 'Zippit Bag');

INSERT INTO public.stype VALUES (1, 'JC', 'Jumping Castle Hire', 0.00, 0.00);

INSERT INTO public.sub_category VALUES (1, 'A1', 'Active Toys for small babies', NULL);


INSERT INTO public.template VALUES (5, '17:32:28.897799+11', 'Welcome to the xxx Toy Library!', '<p>Wahoo! Thank you so much for joining the Toy Library!</p>
<p>&nbsp;</p>
<p>We are so excited to have you on board and really look forward to seeing you at the Toy Library.</p>
<p>&nbsp;</p>
<p>If you have any questions, feedback or concerns please do not hesitate to contact us.</p>
<p>&nbsp;</p>', 'new_member', NULL, 'BULK_EMAIL');
INSERT INTO public.template VALUES (20, '13:16:50.125469+10', 'Username and Password for the [libraryname]', '<p>Dear [firstname],</p>
<p>Thank you for participating in the development of the mibase members app, I am sure your members will appreciate your efforts when it becomes available to them!<br /><br />There may be errors, this is the first beta version, so please be patient and provide feedback in the feedback tab in the app. When selecting a Library when you first open the app, please \"Librarians Test Library\".<br /><br />Here is the landing page for the QUT Test Library. <a href=\"https://demo.mibase.com.au/home/index.php\" target=\"_blank\">Home Page</a><br />This database is a test site, it will not edit your Toy Library data.<br /><br />To Login online through the test database, go to <a class=\"button_menu_member\" href=\"https://demo.mibase.com.au/home/login.php\">Member Login</a> with the following username and password.</p>
<p>Username: <span style=\"color: blue;\">[username]</span><br />Password: <span style=\"color: blue;\">[pwd]</span><br /><br />The above username and password is used to login to the App.</p>
<p>Please <a href=\"https://demo.mibase.com.au/admin/login.php\">login to admin</a> and loan/reserve/hold a few toys.</p>
<p>username: qut<br />pwd: qut123</p>
<p><br />The App can be downloaded from Google Play <a href=\"https://play.google.com/apps/testing/com.mibase\">here</a></p>
<p>download from here https://play.google.com/apps/testing/com.Mibase<br /><br />Have fun!<br /><br /><br />Michelle Baird and the QUT team</p>
<p><br /><img src=\"https://demo.mibase.com.au/toy_images/demo/news/1.jpg\" alt=\"logo\" width=\"145\" height=\"98\" /></p>
<p>&nbsp;</p>
<p><span style=\"color: red;\">This is an automated message, please do not reply</span><br /><br /><br /></p>', 'login', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (3, '15:13:54.826532+10', 'ROSTER ALERT', '<p><span style=\"font-size: x-small; font-family: Arial; color: navy;\"><span style=\"font-size: 10pt; font-family: Arial; color: navy;\">Dear member, </span></span></p>
<p><span style=\"font-size: x-small; font-family: Arial; color: navy;\"><span style=\"font-size: 10pt; font-family: Arial; color: navy;\">Another 3 months of your rostered membership, we hope that you and your kid(s) are enjoying it! Please check if you have put down your name for one of your required duties. As a duty member, you are required to do one 2 hour duty roughly every 3 months of your membership. You can check and / or roster your duties once you have logged in under the following link <a title=\"Member Login\" href=\"../../members/login.php\" target=\"_blank\">Member Login</a></span></span></p>
<p class=\"MsoNormal\"><span style=\"font-size: x-small; font-family: Arial; color: navy;\"><span style=\"font-size: 10pt; font-family: Arial; color: navy;\">Your help is vey much appreciated and  counted on! If you have any trouble booking your duty, please get in touch.</span></span></p>
<p class=\"MsoNormal\"><span style=\"font-size: x-small; font-family: Arial; color: navy;\"><span style=\"font-size: 10pt; font-family: Arial; color: navy;\">Thanks a lot. Jana</span></span></p>', 'roster_alert', NULL, 'AUTO');
INSERT INTO public.template VALUES (6, '06:30:03.908088+11', 'Membership Reminder: Membership is Due to Expire', '<p>&nbsp;</p>
<p>We Love having you onboard as a member of the xxx Toy Library and we really hope that you are able to renew your membership.</p>
<p>&nbsp;</p>
<p>If you have any concerns or for some reason unable to renew at this time, please do talk to us.</p>
<p>&nbsp;</p>
<p>Thanks very much,</p>
<p>&nbsp;</p>', 'due_to_expire', NULL, 'AUTO');
INSERT INTO public.template VALUES (2, '20:05:13.071686+10', 'ROSTER REMINDER', '<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Dear member,</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">You are rostered on for duty in one weeks time - see details below.</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">[rosterlist]</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Thank you ,your help is much needed and appreciated!</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Kind regards.</span></p>
<p><span style=\"font-size: 11.0pt;\">Michelle and the Training Library team.</span></p>', 'roster', 'michelle@marineleisure.com.au', 'AUTO');
INSERT INTO public.template VALUES (7, '14:35:34.556565+11', 'Membership Expired: Your Membership has Expired', '<p>Oh no! Your membership with the xxxxx has expired! :(</p>
<p>To renew your membership please come in and talk to one of the Librarians and we`ll have you renewed in no time!</p>
<p>&nbsp;</p>
<p>Thank you for being a member of the xxx<br />We appreciate your on going support and look forward to seeing you soon.</p>
<p>&nbsp;</p>
<p>Thanks very much,<br />The xx Toy Library team.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>', 'expired', NULL, 'AUTO');
INSERT INTO public.template VALUES (21, '08:44:32.437941+10', 'Birthday Wishes from [libraryname]', 'Dear [childname],<br><br>Put your Happy Birthday message here...<br><br>', 'birthday', NULL, 'AUTO');
INSERT INTO public.template VALUES (1, '20:05:08.285351+10', 'OVERDUE TOYS  REMINDER', '<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Dear Member, </span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">We have noticed that your toys are well overdue now (details below) and would really appreciate a prompt return to allow other Toy Library members to use your toys! If there is anything that makes you unable to come, please get in touch by phone, email or facebook.</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">[toylist]</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Thanks a lot.</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Jana</span></p>
<p><span style=\"font-size: 11.0pt;\">Taranaki Toy Library</span></p>', 'overdue', '', 'AUTO');
INSERT INTO public.template VALUES (9, '21:28:30.571264+10', 'Email Receipt from Mibase Toy Library', '<p>&nbsp;</p>
<table style=\"height: 135px;\" width=\"470\">
<tbody>
<tr>
<td>&nbsp;<img src=\"https://demo.mibase.com.au/toy_images/demo/news/5.jpg\" alt=\"logo\" width=\"200\" height=\"89\" /></td>
<td>
<h2>TRAINING TOY LIBRARY</h2>
<p><strong>Opening hours</strong></p>
<p>Thursday 10.00 - 11.00am<br />Thursday 6.30 - 7.30pm<br />Saturday 10.00 - 11.30am</p>
</td>
</tr>
</tbody>
</table>
<h2>LOAN RECEIPT</h2>
<p><strong>[longname]</strong></p>
<p>[next_duty]</p>
<p><strong>Duties Completed:</strong> [completed] of [duties]</p>
<p><strong>Expires:</strong> [expired]</p>
<p><span style=\"color: #ff0000;\"><strong>Please remember to count your toys before allowing children to play with them . Please advise the librarian of any missing/broken parts within 24 hours or you will be held responsible for missing broken parts.</strong></span></p>
<p>[toyslist_pieces]</p>', 'email_receipt', '', 'BUTTON');
INSERT INTO public.template VALUES (120, '10:00:15.057985+10', 'Missing Parts Today.', 'Dear [longname]

Losing pieces is a normal part of Toy Library borrowing, and not something we want our members to be unduly worried about as we have systems to manage this process.  This email serves as a reminder for you to keep an eye out at home for the following pieces
[missing]
The Toy Library refunds \$xx for each piece returned, and of course we appreciate your assistance in keeping the pieces of our toys, puzzles and games together.  Please chat to one of our friendly staff team members if you have any concerns.
Regards', 'missing', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (121, '15:54:55.442505+10', 'Reservation Reminder.', '[reserve_list]', 'reserve', '', 'PROCESS');
INSERT INTO public.template VALUES (30, '15:15:01.440883+11', 'Loan receipt for receipt printer', '<p>&nbsp;</p>
<table style=\"height: 135px;\" width=\"470\">
<tbody>
<tr>
<td>&nbsp;<img src=\"https://demo.mibase.com.au/toy_images/demo/news/5.jpg\" alt=\"logo\" width=\"200\" height=\"89\" /></td>
<td>
<h2>TRAINING TOY LIBRARY</h2>
<p><strong>Opening hours</strong></p>
<p>Thursday 10.00 - 11.00am<br />Thursday 6.30 - 7.30pm<br />Saturday 10.00 - 11.30am</p>
</td>
</tr>
</tbody>
</table>
<h2>LOAN RECEIPT</h2>
<p><strong>[longname]</strong></p>
<p>[next_duty]</p>
<p><strong>Duties Completed:</strong> [completed] of [duties]</p>
<p><strong>Expires:</strong> [expired]</p>
<p><span style=\"color: #ff0000;\"><strong>Please remember to count your toys before allowing children to play with them . Please advise the librarian of any missing/broken parts within 24 hours or you will be held responsible for missing broken parts.</strong></span></p>
<p>[toyslist_pieces]</p>', 'loan_receipt', '', 'BUTTON');
INSERT INTO public.template VALUES (113, '20:28:55.031601+11', 'Gift Card Application Received', '<p>Dear [p_name],</p>
<p>Thank you for your Gift Card application with the [libraryname].</p>
<p>Your reference number for this gift card is: [id].</p>
<p>One of our committee will be in touch ASAP to finalise payment.</p>
<p>&nbsp;</p>
<p>regards,</p>
<p>&nbsp;</p>
<p>[libraryname]</p>', 'approve_gift', '', 'PROCESS');
INSERT INTO public.template VALUES (44, '19:41:56.204255+10', 'Membership Application Received', '<p>Dear [firstname],</p>
<p>Thank you for your membership application with the [libraryname].</p>
<p>We will be in touch shorly.</p>
<p>[payment_info]</p>
<p>regards,</p>
<p>&nbsp;</p>
<p>[libraryname]</p>', 'approve', NULL, 'PROCESS');
INSERT INTO public.template VALUES (45, '17:42:33.5719+10', 'It`s only one click to agree to our bike helmet waiver - please action!', '<p>All Toy Library members are asked to please read and click on the link at bottom of this email to agree to the&nbsp;waiver below. &nbsp;It is an important requirement from our insurers. &nbsp;Feel free to ask me any questions you may have. &nbsp;(Yes you still need to sign even if you have a baby who isn`t riding bikes and scooters yet!)</p>
<p>Thanks Jane</p>
<p><strong>MIBASE TOY LIBRARY - WAIVER, RELEASE AND INDEMNITY FOR BICYCLE AND SCOOTERS</strong></p>
<p>&nbsp;</p>
<p>The MiBase Toy Library (\"Provider\") is pleased to make bicycles and scooters available to subscribers (\"the Service\").&nbsp;</p>
<p>As a subscriber to the Service you acknowledge that there are dangers and risks inherent with bicycle and scooter riding (the &ldquo;Activity&rdquo;) to which any child under your supervision (\"Your Child\") may be exposed.</p>
<p>&nbsp;</p>
<p>The Provider does not provide protective clothing or bicycle helmets and it is your responsibility to ensure that Your Child wears a helmet at all times and is otherwise appropriately attired when participating in the Activity. You agree that Your Child will participate in the Activity at your own risk. You also agree to voluntarily assume responsibility for supervising the Activity and any injury, death or property damage you or Your Child may suffer or cause as a result of participating in the Activity.</p>
<p>To the maximum extent possible at law, you (both in your personal capacity and on behalf of Your Child) agree to release, hold harmless and indemnify the Provider and its respective officers, employees, servants, agents and contractors (the &ldquo;Indemnified Persons&rdquo;) against all actions, claims, suits, costs, expenses, demands and damages suffered or incurred by the Indemnified Persons or any one or more of them by reason of, or in respect of, or in any manner whatsoever arising out of, or caused by, your use of the Service or Your Child`s participation in the Activity.</p>
<p>&nbsp;You agree that you are subscribing to the Service on the express condition that the Provider:</p>
<p>(a) will, under no circumstances be liable or responsible in any manner whatsoever for any death, loss, accident, damage or injury to you, Your Child or any of your servants, agents, contractors, visitors or invitees or any other person whatsoever (&ldquo;Related Party&rdquo;) which may happen as a result of your use of the Service or Your Child participating in the Activity; and</p>
<p>(b) will not incur or be under any liability whatsoever to you, Your Child, or to any Related Party for any loss, damage or injury to or in respect of any of your property or of any Related Party&rsquo;s property.</p>
<p>The Provider is not liable to you, Your Child or any Related Party in respect of any indirect or consequential loss. For the avoidance of doubt, &lsquo;consequential loss&rsquo; means loss or damage arising from a breach of contract, tort (including negligence), under statute or any other basis in law or equity of an indirect or consequential nature including, but without limitation, loss of profits, loss of revenue, loss or denial of opportunity, loss of goodwill, loss of business reputation, future reputation or publicity, damage to credit rating and indirect, remote, abnormal or <em>unforeseeable loss, or any similar loss whether or not in the reasonable contemplation of the parties.</em></p>
<p style=\"text-align: left;\"><em><strong>Declaration:&nbsp; [helmet_link]</strong></em></p>
<p><em>Copyright &copy; 2016 MiBase Toy Library Inc, All rights reserved.</em></p>', 'helmet_waiver', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (51, '19:29:12.750535+11', 'Please sign up now for roster duty', 'Content for Please sign up now for roster duty', '1_month_roster_reminder', NULL, 'AUTO');
INSERT INTO public.template VALUES (52, '19:32:18.199315+11', 'Please sign up now for roster duty', 'Content for Please sign up now for roster duty', '6_month_roster_reminder', NULL, 'AUTO');
INSERT INTO public.template VALUES (50, '19:24:06.187381+11', 'Subject for saved bulk email', '<p>content for saved bulk email blah blah</p>', 'bulk_email', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (53, '19:41:57.237071+10', 'Bouncy Castle Hire Application Received', '<p>Dear [firstname],</p>
<p>Thank you for your application to hire the Bouncy Castle with the [libraryname].</p>
<p>We will be in touch shortly.</p>
<p>[payment_info]</p>
<p>regards,</p>
<p>&nbsp;</p>
<p>[libraryname]</p>', 'approve_jc', 'michelle@mibase.com.au', 'PROCESS');
INSERT INTO public.template VALUES (54, '09:47:39.205625+11', 'Bouncy Castle Hire Application has been approved', '<h4>Dear [firstname],</h4>
<h4>&nbsp;</h4>
<h4>Thankyou for booking a bouncy castle&nbsp; with the <strong>Rangiora Toy Lib</strong>rary. Please note that bookings are only open to residents of the Waimakariri District.</h4>
<h4>[reservation]</h4>
<h4>TO CONFIRM YOUR BOOKING &ndash;</h4>
<h4>You will need to pay a 50% deposit into the Toy Library Bank Account &ndash; 12-3616-0034018-00 within 48 hours of &nbsp;receipt of this email.</h4>
<h4>Please use your Surname and BC1 or BC2 as a reference, depending on which castle you have booked. You are welcome to transfer the full payment at this time also if you wish. Please note the following prices:<br /><br /></h4>
<h4>Bouncy Castle 1: (Smaller castle).</h4>
<h4>Members &ndash; \$30</h4>
<h4>Non-members &ndash; \$65 plus a \$50 refundable deposit<br /><br /></h4>
<h4>Bouncy Castle 2: (Larger castle with slide)</h4>
<h4>Members &ndash; \$50</h4>
<h4>Non-Members - \$85 plus a \$50 refundable deposit</h4>
<h4>&nbsp;</h4>
<h4>TERMS AND CONDITIONS:</h4>
<h4>Full Bouncy Castle Terms and Conditions and Hire Agreement are here <a href=\"https://rangiora.mibase.com.au/toy_images/rangiora/news/7.pdf\">Bouncy Castle terms and Conditions</a>. You will be required to sign these when you collect the castle from the library. <u><br /></u></h4>
<h4>Please note that if someone is collecting the castle on your behalf they will be required to sign the terms and conditions and hire agreement.</h4>
<h4>&nbsp;</h4>
<h4>IDENTIFICATION:</h4>
<h4>For NON-MEMBERS we ask that you bring a form of ID&nbsp;&ndash; drivers license preferred or passport. We also ask that you bring a proof of address e.g. utility bill.</h4>
<h4>&nbsp;</h4>
<h4>BOND:</h4>
<h4>A \$50 bond is required by non-members at the time of collecting the castle &ndash; this can be made via eftpos or cash.</h4>
<h4>&nbsp;</h4>
<h4>COLLECTION AND RETURN:</h4>
<h4>The Bouncy Castle is only available for collection on a Saturday morning from 11.00am - 12.30pm.</h4>
<h4>The Bouncy Castle must be returned on &nbsp;Wednesday morning from 09.30am-11.30am.</h4>
<h4>As per the terms and conditions you will be charged additional fees if the castles are not returned on time.</h4>
<h4>&nbsp;</h4>
<h4>If any of the above conditions are not met prior to collection of the castle, the Rangiora Toy Library reserve the right to refuse hire.</h4>
<h4>&nbsp;</h4>
<h4>Thank you,</h4>
<h4>&nbsp;</h4>
<h4>Rangiora Toy Library Committee.</h4>
<p><img src=\"https://rangiora.mibase.com.au/toy_images/rangiora/news/6.jpg\" alt=\"logo\" width=\"200\" height=\"145\" /></p>
<h3>&nbsp;</h3>
<p>&nbsp;</p>', 'accept_jc', 'michelle@mibase.com.au', 'PROCESS');
INSERT INTO public.template VALUES (8, '11:33:57.353187+10', 'Toy Library details update', '<p>Hi [firstname],</p>
<p>&nbsp;</p>
<p>Each year the Training&nbsp; Toy Library checks our member details to ensure our records are up to date. In previous years we have done this via a paper list at sessions, but this year we are trialling a digital approach.</p>
<p>We would appreciate you checking your details below and then indicating if they are correct or require updating.</p>
<p>&nbsp;</p>
<p>If <span style=\"color: #008000;\"><strong>CORRECT</strong></span> please use this link to confirm your details [correct_link]</p>
<p><br />If <span style=\"color: #ff0000;\"><strong>INCORRECT</strong></span>, please reply to this email with the corrected details.</p>
<p>&nbsp;</p>
<p><strong>Name:</strong> [longname]</p>
<p><strong>Member type:</strong> [membertype]</p>
<p><strong>Address:</strong> [address]</p>
<p><strong>Borrowing children`s names:</strong> [children]</p>
<p><strong>Mobile number:</strong> [mobile]</p>
<p><strong>Email address</strong>: [email]</p>
<p>&nbsp;</p>
<p>Kind regards</p>
<p><em>Michelle Baird<br /></em></p>
<p><em>&nbsp;</em></p>
<p>&nbsp;</p>', 'member_update', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (100, '16:06:35.381772+10', 'Paypal payments page', '', 'paypal_buttons', '', 'PROCESS');
INSERT INTO public.template VALUES (110, '16:32:18.881474+10', 'Test Bulk Email', '', 'test_email', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (111, '20:25:40.528961+11', 'Gift Card from [Libraryname]', '<p>Dear [p_name],</p>
<p>Thank you for purchasing a Gift Card from [Libraryname]. As part of this electronic receipt you will note below is the Gift Card you can print and provide to the recipient.</p>
<p>Thank you for supporting the [Libraryname].</p>
<p>
<p><span style=\"color: #0000ff;\">Picture of a Gift Card</span></p>
<p>&nbsp;</p>
<p>This Gift Card entitles the recipient to a <span style=\"color: #0000ff;\">xxx</span> Month Toy Library Membership, or<span style=\"color: #0000ff;\"> \$xx</span> off the cost of a full membership, terms and conditions apply.</p>
<p>Please present this to the [Libraryname] to redeem your membership.</p>
<ol>
<li>This Gift Certificate is not redeemable for cash.</li>
<li>This Gift Certificate is non-transferable and resale is prohibited.</li>
<li>If a Gift Certificate is lost, stolen, destroyed or used without permission, a replacement will not be provided in these circumstances.</li>
<li>This Gift Certificate is Valid until <strong>[expired]</strong> unless otherwise discussed and approved with [Libraryname].</li>
</ol>
<p>___________________________________________________________________________________</p>
<p><strong>Admin Only</strong></p>
<p>Gift Card Number:&nbsp; [id]</p>
<p>&nbsp;</p>
<p><span style=\"border-radius: 2px; text-indent: 20px; width: auto; padding: 0px 4px 0px 0px; text-align: center; font: bold 11px/20px &#96;Helvetica Neue&#96;,Helvetica,sans-serif; color: #ffffff; background: #bd081c  no-repeat scroll 3px 50% / 14px 14px; position: absolute; opacity: 1; z-index: 8675309; display: none; cursor: pointer; top: 140px; left: 18px;\">Save</span></p>
<p><span style=\"border-radius: 2px; text-indent: 20px; width: auto; padding: 0px 4px 0px 0px; text-align: center; font: bold 11px/20px &#96;Helvetica Neue&#96;,Helvetica,sans-serif; color: #ffffff; background: #bd081c  no-repeat scroll 3px 50% / 14px 14px; position: absolute; opacity: 1; z-index: 8675309; display: none; cursor: pointer; top: 140px; left: 18px;\">Save</span></p>', 'gift_voucher', '', 'PROCESS');
INSERT INTO public.template VALUES (112, '20:27:03.540133+11', 'Gift Card Paypal Payments Page', '<table style=\"height: 98px;\" width=\"652\">
<tbody>
<tr>
<td colspan=\"3\">
<p><strong>Pay for your Gift Card Using PayPal. Alternatively you can pay in cash at the [Libraryname]<br /></strong></p>
<p><strong>Please Note a Pay Pal fee of \$0.95 will be incurred </strong></p>
<p>&nbsp;</p>
<p><strong><span style=\"color: #0000ff;\">Picture of a Gift Card</span><br /></strong></p>
</td>
</tr>
</tbody>
</table>
<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" target=\"paypal\">
<p><input name=\"cmd\" type=\"hidden\" value=\"_s-xclick\" /> <input name=\"hosted_button_id\" type=\"hidden\" value=\"93WM884TVSDRQ\" /> <input name=\"on0\" type=\"hidden\" value=\"Select a Gift Card Option\" /></p>
<p>&nbsp;</p>
<p><span style=\"color: #0000ff;\">Paypal Button</span></p>
</form>', 'paypal_gift', '', 'PROCESS');
INSERT INTO public.template VALUES (115, '11:19:54.704776+11', 'Payments Receipt - By Email', '[payments]', 'Payments_Email', '', 'PROCESS');
INSERT INTO public.template VALUES (122, '15:56:53.90171+10', 'Your Toy [idcat] is ready to be collected.', '<p>Dear [firstname],</p>
<p>A toy that you placed on hold is ready to be collected, details below.</p>
<p>[toy]</p>
<p>The toy will be held for you for [days_hold] days until <span class=\"_5yl5\">[expired]</span>, after which it will be placed back into the collection.</p>
<p>Any issues, please email us at enquiries@maroondahtoylibrary.org.au.</p>
<p>&nbsp;</p>
<p>Looking forward to seeing you at the toy library shortly.</p>
<p>&nbsp;</p>
<p>Kind regards</p>
<p>Bec Lalor</p>
<p><em>Maroondah Toy Library Coordinator</em></p>', 'hold_ready', '', 'PROCESS');
INSERT INTO public.template VALUES (123, '16:02:22.489429+10', 'Please return toy [idcat], it has been requested by another member.', '<p>Dear [firstname],</p>
<p>A toy that you have on loan has been placed on hold by another member, details below.</p>
<p>[toy]</p>
<p>Please return the toy by its due date of [due]. You will be unable to renew the toy past its due date.&nbsp;</p>
<p>Any issues, please email us at enquiries@maroondahtoylibrary.org.au.</p>
<p>&nbsp;</p>
<p>Looking forward to seeing you at the toy library shortly.</p>
<p>&nbsp;</p>
<p>Kind regards</p>
<p>Bec Lalor</p>
<p><em>Maroondah Toy Library Coordinator</em></p>', 'hold_return', '', 'PROCESS');


INSERT INTO public.typeevent VALUES (1, 'Roster Alert', 1, 'ROSTER ALERT will be sent by automatic Email', 0);
INSERT INTO public.typeevent VALUES (2, 'Helmet', 1, 'Member has signed the Helmet waiver', 0);
INSERT INTO public.typeevent VALUES (4, 'Levy Roster', 2, 'No Roster duty and debit Duty Levy                ', 45);

INSERT INTO public.typepart VALUES (1, 'Broken', NULL, NULL);
INSERT INTO public.typepart VALUES (2, 'Fixed', NULL, NULL);
INSERT INTO public.typepart VALUES (3, 'Found', NULL, NULL);
INSERT INTO public.typepart VALUES (4, 'Goldstar', NULL, NULL);
INSERT INTO public.typepart VALUES (5, 'Missing', NULL, NULL);
INSERT INTO public.typepart VALUES (6, 'Replaced', NULL, NULL);
INSERT INTO public.typepart VALUES (7, 'Spare', NULL, NULL);
INSERT INTO public.typepart VALUES (8, 'Warning', NULL, NULL);
INSERT INTO public.typepart VALUES (10, 'Alert', NULL, NULL);
INSERT INTO public.typepart VALUES (21, 'Small Parts', 'This Toy has Small Parts', 'small_parts');
INSERT INTO public.typepart VALUES (22, 'Batteries', 'This Toy requires batteries', 'batteries');
INSERT INTO public.typepart VALUES (23, 'Small Balls', 'This Toy has Small Balls', 'small_balls');
INSERT INTO public.typepart VALUES (24, 'Magnets', 'This Toy has Magnets', 'magnets');
INSERT INTO public.typepart VALUES (25, 'Wash', 'This Costume requires washing', 'wash');

INSERT INTO public.typepayment VALUES (1, 'Cash');
INSERT INTO public.typepayment VALUES (2, 'Cheque');
INSERT INTO public.typepayment VALUES (3, 'VISA');
INSERT INTO public.typepayment VALUES (5, 'Internet');
INSERT INTO public.typepayment VALUES (6, 'PayPal');

INSERT INTO public.users VALUES ('admin', 'admin', 4, 'admin', 'demo', '', 140, false, NULL, NULL, '2018-05-07', 'admin');

INSERT INTO public.volunteer VALUES (1, 'Toy Purchasing and Processing');
INSERT INTO public.volunteer VALUES (2, 'Fundraising and Events');
INSERT INTO public.volunteer VALUES (3, 'Marketing and Communications');
INSERT INTO public.volunteer VALUES (4, 'Distributing posters to community businesses');
INSERT INTO public.volunteer VALUES (5, 'Administration and Finance');
INSERT INTO public.volunteer VALUES (6, 'Website and IT Support');
INSERT INTO public.volunteer VALUES (7, 'I want to help but I`m not sure how! Please contact me');
INSERT INTO public.volunteer VALUES (8, 'Messy Play Days');

INSERT INTO public.warnings VALUES ('FIRE', 'Please keep away from fire', 1);

SELECT pg_catalog.setval('public.age_id_seq', 1000, true);

SELECT pg_catalog.setval('public.city_id_seq', 1000, true);

SELECT pg_catalog.setval('public.condition_id_seq', 1000, false);

SELECT pg_catalog.setval('public.contact_log_id_seq', 10000000, false);

SELECT pg_catalog.setval('public.discovery_id_seq', 1000, true);

SELECT pg_catalog.setval('public.event_id_seq', 1000, false);

SELECT pg_catalog.setval('public.feedback_id_seq', 1, true);

SELECT pg_catalog.setval('public.files_id_seq', 1000, false);

SELECT pg_catalog.setval('public.gift_cards_sq', 1000, false);

SELECT pg_catalog.setval('public.holds_id_seq', 1000000, true);

SELECT pg_catalog.setval('public.id_seq_users', 1000, false);

--journal

SELECT pg_catalog.setval('public.log_id_seq', 1, false);

--manufacturers

SELECT pg_catalog.setval('public.membertype_id_seq', 1000, true);

SELECT pg_catalog.setval('public.nation_id_seq', 1000, true);

SELECT pg_catalog.setval('public.parts_id_seq', 10000000, true);

SELECT pg_catalog.setval('public.partypack_id_seq', 10000, true);

SELECT pg_catalog.setval('public.paymentoptions_id_seq', 1000, true);

SELECT pg_catalog.setval('public.postcode_id_seq', 10000, false);

SELECT pg_catalog.setval('public.report_id_seq', 1000, false);

SELECT pg_catalog.setval('public.reserve_id_seq', 1000000, true);

SELECT pg_catalog.setval('public.reserve_toy_id_seq', 1000000, true);

--roster

SELECT pg_catalog.setval('public.rostertypes_id_seq', 1000, true);

SELECT pg_catalog.setval('public.select_id_seq', 1000, false);

SELECT pg_catalog.setval('public.settings_id_seq', 10000, true);

--stats

SELECT pg_catalog.setval('public.storage_id_seq', 1, false);

SELECT pg_catalog.setval('public.suburb_id_seq', 10000, true);

--supplier

SELECT pg_catalog.setval('public.template_id_seq', 1000, true);

SELECT pg_catalog.setval('public.toy_id_seq', 1000000, true);

--transaction

SELECT pg_catalog.setval('public.typeevent_id_seq', 1000, true);

SELECT pg_catalog.setval('public.typepart_id_seq', 1000, true);

SELECT pg_catalog.setval('public.typepayment_id', 1000, true);

SELECT pg_catalog.setval('public.volunteer_id_seq', 1000, false);

SELECT pg_catalog.setval('public.warning_id', 1000, false);

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT \"ID\" PRIMARY KEY (id);

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (category);

ALTER TABLE ONLY public.children
    ADD CONSTRAINT child_pkey PRIMARY KEY (childid);

ALTER TABLE ONLY public.city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.borwrs
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.condition
    ADD CONSTRAINT condition_condition_key UNIQUE (condition);

ALTER TABLE ONLY public.condition
    ADD CONSTRAINT condition_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.contact_log
    ADD CONSTRAINT contact_log_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.discovery
    ADD CONSTRAINT discovery_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.files
    ADD CONSTRAINT files_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.gift_cards
    ADD CONSTRAINT gift_cards_id PRIMARY KEY (id);

ALTER TABLE ONLY public.toy_holds
    ADD CONSTRAINT holds_id PRIMARY KEY (id);

ALTER TABLE ONLY public.toys
    ADD CONSTRAINT id_cat PRIMARY KEY (category, id);

ALTER TABLE ONLY public.users
    ADD CONSTRAINT id_key PRIMARY KEY (id);

ALTER TABLE ONLY public.nationality
    ADD CONSTRAINT id_nation PRIMARY KEY (id);

ALTER TABLE ONLY public.homepage
    ADD CONSTRAINT id_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.typepayment
    ADD CONSTRAINT id_type PRIMARY KEY (id);

ALTER TABLE ONLY public.toys
    ADD CONSTRAINT idcat UNIQUE (idcat);

ALTER TABLE ONLY public.journal
    ADD CONSTRAINT journal_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.loan_restrictions
    ADD CONSTRAINT loan_restrictions_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_id PRIMARY KEY (id);

ALTER TABLE ONLY public.log
    ADD CONSTRAINT log_id PRIMARY KEY (id);

ALTER TABLE ONLY public.manufacturer
    ADD CONSTRAINT manufacturer_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.stats
    ADD CONSTRAINT member_stats_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.membertype
    ADD CONSTRAINT membertype_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.membertype
    ADD CONSTRAINT memtype UNIQUE (membertype);

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.age
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.overdue
    ADD CONSTRAINT overdue_id PRIMARY KEY (id);

ALTER TABLE ONLY public.parts
    ADD CONSTRAINT parts_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.partypack
    ADD CONSTRAINT party_pack_id PRIMARY KEY (id);

ALTER TABLE ONLY public.paymentoptions
    ADD CONSTRAINT paymentoptions_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.postcode
    ADD CONSTRAINT postcode_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT reports_id PRIMARY KEY (id);

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reserve_id PRIMARY KEY (id);

ALTER TABLE ONLY public.reserve_toy
    ADD CONSTRAINT reserve_toy_id PRIMARY KEY (id);

ALTER TABLE ONLY public.rostertypes
    ADD CONSTRAINT rostertypes_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.selectbox
    ADD CONSTRAINT selectbox_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT setting_pk PRIMARY KEY (setting_name);

ALTER TABLE ONLY public.storage
    ADD CONSTRAINT storage_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.storage
    ADD CONSTRAINT storage_storage_key UNIQUE (storage);

ALTER TABLE ONLY public.stype
    ADD CONSTRAINT stype_id PRIMARY KEY (id);

ALTER TABLE ONLY public.sub_category
    ADD CONSTRAINT sub_category_pkey PRIMARY KEY (sub_category);

ALTER TABLE ONLY public.suburb
    ADD CONSTRAINT suburb_id PRIMARY KEY (id);

ALTER TABLE ONLY public.supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.template
    ADD CONSTRAINT template_id PRIMARY KEY (id);

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT trans_pk PRIMARY KEY (date_loan, borid, idcat);

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT type_name UNIQUE (notification_type, notification_name);

ALTER TABLE ONLY public.typeevent
    ADD CONSTRAINT typeevent_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.typepart
    ADD CONSTRAINT typepart_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.typepart
    ADD CONSTRAINT typepart_typepart_key UNIQUE (typepart);

ALTER TABLE ONLY public.users
    ADD CONSTRAINT username UNIQUE (username);

ALTER TABLE ONLY public.volunteer
    ADD CONSTRAINT volunteer_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.warnings
    ADD CONSTRAINT warnings_id PRIMARY KEY (id);

CREATE INDEX bor_name_index ON public.borwrs USING btree (lower((surname)::text));

CREATE INDEX category_index ON public.category USING btree (upper((category)::text), upper((category)::text));

CREATE INDEX city_index ON public.city USING btree (upper((city)::text), upper((city)::text));

CREATE INDEX condition_index ON public.condition USING btree (upper((condition)::text), upper((condition)::text));

CREATE INDEX discovery_index ON public.discovery USING btree (upper((discovery)::text), upper((discovery)::text));

CREATE INDEX fki_borwrs ON public.event USING btree (memberid);

CREATE INDEX fki_child ON public.children USING btree (id);

CREATE INDEX fki_idcat ON public.parts USING btree (itemno);

CREATE INDEX fki_memtype ON public.borwrs USING btree (membertype);

CREATE INDEX id ON public.toys USING btree (id);

CREATE INDEX id_trans ON public.transaction USING btree (id);

CREATE INDEX membertype_index ON public.membertype USING btree (upper((membertype)::text), upper((membertype)::text));

CREATE INDEX name_index ON public.children USING btree (lower((child_name)::text), lower((child_name)::text));

CREATE INDEX storage_index ON public.storage USING btree (upper((storage)::text), upper((storage)::text));

CREATE INDEX trans_index ON public.transaction USING btree (upper((idcat)::text));

CREATE INDEX type_event_index ON public.event USING btree (upper((typeevent)::text), upper((typeevent)::text));

ALTER TABLE ONLY public.toys
    ADD CONSTRAINT category FOREIGN KEY (category) REFERENCES public.category(category) ON DELETE RESTRICT;

ALTER TABLE ONLY public.children
    ADD CONSTRAINT child FOREIGN KEY (id) REFERENCES public.borwrs(id) ON DELETE RESTRICT;

ALTER TABLE ONLY public.parts
    ADD CONSTRAINT idcat FOREIGN KEY (itemno) REFERENCES public.toys(idcat) ON DELETE RESTRICT;

ALTER TABLE ONLY public.borwrs
    ADD CONSTRAINT memtype FOREIGN KEY (membertype) REFERENCES public.membertype(membertype) ON UPDATE RESTRICT ON DELETE RESTRICT;


"  ;
         
