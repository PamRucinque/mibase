<?php
/* 
 * Copyright (C) 2019 Mibase
 *
 * This program is provided under the terms of the GNU General Public License version 3
 * refer licence.html in root folder for full text.
 */

require( dirname(__FILE__) . '/../config.php');
require( dirname(__FILE__) . '/../version.php');
require(dirname(__FILE__) . '/db_functions.php');

//if the shared server is not defined then set it to be a single instance server
if (!isset($shared_server)) {
    $shared_server = false;
}

//if the $db_postgres_password is defined then set this user and password for the database and toybase user and password
if (isset($db_postgres_password)) {
    if ($shared_server) {
        $toybasedbuser = 'postgres';
        $toybasedbpasswd = $db_postgres_password;
        if (!isset($toybasedbname)) {
            $toybasedbname = 'toybase';
        }
    } else {
        if (!isset($dbname)) {
            $dbname = 'mibase';
        }
        $dbuser = 'postgres';
        $dbpasswd = $db_postgres_password;
    }
}

if (!session_id()) {
    session_start();
}

if (!isset($_SESSION['dbadmin'])) {
    // there is a session but no session variable app 
    // redirect the user to the login page
    header('location:' . $_SESSION['app_root_location'] . '/db/index.php');
    exit();
} else if ($_SESSION['dbadmin'] != 'true') {
    // there is a session with session variable app but the app session variable is not admin 
    // ( ie they are not logged in to the admin app ) then send them to the login page.
    header('location:' . $_SESSION['app_root_location'] . '/db/index.php');
    exit();
}


$note = "";
$toybase = false;

if (!$shared_server) {
    //a single server
    $databaseuser = $dbuser;
    $databasepasswd = $dbpasswd;
    $databasename = $dbname;
    $toybase = false;
} else {
    //a shared server
    $toybase = filter_input(INPUT_GET, 'toybase', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    if (isset($toybase)) {
        $databaseuser = $toybasedbuser;
        $databasepasswd = $toybasedbpasswd;
        $databasename = $toybasedbname;
        $toybase = true;
    }

    $library_code = filter_input(INPUT_GET, 'library_code', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    if (isset($library_code)) {
        $databaseuser = $library_code;
        $databasepasswd = get_library_database_password($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd, $library_code);
        $databasename = $library_code;
        $toybase = false;
    }
}


$connect_str = "host='" . $dbhost . "' port=" . $dbport . " dbname='" . $databasename . "' user='" . $databaseuser . "' password='" . $databasepasswd . "'";
$conn = pg_connect($connect_str);

include(dirname(__FILE__) . '/fill_demo_data_1_' . $code_version_minor . '_sql.php');

$results = pg_query($conn, $query);

$note = "";

if (!$results) {
    $note = pg_last_error($conn);
} else {
    $note = "Database filled with Demo Data successfully\n";
}
pg_close($conn);
?>

<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width">
    <link href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src='../js/jquery-3.0.0.js' type='text/javascript'></script> 
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="card card-container">
                <h1>Create Database</h1>
                <div><?php echo $note ?></div>
                <a href="manage.php" class="btn btn-info" role="button"> Return </a>
            </div>
        </div>
    </div>
</body>


