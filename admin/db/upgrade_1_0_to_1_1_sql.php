<?php
/* 
 * Copyright (C) 2019 Mibase
 *
 * This program is provided under the terms of the GNU General Public License version 3
 * refer licence.html in root folder for full text.
 */

$query = "

ALTER TABLE public.borwrs ALTER COLUMN id_license TYPE character varying;
ALTER TABLE public.borwrs DROP COLUMN IF EXISTS history;
ALTER TABLE public.borwrs ADD COLUMN pin bigint;

DROP TABLE IF EXISTS public.email_log;

CREATE SEQUENCE public.feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.feedback (
    id integer DEFAULT nextval('public.feedback_id_seq'::regclass) NOT NULL,
    library character varying(50),
    firstname character varying(25),
    lastname character varying(25),
    contact character(5),
    rating character varying(2),
    feedback character varying(1000)
);

DROP TABLE IF EXISTS public.images;

ALTER TABLE public.paymentoptions ADD COLUMN payment_group character varying(20);

DROP TABLE IF EXISTS public.pieces;

ALTER TABLE public.reserve_toy ADD COLUMN install_location character varying;

ALTER TABLE public.stype ALTER COLUMN stype TYPE character varying(5);

ALTER TABLE public.sub_category ADD COLUMN category character varying(5);

CREATE SEQUENCE public.suburb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.suburb ALTER COLUMN id SET DEFAULT nextval('public.suburb_id_seq'::regclass);

ALTER TABLE public.toy_holds ALTER COLUMN approved TYPE character varying;

ALTER TABLE public.toys ALTER COLUMN history TYPE character varying;

ALTER TABLE public.transaction ADD COLUMN times_renew bigint DEFAULT 0;

ALTER TABLE public.warnings ALTER COLUMN description TYPE character varying(10);

CREATE TABLE public.version (
    item varchar(20) NOT NULL,
    major int NOT NULL,
    minor int NOT NULL
);

INSERT INTO public.version VALUES ('db', 1, 1);";

