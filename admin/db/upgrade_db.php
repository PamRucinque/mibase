<?php
/* 
 * Copyright (C) 2019 Mibase
 *
 * This program is provided under the terms of the GNU General Public License version 3
 * refer licence.html in root folder for full text.
 */

require( dirname(__FILE__) . '/../config.php');
require(dirname(__FILE__) . '/../version.php');
require(dirname(__FILE__) . '/db_functions.php');

if (!session_id()) {
    session_start();
}

if (!isset($_SESSION['dbadmin'])) {
    // there is a session but no session variable app 
    // redirect the user to the login page
    header('location:' . $_SESSION['app_root_location'] . '/db/index.php');
    exit();
} else if ($_SESSION['dbadmin'] != 'true') {
    // there is a session with session variable app but the app session variable is not admin 
    // ( ie they are not logged in to the admin app ) then send them to the login page.
    header('location:' . $_SESSION['app_root_location'] . '/db/index.php');
    exit();
}


//if the $db_postgres_password is defined then set this user and password for the database and toybase user and password
if (isset($db_postgres_password)) {
    if ($shared_server) {
        $toybasedbuser = 'postgres';
        $toybasedbpasswd = $db_postgres_password;
        if (!isset($toybasedbname)) {
            $toybasedbname = 'toybase';
        }
    } else {
        if (!isset($dbname)) {
            $dbname = 'mibase';
        }
        $dbuser = 'postgres';
        $dbpasswd = $db_postgres_password;
    }
}


if ($shared_server) {
//get the library_code from the input post of get variables.
    $library_code = filter_input(INPUT_GET, 'library_code', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    if (!isset($library_code)) {
        //try and get it from post
        $library_code = filter_input(INPUT_POST, 'library_code', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    }
    $dbname = $library_code;
    $dbuser = $library_code;
    $dbpasswd = get_library_database_password($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd, $library_code);
}

$connect_str = "host='" . $dbhost . "' port=" . $dbport . " dbname='" . $dbname . "' user='" . $dbuser . "' password='" . $dbpasswd . "'";

$conn = pg_connect($connect_str);

$count = 0;

while (true) {

    $s = get_state($dbhost, $dbport, $dbname, $dbuser, $dbpasswd, $code_version_minor);
    $db_minor = $s['db_minor'];
    $code_minor = $s['code_minor'];

    $count++;

    if ($db_minor == $code_minor || $count > 20) {
        break;
    }

    include(dirname(__FILE__) . '/upgrade_1_' . $db_minor . '_to_1_' . ($db_minor + 1) . '_sql.php');

    $results = pg_query($conn, $query);

    $note = "";

    if (!$results) {
        $note = pg_last_error($conn);
    } else {
        $note .= "Database Upgrade 1." . $db_minor . " to 1." . ($db_minor + 1) . " successfully<br>";
    }
}


pg_close($conn);
?>

<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width">
    <link href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src='../js/jquery-3.0.0.js' type='text/javascript'></script> 
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="card card-container">
                <h1>Update Database</h1>
                <div><?php echo $note ?></div>
                <a href="manage.php" class="btn btn-info" role="button"> Return </a>
            </div><!-- /card-container -->
        </div><!-- /row -->
    </div><!-- /container -->
</body>


