<?php
/* 
 * Copyright (C) 2019 Mibase
 *
 * This program is provided under the terms of the GNU General Public License version 3
 * refer licence.html in root folder for full text.
 */

$query = "--Mibase Database Version 1.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

CREATE FUNCTION public.date_round(base_date timestamp with time zone, round_interval interval) RETURNS timestamp with time zone
    LANGUAGE sql STABLE
    AS \$_\$
SELECT TO_TIMESTAMP((EXTRACT(epoch FROM \$1)::INTEGER + EXTRACT(epoch FROM \$2)::INTEGER / 2)
                / EXTRACT(epoch FROM \$2)::INTEGER * EXTRACT(epoch FROM \$2)::INTEGER)
\$_\$;

CREATE SEQUENCE public.age_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

SET default_with_oids = false;

CREATE TABLE public.age (
    id bigint DEFAULT nextval('public.age_id_seq'::regclass) NOT NULL,
    age text NOT NULL
);

CREATE SEQUENCE public.bor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.borwrs (
    id bigint NOT NULL,
    firstname character varying(100),
    surname character varying(100),
    membertype character varying(50),
    motherssurname character varying(100),
    partnersname character varying(100),
    partnerssurname character varying(100),
    address character varying(100),
    address2 character varying(100),
    address3 character varying(100),
    suburb character varying(100),
    city character varying(100),
    state character varying(100),
    postcode character varying(50),
    country character varying(50),
    work character varying(50),
    phone character varying(50),
    phone2 character varying(50),
    mobile1 character varying(50),
    mobile2 character varying(40),
    email boolean,
    email2 character varying(100),
    emailaddress character varying(100),
    rostertype character varying(100),
    rostertype2 character varying(100),
    rostertype3 character varying(40),
    rostertype4 character varying(40),
    rostertype5 character varying(40),
    id_license character(25),
    skills text,
    skills2 text,
    specialneeds text,
    alert text,
    notes text,
    rosternotes text,
    datejoined date,
    renewed date,
    expired date,
    resigneddate date,
    created timestamp with time zone DEFAULT now(),
    modified timestamp with time zone DEFAULT now(),
    flag boolean,
    lockdate date,
    levy boolean,
    lockreason character varying(200),
    resignedreason character varying(200),
    balance double precision,
    discoverytype character varying(50),
    lastarchive date,
    location character(50),
    member_status character varying(50),
    printbarcode boolean DEFAULT false,
    library character varying(50),
    pwd character varying(50) DEFAULT 'mibase'::character varying,
    helmet boolean,
    user1 character varying,
    rostertype6 character varying(100),
    marital_status character varying(20),
    age date,
    history character varying,
    member_update character varying(50),
    changedby character varying(20),
    agree character varying(5),
    photos character varying(5),
    key character varying,
    helmet_date timestamp with time zone,
    dollar_to_date numeric(12,2),
    agree_date timestamp with time zone,
    wwc character varying(50),
    value_ytd numeric(12,2) DEFAULT 0,
    date_application date,
    conduct character varying,
    conduct_date timestamp with time zone,
    rego_id bigint,
    wwc2 character varying(50)
);

CREATE SEQUENCE public.category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.category (
    id bigint DEFAULT nextval('public.category_id_seq'::regclass) NOT NULL,
    category character varying(3) NOT NULL,
    description text,
    toys integer DEFAULT 1
);

CREATE SEQUENCE public.child_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.children (
    childid bigint DEFAULT nextval('public.child_id_seq'::regclass) NOT NULL,
    child_name character varying(50),
    surname character varying(50),
    sex character varying(8),
    d_o_b date,
    notes character varying(1000),
    alert boolean,
    id bigint,
    changedby character varying(20)
);

CREATE SEQUENCE public.city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.city (
    id bigint DEFAULT nextval('public.city_id_seq'::regclass) NOT NULL,
    city character varying(50) NOT NULL,
    postcode character varying(10),
    exclude character varying(5) DEFAULT 'No'::character varying
);

CREATE SEQUENCE public.condition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.condition (
    id bigint DEFAULT nextval('public.condition_id_seq'::regclass) NOT NULL,
    condition character varying(50) NOT NULL
);

CREATE SEQUENCE public.contact_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.contact_log (
    id bigint DEFAULT nextval('public.contact_log_id_seq'::regclass) NOT NULL,
    type_contact character varying,
    details character varying,
    datetime time with time zone[],
    borid bigint,
    template_id bigint,
    template character varying(20),
    subject character varying,
    email character varying,
    created timestamp with time zone DEFAULT now()
);

CREATE SEQUENCE public.discovery_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.discovery (
    id bigint DEFAULT nextval('public.discovery_id_seq'::regclass) NOT NULL,
    discovery character varying(50) NOT NULL
);

CREATE TABLE public.email_log (
    id bigint,
    subject character varying
);

CREATE SEQUENCE public.event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.event (
    id bigint DEFAULT nextval('public.event_id_seq'::regclass) NOT NULL,
    description text,
    memberid bigint,
    typeevent character varying(30),
    rostertype character varying(30),
    nohours numeric(12,1),
    completed boolean,
    alertuser boolean,
    event_date timestamp with time zone DEFAULT now(),
    amount double precision,
    username character varying(20)
);

CREATE SEQUENCE public.files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.files (
    id bigint DEFAULT nextval('public.files_id_seq'::regclass) NOT NULL,
    filename character varying,
    description character varying,
    type_file character varying(20),
    lastupdate time with time zone,
    access character varying(20)
);

CREATE SEQUENCE public.gift_cards_sq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.gift_cards (
    id bigint DEFAULT nextval('public.gift_cards_sq'::regclass) NOT NULL,
    p_name character varying,
    mobile character varying,
    email character varying,
    newsletter character varying(5),
    created timestamp with time zone,
    library character varying,
    email_sent character varying(5) DEFAULT 'No'::character varying,
    expired date,
    credit_sent character varying(5),
    amount numeric(12,2),
    borid bigint,
    online_id bigint
);

CREATE SEQUENCE public.holds_id_seq
    START WITH 126
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.homepage (
    id bigint NOT NULL,
    frontpage text,
    open_hours text,
    lastupdated date,
    webpage text,
    contact_info text,
    conditions text,
    payment_info character varying,
    castle character varying,
    castle_link character varying,
    helmet_waiver character varying,
    payment_info_join character varying,
    member_homepage character varying,
    wwc character varying,
    rego_id bigint,
    sponsor_page character varying,
    member_options character varying
);

CREATE SEQUENCE public.id_seq_users
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.images (
    id bigint NOT NULL,
    code character varying,
    filename character varying
);

CREATE SEQUENCE public.journal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.journal (
    id bigint DEFAULT nextval('public.journal_id_seq'::regclass) NOT NULL,
    description text,
    name text,
    category text,
    cat2 text,
    typepayment text,
    bcode bigint,
    gst numeric(12,1),
    icode character varying(10),
    type character varying(2),
    amount numeric(12,1),
    datepaid date DEFAULT now(),
    debitdate timestamp with time zone DEFAULT now(),
    location character varying,
    changedby character varying(20),
    holdid bigint
);

CREATE TABLE public.loan_restrictions (
    id bigint NOT NULL,
    membertype character varying,
    category character varying,
    free bigint DEFAULT 0,
    weight numeric(12,2) DEFAULT 0,
    loan_type character varying NOT NULL,
    notes character varying(50)
);

CREATE TABLE public.location (
    id bigint NOT NULL,
    location character(15),
    description character varying(50)
);

CREATE SEQUENCE public.log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.log (
    ip character varying(20),
    id bigint DEFAULT nextval('public.log_id_seq'::regclass) NOT NULL,
    subdomain character varying(50),
    username character varying,
    password character varying,
    logintype character varying(10),
    status character varying,
    created timestamp without time zone DEFAULT now(),
    email character varying
);

CREATE SEQUENCE public.manufacturer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.manufacturer (
    id bigint DEFAULT nextval('public.manufacturer_id_seq'::regclass) NOT NULL,
    manufacturer text,
    address text,
    phone text,
    fax text,
    email text,
    contact text,
    discount numeric(12,1)
);

CREATE SEQUENCE public.membertype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.membertype (
    id bigint DEFAULT nextval('public.membertype_id_seq'::regclass) NOT NULL,
    membertype character varying,
    maxnoitems bigint,
    description text,
    renewal_fee numeric(12,1),
    duties numeric(12,2),
    units bigint,
    bond numeric(12,1),
    rent numeric(12,1),
    returnperiod bigint,
    expiryperiod bigint,
    paypal character varying,
    exclude character varying(3),
    due date,
    jc character varying,
    self_checkout character varying(5),
    extra bigint,
    gold_star integer DEFAULT 0
);

CREATE SEQUENCE public.nation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.nationality (
    id bigint DEFAULT nextval('public.nation_id_seq'::regclass) NOT NULL,
    nationality character varying NOT NULL,
    description character varying
);

CREATE TABLE public.notifications (
    id bigint NOT NULL,
    notification_type character(10) DEFAULT 'email'::bpchar NOT NULL,
    notification_name character varying(50) NOT NULL,
    days_before integer DEFAULT 0,
    weekday character varying(15),
    username character varying,
    password character varying,
    source character varying,
    description character varying,
    active character varying(3)
);

CREATE TABLE public.overdue (
    id integer NOT NULL,
    weekly_fine character varying(3) DEFAULT 'Yes'::character varying,
    rent_as_fine character varying(3) DEFAULT 'No'::character varying,
    fine_value numeric(5,2) DEFAULT 0,
    days_grace integer DEFAULT 0,
    record_fine character varying(3) DEFAULT 'No'::character varying,
    fine_factor numeric(5,2) DEFAULT 1,
    daily_limit numeric DEFAULT 0,
    daily_fine character varying(3),
    roundup_week character varying(3),
    hire_factor bigint DEFAULT 0 NOT NULL,
    extra_fine numeric(5,2) DEFAULT 0,
    custom character varying
);

CREATE SEQUENCE public.parts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.parts (
    id bigint DEFAULT nextval('public.parts_id_seq'::regclass) NOT NULL,
    datepart date,
    itemno text,
    itemname text,
    description text NOT NULL,
    type text,
    qty bigint,
    found boolean,
    borcode bigint,
    cost numeric(12,1),
    foundby text,
    alertuser boolean,
    changedby character varying(20),
    borname character varying,
    comments character varying,
    paid boolean DEFAULT false
);

CREATE TABLE public.partypack (
    toyname character varying,
    id bigint NOT NULL,
    desc1 text,
    desc2 text,
    cost numeric(10,2),
    lastupdated date
);

CREATE SEQUENCE public.partypack_id_seq
    START WITH 114
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.paymentoptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.paymentoptions (
    id bigint DEFAULT nextval('public.paymentoptions_id_seq'::regclass) NOT NULL,
    paymentoptions text NOT NULL,
    crdr text,
    accountcode text,
    amount numeric(12,1),
    typepayment text,
    group1 bigint,
    description character varying,
    \"group\" character varying
);

CREATE TABLE public.pieces (
    id bigint NOT NULL,
    idcat character varying,
    piece character varying,
    qty bigint
);

CREATE SEQUENCE public.postcode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.postcode (
    id bigint DEFAULT nextval('public.postcode_id_seq'::regclass) NOT NULL,
    suburb text,
    postcode text
);

CREATE SEQUENCE public.report_id_seq
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.reports (
    id bigint DEFAULT nextval('public.report_id_seq'::regclass) NOT NULL,
    reportname character varying,
    category character varying,
    sql character varying,
    code character varying(20)
);

CREATE SEQUENCE public.reserve_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.reservations (
    approved boolean,
    member_id bigint,
    date_start date,
    date_end date,
    date_created timestamp without time zone,
    partypack_id bigint,
    id bigint DEFAULT nextval('public.reserve_id_seq'::regclass) NOT NULL
);

CREATE SEQUENCE public.reserve_toy_id_seq
    START WITH 118
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.reserve_toy (
    approved boolean,
    member_id bigint,
    date_start date,
    date_end date,
    date_created timestamp without time zone,
    partypack_id bigint,
    id bigint DEFAULT nextval('public.reserve_toy_id_seq'::regclass) NOT NULL,
    idcat character varying(50),
    borname text,
    phone text,
    status character varying(10),
    paid character varying(5),
    contact character varying,
    address character varying,
    emailaddress character varying
);

CREATE SEQUENCE public.roster_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.roster (
    id bigint DEFAULT nextval('public.roster_id_seq'::regclass),
    date_roster date,
    member_id bigint,
    duration double precision DEFAULT 1,
    type_roster character varying(100),
    approved boolean DEFAULT false,
    session_role character varying(50),
    roster_session character varying(50),
    weekday character varying(100),
    date_created date,
    complete boolean DEFAULT false,
    location character varying,
    comments character varying,
    status character varying(10),
    modified timestamp with time zone,
    contact bigint DEFAULT 1,
    delete_comments character varying
);

CREATE SEQUENCE public.rostertypes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

SET default_with_oids = false;

CREATE TABLE public.rostertypes (
    id bigint DEFAULT nextval('public.rostertypes_id_seq'::regclass) NOT NULL,
    rostertype text,
    description character varying(50),
    nohours numeric DEFAULT 1,
    volunteers bigint,
    weekday character varying(10),
    location character varying,
    reservations character varying(3) DEFAULT 'Yes'::character varying
);

CREATE SEQUENCE public.select_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.selectbox (
    id bigint DEFAULT nextval('public.select_id_seq'::regclass) NOT NULL,
    selectbox character varying
);

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.settings (
    setting_name character varying NOT NULL,
    setting_value character varying,
    id bigint DEFAULT nextval('public.settings_id_seq'::regclass),
    description character varying,
    setting_type character varying
);

CREATE SEQUENCE public.stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.stats (
    id bigint DEFAULT nextval('public.stats_id_seq'::regclass) NOT NULL,
    current bigint,
    new bigint,
    locked bigint,
    date_stats date,
    timezone character varying,
    children bigint,
    week bigint,
    loans bigint,
    returns bigint,
    location character varying(20),
    members bigint,
    renewed bigint,
    weekday character varying(20),
    expired bigint,
    resigned bigint,
    total bigint,
    volunteer bigint
);

CREATE TABLE public.storage (
    id bigint NOT NULL,
    storage character varying(50)
);

CREATE SEQUENCE public.storage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.storage_id_seq OWNED BY public.storage.id;

CREATE TABLE public.stype (
    id bigint NOT NULL,
    stype character varying(10),
    description character varying,
    amount numeric(12,2) DEFAULT 0,
    overdue numeric(12,2) DEFAULT 0
);

CREATE TABLE public.sub_category (
    id bigint DEFAULT nextval('public.category_id_seq'::regclass) NOT NULL,
    sub_category character varying(3) NOT NULL,
    description text
);

CREATE SEQUENCE public.sub_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.suburb (
    id bigint NOT NULL,
    suburb character varying
);

CREATE SEQUENCE public.supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.supplier (
    id bigint DEFAULT nextval('public.supplier_id_seq'::regclass) NOT NULL,
    supplier text,
    address text,
    phone text,
    fax text,
    email text,
    contact text,
    discount numeric(12,1)
);

CREATE TABLE public.template (
    id bigint NOT NULL,
    datetime time with time zone DEFAULT now(),
    subject character varying(100),
    message text,
    type_template character varying,
    template_email character varying(50),
    delivery_type character varying(15)
);

CREATE SEQUENCE public.template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.template_id_seq OWNED BY public.template.id;

CREATE TABLE public.toy_holds (
    id bigint DEFAULT nextval('public.holds_id_seq'::regclass) NOT NULL,
    borid bigint,
    date_start date,
    date_end date,
    approved boolean,
    created timestamp without time zone,
    idcat character varying,
    status character varying(10),
    paid character varying,
    notify_date date,
    transid bigint,
    reminder_date date
);

CREATE SEQUENCE public.toy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.toys (
    counter bigint DEFAULT nextval('public.toy_id_seq'::regclass) NOT NULL,
    id bigint NOT NULL,
    category character varying(3) NOT NULL,
    idcat character varying(50),
    reservecode character varying(50),
    toyname character varying(150),
    rent numeric(10,2),
    age character varying(30),
    datestocktake date,
    withdrawn boolean,
    withdrawndate date,
    manufacturer character varying(50),
    discountcost numeric(12,2),
    cost numeric(12,2),
    supplier character varying(50),
    desc1 character varying(2000),
    desc2 character varying(2000),
    comments character varying(1000),
    status boolean,
    returndateperiod integer DEFAULT 14,
    lockreason character varying(50),
    returndate timestamp with time zone,
    no_pieces integer DEFAULT 0,
    date_purchase date,
    sponsorname character varying(200),
    warnings character varying(1000),
    printbarcode boolean,
    user1 character varying(200),
    alert character varying(2000),
    toy_status character varying(20),
    stype character varying(20),
    units integer DEFAULT 1,
    borrower character varying(200),
    withdrawnreason text,
    reservedfor text,
    missingpieces text,
    reserved boolean,
    lockdate date,
    created timestamp with time zone DEFAULT now() NOT NULL,
    modified timestamp with time zone DEFAULT now(),
    storage character varying,
    helmet boolean,
    copy integer DEFAULT 1,
    location character varying(20),
    stocktake_status character(20),
    packaging_cost money,
    process_time numeric(12,2),
    loan_type character varying(20),
    freight numeric(10,2) DEFAULT 0,
    color character varying,
    changedby character varying(20),
    history text,
    link character varying(100),
    twitter character varying(100),
    sub_category character varying(10),
    count_loans bigint,
    no_pic character varying(3),
    block_image character varying(10)
);

CREATE SEQUENCE public.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.transaction (
    created timestamp with time zone DEFAULT now(),
    id bigint DEFAULT nextval('public.transaction_id_seq'::regclass) NOT NULL,
    idcat character varying(50) NOT NULL,
    borid bigint NOT NULL,
    due date,
    return date,
    borname character varying(100),
    item character varying(100),
    location character varying(50),
    units integer,
    timetrans time without time zone,
    date_loan date DEFAULT (now())::date NOT NULL,
    phone text,
    email_status date,
    contact_status bigint,
    session character varying(3),
    group_trans character varying(50),
    returnby character varying(20),
    loanby character varying(20),
    renew integer
);

CREATE SEQUENCE public.typeevent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.typeevent (
    id bigint DEFAULT nextval('public.typeevent_id_seq'::regclass) NOT NULL,
    type text,
    nohours bigint,
    description character varying(100),
    amount double precision
);

CREATE SEQUENCE public.typepart_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.typepart (
    id bigint DEFAULT nextval('public.typepart_id_seq'::regclass) NOT NULL,
    typepart text,
    description character varying(100),
    picture character varying
);

CREATE SEQUENCE public.typepayment_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.typepayment (
    id bigint DEFAULT nextval('public.typepayment_id'::regclass) NOT NULL,
    typepayment character varying
);

CREATE TABLE public.users (
    username character varying(25),
    password character varying(32),
    id bigint DEFAULT nextval('public.id_seq_users'::regclass) NOT NULL,
    login_type character varying(15) DEFAULT 'member'::bpchar NOT NULL,
    subdomain character varying(25),
    location character varying(20),
    libraryid bigint,
    lock boolean DEFAULT false NOT NULL,
    email character varying,
    mobile character varying(30),
    created date DEFAULT now(),
    level character varying
);

CREATE SEQUENCE public.volunteer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.volunteer (
    id bigint DEFAULT nextval('public.volunteer_id_seq'::regclass) NOT NULL,
    volunteer character varying NOT NULL
);

CREATE SEQUENCE public.warning_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.warnings (
    warning character varying(10),
    description character varying(100),
    id bigint DEFAULT nextval('public.warning_id'::regclass) NOT NULL
);

ALTER TABLE ONLY public.storage ALTER COLUMN id SET DEFAULT nextval('public.storage_id_seq'::regclass);

ALTER TABLE ONLY public.template ALTER COLUMN id SET DEFAULT nextval('public.template_id_seq'::regclass);

INSERT INTO public.age (id, age) VALUES (57, '12-18 months, 18 mnths-3 ');
INSERT INTO public.age (id, age) VALUES (58, '12-18 months, 3-5 years');
INSERT INTO public.age (id, age) VALUES (59, '18 mnths-3 yrs');
INSERT INTO public.age (id, age) VALUES (60, '18 mnths-3 yrs, 3-5 years');
INSERT INTO public.age (id, age) VALUES (61, '18 months - 4 years');
INSERT INTO public.age (id, age) VALUES (62, '18 months - 5 years');
INSERT INTO public.age (id, age) VALUES (63, '18 months - 6 years');
INSERT INTO public.age (id, age) VALUES (64, '18 months +');
INSERT INTO public.age (id, age) VALUES (65, '2 - 3 years');
INSERT INTO public.age (id, age) VALUES (66, '2 - 5 years');
INSERT INTO public.age (id, age) VALUES (67, '2 - 6 years');
INSERT INTO public.age (id, age) VALUES (68, '2 - 7 years');
INSERT INTO public.age (id, age) VALUES (69, '2+ years');
INSERT INTO public.age (id, age) VALUES (70, '3 - 10 years');
INSERT INTO public.age (id, age) VALUES (71, '3 - 24 months');
INSERT INTO public.age (id, age) VALUES (72, '3 - 5 years');
INSERT INTO public.age (id, age) VALUES (73, '3 - 6 years');
INSERT INTO public.age (id, age) VALUES (74, '3 - 7 years');
INSERT INTO public.age (id, age) VALUES (75, '3 - 8 years');
INSERT INTO public.age (id, age) VALUES (76, '3+ years');
INSERT INTO public.age (id, age) VALUES (77, '3-5 years');
INSERT INTO public.age (id, age) VALUES (78, '3-5 years, 5+ years');
INSERT INTO public.age (id, age) VALUES (79, '3-6 months');
INSERT INTO public.age (id, age) VALUES (80, '3-6 months, 6-9 months, 9');
INSERT INTO public.age (id, age) VALUES (81, '4 - 5 years');
INSERT INTO public.age (id, age) VALUES (82, '4 - 6 years');
INSERT INTO public.age (id, age) VALUES (83, '4 - 7 years');
INSERT INTO public.age (id, age) VALUES (84, '4 - 8 years');
INSERT INTO public.age (id, age) VALUES (85, '4 months to walking');
INSERT INTO public.age (id, age) VALUES (86, '4+ years');
INSERT INTO public.age (id, age) VALUES (87, '5 - 7 years');
INSERT INTO public.age (id, age) VALUES (88, '5 - 8 years');
INSERT INTO public.age (id, age) VALUES (89, '5+ years');
INSERT INTO public.age (id, age) VALUES (90, '6 - 10 years');
INSERT INTO public.age (id, age) VALUES (91, '6 - 24 months');
INSERT INTO public.age (id, age) VALUES (92, '6 - 36 months');
INSERT INTO public.age (id, age) VALUES (93, '6 - 8 years');
INSERT INTO public.age (id, age) VALUES (94, '6 months +');
INSERT INTO public.age (id, age) VALUES (95, '6+ years');
INSERT INTO public.age (id, age) VALUES (96, '6-9 months');
INSERT INTO public.age (id, age) VALUES (97, '6-9 months, 12-18 months');
INSERT INTO public.age (id, age) VALUES (98, '6-9 months, 9-12 months');
INSERT INTO public.age (id, age) VALUES (99, '6-9 months, 9-12 months, ');
INSERT INTO public.age (id, age) VALUES (100, '7+ years');
INSERT INTO public.age (id, age) VALUES (101, '8 - 30 months');
INSERT INTO public.age (id, age) VALUES (102, '8+ years');
INSERT INTO public.age (id, age) VALUES (103, '9 - 36 months');
INSERT INTO public.age (id, age) VALUES (104, '9-12 months');
INSERT INTO public.age (id, age) VALUES (105, '9-12 months, 12-18 months');
INSERT INTO public.age (id, age) VALUES (106, 'Exempt (babies)');
INSERT INTO public.age (id, age) VALUES (107, 'G rating');
INSERT INTO public.age (id, age) VALUES (108, 'Parents');
INSERT INTO public.age (id, age) VALUES (109, 'PG rating');
INSERT INTO public.age (id, age) VALUES (110, 'Infant');

INSERT INTO public.category VALUES (69, 'AP', 'Active Play', 1);
INSERT INTO public.category VALUES (70, 'C', 'Construction', 1);
INSERT INTO public.category VALUES (71, 'E', 'Educational', 1);
INSERT INTO public.category VALUES (72, 'M', 'Musical', 1);
INSERT INTO public.category VALUES (73, 'P', 'Pretend Play', 1);

INSERT INTO public.city VALUES (1, 'Albion', '3020', 'No');

INSERT INTO public.condition VALUES (1, 'Excellent');
INSERT INTO public.condition VALUES (2, 'Good');
INSERT INTO public.condition VALUES (3, 'Fair');
INSERT INTO public.condition VALUES (4, 'Poor');
INSERT INTO public.condition VALUES (5, 'Very Poor');

INSERT INTO public.contact_log VALUES (1, 'email', 'Template detail can be found in Templates. <br>', NULL, 235, NULL, 'due_to_expire', 'Membership Reminder: Membership is Due to Expire', 'kyme1@outlook.com', '2016-08-14 11:15:09.809+10');
INSERT INTO public.contact_log VALUES (2, 'email', ' **** Due to Expire Messages have been sent! <br> On behalf of Swan View Toy Library<br><br>email sent to: 235: Kyme PASZKO : kyme1@outlook.com Expires:  05-09-2016 : Success<br>', NULL, 0, NULL, 'due_to_expire', 'REPORT: Due To Expire Reminders sent for Swan View Toy Library', ' svtlcommittee@gmail.com', '2016-08-14 11:15:09.883363+10');

INSERT INTO public.discovery VALUES (10, 'Neighbour');
INSERT INTO public.discovery VALUES (13, 'Playgroup');
INSERT INTO public.discovery VALUES (28, 'Previous Member');
INSERT INTO public.discovery VALUES (37, 'Member');
INSERT INTO public.discovery VALUES (39, 'Newspaper');
INSERT INTO public.discovery VALUES (40, 'Other');


INSERT INTO public.files VALUES (2, '2.jpg', 'not open', 'jpg', '06:28:26+10', 'public');

INSERT INTO public.homepage VALUES (1, '<p>&nbsp;</p>
<p><em><strong><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://albion.mibase.com.au/toy_images/albion/news/2.jpg\" alt=\"not open\" width=\"200\" height=\"133\" /></strong></em></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p class=\"p1\"><span class=\"s1\">&nbsp;</span></p>', '<p style=\"text-align: left;\"><strong>Opening hours:&nbsp;</strong></p>
<p>Not Open Yet</p>
<p>&nbsp;</p>', '2016-08-19', '<p class=\"p1\"><span class=\"s1\"><strong>Address: <br /></strong></span></p>
<p class=\"p1\"><span class=\"s1\"><strong>Contact:&nbsp; EMAIL</strong>&nbsp;</span></p>
<p class=\"p3\"><span class=\"s2\">&nbsp;</span></p>
<p class=\"p1\"><span class=\"s1\"><strong>FACEBOOK</strong></span></p>
<p class=\"p3\">&nbsp;</p>
<p>&nbsp;</p>
<p class=\"p3\">&nbsp;</p>', NULL, '<p>condtions of membership here</p>', '<p>Please pay for your membership by direct deposit with the following bank account:</p>
<p>Or you can direct deposit your renewal fees to the following bank account:</p>
<p class=\"p1\"><span class=\"s1\">BSB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br /></span></p>
<p class=\"p1\"><span class=\"s1\">Account name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br /></span></p>
<p class=\"p1\"><span class=\"s1\">Account number&nbsp;&nbsp;&nbsp;&nbsp; <br /></span></p>
<p>&nbsp;</p>', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);


INSERT INTO public.manufacturer VALUES (1, 'Donated', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (2, 'Dressupasaurus', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (3, 'Edex Toys', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (4, 'Every Educaid', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (5, 'Farmers', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (6, 'Kmart', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (7, 'Leap Frog', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (8, 'Paper Plus', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (9, 'Pinegrove', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (10, 'Toy Express', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (11, 'Toy World', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (12, 'Warehouse', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (13, 'Whitcoulls', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (14, 'Woolworths', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.manufacturer VALUES (15, 'Zero Hero', NULL, NULL, NULL, NULL, NULL, 0.0);

INSERT INTO public.membertype VALUES (1, 'Family 12 month', 4, 'Family membership for 12 months', 60.0, 0.00, NULL, NULL, NULL, 0, 12, NULL, 'No', NULL, NULL, NULL, NULL, 0);
INSERT INTO public.membertype VALUES (2, 'Family 6 month', 4, 'Family membership for 6 months', 35.0, 0.00, NULL, NULL, NULL, 0, 6, NULL, 'No', NULL, NULL, NULL, NULL, 0);
INSERT INTO public.membertype VALUES (3, 'Family Concession', 4, 'Family membership for 12 months - Concession Card Holders', 35.0, 0.00, NULL, NULL, NULL, 0, 12, NULL, 'No', NULL, NULL, NULL, NULL, 0);
INSERT INTO public.membertype VALUES (4, 'Volunteer one duty', 4, 'Volunteer Membership 6 months', 20.0, 1.00, NULL, NULL, NULL, 0, 6, NULL, 'No', NULL, NULL, NULL, NULL, 0);
INSERT INTO public.membertype VALUES (5, 'Volunteer two Duties', 4, 'Volunteer Membership 6 months with 2 duties', 15.0, 2.00, NULL, NULL, NULL, 0, 6, NULL, 'No', NULL, NULL, NULL, NULL, 0);
INSERT INTO public.membertype VALUES (6, 'Party Membership', 8, 'DO NOT DELETE', 25.0, 0.00, NULL, 100.0, NULL, 21, 1, NULL, 'No', NULL, 'Yes', NULL, NULL, 0);

INSERT INTO public.nationality VALUES (2, 'Australian', NULL);

INSERT INTO public.notifications VALUES (4, 'email     ', 'overdue', 6, 'Thursday', NULL, NULL, NULL, NULL, 'Yes');
INSERT INTO public.notifications VALUES (2, 'email     ', 'roster', 5, 'Everyday', NULL, NULL, NULL, NULL, 'Yes');

INSERT INTO public.overdue VALUES (1, 'Yes', 'No', 0.00, 0, 'No', 1.00, 0.00, 'No', NULL, 0, 0.00, NULL);

INSERT INTO public.partypack VALUES ('Party Pack 2', 2, 'Suits 2 - 4 year olds. Roller coaster and whirly rocker 
xxx', 'Items in this party pack', 100.00, '2013-07-11');
INSERT INTO public.partypack VALUES ('Party Pack 1', 1, '<p>Suits 1 - 3 year olds. Tunnel and climbing castle</p>', '<p>2 slides</p>
<p>1 table base</p>
<p>1 table top</p>
<p>2 red blocks</p>
<p>2 blue blocks</p>
<p>2 purple blocks</p>
<p>1 table base</p>
<p>1 table top</p>
<p>2 red blocks</p>
<p>2 blue blocks</p>
<p>2 purple blocks</p>', 100.00, '2013-07-11');
INSERT INTO public.partypack VALUES ('Party Pack 3', 3, '<p>Suits 2 - 3 year olds. A set of giant Lego blocks.</p>
<p>So much fun, you&rsquo;ll have to make sure you keep the dads out of the way! xx</p>', '<p>Items in this party pack</p>', 200.00, '2013-07-11');


INSERT INTO public.reports VALUES (2, 'MemberContactReport', 'member', NULL, NULL);
INSERT INTO public.reports VALUES (5, 'Roster', 'roster', NULL, NULL);
INSERT INTO public.reports VALUES (6, 'Toys Borrowed Today', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (7, 'Toys Returned Today', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (9, 'Debits', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (12, 'overdues', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (13, 'overdues short', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (17, 'Daily Stats', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (20, 'Settings', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (14, 'Membership Type', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (15, 'Expired Members', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (22, 'Current Members order by id', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (23, 'Payments by date range', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (29, 'Current Members by Member Type', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (31, 'Locked Toys', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (30, 'New Toys', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (33, 'Roster Duty Count', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (34, 'Withdrawn Toys', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (35, 'Payments Due', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (16, 'New Members', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (36, 'Renewed Members', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (37, 'Expired Members - Date Range', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (38, 'Buyers Report - Useage Statistics', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (8, 'Payments', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (44, 'Reservations', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (45, 'Member Activity', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (47, 'Toys that need Checking', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (49, 'Stocktake Locked Toys', 'daily', '', NULL);
INSERT INTO public.reports VALUES (50, 'Roster for Members', 'monthly', '', NULL);
INSERT INTO public.reports VALUES (51, 'Member Cards', 'member', '', NULL);
INSERT INTO public.reports VALUES (54, 'All Current Toys', 'excel', 'select * from toys where toy_status = ''ACTIVE'' order by category, id;', 'toys_current');
INSERT INTO public.reports VALUES (56, 'Payments by date range', 'monthly', NULL, NULL);
INSERT INTO public.reports VALUES (57, 'Payments by date range', 'excel', 'select * from journal where datepaid::text >= ''\${start_excel}''  and datepaid::text <=  ''\${end_excel}'' and type=''CR'' order by typepayment, datepaid;', 'payments');
INSERT INTO public.reports VALUES (10, 'BagLabel sw', NULL, NULL, NULL);
INSERT INTO public.reports VALUES (60, 'All Current Members', 'excel', 'SELECT * from borwrs where member_status = ''ACTIVE''', 'mem_current');
INSERT INTO public.reports VALUES (61, 'Current Members', 'daily', NULL, NULL);
INSERT INTO public.reports VALUES (64, 'Members Basic', 'excel', 'select firstname, surname, emailaddress, phone, expired, address, suburb, postcode from borwrs where member_status = ''ACTIVE'' order by expired', 'mem_basic');
INSERT INTO public.reports VALUES (65, 'BagLabel with warnings', 'baglabel', NULL, NULL);
INSERT INTO public.reports VALUES (66, 'BagLabel', 'baglabel', NULL, NULL);



INSERT INTO public.rostertypes VALUES (25, 'Roster', '9.30-11.00am', 1, 2, 'Tuesday', '', 'Yes');
INSERT INTO public.rostertypes VALUES (26, 'Roster', '9.30-11.00am', 1, 3, 'Saturday', '', 'Yes');

INSERT INTO public.settings VALUES ('inlibrary', 'Toy Library', 24, ' Library status for the Toy to the Library location', NULL);
INSERT INTO public.settings VALUES ('show_phone', 'No', 26, 'Show the member mobile phone on Toy search screen', NULL);
INSERT INTO public.settings VALUES ('rentasfine', 'No', 14, 'Do you use the Rent value as the fine amount if the Toy is Overdue', NULL);
INSERT INTO public.settings VALUES ('reserve_from', '', 22, 'Fixed date to reserve toys, leave blank if default to today.', NULL);
INSERT INTO public.settings VALUES ('weekly_fine', 'No', 17, 'Fine per week or per session?', NULL);
INSERT INTO public.settings VALUES ('recordfine', 'No', 4, 'Do you want mibase to record overdue fines', NULL);
INSERT INTO public.settings VALUES ('user_borwrs', 'Moved from:', 28, 'Changes the User defined field label in Members', NULL);
INSERT INTO public.settings VALUES ('idcat_noedit', 'Yes', 29, 'If migrating from Libraratu don''t change the toy number', NULL);
INSERT INTO public.settings VALUES ('overdue', 'OVERDUE', 32, 'Description in list if a Toy is Overdue, e.g. OVERDUE or AVAILABLE', NULL);
INSERT INTO public.settings VALUES ('days_grace', '0', 34, 'Days Grace', NULL);
INSERT INTO public.settings VALUES ('export_toys', 'id, category, idcat, toyname , alert, date_purchase, manufacturer, storage ', 40, 'Columns to export when exporting toys, seperated by a comma.', 'textarea');
INSERT INTO public.settings VALUES ('chargerent', 'No', 12, 'Charge rent for toy hire', NULL);
INSERT INTO public.settings VALUES ('new_toys', 'Yes', 41, 'Display new toys on index page', NULL);
INSERT INTO public.settings VALUES ('days_overdue', '5', 43, 'Email Reminder is sent > 0 days overdue', NULL);
INSERT INTO public.settings VALUES ('format_toyid', 'No', 3, 'Numbering Toys with leading zeros, e.g. A001', NULL);
INSERT INTO public.settings VALUES ('member_alerts', 'Yes', 48, 'Activate member alerts for emails', NULL);
INSERT INTO public.settings VALUES ('new_member_email', 'Yes', 51, 'Sends email when a member joins', NULL);
INSERT INTO public.settings VALUES ('reservations', 'Yes', 45, 'Activate reservations', NULL);
INSERT INTO public.settings VALUES ('toy_reserve', 'No', 13, 'Do you use Toy Reservations', NULL);
INSERT INTO public.settings VALUES ('reserve_fee', '0', 27, 'Does your Library charge a fee for Toy Reservations, 0 = No', NULL);
INSERT INTO public.settings VALUES ('mem_reserves', 'No', 47, 'Allow Members to do their own reservations', NULL);
INSERT INTO public.settings VALUES ('automatic_debit_off', 'Yes', 52, NULL, NULL);
INSERT INTO public.settings VALUES ('checked', 'No', 50, 'Add checked button to loans', NULL);
INSERT INTO public.settings VALUES ('partypack', 'No', 18, 'Do you have a party pack for hire?', NULL);
INSERT INTO public.settings VALUES ('timezone', 'AWST', 25, 'time zone', NULL);
INSERT INTO public.settings VALUES ('reuse_toyno', 'Yes', 115, NULL, NULL);
INSERT INTO public.settings VALUES ('password', 'mibase', 16, NULL, NULL);
INSERT INTO public.settings VALUES ('renew_button', 'Yes', 130, 'Include Renew Button in Edit member mode', NULL);
INSERT INTO public.settings VALUES ('loanperiod', '21', 21, 'Toy Loan Period, also Reservation period in days.', NULL);
INSERT INTO public.settings VALUES ('receipt_printer', 'Yes', 100, 'Enables direct printing to receipt printer', NULL);
INSERT INTO public.settings VALUES ('mem_times_renew', '0', 44, 'No of times member can renew', NULL);
INSERT INTO public.settings VALUES ('reserve_period', '8', 23, 'Reserve Period - No days the toy is reserved', NULL);
INSERT INTO public.settings VALUES ('receipt_email', 'Yes', 150, 'Add Email Receipt functionality to loans, Set to Yes to switch on email loan receipts', NULL);
INSERT INTO public.settings VALUES ('open_hours', 'Opening hours: 

Tuesday 9:30-11:00am
Saturday 9:30-11:00am', 31, 'Toy Library Open Hours, to print on bag label', 'textarea');
INSERT INTO public.settings VALUES ('times_renew', '1', 33, 'Number of times member is allowed to renew a toy', NULL);
INSERT INTO public.settings VALUES ('rent', '0', 15, 'Rent amount for all Toys', NULL);
INSERT INTO public.settings VALUES ('fine_value', '0', 20, 'Weekly or session fine amount', NULL);
INSERT INTO public.settings VALUES ('online_rego', 'Yes', 60, 'Activate online registration', NULL);
INSERT INTO public.settings VALUES ('catsort', 'No', 1, 'Does your Toy Library use the category in the Toy Number?', NULL);
INSERT INTO public.settings VALUES ('address', 'address here', 19, 'Toy Library Address for Bag Label and reports', 'textarea');
INSERT INTO public.settings VALUES ('email_from', 'michelle@mibase.com.au', 42, 'Email reply address', NULL);
INSERT INTO public.settings VALUES ('loan_receipt', 'Toy Library Website
Go to 

', 30, 'Comments to print on the loan receipt', 'textarea');
INSERT INTO public.settings VALUES ('multi_location', 'No', 151, 'Does this Toy Library have another branch?', NULL);
INSERT INTO public.settings VALUES ('libraryname', 'Template Toy Library', 2, 'Toy Library Name', NULL);
INSERT INTO public.settings VALUES ('mem_coord_off', 'Yes', 55, 'Exclude Coordinator from Roster in Members', NULL);
INSERT INTO public.settings VALUES ('holiday_due', '', 145, 'Setting to set due date for all the toys when the toy library opens after the holidays', NULL);
INSERT INTO public.settings VALUES ('hide_value', 'Yes', 200, 'Hides the total value of toys loaned by this member in loans page', NULL);
INSERT INTO public.settings VALUES ('disable_cc_email', 'Yes', 301, 'Do not send CC email to Library for loan receipts.', NULL);
INSERT INTO public.settings VALUES ('auto_approve_roster', 'No', 302, 'Auto approve Roster duties from member login.', NULL);
INSERT INTO public.settings VALUES ('online_roster_pref', 'No', 304, 'Add Roster Preferences to Online signup form.', NULL);
INSERT INTO public.settings VALUES ('online_renew', 'No', 305, 'Allow online membership renewals.', NULL);
INSERT INTO public.settings VALUES ('member_update', 'Yes', 306, 'Allow email membership updates.', NULL);
INSERT INTO public.settings VALUES ('exclude_mem_lote', 'No', 308, 'Exclude Member Nationality from online signup form', NULL);
INSERT INTO public.settings VALUES ('disable_auto_report', 'No', 309, 'Dont send Summary reports for overdue and due toys reports.', NULL);
INSERT INTO public.settings VALUES ('mobile_login_off', 'No', 310, 'Turn off Mobile Login on website menu.', NULL);
INSERT INTO public.settings VALUES ('hiretoys_label', 'Hire Toys', 311, 'Customize Hire toys label and link.', NULL);
INSERT INTO public.settings VALUES ('email_second_contact', 'No', 312, 'Emails second contact for all email reminders and bulk emailing.', NULL);
INSERT INTO public.settings VALUES ('online_wwc', 'No', 313, 'Add Working with children card number to Online signup form', NULL);
INSERT INTO public.settings VALUES ('online_id', 'No', 314, 'Add licence or concession card id to Online signup form', NULL);
INSERT INTO public.settings VALUES ('online_hire', 'No', 315, '	Allow the Party Packs or Jumping Castles to be hired by non-members.', NULL);
INSERT INTO public.settings VALUES ('state', 'VIC', 316, 'Default State in Australia.', NULL);
INSERT INTO public.settings VALUES ('suburb_title', 'Suburb: ', 317, 'Default Suburb Title.', NULL);
INSERT INTO public.settings VALUES ('online_second_contact', 'No', 318, 'Add Second Contact to Online signup form.', NULL);
INSERT INTO public.settings VALUES ('online_children', 'No', 319, 'Add Children to Online signup form.', NULL);
INSERT INTO public.settings VALUES ('online_help_off', 'Yes', 321, 'Exclude How can I help from Online signup form.', NULL);
INSERT INTO public.settings VALUES ('mem_levy', '0', 322, 'Charge Members a levy.', NULL);
INSERT INTO public.settings VALUES ('overdue_fine', 'No', 323, 'New fine calculations.', NULL);
INSERT INTO public.settings VALUES ('loan_restrictions', 'No', 324, 'Apply Loan restrictions.', NULL);
INSERT INTO public.settings VALUES ('hiretoys_label_button', 'Hire Toy', 325, 'Label for Hire Toy Button in Member login.', NULL);
INSERT INTO public.settings VALUES ('free_rent_btn', 'No', 326, 'Switch off rent charge in loans.', NULL);
INSERT INTO public.settings VALUES ('vol_missing_off', 'No', 327, 'Switch off missing parts in Loans for volunteer login.', NULL);
INSERT INTO public.settings VALUES ('mem_roster_show_name', 'No', 329, 'Show members name on roster.', NULL);
INSERT INTO public.settings VALUES ('mem_private', 'Yes', 330, 'Show members name on roster.', NULL);
INSERT INTO public.settings VALUES ('online_address', 'Yes', 331, 'Show members address on signup form.', NULL);
INSERT INTO public.settings VALUES ('online_findus', 'Yes', 332, 'Show Find Us on signup form.', NULL);
INSERT INTO public.settings VALUES ('online_membertype', 'Yes', 333, 'Show Member Category on signup form.', NULL);
INSERT INTO public.settings VALUES ('renew_loan_period', '0', 334, 'Renew period if differnet from loan period, leave as 0 if not.', NULL);
INSERT INTO public.settings VALUES ('online_conduct', 'No', 335, 'Include Code of Conduct in member signup form.', NULL);
INSERT INTO public.settings VALUES ('show_desc', 'No', 336, 'Show Piece Description in Loans Page.', NULL);
INSERT INTO public.settings VALUES ('mem_self_checkout', 'No', 337, 'Allow members to loan and return toys.', NULL);
INSERT INTO public.settings VALUES ('toy_holds', 'No', 338, 'Allow members to place toys on hold.', NULL);
INSERT INTO public.settings VALUES ('toy_hold_period', '0', 339, 'Number of days to pick up toys on hold.', NULL);
INSERT INTO public.settings VALUES ('online_gift', 'No', 340, 'Online Membership Gift Cards.', NULL);
INSERT INTO public.settings VALUES ('mem_loanperiod', '0', 341, 'Member login renew loan period.', NULL);
INSERT INTO public.settings VALUES ('holiday_override', 'No', 342, 'Override loan period when the toy library is closed over the holdiays.', NULL);
INSERT INTO public.settings VALUES ('roster', 'Yes', 343, 'Include Roster system.', NULL);
INSERT INTO public.settings VALUES ('online_helmet_waiver', 'No', 344, 'Include Helmet waiver in online signup form.', NULL);
INSERT INTO public.settings VALUES ('toy_hold_fee', '0', 345, 'Value as an integer for fee to charge member for adding a hold to a toy.', NULL);
INSERT INTO public.settings VALUES ('menu_font_color', 'yellow', 346, 'Menu color for home page.', NULL);
INSERT INTO public.settings VALUES ('menu_color', '#900C3F', 347, 'Menu color for home page.', NULL);
INSERT INTO public.settings VALUES ('mem_toy_holds_off', 'No', 348, 'Disable Holds in member login.', NULL);
INSERT INTO public.settings VALUES ('gtmcode', 'No', 349, 'Google Code', NULL);
INSERT INTO public.settings VALUES ('system_alert', 'No', 350, 'Enable System alerts for communication, appear in home page.', NULL);
INSERT INTO public.settings VALUES ('menu_member_options', 'No', 351, 'Enable Menu item menu options on website.', NULL);
INSERT INTO public.settings VALUES ('menu_faq', 'No', 352, 'Enable Menu item faq on website.', NULL);
INSERT INTO public.settings VALUES ('sponsor', 'No', 353, 'Enable Menu item sponsor on website.', NULL);
INSERT INTO public.settings VALUES ('upgrade', 'No', 354, 'Update to access the New returns and Loans Pages.', NULL);
INSERT INTO public.settings VALUES ('library_heading_off', 'No', 355, 'Home page default Toy Library name switched off.', NULL);
INSERT INTO public.settings VALUES ('rounddown_week', 'No', 357, 'days overdue is rounded down to the nearest week.', NULL);
INSERT INTO public.settings VALUES ('roundup_week', 'No', 356, 'Fine is charged if OVER a week late.', NULL);

INSERT INTO public.stats VALUES (312, 1, 1, 0, '2016-08-17', 'Australia/ACT', 3, 33, 0, 0, 'TOT', 0, 1, 'Wednesday', 1, 0, 0, 0);
INSERT INTO public.stats VALUES (313, 2, 1, 0, '2017-06-28', 'Australia/ACT', 3, 26, 0, 0, 'TOT', 0, 1, 'Wednesday', 1, 0, 0, 0);

INSERT INTO public.storage VALUES (1, 'Zip Lock Bag');

INSERT INTO public.stype VALUES (1, 'Gold Star', NULL, 0.00, 0.00);

INSERT INTO public.suburb VALUES (1, 'Perth');

INSERT INTO public.supplier VALUES (1, 'Donated', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.supplier VALUES (2, 'Edex Toys', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.supplier VALUES (3, 'Farmers', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.supplier VALUES (4, 'MTA', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.supplier VALUES (5, 'The Warehouse', 'High Streer', '033', '0333', 'warehouse@nz.com', 'Dave', 0.0);
INSERT INTO public.supplier VALUES (6, 'Toy Conference', NULL, NULL, NULL, NULL, NULL, 0.0);
INSERT INTO public.supplier VALUES (7, 'ToyWorld', NULL, NULL, NULL, NULL, NULL, 0.0);

INSERT INTO public.template VALUES (44, '19:41:58.43766+10', 'Membership Application Received', '<p>Dear [firstname],</p>
<p>Thank you for your membership application with the Albion and Friends Toy Library.</p>
<p>We will be in touch shortly.</p>
<p>Paying your fees - cash payments only accepted.</p>
<p>Full Fee: \$60 full year/\$35 half year.</p>
<p>Health Care (HCC)/Concession Card holder: \$35 full year/\$20 half year.</p>
<p>Volunteer fee - per half year <br /> \$20 (\$15 conc/HCC) with one 90-minute duty<br /> \$15 (\$10 conc/HCC) with two 90-minute duties OR committee member</p>
<p>&nbsp;</p>
<p>Thanks very much,<br />The Albion and Friends Toy Library team.</p>
<p>&nbsp;</p>', 'approve', '', 'PROCESS');
INSERT INTO public.template VALUES (3, '15:13:54.826532+10', 'ROSTER ALERT', '', 'roster_alert', '', 'AUTO');
INSERT INTO public.template VALUES (1, '20:05:08.285351+10', 'OVERDUE TOYS  REMINDER', '<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Dear member, </span></p>
<p class=\"MsoNormal\">&nbsp;</p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">[toylist]</span></p>
<p class=\"MsoNormal\">&nbsp;</p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">We have noticed that your toys are well overdue now and would really appreciate a prompt return to allow other Toy Library members to use these toys! If there is anything that makes you unable to come, please get in touch by email or facebook.</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Thanks very much,</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Toy Library Committee.</span></p>
<p>&nbsp;</p>
<p><br />All toys to be complete, cleaned &amp; dry before return. Thank you.</p>
<p class=\"MsoNormal\">&nbsp;</p>
<p class=\"MsoNormal\">&nbsp;</p>
<p class=\"MsoNormal\">&nbsp;</p>', 'overdue', '', 'AUTO');
INSERT INTO public.template VALUES (9, '21:28:30.390976+10', 'Email Receipt from xx Toy Library', '<h1 style=\"text-align: left;\">xx TOY LIBRARY</h1>
<h2>LOAN ISSUE RECEIPT:&nbsp;&nbsp; Issued: [now]</h2>
<h2 style=\"text-align: left;\"><strong>[borid]: [longname]</strong></h2>
<p style=\"text-align: left;\"><strong>expires: [expired] :&nbsp; &nbsp; Account Balance:&nbsp; [balance]</strong></p>
<p style=\"text-align: left;\">[toylist]</p>
<p>&nbsp;</p>
<p style=\"text-align: left;\"><strong>Opening hours:&nbsp;</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>', 'email_receipt', '', 'BUTTON');
INSERT INTO public.template VALUES (7, '14:35:34.556565+11', 'Membership Expired: Your Membership has Expired', '<p>Oh no! Your membership with the Toy Library has expired! :(</p>
<p>To renew your membership please come in and we`ll have you renewed in no time!</p>
<p>&nbsp;</p>', 'expired', '', 'AUTO');
INSERT INTO public.template VALUES (21, '08:44:32.437941+10', 'Birthday Wishes from [libraryname]', '<p><em><span style=\"color: #000080;\"><strong>Dear [childname],</strong></span></em><br /><br /><span style=\"color: #000080;\"><em><strong>Happy Birthday from the Swan View Toy Library!</strong></em></span></p>
<p><span style=\"color: #000080;\"><em><strong>We look forward to seeing you soon.</strong></em></span></p>
<p>&nbsp;</p>
<p style=\"text-align: left;\"><strong>Opening hours:&nbsp;</strong></p>
<p><strong>Tuesday &nbsp;9:30-11:00am</strong><br /><strong>Saturday&nbsp;<strong>9:30-11:00</strong>am</strong></p>
<p>Closed for Easter, Public Holidays<br />and Christmas Holidays (13th December 2016 - February 3rd 2017)</p>
<p><br /><strong>EMAIL</strong> <br /><a href=\"mailto:svtlcommittee@gmail.com\">mailto:toylibrary@gmail.com</a>&nbsp;</p>
<p><strong>FACEBOOK</strong><br /><a href=\"https://www.facebook.com/ToyLibrary/\">https://www.facebook.com/ToyLibrary/</a></p>
<p><strong>WEB</strong><br /><a href=\"http://toylibrary.wixsite.com/toylibrary\">http://toylibrary.wixsite.com/toylibrary</a></p>
<p>Address: Brown Park Community Centre, Salisbury Road, SWAN VIEW, WA, 6056.</p>', 'birthday', '', 'AUTO');
INSERT INTO public.template VALUES (30, '15:15:03.420615+11', 'Loan receipt for receipt printer', '<h1 style=\"text-align: left;\">TOY LIBRARY</h1>
<h2>LOAN ISSUE RECEIPT:&nbsp;&nbsp; Issued: [now]</h2>
<h2 style=\"text-align: left;\"><strong>[borid]: [longname]</strong></h2>
<p style=\"text-align: left;\"><strong>expires: [expired] :&nbsp; &nbsp; Account Balance:&nbsp; [balance]</strong></p>
<p style=\"text-align: left;\">[toylist]</p>
<p>&nbsp;</p>
<p style=\"text-align: left;\"><strong>Opening hours:&nbsp;</strong></p>
<p>&nbsp;</p>', 'loan_receipt', '', 'BUTTON');
INSERT INTO public.template VALUES (20, '13:16:50.125469+10', 'Username and Password for the [libraryname]', '<p>Dear [firstname],<br /><br />Your Username and password has been reset.<br /><br />To Login go to xxx.mibase.com.au - login - members&nbsp;with the following username and password.<br /><br />Username: <span style=\"color: blue;\">[username]</span><br />Password: <span style=\"color: blue;\">[pwd]</span><br /><br /><span style=\"color: red;\"> This is an automated message, please do not reply</span><br /><br /><br /></p>
<p style=\"text-align: left;\"><strong>Opening hours:&nbsp;</strong></p>
<p>&nbsp;</p>', 'login', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (5, '17:32:28.897799+11', 'Welcome to the xxxx Toy Library!', '<p><em><strong>Welcome! Thank you so much for joining.</strong></em></p>
<p>We are so excited to have you on board and really look forward to seeing you at the Albion and Friends Toy Library.</p>
<p style=\"text-align: left;\"><strong>Opening hours:&nbsp;</strong></p>
<p><br /><strong>Saturday&nbsp;<strong>9:30-11:00</strong>am</strong></p>
<p>Closed for Easter, Public Holidays<br />and Christmas Holidays (13th December 2016 - February 3rd 2017)</p>
<p><strong>EMAIL</strong> <br /><br /></p>
<p><strong>FACEBOOK</strong></p>
<p>Address:</p>
<p>&nbsp;</p>
<p>Please read our&nbsp; here.</p>
<p>&nbsp;</p>
<p>Welcome Information &nbsp;Brochure -&nbsp;</p>
<p>Waiver, Release and Indemnity for Bicycles and Scooters -&nbsp;</p>', 'new_member', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (6, '06:30:03.908088+11', 'Membership Reminder: Membership is Due to Expire', '<p>Dear <strong>(member)</strong>,</p>
<p>Your membership is due to expire on <strong>(date)</strong>.</p>
<p>We love having you onboard as a member of the xx Toy Library&nbsp;and we really hope that you are able to renew your membership.&nbsp;To renew your membership please come in and we`ll have you renewed in no time!</p>
<p>Or you can direct deposit your renewal fees to the following bank account:</p>
<p class=\"p1\"><span class=\"s1\">BSB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br /></span></p>
<p class=\"p1\"><span class=\"s1\">Account name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br /></span></p>
<p class=\"p1\"><span class=\"s1\">Account number&nbsp;&nbsp;&nbsp;&nbsp; <br /></span></p>
<p>Please use your surname as a reference.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Thank you for being a member of the xx View&nbsp;Toy Library.<br />We appreciate your on going support and look forward to seeing you soon.</p>
<p>Click here to print your&nbsp;<a title=\"Membership Renewal Form\" href=\"https://.mibase.com.au/toy_images//news/4.pdf\" target=\"_blank\">Membership Renewal Form</a>&nbsp;or fill out when you come in next.</p>
<p>If you have any concerns or for some reason unable to renew at this time, please do talk to us.</p>
<p>Thanks very much,<br /><br /></p>
<p>&nbsp;</p>
<p style=\"text-align: left;\"><strong>Opening hours:&nbsp;</strong></p>
<p>&nbsp;</p>', 'due_to_expire', '', 'AUTO');
INSERT INTO public.template VALUES (51, '19:29:12.47922+11', 'Please sign up now for roster duty', 'Content for Please sign up now for roster duty', '1_month_roster_reminder', NULL, 'AUTO');
INSERT INTO public.template VALUES (34, '09:38:06.005714+10', 'Subject for Bulk Email', '<p>use this template to save a draft email</p>', 'bulk_email', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (2, '20:05:13.071686+10', 'ROSTER REMINDER', '<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Dear [firstname],</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">[rosterlist]</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">If you can`t make your rostered day, please let us know in advance so we can assist you in&nbsp;swapping your day with someone else.</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Thank you, your help is much needed and appreciated!</span></p>
<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt;\">Kind regards.</span></p>
<p><span style=\"font-size: 11.0pt;\">Toy Library Committee</span></p>
<p>&nbsp;</p>
<p style=\"text-align: left;\"><strong>Opening hours:&nbsp;</strong></p>
<p>&nbsp;</p>', 'roster', '', 'AUTO');
INSERT INTO public.template VALUES (33, '11:33:57.353187+10', 'Toy Library details update', '<p>Hi [firstname],</p>
<p>&nbsp;</p>
<p>Each year &nbsp;Toy Library checks our member details to ensure our records are up to date. In previous years we have done this via a paper list at sessions, but this year we are trialling a digital approach.</p>
<p>We would appreciate you checking your details below and then indicating if they are correct or require updating.</p>
<p>&nbsp;</p>
<p>If <span style=\"color: #008000;\"><strong>CORRECT</strong></span> please use this link to confirm your details [correct_link]</p>
<p><br />If <span style=\"color: #ff0000;\"><strong>INCORRECT</strong></span>, please reply to this email with the corrected details.</p>
<p>&nbsp;</p>
<p><strong>Name:</strong> [longname]</p>
<p><strong>Member type:</strong> [membertype]</p>
<p><strong>Address:</strong> [address]</p>
<p><strong>Borrowing children`s names:</strong> [children]</p>
<p><strong>Mobile number:</strong> [mobile]</p>
<p><strong>Email address</strong>: [email]</p>
<p>&nbsp;</p>
<p>Kind regards</p>
<p>Bec Lalor</p>
<p><em>&nbsp;Toy Library Coordinator</em></p>
<p><em><img src=\"https://.mibase.com.au/toy_images//news/1.jpg\" alt=\"logo\" width=\"182\" height=\"107\" /></em></p>
<p>&nbsp;</p>', 'member_update', '', 'PROCESS');
INSERT INTO public.template VALUES (52, '19:32:17.89702+11', 'Please sign up now for roster duty', 'Content for Please sign up now for roster duty', '6_month_roster_reminder', NULL, 'AUTO');
INSERT INTO public.template VALUES (100, '16:06:32.540701+10', 'Paypal payments page', '', 'paypal_buttons', '', 'PROCESS');
INSERT INTO public.template VALUES (110, '16:32:13.064668+10', 'Test Bulk Email', '', 'test_email', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (112, '16:06:31.364256+10', 'Gift Card Paypal Payments Page', '<table style=\"height: 98px;\" width=\"652\">
<tbody>
<tr>
<td colspan=\"3\">
<p><strong>Pay for your Gift Card Using PayPal. Alternatively you can pay in cash at the [Libraryname]<br /></strong></p>
<p><strong>Please Note a Pay Pal fee of \$0.95 will be incurred </strong></p>
<p>&nbsp;</p>
<p><strong><span style=\"color: #0000ff;\">Picture of a Gift Card</span><br /></strong></p>
</td>
</tr>
</tbody>
</table>
<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" target=\"paypal\">
<p><input name=\"cmd\" type=\"hidden\" value=\"_s-xclick\" /> <input name=\"hosted_button_id\" type=\"hidden\" value=\"93WM884TVSDRQ\" /> <input name=\"on0\" type=\"hidden\" value=\"Select a Gift Card Option\" /></p>
<p>&nbsp;</p>
<p><span style=\"color: #0000ff;\">Paypal Button</span></p>
</form>', 'paypal_gift', '', 'PROCESS');
INSERT INTO public.template VALUES (113, '19:41:56.954366+10', 'Gift Card Application Received', '<p>Dear [p_name],</p>
<p>Thank you for your Gift Card application with the [libraryname].</p>
<p>Your reference number for this gift card is: [id].</p>
<p>One of our committee will be in touch ASAP to finalise payment.</p>
<p>&nbsp;</p>
<p>regards,</p>
<p>&nbsp;</p>
<p>[libraryname]</p>', 'approve_gift', '', 'PROCESS');
INSERT INTO public.template VALUES (111, '08:47:50.112366+10', 'Gift Card from [Libraryname]', '<p>Dear [p_name],</p>
<p>Thank you for purchasing a Gift Card from [Libraryname]. As part of this electronic receipt you will note below is the Gift Card you can print and provide to the recipient.</p>
<p>Thank you for supporting the [Libraryname].</p>
<p>
<p><span style=\"color: #0000ff;\">Picture of a Gift Card</span></p>
<p>&nbsp;</p>
<p>This Gift Card entitles the recipient to a <span style=\"color: #0000ff;\">xxx</span> Month Toy Library Membership, or<span style=\"color: #0000ff;\"> \$xx</span> off the cost of a full membership, terms and conditions apply.</p>
<p>Please present this to the [Libraryname] to redeem your membership.</p>
<ol>
<li>This Gift Certificate is not redeemable for cash.</li>
<li>This Gift Certificate is non-transferable and resale is prohibited.</li>
<li>If a Gift Certificate is lost, stolen, destroyed or used without permission, a replacement will not be provided in these circumstances.</li>
<li>This Gift Certificate is Valid until <strong>[expired]</strong> unless otherwise discussed and approved with [Libraryname].</li>
</ol>
<p>___________________________________________________________________________________</p>
<p><strong>Admin Only</strong></p>
<p>Gift Card Number:&nbsp; [id]</p>
<p>&nbsp;</p>
<p><span style=\"border-radius: 2px; text-indent: 20px; width: auto; padding: 0px 4px 0px 0px; text-align: center; font: bold 11px/20px &#96;Helvetica Neue&#96;,Helvetica,sans-serif; color: #ffffff; background: #bd081c  no-repeat scroll 3px 50% / 14px 14px; position: absolute; opacity: 1; z-index: 8675309; display: none; cursor: pointer; top: 140px; left: 18px;\">Save</span></p>
<p><span style=\"border-radius: 2px; text-indent: 20px; width: auto; padding: 0px 4px 0px 0px; text-align: center; font: bold 11px/20px &#96;Helvetica Neue&#96;,Helvetica,sans-serif; color: #ffffff; background: #bd081c  no-repeat scroll 3px 50% / 14px 14px; position: absolute; opacity: 1; z-index: 8675309; display: none; cursor: pointer; top: 140px; left: 18px;\">Save</span></p>', 'gift_voucher', '', 'PROCESS');
INSERT INTO public.template VALUES (115, '11:19:50.982633+11', 'Payments Receipt - By Email', '[payments]', 'Payments_Email', '', 'PROCESS');
INSERT INTO public.template VALUES (120, '10:00:10.670672+10', 'Missing Parts Today.', 'Dear [longname]

Losing pieces is a normal part of Toy Library borrowing, and not something we want our members to be unduly worried about as we have systems to manage this process.  This email serves as a reminder for you to keep an eye out at home for the following pieces
[missing]
The Toy Library refunds \$xx for each piece returned, and of course we appreciate your assistance in keeping the pieces of our toys, puzzles and games together.  Please chat to one of our friendly staff team members if you have any concerns.
Regards', 'missing', '', 'BULK_EMAIL');
INSERT INTO public.template VALUES (121, '15:54:51.379015+10', 'Reservation Reminder.', '[reserve_list]', 'reserve', '', 'PROCESS');
INSERT INTO public.template VALUES (122, '15:56:50.472716+10', 'Your Toy Hold is ready to be collected.', '[toy]', 'hold_ready', '', 'PROCESS');
INSERT INTO public.template VALUES (123, '16:02:22.023731+10', 'Please return toy [idcat], it has been requested by another member.', '[toy]', 'hold_return', '', 'PROCESS');



INSERT INTO public.typeevent VALUES (1, 'Roster Alert', 1, 'ROSTER ALERT will be sent by automatic Email', 0);
INSERT INTO public.typeevent VALUES (2, 'Helmet', 1, 'Member has signed the Helmet waiver', 0);
INSERT INTO public.typeevent VALUES (4, 'Levy Roster', 2, 'No Roster duty and debit Duty Levy                ', 45);

INSERT INTO public.typepart VALUES (1, 'Broken', NULL, NULL);
INSERT INTO public.typepart VALUES (2, 'Fixed', NULL, NULL);
INSERT INTO public.typepart VALUES (3, 'Found', NULL, NULL);
INSERT INTO public.typepart VALUES (4, 'Goldstar', NULL, NULL);
INSERT INTO public.typepart VALUES (5, 'Missing', NULL, NULL);
INSERT INTO public.typepart VALUES (6, 'Replaced', NULL, NULL);
INSERT INTO public.typepart VALUES (7, 'Spare', NULL, NULL);
INSERT INTO public.typepart VALUES (8, 'Warning', NULL, NULL);
INSERT INTO public.typepart VALUES (10, 'Alert', NULL, NULL);
INSERT INTO public.typepart VALUES (21, 'Small Parts', 'This Toy has Small Parts', 'small_parts');
INSERT INTO public.typepart VALUES (22, 'Batteries', 'This Toy requires batteries', 'batteries');
INSERT INTO public.typepart VALUES (23, 'Small Balls', 'This Toy has Small Balls', 'small_balls');
INSERT INTO public.typepart VALUES (24, 'Magnets', 'This Toy has Magnets', 'magnets');
INSERT INTO public.typepart VALUES (25, 'Wash', 'This Costume requires washing', 'wash');

INSERT INTO public.typepayment VALUES (1, 'Cash');
INSERT INTO public.typepayment VALUES (2, 'Online');

INSERT INTO public.users VALUES ('template', 'JK7AZt3Y8rJ4HQy', 1, 'admin', 'template', 'template', 2487, false, NULL, NULL, '2018-05-07', 'admin');

INSERT INTO public.version VALUES (1, '1', 1);

INSERT INTO public.volunteer VALUES (1, 'Toy Purchasing and Processing');
INSERT INTO public.volunteer VALUES (2, 'Fundraising and Events');
INSERT INTO public.volunteer VALUES (3, 'Marketing and Communications');
INSERT INTO public.volunteer VALUES (4, 'Distributing posters to community businesses');
INSERT INTO public.volunteer VALUES (5, 'Administration and Finance');
INSERT INTO public.volunteer VALUES (6, 'Website and IT Support');
INSERT INTO public.volunteer VALUES (7, 'I want to help but I`m not sure how! Please contact me');

SELECT pg_catalog.setval('public.age_id_seq', 163, true);

SELECT pg_catalog.setval('public.bor_id_seq', 1, false);

SELECT pg_catalog.setval('public.category_id_seq', 73, true);

SELECT pg_catalog.setval('public.child_id_seq', 1, false);

SELECT pg_catalog.setval('public.city_id_seq', 15, true);

SELECT pg_catalog.setval('public.condition_id_seq', 1, false);

SELECT pg_catalog.setval('public.contact_log_id_seq', 1, false);

SELECT pg_catalog.setval('public.discovery_id_seq', 42, true);

SELECT pg_catalog.setval('public.event_id_seq', 1, false);

SELECT pg_catalog.setval('public.files_id_seq', 1, false);

SELECT pg_catalog.setval('public.gift_cards_sq', 1, false);

SELECT pg_catalog.setval('public.holds_id_seq', 126, false);

SELECT pg_catalog.setval('public.id_seq_users', 1, false);

SELECT pg_catalog.setval('public.journal_id_seq', 1, false);

SELECT pg_catalog.setval('public.log_id_seq', 1, false);

SELECT pg_catalog.setval('public.manufacturer_id_seq', 15, true);

SELECT pg_catalog.setval('public.membertype_id_seq', 124, true);

SELECT pg_catalog.setval('public.nation_id_seq', 1, true);

SELECT pg_catalog.setval('public.parts_id_seq', 1, false);

SELECT pg_catalog.setval('public.partypack_id_seq', 116, true);

SELECT pg_catalog.setval('public.paymentoptions_id_seq', 2, true);

SELECT pg_catalog.setval('public.postcode_id_seq', 1, false);

SELECT pg_catalog.setval('public.report_id_seq', 10, true);

SELECT pg_catalog.setval('public.reserve_id_seq', 126, true);

SELECT pg_catalog.setval('public.reserve_toy_id_seq', 759, true);

SELECT pg_catalog.setval('public.roster_id_seq', 124, true);

SELECT pg_catalog.setval('public.rostertypes_id_seq', 1, false);

SELECT pg_catalog.setval('public.select_id_seq', 1, false);

SELECT pg_catalog.setval('public.settings_id_seq', 19, true);

SELECT pg_catalog.setval('public.stats_id_seq', 313, true);

SELECT pg_catalog.setval('public.storage_id_seq', 1, false);

SELECT pg_catalog.setval('public.sub_category_id_seq', 1, false);

SELECT pg_catalog.setval('public.supplier_id_seq', 7, true);

SELECT pg_catalog.setval('public.template_id_seq', 2, true);

SELECT pg_catalog.setval('public.toy_id_seq', 20660, true);

SELECT pg_catalog.setval('public.transaction_id_seq', 6487, true);

SELECT pg_catalog.setval('public.typeevent_id_seq', 1, true);

SELECT pg_catalog.setval('public.typepart_id_seq', 8, true);

SELECT pg_catalog.setval('public.typepayment_id', 8, true);

SELECT pg_catalog.setval('public.version_id', 1, false);

SELECT pg_catalog.setval('public.volunteer_id_seq', 10, true);

SELECT pg_catalog.setval('public.warning_id', 1, false);

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (category);

ALTER TABLE ONLY public.children
    ADD CONSTRAINT child_pkey PRIMARY KEY (childid);

ALTER TABLE ONLY public.city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.borwrs
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.condition
    ADD CONSTRAINT condition_condition_key UNIQUE (condition);

ALTER TABLE ONLY public.condition
    ADD CONSTRAINT condition_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.contact_log
    ADD CONSTRAINT contact_log_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.discovery
    ADD CONSTRAINT discovery_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.files
    ADD CONSTRAINT files_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.gift_cards
    ADD CONSTRAINT gift_cards_id PRIMARY KEY (id);

ALTER TABLE ONLY public.toys
    ADD CONSTRAINT id_cat PRIMARY KEY (category, id);

ALTER TABLE ONLY public.users
    ADD CONSTRAINT id_key PRIMARY KEY (id);

ALTER TABLE ONLY public.nationality
    ADD CONSTRAINT id_nation PRIMARY KEY (id);

ALTER TABLE ONLY public.homepage
    ADD CONSTRAINT id_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.typepayment
    ADD CONSTRAINT id_type PRIMARY KEY (id);

ALTER TABLE ONLY public.toys
    ADD CONSTRAINT idcat UNIQUE (idcat);

ALTER TABLE ONLY public.images
    ADD CONSTRAINT images_id PRIMARY KEY (id);

ALTER TABLE ONLY public.journal
    ADD CONSTRAINT journal_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.loan_restrictions
    ADD CONSTRAINT loan_restrictions_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_id PRIMARY KEY (id);

ALTER TABLE ONLY public.log
    ADD CONSTRAINT log_id PRIMARY KEY (id);

ALTER TABLE ONLY public.manufacturer
    ADD CONSTRAINT manufacturer_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.stats
    ADD CONSTRAINT member_stats_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.membertype
    ADD CONSTRAINT membertype_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.membertype
    ADD CONSTRAINT memtype UNIQUE (membertype);

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.age
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.overdue
    ADD CONSTRAINT overdue_id PRIMARY KEY (id);

ALTER TABLE ONLY public.parts
    ADD CONSTRAINT parts_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.partypack
    ADD CONSTRAINT party_pack_id PRIMARY KEY (id);

ALTER TABLE ONLY public.paymentoptions
    ADD CONSTRAINT paymentoptions_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.pieces
    ADD CONSTRAINT pieces_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.postcode
    ADD CONSTRAINT postcode_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT reports_id PRIMARY KEY (id);

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reserve_id PRIMARY KEY (id);

ALTER TABLE ONLY public.reserve_toy
    ADD CONSTRAINT reserve_toy_id PRIMARY KEY (id);

ALTER TABLE ONLY public.rostertypes
    ADD CONSTRAINT rostertypes_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.selectbox
    ADD CONSTRAINT selectbox_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT setting_pk PRIMARY KEY (setting_name);

ALTER TABLE ONLY public.storage
    ADD CONSTRAINT storage_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.stype
    ADD CONSTRAINT stype_id PRIMARY KEY (id);

ALTER TABLE ONLY public.sub_category
    ADD CONSTRAINT sub_category_pkey PRIMARY KEY (sub_category);

ALTER TABLE ONLY public.suburb
    ADD CONSTRAINT suburb_id PRIMARY KEY (id);

ALTER TABLE ONLY public.supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.template
    ADD CONSTRAINT template_id PRIMARY KEY (id);

ALTER TABLE ONLY public.toy_holds
    ADD CONSTRAINT toy_holds_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT trans_pk PRIMARY KEY (date_loan, borid, idcat);

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT type_name UNIQUE (notification_type, notification_name);

ALTER TABLE ONLY public.typeevent
    ADD CONSTRAINT typeevent_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.typepart
    ADD CONSTRAINT typepart_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.typepart
    ADD CONSTRAINT typepart_typepart_key UNIQUE (typepart);

ALTER TABLE ONLY public.users
    ADD CONSTRAINT username UNIQUE (username);

ALTER TABLE ONLY public.version
    ADD CONSTRAINT version_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.volunteer
    ADD CONSTRAINT volunteer_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.warnings
    ADD CONSTRAINT warnings_id PRIMARY KEY (id);

CREATE INDEX bor_name_index ON public.borwrs USING btree (lower((surname)::text));

CREATE INDEX category_index ON public.category USING btree (upper((category)::text), upper((category)::text));

CREATE INDEX city_index ON public.city USING btree (upper((city)::text), upper((city)::text));

CREATE INDEX condition_index ON public.condition USING btree (upper((condition)::text), upper((condition)::text));

CREATE INDEX discovery_index ON public.discovery USING btree (upper((discovery)::text), upper((discovery)::text));

CREATE INDEX fki_borwrs ON public.event USING btree (memberid);

CREATE INDEX fki_child ON public.children USING btree (id);

CREATE INDEX fki_idcat ON public.parts USING btree (itemno);

CREATE INDEX fki_memtype ON public.borwrs USING btree (membertype);

CREATE INDEX id ON public.toys USING btree (id);

CREATE INDEX id_trans ON public.transaction USING btree (id);

CREATE INDEX membertype_index ON public.membertype USING btree (upper((membertype)::text), upper((membertype)::text));

CREATE INDEX name_index ON public.children USING btree (lower((child_name)::text), lower((child_name)::text));

CREATE INDEX storage_index ON public.storage USING btree (upper((storage)::text), upper((storage)::text));

CREATE INDEX trans_index ON public.transaction USING btree (upper((idcat)::text));

CREATE INDEX type_event_index ON public.event USING btree (upper((typeevent)::text), upper((typeevent)::text));

ALTER TABLE ONLY public.toys
    ADD CONSTRAINT category FOREIGN KEY (category) REFERENCES public.category(category) ON DELETE RESTRICT;

ALTER TABLE ONLY public.children
    ADD CONSTRAINT child FOREIGN KEY (id) REFERENCES public.borwrs(id) ON DELETE RESTRICT;

ALTER TABLE ONLY public.parts
    ADD CONSTRAINT idcat FOREIGN KEY (itemno) REFERENCES public.toys(idcat) ON DELETE RESTRICT;

ALTER TABLE ONLY public.borwrs
    ADD CONSTRAINT memtype FOREIGN KEY (membertype) REFERENCES public.membertype(membertype) ON UPDATE RESTRICT ON DELETE RESTRICT;


";

