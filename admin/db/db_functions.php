<?php
/* 
 * Copyright (C) 2019 Mibase
 *
 * This program is provided under the terms of the GNU General Public License version 3
 * refer licence.html in root folder for full text.
 */

const NoDB = 0;
const EmptyDB = 1;
Const VersionMismatch = 2;
Const NoDataDB = 3;
Const DBwithData = 4;

/**
 * Gets the state of a database.
 * the states  can be:
 *      No Database found
 *      
 * @param type $dbhost
 * @param type $dbport
 * @param type $dbname
 * @param type $dbuser
 * @param type $dbpasswd
 * @param type $code_version_minor
 * @return type
 */
function get_state($dbhost, $dbport, $dbname, $dbuser, $dbpasswd, $code_version_minor) {
    $note = '';
    $db_minor = -1;
    try {
        if (database_exists($dbhost, $dbport, $dbname, $dbuser, $dbpasswd)) {
            $note = "database found OK<br>";

            if (database_not_built($dbhost, $dbport, $dbname, $dbuser, $dbpasswd)) {
                if (table_exists('version', $dbhost, $dbport, $dbname, $dbuser, $dbpasswd)) {
                    $note .= "version table found OK<br>";
                    $db_minor = get_version($dbhost, $dbport, $dbname, $dbuser, $dbpasswd);
                    $note .= "Verions 1." . $db_minor . "<br>";
                } else {
                    $note .= "NO version table found<br>";
                    $db_minor = 0;
                }

                if ($db_minor != $code_version_minor) {
                    $note .= "Version Mismatch code: 1." . $code_version_minor . " database: 1." . $db_minor . "<br>";
                    $state = VersionMismatch;
                } else {
                    $note .= "Versions match code: 1." . $code_version_minor . " database: 1." . $db_minor . "<br>";
                    if (database_has_data($dbhost, $dbport, $dbname, $dbuser, $dbpasswd)) {
                        $note .= "Database has data in it<br>";
                        $state = DBwithData;
                    } else {
                        $note .= "Database appears to be empty of data<br>";
                        $state = NoDataDB;
                    }
                }
            } else {
                $note = "database is empty (ie tables, indexes not setup)<br>";
                $state = EmptyDB;
            }
        } else {
            $state = 0;
            $note = "No database " . $dbname . " was found<br>";
        }
    } catch (Exception $e) {
         $state = 0;
        $note = 'Exception getting DB State' . $e->getMessage() . '<br>';
    }
        $r = array();
        $r['state'] = $state;
        $r['note'] = $note;
        $r['db_minor'] = $db_minor;
        $r['code_minor'] = $code_version_minor;
        return $r;

}

/**
 * Gets the state of a database.
 * the states  can be:
 *      No Database found
 *      
 * @param type $dbhost
 * @param type $dbport
 * @param type $dbname
 * @param type $dbuser
 * @param type $dbpasswd
 * @param type $code_version_minor
 * @return type
 */
function get_toybase_state($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd) {
    $note = '';
    $db_minor = -1;
    if (database_exists($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd)) {
        $note = "toybase database found OK<br>";
        if (table_exists('libraries', $dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd)) {
            $note .= "libraries table found OK<br>";
            $num_lib = toybase_database_number_libraries($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd);
            if ($num_lib > 0) {
                $note = "toybase database has " . $num_lib . " library in it<br>";
                $state = DBwithData;
            } else {
                $note = "toybase database exists but has no libraries in it<br>";
                $state = NoDataDB;
            }
        } else {
            $note = "toybase database is not built (ie tables not setup)<br>";
            $state = EmptyDB;
        }
    } else {
        $state = 0;
        $note = "No toybase database: " . $toybasedbname . " was found<br>";
    }

    $r = array();
    $r['state'] = $state;
    $r['note'] = $note;
    return $r;
}

/**
 * Checks to see if a database exists
 * Note that this check is only that the database exists and odes not check that the schema has been built.
 * ie the database can be empty.
 * @param type $dbhost
 * @param type $dbport
 * @param type $dbname
 * @param type $dbuser
 * @param type $dbpasswd
 * @return boolean
 */
function database_exists($dbhost, $dbport, $dbname, $dbuser, $dbpasswd) {

    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=template1";
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sql = "SELECT datname FROM pg_catalog.pg_database WHERE datname = ? ;";
    $sth = $pdo->prepare($sql);
    $array = array($dbname);
    $sth->execute($array);
    $result = $sth->fetchAll();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        echo "A query error occurred.\n";
        echo $connect_pdo;
        echo 'Error' . $stherr[0] . '<br>';
        echo 'Error' . $stherr[1] . '<br>';
        echo 'Error' . $stherr[2] . '<br>';
    }

    if (sizeof($result) == 1) {
        //we found the database
        return true;
    } else {
        return false;
    }
}

/**
 * Checks to see if a table is in the database
 * @param type $table_name  the table to check exists
 * @param type $dbhost
 * @param type $dbport
 * @param type $dbname
 * @param type $dbuser
 * @param type $dbpasswd
 * @return boolean  true if the table exists faluse otherwise
 */
function table_exists($table_name, $dbhost, $dbport, $dbname, $dbuser, $dbpasswd) {

    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbname;
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sql = "SELECT tablename FROM pg_tables WHERE tablename = ? ;";
    $sth = $pdo->prepare($sql);
    $array = array($table_name);
    $sth->execute($array);
    $result = $sth->fetchAll();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        echo "A query error occurred.\n";
        echo $connect_pdo;
        echo 'Error' . $stherr[0] . '<br>';
        echo 'Error' . $stherr[1] . '<br>';
        echo 'Error' . $stherr[2] . '<br>';
    }

    if (sizeof($result) == 1) {
        //we found the database
        return true;
    } else {
        return false;
    }
}

/**
 * Checks if the database has been built.
 * ie that all the tables indexes sequences etc have been created
 * @param type $dbhost
 * @param type $dbport
 * @param type $dbname
 * @param type $dbuser
 * @param type $dbpasswd
 * @return boolean
 */
function database_not_built($dbhost, $dbport, $dbname, $dbuser, $dbpasswd) {

    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbname;
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sql = "SELECT tablename FROM pg_tables WHERE schemaname = 'public' ;";
    $sth = $pdo->prepare($sql);
    $array = array();
    $sth->execute($array);
    $result = $sth->fetchAll();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        echo "A query error occurred.\n";
        echo $connect_pdo;
        echo 'Error' . $stherr[0] . '<br>';
        echo 'Error' . $stherr[1] . '<br>';
        echo 'Error' . $stherr[2] . '<br>';
    }

    if (sizeof($result) > 10) {
        //we found the database
        return true;
    } else {
        return false;
    }
}

function database_has_data($dbhost, $dbport, $dbname, $dbuser, $dbpasswd) {

    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbname;
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sql = "SELECT id FROM boewrs ;";
    $sth = $pdo->prepare($sql);
    $array = array();
    $sth->execute($array);
    $result = $sth->fetchAll();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        echo "A query error occurred.\n";
        echo $connect_pdo;
        echo 'Error' . $stherr[0] . '<br>';
        echo 'Error' . $stherr[1] . '<br>';
        echo 'Error' . $stherr[2] . '<br>';
    }

    if (sizeof($result) > 0) {
        //we found the database
        return true;
    } else {
        $sql = "SELECT id FROM toys ;";
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);
        $result = $sth->fetchAll();

        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            echo "A query error occurred.\n";
            echo $connect_pdo;
            echo 'Error' . $stherr[0] . '<br>';
            echo 'Error' . $stherr[1] . '<br>';
            echo 'Error' . $stherr[2] . '<br>';
        }

        if (sizeof($result) > 0) {
            //we found the database
            return true;
        } else {
            return false;
        }
    }
}

/**
 * Determines if the toybase database has any libraries in it.
 * if there is atleast 1 library setup then the function returns true
 * if there are no libraries setup then it returns false
 * @param type $dbhost
 * @param type $dbport
 * @param type $toybasedbname
 * @param type $toybasedbuser
 * @param type $toybasedbpasswd
 * @return boolean
 */
function toybase_database_number_libraries($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd) {

    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $toybasedbname;
    $pdo = new PDO($connect_pdo, $toybasedbuser, $toybasedbpasswd);
    $sql = "SELECT * FROM libraries ;";
    $sth = $pdo->prepare($sql);
    $array = array();
    $sth->execute($array);
    $result = $sth->fetchAll();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        echo "A query error occurred.\n";
        echo $connect_pdo;
        echo 'Error' . $stherr[0] . '<br>';
        echo 'Error' . $stherr[1] . '<br>';
        echo 'Error' . $stherr[2] . '<br>';
    }

    return sizeof($result);
}

/**
 * gets the minor version of the database.
 * @param type $dbhost
 * @param type $dbport
 * @param type $dbname
 * @param type $dbuser
 * @param type $dbpasswd
 * @return type
 */
function get_version($dbhost, $dbport, $dbname, $dbuser, $dbpasswd) {

    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbname;
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sql = "SELECT minor FROM version WHERE item = 'db' ;";
    $sth = $pdo->prepare($sql);
    $array = array();
    $sth->execute($array);
    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        echo "A query error occurred.\n";
        echo $connect_pdo;
        echo 'Error' . $stherr[0] . '<br>';
        echo 'Error' . $stherr[1] . '<br>';
        echo 'Error' . $stherr[2] . '<br>';
    }

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        $minor = $row['minor'];
    }

    return $minor;
}

/**
 * gets the database password for a library.
 * looks in the toybase database and extracts the dbpassword
 * @param type $dbhost
 * @param type $dbport
 * @param type $toybasedbname
 * @param type $toybasedbuser
 * @param type $toybasedbpasswd
 * @param type $library_code
 * @return type
 */
function get_library_database_password($dbhost, $dbport, $toybasedbname, $toybasedbuser, $toybasedbpasswd, $library_code) {
    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $toybasedbname;
    $pdo = new PDO($connect_pdo, $toybasedbuser, $toybasedbpasswd);
    $sql = "SELECT dbpassword FROM libraries WHERE library_code = ? ;";
    $sth = $pdo->prepare($sql);
    $array = array($library_code);
    $sth->execute($array);
    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        echo "A query error occurred.\n";
        echo $connect_pdo;
        echo 'Error' . $stherr[0] . '<br>';
        echo 'Error' . $stherr[1] . '<br>';
        echo 'Error' . $stherr[2] . '<br>';
    }

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        $dbpassword = $row['dbpassword'];
    }

    return $dbpassword;
}
