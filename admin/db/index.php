<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/menu.css" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/login.css">
</head>


<?php
/* 
 * Copyright (C) 2019 Mibase
 *
 * This program is provided under the terms of the GNU General Public License version 3
 * refer licence.html in root folder for full text.
 */

require( dirname(__FILE__) . '/../config.php');

$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
if (isset($password)) {
//limit length
    if ((strlen($password)) > 31) {
        echo 'Login Error: Password exceeds minimum length of 30 charaters';
        exit;
    }
//limit allowed charaters   
    if (preg_match('/^[\w@]*$/', $password) == 0) {
        echo 'Login error: Password can only contain word charaters and numbers and -_ charaters';
        exit;
    }
}

$host = filter_input(INPUT_SERVER, "SERVER_NAME", FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
//$host = $_SERVER["SERVER_NAME"];
//validate the input
//limit length
if ((strlen($host)) > 31) {
    echo 'Login Error: Server Name exceeds minimum length of 30 charaters';
    exit;
}
//limit allowed charaters  
if (preg_match('/^[\w\.\-]*$/', $host) == 0) {
    echo 'Host Name Error: Host name can only contain word charaters and numbers _ - and . charaters';
    exit;
}

if (!isset($db_admin_password) || $db_admin_password == 'CHANGE ME' || strlen($db_admin_password) < 1) {
    ?>
    <body style="background-image:url('css/images/bk-admin.svg');background-repeat: no-repeat;center: fixed; 
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;" >


        <div class="container">
            <div class="row">
                <div class="card card-container">
                    <h1>Mibase Database Administrator Login</h1>
                    <div class="col-md-6" style="color: red">
                        Password in config.php is the default or is not set<br>
                        Please update $db_admin_password in config.php<br>
                        Please use a strong password.
                    </div>           
                </div>
            </div>
            <br><a href="../login.php" class="btn btn-info" role="button"> Return to Login</a><br>
        </div>
    </body>

    <?php
    exit();
}

$error = '';

if (isset($password)) {
//the user has attempted to log in

    if ($password == $db_admin_password) {
        //login successful
        //start a session 
        if (!session_id()) {
            session_start();
        }

        $_SESSION['dbadmin'] = 'true';


        if ($shared_server) {
            $headerStr = 'location:' . $app_root_location . '/db/manage.php';
            header($headerStr);
            exit();
        } else {
            //and forward the user to the admin home pages.
            $headerStr = 'location:' . $app_root_location . '/db/manage.php';
            header($headerStr);
            exit();
        }
    } else {
        //login failed
        $error = 'login failed';
    }
}
?>


<body style="background-image:url('css/images/bk-admin.svg');background-repeat: no-repeat;center: fixed; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;" >


    <div class="container">
        <div class="row">
            <div class="card card-container">
                <h1>Mibase Database Administrator Login</h1>
                <form name="form1" method="post" action="index.php" class="form-signin">
                    <span id="reauth-email" class="reauth-email" style="color: red"><?php echo $error ?></span>
                    <div class="col-md-6">
                        <input name="password" type="password" id="password" class="form-control" placeholder="Password">
                        <input type="submit" name="Submit" value="Login" class="btn btn-info" role="button">  
                    </div>           
                </form>  
            </div>
        </div>
        <br><a href="../login.php" class="btn btn-info" role="button"> Return to Login</a><br>
    </div>
</body>



<?php

function get_host_name($fqn, $shared_domain) {
    return substr($fqn, 0, strlen($fqn) - strlen($shared_domain) - 1);
}
