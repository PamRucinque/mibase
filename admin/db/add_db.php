<?php
/* 
 * Copyright (C) 2019 Mibase
 *
 * This program is provided under the terms of the GNU General Public License version 3
 * refer licence.html in root folder for full text.
 */

require( dirname(__FILE__) . '/../config.php');

//const SUCCESS = 0;
//const FAIL = 1;
//const ERROR = 2;
//if the shared server is not defined then set it to be a single instance server
if (!isset($shared_server)) {
    $shared_server = false;
}

//if the $db_postgres_password is defined then set this user and password for the database and toybase user and password
if (isset($db_postgres_password)) {
    if ($shared_server) {
        $toybasedbuser = 'postgres';
        $toybasedbpasswd = $db_postgres_password;
        if (!isset($toybasedbname)) {
            $toybasedbname = 'toybase';
        }
    } else {
        if (!isset($dbname)) {
            $dbname = 'mibase';
        }
        $dbuser = 'postgres';
        $dbpasswd = $db_postgres_password;
    }
}

if (!session_id()) {
    session_start();
}

if (!isset($_SESSION['dbadmin'])) {
    // there is a session but no session variable app 
    // redirect the user to the login page
    header('location:' . $_SESSION['app_root_location'] . '/db/index.php');
    exit();
} else if ($_SESSION['dbadmin'] != 'true') {
    // there is a session with session variable app but the app session variable is not admin 
    // ( ie they are not logged in to the admin app ) then send them to the login page.
    header('location:' . $_SESSION['app_root_location'] . '/db/index.php');
    exit();
}

$note = "";


if (!$shared_server) {
    //a single server
    $note .= "Add DB is only for a shared system<br>";
} else {
    //a shared server
    //get the library_code from the input post of get variables.
    $library_code = filter_input(INPUT_GET, 'library_code', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    if (!isset($library_code)) {
        //try and get it from post
        $library_code = filter_input(INPUT_POST, 'library_code', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    }

    if (isset($library_code)) {

        $dbname = $library_code;
        //a library_code was sent to us
        $dbpassword = genPassword(10);

        $connect_str = "host='" . $dbhost . "' port=" . $dbport . " dbname='template1' user='" . $toybasedbuser . "' password='" . $toybasedbpasswd . "'";
        $dbconn = pg_connect($connect_str);

        $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $toybasedbname;
        $pdo = new PDO($connect_pdo, $toybasedbuser, $toybasedbpasswd);

        $note = '';

        try {
            if (!userExists($library_code, $dbconn)) {
                createUser($library_code, $dbpassword, $dbconn);
                $note .= "User: " . $library_code . " created on database server.<br>";
            } else {
                //the user already exists
                $note .= "User: " . $library_code . " already exists.<br>"
                        . "<span style=\"color:red;\">Note this users password is not known, you will need to update the users password in the toybase (database) libraries (table)</span><br>";
            }

            if (!database_exists($library_code, $pdo)) {
                createDatabase($library_code, $dbconn);
                $note .= "Database: " . $library_code . " created successfully<br>";
            } else {
                $note .= "Database: " . $library_code . " already exists<br>";
            }

            if (addDBtoToybase($dbname, $dbpassword, $pdo)) {
                $note .= "Library: " . $dbname . " added to toybase database.<br>";
            }
            pg_Close($dbconn);
        } catch (Exception $e) {
            $note .= 'Exception: ' . $e->getMessage() . "<br>";
        }
    } else {
        //no library code was passed to the page
        $note .= "Error no library_code passed to page as get variable.<br>";
    }
}

/**
 * Generates a password
 * @param type $length  length of the password
 * @return the password
 */
function genPassword($length = 10) {
    $chars = 'bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ0123456789!@#$%^&*()';
    $count = strlen($chars) - 1;
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count);
        $result .= substr($chars, $index, 1);
    }
    return $result;
}

function userExists($user, $dbconn) {
    $sql = "SELECT * FROM pg_catalog.pg_user WHERE usename = '" . $user . "' ;";
    $result = pg_query($dbconn, $sql);
    if (!$result) {
        throw new Exception(pg_last_error($dbconn) . "<br>");
    } else if (pg_numrows($result) > 0) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function createUser($user, $password, $dbconn) {
    $sql = "CREATE USER " . $user . " WITH LOGIN NOSUPERUSER INHERIT CREATEDB NOCREATEROLE NOREPLICATION PASSWORD '" . $password . "' ;";
    $result = pg_query($dbconn, $sql);
    if (!$result) {
        throw new Exception(pg_last_error($dbconn) . "<br>");
    } else {
        return TRUE;
        // $r['note'] = "Library user: " . $user . " added to database server.<br>";
    }
}

function database_exists($dbname, $pdo) {
    $r = array();
    $sql = "SELECT datname FROM pg_catalog.pg_database WHERE datname = ? ;";
    $sth = $pdo->prepare($sql);
    $array = array($dbname);
    $sth->execute($array);
    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        throw new Exception($stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>');
    }
    if (sizeof($result) == 1) {
        //we found the database
        return TRUE;
    } else {
        return FALSE;
    }
}

function createDatabase($dbname, $dbconn) {
    $sql = "CREATE DATABASE " . $dbname . " WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';";
    $result = pg_query($dbconn, $sql);
    if (!$result) {
        throw new Exception(pg_last_error($dbconn) . "<br>");
    } else {
        return TRUE;
        //$r['note'] = "Database: " . $dbname . " created successfully<br>";
    }
}

function addDBtoToybase($dbname, $dbpassword, $pdo) {
    $sql = "INSERT INTO libraries ( library_code , dbpassword ) VALUES ( ?,?);";
    $sth = $pdo->prepare($sql);
    $array = array($dbname, $dbpassword);
    $row_count = $sth->execute($array);
    // $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        throw new Exception($stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>');
    }
    if ($row_count == 1) {
        return TRUE;
    } else {
        return FALSE;
    }
}
?>

<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width">
    <link href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src='../js/jquery-3.0.0.js' type='text/javascript'></script> 
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="card card-container">
                <h1>Add Library</h1>
                <div><?php echo $note ?></div>
                <a href="manage.php" class="btn btn-info" role="button"> Return </a>
            </div>
        </div>
    </div>
</body>

