<?php
require( dirname(__FILE__) . '/../mibase_check_login.php');
?>
<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include( dirname(__FILE__) . '/../header/head.php');
        ?>
        <link href="../css/jquery-ui.min.css" rel="stylesheet" type="text/css">
        <script src="../js/jquery-2.0.3.min.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <script>
            function setFocus()
            {
                document.getElementById("scanid").focus();
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }
            }
        </script>

        <script type="text/javascript">
            $(function () {

                //autocomplete
                $(".auto").autocomplete({
                    source: "../loans/data/search.php",
                    autoFocus: true,
                    select: function (event, ui) {
                        //For better understanding kindly alert the below commented code
                        //alert(ui.toSource()); 
                        var selectedObj = ui.item;
                        //alert(selectedObj.value);
                        document.getElementById('idcat').value = selectedObj.value
                        document.forms["change_toy"].submit();
                    }
                });
            });
        </script>
    </head>

    <body onload="setFocus()"> 
        <section class="container fluid">
            <div class="row">
                <div class="col-sm-12">
                    <?php
                    if (!session_id()) {
                        session_start();
                    }
                    if ($_SESSION['settings']['weekly_fine'] == '') {
                        include( dirname(__FILE__) . '/../functions.php');
                        $_SESSION['settings'] = fillSettingSessionVariables($connect_str);
                    }

                    $str_return_btn = '';

                    include( dirname(__FILE__) . '/../menu.php');
                    include( dirname(__FILE__) . '/process.php');
                    include( dirname(__FILE__) . '/data/get_toy.php');
                    include( dirname(__FILE__) . '/returns_list.php');
                    if ($idcat != '') {
                        $toy_str = '<h4><strong>' . $idcat . ':</strong> ' . $toy['toyname'] . '</h4>';
                        $toy_str .= '<font color="green">' . $_SESSION['return_status'] . '</font><br>';
                        $toy_str .= $due_status;
                    } else {
                        $toy_str = 'Select or Scan a Toy!';
                    }
                    if ($toy_exists == 'Yes') {
                        if ($toy['transid'] != null) {
                            $str_return_btn = '<a href="return_toy.php?idcat=' . $toy['idcat'] . '"><button style="background-color: #FFA500; color: black;">Return ' . $idcat . '</button></a>';
                        } else {
                            if ($idcat != '') {
                                $str_return_btn = '<br>This toy is in the Library';
                            }
                        }
                        $str_toy_alert = '' . $toy['alert'];
                    } else {
                        $str_toy_alert = '';
                    }
                    $modal_str = '<font color="red">' . $str_toy_alert . '</font><br>';
                    $modal_str .= '<font color="blue">' . $_SESSION['fine'] . '</font><br>';
                    ?>

                </div>
            </div>
            <div id='msg' style='display: none;'><?php echo $str_toy_alert . $_SESSION['fine']; ?></div>
        </section>
        <section class="container">
            <div class="container-fluid">
                <div class="row"  style="background-color: #f5f5f5;">
                    <div class="col-md-2" style="padding-right: 5px;">
                        <form class="form-inline" id="scan" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" id="scanid"  style="height: 25px;"  size="10"  name="scanid" placeholder="Scan Barcode"  onchange='this.form.submit()'>
                            </div>
                        </form>

                    </div>
                    <div class="col-md-2">
                        <form class="form-inline" id="change_toy" method="post"  action="index.php" >
                            <div class="form-group">
                                <input type="text" class="auto" id="toyid"   name='toyid'  size="20"  name="scanid" placeholder="Select a Toy">
                                <input type="hidden" id="idcat" name ="idcat" />
                            </div>
                        </form>

                    </div>
                    <div class="col-md-4"><?php echo $toy_str; ?></div>
                    <div class="col-md-2"><?php echo $str_pic; ?></div>
                    <div class="col-md-2"><?php echo $total_returns . $str_return_btn; ?></div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="toy_alert" class="col-md-6" style="background-color: #f2dede;"><?php echo $str_toy_alert; ?></div>
                    <div id="fine_alert" class="col-md-6" style="background-color: #d9edf7;"><?php echo $_SESSION['fine']; ?></div>
                </div>

                <!-- Trigger the modal with a button -->
                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Alerts</h4>
                            </div>
                            <div class="modal-body">
                                <p><?php echo $modal_str; ?></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <?php
            echo $returns_txt;
            $_SESSION['fine'] = '';
            $_SESSION['return_status'] = '';
            ?>
        </section>
        <script type="text/javascript" src="../js/menu.js"></script>
    </body>
</html>




