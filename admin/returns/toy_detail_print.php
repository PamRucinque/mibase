<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
require( dirname(__FILE__) . '/../mibase_check_login.php');

//get configuration data
//include(__DIR__ . '/../config.php');
$alert = '';

include( dirname(__FILE__) . '/../loans/data/get_toy.php');

//$id = $_GET['id'];
$idcat = $_SESSION['idcat'];
//echo "id:" . $id;
//include 'get_toy.php';
$logo = $_SESSION['logo'];

//$desc1 = str_replace("\r\n", "</br>", $desc1);
//$desc2 = str_replace("\r\n", "</br>", $desc2);
$str_detail = '';
$str_pic = '';



if ($_SESSION['shared_server']) {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/' . strtolower($idcat) . '.jpg';
    } else {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . $_SESSION['library_code'] . '/blank.jpg';
    }
} else {
    $file_pic = $_SESSION['web_root_folder'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
    if (file_exists($file_pic)) {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/' . strtolower($idcat) . '.jpg';
    } else {
        $pic_url = $_SESSION['web_server_protocol'] . '://' . $_SESSION['host'] . $_SESSION['toy_images_location'] . '/blank.jpg';
    }
}

//$linktotoy = '<a class="button1" href="../../admin/toys/update/toy_detail.php?idcat=' . $idcat . '">' . $idcat . '</a>';
if ($toy_exists == 'Yes') {
    $str_detail = '<br><h3><font color="blue">' . $idcat . ': </font>  ' . $toyname . '</h3> ';
} else {
    $str_detail = '<br><h3><font color="blue">Please Select a Toy</font></h3>';
}


if ($toy_exists == 'No') {
    // echo '<h3><font color="red">This Toy Does Not Exist.</font></h3> ';
} else {
    $_SESSION['idcat'] = $idcat;
    if ($toy_status != 'ACTIVE') {
        $str_detail .= '<h3><font color="red">This Toy is LOCKED or WITHDRAWN.</font></h3> ' . $toy_status;
    }
    //$str_detail .= '<p><font color="red">' . $alert . ' ' . $warnings . '</font></p>';
    if ($rent > 0) {
        // $str_detail .= '<p><font color="red">Rent: ' . $rent . '</font></p>';
    }
}
//$link = '<a class="button_small" href="loan.php?b=' . $borid . '"> ' . $borid . ' </a>';
//$loan_to_current = ' <a class="button_small_yellow" href="loan_to_current.php?transid=' . $trans_id .  '">Return ' . $_SESSION['idcat'] . '</a>';
if (isset($_SESSION['loan_status'])) {
    $str_detail .= ' <font color="green">' . $_SESSION['loan_status'] . '</font> ';
}

$str_detail .= ' <font color="red">' . $alert . '</font> ';

if (($toy_exists == 'Yes') && ($onloan > 0)) {
    $_SESSION['loan_status'] = ' ';

    //$_SESSION['return_alert'] = '';
    $ref2 = 'return_toy.php?id=' . $transid;
    $str_detail .= "<td width='200px' valign='top' align='left'><br><a class ='button_yellow_loan' href='" . $ref2 . "'>Return</a></td>";
}
?>
<?php

//echo $str_parts;
//include ('parts.php');
//$alert .= $str_parts_msg;

if (($alert != '')) {
    $alert = str_replace('<br>', '\n', $alert);
    echo '<script>
        $(document).ready(function(){
		alert("' . $alert . '");});
        </script>';
}

if (file_exists($file_pic)) {
    $img = '<img height="75px" src="' . $pic_url . '" alt="Toy Image">';
} else {
    $img = ' ';
}




if (isset($_SESSION['idcat'])) {
    $str_pic = '<br><a>' . $img . '</a>';
}
$_SESSION['loan_status'] = ' ';
$_SESSION['return_alert'] = '';
?>





