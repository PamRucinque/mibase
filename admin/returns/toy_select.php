<?php
require( dirname(__FILE__) .  '/../mibase_check_login.php');
?>
<link href="../css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-2.0.3.min.js"></script>
<script src="../js/jquery-ui.js"></script>
<script>
    function setFocus()
    {
        document.getElementById("scanid").focus();
    }
</script>
<script type="text/javascript">
    $(function () {

        //autocomplete
        $(".auto").autocomplete({
            source: "../loans/data/search.php",
            autoFocus: true,
            select: function (event, ui) {
                //For better understanding kindly alert the below commented code
                //alert(ui.toSource()); 
                var selectedObj = ui.item;

                //alert(selectedObj.value);
                document.getElementById('idcat').value = selectedObj.value
                document.forms["change_toy"].submit();
            }

        });

    });
</script>


<form class="form-inline" id="scan" method="post">
    <div class="form-group">
        <input type="text" class="form-control" id="scanid"  style="height: 25px;"  size="12"  name="scanid" placeholder="Scan Barcode"  onchange='this.form.submit()'>
    </div>
</form>

<form class="form-inline" id="change_toy" method="post"  action="index.php" >
    <div class="form-group">
        <input type="text" class="auto" id="toyid"   name='toyid'  size="25"  name="scanid" placeholder="Select a Toy">
        <input type="hidden" id="idcat" name ="idcat" />
    </div>
</form>


