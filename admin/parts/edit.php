<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');
?>
<html lang="en">
    <head>
        <?php
        include('../header.php');
        ?> 

        <script type="text/javascript">
            $(function () {
                var pickerOpts = {
                    dateFormat: "d MM yy",
                    showOtherMonths: true

                };
                $("#datepart").datepicker(pickerOpts);
            });

        </script>
    </head>
    <script>
        function setFocus()
        {
            var msg = document.getElementById("msg").innerText;
            //alert(msg);
            if (msg !== '') {
                $('#myModal').modal('show');
            }

        }
    </script>

    <body  onload="setFocus()">
        <div id="form_container">

            <?php
            include('../menu.php');
            //include('../header/head.php');
            include('../connect.php');
            include('p_class.php');
            $button_str = 'OK';
            $java_str = "$(location).attr('href', '../toys/update/toy_detail.php')";
            $str_alert = '';
            $id=0;
            //$id = 298;
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
            }

            $part = new Part();

            //When the user submits the request:
            if (isset($_POST['submit'])) {
                //$_POST['toyname'] = $part->toyname;

                $part->fill_from_array($_POST);
                $part->save();
                if ($part->result) {
                    $str_alert .= '<br><br>This Part has been saved.<br>';
                } else {
                    $str_alert .= $part->sql_error;
                    echo $str_alert;
                }
            } else {
                $part->data_from_table($id);
                include('edit_form.php');
            }
            ?>
        </div>
        <?php
        include ('msg_form.php');
        //$redirect = "Location: repairers.php";               //print $redirect;
        //header($redirect);
        ?>
    </body>
</html>