<?php

class Part {

    public $id, $datepart, $idcat, $toyname, $description, $typepart, $qty, $found, $itemname,
            $borid, $cost, $sql_error, $result, $alertuser, $comments, $paid, $borname;

    public function data_from_table($id) {

        $this->result = false;

        $sql = "select parts.*, (firstname || ' ' || surname) as borname, toyname  
            from parts 
            left join borwrs on  parts.borcode = borwrs.id
            left join toys on  toys.idcat = parts.itemno
            where parts.id = ?;";
        $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

        $sth = $pdo->prepare($sql);
        $array = array($id);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        for ($ri = 0; $ri < $numrows; $ri++) {
            $r = $result[$ri];
            $this->id = $r['id'];
            $this->datepart = $r['datepart'];
            $this->idcat = $r['itemno'];
            $this->itemname = $r['itemname'];
            $this->description = $r['description'];
            $this->typepart = $r['type'];
            $this->borid = $r['borcode'];
            $this->cost = $r['cost'];
            $this->alertuser = $r['alertuser'];
            $this->comments = $r['comments'];
            $this->paid = $r['paid'];
            $this->toyname = $r['toyname'];
            $this->borname = $r['borname'];
            $this->result = true;
        }
        $this->numrows = $numrows;
    }

    public function fill_from_array($r) {
        $this->id = $r['id'];
        $this->idcat = $r['idcat'];
        $this->datepart = $r['datepart'];
        $this->idcat = $r['idcat'];
        $this->toyname = $r['toyname'];
        $this->description = $r['description'];
        $this->borid = $r['borid'];
        $this->type = $r['type'];
        $this->cost = $r['cost'];
        $this->alertuser = $r['alertuser'];
        $this->comments = $r['comments'];
        $this->paid = $r['paid'];
    }

    public function save() {

        $this->result = false;

        $sql = "update parts "
                . "set datepart=?,  "
                . " itemno=?, itemname=?, description=?, type=?, borcode=?, "
                . "cost=?, alertuser=?, comments=?, paid=? "
                . "WHERE id = ?;";

        if ($this->id == 0) {
            $sql = "INSERT INTO parts (datepart, itemno,itemname,description, type, "
                    . "borcode,cost, alertuser,comments,paid, id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
            $id = $this->get_newid();
            //echo $id;
        } else {
            $id = $this->id;
        }

        $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
        $sth = $pdo->prepare($sql);

        $u = array();
        $u[] = $this->datepart;
        $u[] = $this->idcat;
        $u[] = $this->toyname;
        $u[] = $this->description;
        $u[] = $this->type;
        $u[] = $this->borid;
        $u[] = $this->cost;
        $u[] = $this->alertuser;
        $u[] = $this->comments;
        $u[] = $this->paid;
        $u[] = $id;
        $sth->execute($u);

        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            //echo "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
            $this->sql_error = "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
            $this->result = false;
        } else {
            $this->result = true;
        }
    }

    public function get_newid() {

        $max = 0;
        $sql = "select max(id) as max from parts;";
        $conn = pg_connect($_SESSION['connection_str']);
        $nextval = pg_Exec($conn, $sql);
        $row = pg_fetch_array($nextval);
        $max = $row['max'];
        $max = $max + 1;
        return $max;
    }

    public function delete($id) {

        $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
        $sql = "delete from repairers where id = ?;";
        $sth = $pdo->prepare($sql);

        $array = array($id);

        $sth->execute($array);
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            //echo "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
            $this->sql_error = "A delete error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
            $this->result = false;
        } else {
            $this->result = true;
        }
    }

    public static function get_all_r() {

        $sql = "select * from parts order by datepart desc;";

        $pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $rs = array();
        for ($ri = 0; $ri < $numrows; $ri++) {
            $r = new Part();
            $r->fill_from_array($result[$ri]);
            $rs[] = $r;
        }

        return $rs;
    }

}
