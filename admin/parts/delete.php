<html lang="en">
    <head>
        <?php
        include('../header/head.php');
        ?>
    </head>
    <script>
        function setFocus()
        {
            var msg = document.getElementById("msg").innerText;
            //alert(msg);
            if (msg !== '') {
                $('#myModal').modal('show');
            }

        }
    </script>

    <body  onload="setFocus()">
        <div id="form_container">
            <?php
            if (isset($_POST['id_delete']) && is_numeric($_POST['id_delete'])) {
                $group_id = $_POST['id_delete'];
                include('../connect/connect.php');
                include('r_class.php');
                $button_str = 'OK';
                $java_str = "$(location).attr('href', 'repairers.php')";
                $repairer = new Repairer();
                $repairer->delete($group_id);
                if ($repairer->result) {
                    $str_alert = 'Repairer Successfully Deleted.';
                } else {
                    $str_alert .= $repairer->sql_error;
                }
                echo $str_alert;
                //include ('msg_form.php');
            }
            $redirect = "Location: repairers.php";               //print $redirect;
            header($redirect);
            ?>
        </div>
    </body>
</html>


