<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php
        include('../../header.php');
        ?> 

        <script type="text/javascript">
            $(function () {
                var pickerOpts = {
                    dateFormat: "d MM yy",
                    showOtherMonths: true

                };
                $("#datepart").datepicker(pickerOpts);
            });

        </script>


    </head>
    <body>
        <div id="form_container">
            <?php
            include('../../menu.php');
            include('../../connect.php');
            
            ?>

            <?php
            session_start();
            $today = date('Y-m-d');


            if (isset($_POST['submit'])) {
                $_SESSION['idcat_part'] = $_POST['idcat'];

                include('new_partid.php');


                if ($_POST['alert'] == 'on') {
                    $alert_txt = 'TRUE';
                } else {
                    $alert_txt = 'FALSE';
                }
                if ($_POST['paid'] == 'on') {
                    $paid_txt = 'TRUE';
                } else {
                    $paid_txt = 'FALSE';
                }
                //$toyid = sprintf("%02s", $toyid);

                $description = pg_escape_string($_POST['description']);
                $parttype = pg_escape_string($_POST['parttype']);
                $description = clean($description);
                //$idcat = $_GET['idcat'];
                //echo $idcat;
                $query = "INSERT INTO parts (id, itemno, description, type, cost, borcode, datepart, alertuser, paid)
         VALUES ({$newpartid}, 
        '{$_POST['idcat']}', 
        '{$description}', '{$parttype}', {$_POST['cost']}, {$_POST['borid']},
        '{$_POST['datepart']}', '{$alert_txt}', '{$paid_txt}')";

                $result = pg_Exec($conn, $query);


                if (!$result) {
                    echo "An INSERT query error occurred.\n";
                    echo $query;
                    //echo $connection_str;
                    exit;
                }
// Get the last record inserted
// Print out the Contact ID
                else {
                    //$newid = $newid;
                    $edit_url = '../update/toy_detail.php?idcat=' . $_SESSION['idcat_part'];

                    echo "<br>The record was successfully entered and the ID is:" . $newpartid . "<br><br>";
                    //echo 'format Toy id: ' . $format_toyid;
                    echo '<a class="button1" href="' . $edit_url . '">View Toy with parts list</a>';
                    echo '<a class="button1" href="../toys.php">Toy List</a>';



                    // print $query;
                    //include('send_email.php');
                    // include('send_email_insurance.php');
                }

                pg_FreeResult($result);
// Close the connection
                pg_Close($conn);
            } else {
                include('new_partid.php');
                //include('find/find_member.php');
                include('new_form_part.php');
            }

            function clean($input) {
                $output = stripslashes($input);
                $output = str_replace("'", "`", $output);
                $output = str_replace("/", "-", $output);
                return $output;
            }
            ?>
        </div>
    </body>
</html>