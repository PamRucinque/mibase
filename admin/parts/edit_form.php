<div class="container-fluid" style="padding: 10px;">
    <div class="row">
        <div class="col-sm-8">
            <?php

            if ($id == 0) {
                echo '<h2>New Part</h2>';
            } else {
                echo '<h2>Edit Part</h2>';
                echo '<h4><font color="blue">' . $part->idcat . ': ' . $part->toyname . '</font></h4>';
            }
            ?>
        </div>
        <div class="col-sm-2">
            <br><a href='../toys/update/toy_detail.php' class ='btn btn-info'>Back to Toy Details</a><br>
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-1">
            <br><a target="target _blank" href='https://www.wiki.mibase.org/' class ='btn btn-default' style="background-color: gainsboro;">Help</a><br>
        </div>
    </div>
</div>
<?php
$alertuser = 'No';
$paid = 'No';

if (($part->alertuser == TRUE)) {
    $alertuser = 'Yes';
}
if ($part->paid == TRUE) {
    $paid = 'Yes';
}
$toyname = $part->toyname;
if ($part->itemname == ''){
    $toyname = $part->toyname;
}
//echo 'alert: ' . $alertuser . '<br>';
//echo 'paid: ' . $paid . '<br>';
?>
<div class="container-fluid" style="padding: 10px;">
    <form action="edit.php" method="post">
        <div class="row" style="background-color:whitesmoke;padding-bottom: 70px;padding-left: 10px;">
            <div class="col-sm-6">
                <label for="description"><b>Missing Part Details:</b></label>
                <input type="text" class="form-control" id="description" placeholder="" name="description" value="<?php echo $part->description; ?>">
                <br>
                <label for="description"><b>Toyname:</b></label>
                <input type="text" class="form-control" id="toyname" placeholder="" name="toyname" value="<?php echo $toyname; ?>">

                <div class="row">

                    <div class="col-sm-4">
                        <label for="datepart"><b>Date:</b></label>
                        <input type="text" class="form-control" id="datepart" placeholder="" name="datepart" value="<?php echo $part->datepart; ?>">

                    </div>
                    <div class="col-sm-3">
                        <label for="cost"><b>Cost:</b></label>
                        <input type="text" class="form-control" id="cost" placeholder="" name="cost" value="<?php echo $part->cost; ?>">

                    </div>
                    <div class="col-sm-5">
                        <label for="part_type"><b>Type of Part:</b></label>
                        <?php include('part_type.php'); ?>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <br><b>Alert User:</b> 
                        <select name='alertuser' id='alertuser' style='min-width: 200px' class='form-control'>
                            <option selected='selected' value='<?php echo $alertuser; ?>' ><?php echo $alertuser; ?></option>
                            <option value="No">No</option>
                            <option value="Yes">Yes</option>

                        </select> 

                    </div>
                    <div class="col-sm-6">
                        <br><b>Paid deposit for missing part:</b> 
                        <select name='paid' id='paid' style='min-width: 200px' class='form-control'>
                            <option selected='selected' value='<?php echo $paid; ?>' ><?php echo $paid; ?></option>
                            <option value="No">No</option>
                            <option value="Yes">Yes</option>

                        </select> 
                    </div>
                </div>
            </div>


            <div class="col-sm-4"  id="contact">
                <div class="row">
                    <label for="notes"><b>Notes:</b></label>
                    <textarea id="comments" class="form-control" name="comments" rows="4"><?php echo $part->comments; ?></textarea>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <br><b>Member Responsible:</b>
                        <?php include('get_member.php'); ?>

                    </div>
                </div>


            </div>
            <div class="col-sm-12" style="padding-top: 20px;align-content: right;">
                <input type=submit id="submit" name="submit" class="btn btn-success" value="Save Part"> <br>
                <input type="hidden" class="form-control" id="id" placeholder="" name="id" value="<?php echo $part->id; ?>">
                <input type="hidden" class="form-control" id="idcat" placeholder="" name="idcat" value="<?php echo $part->idcat; ?>">

            </div>
        </div>



        <div style="height:100px;"></div>
</form>
</div>

<?php include ('msg_form.php'); ?>




