<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include('../header/head.php');
        ?>

    </head>
    <script type="text/javascript">
        function edit(str) {
            //document.forms['form_edit']['user_id_edit'].value = str;
            //alert(str);
            document.getElementById("id_edit").value = str;
            document.getElementById("form_edit").submit();
        }

        function del(str) {
            //alert(str);
            document.getElementById("id_delete").value = str;
            document.getElementById("form_delete").submit();
        }
        $(document).ready(function () {
            $('#rs').DataTable({
                "aLengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
                "iDisplayLength": 200,
                "order": [[2, "asc"]],
                "orderClasses": false,
                "searching": true,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'th'
                    }
                },
                "pageLength": 500,
                "paging": true,
                language: {
                    searchPlaceholder: "Search Repairers",
                    searchClass: "form-control",
                    search: "",
                    "lengthMenu": "Display _MENU_  records per page",
                }

            });
            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_length select').addClass('recs');
        });
    </script>

    <body>


        <div class="container-fluid">
            <?php
            include('../header/header.php');
            include('r_class.php');
            include('functions.php');
            $repairers = Repairer::get_all_r();
            //print_r($repairers);
            $table_str = draw_table($repairers);
            $new_btn = ' <br><a class="btn btn-warning" title="Edit" href="edit.php">New Repairer</a>';
            ?>
            <div class ="row" style='padding: 20px;padding-top: 30px;'>
                <div class="col-sm-6"><h2>Resources: Repairers</h2></div>
                <div class="col-sm-6"><?php echo $new_btn; ?></div>
            </div>
            <div class="container-fluid">
                <div class ="row" style='padding-left: 20px;padding-right: 20px;'>
                    <div class="col-sm-12">
                        <?php echo $table_str; ?>
                    </div>
                </div>
            </div>
            <form id="form_delete" action="delete.php" method="post">
                <input type="hidden" class="form-control" id="id_delete" placeholder="" name="id_delete" value="">
            </form>
            <form  action="edit.php" method="post" id="form_edit">
                <input type="hidden" class="form-control" id="id_edit" placeholder="" name="id_edit" value="">
            </form>
        </div>

    </body>
</html>


