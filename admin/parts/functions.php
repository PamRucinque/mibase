<?php

function draw_table($groups) {

    $count = 0;
    $str = '';
    $numrows = sizeof($groups);

    if ($numrows > 0) {

        $str = '<table id="rs" class="table table-striped table-bordered table-sm table-hover table-responsive-sm compact nowrap order-column">';
        $str .= '<thead>
        <tr>
            <th class="th-sm">id</th>
            <th class="th-sm">Company Name</th>
            <th class="th-sm all">Address</th>
            <th class="th-sm all">Address2/ABN</th>
            <th class="th-sm all">suburb</th>
            <th class="th-sm all">postcode</th>
            <th class="th-sm all">state</th>
            <th class="th-sm all">phone</th>
            <th class="th-sm all">email</th>
            <th class="th-sm">status</th>
            <th class="th-sm all"></th>
        </tr></thead>';
    }

    foreach ($groups as $p) {
        $edit_btn = '<a class="btn btn-primary btn btn-sm" style="color:white;" onclick="edit(' . $p->id . ');">Edit</a>';
        $delete_btn = '  <a class="btn btn-danger btn btn-sm" style="color:white;" onclick="del(' . $p->id . ');">Delete</a>';
        //$edit_btn = '<a class="btn btn-primary btn btn-sm" title="Edit" href="edit.php?id=' . $p->id . '">Edit</a>';
        //$delete_btn = ' <a class="btn btn-danger btn btn-sm" title="Edit" href="delete.php?user_id=' . $p->user_id . '">Delete</a>';
        $str .= '<tr>';
        $str .= '<td>' . $p->id . '</td>';
        $str .= '<td style="max-width: 350px;">' . $p->company_name . '</td>';
        $str .= '<td>' . $p->address . '</td>';
        $str .= '<td>' . $p->address2 . '</td>';
        $str .= '<td>' . $p->suburb . '</td>';
        $str .= '<td>' . $p->postcode . '</td>';
        $str .= '<td>' . $p->state . '</td>';
       $str .= '<td>' . $p->phone . '</td>';
        $str .= '<td>' . $p->email . '</td>';
        $str .= '<td>' . $p->status . '</td>';
        $str .= '<td style="min-width: 120px;"> ' . $edit_btn . ' ' . $delete_btn . ' </td>';
        $str .= '</tr>';
        $count = $count + 1;
    }
    $str .= '</tbody></table>';
    return $str;
}

function draw_select_user($items, $item_value, $item_value_str) {
    $count = 0;
    $str = '';
    $numrows = sizeof($items);


    $str .= '<label for="user_id"><b>User:</b></label><br>';
    $str .= '<select id="user_id" name="user_id" style="width: 300px;" required>';
    $str .= '<option value="' . $item_value . '" selected="selected">' . $item_value_str . '</option>';

    foreach ($items as $p) {
        $str .= '<option value="' . $p->user_id . '" >' . $p->username . ': ' . $p->firstname . ' ' . $p->surname . '</option>';
        $count = $count + 1;
    }
    $str .= '</select><br>';
    return $str;
}
