CREATE TABLE 
favs
(
  id bigint NOT NULL DEFAULT nextval('favs_id_seq'::regclass),
  
borid bigint,
  idcat character varying,
  
CONSTRAINT favs_pk PRIMARY KEY (id)
)

WITH (
  OIDS=FALSE
);
ALTER TABLE favs
  OWNER TO maroondah;

CREATE SEQUENCE public.favs_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.favs_id_seq
  OWNER TO maroondah;
